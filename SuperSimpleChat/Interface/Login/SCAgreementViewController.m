//
//  SCAgreementViewController.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 17.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAgreementViewController.h"
#import "SCSettings.h"

@interface SCAgreementViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation SCAgreementViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.settings = [SCSettings sharedSettings];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    self.webView.delegate = self;
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"textPrivacyPolicy" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}


- (IBAction)agreeButton:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLoadApp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [_settings netInit];
    
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        //Open link in Safari
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}
@end
