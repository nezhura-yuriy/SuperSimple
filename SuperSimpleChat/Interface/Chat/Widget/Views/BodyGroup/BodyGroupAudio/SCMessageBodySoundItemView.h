//
//  SCMessageBodySoundView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 12.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyItemView.h"
#import <AVFoundation/AVFoundation.h>

@class SCSettings,SCNetFile;


@interface SCMessageBodySoundItemView : SCMessageBodyItemView <AVAudioPlayerDelegate>

//@property(nonatomic,strong) NSString* audioFile;
@property(nonatomic,strong) SCNetFile* audioNetFile;

-(void) loadAudioFile:(NSString*) audioFile;
@end

//////////////////////////////////////////////////////////

