//
//  ContactInformationViewController.m
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 15.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "ContactInformationViewController.h"
#import "UIImage+SCCropImageToCircleWithBorder.h"
#import "SCSettings.h"
#import "UIImageView+Letters.h"
#import "AboutMyTableViewCell.h"
#import "EmailTableViewCell.h"
#import "PhoneTableViewCell.h"
#import "CityTableViewCell.h"
#import "GenderTableViewCell.h"
#import "BlockContactTableViewCell.h"
#import "BirthdayTableViewCell.h"
#import "SCModelUser.h"
#import <SVProgressHUD.h>

static NSString *kStatusOnlineImgName = @"status-online";
static NSString *kStatusBusyImgName = @"status-busy";
static NSString *kStatusAwayImgName = @"status-away";
static NSString *kStatusInvisibleImgName = @"status-invisible";
static NSString *kStatusOfflineImgName = @"status-offline";

#define COUNT_CELL 7.f


typedef enum  {
    
    AboutMyCell,
    EmailCell,
    PhoneCell,
    CityCell,
    GenderCell,
    BlockCell,
    BirthdayCell
    
} SCContactCellType;

@interface ContactInformationViewController ()<SCContactItemDelegate>

@property (strong, nonatomic) AboutMyTableViewCell* abouMyCell;
@property (strong, nonatomic) EmailTableViewCell* emailCell;
@property (strong, nonatomic) PhoneTableViewCell* phoneCell;
@property (strong, nonatomic) CityTableViewCell* cityCell;
@property (strong, nonatomic) GenderTableViewCell* genderCell;
@property (strong, nonatomic) BlockContactTableViewCell* blockCell;
@property (strong, nonatomic) BirthdayTableViewCell* birthdayCell;


@end

@implementation ContactInformationViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [_tableView registerNib:[UINib nibWithNibName:@"AboutMyTableViewCell" bundle:nil] forCellReuseIdentifier:@"abouMyCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"EmailTableViewCell" bundle:nil] forCellReuseIdentifier:@"emailCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"PhoneTableViewCell" bundle:nil] forCellReuseIdentifier:@"phoneCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"cityCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"GenderTableViewCell" bundle:nil] forCellReuseIdentifier:@"genderCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"BlockContactTableViewCell" bundle:nil] forCellReuseIdentifier:@"blockCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"BirthdayTableViewCell" bundle:nil] forCellReuseIdentifier:@"birthdayCell"];

    

    
    [self settingNavigationBar];
    
}

- (void)dealloc
{
    FTLog(@"%@ dealoc",NSStringFromClass([self class]));
    
    if(_contact && _contact.user)
    {
        [_contact.user delDelegate:self];
    }
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationItem.title = @"Contact information";
    
    self.navigationController.navigationBarHidden = NO;
    
    [self.navigationController.navigationBar setBarTintColor:SKCOLOR_NavBarBg];
    
    self.navigationController.navigationBar.translucent = NO;
    
    self.tableView.backgroundColor = SKCOLOR_TabBarBg;
    
    self.view.backgroundColor = SKCOLOR_TabBarBg;
    
}

-(void)setContact:(SCContactItem *)contact
{
    if(_contact && _contact.user)
    {
        [_contact.user delDelegate:self];
    }
    _contact = contact;
    
    if(_contact.user)
        
        [_contact.user addDelegate:self];
    
    NSString* initialsForPhotos = [NSString new];
    
    if (!self.contact.contactLastName.length==0 || !self.contact.contactFirstName.length==0) {
        
        NSString* fullName = [NSString new];
        
        if (self.contact.contactLastName.length > 0) {
            
            fullName = [NSString stringWithFormat:@"%@ ",self.contact.contactLastName];
            
        }
        
        if (self.contact.contactFirstName.length > 0) {
            
            fullName = [fullName stringByAppendingString:self.contact.contactFirstName];
            
        }
        
        initialsForPhotos = fullName;
        
    } else if (!self.contact.contactNikName.length == 0) {
        
        initialsForPhotos  = self.contact.contactNikName;
        
    } else  if (!self.contact.contactPhone.length == 0) {
        
        initialsForPhotos = self.contact.contactPhone;
        
    } else {
        
        initialsForPhotos = self.contact.contactEmail;
        
    }
    
    [self.contactFotoCircular setImageWithString:initialsForPhotos color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
    
    [self.contact.user.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result)
     {
         switch (type)
         {
             case SCNetFileComletionTypeStatus:
             {
                 FTLog(@"SCNetFileComletionTypeStatus");
             }
                 break;
                 
             case SCNetFileComletionTypeCompleet:
             {
                 [self.contactFotoCircular endProgress];
                 
                 if([result isKindOfClass:[SCNetFile class]])
                 {
                     self.contactFotoCircular.image = [(SCNetFile*)result imageForSize:self.contactFotoCircular.frame.size];
                     
                     self.contactFotoBlur.image = [(SCNetFile*)result blureImage:self.contactFotoBlur.frame];
                 }
             }
                 break;
                 
             case SCNetFileComletionTypeProgress:
                 
                 [self.contactFotoCircular setProgressPercent:[result doubleValue]];
                 
                 break;
                 
             default:
                 FTLog(@"default case");
                 break;
         }
     }];

    if (self.contact.user.contactNikName.length > 0) {
        
        self.nikeNameLabel.text = [NSString stringWithFormat:@"@%@",self.contact.user.contactNikName];
        
    }
    
    [self changeStatus:_contact.user.contactStatus];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    [format setDateFormat:@"MMM dd, yyyy HH:mm"];
    
    self.lastConnectionLabelOutlet.text = [format stringFromDate:self.contact.user.lastConnection];
    
    self.lastConnectionLabelOutlet.textColor = SKCOLOR_FontRegular;
    
    self.settings.modelUsers.delegate = self;
    
    self.nikeNameView.backgroundColor = SKCOLOR_NavBarBg;
    
    [self.creatChatIcon setImage:[_settings.skinManager getImageForKeyX:@"tabbar-chats"] forState:UIControlStateNormal];



}

#pragma mark UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return COUNT_CELL;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *abouMyCell = @"abouMyCell";
    static NSString *emailCell = @"emailCell";
    static NSString *phoneCell = @"phoneCell";
    static NSString *cityCell = @"cityCell";
    static NSString *genderCell = @"genderCell";
    static NSString *blockCell = @"blockCell";
    static NSString *birthdayCell = @"birthdayCell";

    switch (indexPath.row)
    
    {
        case AboutMyCell:
            
        _abouMyCell = [tableView dequeueReusableCellWithIdentifier:abouMyCell];
        _abouMyCell.aboutMyTextField.textColor = SKCOLOR_FontRegular;

            
            if (self.contact.user.contactAbout.length > 0) {
                
                _abouMyCell.aboutMyTextField.text = self.contact.user.contactAbout;
                
            }

            return _abouMyCell;
            
            break;
            
        case EmailCell:
            
            _emailCell = [tableView dequeueReusableCellWithIdentifier:emailCell];
            _emailCell.emailTextField.textColor = SKCOLOR_FontRegular;
            _emailCell.imageIcon.image = [_settings.skinManager getImageForKeyX:@"profile-alterncont"];
            _emailCell.backgroundColor = SKCOLOR_TableCellBg;

            
            if (self.contact.user.contactEmail.length > 0) {
                
                _emailCell.emailTextField.text = self.contact.user.contactEmail;

                
            }
            
            return _emailCell;
            
            break;
            
        case PhoneCell:
            
          _phoneCell = [tableView dequeueReusableCellWithIdentifier:phoneCell];
          _phoneCell.phoneTextField.textColor = SKCOLOR_FontRegular;
          _phoneCell.backgroundColor = SKCOLOR_TableCellBg;
          _phoneCell.imageIcon.image = [_settings.skinManager getImageForKeyX:@"profile-phone"];

            
            if (self.contact.user.contactPhone.length > 0) {
                
                _phoneCell.phoneTextField.text = self.contact.user.contactPhone;
                
            }
            
            return _phoneCell;
            
            break;
            
        case CityCell:
            
            _cityCell = [tableView dequeueReusableCellWithIdentifier:cityCell];
            _cityCell.imageIcon.image =[_settings.skinManager getImageForKeyX:@"profile-city"];
            _cityCell.cityTextField.textColor = SKCOLOR_FontRegular;
            _cityCell.backgroundColor = SKCOLOR_TableCellBg;

            if (self.contact.user.contactAddress.city.length > 0) {
                
                _cityCell.cityTextField.text = self.contact.user.contactAddress.city;
                
            }
            
            return _cityCell;
            
            break;
            
        case GenderCell:
            
            _genderCell = [tableView dequeueReusableCellWithIdentifier:genderCell];
            _genderCell.textLabelOutlet.textColor = SKCOLOR_FontRegular;
            _genderCell.backgroundColor = SKCOLOR_TableCellBg;
            
            if (self.contact.user.contactSex.length > 0) {
                
                if([self.contact.user.contactSex isEqualToString:@"Male"]){
                    
                    _genderCell.genderImageView.image = [_settings.skinManager getImageForKeyX:@"gender-man-active"];
                    
                } else {
                    
                    _genderCell.genderImageView.image =[_settings.skinManager getImageForKeyX:@"gender-woman-active"];

                }
                
            }
            
            return _genderCell;
            
            break;
            
        case BlockCell:
            
            _blockCell = [tableView dequeueReusableCellWithIdentifier:blockCell];
            _blockCell.backgroundColor = SKCOLOR_TableCellBg;
            _blockCell.blockContactLabel.textColor = SKCOLOR_FontRegular;
            [_blockCell.blockContactSwitch setOnTintColor:SKCOLOR_SwitchOn];
            [_blockCell.blockContactSwitch setTintColor:SKCOLOR_SwitchOff];
            _blockCell.settings = _settings;
            
            _blockCell.contact = _contact;
            
            if (self.contact.isBlocked) {
                
                _blockCell.blockContactSwitch.on = NO;
                
                _blockCell.blockContactLabel.text = @"Unblock Contact";
            }
            
            return _blockCell;
            
            break;
            
            case BirthdayCell:

            _birthdayCell = [tableView dequeueReusableCellWithIdentifier:birthdayCell];
            _birthdayCell.birthdayTextFieldOutlet.textColor = SKCOLOR_FontRegular;
            _birthdayCell.imageIcon.image = [_settings.skinManager getImageForKeyX:@"profile-birthday"];
            
            _birthdayCell.backgroundColor = SKCOLOR_TableCellBg;
            
            if (self.contact.contactBirthdate) {
                
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                
                [format setDateFormat:@"MMM dd, yyyy HH:mm"];
                
                _birthdayCell.birthdayTextFieldOutlet.text = [format stringFromDate:self.contact.user.contactBirthdate];
            }
            
            return _birthdayCell;
            
            break;
            
    }
    
    return nil;
}

#pragma mark UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark - Initialization methods

-(void)changeStatus:(SCUserStatus) status{
    
    switch (status) {
            
        case SCUserStatusAway:
            
            self.statusImageOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusAwayImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            
            self.statusLabelOutlet.text = @"away";
            
            break;
            
        case SCUserStatusBusy:
            
            self.statusImageOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusBusyImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            self.statusLabelOutlet.text = @"busy";

            break;
            
        case SCUserStatusInvisible:
            
            self.statusImageOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusInvisibleImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            self.statusLabelOutlet.text = @"offline";

            
            break;
            
        case SCUserStatusOffline:
            
            self.statusImageOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusInvisibleImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            self.statusLabelOutlet.text = @"offline";

            
            break;
            
        case SCUserStatusOnline:
            
            self.statusImageOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusOnlineImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            self.statusLabelOutlet.text = @"online";

            
            break;
            
        default:
            break;
    }
    
}


- (UIImage *)blurWithCoreImage:(UIImage *)sourceImage
{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not
    // look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@30 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    
    // Invert image coordinates
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.view.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, self.view.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
    CGContextFillRect(outputContext, self.view.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CFRelease(cgImage);
    
    return outputImage;
}


- (void)settingNavigationBar {
    
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
}

#pragma mark - Actoins


- (void)backAction {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)creatChatAction:(id)sender {
    
     [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_START_PRIVATE_CHAT object:_contact];
    
    
}
- (IBAction)subscriptAction:(id)sender
{
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                _settings.modelUsers, @"delegate",
                                [[SCPUUID alloc] initWithRandom], @"UUID",
                                _contact.userPUUID,@"userUUID",
                                nil];
    
    [_settings.modelProfile.protoBufManager subscribeRequest:dictionary];

}

#pragma mark - SCContactItemDelegate

-(void)addUsersToBlackResponse:(id)data{
    
    
    
    
}

#pragma mark SCPBManagerDelegate <NSObject>

-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"ContactInformationViewController : receiveParsedData");
    switch (type)
    {
        case SCPBManagerParceTypeChangeStatus:
        case SCPBManagerParceTypeChangeStatusEvent:
            [self changeStatus:_contact.user.contactStatus];
            break;
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;
    }
}
@end
