//
//  SCAlternativeLoginViewController.h
//  SuperSimpleChat
//
//  Created by yury on 12.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"


@class SCContactItem;

@interface SCAlternativeLoginViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>


@property (strong, nonatomic) SCSettings *settings;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) SCContactItem* contactItem;

@end
