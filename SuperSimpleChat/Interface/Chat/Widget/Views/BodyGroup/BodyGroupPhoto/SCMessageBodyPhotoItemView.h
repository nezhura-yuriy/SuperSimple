//
//  SCMessageBodyPhotoView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyItemView.h"


@class SCSettings,SCNetFile;


@interface SCMessageBodyPhotoItemView : SCMessageBodyItemView


//@property(nonatomic,strong) NSString* photoFile;
@property(nonatomic,strong) SCNetFile* photoNetFile;


-(void) loadPhotoFile:(NSString*) photoFile;
@end


//////////////////////////////////////////////////////////



