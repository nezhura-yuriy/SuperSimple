//
//  SCMessageBodyView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"
#import "SCMessageBodyTextGroupView.h"
#import "SCMessageBodySoundGroupView.h"
#import "SCMessageBodyVideoGroupView.h"
#import "SCMessageBodyPhotoGroupView.h"
#import "SCMessageBodySoundItemView.h"
#import "SCMessageBodyVideoItemView.h"
#import "SCMessageBodyPhotoItemView.h"

@implementation SCMessageBodyView
{
    NSMutableArray* _viewComponents;
    CGFloat _itemWidth;
    CGFloat _itemHeight;
    SCMessageBodys* _messageBodys;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    _viewComponents = [[NSMutableArray alloc] init];
    _itemWidth = 0;
    _itemHeight = 0;
    
//    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
//    [self addGestureRecognizer:panGesture];
    
    
//    self.layer.borderWidth = 1;
//    self.layer.borderColor = [[UIColor redColor] CGColor];
//    self.backgroundColor = [UIColor greenColor];

}

-(void) dealloc
{
    FTLog(@"%@ : dealloc",NSStringFromClass([self class]));
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
}


-(void) setMessageBodys:(SCMessageBodys*) messageBodys
{
    _messageBodys = messageBodys;
    CGFloat posY = 0;
    if([_messageBodys countOfType:SCDataTypeText] > 0)
    {
        SCMessageBodyTextGroupView* _textView = [[SCMessageBodyTextGroupView alloc] initWithSettings:_settings];
        _textView.maxWidht = _maxWidht;
        [_textView setMessageBodys:_messageBodys];
        _textView.frame = CGRectMake(0, posY, _textView.viewSize.width, _textView.viewSize.height);
        _itemWidth = MAX(_itemWidth,_textView.viewSize.width);
        posY +=  _textView.viewSize.height;
        [_viewComponents addObject:_textView];
        [self addSubview:_textView];
    }
    
    if([_messageBodys countOfType:SCDataTypeAudio] > 0)
    {
        SCMessageBodySoundGroupView* _soundView = [[SCMessageBodySoundGroupView alloc] initWithSettings:_settings];
        _soundView.maxWidht = _maxWidht;
        [_soundView setMessageBodys:_messageBodys];
        if( posY > 0 )
            posY += MARGIN;
        _soundView.frame = CGRectMake(0, posY, _soundView.viewSize.width, _soundView.viewSize.height);
        _itemWidth = MAX(_itemWidth,_soundView.viewSize.width);
        posY +=_soundView.viewSize.height;
        [self addSubview:_soundView];
        [_viewComponents addObject:_soundView];

    }
    
    if([_messageBodys countOfType:SCDataTypeVideo] > 0)
    {
        SCMessageBodyVideoGroupView* _videoView = [[SCMessageBodyVideoGroupView alloc] initWithSettings:_settings];
        _videoView.maxWidht = _maxWidht;
        [_videoView setMessageBodys:_messageBodys];
        if( posY > 0 )
            posY += MARGIN;
        _videoView.frame = CGRectMake(0, posY, _videoView.viewSize.width, _videoView.viewSize.height);
        _itemWidth = MAX(_itemWidth,_videoView.viewSize.width);
        posY +=_videoView.viewSize.height;
        [self addSubview:_videoView];
        [_viewComponents addObject:_videoView];

    }
    
    if([_messageBodys countOfType:SCDataTypePhoto] > 0)
    {
        SCMessageBodyPhotoGroupView* _photoView = [[SCMessageBodyPhotoGroupView alloc] initWithSettings:_settings];
        _photoView.maxWidht = _maxWidht;
        [_photoView setMessageBodys:_messageBodys];
        if( posY > 0 )
            posY += MARGIN;
        _photoView.frame = CGRectMake(0, posY, _photoView.viewSize.width, _photoView.viewSize.height);
        _itemWidth = MAX(_itemWidth,_photoView.viewSize.width);
        posY +=_photoView.viewSize.height;
        [self addSubview:_photoView];
        [_viewComponents addObject:_photoView];
    }
    
    _itemHeight = posY;
}

-(void) setBodyAlignment:(SCMessageAlignment)bodyAlignment
{
    _bodyAlignment = bodyAlignment;
    for(NSObject* obj in _viewComponents)
    {
        if([obj isKindOfClass:[SCMessageBodyTextGroupView class]])
        {
            ((SCMessageBodyTextGroupView*)obj).bodyAlignment = _bodyAlignment;
        }
        else if([obj isKindOfClass:[SCMessageBodySoundGroupView class]])
        {
            ((SCMessageBodySoundGroupView*)obj).bodyAlignment = _bodyAlignment;
        }
        else if([obj isKindOfClass:[SCMessageBodyVideoGroupView class]])
        {
            ((SCMessageBodyVideoGroupView*)obj).bodyAlignment = _bodyAlignment;
        }
        else if([obj isKindOfClass:[SCMessageBodyPhotoGroupView class]])
        {
            ((SCMessageBodyPhotoGroupView*)obj).bodyAlignment = _bodyAlignment;
        }
        else
        {
            FTLog(@"No controlled class");
        }
    }
}


-(CGSize) viewSize
{
    return CGSizeMake(_itemWidth, _itemHeight);
}

#pragma mark UIPanGestureRecognizerDelegate
-(void) panGestureAction:(UIPanGestureRecognizer*) panGestureNotify
{
    FTLog(@"panGestureNotify");
}



@end
