//
//  SCAddressbookManager.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 24.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAddressbookManager.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SCAddressbookContact.h"
#import "SCPBManager.h"
#import "SCSettings.h"
#import "SCPUUID.h"
#import "SCmodelContacts.h"

@interface SCAddressbookManager ()

@property (strong, nonatomic) NSString * addressBookNum;
@property (strong, nonatomic) NSMutableArray* localAddressbookContacts;
@property(nonatomic,strong) NSMutableArray* addressbook;


@property(strong,nonatomic) NSArray *paths;
@property(strong,nonatomic) NSString *documentsDirectory;
@property(strong,nonatomic) NSString *path;
@property(strong,nonatomic) NSFileManager *fileManager;


@end



@implementation SCAddressbookManager


{
    SCPBManager* _pbManager;

    
}


-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if ( self )
    {
        _settings = settings;
        _pbManager = _settings.modelProfile.protoBufManager;

        
        [self _init];
    }
    return self;
}

-(void) _init


{
    self.paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.documentsDirectory = [self.paths objectAtIndex:0];
    self.path = [self.documentsDirectory stringByAppendingPathComponent:@"addressbook.plist"];
    self.fileManager = [NSFileManager defaultManager];
    
//    self.localAddressbookContacts = [NSMutableArray new];
    self.localAddressbookContacts = [self returnAddressbook];
    self.addressBookNum = [NSString  new];

}



-(void)getContacts{
    
    
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
    
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
            
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
                
                if (granted) {
                    
                    [self searchUser];
                    
                }
            });
        }
        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
          
            [self searchUser];

        }
        else {
            
           
            
        }
    if (addressBookRef) {
        
            CFRelease(addressBookRef);

        
    }
    

}


-(void)searchUser {
    
    [self parseAddressBook];
    
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    
        
    NSDictionary* temp = @{@"addressbook": self.localAddressbookContacts,@"id":transportPUUID,@"delegate":_settings.modelContacts};
    
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithDictionary:temp];
    
    [self.settings.modelContacts getAllContacts:params];
    
    
}

-(void)parseAddressBook{
    
    CFErrorRef *error = NULL;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    
    for(int i = 0; i < numberOfPeople; i++) {
        
        SCAddressbookContact* contact = [[SCAddressbookContact alloc]init];
        
        ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
        
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        
        NSString *lastName = CFBridgingRelease (ABRecordCopyValue(person, kABPersonLastNameProperty));
        
        
        
        contact.contactFirstName = firstName;
        
        contact.contactLastName = lastName;
        
        
        //get Contact foto
        
        NSData  *imgData = CFBridgingRelease(ABPersonCopyImageData(person));
        
        contact.contactFoto = [UIImage imageWithData:imgData];
        
//        if (!contact.contactFoto) {
//            
//            contact.contactFoto = [UIImage imageNamed:@"nofoto"];
//        }
        
        //get Contact email
        
        NSMutableArray *contactEmails = [NSMutableArray new];
        ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
        
        for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
            CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
            NSString *contactEmail = (__bridge NSString *)contactEmailRef;
            
            
            [contactEmails addObject:contactEmail];
            
            CFRelease(contactEmailRef);

        }
        
        contact.contactEmail = [NSMutableArray array];
        contact.contactEmail = contactEmails;
        
        CFRelease(multiEmails);
        //get Contact phoneNubers
        
        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
            
            CFStringRef currentPhoneLabel = ABMultiValueCopyLabelAtIndex(phoneNumbers, i);
            
            CFStringRef currentPhoneValue = ABMultiValueCopyValueAtIndex(phoneNumbers, i);
            
            NSMutableDictionary* phoneNumber = [NSMutableDictionary dictionary];
            
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneMobileLabel, 0) == kCFCompareEqualTo) {
                
                NSString* phone = ((__bridge NSString *)currentPhoneValue);
                phone =  [self separatedPhoneByCharacters:phone];
                [phoneNumber setObject:phone forKey:@"mobileNumber"];
                [contact.contactPhoneNumbers addObject:phone];
            }
            
            if (CFStringCompare(currentPhoneLabel, kABHomeLabel, 0) == kCFCompareEqualTo) {
                
                NSString* phone = ((__bridge NSString *)currentPhoneValue);
                phone =  [self separatedPhoneByCharacters:phone];
                [phoneNumber setObject:phone forKey:@"homeNumber"];
                [contact.contactPhoneNumbers addObject:phone];
            }
            
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneIPhoneLabel, 0) == kCFCompareEqualTo) {
                
                NSString* phone = ((__bridge NSString *)currentPhoneValue);
                phone =  [self separatedPhoneByCharacters:phone];
                [phoneNumber setObject:phone forKey:@"iphoneNumber"];
                [contact.contactPhoneNumbers addObject:phone];
            }
            
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneMainLabel, 0) == kCFCompareEqualTo) {
                
                NSString* phone = ((__bridge NSString *)currentPhoneValue);
                phone =  [self separatedPhoneByCharacters:phone];
                [phoneNumber setObject:phone forKey:@"mainNumber"];
                [contact.contactPhoneNumbers addObject:phone];
            }
            
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneHomeFAXLabel, 0) == kCFCompareEqualTo) {
                [phoneNumber setObject:(__bridge NSString *)currentPhoneValue forKey:@"phoneHomeFAX"];
            }
            
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneWorkFAXLabel, 0) == kCFCompareEqualTo) {
                [phoneNumber setObject:(__bridge NSString *)currentPhoneValue forKey:@"workFAXLabel"];
            }
            
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneOtherFAXLabel, 0) == kCFCompareEqualTo) {
                [phoneNumber setObject:(__bridge NSString *)currentPhoneValue forKey:@"otherFAXLabel"];
            }
            
            
            CFRelease(currentPhoneLabel);
            
            CFRelease(currentPhoneValue);
            
        }
        
        CFRelease(phoneNumbers);
        
        
        if (contact.contactFirstName || contact.contactLastName) {
            
            if (![self existAddressbookContactInSaving:contact]) {
                
                [self.localAddressbookContacts addObject:contact];
                
            }
            
        }
        

    }

    
    CFRelease(allPeople);
    CFRelease(addressBook);
    

    
    [self saveAddressbook:self.localAddressbookContacts];
    
}


-(BOOL)existAddressbookContactInSaving:(SCAddressbookContact*)addressbookContact{
    
    if (self.localAddressbookContacts.count>0) {
        
        for(SCAddressbookContact* contact in self.localAddressbookContacts) {
            
            if ((addressbookContact.contactPhoneNumbers && contact.contactPhoneNumbers) || (addressbookContact.contactEmail && contact.contactEmail) ) {
                
                if ([addressbookContact.contactPhoneNumbers isEqualToMyArray:contact.contactPhoneNumbers] && [addressbookContact.contactEmail isEqualToMyArray:contact.contactEmail] ) {
                    
                    return YES;
                    
                }
                
                
            }
            
            
        }

    }
    
    
    return NO;
}

-(NSString*)separatedPhoneByCharacters:(NSString*) phone{
    
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"+()"];
    
    phone = [[phone componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    phone = [[phone componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString: @""];
    
    return phone;
}


-(void)saveAddressbook:(NSMutableArray*) contacts{
    
    NSMutableArray* addessbookContacts = [NSMutableArray new];
    
    for (SCAddressbookContact* contact in contacts) {
        
        NSDictionary* contactDict = [contact toDictionary];
        
        [addessbookContacts addObject:contactDict];
    }
    
    if (![self.fileManager fileExistsAtPath: self.path])
    {
        self.path = [self.documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"addressbook.plist"] ];
    }
    
    [addessbookContacts writeToFile: self.path atomically:YES];
    
}


-(NSMutableArray*)returnAddressbook{
    
    NSMutableArray* addressbookContacts = nil;
    
    if ([self.fileManager fileExistsAtPath: self.path])
        
    {
        addressbookContacts = [[NSMutableArray alloc] initWithContentsOfFile:self.path];
        
        self.addressbook = [NSMutableArray new];
        
        
        for (NSDictionary* contactDict in addressbookContacts ) {
            
            SCAddressbookContact* contact = [[SCAddressbookContact alloc] init];
            
            [contact fromDictionary:contactDict];
            
            [self.addressbook addObject:contact];
        }
        
        
    } else
    {
        
        self.addressbook = [[NSMutableArray alloc] init];
        
    }
    
    return self.addressbook;
    
}




@end
