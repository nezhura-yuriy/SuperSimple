//
//  SCTypes.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 20.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
//

typedef enum : NSUInteger {
    SCAppStateChangeWillResignActive,
    SCAppStateChangeDidEnterBackground,
    SCAppStateChangeWillEnterForeground,
    SCAppStateChangeDidBecomeActive,
    SCAppStateChangeWillTerminate,
} SCAppStateChange;

typedef enum
{
    SCDataTypeNone,
    SCDataTypeSystemText,
    SCDataTypeMixed,
    SCDataTypeText,//
    SCDataTypeHtml,
    SCDataTypeEmoticon,
    SCDataTypePhoto,//
    SCDataTypeGif,
    SCDataTypeStiker,
    SCDataTypeVideo,//
    SCDataTypeAudio,
    SCDataTypeEmoticonLink,
    SCDataTypePhotoLink,
    SCDataTypeGifLink,
    SCDataTypeVideoLink,
    SCDataTypeAudioLink,
    
    SCDataTypeAudioFile,//
    SCDataTypeVideoFile,
    SCDataTypePhotoData,
    SCDataTypeVideoData,
    SCDataTypeAudioData,
    SCDataTypePhotoFile,//
    SCDataTypeReserv1,
    SCDataTypeReserv2
} SCDataType;


typedef enum
{
    SCMessageSecurityLevelNone,
    SCMessageSecurityLevelLock,
} SCMessageSecurityLevel;

typedef enum
{
    SCUserAuthItemTypePhone,
    SCUserAuthItemTypeEmail,
    SCUserAuthItemTypeFacebook,
} SCUserAuthItemType;


typedef enum
{
    SCModelChatsTypeLast,
    SCModelChatsTypePublic,
    SCModelChatsTypeFriends,
    SCModelChatsTypeGrouped
} SCModelChatsType;

typedef enum
{
    SCChatTypeNone = 0,
    SCChatTypePrivate = 1,
    SCChatTypeGroup = 2,
    SCChatTypePublic = 3,
    SCChatTypeSearch = 4,
    SCChatTypeReserv = 5,
} SCChatType;

typedef enum {
    
    SCUserStatusAway = 1,   //Away
    SCUserStatusOnline = 2, //Online
    SCUserStatusOffline = 3,//Offline. User can not set this status manually
    SCUserStatusBusy = 4,   //Busy
    SCUserStatusInvisible = 5,//Invisible. For other users you will have OFFLINE status, but you can receive and send messages
    
} SCUserStatus;


typedef enum {
    SCFieldTypeNone,
    SCFieldTypeLoginName, //NSString* contactLoginName
    SCFieldTypeNickName, //NSString* contactNikName
    SCFieldTypeFirstName, //NSString* contactFirstName
    SCFieldTypeLastName, //NSString* contactLastName
    SCFieldTypeSex, //NSString* contactSex
    SCFieldTypeBirthdate, //NSDate* contactBirthdate
    SCFieldTypeImageLink, //NSString* contactImageLink
    SCFieldTypeLanguage, //NSString* contactLanguage
    SCFieldTypeTimeZone, //NSString* contactTimeZone
    SCFieldTypeCountry, //NSString * country //SCAdress
    SCFieldTypeAbout, //NSString* contactAbout
    SCFieldTypeWebsite, //NSString* contactWebSite
    SCFieldTypeStreet, //NSString * street //SCAdress
    SCFieldTypeCyty, //NSString * city //SCAdress
    SCFieldTypePostCode, //NSString * postCode //SCAdress
    SCFieldTypeAppartament, //NSString * appartament //SCAdress
    SCFieldTypeHose, //NSString * house 
    SCFieldTypeImageid,
    SCFieldTypeAuths,
    SCFieldTypePhone,
    SCFieldTypeEmail,
} SCFieldType;


typedef enum : NSUInteger
{
    SCMediaFileTypeUnknown,
    SCMediaFileTypePhoto,
    SCMediaFileTypeAudio,
    SCMediaFileTypeVideo,
} SCMediaFileType;

typedef enum : NSUInteger {
    SCNetFileStatusNew,
    SCNetFileStatusWait,
    SCNetFileStatusStart,
    SCNetFileStatusProgress,
    SCNetFileStatusError,
    SCNetFileStatusCompleet,
    SCNetFileStatusTryRepeet,
} SCNetFileStatus;

typedef enum : NSUInteger {
    SCNetFileComletionTypeStatus,
    SCNetFileComletionTypeProgress,
    SCNetFileComletionTypeError,
    SCNetFileComletionTypeCompleet,
} SCNetFileComletionType;

typedef enum
{
    SCMessageAlignmentNone = 0,
    SCMessageAlignmentLeft = 1,
    SCMessageAlignmentRight = 2,
    SCMessageAlignmentCenter = 3
} SCMessageAlignment;

typedef enum : NSUInteger {
    SCPingStatusStart,
    SCPingStatusStop,
    SCPingStatusCheck,
} SCPingStatus;

