//
//  SCPUUID.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 05.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCPUUID : NSObject

@property(nonatomic,strong) NSUUID *nsuuid;
@property(nonatomic,assign) SInt64 leastSignificantBits;
@property(nonatomic,assign) SInt64 mostSignificantBits;



- (id)initFromString:(NSString*) scpuuidStr;
- (id) initWithLeastSignificantBits:(SInt64) leastSignificantBits MostSignificantBits:(SInt64) mostSignificantBits;
- (NSString*) scpuuidToString;
- (id) initWithRandom;
-(BOOL) isEqual:(SCPUUID*)object;
-(BOOL) isEmpty;
@end
