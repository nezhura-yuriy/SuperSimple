//
//  SCChatListDetailView.h
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 03.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCModelsDelegates.h"

@class SCSettings,SCMessageBodys;

@interface SCChatListDetailView : UIView <SCNetFileDelegate>

-(void) setDescription:(NSString*) chatDescription withSettings:(SCSettings*) settings;
-(void) setMessageBodys:(SCMessageBodys*) messageBodys withSettings:(SCSettings*) settings;
-(void) clear;

+(CGFloat) calcHeightDescription:(NSString*) chatDescription forWidth:(CGFloat) width;
+(CGFloat) calcHeight:(SCMessageBodys*) bodyArray forWidth:(CGFloat) width;

@end
