//
//  SCCoutrySection.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 21.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCCoutrySection : NSObject

@property(strong, nonatomic) NSString* sectionName;

@property(strong, nonatomic) NSMutableArray* itemsArray;

@end
