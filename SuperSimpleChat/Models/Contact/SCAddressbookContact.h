//
//  SCAddressbookContact.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 26.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCAddressbookContact : NSObject

@property (strong, nonatomic) NSString* contactFirstName;
@property (strong, nonatomic) NSString* contactLastName;
@property (strong, nonatomic) NSString* fullName;
@property (strong, nonatomic) UIImage* contactFoto;
@property (strong, nonatomic) NSMutableArray* contactPhoneNumbers;
@property (strong, nonatomic) NSMutableArray* contactEmail;
@property (assign, nonatomic) BOOL checked;

-(void) fromDictionary:(NSDictionary*) dictionary;
-(NSDictionary*) toDictionary;


@end
