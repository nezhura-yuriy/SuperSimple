//
//  SCMessageBobys.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 07.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModelsDelegates.h"
@class SCSettings,SCMessageBodyItem;


@interface SCMessageBodys : NSObject <SCMessageBodyItemDelegate>

@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,weak) id<SCMessageBodysDelegate> delegate;

-(id) initWithSettings:(SCSettings*) settings;
-(id) initFromArrayMessageBody:(NSArray*) messageBodysArray;
-(void) clear;
-(void) addItem:(SCMessageBodyItem*) item;
-(void) changeItem:(SCMessageBodyItem*) item;
-(void) removeItem:(SCMessageBodyItem*) item;
-(void) addItemsFromArray:(NSArray*) items;
-(SCMessageBodyItem*) editAtIndex:(NSUInteger) bodyIdx;

-(NSUInteger) countOfType:(SCDataType) type;
-(NSMutableArray*) items;
-(NSMutableArray*) itemsByType:(SCDataType) type;

-(void) sendMessageBodysWithCompletionBlock:(void (^)(SCNetFileComletionType type))completionBlock;
@end
