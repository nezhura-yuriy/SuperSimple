//
//  SCModelChats.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCModelChats.h"
#import "SCChatItem.h"
#import "SCContactItem.h"
#import "SCSettings.h"
#import "SCPBManager.h"
#import "SCMessageItem.h"
#import "SCChatItem.h"

@implementation SCModelChats
{
    NSMutableArray* _delegates;
    NSMutableArray* _items;
    
    NSInteger _counterAllMessagesBeforeBackgraund;
    NSTimer* _delaySaveTimer;
}


-(id) init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }
    return self;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if ( self )
    {
        _settings = settings;
        _protoBufManager = _settings.modelProfile.protoBufManager;
        [self _init];
        
//        [self loadCached];
    }
    return self;
}

-(void) _init
{
    _items = [[NSMutableArray alloc] init];
    _delegates = [[NSMutableArray alloc] init];
}

-(void) dealloc
{
    FTLog(@"%@ dealloc",NSStringFromClass([self class]));
}


-(void) addDelegate:(id<SCModelChatsDelegate>) delegate
{
    if(![_delegates containsObject:delegate])
    {
        [_delegates addObject:delegate];
    }
}

-(void) delDelegate:(id<SCModelChatsDelegate>) delegate
{
    if([_delegates containsObject:delegate])
    {
        [_delegates removeObject:delegate];
    }
}

-(NSArray*) items:(SCChatType) scChatType
{
    NSMutableArray* outAr = [[NSMutableArray alloc] init];
    for(SCChatItem* item in _items)
    {
        if(!item.isDeleted)
        {
            if(item.chatType == scChatType)
            {
                [outAr addObject:item];
            }
            else if(scChatType == SCChatTypePublic && item.chatType == 0)
            {
                [outAr addObject:item];
            }
        }
    }
    return outAr;
}

-(SCChatItem*) getPrivateForUser:(SCContactItem*) user
{
    for(SCChatItem* chatItem in _items)
    {
        if(chatItem.chatType ==SCChatTypePrivate)
        {
            for(SCContactItem* userItem in chatItem.usersInChat)
            {
                if([userItem.userPUUID isEqual:user.userPUUID])
                    return chatItem;
            }
        }
    }
    return nil;
}

-(SCChatItem*) searchChatWithSCPUUID:(SCPUUID*) scpuuid
{
    for(SCChatItem* item in _items)
    {
        if([item.chatPuuid isEqual:scpuuid])
        {
            return item;
        }
    }
    return nil;
}

-(NSUInteger) countNotViewedForType:(SCChatType) scChatType
{
    NSUInteger countMessages = 0;
    for(SCChatItem* item in _items)
    {
        if(item.chatType == scChatType)
        {
            countMessages += item.notViewedMessagesCount;
        }
    }
    return countMessages;
}

-(void) calcAllMessagesBeforeBackgraund
{
    _counterAllMessagesBeforeBackgraund = 0;
    _counterAllMessagesBeforeBackgraund += [self countNotViewedForType:SCChatTypePrivate];
    _counterAllMessagesBeforeBackgraund += [self countNotViewedForType:SCChatTypeGroup];
    _counterAllMessagesBeforeBackgraund += [self countNotViewedForType:SCChatTypePublic];

}

#pragma mark - CACHE
-(void) clearBeforeLogout
{
    [self saveCache];
    [_items removeAllObjects];
}

-(void) loadCached
{
    NSString* path = [_settings.cacheChatPath stringByAppendingPathComponent:@"ChatRoomList"];

    NSArray *tmpArray = [NSMutableArray arrayWithContentsOfFile:path];
    for(NSDictionary *dict in tmpArray)
    {
        SCChatItem* chatItem = [[SCChatItem alloc] initWithSettings:_settings];
        [chatItem fromDictionary:dict];
        [_items addObject:chatItem];
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [chatItem loadCachedMessages];
//        });
    }
}
-(void) saveCache
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    for(SCChatItem* chatItem in _items)
    {
        NSDictionary* dictItem = [chatItem toDictionary];
        [tmpArray addObject:dictItem];
    }
    NSString* path = [_settings.cacheChatPath stringByAppendingPathComponent:@"ChatRoomList"];
    BOOL sucsess = [tmpArray writeToFile:path atomically:YES];
    if(!sucsess)
    {
        FTLog(@"Error saveCache");
    }
    else
        FTLog(@"SCModelChats chatRoom list to cache");
}

-(void) clearCached
{
    [_items removeAllObjects];
    
    NSString* path = [_settings.cacheChatPath stringByAppendingPathComponent:@"ChatRoomList"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSError* err = nil;
        BOOL res;
        
        res = [[NSFileManager defaultManager] removeItemAtPath:path error:&err];
        if (!res && err)
        {
            FTLog(@"clearCached oops: %@", err);
        }
    }
}

-(void) checkCachedItems
{
    for(SCChatItem* item in _items)
    {
        NSTimeInterval intervaNeedUpdate = 10 * 60;
        
        BOOL isRequired = NO;
        if(!item.itemUpdatedTime)
            isRequired = YES;
        else
        {
            NSComparisonResult result = [[item.itemUpdatedTime dateByAddingTimeInterval:intervaNeedUpdate] compare:[NSDate date]];
            if(result == NSOrderedDescending)
                isRequired = NO;
            else
                isRequired = YES;
        }
        
        if(isRequired)
        {
            SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
            NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        self, @"delegate",
                                        transportPUUID, @"UUID",
                                        [NSNumber numberWithInteger:SCPBManagerParceTypeGetChatRoomByIdRequest], @"type",
                                        item,@"SCChatItem",
                                        nil];
            
            [_protoBufManager getChatRoomByIdRequest:dictionary];
        }
    }
}

-(void)timerSaveCache:(NSTimer *)timer
{
    if(_delaySaveTimer)
        [_delaySaveTimer invalidate];
    _delaySaveTimer = nil;
    
    [self saveCache];
}

-(void) timerSetInterval
{
    FTLog(@"timerSetInterval");
    if(_delaySaveTimer)
        [_delaySaveTimer invalidate];
    _delaySaveTimer = nil;
    
    _delaySaveTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerSaveCache:) userInfo:nil repeats:NO];
}


#pragma mark - SERVER
-(void) serverLoad
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                self, @"delegate",
                                transportPUUID, @"UUID",
                                [NSNumber numberWithInteger:SCPBManagerParceTypeGetAllRoomsForUser], @"type",
                                nil];

    [_protoBufManager getAllRoomsForUserRequest:dictionary];
}

-(void) addNewChatRoom:(SCChatItem *) newChatItem
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       self, @"delegate",
                                       transportPUUID, @"UUID",
                                       [NSNumber numberWithInteger:SCPBManagerParceTypeCreateChat], @"type",
                                       newChatItem,@"SCChatItem",
                                       nil];
    [_protoBufManager createChatRoomRequest:params];
}

-(void) changeChatRoom:(SCChatItem *) corrChatItem//:(id) params//
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       self, @"delegate",
                                       transportPUUID, @"UUID",
                                       [NSNumber numberWithInteger:SCPBManagerParceTypeChangeChat], @"type",
                                       corrChatItem,@"SCChatItem",
                                       nil];
    [_protoBufManager changeChatRoomRequest:params];
}

-(void) removeChatRoom:(SCChatItem*) chatItem
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   transportPUUID, @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeRemoveChat], @"type",
                                   chatItem,@"SCChatItem",
                                   nil];
    [_protoBufManager removeChatRoomRequest:params];
}

-(void) activateChatRoom:(SCChatItem*) chatItem
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   transportPUUID, @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeActivateChat], @"type",
                                   chatItem,@"SCChatItem",
                                   nil];
    [_protoBufManager chatRoomActivate:params];
}

-(void) searchPublicChatRooms:(NSMutableDictionary*) params
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    [params setObject:self forKey:@"delegate"];
    [params setObject:transportPUUID forKey:@"UUID"];
    
    [_protoBufManager searchPublicChatRooms:params];
}


#pragma mark SCPBManagerDelegate <NSObject>

-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"%@ : parsedType",NSStringFromClass([self class]));
    switch (type)
    {
//MARK: Chats first level ===========================================
        case SCPBManagerParceTypeCreateChat:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                [_items addObject:data];
                [(SCChatItem*)data updateItemContent];

                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                    {
                        [delegate parsedType:SCPBManagerParceTypeCreateChat data:data];
                    }
                }
            }
            
// old
//            for(id<SCModelChatsDelegate> delegate in _delegates)
//            {
//                if([delegate respondsToSelector:@selector(didAddItemToModelChatsList:)])
//                {
//                    [delegate didAddItemToModelChatsList:data];
//                }
//            }
//
        }
            break;
            
        case SCPBManagerParceTypeGetChatRoomByIdRequest:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                FTLog(@"%@",data);
                SCChatItem* chatItem = (SCChatItem*) data;
                if(chatItem.isDeleted)
                {
                    [_items removeObject:chatItem];
                    for(id<SCPBManagerDelegate> delegate in _delegates)
                    {
                        if([delegate respondsToSelector:@selector(parsedType:data:)])
                        {
                            [delegate parsedType:SCPBManagerParceTypeRemoveChat data:chatItem];
                        }
                    }
                }
                else
                {
                    [chatItem updateItemContent];
                    for(id<SCPBManagerDelegate> delegate in _delegates)
                    {
                        if([delegate respondsToSelector:@selector(parsedType:data:)])
                        {
                            [delegate parsedType:type data:data];
                        }
                    }

                }
                [self timerSetInterval];
            }
            else if([data isKindOfClass:[NSDictionary class]])
            {
                SCChatItem* item = [data objectForKey:@"SCChatItem"];
                [_items removeObject:item];
                [self timerSetInterval];
                for(id<SCModelChatsDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(didRemoveItemFromModemChatsList:)])
                    {
                        [delegate didRemoveItemFromModemChatsList:item];
                    }
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeChangeRoomEvent:
        case SCPBManagerParceTypeChangeChat:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = data;
                if([chatItem respondsToSelector:@selector(parsedType:data:)])
                {
                    [chatItem parsedType:type data:data];
                }
//                FTLog(@"chatItem %@",chatItem);
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                    {
                        [delegate parsedType:type data:data];
                    }
                }

            }
//            for(id<SCModelChatsDelegate> delegate in _delegates)
//            {
//                if([delegate respondsToSelector:@selector(didChangeItemInModelChatsList:)])
//                {
//                    [delegate didChangeItemInModelChatsList:data];
//                }
//            }
        }
            break;
            
        case SCPBManagerParceTypeRemoveChat:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = data;
//                FTLog(@"chatItem %@",chatItem);
                [_items removeObject:chatItem];
            }
            [self timerSetInterval];
            
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

        }
            break;
            
        case SCPBManagerParceTypeActivateChat:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = data;
                if([self searchChatWithSCPUUID:chatItem.chatPuuid] != nil)
                {
                    FTLog(@"Хоп-п-п-па");
                }

                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                    {
                        [delegate parsedType:type data:data];
                    }
                }

//                for(id<SCModelChatsDelegate> delegate in _delegates)
//                {
//                    if([delegate respondsToSelector:@selector(didAddItemToModelChatsList:)])
//                    {
//                        [delegate didAddItemToModelChatsList:chatItem];
//                    }
//                }
            }


        }
            break;
            
        case SCPBManagerParceTypeGetAllRoomsForUser:
        {
            if([data isKindOfClass:[NSArray class]])
            {
                for(SCChatItem* chatItem in data)
                {
                    if([self searchChatWithSCPUUID:chatItem.chatPuuid] == nil)
                    {
                        chatItem.settings = _settings;
                        [_items addObject:chatItem];
                    }
                    else
                    {
                        FTLog(@"");
                    }
                    [chatItem updateItemContent];
                    
                }
                
                [self timerSetInterval];
                
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                    {
                        [delegate parsedType:type data:data];
                    }
                }
                
                [self checkCachedItems];
                
            }
        }
            break;
            
        case SCPBManagerParceTypeJoinUserToRoomEvent:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = (SCChatItem*) data;
                if([self searchChatWithSCPUUID:chatItem.chatPuuid] == nil)
                {
                    chatItem.settings = _settings;
                    [_items addObject:chatItem];
                }
                
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                    {
                        [delegate parsedType:type data:data];
                    }
                }
//                for(id<SCModelChatsDelegate> delegate in _delegates)
//                {
//                    if([delegate respondsToSelector:@selector(didAddItemToModelChatsList:)])
//                    {
//                        [delegate didAddItemToModelChatsList:_items];
//                    }
//                }

            }
        }
            break;
            
        case SCPBManagerParceTypeCallTypeRemoveRoomEvent:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

            break;

        case SCPBManagerParceTypeCallTypeLeaveUserFromRoomEvent:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

            break;

        case SCPBManagerParceTypeGetAllUsersForRoom:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

            break;
            
        case SCPBManagerParceTypeGetChatRoomHistory:
        case SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange:
        {
            FTLog(@"SCPBManagerParceTypeGetChatRoomHistory");
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            
        }
            break;
            
        case SCPBManagerParceTypeSearchPublicChatRooms:
        {
//            FTLog(@"%@",data);
            
//            for(id<SCModelChatsDelegate> delegate in _delegates)
//            {
//                if([delegate respondsToSelector:@selector(didSearchItemInModelChatsList:)])
//                {
//                    [delegate didSearchItemInModelChatsList:data];
//                }
//            }
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

        }
            break;
            

        case SCPBManagerParceTypeChatRoomGetById:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

            break;
        case SCPBManagerParceTypeSearchCreateChat:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            break;
            
        case SCPBManagerParceTypeGetAllRooms:
            FTLog(@"%@",data);\
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

            break;
        case SCPBManagerParceTypeLeaveUserFromRoom:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                FTLog(@"%@",data);
                SCChatItem* chatItem = (SCChatItem*) data;
                if(chatItem.isDeleted)
                {
                    [_items removeObject:chatItem];
                    for(id<SCPBManagerDelegate> delegate in _delegates)
                    {
                        if([delegate respondsToSelector:@selector(parsedType:data:)])
                        {
                            [delegate parsedType:SCPBManagerParceTypeRemoveChat data:chatItem];
                        }
                    }
                }
                else
                {
                    [chatItem updateItemContent];
                    for(id<SCPBManagerDelegate> delegate in _delegates)
                    {
                        if([delegate respondsToSelector:@selector(parsedType:data:)])
                        {
                            [delegate parsedType:type data:data];
                        }
                    }
                    
                }
                [self timerSetInterval];
            }
        }
            break;
            
        case SCPBManagerParceTypeJoinUserToRoom:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            break;
            
        case SCPBManagerParceTypeGetHistory:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            break;
            
        case SCPBManagerParceTypeGetLastReadMessageDate:
            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            break;
            
        case SCPBManagerParceTypeSetLastReadMessageDate:
//            FTLog(@"%@",data);
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

            break;
            
//MARK: Messages second level =====================================
        case SCPBManagerParceTypeSendMessage:
        {
            FTLog(@"%@",data);
        }
            break;
            
        case SCPBManagerParceTypeMessage:
        {
//            FTLog(@"%@",data);
            if([data isKindOfClass:[SCMessageItem class]])
            {
                SCMessageItem* messageItem = (SCMessageItem*) data;
                for(SCChatItem* chatItem in _items)
                {
                    if([chatItem.chatPuuid isEqual:messageItem.chat.chatPuuid])
                    {
                        [chatItem parsedType:SCPBManagerParceTypeMessage data:messageItem];
                        break;
                    }
                }
                
            }
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }


            if([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
            {
                NSInteger _counterAllMessages = 0;
                _counterAllMessages += [self countNotViewedForType:SCChatTypePrivate];
                _counterAllMessages += [self countNotViewedForType:SCChatTypeGroup];
                _counterAllMessages += [self countNotViewedForType:SCChatTypePublic];

                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:_counterAllMessages - _counterAllMessagesBeforeBackgraund];
            }
        }
            break;
            
        case SCPBManagerParceTypeUpdateMessage:
            if([data isKindOfClass:[SCMessageItem class]])
            {
                SCMessageItem* messageItem = (SCMessageItem*) data;
                for(SCChatItem* chatItem in _items)
                {
                    if([chatItem.chatPuuid isEqual:messageItem.chat.chatPuuid])
                    {
                        [chatItem parsedType:SCPBManagerParceTypeMessage data:messageItem];
                        break;
                    }
                }
                
            }
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }

            break;
            
        case SCPBManagerParceTypeRemoveMessage:
            if([data isKindOfClass:[SCMessageItem class]])
            {
                SCMessageItem* messageItem = (SCMessageItem*) data;
                for(SCChatItem* chatItem in _items)
                {
                    if([chatItem.chatPuuid isEqual:messageItem.chat.chatPuuid])
                    {
                        [chatItem parsedType:SCPBManagerParceTypeMessage data:messageItem];
                        break;
                    }
                }
                
            }
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            break;
            
        case SCPBManagerParceTypeRefreshedMessageEvent:
            if([data isKindOfClass:[SCMessageItem class]])
            {
                SCMessageItem* messageItem = (SCMessageItem*) data;
                for(SCChatItem* chatItem in _items)
                {
                    if([chatItem.chatPuuid isEqual:messageItem.chat.chatPuuid])
                    {
                        [chatItem parsedType:SCPBManagerParceTypeMessage data:messageItem];
                        break;
                    }
                }
                
            }
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            break;
            
/* ======================================================================================== */
            
        default:
        {
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            FTLog(@"%@",data);
        }
            break;
    }
}


@end
