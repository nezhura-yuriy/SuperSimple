//
//  SCChatUserMenu.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 23.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatUserMenu.h"
#import "SCSettings.h"
#import "SCmodelContacts.h"
#import "SCContactItemSmall.h"
#import "SCModelChats.h"

#import "SCChatUserCell.h"


#define CHAT_USERS_TABLEVIEW_CELL @"SCChatListCell"
#define CHAT_USERS_TABLEVIEW_CELL_HEIGHT 56

@implementation SCChatUserMenu
{
    void (^afterSelect) (SCPopChatMenuViewItem selectItem);
    NSArray* _rowsArray;
    BOOL inShowAdd;
    NSMutableArray* userInRoomT;
}

-(id) init
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}

-(id) initWithCompletionblock:(void (^)(SCPopChatMenuViewItem selectItem))completionBlock
{
    self = [super init];
    if(self)
    {
        afterSelect = [completionBlock copy];
        [self _init];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self _init];
}


-(void) _init
{
    [super _init];
    self.clipsToBounds = YES;
    self.layer.borderWidth = 1;
    _inAddMode = NO;
//    self.layer.borderColor = [[UIColor colorWithRed:164.0/255.0 green:205.0/255.0 blue:255.0/255.0 alpha:0.85] CGColor];
//    self.backgroundColor = [UIColor colorWithRed:0.7176 green:0.8235 blue:0.8784 alpha:0.85];
    
    [_tableView registerNib:[UINib nibWithNibName:@"SCChatUserCell" bundle:nil] forCellReuseIdentifier:CHAT_USERS_TABLEVIEW_CELL];
}

-(void) dealloc
{
    
}

-(void) setAfterSelect:(void(^)(SCPopChatMenuViewItem selectItem)) completionBlock
{
    afterSelect = [completionBlock copy];
}

-(void) setInAddMode:(BOOL)inAddMode
{
    _inAddMode = inAddMode;
    inShowAdd = _inAddMode;
}


-(void) layoutSubviews
{
    [super layoutSubviews];
}



+(CGSize) sizeView
{
    return CGSizeMake(320, 400);
}

-(void) setUserInRoom:(NSMutableArray *)userInRoom
{
    _userInRoom = userInRoom;
    userInRoomT = [_userInRoom mutableCopy];
    if(userInRoomT.count < 1)
    {
        _rowsArray = [_settings.modelContacts items:nil];
        _btnAdd.hidden = YES;
    }
    else
    {
        _rowsArray = userInRoom;
        _btnAdd.hidden = NO;
    }
    //[_settings.modelContacts items:nil] ;//
//    NSMutableArray* allCont = [_settings.modelContacts items:nil];
}

- (IBAction)btnDoneAction:(UIButton *)sender forEvent:(UIEvent *)event
{
    if(_inAddMode)
    {
        [_userInRoom removeAllObjects];
        [_userInRoom addObjectsFromArray:userInRoomT];
    }
    [_delegate selectItem:SCPopChatMenuViewItemUserDone];
}

- (IBAction)btnAddAction:(id)sender
{
    inShowAdd = YES;
    _btnAdd.hidden = YES;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[UITableViewCell alloc] init];
}
@end
