//
//  SCModelList.h
//  SmartChat
//
//  Created by Yury Radchenko on 1/14/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
/**
 Класс описывает модель данных Листа
 https://gitlab.com/Bischak/smartchat-backend-api/blob/staging/com.smartchat.models.List#list
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SCListItem.h"
#import "SCPUUID.h"
#import "SCModelsDelegates.h"

@class SCList;

@protocol SCModelListProtocol <NSObject>
@end

@interface SCList : NSObject <SCPBManagerDelegate>

@property (strong, nonatomic) SCPUUID *puuid;
@property (strong, nonatomic) NSString *title; //name
@property (strong, nonatomic) NSString* review; //description
@property (strong, nonatomic) NSString *imageListSource;
@property (strong, nonatomic) SCPUUID *ownerPUUID;
@property (strong, nonatomic) NSDate *changeTime;
@property (nonatomic) SCListType type;
@property (nonatomic) SCContentType contentType;
@property (strong, nonatomic) NSMutableArray *items;

- (instancetype) init NS_DESIGNATED_INITIALIZER;
- (instancetype) initAsFavorite;

- (instancetype) initFromDictionary:(NSDictionary*) dictionary;
- (NSDictionary *) toDictionary;

- (void) getListItemsFromServer;
- (void) addItem: (SCListItem *) item;
- (void) updateItem: (SCListItem *) item;
- (void) removeItem: (SCListItem *) item;
- (void) setItemsOrder: (NSArray *) itemsPUUID;

- (BOOL) isEqualList:(SCList *) list;

@end
