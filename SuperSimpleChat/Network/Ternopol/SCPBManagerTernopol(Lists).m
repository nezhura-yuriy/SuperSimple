//
//  SCPBManagerTernopol+Lists.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol(Lists).h"
#import "SCPBManagerTernopol(Converts).h"

#import "SmartChat.pb.h"
#import "SCPUUID.h"

#import "SCList.h"
#import "SCContactItem.h"

static const NSString *kKeyPUUID = @"keyPUUID";
static const NSString *kKeyTrasportPUUID = @"keyTransportPUUID";
static const NSString *kDelegate = @"delegate";
static const NSString *kKeySuccess = @"success";

@implementation SCPBManagerTernopol (Lists)

/*
 message List {
 required PUUID id = 1; /// List id
 required string name = 2; /// List name
 optional string description = 3; /// List description
 optional PUUID file_image_id = 4; /// List image
 optional PUUID owner_id = 5; /// Id of list owner
 optional sint64 change_time = 6; /// Last time of changing list (updating list info / adding new list items to list)
 optional ListType list_type = 7; /// Type of list
 optional ContentType content_type = 8;
 }
 
 enum ListType {
 PUBLIC = 1; /// Everyone can find list with this type and get this list to own lists. For not owners - list only for read, if owner change list - these changes will appear in every user, who get this list.
 PRIVATE = 2; /// Default list type after creating new list. User can share this list to other user. For receiver, this list has type SHARED. If owner change list - these changes will appear in every user, who get this list.
 PURCHASED = 3; /// Customer can not change or share list with type PURCHASED.
 NOT_INITIALIZED = 4; /// Ignore this type.
 DEFAULT = 5; /// Type for default lists. Not supported yet.
 SHARED = 6; /// Type for shared lists. Receiver can not change this list.
 }
 
 enum ContentType {
 TEXT_CONTENT = 1;
 FILE_CONTENT = 2;
 EMOTICON_CONTENT = 3;
 }
 
 */

//MARK: ====== Lists (Request & Parse) ======


/* =======================================================================================
 MARK: LIST_GET
 message GetListRequest {
 required PUUID list_id = 1;
 }
 */
-(void) getListRequest: (id) dictionary
{
    if ([dictionary isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *keyPUUID = [dictionary objectForKey:kKeyPUUID];
        id<SCPBManagerDelegate> delegate = [dictionary objectForKey:kDelegate];
        SCList *list = [dictionary objectForKey:@"value"];
        
        [_delegateDictionary setObject:delegate forKey:[keyPUUID scpuuidToString]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeListGet];
        [_requestBuilder setId:[self puuidBuilder:keyPUUID]];
        
        GetListRequestBuilder *_getListRequestBuilder = [[GetListRequestBuilder alloc] init];
        [_getListRequestBuilder setListId:[self puuidBuilder:list.puuid]];
        
        [_requestBuilder setRequest:[_getListRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    } else {
        FTLog(@"Dictionary is not dictionary");
    }
}

/*
 message GetListResponse {
 optional List list = 1;
 }
 */
-(void) parseGetList:(id)data
{
    Response *response = data;
    if(response.status.success)
    {
        SCPUUID *keyPUUID = [self scpuuidBuilder:response.id];
        NSString *keyPUUIDAsString = [keyPUUID scpuuidToString];
        
        if ([[_delegateDictionary allKeys] containsObject:keyPUUIDAsString]) {
            id<SCPBManagerDelegate>delegate = [_delegateDictionary objectForKey:keyPUUIDAsString];
            [_delegateDictionary removeObjectForKey:keyPUUIDAsString];
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)]) {
                
                List *serverList = [GetListResponse parseFromData:response.response].list;
                SCList *_list = [[SCList alloc] init];
                
                [self equatePropertiesServerList:serverList forList:_list];
                [delegate parsedType:SCPBManagerParceTypeGetList data:_list];
            }
        }
    }
    else
    {
        FTLog(@"Error GetList response.status.success");
    }
}

//MARK: ====== Items List (Request & Parse) ======
/*
 message ListItem {
 required PUUID id = 1;
 required PUUID list_id = 2;
 required PUUID user_id = 3;
 required string text = 4;
 optional int32 sortby = 5; // **DEPRECATED**
 }
 */

/* =======================================================================================
 MARK: LIST_ITEM(S)_GET
 
 message GetListItemsRequest {
 required PUUID list_id = 1;
 }
 */
-(void) getListItemsRequest:(id) dictionary
{
    if ([dictionary isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *keyPUUID = [dictionary objectForKey:kKeyPUUID];
        id<SCPBManagerDelegate> delegate = [dictionary objectForKey:kDelegate];
        SCList *list= [dictionary objectForKey:@"value"];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeListItemsGet];
        [_requestBuilder setId:[self puuidBuilder:keyPUUID]];
        
        [_delegateDictionary setObject:delegate forKey:[keyPUUID scpuuidToString]];
        
        GetListItemsRequestBuilder* _getlistItemsRequestBuilder = [[GetListItemsRequestBuilder alloc] init];
        [_getlistItemsRequestBuilder setListId:[self puuidBuilder:list.puuid]];
        
        [_requestBuilder setRequest:[_getlistItemsRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
    else
    {
        FTLog(@"Dictionary is not dictionary");
    }
}

/*
 message GetListItemsResponse {
 repeated ListItem items = 1;
 }
 */

- (void) parseGetListItems:(id)data
{
    Response *response = data;
    if(response.status.success)
    {
        SCPUUID *keyPUUID = [self scpuuidBuilder:response.id];
        NSString *keyPUUIDAsString = [keyPUUID scpuuidToString];
        
        if ([[_delegateDictionary allKeys] containsObject:keyPUUIDAsString])
         {
            id<SCPBManagerDelegate>delegate = [_delegateDictionary objectForKey:keyPUUIDAsString];
            [_delegateDictionary removeObjectForKey:keyPUUIDAsString];
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                NSArray *serverArrayItems = [GetListItemsResponse parseFromData:response.response].items;
                
                NSMutableArray *_itemsArray = [[NSMutableArray alloc] init];
                
                if (serverArrayItems.count > 0) {
                    for (ListItem *serverListItem in serverArrayItems)
                    {
                        SCListItem *_item = [[SCListItem alloc] init];
                        [self equatePropertiesServerItem:serverListItem forItem:_item];
                        
                        [_itemsArray addObject:_item]; //because [_addListItemRequestBuilder setMakeFirstInList:YES]; in AddListItemRequest
                    }
                }
                
                NSDictionary *dictionary = @{kKeyPUUID: keyPUUID, @"items": _itemsArray};
                [delegate parsedType:SCPBManagerParceTypeGetListItems data:dictionary];
            }
        }
    }
    else
    {
        FTLog(@"Error getListItemsRequest esponse.status.success");
    }
}

//MARK: LIST_ITEM_ADD
/* =======================================================================================
 message AddListItemRequest {
 required PUUID list_id = 1; /// List id
 optional string text = 2; /// Text part of list item
 optional int32 sortby = 3 [default = 0]; /// **DEPRECATED** SortBy flag of list item
 optional PUUID file_id = 4;
 required ContentType type = 5;
 optional bool makeFirstInList = 6;
 }
 */

-(void) addItemListRequest: (id) dictionary
{
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        
        SCPUUID *transportPUUID = dictionary[kKeyTrasportPUUID];
        
        [_delegateDictionary setObject:dictionary forKey:[transportPUUID scpuuidToString]];
        
        SCList *list = dictionary[@"list"];
        SCListItem *item = dictionary[@"item"];
        
        //*** Set _requestBuilder Properties
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc] init];
        [_requestBuilder setType:CallTypeListItemAdd];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        AddListItemRequestBuilder *_addListItemRequestBuilder = [[AddListItemRequestBuilder alloc] init];
        [_addListItemRequestBuilder setListId:[self puuidBuilder:list.puuid]];
        
        if (item.text.length > 0) {
            [_addListItemRequestBuilder setText:item.text];
        }
        
        /* **DEPRECATED**
        if (item.sortBy != 0) {
            [_addListItemRequestBuilder setSortby:(SInt32) item.sortBy];
        }
         */
        
        if (item.contentSource.length > 0) {
            SCPUUID *contentSourceSCPUUID = [[SCPUUID alloc] initFromString:item.contentSource];
            [_addListItemRequestBuilder setFileId:[self puuidBuilder:contentSourceSCPUUID]];
        }
        
        [_addListItemRequestBuilder setMakeFirstInList:YES];

        [_addListItemRequestBuilder setType:[self convertLocalContentType:item.type]];
        
        //****
        
        [_requestBuilder setRequest:[_addListItemRequestBuilder build].data];
        
        Request *query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    } else {
        FTLog(@"Dictionary is not dictionary");
    }
}

/*
 message AddListItemResponse {
 required PUUID id = 1;
 }
 */
- (void) parseAddListItem:(id)data
{
    Response *response = data;

    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDAsString = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDAsString]) {
        
        NSDictionary *savedDict = _delegateDictionary[transportPUUIDAsString];
        
        id<SCPBManagerDelegate>delegate = savedDict[kDelegate];
        
        NSMutableDictionary *transportDic = [[NSMutableDictionary alloc] init];
        transportDic[kKeySuccess] = [NSNumber numberWithBool:response.status.success];
        transportDic[@"list"] = savedDict[@"list"];
        transportDic[@"item"] = savedDict[@"item"];
        
        [_delegateDictionary removeObjectForKey:transportPUUIDAsString];
        
        if (response.status.success) {
            NSData *data = response.response;
            PUUID *serverItemPUUID = [AddListItemResponse parseFromData:data].id;
            transportDic[@"newItemPUUID"] = [self scpuuidBuilder:serverItemPUUID];
        }
        
        if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)]) {
            [delegate parsedType:SCPBManagerParceTypeAddListItem data:transportDic];
        }
    }
    else
    {
        FTLog(@"Error delegateDictionary");
    }
}

/* =======================================================================================
 MARK: LIST_ITEM_UPDATE
 message UpdateListItemRequest {
 required PUUID list_id = 1; /// List id
 required PUUID id = 2; /// List item id
 required string text = 3; /// Text part of list item, which will be updated
 optional int32 sortby = 4 [default = 0]; /// SortBy flag of list item // **DEPRECATED**
 required ListItemType type = 5;
 optional PUUID file_id = 6;
 }
 */
-(void) updateListItemRequest:(id) dictionary
{
    if ([dictionary isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = dictionary[kKeyTrasportPUUID];
        
        [_delegateDictionary setObject:dictionary forKey:[transportPUUID scpuuidToString]];
        
        SCList *list = dictionary[@"list"];
        SCListItem *item = dictionary[@"item"];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeListItemUpdate];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        UpdateListItemRequestBuilder* _updatelistItemRequestBuilder = [[UpdateListItemRequestBuilder alloc] init];
        [_updatelistItemRequestBuilder setListId:[self puuidBuilder:list.puuid]];
        [_updatelistItemRequestBuilder setId:[self puuidBuilder:item.puuid]];
        [_updatelistItemRequestBuilder setText:item.text];
        //[_updatelistItemRequestBuilder setSortby:(SInt32) item.sortBy]; // **DEPRECATED**
        [_updatelistItemRequestBuilder setType:[self convertLocalContentType:item.type]];
        
        if (item.contentSource.length > 0) {
            SCPUUID *contentSourcePUUID = [[SCPUUID alloc] initFromString:item.contentSource];
            [_updatelistItemRequestBuilder setFileId:[self puuidBuilder:contentSourcePUUID]];
        }
        
        [_requestBuilder setRequest:[_updatelistItemRequestBuilder build].data];
        
        Request *query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
    else
    {
        FTLog(@"Dictionary is not dictionary");
    }
}

/*
 no response object
 */

- (void) parseUpdateListItem:(id)data
{
    Response *response = data;
    
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDAsString = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDAsString]) {
        NSDictionary *savedDic = _delegateDictionary[transportPUUIDAsString];
        
        NSDictionary *transportDic = @{kKeySuccess:[NSNumber numberWithBool:response.status.success],
                                       @"list": savedDic[@"list"],
                                       @"item": savedDic[@"item"]
                                       };
        
        id<SCPBManagerDelegate>delegate = savedDic[kDelegate];
        [_delegateDictionary removeObjectForKey:transportPUUIDAsString];
        
        if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)]) {
            [delegate parsedType:SCPBManagerParceTypeUpdateListItem data:transportDic];
        }
    }
}

/* =======================================================================================
 MARK: LIST_ITEM_REMOVE
 message RemoveListItemRequest {
 required PUUID list_id = 1; /// List id
 required PUUID id = 2; /// List item id
 }
 */
-(void) removeListItemRequest:(id) dictionary
{
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        
        SCPUUID *transportPUUID = dictionary[kKeyTrasportPUUID];
        [_delegateDictionary setObject:dictionary forKey:[transportPUUID scpuuidToString]];
        
        SCList *list = dictionary[@"list"];
        SCListItem *item = dictionary[@"item"];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeListItemRemove];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        RemoveListItemRequestBuilder* _removeListItemRequestBuilder = [[RemoveListItemRequestBuilder alloc] init];
        [_removeListItemRequestBuilder setListId:[self puuidBuilder:list.puuid]];
        [_removeListItemRequestBuilder setId:[self puuidBuilder:item.puuid]];
        
        [_requestBuilder setRequest:[_removeListItemRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    } else {
        FTLog(@"Dictionary is not dictionary");
    }
}

/*
 no response object
 */
- (void) parseRemoveListItem:(id)data
{
    Response *response = data;
    
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDString = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDString])
    {
        NSDictionary *savedDict = _delegateDictionary[transportPUUIDString];
        
        id<SCPBManagerDelegate>delegate = savedDict[kDelegate];
        
        NSDictionary *transportDic = @{kKeySuccess:[NSNumber numberWithBool:response.status.success],
                                       @"list": savedDict[@"list"],
                                       @"item": savedDict[@"item"]
                                       };
        
        [_delegateDictionary removeObjectForKey:transportPUUIDString];
        
        if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)]) {
            [delegate parsedType:SCPBManagerParceTypeRemoveListItem data:transportDic];
        }
    }
}

/* =======================================================================================
 MARK: LIST_ITEMS_SET_ORDER
message SetListItemsOrderRequest {
    required PUUID list_id = 1; /// List id
    repeated PUUID ids = 2; /// Full list of list items ids in desirable order
}
 */

-(void) setListItemsOrderRequest:(id) dictionary
{
    if ([dictionary isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = dictionary[kKeyTrasportPUUID];
        //id<SCPBManagerDelegate> delegate = dictionary[kDelegate];

        [_delegateDictionary setObject:dictionary forKey:[transportPUUID scpuuidToString]];
        
        SCList *list = dictionary[@"list"];
        SCPUUID *listPUUID = list.puuid;
        //NSString *listPUUIDString = [listPUUID scpuuidToString];
        //SCPUUID *listPUUID = [[SCPUUID alloc] initFromString:listPUUIDString];
        
        NSArray *itemsPUUID = dictionary[@"itemsPUUID"];
        
        RequestBuilder *_requestBuilder = [[RequestBuilder alloc] init];
        [_requestBuilder setType:CallTypeListItemsSetOrder];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        SetListItemsOrderRequestBuilder *_setListItemsOrderRequestBuilder = [[SetListItemsOrderRequestBuilder alloc] init];
        
        NSMutableArray *idsArray = [NSMutableArray new];
        for (SCPUUID *itemPUUD in itemsPUUID) {
            [idsArray addObject:[self puuidBuilder:itemPUUD]];
        }
        
        [_setListItemsOrderRequestBuilder setListId:[self puuidBuilder:listPUUID]];
        [_setListItemsOrderRequestBuilder setIdsArray:idsArray];
        
        [_requestBuilder setRequest:[_setListItemsOrderRequestBuilder build].data];
        
        Request *query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    } else {
        FTLog(@"SetListItemsOrderRequest: ictionary is not dictionary");
    }
}

/*
 no response object
 */

- (void) parseSetListItemsOrder:(id) data
{
    Response *responce = data;

    SCPUUID *transportPUUID = [self scpuuidBuilder:responce.id];
    NSString *transportPUUIDAsString = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDAsString])
    {
        NSDictionary *savedDic = _delegateDictionary[transportPUUIDAsString];
        
        id<SCPBManagerDelegate>delegate = savedDic[kDelegate];
        
        NSMutableDictionary *transportDic = [[NSMutableDictionary alloc] init];
        transportDic[kKeySuccess] = [NSNumber numberWithBool:responce.status.success];
        transportDic[@"list"] = savedDic[@"list"];
        transportDic[@"itemsPUUID"] = savedDic[@"itemsPUUID"];
        
        [_delegateDictionary removeObjectForKey:transportPUUIDAsString];
        
        if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)]) {
            [delegate parsedType:SCPBManagerParceTypeSetListItemsOrder data:transportDic];
        }
    }
}

//MARK: ====== Supporting method's ======

- (NSDate *) dateFromSInt64: (SInt64) date
{
    return [NSDate dateWithTimeIntervalSince1970:((SInt64)date/1000)];
}

//MARK: equateProperties List

- (void) equatePropertiesServerList: (List *) serverList forList: (SCList *) list
{
    list.puuid = [self scpuuidBuilder:serverList.id];
    list.title = serverList.name;
    list.review = serverList.pb_description;

    list.imageListSource = [self  scpuuidStringBuilder: serverList.fileImageId];
    list.ownerPUUID = [self scpuuidBuilder:serverList.ownerId];
    list.changeTime = [self dateFromSInt64:serverList.changeTime];
    
    list.type = [self convertServerListType:serverList.listType];
    list.contentType = [self convertServerContentType:serverList.contentType];
}

//MARK: List Type Converter

- (ListType) convertLocalListType: (SCListType) listType
{
    switch (listType) {
        case SCListTypePublic:
            return ListTypePublic;
            break;
            
        case SCListTypePrivate:
            return ListTypePrivate;
            break;

        case SCListTypePurchased:
            return ListTypePurchased;
            break;
            
        case SCListTypeNotInitialised:
            return ListTypeNotInitialized;
            break;
            
        case SCListTypeDefault:
            return ListTypeDefault;
            break;
            
        case SCListTypeShared:
            return ListTypeShared;
            break;
            
        default:
            break;
    }
}

- (SCListType) convertServerListType: (ListType) serverListType
{
    switch (serverListType) {
        case ListTypePublic:
            return SCListTypePublic;
            break;
            
        case ListTypePrivate:
            return SCListTypePrivate;
            break;
            
        case ListTypePurchased:
            return SCListTypePurchased;
            break;
            
        case ListTypeNotInitialized:
            return SCListTypeNotInitialised;
            break;
            
        case ListTypeDefault:
            return SCListTypeDefault;
            break;
            
        case ListTypeShared:
            return SCListTypeShared;
            break;
            
        default:
            break;
    }
}

//MARK: List Content Type Converter

- (ContentType) convertLocalContentType: (SCContentType) contentType
{
    switch (contentType) {
        case SCContentTypeText:
            return ContentTypeTextContent;
            break;
            
        case SCContentTypeFile:
            return ContentTypeFileContent;
            break;
            
        case SCContentTypeEmoticon:
            return ContentTypeEmoticonContent;
            break;
            
        default:
            break;
    }
}

- (SCContentType) convertServerContentType: (ContentType) serverContentType
{
    switch (serverContentType) {
        case ContentTypeTextContent:
            return SCContentTypeText;
            break;
            
        case ContentTypeFileContent:
            return SCContentTypeFile;
            break;
            
        case ContentTypeEmoticonContent:
            return SCContentTypeEmoticon;
            break;
            
        default:
            break;
    }
}

//MARK: equateProperties Item

- (void) equatePropertiesServerItem: (ListItem *) serverItem forItem:(SCListItem *) item
{
    item.puuid = [self scpuuidBuilder:serverItem.id];
    item.text = serverItem.text;
    item.type = [self convertServerContentType:serverItem.type];
    //item.sortBy = (SInt32)serverItem.sortby; //**DEPRECATED**

    SCPUUID *contentSourcePUUID = [self scpuuidBuilder:serverItem.fileId];
    NSString *contentSourceString = [contentSourcePUUID scpuuidToString];
    
    if (contentSourceString) {
        item.contentSource = contentSourceString;
    } else {
        item.contentSource = @"";
    }
}

/*
 message ListItemBasic {
 required ListItemType type = 1;
 optional string text = 2;
 optional PUUID file_id = 3;
 optional int32 sort_by = 4 [default = 0];
 }
 */

- (void) equatePropertiesItem:(SCListItem *) item forServerItem:(ListItemBasicBuilder *) serverItem
{
    serverItem.text = item.text;
    serverItem.type = [self convertLocalContentType:item.type];
    //serverItem.sortBy = (SInt32)item.sortBy;// **DEPRECATED**
    
    if (item.contentSource.length > 0) {
        SCPUUID *contentSourcePUUID = [[SCPUUID alloc] initFromString:item.contentSource];
        serverItem.fileId = [self puuidBuilder:contentSourcePUUID];
        
    } else {
        serverItem.fileId = [[PUUID alloc] init];
    }
}

@end
