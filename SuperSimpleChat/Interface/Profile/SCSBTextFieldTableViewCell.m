//
//  SCSBTextFieldTableViewCell.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSBTextFieldTableViewCell.h"
#import "SCModelProfile.h"
#import "SCContactItem.h"

@implementation SCSBTextFieldTableViewCell
{
    NSString *_tempText;
}
- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
}

- (void) _init
{
    _textField.delegate = self;
    [self subscribeKeyboardNotification];
}

- (instancetype)init
{
    [self _init];
    self = [super init];
    
    if (self) {

    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    [self _init];
    self = [super initWithCoder:aDecoder];
    if (self) {

    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    _icoImageView.frame = CGRectMake(_edgeInsets.left, 0.0,
                                     self.contentView.bounds.size.height, self.contentView.bounds.size.height);
    
    CGFloat textFieldX = _icoImageView.frame.origin.x + _icoImageView.frame.size.width;
    
    _textField.frame = CGRectMake(textFieldX, 0.0,
                                  self.contentView.frame.size.width - textFieldX - _edgeInsets.right,
                                  self.contentView.frame.size.height);
}

//MARK: Setups
- (void) subscribeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//MARK: Set
- (void) setTypeFieldProfile:(SCTypeFieldProfile)typeFieldProfile
{
    _typeFieldProfile = typeFieldProfile;
    NSString *placeholderText = @"";
    
    switch (_typeFieldProfile) {
        case SCTypeFieldProfileAbout:
        {
            _icoImageView.image = nil;
            placeholderText = @"About me";
            
            if (_settings.modelProfile.contactItem.contactAbout) {
                _textField.text = _settings.modelProfile.contactItem.contactAbout;
            } else {
                _textField.text = @"";
                
            }
        }
            break;
            
        case SCTypeFieldProfileFirstName:
        {
            _icoImageView.image = [_settings.skinManager getImageForKeyX:@"profile-name"];
            placeholderText = @"First name";
            
            if (_settings.modelProfile.contactItem.contactFirstName) {
                _textField.text = _settings.modelProfile.contactItem.contactFirstName;
            } else {
                _textField.text = @"";
            }
        }
            break;
            
        case SCTypeFieldProfileLastName:
        {
            _icoImageView.image = [_settings.skinManager getImageForKeyX:@"profile-name"];
             placeholderText = @"Last name";
            
            if (_settings.modelProfile.contactItem.contactLastName) {
                _textField.text = _settings.modelProfile.contactItem.contactLastName;
            } else {
                _textField.text = @"";
            }
        }
            break;
            
        default:
            break;
    }
    
    NSAttributedString *attrPlaceholder = [[NSAttributedString alloc] initWithString:placeholderText attributes:@{NSForegroundColorAttributeName:SKCOLOR_TableTextFieldPlchldrFont}];
    _textField.attributedPlaceholder = attrPlaceholder;
    
    _textField.textColor = SKCOLOR_TableCellFont;
}

//MARK: UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _tempText = _textField.text;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (![_tempText isEqualToString:_textField.text]) {
        if ([_delegate respondsToSelector:@selector(changeTextValue:for:)]) {
            
            if ([_textField.text isEqualToString:@""]) {
                [_delegate changeTextValue:nil for:_typeFieldProfile];
                
            } else {
                [_delegate changeTextValue:_textField.text for:_typeFieldProfile];
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

//MARK: Keyboard Notification
- (void) keyboardWillShow:(NSNotification *)notification
{
    if ([_delegate respondsToSelector:@selector(showKeyboard)]) {
        [_delegate showKeyboard];
    }
}

- (void) keyboardWillHide:(NSNotification *)notification
{
    if ([_delegate respondsToSelector:@selector(hideKeyboard)]) {
        [_delegate hideKeyboard];
    }
}

@end