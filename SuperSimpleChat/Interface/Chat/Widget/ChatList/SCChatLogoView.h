//
//  SCChatLogoView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 28.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCViewWithProgress.h"
#import "SCModelsDelegates.h"


@interface SCChatLogoView : SCViewWithProgress <SCModelAbstarctDelegate>


-(void) clear;
-(void) setLogoSource:(id) source;
@end
