//
//  SCDatePickerViewController.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SCDatePickerDelegate <NSObject>

- (void) setBirthdate: (NSDate *) date;
- (void) closeModalView;

@end

@interface SCDatePickerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (weak, nonatomic) NSDate *birthdate;
@property (strong, nonatomic) id <SCDatePickerDelegate> delegate;

- (IBAction)deleteButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;

@end
