//
//  SCCountry.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 16.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCCountry : NSObject

+ (NSArray *) countryList;

@end
