//
//  SCPBManagerAbstract.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 02.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCWebSocketClient.h"
#import "SCModelsDelegates.h"


@class SCChatItem;
@class SCContactItem;
@class SCListItem;
@class SCList;
@class SCMessageItem;
@class SCPUUID;
@class SCUserAuthItem;



@interface SCPBManagerDelegateTypes : NSObject
@property(nonatomic,assign) SCPBManagerParceType type;
@property(nonatomic,weak) id<SCPBManagerDelegate> delegate;
@end

@interface SCPBManager : NSObject <SCWebSocketClientDelegate>
{
    SCSettings* _settings;
    SCWebSocketClient* _socketClient;
    NSMutableArray* _delegates;
    NSMutableDictionary *_delegateDictionary;
    NSTimer *_pingTimer;
}

@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) SCWebSocketClient* socketClient;

-(void) addDelegate:(id<SCPBManagerDelegate>) delegate forType:(SCPBManagerParceType) type;
-(void) delDelegate:(id<SCPBManagerDelegate>) delegate forType:(SCPBManagerParceType) type;
-(void) callDelegate:(SCPBManagerParceType) type withData:(id) idData;

// Requests

// Profile
//-(void) getProfile:(id) params;
-(void) profileGet:(id)params;
-(void) setProfileField:(id) params;
-(void) removeProfileField:(id) params;
-(void) checkIsNicknameFree:(id) params;
-(void) changeStatus:(id) params;
-(void) pingServerStatus:(SCPingStatus) pingStatus;
-(void) logout:(id) params;
-(void) subscribeToAmazonSNS:(id) params;
-(void) unSubscribeFromAmazonSNS:(id) params;
-(void) authAdd:(id) params;
-(void) authConfirm:(id) params;
-(void) authRemove:(id) params;
-(void) authGet:(id) params;

// users
-(void) getUserById: (id) params;
-(void) searchUsersByAuthRequest:(NSMutableDictionary*) params;
-(void) getUserByAuth:(id) params;
#pragma mark - invate
-(void) inviteUsersRequest:(id)data;
#pragma mark - subscribtion
-(void) subscribeRequest:(id)params;
-(void) unSubscribeRequest:(id)params;
-(void) getAllUsersWhichISubscribed:(id)params;
#pragma mark - banned
-(void) checkIfUserInBlackListRequest:(id)params;
-(void) removeUsersFromBlackListRequest:(id)params;
-(void) addUsersToBlackListRequest:(id)params;
-(void) getBannedUsersRequest:(id)params;




// -(void) profileSetWithContact:(SCContactItem *)profile;



// Lists
-(void) getListRequest: (id) dictionary;
-(void) addItemListRequest: (id) dictionary;
-(void) getListItemsRequest:(id) dictionary;
-(void) updateListItemRequest:(id) dictionary;
-(void) removeListItemRequest:(id) dictionary;
-(void) setListItemsOrderRequest:(id) dictionary;

// ChatRoom
-(void) createChatRoomRequest:(id) params;
-(void) getChatRoomByIdRequest:(id) params;
//-(void) searchCreateChatRoomRequest:(id) params;
-(void) removeChatRoomRequest:(id) params;
-(void) chatRoomActivate:(id) params;
-(void) changeChatRoomRequest:(id)params;
-(void) getAllRoomsForUserRequest:(id)params;
-(void) searchPublicChatRooms:(id)params;
-(void) getAllUsersForRoomRequest:(id)params;
-(void) leaveUserFromRoomRequest:(id)params;
-(void) joinUserToRoomRequest:(id)params;
-(void) getLastReadMessageDate:(id)params;
-(void) setLastReadMessageDate:(id)params;

// Messages
-(void) sendMessage:(id)params;
-(void) updateMessage:(id) params;
-(void) removeMessage:(id) params;
-(void) getChatRoomHistory:(id) params;
-(void) getChatRoomHistoryWithTimeRangeRequest:(id) params;

// Contacts
-(void) createContactRequestWithParams:(NSMutableDictionary*) params;
-(void) removeContactRequest:(SCContactItem*) contact;
-(void) getAllContactFieldsRequest:(SCContactItem*) contact;
-(void) setFieldsRequestWithParams:(NSMutableDictionary*) params;
-(void) removeFieldRequest:(SCContactItem*) contact;
-(void) getAllContactsRequestWithParams:(NSMutableDictionary*) params;
-(void) creatContactWithFields:(NSDictionary*) fields;


@end
