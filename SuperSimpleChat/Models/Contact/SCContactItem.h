//
//  SCUserItem.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"
#import "SCAdress.h"
#import "SCUserAuthItem.h"
#import "SCModelUser.h"
#import "SCNetFile.h"
//#import "SCTypes.h" -> in SCModelsDelegates.h
#import "SCModelsDelegates.h"


@class SCContactItemSmall,SCParentTableViewCell,SCNetFile;



@interface SCContactItem : SCAbstractItem <SCPBManagerDelegate>


@property(nonatomic,strong) SCPUUID* userPUUID;     // User PUUID
@property(nonatomic,strong) SCPUUID* contactPUUID;  // Contact PUUID
@property(nonatomic,strong) NSString* contactLoginName;
@property(nonatomic,strong) NSString* contactNikName;
@property(nonatomic,strong) NSString* contactFirstName;
@property(nonatomic,strong) NSString* contactLastName;
@property(nonatomic,strong) NSString* contactSex;
@property(nonatomic,strong) NSDate* contactBirthdate;
@property(nonatomic,strong) SCNetFile* netFile;
@property(nonatomic,strong) NSMutableArray* userAuths;
//#warning contactEmail & contactPhone is DEPRECATED
//FIXIT: Удалить устаревшие свойства
@property(nonatomic,strong) NSString* contactEmail;// deprecated
@property(nonatomic,strong) NSString* contactPhone;//deprecated
@property(nonatomic,strong) NSString* contactLanguage;
@property(nonatomic,strong) NSString* contactTimeZone;
@property(nonatomic,strong) SCAdress* contactAddress;
@property(nonatomic,strong) NSString* contactAbout;
@property(nonatomic,strong) NSString* contactWebSite;
@property(nonatomic,strong) NSString* fullName;
@property(nonatomic,assign) SCUserStatus contactStatus;
@property(nonatomic,strong) NSDate* lastConnection;

// для профиля
@property(nonatomic,strong) SCPUUID* favoriteListPUUID;

// dynamic
//@property(nonatomic,strong) NSString* contactChatStatus;
@property(nonatomic,strong) NSString* contactIsFriend;
@property(nonatomic,strong) NSString* contactGroupID;
@property(nonatomic,assign) BOOL isBlocked;
@property(nonatomic,strong) SCContactItem* user;
@property(nonatomic,strong) NSDate* itemUpdatedTime;

#pragma mark - AUTH
-(void) contactAddAuth:(SCUserAuthItem*) userAuth;

-(void) updateFrom:(SCContactItem*) newItem;
//-(void) contactDefault;
-(NSArray *)toFullArray;
-(NSArray*)toAddressArray;
-(NSArray *) toFiltredArray;
-(SCContactItemSmall*) getSmall;

-(void)setContactFields;
-(void)getContactFields;

@end


