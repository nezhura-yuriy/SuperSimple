//
//  BlockContactTableViewCell.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 21.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"
#import "SCContactItem.h"

@interface BlockContactTableViewCell : UITableViewCell

@property (strong, nonatomic) SCSettings* settings;
@property (strong, nonatomic) SCContactItem* contact;
@property (weak, nonatomic) IBOutlet UISwitch *blockContactSwitch;
@property (weak, nonatomic) IBOutlet UILabel *blockContactLabel;


- (IBAction)blockContactAction:(id)sender;

@end
