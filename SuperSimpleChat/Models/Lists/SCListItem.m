//
//  SCListItem.m
//  SmartChat
//
//  Created by Yury Radchenko on 1/14/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCListItem.h"
#import "SCModelProfile.h"
#import "SCSettings.h"
#import "SCMessageBodyItem.h"

static const SCContentType kItemTypeDefault = SCContentTypeText;

@implementation SCListItem
{
    SCSettings *_settings;
}

//MARK: Item Inits
- (instancetype) init {
    self = [super init];
    
    if (self) {
        [self _init];
    }
    return self;
}

-(instancetype) initWithText:(NSString*) itemText
{
    [self _init];
    
    if (self)
    {
        if (itemText != nil) {
            _text = itemText;
        }
    }
    return self;
}

+ (instancetype) itemWithText: (NSString *) text
{
    return [[SCListItem alloc] initWithText:text];
}

+ (instancetype) itemRandomPUUIDWithText: (NSString *) text {
    
    SCListItem *itemRandom = [[SCListItem alloc] initWithText:text];
    itemRandom.puuid = [[SCPUUID alloc] initWithRandom];
    return itemRandom;
}

- (instancetype) initWithItem:(SCListItem *) realItem
{
    self = [self init];
    
    if (self) {
        self.puuid = realItem.puuid;
        self.text = realItem.text;
        self.type = realItem.type;
        self.contentSource = realItem.contentSource;
    }
    
    return self;
}

- (void) _init {
    _settings = [SCSettings sharedSettings];
    
    //Default values
    _puuid = [[SCPUUID alloc] init];
    _text = @"";
    _sortBy = 0; // **DEPRECATED**
    _type = kItemTypeDefault;
    _contentSource = @"";
}

- (instancetype) initFromDictionary: (NSDictionary *) dictionary
{
    [self _init];
    
    if (self) {
        _puuid = [[SCPUUID alloc] initFromString:dictionary[@"puuid"]];
        _text = dictionary[@"text"];
        _type = [dictionary [@"type"] intValue];
        // _sortBy is **DEPRECATED**
        //_sortBy = [[dictionary objectForKey:@"sortBy"] integerValue]; //**DEPRECATED**
        _sortBy = 0;
        
        _contentSource = dictionary[@"contentSource"];
    }
    return self;
}

//MARK: Convert
- (NSDictionary *) toDictionary {
    
    [self checkNilValues];
    
    return @{@"puuid":[_puuid scpuuidToString],
             @"text": _text,
             @"type": [NSNumber numberWithInt:_type],
             //@"sortBy": [NSNumber numberWithInteger:_sortBy], // **DEPRECATED**
             @"contentSource": _contentSource
             };
}

- (void) checkNilValues
{
    if (!_text) {
        _text = @"";
    }
    
    if (!_contentSource) {
        _contentSource =  @"";
    }
}

- (NSArray *) toMessageArray
{
    switch (_type) {
        case SCContentTypeText:
        {
            SCMessageBodyItem *message = [[SCMessageBodyItem alloc] initWithSettings:_settings];
            message.type = [self convertItemType:_type];
            message.data = _text;
            return @[message];
        }
            break;
            
        default:
            return [[NSArray alloc] init];
            break;
    }
}

- (SCDataType) convertItemType: (SCContentType) itemType
{
    switch (itemType) {
        case SCContentTypeText:
            return SCDataTypeText;
            break;
        
        case SCContentTypeFile:
            return SCDataTypePhoto;
        
        case SCContentTypeEmoticon:
            return SCDataTypeEmoticon;
            
        default:
            return SCDataTypeNone;
            break;
    }
}

//MARK: Equal

- (BOOL) isEqualItem:(SCListItem *) item
{
    if (![_puuid isEqual:item.puuid]) {
        return NO;
    }
    
    if (![_text isEqualToString:item.text]) {
        return NO;
    }
    
    if (_type != item.type) {
        return NO;
    }
    
    if (![_contentSource isEqualToString:item.contentSource]) {
        return NO;
    }
    
    //sortBy not use 
    
    return YES;
}

- (void) equateItem: (SCListItem *) item
{
    //_puuid is not changed
    _text = item.text;
    _type = item.type;
    _contentSource = item.contentSource;
    //sortBy not use //**DEPRECATED**
}

//MARK: Description
-(NSString*) description
{
    NSString *typeStr = @"";
    switch (_type) {
        case SCContentTypeText:
            typeStr = @"SCItemTypeText";
            break;
            
        case SCContentTypeFile:
            typeStr = @"SCItemTypeFile";
            break;
            
        case SCContentTypeEmoticon:
            typeStr = @"SCItemTypeEmoticon";
            break;
            
        default: typeStr = @"I don't know item type";
            break;
    }
    
    NSString *puuidStr = [_puuid scpuuidToString];
    
    return [NSString stringWithFormat:@"Item: puuid= %@ text= %@, type = %@, contentSource = %@", puuidStr, _text, typeStr, _contentSource];
}

@end
