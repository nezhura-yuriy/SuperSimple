//
//  SCChatListTVCell.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatListCell.h"
#import "SCSettings.h"
#import "SCImageViewWithIndicator.h"
#import "SCChatItem.h"
#import "SCNetFile.h"
#import "SCMessageItem.h"
#import "SCMessageBodys.h"
#import "UIImageView+Letters.h"
#import "SCChatLogoView.h"



#define MARGIN 10.0f
#define CHAT_IMAGE_SIZE 44.0f
#define SEPARATOR_HEIGHT 2.0f
#define HEADER_HEIGHT 33

@implementation SCChatListCell
{
    SCSettings* _settings;
    CGRect fR;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
//    fR = _imageChat.frame;
    [self _init];
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        [self _init];
    }
    return self;
}


-(void) _init
{
    _settings = [SCSettings sharedSettings];
    
    _labName.text = @"";
//    _labDescript.text = @"";
    [self skinChange];
    [_chatLogoView setSettings:_settings];
}

-(void) dealloc
{
    FTLog(@"%@ dealloc",NSStringFromClass([self class]));
    [_chatData delDelegate:self];
    _detail = nil;
    [_chatLogoView clear];
}

-(void) prepareForReuse
{
    [super prepareForReuse];
    _dataDelegate = nil;
    self.delegate = nil;
    [_chatLogoView clear];
    [_detail clear];
    if(_chatData)
    {
        [_chatData delDelegate:self];
        _chatData = nil;
    }
    _labName.text = @"";
    _labCount.text = @"";
    _labCount.hidden = YES;
    [self skinChange];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
}

-(void) skinChange
{
    _labName.textColor = SKCOLOR_ChatListNickName;
    [_labName setFont:[UIFont fontWithName:FT_FontNameBold size:12.0]];
    [_labCount setFont:[UIFont fontWithName:FT_FontNameDefult size:12.0]];
    _imageSeparator.backgroundColor = SKCOLOR_TableSeparator;
    self.backgroundColor = SKCOLOR_Background;
}

-(void) setChatData:(SCChatItem *)chatData
{
    _chatData = chatData;
    [_chatData addDelegate:self];
    _labName.text = _chatData.chatName;
    if(_chatData.notViewedMessagesCount > 0)
    {
        [_labCount setFont:[UIFont fontWithName:FT_FontNameLight size:12]];
        _labCount.text = [NSString stringWithFormat:@"%ld",(long)_chatData.notViewedMessagesCount];
        [_labCount sizeToFit];
        CGFloat labCountSize = MAX(_labCount.frame.size.width, _labCount.frame.size.height)*1.1;
        _labCount.frame = CGRectMake(_labCount.frame.origin.x, _labCount.frame.origin.y, labCountSize, labCountSize);
        _labCount.layer.cornerRadius = labCountSize*0.5;
        _labCount.backgroundColor = SKCOLOR_ChatListBadge;
        _labCount.hidden = NO;
    }
    else
        _labCount.hidden = YES;
    
//    if(_chatData.chatType == SCChatTypePublic)
    if(!_chatData.lastMessage || _chatData.chatType == SCChatTypePublic)
    {
        [_detail setDescription:_chatData.chatDescription withSettings:_settings];
    }
    else
    {
        [_detail setMessageBodys:_chatData.lastMessage.messageBodys withSettings:_settings];
    }
    [_chatLogoView setLogoSource:_chatData.logoSource];
/* /    _imageChat.isRounded = NO;
    [_imageChat setImageWithString:_chatData.chatName color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
    [_chatData.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                break;
                
            case SCNetFileComletionTypeProgress:
                [_imageChat setProgressPercent:[result doubleValue]];
                break;
                
            case SCNetFileComletionTypeError:
                FTLog(@"%@ SCNetFileComletionTypeError обработать",NSStringFromClass([self class]));
                [_imageChat endProgress];
                break;
                
            case SCNetFileComletionTypeCompleet:
                _imageChat.image = [_chatData.netFile imageForSize:_imageChat.frame.size];
                [_imageChat endProgress];
                break;
                
            default:
                break;
        }
    }];
*/
}

-(void) showCount
{
    if(_chatData.notViewedMessagesCount > 0)
    {
        [_labCount setFont:[UIFont fontWithName:FT_FontNameLight size:12]];
        _labCount.text = [NSString stringWithFormat:@"%ld",(long)_chatData.notViewedMessagesCount];
        [_labCount sizeToFit];
        CGFloat labCountSize = MAX(_labCount.frame.size.width, _labCount.frame.size.height)*1.1;
        _labCount.frame = CGRectMake(_labCount.frame.origin.x, _labCount.frame.origin.y, labCountSize, labCountSize);
        _labCount.layer.cornerRadius = labCountSize*0.5;
        _labCount.backgroundColor = SKCOLOR_ChatListBadge;
        _labCount.hidden = NO;
    }
    else
        _labCount.hidden = YES;

}

#pragma mark @protocol SCChatItemDelegate <SCModelAbstarctDelegate>
-(void) loadedGroupsMessage:(NSArray *)changedGroups __deprecated
{
    _labCount.text = [NSString stringWithFormat:@"%ld",(long)_chatData.notViewedMessagesCount];
    if(_dataDelegate && [_dataDelegate respondsToSelector:@selector(cellNeedReload:)])
        [_dataDelegate cellNeedReload:self];
}


#pragma mark SCPBManagerDelegate <NSObject>
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"%@ : parsedType",NSStringFromClass([self class]));
    switch (type)
    {
        case SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange:
        case SCPBManagerParceTypeGetChatRoomHistory:
        case SCPBManagerParceTypeSendMessage:
        case SCPBManagerParceTypeMessage:
        case SCPBManagerParceTypeChangeChat:
        case SCPBManagerParceTypeChangeRoomEvent:
        {
//            _labCount.text = [NSString stringWithFormat:@"%ld",(long)_chatData.notViewedMessagesCount];
//TODO: мешает
//            if(_dataDelegate && [_dataDelegate respondsToSelector:@selector(cellNeedReload:)])
//                [_dataDelegate cellNeedReload:self];

            [self showCount];
            
            [self visualSignal];
        }
            break;
        case SCPBManagerParceTypeGetLastReadMessageDate:
        case SCPBManagerParceTypeSetLastReadMessageDate:
        {
            [self showCount];
        }
            break;
            
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;

    }
}

-(void) visualSignal
{
    [UIView animateWithDuration:1.0 animations:^{
        _labCount.backgroundColor = [UIColor redColor];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2.5 animations:^{
            _labCount.backgroundColor = SKCOLOR_ChatListBadge;
        } completion:^(BOOL finished) {
            FTLog(@"End");
        }];
    }];
}

+(CGFloat) calcSize:(CGFloat) width chatItem:(SCChatItem*) chatItem
{
    SCMessageItem* lastMessage = chatItem.lastMessage;
    CGFloat addHeigh = 33;
    if(!lastMessage || chatItem.chatType == SCChatTypePublic)
    {
        addHeigh = (MAX(addHeigh,[SCChatListDetailView calcHeightDescription:chatItem.chatDescription forWidth:width*0.7]));
    }
    else
    {
        addHeigh = (MAX(addHeigh,[SCChatListDetailView calcHeight:lastMessage.messageBodys forWidth:width*0.7]));
    }
    return MAX(HEADER_HEIGHT + addHeigh,70);
}
@end
