//
//  BirthdayTableViewCell.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 04.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirthdayTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *birthdayTextFieldOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;

@end
