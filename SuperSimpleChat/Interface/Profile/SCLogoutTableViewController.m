//
//  SCLogoutTableViewController.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 16.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCLogoutTableViewController.h"
#import "SCHTTPManager.h"

@interface SCLogoutTableViewController ()

@end

@implementation SCLogoutTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupNavigation];
    self.tableView.scrollEnabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

//MARK: Setup
- (void) setupNavigation
{
    //Nav Title
    self.title = @"Logout";
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    
    //Back button
    UIImage *backButtonImage = [_settings.skinManager getImageForKeyX:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

//MARK: TableView
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        cell.textLabel.text = @"Logout of this device only";
        
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        cell.textLabel.text = @"Logout of all devices";
    }
    
    cell.textLabel.frame = cell.contentView.frame;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = SKCOLOR_ButtonRemoveBg;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 &&indexPath.row == 0) {
        [self logoutAllDevice:NO];
        
    } else if (indexPath.section ==1 && indexPath.row == 0) {
        [self logoutAllDevice:YES];
    }
}

- (void) logoutAllDevice: (BOOL) allDevice
{
    if (_settings)
    {
        [_settings play:SCSoundPlayLogout];
        [_settings.modelProfile logout:allDevice];
    }
}

@end
