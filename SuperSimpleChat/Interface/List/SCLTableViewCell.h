//
//  SCLTableViewCell.h
//  SuperSimpleChat
//
//  Created by yury on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCListItem.h"
#import "SCLItemTextField.h"
#import "SCLTableViewCellDelegate.h"
#import "SCSettings.h"

typedef NS_ENUM(NSInteger, SCLTableViewCellType) {
    SCLTableViewCellTypeExistingCell = 0, //default
    SCLTableViewCellTypeAddingCell
};

@interface SCLTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, strong) SCListItem *item;
@property (nonatomic, weak) id <SCLTableViewCellDelegate> delegate;
@property (nonatomic, strong, readonly) SCLItemTextField *label;
@property (nonatomic, strong) SCSettings *settings;
@property (nonatomic) SCLTableViewCellType cellType;

- (instancetype) initWithType: (SCLTableViewCellType) cellType;

@end
