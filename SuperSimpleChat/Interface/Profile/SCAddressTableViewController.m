//
//  SCAddressTableViewController.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAddressTableViewController.h"
#import "SCModelProfile.h"
#import "SCContactItem.h"
#import "SCAdress.h"

static NSString *kCountryNotSelected = @"Country not selected";

static NSString *kTextFieldPlaceholderCity = @"City";
static NSString *kTextFieldPlaceholderStreet = @"Street";
static NSString *kTextFieldPlaceholderHouse = @"House";
static NSString *kTextFieldPlaceholderApartment = @"Apartment";
static NSString *kTextFieldPlaceholderPostCode = @"Post code";

typedef NS_ENUM(NSInteger, SCCountrySelected) {
    SCCountrySelectedNot = 0,
    SCCountrySelectedYes
};

@implementation SCAddressTableViewController
{
    NSString *_tempText;
    CGFloat _currentOffset;
    CGSize _tableViewContentSize;
    UITextField *_tempTextField;
    UIEdgeInsets _tableViewSeparatorEdgeInsets;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    FTLog(@"%@", NSStringFromSelector(_cmd));
    [self setupNavigation];

    //TextFields
    _cityTextField.delegate = self;
    _cityTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTextFieldPlaceholderCity attributes:@{NSForegroundColorAttributeName:SKCOLOR_TableTextFieldPlchldrFont}];
    _cityTextField.textColor = SKCOLOR_TableCellFont;
    _cityTextField.tag = 1; //row number
    
    _streetTextField.delegate = self;
    _streetTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTextFieldPlaceholderStreet attributes:@{NSForegroundColorAttributeName:SKCOLOR_TableTextFieldPlchldrFont}];
    _streetTextField.textColor = SKCOLOR_TableCellFont;
    _streetTextField.tag = 2;
    
    _houseTextField.delegate = self;
    _houseTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTextFieldPlaceholderHouse attributes:@{NSForegroundColorAttributeName:SKCOLOR_TableTextFieldPlchldrFont}];
    _houseTextField.textColor = SKCOLOR_TableCellFont;
    _houseTextField.tag = 3;
    
    _apartmentTextField.delegate = self;
    _apartmentTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTextFieldPlaceholderApartment attributes:@{NSForegroundColorAttributeName:SKCOLOR_TableTextFieldPlchldrFont}];
    _apartmentTextField.textColor = SKCOLOR_TableCellFont;
    _apartmentTextField.tag = 4;
    
    _postCodeTextField.delegate = self;
    _postCodeTextField.attributedPlaceholder  = [[NSAttributedString alloc] initWithString:kTextFieldPlaceholderPostCode attributes:@{NSForegroundColorAttributeName:SKCOLOR_TableTextFieldPlchldrFont}];
    _postCodeTextField.textColor = SKCOLOR_TableCellFont;
    _postCodeTextField.tag = 5;
    
    [self setupTextValues];
    
    [self subscribeKeyboardNotification];
    
    _tableViewSeparatorEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupUI];
    _settings.modelProfile.delegateChangeField = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//MARK:Setups
- (void) setupNavigation
{
    //Nav Title
    self.title = @"Address";
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    
    //Back button
    UIImage *backButtonImage = [_settings.skinManager getImageForKeyX:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

- (void) setupTextValues
{
    if (_settings.modelProfile.contactItem.contactAddress.country &&
        _settings.modelProfile.contactItem.contactAddress.country.length > 0)
    {
         NSMutableAttributedString *attributedText = [self attribeteText:_settings.modelProfile.contactItem.contactAddress.country forState:SCCountrySelectedYes];
        _countryLabel.attributedText = attributedText;
        
    } else {
        NSMutableAttributedString *attributedText = [self attribeteText:kCountryNotSelected forState:SCCountrySelectedNot];
        _countryLabel.attributedText = attributedText;
    }
    
    if (_settings.modelProfile.contactItem.contactAddress.city) {
        _cityTextField.text = _settings.modelProfile.contactItem.contactAddress.city;
    }
    
    if (_settings.modelProfile.contactItem.contactAddress.street) {
        _streetTextField.text = _settings.modelProfile.contactItem.contactAddress.street;
    }
    
    if (_settings.modelProfile.contactItem.contactAddress.house) {
        _houseTextField.text = _settings.modelProfile.contactItem.contactAddress.house;
    }
    
    if (_settings.modelProfile.contactItem.contactAddress.appartament) {
        _apartmentTextField.text = _settings.modelProfile.contactItem.contactAddress.appartament;
    }
    
    if (_settings.modelProfile.contactItem.contactAddress.postCode) {
        _postCodeTextField.text = _settings.modelProfile.contactItem.contactAddress.postCode;
    }
}

- (NSMutableAttributedString *) attribeteText:(NSString *) text forState: (SCCountrySelected) countrySelected
{
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentLeft;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    UIColor *fontColor;
    
    if(countrySelected == SCCountrySelectedYes) {
        fontColor = SKCOLOR_TableCellFont;
        
    } else if (countrySelected == SCCountrySelectedNot) {
        fontColor = SKCOLOR_TableTextFieldPlchldrFont;
    }
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:fontColor,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:14],
                                NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attribute];
}

- (void) setupUI
{
    NSIndexPath *cellCityIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *cellCity = [self.tableView cellForRowAtIndexPath:cellCityIndexPath];
    
    UIImage *cityImg = [_settings.skinManager getImageForKeyX:@"profile-city"];
    
    _cityImageView.image = cityImg;
    _cityImageView.frame = CGRectMake(15, 0, cityImg.size.width, cellCity.contentView.frame.size.height);
}

//MARK: Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.view endEditing:YES];
    
    if ([segue.identifier isEqualToString:@"CountrySegue"]) {
        SCCountrysTableViewController *countryTVC = segue.destinationViewController;
        countryTVC.settings = _settings;
        countryTVC.delegate = self;
    }
}

//MARK: TableView
-(void)viewDidLayoutSubviews
{
    // iOS 7
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:_tableViewSeparatorEdgeInsets];
    }
    // iOS 8
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:_tableViewSeparatorEdgeInsets];
    }
}

//MARK: UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _tempTextField = textField;
    _tempText = textField.text;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
};

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (![_tempText isEqualToString:textField.text]) {
        
        if (textField.text.length > 0) {
            [self saveField:textField.placeholder forValue:textField.text];
        } else {
            [self removeProfileField:textField.placeholder];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

//MARK: Change fields
- (void) saveField: (NSString *) fieldPlaceholder forValue: (NSString *) value {
    
    if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderCity]) {
        [_settings.modelProfile setField:SCFieldTypeCyty forValue:value];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderStreet]) {
        [_settings.modelProfile setField:SCFieldTypeStreet forValue:value];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderHouse]) {
        [_settings.modelProfile setField:SCFieldTypeHose forValue:value];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderApartment]) {
        [_settings.modelProfile setField:SCFieldTypeAppartament forValue:value];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderPostCode]) {
        [_settings.modelProfile setField:SCFieldTypePostCode forValue:value];
    }
}

- (void) removeProfileField: (NSString *) fieldPlaceholder
{
    if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderCity]) {
        [_settings.modelProfile removeProfileField:SCFieldTypeCyty];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderStreet]) {
        [_settings.modelProfile removeProfileField:SCFieldTypeStreet];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderHouse]) {
        [_settings.modelProfile removeProfileField:SCFieldTypeHose];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderApartment]) {
        [_settings.modelProfile removeProfileField:SCFieldTypeAppartament];
        
    } else if ([fieldPlaceholder isEqualToString:kTextFieldPlaceholderPostCode]) {
        [_settings.modelProfile removeProfileField:SCFieldTypePostCode];
    }
}

//MARK: SCCountrysTableVCProtocol
- (void) changeCountry:(NSString *)countryTitle
{
    if (countryTitle)
    {
        NSMutableAttributedString *attributedText = [self attribeteText:countryTitle forState:SCCountrySelectedYes];
        _countryLabel.attributedText = attributedText;
    }
    else
    {
        NSMutableAttributedString *attributedText = [self attribeteText:kCountryNotSelected forState:SCCountrySelectedNot];
            _countryLabel.attributedText = attributedText;
    }
}

//MARK: Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

//MARK: Keyboard
- (void) subscribeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showKeyboard)
                                                 name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideKeyboard)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void) showKeyboard
{
    if (IS_IPHONE_4_AND_OLDER || IS_IPHONE_5) {
        
        NSIndexPath *selectedCellIndexPath = [NSIndexPath indexPathForRow:_tempTextField.tag inSection:0];
        [self.tableView scrollToRowAtIndexPath:selectedCellIndexPath
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
        self.tableView.scrollEnabled = YES;
    }
}

- (void) hideKeyboard
{
    if (IS_IPHONE_4_AND_OLDER || IS_IPHONE_5) {
        self.tableView.scrollEnabled = NO;
    }
}

//MARK: SCModelProfileProtocol
- (void) field:(SCFieldType)fieldType change:(SCFieldChange)fieldChange value:(id)fieldValue
{
    if (fieldChange == SCFieldChangeNoSuccess) {
        switch (fieldType) {
            case SCFieldTypeCountry:
            {
                if (_settings.modelProfile.contactItem.contactAddress.country) {
                    
                    NSMutableAttributedString *attributedText = [self attribeteText:_settings.modelProfile.contactItem.contactAddress.country forState:SCCountrySelectedYes];
                    _countryLabel.attributedText = attributedText;
                    
                } else {
                    NSMutableAttributedString *attributedText = [self attribeteText:kCountryNotSelected forState:SCCountrySelectedNot];
                    _countryLabel.attributedText = attributedText;
                }
            }
                break;
                
            case SCFieldTypeCyty:
            {
                if (_settings.modelProfile.contactItem.contactAddress.city) {
                    _cityTextField.text = _settings.modelProfile.contactItem.contactAddress.city;
                } else {
                    _cityTextField.text = @"";
                }
            }
                break;
                
            case SCFieldTypeStreet:
            {
                if (_settings.modelProfile.contactItem.contactAddress.street) {
                    _streetTextField.text = _settings.modelProfile.contactItem.contactAddress.street;
                } else {
                    _streetTextField.text = @"";
                }
            }
                break;
                
            case SCFieldTypeHose:
            {
                if (_settings.modelProfile.contactItem.contactAddress.house) {
                    _houseTextField.text = _settings.modelProfile.contactItem.contactAddress.house;
                } else {
                    _houseTextField.text = @"";
                }
            }
                break;
            
            case SCFieldTypeAppartament:
            {
                if (_settings.modelProfile.contactItem.contactAddress.appartament) {
                    _apartmentTextField.text = _settings.modelProfile.contactItem.contactAddress.appartament;
                } else {
                    _apartmentTextField.text = @"";
                }
            }
                break;
            
            case SCFieldTypePostCode:
            {
                if (_settings.modelProfile.contactItem.contactAddress.postCode) {
                    _postCodeTextField.text = _settings.modelProfile.contactItem.contactAddress.postCode;
                } else {
                    _postCodeTextField.text = @"";
                }
            }
                break;
                
            default:
                break;
        }
    }
}

@end
