//
//  SCPBManagerTernopol+User.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol(User).h"
#import "SCPBManagerTernopol(Converts).h"

#import "SmartChat.pb.h"
#import "SCPUUID.h"
//#import "SCSettings.h" in SCPBManagerTernopol.h
#import "SCContactItem.h"
#import "SCNetFile.h"
#import "SCAddressbookContact.h"
#import "NSString+SCContains.h"


@implementation SCPBManagerTernopol (User)

/* =======================================================================================
 message GetUserByIdRequest {
 required PUUID userId = 1;
 }*/

-(void) getUserById: (id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        SCContactItem* contactItem = [params objectForKey:@"SCContactItem"];
        //        NSArray* array = [params objectForKey:@"outArray"];
        //        NSInteger index = [params objectForKey:@"index"];
        
        GetUserByIdRequestBuilder* _getUserByIdRequestBuilder = [[GetUserByIdRequestBuilder alloc] init];
        [_getUserByIdRequestBuilder setUserId:[self puuidBuilder:contactItem.userPUUID]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeGetUserById];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_getUserByIdRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
    
}

-(void) parseGetUserById:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        NSInteger type = [[params objectForKey:@"type"] integerValue];
        SCContactItem* contactItem = [params objectForKey:@"SCContactItem"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            User* usr =[GetUserByAuthResponse parseFromData:response.response].user;

            [self scContactItem:contactItem fromUser:usr];
            /*
            if(!contactItem.contactAddress)
                contactItem.contactAddress = [[SCAdress alloc] init];

            if (usr.hasNick)
                contactItem.contactNikName = usr.nick;
            
            if (usr.hasLastName)
                contactItem.contactLastName = usr.lastName;
            
            if (usr.hasFirstName)
                contactItem.contactFirstName = usr.firstName;
            
            if (usr.hasCity)
            {
                contactItem.contactAddress.city = usr.city;
            }

            if (usr.hasName)
            {
                contactItem.contactLoginName = usr.name;
            }
            if (usr.hasAbout)
            {
                contactItem.contactAbout = usr.about;
            }
            
            if (usr.hasGender)
            {
                contactItem.contactSex = usr.gender;
            }
            
            if (usr.hasFileImageId)
            {
                [contactItem.netFile setImageNetPath:[[self scpuuidBuilder:usr.fileImageId]scpuuidToString]];
            }
            
            if (usr.hasStatus)
            {
                
                contactItem.contactStatus = [self statusToSCContactStatus:usr.status];
            }
            
            if (usr.hasLastConnection)
            {
                contactItem.lastConnection = [NSDate dateWithTimeIntervalSince1970:((SInt64)usr.lastConnection/1000)];
            }
            else
                contactItem.lastConnection = nil;
            
            if (usr.hasBirthdate)
            {
                
                contactItem.contactBirthdate = [NSDate dateWithTimeIntervalSince1970:((SInt64)usr.birthdate/1000)];
            }
            else
                contactItem.contactBirthdate = nil;
            */
            
            if(type == SCPBManagerParceTypeGetUserById)
            {
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeGetProfile data:contactItem];
                }
            }
            else if(type == SCPBManagerParceTypeGetUserByIdExt)
            {
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeGetUserByIdExt data:contactItem];
                }
            }
        }
        else
        {
            FTLog(@"Error parseGetUserById response.status.success");
        }
    }
    else
        FTLog(@"Error transportPUUIDstring");
    
}
/* =======================================================================================
 SEARCH_USERS_BY_AUTH
 
 message SearchUsersByAuthRequest {
 repeated Auth auths = 1;
 
 Auth {
 required AuthType type = 1;
 required string value = 2;
 optional bool confirmed = 3;
 }
 */

-(void) searchUsersByAuthRequest: (NSMutableDictionary*) params
{
    
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeSearchUsersByAuth];
    
    SearchUsersByAuthRequestBuilder* _searchUsersByAuthRequestBuilder = [[SearchUsersByAuthRequestBuilder alloc] init];
    
    NSMutableArray* addressbook = [NSMutableArray new];
    
    if ([params isKindOfClass:[NSMutableDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"id"];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        addressbook = [params objectForKey:@"addressbook"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        for (SCAddressbookContact* addressbookContact in addressbook)
        {
            if (!addressbookContact.checked)
            {
                for (NSString* phone in addressbookContact.contactPhoneNumbers)
                {
                    AuthBuilder* auth = [[AuthBuilder alloc]init];
                    [auth setType:AuthTypePhoneAuth];
                    [auth setValue:phone];
                    [_searchUsersByAuthRequestBuilder addAuths:[auth build]];
                }
                for (NSString* email in addressbookContact.contactEmail)
                {
                    AuthBuilder* auth = [[AuthBuilder alloc]init];
                    [auth setType:AuthTypeEmailAuth];
                    [auth setValue:email];
                    [_searchUsersByAuthRequestBuilder addAuths:[auth build]];
                }
            }
        }
    }

    [_requestBuilder setRequest:[_searchUsersByAuthRequestBuilder build].data];
    
    Request * query = [_requestBuilder build];
    [_socketClient sendData:[query data]];
    
}

-(void) parseSearchUsersByAuth:(id)data
{
// удалить из _delegateDictionary парамс после всех проверок
    Response * response = data;
    if(response.status.success)
        
    {
        NSArray* contacts = [SearchUsersByAuthReponse parseFromData:response.response].auth;
                
        NSMutableArray* contactsItem = [NSMutableArray array];
        
        for (UserAuth* userAuth in contacts) {
            
            SCContactItem* contactItem = [[SCContactItem alloc]init];
            
            contactItem.userPUUID = [[SCPUUID alloc]initWithLeastSignificantBits:userAuth.user.userId.leastSignificantBits MostSignificantBits:userAuth.user.userId.mostSignificantBits];
            
            Auth* auth = userAuth.auth;
            
            if (auth.type == AuthTypePhoneAuth) {
                
                contactItem.contactPhone = auth.value;
                
                [contactItem.userAuths addObject:[[SCUserAuthItem alloc]initWithType:SCUserAuthItemTypePhone andData:auth.value andConfirm:NO]];
                
                
            } else if (auth.type == AuthTypeEmailAuth){
                
                [contactItem.userAuths addObject:[[SCUserAuthItem alloc]initWithType:SCUserAuthItemTypeEmail andData:auth.value andConfirm:NO]];
                
                contactItem.contactEmail = auth.value;
                
            }
            [contactItem.netFile setImageNetPath:[[self scpuuidBuilder:userAuth.user.fileImageId] scpuuidToString]];
            contactItem.contactNikName = userAuth.user.nickName;
            
            
            [contactsItem addObject:contactItem];
        }
        
        SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
        
        
        NSMutableDictionary* params = [_delegateDictionary objectForKey:[transportPUUID scpuuidToString]];
        
        NSMutableArray* addressbook = [params objectForKey:@"addressbook"];
        
        NSMutableArray* existContacts = [params objectForKey:@"contacts"];
        
        NSMutableArray* contactForCreat = [NSMutableArray array];
        
        if (contactsItem.count>0) {
            
            for (SCContactItem* contact in contactsItem){
                
                for(SCAddressbookContact* addressbookContact in addressbook){
                    
                    for (NSString* phone in addressbookContact.contactPhoneNumbers) {
                        
                        if ([contact.contactPhone isEqualToString:phone]&&![self existContact:contact inArray:existContacts]) {
                            
                            contact.contactFirstName = addressbookContact.contactFirstName;
                            contact.contactLastName = addressbookContact.contactLastName;
                            
                            
                            if (contact.contactNikName.length == 0) {
                                
                                NSString* fullName = [NSString new];
                                
                                if (contact.contactLastName.length > 0) {
                                    
                                    fullName = [NSString stringWithFormat:@"%@ ",contact.contactLastName];
                                    
                                }
                                
                                if (contact.contactFirstName.length > 0) {
                                    
                                    fullName = [fullName stringByAppendingString:contact.contactFirstName];
                                    
                                }
                                contact.contactNikName = fullName;
                            }
                            
                            [contactForCreat addObject:contact];
                            
                        } else if ([contact.contactPhone isEqualToString:phone]&&[self existContact:contact inArray:existContacts]) {
                            
                            addressbookContact.checked = YES;
                            
                        }
                        
                    }
                    
                    for ( NSString* email in addressbookContact.contactEmail ) {
                        
                        
                        if ([contact.contactEmail isEqualToString:email]&&![self existContact:contact inArray:existContacts]) {
                            
                            contact.contactFirstName = addressbookContact.contactFirstName;
                            contact.contactLastName = addressbookContact.contactLastName;
                            
                            if (contact.contactNikName.length == 0) {
                                
                                NSString* fullName = nil;
                                
                                if (contact.contactLastName.length > 0) {
                                    
                                    fullName = [NSString stringWithFormat:@"%@ ",contact.contactLastName];
                                    
                                }
                                
                                if (contact.contactFirstName.length > 0) {
                                    
                                    fullName = [fullName stringByAppendingString:contact.contactFirstName];
                                    
                                }
                                contact.contactNikName = fullName;
                            }

                            [contactForCreat addObject:contact];
                            
                        } else if ([contact.contactEmail isEqualToString:email]&&[self existContact:contact inArray:existContacts]){
                            
                            addressbookContact.checked = YES;
                            
                        }
                        
                    }
                    
                }
            }
            
            NSNumber* indexCreationObject = [NSNumber numberWithInt:0];
            
            SCAddressbookManager* addressbookModels = _settings.modelAddressbook;
            
            [addressbookModels saveAddressbook:addressbook];
            
            [params setObject:addressbook forKey:@"addressbook"];
            
            [params setValue:contactForCreat forKey:@"contactForCreat"];
            
            [params setValue:indexCreationObject forKey:@"indexCreationObject"];
            
            [self groupCreatesContactWithParams:params];
        }
        
    }
    else
        FTLog(@"Error parseSearchUsersByAuth response.status.success");
    
}

/* =======================================================================================
 message GetUserByAuthRequest {
 required AuthType auth_type = 1;
 required string value = 2;
 }
 */

-(void) getUserByAuth:(id) params
{
    FTLog(@"getUserByAuth");
    if([params isKindOfClass:[NSDictionary class]])
    {
        Auth* auth;
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        if([[params allKeys] containsObject:@"SCUserAuthItem"])
        {
            SCUserAuthItem* userAuth = [params objectForKey:@"SCUserAuthItem"];
            AuthBuilder *authBuilder = [[AuthBuilder alloc] init];
            [authBuilder setType:[self theAuth:userAuth.authType]];
            [authBuilder setValue:(NSString*) userAuth.data];
            auth = [authBuilder build];
        }
        else if([[params allKeys] containsObject:@"Auth"])
        {
            auth = [params objectForKey:@"Auth"];
            
        } else if([[params allKeys] containsObject:@"AuthForSearchUser"])
        {
            
            NSMutableDictionary* authDict = [params objectForKey:@"AuthForSearchUser"];
            
            if ([authDict objectForKey:@"phone"]) {
                
                AuthBuilder *authBuilder = [[AuthBuilder alloc] init];
                [authBuilder setType:AuthTypePhoneAuth];
                [authBuilder setValue:[authDict objectForKey:@"phone"]];
                auth = [authBuilder build];
                
            } else {
                
                AuthBuilder *authBuilder = [[AuthBuilder alloc] init];
                [authBuilder setType:AuthTypeEmailAuth];
                [authBuilder setValue:[authDict objectForKey:@"mail"]];
                auth = [authBuilder build];
                
            }
            
            
        }
        
        
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        GetUserByAuthRequestBuilder* getUserByAuthRequestBuilder = [[GetUserByAuthRequestBuilder alloc] init];
        [getUserByAuthRequestBuilder setAuthType:auth.type];
        [getUserByAuthRequestBuilder setValue:auth.value];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeGetUserByAuth];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[getUserByAuthRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
    
}

-(void) parseGetUserByAuth:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        NSInteger type = [[params objectForKey:@"type"] integerValue];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
            
        {
            User* usr =[GetUserByAuthResponse parseFromData:response.response].user;
//TODO:сделать с итема modelUser required PUUID id = 1; /// User id
//            SCContactItem* contactItem = [[SCContact Item alloc] initWithSettings:_settings];
//            contactItem.userPUUID = [[SCPUUID alloc] initWithLeastSignificantBits:usr.id.leastSignificantBits MostSignificantBits:usr.id.mostSignificantBits];
// TODO:
            SCContactItem* contactItem = [_settings.modelUsers makeContactItemPUUID:[self scpuuidBuilder:usr.id]];
            [self scContactItem:contactItem fromUser:usr];
            
            if(type == SCPBManagerParceTypeGetProfile)
            {
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeGetProfile data:contactItem];
                }
            }
            else
            {
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeGetUserByAuth data:contactItem];
                }
            }
        }
        else
        {
            
            NSDictionary* errorMessage = @{@"message":@"Server error, try search again"};
            
            [delegate parsedType:SCPBManagerParceTypeGetUserByAuth data:errorMessage];
            
            //            [delegate parsedType:SCPBManagerParceTypeGetUserByAuth data:nil];
            //
            //            FTLog(@"Error parseGetUserByAuth response.status.success");
        }
    }
    else {
        
        //        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        //        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        //
        //        NSDictionary* errorMessage = @{@"message":@"Server error, try search again"};
        //
        //        [delegate parsedType:SCPBManagerParceTypeGetUserByAuth data:errorMessage];
        
        FTLog(@"Error transportPUUIDstring");
    }
}

#pragma mark - invate
-(void)inviteUsersRequest:(id)data
{
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeInviteUsers];
    
    if ([data isKindOfClass:[NSMutableArray class]]) {
        
        InvitedUsersBuilder* _iviteUsersBuilde = [[InvitedUsersBuilder alloc]init];

        for (NSString* authContact in data) {
            
            if ([authContact myContainsString:@"@"]) {
                
                AuthBuilder* auth = [[AuthBuilder alloc]init];
                [auth setType:AuthTypeEmailAuth];
                [auth setValue:authContact];
                [_iviteUsersBuilde addAuths:[auth build]];
                
            } else {
                
                AuthBuilder* auth = [[AuthBuilder alloc]init];
                [auth setType:AuthTypePhoneAuth];
                [auth setValue:authContact];
                [_iviteUsersBuilde addAuths:[auth build]];
            }
            
        }
        
        InviteUsersRequestBuilder* _inviteUsersRequestBuilder = [[InviteUsersRequestBuilder alloc] init];
        
        [_inviteUsersRequestBuilder addInvitedUsers:[_iviteUsersBuilde build]];
        
        [_requestBuilder setRequest:[_inviteUsersRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
    
}

-(void) parseInviteUsersRequest:(id)data
{
    Response * response = data;
    
    if(response.status.success)
    {
        [self callDelegate:SCPBManagerParceTypeInviteUsers withData:nil];
    }
    else
    {
        
    }
}

#pragma mark - subscribtion
/* =======================================================================================
message SubscribeRequest {
    repeated PUUID user_id = 1;
}*/
-(void) subscribeRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        SubscribeRequestBuilder* _subscribeRequestBuilder;
        
        if([[params allKeys] containsObject:@"userUUID"])
        {
            _subscribeRequestBuilder = [[SubscribeRequestBuilder alloc] init];
            SCPUUID *userUUID = [params objectForKey:@"userUUID"];
            [_subscribeRequestBuilder addUserId:[self puuidBuilder:userUUID]];
        }
        else if ([[params allKeys] containsObject:@"userArray"])
        {
            _subscribeRequestBuilder = [[SubscribeRequestBuilder alloc] init];
            NSArray* arrayContacts = [params objectForKey:@"userArray"];
            for(SCContactItem* item in arrayContacts)
            {
                [_subscribeRequestBuilder addUserId:[self puuidBuilder:item.userPUUID]];
            }
        }
        
        if(_subscribeRequestBuilder)
        {
            RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
            [_requestBuilder setType:CallTypeSubscribe];
            [_requestBuilder setRequest:[_subscribeRequestBuilder build].data];
            [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
            Request * query = [_requestBuilder build];
            NSData* dataToSend = [query data];
            [_socketClient sendData:dataToSend];
        }
    }
}

-(void) parseSubscribe:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            id outData = nil;
//            FTLog(@"accessToken = %@",_settings.modelProfile.sessionID);
//            FTLog(@"logined user PUUIDString = %@",_settings.modelProfile.userID);
            if([[params allKeys] containsObject:@"userUUID"])
            {
                outData = [params objectForKey:@"userUUID"];
//                FTLog(@"add user PUUIDString = %@",[outData scpuuidToString]);
            }
            else if ([[params allKeys] containsObject:@"userArray"])
            {
                outData = [params objectForKey:@"userArray"];
//                for(SCContactItem* item in outData)
//                    FTLog(@"add user PUUIDString = %@",[item.userPUUID scpuuidToString]);
            }
//            FTLog(@"is success");
            [_settings.modelUsers addSubscribedItems:outData];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeSubscribe data:nil];
            }
        }
        else
        {
            FTLog(@"Error parseSubscribe response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}
/* =======================================================================================
message UnSubscribeRequest {
    repeated PUUID user_id = 1;
}*/

-(void) unSubscribeRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        if([[params allKeys] containsObject:@"userUUID"])
        {
            SCPUUID *userUUID = [params objectForKey:@"userUUID"];
            [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
            
            UnSubscribeRequestBuilder* _unSubscribeRequestBuilder = [[UnSubscribeRequestBuilder alloc] init];
            [_unSubscribeRequestBuilder addUserId:[self puuidBuilder:userUUID]];
            
            RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
            [_requestBuilder setType:CallTypeUnSubscribe];
            [_requestBuilder setRequest:[_unSubscribeRequestBuilder build].data];
            [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
            Request * query = [_requestBuilder build];
            NSData* dataToSend = [query data];
            
            [_socketClient sendData:dataToSend];
        }
    }
}

-(void) parseUnSubscribe:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeUnSubscribe data:nil];
            }
        }
        else
        {
            FTLog(@"Error parseUnSubscribe response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}
/* =======================================================================================
 GetAllUsersWhichISubscribe
*/
-(void) getAllUsersWhichISubscribed:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeGetAllUsersWhichISubscribed];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];
        
        [_socketClient sendData:dataToSend];
    }
}
/*
message GetAllUsersWhichISubscribedResponse {
    repeated PUUID users_ids = 1;
    }
*/
-(void) parseGetAllUsersWhichISubscribed:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            GetAllUsersWhichISubscribedResponse* getAllUsersWhichISubscribedResponse = [GetAllUsersWhichISubscribedResponse parseFromData:response.response];
            
            NSArray* subscibedUsers = getAllUsersWhichISubscribedResponse.usersIds;
            
            for(PUUID* p in subscibedUsers)
            {
                SCPUUID* pp = [self scpuuidBuilder:p];
                [_settings.modelUsers addSubscribedUserByPuuid:pp];
            }
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeUsersWhichISubscribed data:[_settings.modelUsers subscribedItems]];
            }
        }
        else
        {
            FTLog(@"Error parseUnSubscribe response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

#pragma mark - banned
/* =======================================================================================
//    required PUUID user_id = 1;
//    optional PUUID chat_room_id = 2;
*/
-(void)checkIfUserInBlackListRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCContactItem* contact = [params objectForKey:@"contact"];
//        SCContactItem* user = [_settings.modelUsers getUserByPUUID:contact.userPUUID];

        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];

        CheckIfUserInBlackListRequestBuilder* _checkIfUserInBlackListRequestBuilder = [[CheckIfUserInBlackListRequestBuilder alloc] init];
        [_checkIfUserInBlackListRequestBuilder setUserId:[self puuidBuilder:contact.userPUUID]];

        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeCheckIfUserInBlackList];
        [_requestBuilder setRequest:[_checkIfUserInBlackListRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];

        [_socketClient sendData:dataToSend];
    }
}

-(void)parseCheckIfUserInBlackListRequest:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            BOOL is_in_black_list = [CheckIfUserInBlackListResponse parseFromData:response.response].isInBlackList;
            [params setObject:[NSNumber numberWithBool:is_in_black_list]  forKey:@"blackListStatus"];
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeCheckIfUserInBlackListRequest data:params];
            }
        }
        else
        {
            FTLog(@"Error parseCheckIfUserInBlackListRequest response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

//    repeated PUUID users_ids = 1;
//    optional PUUID chat_room_id = 2;
-(void)addUsersToBlackListRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCContactItem* contact = [params objectForKey:@"contact"];
        
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        AddUsersToBlackListRequestBuilder* _addUsersToBlackListRequestBuilder = [[AddUsersToBlackListRequestBuilder alloc] init];
        [_addUsersToBlackListRequestBuilder addUsersIds:[self puuidBuilder:contact.userPUUID]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeAddUsersToBlackList];
        [_requestBuilder setRequest:[_addUsersToBlackListRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];
        
        [_socketClient sendData:dataToSend];
    }
}

-(void)parseAddUsersToBlackListRequest:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAddUsersToBlackListRequest data:nil];
            }
        }
        else
        {
            FTLog(@"Error parseAddUsersToBlackListRequest response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

//    repeated PUUID users_ids = 1;
//    optional PUUID chat_room_id = 2;
-(void)removeUsersFromBlackListRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCContactItem* contact = [params objectForKey:@"contact"];
        
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        RemoveUsersFromBlackListRequestBuilder  * _removeUsersFromBlackListRequestBuilder = [[RemoveUsersFromBlackListRequestBuilder alloc] init];
        [_removeUsersFromBlackListRequestBuilder addUsersIds:[self puuidBuilder:contact.userPUUID]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeRemoveUsersFromBlackLists];
        [_requestBuilder setRequest:[_removeUsersFromBlackListRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];
        
        [_socketClient sendData:dataToSend];
    }
}

-(void)parseRemoveUsersFromBlackListRequest:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        SCContactItem* contact = [params objectForKey:@"contact"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeRemoveUsersFromBlackListRequest data:contact];
            }
        }
        else
        {
            FTLog(@"Error parseRemoveUsersFromBlackListRequest response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}


//    optional PUUID chat_room_id = 1;
-(void)getBannedUsersRequest:(id)params
{
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeGetAllBannedUsers];
    Request * query = [_requestBuilder build];
    [_socketClient sendData:[query data]];

    
}

-(void)parseGetBannedUsersRequest:(id)data
{
    
    Response * response = data;
    if(response.status.success)
    {
        
        NSArray* contacts = [GetBannedUsersResponse parseFromData:response.response].users;
        
        NSMutableArray* contactsItem = [NSMutableArray array];
        
        for (UserShort* user in contacts ) {
            
            SCContactItem* contactItem = [[SCContactItem alloc]init];
            
            if (user.hasUserId) {
                
                contactItem.userPUUID = [[SCPUUID alloc]initWithLeastSignificantBits:user.userId.leastSignificantBits MostSignificantBits:user.userId.mostSignificantBits];

            }
            
            if (user.hasNickName) {
                contactItem.contactNikName = user.nickName;
            }
            
            if (user.hasFileImageId)
            {
                [contactItem.netFile setImageNetPath:[[self scpuuidBuilder:user.fileImageId] scpuuidToString]];
            }
            
            if (user.hasStatus)
            {
                
                contactItem.contactStatus = [self statusToSCContactStatus:user.status];
            }
            
                contactItem.user = [_settings.modelUsers getUserByPUUID:contactItem.userPUUID];
            
            [contactsItem addObject:contactItem];
        }
        
        [self callDelegate:SCPBManagerParceTypeGetBannedUsersRequest withData:contactsItem];
    }
    else
    {
        FTLog(@"Error");
    }

    
}



#pragma mark - Notifications функции нотификаций (events) СЮДЫ

-(void) parseChangeUserStatusEvent:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        ChangeUserStatusEventResponse* changeUserStatusEventResponse = [ChangeUserStatusEventResponse parseFromData:response.response];
        
        SCContactItem* _contact = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:changeUserStatusEventResponse.userId]];
        if(!_contact)
        {
            FTLog(@"бля пипец, этого не должно быть _contact = nil");
            _settings.modelProfile.contactItem.contactStatus = [self statusToSCContactStatus:changeUserStatusEventResponse.status];
        }
        else
        {
            _contact.contactStatus = [self statusToSCContactStatus:changeUserStatusEventResponse.status];
            FTLog(@"%u",_contact.contactStatus);
            if([_contact respondsToSelector:@selector(parsedType:data:)])
                [_contact parsedType:SCPBManagerParceTypeChangeStatusEvent data:_contact];
        }
    }
}

-(void) parseUpdateProfileEvent:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        UpdateProfileEvent* updateProfileEvent = [UpdateProfileEvent parseFromData:response.response];
        SCContactItem* contactItem = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:updateProfileEvent.userId]];
        UserShort* us = updateProfileEvent.userShort;
    }
}

#pragma mark - private
-(void) scContactItem:(SCContactItem*) contactItem fromUser:(User*) usr
{
//contactPUUID
//contactLoginName
//contactNikName = optional string nick = 5; /// Nickname = optional string name = 6; /// Nickname
    if(usr.hasNick)
        contactItem.contactNikName = usr.nick;
//contactFirstName = optional string first_name = 2; /// First name
    if(usr.hasFirstName)
        contactItem.contactFirstName = usr.firstName;
//contactLastName = optional string last_name = 3; /// Last name
    if(usr.hasLastName)
        contactItem.contactLastName = usr.lastName;
//contactSex = optional string gender = 10;
    if(usr.hasGender)
        contactItem.contactSex = usr.gender;
    
//contactBirthdate = optional sint64 birthdate = 13;
    if(usr.hasBirthdate)
        contactItem.contactBirthdate = [NSDate dateWithTimeIntervalSince1970:((SInt64)usr.birthdate/1000)];
    else
        contactItem.contactBirthdate = nil;
    
//netFile = optional PUUID file_image_id = 7; /// User's profile picture
    if(usr.hasFileImageId)
        [contactItem.netFile setImageNetPath:[self scpuuidStringBuilder:usr.fileImageId]];
//userAuths
//TODO: еще запрос
//contactEmail deprecated
//contactPhone deprecated
//contactLanguage
//contactTimeZone
//contactAddress
    if(contactItem.contactAddress==nil)
        contactItem.contactAddress = [[SCAdress alloc] initWithSettings:_settings];
    //country
    //postCode
    //city optional string city = 4; /// City
    if(usr.hasCity)
        contactItem.contactAddress.city = usr.city;
    //street
    //house
    //appartament
    
//contactAbout = optional string about = 9;
    if(usr.hasAbout)
        contactItem.contactAbout = usr.about;
//contactWebSite = optional string website = 12;
    if(usr.hasWebsite)
        contactItem.contactWebSite = usr.website;
//contactStatus = optional UserStatus status = 8; /// Status of user (ONLINE, AWAY, ...)
    if(usr.hasStatus)
        contactItem.contactStatus = [self statusToSCContactStatus:usr.status];
//lastConnection = optional sint64 last_connection = 11;
    if(usr.hasLastConnection)
        contactItem.lastConnection = [NSDate dateWithTimeIntervalSince1970:((SInt64)usr.lastConnection/1000)];
    
}
@end
