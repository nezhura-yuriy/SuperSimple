 //
//  SCAddressbookContact.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 26.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAddressbookContact.h"

@implementation SCAddressbookContact

- (id)init
{
    self = [super init];
    if (self) {
        
        self.contactPhoneNumbers = [NSMutableArray array];
        self.contactEmail = [NSMutableArray new];
    }
    return self;
}

-(NSDictionary*) toDictionary{
    
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    
    if (_contactFirstName) {
        [outDict setObject:_contactFirstName forKey:@"contactFirstName"];
    }
    if (_contactLastName) {
        [outDict setObject:_contactLastName forKey:@"contactLastName"];
    }
    if (_contactPhoneNumbers) {
        [outDict setObject:_contactPhoneNumbers forKey:@"contactPhoneNumbers"];
    }
    if (_contactEmail) {
        [outDict setObject:_contactEmail forKey:@"contactEmail"];
    }
    
    if (_contactFoto) {
        
        NSData *imgData= UIImageJPEGRepresentation(_contactFoto,0.0);
        
        [outDict setObject:imgData forKey:@"contactFoto"];
    }
  
    [outDict setObject:[NSNumber numberWithBool:_checked]forKey:@"checked"];

    return outDict;
}

-(void) fromDictionary:(NSDictionary*) dictionary{
    
    _contactFirstName = [dictionary objectForKey:@"contactFirstName"];
    _contactLastName = [dictionary objectForKey:@"contactLastName"];
    _contactPhoneNumbers = [dictionary objectForKey:@"contactPhoneNumbers"];
    _contactEmail = [dictionary objectForKey:@"contactEmail"];
    _checked = [[dictionary objectForKey:@"checked"] boolValue];
    _contactFoto = [UIImage imageWithData:[dictionary objectForKey:@"contactFoto"]];

}

@end
