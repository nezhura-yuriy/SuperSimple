//
//  SCTextViewController.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCTextViewController.h"

static NSString *kPrivacyPolicyFileName = @"textPrivacyPolicy";
static NSString *kTermsOfUseFileName = @"textTermsOfUse";
static NSString *kAboutFileName = @"textAbout";

@implementation SCTextViewController
{
    NSString* _title;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *fileName = [[NSString alloc] init];
    
    switch (_viewController) {
        case SCViewControllerPrivacyPolicy:
        {
            fileName = kPrivacyPolicyFileName;
            _title = @"PrivacyPolicy";
        }
            break;
        
        case SCViewControllerTermsOfUse:
        {
            fileName = kTermsOfUseFileName;
            _title = @"Terms of use";
        }
            break;
         
        case SCViewControllerAbout:
        {
            fileName = kAboutFileName;
            _title = @"About";
        }
            break;
            
        default:
        {
            fileName = @"";
            _title = @"";
        }
            break;
    }
    
    [self setupNavigation];
    _webView.delegate = self;
    
    if (fileName.length > 0) {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:fileName ofType:@"html"];
        
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        
        [_webView loadHTMLString:htmlString baseURL:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

- (void) setViewController:(SCViewController)viewController
{
    _viewController = viewController;
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _webView.frame = self.view.frame;
}

//MARK: Setup
- (void) setupNavigation
{
    //Nav Title
    self.title = _title;
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    
    //Back button
    UIImage *backButtonImage = [_settings.skinManager getImageForKeyX:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

 //MARK: Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

@end
