//
//  SCMainTabBarViewController.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 06.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSBProfileViewController.h"

@class SCSettings;

@interface SCMainTabBarViewController : UITabBarController <UITabBarControllerDelegate, SCSBProfileViewControllerProtocol>

@property (nonatomic,strong) SCSettings* settings;

@end
