//
//  SCPBManagerTernopol+Contacts.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol.h"

@interface SCPBManagerTernopol (Contacts)


- (void) parseCreateContact:(id)data;
- (void) parseRemoveContact:(id)data;
- (void) parseGetAllContactFields:(id)data;
- (void) parseSetFields:(id)data;
- (void) parseRemoveField:(id)data;
- (void) parseGetAllContacts:(id)data;

@end
