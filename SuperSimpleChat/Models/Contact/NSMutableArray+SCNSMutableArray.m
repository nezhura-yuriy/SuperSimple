//
//  NSMutableArray+SCNSMutableArray.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 25.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "NSMutableArray+SCNSMutableArray.h"
#import "SCList.h"
#import "SCListItem.h"

@implementation NSMutableArray (SCNSMutableArray)

- (BOOL)isEqualToMyArray:(NSMutableArray *)array {
    
    if (!array || [self count] != [array count]) {
        
        return NO;
    }
    
    for (NSUInteger idx = 0; idx < [array count]; idx++) {
        
        if (![self[idx] isEqual:array[idx]]) {
            
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)isEqualToListsArray:(NSMutableArray *)array
{
    if (!array || self.count != array.count) {
        return NO;
    }
    
    for (NSUInteger index = 0; index < array.count; index++) {
        
        SCList *_selfList = self[index];
        SCList *_arrayList = array[index];
        
        if ([_selfList respondsToSelector:@selector(isEqualList:)]) {
            if (![_selfList isEqualList:_arrayList]) {
                return NO;
            }
        }
    }
    return YES;
}

- (BOOL)isEqualToListItemsArray:(NSMutableArray *)array
{
    if (!array || self.count != array.count) {
        return NO;
    }
    
    for (NSUInteger index = 0; index < array.count; index++) {
        
        SCListItem *_selfItem = self[index];
        SCListItem *_arrayItem = array[index];
        
        if ([_selfItem respondsToSelector:@selector(isEqualItem:)] &&
            ![_selfItem isEqualItem:_arrayItem]) {
                return NO;
        }
    }
    return YES;
}

- (NSMutableArray *)mutableCopyListItem
{
    NSMutableArray *copyArray = [NSMutableArray new];
    
    if (self.count > 0) {
        
        if ([[self objectAtIndex:0] isKindOfClass:[SCListItem class]]) {
            
            for (SCListItem *item in self) {
                SCListItem *itemNew = [[SCListItem alloc] initWithItem:item];
                [copyArray addObject:itemNew];
            }
        }
    }
    return copyArray;
}

@end
