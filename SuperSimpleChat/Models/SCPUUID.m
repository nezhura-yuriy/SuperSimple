//
//  SCPUUID.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 05.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPUUID.h"

@implementation SCPUUID

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (id)initFromString:(NSString*) scpuuidStr
{
    self = [super init];
    if (self)
    {
        _nsuuid = [[NSUUID alloc] initWithUUIDString:scpuuidStr];
        unsigned char ee[16];
        [_nsuuid getUUIDBytes:ee];
        
        SInt64 outL;
        outL =   ((SInt64)(ee[15] & 0xff));
        outL |=  ((SInt64)(ee[14] & 0xff)) << 8 ;
        outL |=  ((SInt64)(ee[13] & 0xff)) << 16 ;
        outL |=  ((SInt64)(ee[12] & 0xff)) << 24 ;
        outL |=  ((SInt64)(ee[11] & 0xff)) << 32 ;
        outL |=  ((SInt64)(ee[10] & 0xff)) << 40 ;
        outL |=  ((SInt64)(ee[9] & 0xff)) << 48 ;
        outL |=  ((SInt64)(ee[8] & 0xff)) << 56;
        
        SInt64 outM;
        outM =  ((SInt64)(ee[7] & 0xff));
        outM |= ((SInt64)(ee[6] & 0xff)) << 8 ;
        outM |= ((SInt64)(ee[5] & 0xff)) << 16 ;
        outM |= ((SInt64)(ee[4] & 0xff)) << 24 ;
        outM |= ((SInt64)(ee[3] & 0xff)) << 32 ;
        outM |= ((SInt64)(ee[2] & 0xff)) << 40 ;
        outM |= ((SInt64)(ee[1] & 0xff)) << 48 ;
        outM |= ((SInt64)(ee[0] & 0xff)) << 56;
        
        _leastSignificantBits = outL;
        _mostSignificantBits = outM;
    }
    return self;
}

- (id) initWithLeastSignificantBits:(SInt64) leastSignificantBits MostSignificantBits:(SInt64) mostSignificantBits
{
    self = [super init];
    if (self)
    {
        _leastSignificantBits = leastSignificantBits;
        _mostSignificantBits = mostSignificantBits;
        
        [self makeSelfs];
        /*
        unsigned char dd[16];
        dd[0]=(Byte)(_mostSignificantBits>>56);//most;
        dd[1]=(Byte)(_mostSignificantBits>>48);//(most>>8);//
        dd[2]=(Byte)(_mostSignificantBits>>40);//(most>>16);
        dd[3]=(Byte)(_mostSignificantBits>>32);//(most>>24);
        dd[4]=(Byte)(_mostSignificantBits>>24);//(most>>32);
        dd[5]=(Byte)(_mostSignificantBits>>16);//(most>>40);
        dd[6]=(Byte)(_mostSignificantBits>>8); //(most>>48);
        dd[7]=(Byte)(_mostSignificantBits);    //(most>>56);
        dd[8]=(Byte)(_leastSignificantBits>>56);//least;
        dd[9]=(Byte)(_leastSignificantBits>>48);//(least>>8);
        dd[10]=(Byte)(_leastSignificantBits>>40);//(least>>16);
        dd[11]=(Byte)(_leastSignificantBits>>32);//(least>>24);
        dd[12]=(Byte)(_leastSignificantBits>>24);//(least>>32);
        dd[13]=(Byte)(_leastSignificantBits>>16);//(least>>40);
        dd[14]=(Byte)(_leastSignificantBits>>8);//(least>>48);
        dd[15]=(Byte)(_leastSignificantBits);//(least>>56);//
        _nsuuid = [[NSUUID alloc] initWithUUIDBytes:dd];
        */
    }
    return self;

}

- (id) initWithRandom {
    
    self = [super init];
    if (self) {
        
        CFUUIDRef uuidRef = CFUUIDCreate(NULL);
        CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
        CFRelease(uuidRef);
        
        _nsuuid = [[NSUUID alloc] initWithUUIDString:(__bridge_transfer NSString *)uuidStringRef];
        unsigned char ee[16];
        [_nsuuid getUUIDBytes:ee];
//        CFRelease(uuidStringRef);
        
        SInt64 outL;
        outL =   ((SInt64)(ee[15] & 0xff));
        outL |=  ((SInt64)(ee[14] & 0xff)) << 8 ;
        outL |=  ((SInt64)(ee[13] & 0xff)) << 16 ;
        outL |=  ((SInt64)(ee[12] & 0xff)) << 24 ;
        outL |=  ((SInt64)(ee[11] & 0xff)) << 32 ;
        outL |=  ((SInt64)(ee[10] & 0xff)) << 40 ;
        outL |=  ((SInt64)(ee[9] & 0xff)) << 48 ;
        outL |=  ((SInt64)(ee[8] & 0xff)) << 56;
        
        SInt64 outM;
        outM =  ((SInt64)(ee[7] & 0xff));
        outM |= ((SInt64)(ee[6] & 0xff)) << 8 ;
        outM |= ((SInt64)(ee[5] & 0xff)) << 16 ;
        outM |= ((SInt64)(ee[4] & 0xff)) << 24 ;
        outM |= ((SInt64)(ee[3] & 0xff)) << 32 ;
        outM |= ((SInt64)(ee[2] & 0xff)) << 40 ;
        outM |= ((SInt64)(ee[1] & 0xff)) << 48 ;
        outM |= ((SInt64)(ee[0] & 0xff)) << 56;
        
        _leastSignificantBits = outL;
        _mostSignificantBits = outM;
    }
    
    return self;
}

-(void) dealloc
{
    
}

-(void) setLeastSignificantBits:(SInt64)leastSignificantBits
{
    _leastSignificantBits = leastSignificantBits;
    [self makeSelfs];
}

-(void) setMostSignificantBits:(SInt64)mostSignificantBits
{
    _mostSignificantBits = mostSignificantBits;
    [self makeSelfs];
}

-(BOOL) isEqual:(SCPUUID*)object
{
    if(_mostSignificantBits == object.mostSignificantBits && _leastSignificantBits == object.leastSignificantBits)
        return YES;
    else
        return NO;
}

-(BOOL) isEmpty
{
    if(_leastSignificantBits != 0 && _mostSignificantBits != 0)
        return NO;
    else
        return YES;
}

-(void) makeSelfs
{
//    if(_leastSignificantBits != 0 && _mostSignificantBits != 0)///???
    {
        unsigned char dd[16];
        dd[0]=(Byte)(_mostSignificantBits>>56);//most;
        dd[1]=(Byte)(_mostSignificantBits>>48);//(most>>8);//
        dd[2]=(Byte)(_mostSignificantBits>>40);//(most>>16);
        dd[3]=(Byte)(_mostSignificantBits>>32);//(most>>24);
        dd[4]=(Byte)(_mostSignificantBits>>24);//(most>>32);
        dd[5]=(Byte)(_mostSignificantBits>>16);//(most>>40);
        dd[6]=(Byte)(_mostSignificantBits>>8); //(most>>48);
        dd[7]=(Byte)(_mostSignificantBits);    //(most>>56);
        dd[8]=(Byte)(_leastSignificantBits>>56);//least;
        dd[9]=(Byte)(_leastSignificantBits>>48);//(least>>8);
        dd[10]=(Byte)(_leastSignificantBits>>40);//(least>>16);
        dd[11]=(Byte)(_leastSignificantBits>>32);//(least>>24);
        dd[12]=(Byte)(_leastSignificantBits>>24);//(least>>32);
        dd[13]=(Byte)(_leastSignificantBits>>16);//(least>>40);
        dd[14]=(Byte)(_leastSignificantBits>>8);//(least>>48);
        dd[15]=(Byte)(_leastSignificantBits);//(least>>56);//
        _nsuuid = [[NSUUID alloc] initWithUUIDBytes:dd];
    }
}

-(NSString*) scpuuidToString
{
    if(!_nsuuid)
        //{
        [self makeSelfs];
    /*
        unsigned char dd[16];
        dd[0]=(Byte)(_mostSignificantBits>>56);//most;
        dd[1]=(Byte)(_mostSignificantBits>>48);//(most>>8);//
        dd[2]=(Byte)(_mostSignificantBits>>40);//(most>>16);
        dd[3]=(Byte)(_mostSignificantBits>>32);//(most>>24);
        dd[4]=(Byte)(_mostSignificantBits>>24);//(most>>32);
        dd[5]=(Byte)(_mostSignificantBits>>16);//(most>>40);
        dd[6]=(Byte)(_mostSignificantBits>>8); //(most>>48);
        dd[7]=(Byte)(_mostSignificantBits);    //(most>>56);
        dd[8]=(Byte)(_leastSignificantBits>>56);//least;
        dd[9]=(Byte)(_leastSignificantBits>>48);//(least>>8);
        dd[10]=(Byte)(_leastSignificantBits>>40);//(least>>16);
        dd[11]=(Byte)(_leastSignificantBits>>32);//(least>>24);
        dd[12]=(Byte)(_leastSignificantBits>>24);//(least>>32);
        dd[13]=(Byte)(_leastSignificantBits>>16);//(least>>40);
        dd[14]=(Byte)(_leastSignificantBits>>8);//(least>>48);
        dd[15]=(Byte)(_leastSignificantBits);//(least>>56);//
        NSUUID* aa = [[NSUUID alloc] initWithUUIDBytes:dd];
    */
        return [[_nsuuid UUIDString] lowercaseString];
//    }
//        else
//    {
//            return @"";
//    }
}

-(NSString*) description
{
    
    return [NSString stringWithFormat:@"\nleastSBits=%lld,\nmostSBits=%lld\nPUUIDString=%@",_leastSignificantBits,_mostSignificantBits, [self scpuuidToString]];
}

@end
