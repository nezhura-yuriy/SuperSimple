//
//  SCSBTextFieldTableViewCell.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"
#import "SCTextField.h"

typedef NS_ENUM(NSInteger, SCTypeFieldProfile) {
    SCTypeFieldProfileAbout = 0,
    SCTypeFieldProfileFirstName,
    SCTypeFieldProfileLastName
};

@protocol SCTextFieldCellProtocol <NSObject>
- (void) changeTextValue:(NSString*)textValue for:(SCTypeFieldProfile) typeFieldProfile;
- (void) showKeyboard;
- (void) hideKeyboard;
@end

@interface SCSBTextFieldTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *icoImageView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic) SCTypeFieldProfile typeFieldProfile;

@property (nonatomic, weak) id <SCTextFieldCellProtocol> delegate;

@property (nonatomic, strong) SCSettings *settings;
@property (nonatomic) UIEdgeInsets edgeInsets;

@end
