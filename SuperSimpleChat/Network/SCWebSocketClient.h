//
//  SCWebSocketClient.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 06.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SRWebSocket.h>
#import "SCModelsDelegates.h"

@class SCSettings;

@interface SCWebSocketClientType : NSObject
@property(nonatomic,assign) NSInteger type;
@property(nonatomic,weak) id<SCWebSocketClientDelegate> delegate;
@end

@interface SCWebSocketClient : NSObject <SRWebSocketDelegate>


@property (nonatomic,strong) SCSettings* settings;
@property (strong, nonatomic) NSString* userID;
@property (nonatomic,assign) BOOL isConnected;

- (id) initWithSettings:(SCSettings*) settings;

-(void) addDelegate:(id<SCWebSocketClientDelegate>) delegate;
-(void) delDelegate:(id<SCWebSocketClientDelegate>) delegate;

- (void) connectWebSocket;
- (void) disconnectWebSocket;
- (void) sendData:(NSData*) data;
@end
