//
//  SCPBManagerTernopol.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 02.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
/*
 https://gitlab.com/Bischak/smartchat-backend-api/blob/master/interface/SmartChat.proto
 */

#import "SCPBManagerTernopol.h"
#import "SCPBManagerTernopol(Converts).h"
#import "SCPBManagerTernopol(Profile).h"
#import "SCPBManagerTernopol(User).h"
#import "SCPBManagerTernopol(Lists).h"
#import "SCPBManagerTernopol(Contacts).h"
#import "SCPBManagerTernopol(ChatRooms).h"
#import "SCPBManagerTernopol(Messages).h"

#import "SCSettings.h"
#import "SmartChat.pb.h"
#import "SCPUUID.h"

//#import "SCModelLists.h"
#import "SCList.h"
#import "SCModelChats.h"
#import "SCModelUser.h"
#import "SCListItem.h"
#import "SCChatItem.h"
#import "SCContactItem.h"
#import "SCMessageItem.h"
#import "SCMessageBodyItem.h"
#import "SCUserAuthItem.h"

#import "SCPrints.h"

@implementation SCPBManagerTernopol
{
    UIAlertView* alertError;
    
}

// Запросы Профиль
#pragma mark - Profile operations
//TODO

-(void) profileSetWithContactwithParam:(id)params{
    
    
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeSetProfileField];
        SetProfileFieldRequestBuilder *fieldProfiles = [[SetProfileFieldRequestBuilder alloc]init];
        
        SCContactItem * item = [params objectForKey:@"contactItem"];
        
        NSArray* fieldsArray = [self fieldsFromContact:item];
        [fieldProfiles setFieldsArray:fieldsArray];
        [_requestBuilder setRequest:[fieldProfiles build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
    
}




-(BOOL)existContact:(SCContactItem*)existContact inArray:(NSMutableArray*) array
{
    
    for (SCContactItem* contact in array) {
        
        if ([existContact.userPUUID isEqual:contact.userPUUID]) {
            
            return YES;
        }
        
    }
    
    return NO;
}


-(void)groupCreatesContactWithParams:(NSMutableDictionary*) params
{
    
    NSMutableArray* contactForCreat = [params objectForKey:@"contactForCreat"];
    
    int indexCreationObject = [[params objectForKey:@"indexCreationObject"]integerValue];
    
    if (contactForCreat.count>indexCreationObject) {
        
        SCContactItem* contact = contactForCreat[indexCreationObject];
        
        SCPUUID *transportPUUIDForGetParams = [params objectForKey:@"id"];
        
        SCPUUID *transportPUUIDForSetFields = [[SCPUUID alloc] initWithRandom];
        
        NSDictionary* paramsForCreat = @{@"fields":contact,@"id":transportPUUIDForGetParams, @"idFilds":transportPUUIDForSetFields};
        
        indexCreationObject++;
        
        [params setObject:[NSNumber numberWithInt:indexCreationObject] forKey:@"indexCreationObject"];
        
        [_delegateDictionary setObject:params forKey:[transportPUUIDForGetParams scpuuidToString]];
        
        [self createContactRequestWithParams:[NSMutableDictionary dictionaryWithDictionary:paramsForCreat]];
        
        
    } else {
        
        [self getAllContactsRequestWithParams:nil];
        
        [_delegateDictionary removeObjectForKey:[[params objectForKey:@"id"] scpuuidToString]];
        
    }
    
}

//MARK: List and List's Item Requests & Parse
//MARK: Request Chat Rooms
#pragma mark - messages
#pragma mark - Contacts



#pragma mark SCWebSocketClientDelegate <NSObject>
-(void) clientSocketDidReceiveData:(id) dataIn
{
    
    Response * response = [Response parseFromData:dataIn];
    FTLog(@"response = %@",NSStringCallType(response.type));//[SCPBManagerTernopol textFromType:response.type]);
    if(!response.status.success)
    {
        FTLog(@"=========== Error: %@", response.status.error.pb_description);
        switch (response.type)
        {
            case CallTypeLastReadMessageDateGet:
            case CallTypeAuthAdd:
            case CallTypeAuthGet:
            case CallTypeAuthConfirm:
            case CallTypeAuthRemove:
            case CallTypeGetUserByAuth:
            case CallTypeAuthorizationFailedEvent:
                break;
                
            default:
            {
                alertError = [[UIAlertView alloc] initWithTitle:@"Error" message:response.status.error.pb_description delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alertError show];
            }
                break;
        }
    }
    
    switch (response.type)
    {
//Profile
        case CallTypeProfileGet:[self parseProfileGet:response];break;
        case CallTypeSetProfileField:[self parseSetProfileField:response];break;
        case CallTypeRemoveProfileField:[self parseRemoveProfileField:response];break;
        case CallTypeAuthGet:[self parseAuthGet:response];break;
        case CallTypeAuthAdd: [self parseAuthAdd:response]; break;
        case CallTypeAuthRemove: [self parseAuthRemove:response]; break ;
        case CallTypeAuthConfirm: [self parseAuthConfirm:response]; break;
        case CallTypeUserUpdateStatus:[self parseChangeStatus:response];break;
        case CallTypeCheckIsNicknameFree:[self parseCheckIsNicknameFree:response];break;
        case CallTypeAuthorizationFailedEvent:[self parseAuthorizationFailedEvent:response];break;
        case CallTypeSubscribeToAmazonSns:[self parseSubscribeToAmazonSNS:response];break;
        case CallTypeUnsubscribeFromAmazonSns:[self parseUnsubscribeFromAmazonSns:response];break;
// Users
        case CallTypeGetUserByAuth:[self parseGetUserByAuth:response];break;
        case CallTypeGetUserById:[self parseGetUserById:response];break;
        case CallTypeSearchUsersByAuth:[self parseSearchUsersByAuth:response];break;
        case CallTypeSubscribe:[self parseSubscribe:response];break;
        case CallTypeUnSubscribe:[self parseUnSubscribe:response];break;
        case CallTypeGetAllUsersWhichISubscribed:[self parseGetAllUsersWhichISubscribed:response];break;
        case CallTypeInviteUsers: [self parseInviteUsersRequest:response];break;
        case CallTypeCheckIfUserInBlackList: [self parseCheckIfUserInBlackListRequest:response];break;
        case CallTypeAddUsersToBlackList: [self parseAddUsersToBlackListRequest:response];break;
        case CallTypeRemoveUsersFromBlackLists: [self parseRemoveUsersFromBlackListRequest:response]; break;
        case CallTypeGetAllBannedUsers: [self parseGetBannedUsersRequest:response];break;
        case CallTypeUserUpdateStatusEvent:[self parseChangeUserStatusEvent:response];break;
        case CallTypeUpdateProfileEvent:[self parseUpdateProfileEvent:response];break;
            
// Lists
        case CallTypeListGet:[self parseGetList:response];break;
//        case CallTypeListsGet:[self parseGetLists:response];break;
//        case CallTypeListAdd:[self parseAddList:response];break;
//        case CallTypeListUpdate:[self parseUpdateList:response];break;
//        case CallTypeListRemove:[self parseRemoveList:response];break;
//        case CallTypePublicListAdd:[self parseAddPublicList:response];break;
//        case CallTypePublicListGet:[self parseGetPublicLists:response];break;
        case CallTypeListItemAdd:[self parseAddListItem:response];break;
//        case CallTypeListItemGet:[self parseGetListItem:response];break;
        case CallTypeListItemsGet:[self parseGetListItems:response];break;
        case CallTypeListItemUpdate:[self parseUpdateListItem:response];break;
        case CallTypeListItemRemove:[self parseRemoveListItem:response];break;
        case CallTypeListItemsSetOrder: [self parseSetListItemsOrder:response];break;
            
// ChatRoom
        case CallTypeCreateChatroom:[self parseCreateChatroom:response];break;
        case CallTypeRemoveChatroom:[self parseRemoveChatroom:response];break;
        case CallTypeMakeChatRoomActive:[self parseChatRoomActivate:response];break;
        case CallTypeChangeInfoChatroom:[self parseChangeChatRoom:response];break;
        case CallTypeGetAllUsersForRoom:[self parseGetAllUsersForRooms:response];break;
        case CallTypeGetAllRoomsForUser:[self parseGetAllRoomsForUser:response];break;
        case CallTypeSearchPublicChatRooms:[self parseSeachPublicChatRooms:response];break;
        case CallTypeJoinUserToRoom:[self parseJoinUserToRoom:response];break;
        case CallTypeJoinUsersToRoom:[self parseJoinUsersToRoom:response];break;
        case CallTypeLeaveUserFromRoom:[self parseLeaveUserFromRoom:response];break;
        case CallTypeChatroomGetById:[self parseGetChatRoomByIdResponse:response];break;
        case CallTypeLastReadMessageDateGet: [self parseGetLastReadMessageDate:response];break;
        case CallTypeLastReadMessageDateSet: [self parseSetLastReadMessageDate:response];break;
        case CallTypeChangeInfoChatroomEvent:[self parseChangeRoomEventResponse:response];break;
        case CallTypeJoinUserToRoomEvent:[self parseJoinRoomEventResponse:response];break;
        case CallTypeLeaveUserFromRoomEvent:[self parseLeaveRoomEventResponse:response];break;
        case CallTypeRemoveChatroomEvent:[self parseRemoveRoomEventResponse:response];break;
            
// Messages
        case CallTypeSendMessage:[self parseSendMessage:response];break;
        case CallTypeMessage:[self parseReceiveMessage:response];break;
        case CallTypeChatRoomHistoryGet:[self parseGetChatRoomHistory:response];break;
        case CallTypeChatRoomHistoryGetWithTimeRange:[self parseGetChatRoomHistoryWithTimeRangeResponse:response];break;
        case CallTypeRemoveMessage:[self parseRemoveMessage:response];break;
        case CallTypeUpdateMessage:[self parseUpdateMessage:response];break;
//        case CallTypeRefreshedMessageEvent:[self parseRefreshedMessageEvent:response];break;
        case CallTypeRemoveMessageFromChatroomEvent:[self parseRemoveMessageFromChatRoomEvent:response];break;
        case CallTypeUpdateMessageInChatroomEvent:[self parseUpdateMessageInChatroomEvent:response];break;
//Contacts
        case CallTypeContactCreate:[self parseCreateContact:response];break;
        case CallTypeContactRemove:[self parseRemoveContact:response];break;
        case CallTypeContactFieldsGet:[self parseGetAllContactFields:response];break;
        case CallTypeContactSetFields:[self parseSetFields:response];break;
        case CallTypeContactRemoveField:[self parseRemoveField:response];break;
        case CallTypeContactsGet:[self parseGetAllContacts:response];break;
            
        default:
            FTLog(@"SCPBManagerTernopol : clientSocketDidReceiveData default case");
            break;
    }
    
    
}

-(NSDictionary*) makeErrorDictionary:(Response*) response
{
    NSDictionary* errDict;
    if(response.hasStatus && response.status.hasError)
    {
        NSString* errDescription = @"";
        NSInteger errCode = 0;
        if(response.status.error.hasCode)
            errCode = (NSInteger)response.status.error.code;
        if(response.status.error.hasDescription)
            errDescription = response.status.error.pb_description;

        errDict = [[NSDictionary alloc] initWithObjectsAndKeys:errDescription,@"ErrorDescription", [NSNumber numberWithInteger:errCode], @"ErrorCode", nil];
    }
    
    return errDict;
}

@end
