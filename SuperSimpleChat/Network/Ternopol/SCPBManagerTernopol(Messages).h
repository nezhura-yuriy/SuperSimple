//
//  SCPBManagerTernopol+Messages.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol.h"

@interface SCPBManagerTernopol (Messages)

-(void) parseSendMessage:(id)data;
-(void) parseUpdateMessage:(id) data;
-(void) parseRemoveMessage:(id) data;
//-(void) parseHistory:(id)data;
-(void) parseGetChatRoomHistory:(id)data;
-(void) parseGetChatRoomHistoryWithTimeRangeResponse:(id)data;
#pragma mark - notifications
-(void) parseReceiveMessage:(id)data;
-(void) parseRefreshedMessageEvent:(id)data;
-(void) parseUpdateMessageInChatroomEvent:(id)data;
-(void) parseRemoveMessageFromChatRoomEvent:(id) data;
@end
