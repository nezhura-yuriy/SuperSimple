//
//  SCUserGroup.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"

@interface SCContactGroup : SCAbstractItem

@property(nonatomic,strong) NSString* grContID;
@property(nonatomic,strong) NSString* grContParentID;
@property(nonatomic,strong) NSString* grContName;
@property(nonatomic,strong) NSString* grContImageLink;

@end
