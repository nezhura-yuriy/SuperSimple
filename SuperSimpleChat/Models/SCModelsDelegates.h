//
//  SCModelsDelegates.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 21.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SCTypes.h"

@class SCContactItem;

// SCWebSocketClientDelegate ====================================================================
@protocol SCWebSocketClientDelegate <NSObject>

@optional
-(void) clientSocketDidOpen;
-(void) clientSocketDidFailWithError:(NSError *)error;
-(void) clientSocketDidCloseWithCode:(NSError *)error;
-(void) clientSocketDidReceiveData:(id) dataIn;

@end



// SCPBManagerDelegate ==========================================================================

typedef enum
{
    SCPBManagerParceTypeNone,
    // profile
    SCPBManagerParceTypeGetProfile,
    SCPBManagerParceTypeSetProfileField,
    SCPBManagerParceTypeRemoveProfileField,
    SCPBManagerParceTypeCheckIsNicknameFree,
    SCPBManagerParceTypeChangeStatus,
    SCPBManagerParceTypePrepareLogout,
    SCPBManagerParceTypeLogout,
    SCPBManagerParceTypeAuthorizationFailedEvent,
    SCPBManagerParceTypeChangeStatusEvent,
    SCPBManagerParceTypeUpdateProfileEvent,
    SCPBManagerParceTypeSubscribeToAmazonSNS,
    SCPBManagerParceTypeUnsubscribeFromAmazonSNS,
    
    SCPBManagerParceTypeAuthAdd,
    SCPBManagerParceTypeAuthConfirm,
    SCPBManagerParceTypeAuthRemove,
    SCPBManagerParceTypeAuthGet,
    // user
    SCPBManagerParceTypeGetUserByAuth,
    SCPBManagerParceTypeGetUserById,
    SCPBManagerParceTypeGetUserByIdExt,
    SCPBManagerParceTypeGetUserByIdContact,
    SCPBManagerParceTypeSubscribe,
    SCPBManagerParceTypeUnSubscribe,
    SCPBManagerParceTypeUsersWhichISubscribed,
    SCPBManagerParceTypeCheckIfUserInBlackListRequest,
    SCPBManagerParceTypeAddUsersToBlackListRequest,
    SCPBManagerParceTypeRemoveUsersFromBlackListRequest,
    SCPBManagerParceTypeGetBannedUsersRequest,

    // lists
    SCPBManagerParceTypeGetLists,
    SCPBManagerParceTypeAddList,
    SCPBManagerParceTypeGetList,
    SCPBManagerParceTypeUpdateList,
    SCPBManagerParceTypeRemoveList,
    SCPBManagerParceTypeAddPublicList,
    SCPBManagerParceTypeGetPublicLists,
    SCPBManagerParceTypeAddListItem,
    SCPBManagerParceTypeGetListItem,
    SCPBManagerParceTypeGetListItems,
    SCPBManagerParceTypeUpdateListItem,
    SCPBManagerParceTypeRemoveListItem,
    SCPBManagerParceTypeSetListItemsOrder,
    // chat
    SCPBManagerParceTypeCreateChat,
    SCPBManagerParceTypeGetChatRoomByIdRequest,
    SCPBManagerParceTypeSearchCreateChat,
    SCPBManagerParceTypeChatRoomGetById,
    SCPBManagerParceTypeRemoveChat,
    SCPBManagerParceTypeActivateChat,
    SCPBManagerParceTypeChangeChat,
    SCPBManagerParceTypeGetAllRooms,
    SCPBManagerParceTypeGetAllRoomsForUser,
    SCPBManagerParceTypeSearchPublicChatRooms,
    SCPBManagerParceTypeGetAllUsersForRoom,
    SCPBManagerParceTypeLeaveUserFromRoom,
    SCPBManagerParceTypeJoinUserToRoom,
    SCPBManagerParceTypeGetHistory,
    SCPBManagerParceTypeGetChatRoomHistory,
    SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange,
    SCPBManagerParceTypeGetLastReadMessageDate,
    SCPBManagerParceTypeSetLastReadMessageDate,
    SCPBManagerParceTypeChangeRoomEvent,
    SCPBManagerParceTypeJoinUserToRoomEvent,
    SCPBManagerParceTypeCallTypeLeaveUserFromRoomEvent,
    SCPBManagerParceTypeCallTypeRemoveRoomEvent,
    // messages
    SCPBManagerParceTypeSendMessage,
    SCPBManagerParceTypeUpdateMessage,
    SCPBManagerParceTypeRemoveMessage,
    SCPBManagerParceTypeRefreshMessage,
    SCPBManagerParceTypeMessage,
    SCPBManagerParceTypeRefreshedMessageEvent,
    SCPBManagerParceTypeRemoveMessageFromChatroomEvent,
    SCPBManagerParceTypeUpdateMessageInChatroomEvent,
    // contacts
    SCPBManagerParceTypeContactCreate,
    SCPBManagerParceTypeContactRemove,
    SCPBManagerParceTypeGetAllContacts,
    SCPBManagerParceTypeGetAllContactFields,
    SCPBManagerParceTypeContactSetFields,
    SCPBManagerParceTypeSearchUsersByAuth,
    SCPBManagerParceTypeUpdateAddressBook,
    SCPBManagerParceTypeInviteUsers,
    
    SCPBManagerParceTypeInternalMessageItemTickDestroy,
    SCPBManagerParceTypeInternalChatItemTickDestroy,
} SCPBManagerParceType;

@protocol SCPBManagerDelegate <NSObject>

@optional
-(void) parsedType:(SCPBManagerParceType) type data:(id) data;
@end


// SCSettingsDelegate ==========================================================================
@protocol SCSettingsDelegate <NSObject>

-(void) didLoadProfile;
-(void) didUpdateProfile;
@end


// SCNetFileDelegate ========================================================================

@class SCNetFile;

@protocol SCNetFileDelegate <NSObject>

@optional
-(void) setStatus:(SCNetFileStatus) status;
-(void) setProgressPercent:(CGFloat) persents;
-(void) isCompleet:(SCNetFile*) netFile;
-(void) loadVideoFragment:(UIImage*) videoFragment;
@end


// SCMediaCaptureDelegate ========================================================================

@protocol SCMediaCaptureDelegate <NSObject>

@optional
-(void) fileCaptured:(NSString*) fileName forType:(SCDataType) type;
@end



// SCHTTPManagerDelegate ========================================================================
@class SCNetError;

@protocol SCHTTPManagerDelegate <NSObject>

@optional
-(void) onSuccess:(NSString*) tmpfile;
-(void) onFailure:(SCNetError*) scNetError;
-(void) onChangeStatus:(SCNetFileStatus) status;
@end



// SCModelAbstarctDelegate ========================================================================
@protocol SCModelAbstarctDelegate <NSObject>

@optional
-(void) showError:(NSDictionary*) errorDictionary;
@end




//* не используем
// SCModelChatsDelegate ========================================================================
@protocol SCModelChatsDelegate <SCModelAbstarctDelegate>

@optional
-(void) didAddItemToModelChatsList:(id) respond;
-(void) didChangeItemInModelChatsList:(id) respond;
-(void) didRemoveItemFromModemChatsList:(id) respond;
-(void) didSearchItemInModelChatsList:(id) respond;
-(void) didChangeModelChatItem;

@end


//* не используем
// SCChatItemDelegate ===========================================================================
@protocol SCChatItemDelegate <SCModelAbstarctDelegate>

@optional
-(void) itemUpdate;
-(void) loadedMessageItems:(NSArray*) newItems;
-(void) loadedGroupsMessage:(NSArray*) changedGroups;
//-(void) userAuthUpdate:(NSArray*) userAuthes;
//-(void) statusChange:(SCSt)
@end
//*/

/* не используем
// SCChatItemDelegate ===========================================================================
@protocol SCMessageItemDelegate <SCModelAbstarctDelegate>

@optional
-(void) messageLoad;
@end
*/


// SCMessageBodysDelegate =======================================================================
@protocol SCMessageBodysDelegate <SCModelAbstarctDelegate>

@optional
-(void) arrayChanged;
@end


// SCMessageBodyItemDelegate =======================================================================

@protocol SCMessageBodyItemDelegate <SCModelAbstarctDelegate>

@optional
-(void) netFileStatus:(SCNetFileStatus) status;
-(void) netFileProgress:(double) percent;
-(void) netFileError;
-(void) netFileCompleet;
@end


// SCContactItemDelegate ========================================================================
@protocol SCContactItemDelegate <SCModelAbstarctDelegate>

@optional
-(void) userAuthUpdate:(NSArray*) userAuthes;
-(void) statusChange:(SCUserStatus) status;
-(void) updateContactItem:(SCContactItem*)item;
-(void) getBannedUsersResponse:(NSMutableArray*) users;
-(void) addUsersToBlackResponse:(id)data;
-(void) removeUsersFromBlackResponse:(id)data;
-(void) checkUserInBlackListResponse:(id)data;



@end



