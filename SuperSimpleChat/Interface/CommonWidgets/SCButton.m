//
//  SCButton.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 17.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCButton.h"

@implementation SCButton

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
    [self setupUI];
}

- (void) _init
{
    
}

- (void) setupUI
{
    [self.titleLabel setFont:[UIFont fontWithName:FT_FontNameDefult size:self.titleLabel.font.pointSize]];
}

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        [self _init];
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self _init];
        [self setupUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self _init];
        [self setupUI];
    }
    return self;
}


@end
