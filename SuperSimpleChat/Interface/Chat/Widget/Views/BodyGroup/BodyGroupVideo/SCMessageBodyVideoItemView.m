//
//  SCMessageBodyVideoView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyVideoItemView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"
#import "SCNetFile.h"




@implementation SCMessageBodyVideoItemView
{
    NSString* _namePlayImage;
    NSString* _nameStopImage;
    NSString* _nameWaitImage;

    NSURL *_theMovieURL;
    
    AVPlayer* _videoPlayer;
    AVPlayerLayer *_videoPlayerLayer;
    AVPlayerItem *_videoPlayerItem;
    NSString* _videoFileCached;
    
    UIButton* _btnPlay;
    UIImageView* _fragmentImage;
    
    BOOL _isPlayed;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super initWithSettings:settings];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
//    [super _init];

    _isPlayed = NO;
    _videoPlayer = nil;
    _namePlayImage = @"chat-play";
    _nameStopImage = @"chat-record-stop";
    _nameWaitImage = @"chat-add-message-video";

    _fragmentImage = [[UIImageView alloc] init];//WithImage:[_settings.skinManager getImageForKeyX:_nameWaitImage]];
    _fragmentImage.contentMode = UIViewContentModeScaleAspectFill;
    _fragmentImage.hidden = YES;
    
    _btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnPlay.frame = CGRectMake(0, 0, VIDEO_ICON_SIZE, VIDEO_ICON_SIZE);
    [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_nameWaitImage] forState:UIControlStateNormal];
    [_btnPlay addTarget:self action:@selector(_playerPrepare) forControlEvents:UIControlEventTouchUpInside];
    
//    UITapGestureRecognizer* tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapVideoAction:)];
//    tapGuesture.numberOfTapsRequired = 1;
//    tapGuesture.delaysTouchesBegan = YES;
//    tapGuesture.delaysTouchesEnded = NO;
//    [self addGestureRecognizer:tapGuesture];
    
    _activityIndicatorView.frame = CGRectMake(0, 0, VIDEO_ICON_SIZE, VIDEO_ICON_SIZE);

    self.backgroundColor = [UIColor clearColor];
    [self addSubview:_fragmentImage];
    [self addSubview:_btnPlay];
    [self addSubview:_activityIndicatorView];
    [self.layer addSublayer:shadowLayer];
    [self.layer addSublayer:shapeLayer];

}

-(void) dealloc
{
    FTLog(@"%@ : dealloc",NSStringFromClass([self class]));
    if(_videoPlayer)
    {
        [_videoPlayer removeObserver:self forKeyPath:@"status" context:0];
        _videoPlayer = nil;
    }
    if(_videoPlayerItem)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_videoPlayerItem];
        _videoPlayerItem = nil;
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        CGPoint centerView = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        _activityIndicatorView.center = centerView;
//        _progressIndicatorRadius = shadowLayer.frame.size.width*0.48;
//        _progressBackgraundRadius = shadowLayer.frame.size.width*0.5;
        _fragmentImage.frame = self.bounds;
        _btnPlay.center = centerView;
        shapeLayer.frame = _btnPlay.frame;
        shadowLayer.frame = _btnPlay.frame;
        if(_videoPlayerLayer!= nil)
            _videoPlayerLayer.frame = self.layer.bounds;
    }
}

-(void) setVideoNetFile:(SCNetFile *)videoNetFile
{
//    [_activityIndicator View startAnimating];
    _videoNetFile = videoNetFile;
//    [_videoNetFile getVideo:self];
    [_videoNetFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                [self setStatus:_videoNetFile.status];
                break;
                
            case SCNetFileComletionTypeCompleet:
            {
                if([result isKindOfClass:[SCNetFile class]])
                {
                    UIImage* imm = [_videoNetFile imageForSize:CGSizeMake(VIDEO_WIDTH, VIDEO_WIDTH)];
                    _fragmentImage.image = imm;
                    _fragmentImage.hidden = NO;
                    [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_namePlayImage] forState:UIControlStateNormal];

                    _theMovieURL = [NSURL fileURLWithPath:[(SCNetFile*)result localPath]];
                }
            }
                break;
                
            case SCNetFileComletionTypeProgress:
                [self setProgressPercent:[result doubleValue]];
                break;
                
            case SCNetFileComletionTypeError:
                FTLog(@"SCNetFileComletionTypeError");
                break;
                
            default:
                FTLog(@"default case");
                break;
        }
//        [self setStatus:_videoNetFile.status];
    }];
}

-(void) loadVideoFile:(NSString*) videoFile
{
    
//    [_activityIndicator View stopAnimating];
    _theMovieURL = [NSURL fileURLWithPath:videoFile];
    
}

-(void) loadVideoFragment:(UIImage*) fragmentImage
{
    _fragmentImage.image = fragmentImage;
    _fragmentImage.hidden = NO;
    [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_namePlayImage] forState:UIControlStateNormal];
}

-(void) _playerPrepare
{
    if(_videoPlayer)
    {
        [self _playerAction];
    }
    else
    {
        _videoPlayerItem = [AVPlayerItem playerItemWithURL:_theMovieURL];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:_videoPlayerItem];
 
        _videoPlayer = [AVPlayer playerWithPlayerItem:_videoPlayerItem];
        [_videoPlayer addObserver:self forKeyPath:@"status" options:0 context:0];
        _videoPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:_videoPlayer];
        [self.layer insertSublayer:_videoPlayerLayer above:_fragmentImage.layer];
        
        if(_videoPlayerLayer!= nil)
            _videoPlayerLayer.frame = self.layer.bounds;

    }
}
-(void) _playerAction
{
    if(_videoPlayer.status == AVPlayerStatusReadyToPlay)
    {
        // здесь проверить мож уже плеит
        if(!_isPlayed)
        {
            [_videoPlayer seekToTime:kCMTimeZero];
            _isPlayed = YES;
            [UIView animateWithDuration:0.25 animations:^{
                [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_nameStopImage] forState:UIControlStateNormal];
                _btnPlay.frame = CGRectMake(0, _fragmentImage.frame.size.height - VIDEO_ICON_SIZE*0.5, VIDEO_ICON_SIZE*0.5, VIDEO_ICON_SIZE*0.5);
            } completion:^(BOOL finished) {
                _fragmentImage.hidden = YES;
                [_videoPlayer play];
            }];
        }
        else
        {
            [_videoPlayer pause];
            [self clearVideoPlayer];
        }
    }
    else if(_videoPlayer.status == AVPlayerStatusFailed)
    {
        FTLog(@"_videoPlayer Error");
        [self clearVideoPlayer];
    }
}

-(void) clearVideoPlayer
{
    _isPlayed = NO;
    _fragmentImage.hidden = NO;
    [UIView animateWithDuration:0.25 animations:^{
        [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_namePlayImage] forState:UIControlStateNormal];
        _btnPlay.frame = CGRectMake(0, 0, VIDEO_ICON_SIZE, VIDEO_ICON_SIZE);
        _btnPlay.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    } completion:^(BOOL finished) {
        [_videoPlayer play];
    }];


    if(_videoPlayerLayer)
    {
        [_videoPlayerLayer removeFromSuperlayer];
        _videoPlayerLayer = nil;
    }

    if(_videoPlayer)
    {
        [_videoPlayer removeObserver:self forKeyPath:@"status" context:0];
        _videoPlayer = nil;
    }
    
    if(_videoPlayerItem)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_videoPlayerItem];
        _videoPlayerItem = nil;
    }
    
}


-(void)itemDidFinishPlaying:(NSNotification *) notification
{
    [self clearVideoPlayer];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context
{
    if (object == _videoPlayer && [keyPath isEqualToString:@"status"])
    {
        if (_videoPlayer.status == AVPlayerStatusReadyToPlay)
        {

            FTLog(@"AVPlayerStatusReadyToPlay");
            [self _playerAction];
        }
        else if (_videoPlayer.status == AVPlayerStatusFailed)
        {
            FTLog(@"AVPlayerStatusFailed");
            // something went wrong. player.error should contain some information
        }
    }
}
-(void) setBodyAlignment:(SCMessageAlignment)bodyAlignment
{
    _bodyAlignment = bodyAlignment;
//    TODO:setBodyAlignment
    [self updateUI];
}

-(void) updateUI
{
    switch (_bodyAlignment)
    {
        case SCMessageAlignmentLeft:
            _activityIndicatorView.color = SKCOLOR_ChatMessageIncomingText;
            break;
            
        case SCMessageAlignmentRight:
            _activityIndicatorView.color = SKCOLOR_ChatMessageOutgoingText;
            break;
            
        default:
            FTLog(@"КАПЕЦ, от сервера пришел видео");
            break;
    }
}


-(CGSize) viewSize
{
    return CGSizeMake(VIDEO_WIDTH, VIDEO_WIDTH);
}

#pragma mark UITapGestureRecognizerDelegate
-(void) tapVideoAction:(UITapGestureRecognizer*) tapMapGesture
{
    [self _playerPrepare];
}


@end


