//
//  SCMessageBobys.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 07.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"

@implementation SCMessageBodys
{
    NSMutableArray* _messageBodys;
    NSUInteger _progressCount;
    void (^_completionBlock) (SCNetFileComletionType statusCompleet);
}

-(id) init
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if( self )
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(id) initFromArrayMessageBody:(NSArray*) messageBodysArray
{
    self = [super init];
    if(self)
    {
        _messageBodys = [[NSMutableArray alloc] initWithArray:messageBodysArray];
    }
    return self;
}

-(void) _init
{
    _messageBodys = [[NSMutableArray alloc] init];
    _progressCount = 0;
}

-(void) dealloc
{
    
}

-(void) clear
{
    for(SCMessageBodyItem *item in _messageBodys)
    {
        [item delDelegate:self];
    }
    
    [_messageBodys removeAllObjects];
    if(_delegate && [_delegate respondsToSelector:@selector(arrayChanged)])
        [_delegate arrayChanged];
}

-(void) addItemsFromArray:(NSArray*) items
{
    for(SCMessageBodyItem* item in items)
    {
        [_messageBodys addObject:item];
        [item addDelegate:self];
    }
    
    if(_delegate && [_delegate respondsToSelector:@selector(arrayChanged)])
        [_delegate arrayChanged];
}

-(void) addItem:(SCMessageBodyItem*) item
{
    [_messageBodys addObject:item];
    [item addDelegate:self];
    
    if(_delegate && [_delegate respondsToSelector:@selector(arrayChanged)])
        [_delegate arrayChanged];
}

-(void) changeItem:(SCMessageBodyItem*) item
{
    if(_delegate && [_delegate respondsToSelector:@selector(arrayChanged)])
        [_delegate arrayChanged];
}

-(void) removeItem:(SCMessageBodyItem*) item
{
    [item delDelegate:self];
    
    [_messageBodys removeObject:item];
    if(_delegate && [_delegate respondsToSelector:@selector(arrayChanged)])
        [_delegate arrayChanged];
}

-(SCMessageBodyItem*) editAtIndex:(NSUInteger) bodyIdx
{
    if(_messageBodys.count > bodyIdx)
    {
        return [_messageBodys objectAtIndex:bodyIdx];
    }
    else
    {
#warning ERROR CORRECT
        SCMessageBodyItem* newBody = [[SCMessageBodyItem alloc] initWithSettings:_settings];
        [_messageBodys addObject:newBody];
        return newBody;
    }
}

-(NSUInteger) countOfType:(SCDataType) type
{
    NSUInteger countText = 0;
    NSUInteger countPhoto = 0;
    NSUInteger countAudio = 0;
    NSUInteger countVideo = 0;
    
    for(SCMessageBodyItem* item in _messageBodys)
    {
        switch (item.type)
        {
            case SCDataTypeSystemText:
            case SCDataTypeText:
            case SCDataTypeHtml:
                countText++;
                break;

            case SCDataTypePhoto:
            case SCDataTypePhotoFile:
            case SCDataTypePhotoData:
            case SCDataTypePhotoLink:
                countPhoto++;
                break;
            case SCDataTypeAudio:
            case SCDataTypeAudioFile:
            case SCDataTypeAudioLink:
                countAudio++;
                break;
            case SCDataTypeVideo:
            case SCDataTypeVideoFile:
            case SCDataTypeVideoLink:
                countVideo++;
                break;
            default:
                break;
        }
    }
    switch (type)
    {
        case SCDataTypeText:return countText;break;
        case SCDataTypePhoto:return countPhoto;break;
        case SCDataTypeAudio:return countAudio;break;
        case SCDataTypeVideo:return countVideo;break;
        default:return 0;break;
    }
}

-(NSMutableArray*) items
{
    return _messageBodys;
}

-(NSMutableArray*) itemsByType:(SCDataType) type
{
    NSMutableArray* outArray = [[NSMutableArray alloc] init];
    for(SCMessageBodyItem* item in _messageBodys)
    {
        switch (item.type)
        {
            case SCDataTypeSystemText:
            case SCDataTypeText:
            case SCDataTypeHtml:
            {
                switch (type)
                {
                    case SCDataTypeSystemText:
                    case SCDataTypeText:
                    case SCDataTypeHtml:
                        [outArray addObject:item];
                        break;
                    default:
                        break;
                }
            }
                break;
                
            case SCDataTypePhoto:
            case SCDataTypePhotoData:
            case SCDataTypePhotoLink:
            {
                switch (type)
                {
                    case SCDataTypePhoto:
                    case SCDataTypePhotoData:
                    case SCDataTypePhotoLink:
                        [outArray addObject:item];
                        break;
                    default:
                        break;
                }
            }
                break;
                
            case SCDataTypeAudio:
            case SCDataTypeAudioFile:
            case SCDataTypeAudioLink:
            {
                switch (type)
                {
                    case SCDataTypeAudio:
                    case SCDataTypeAudioFile:
                    case SCDataTypeAudioLink:
                        [outArray addObject:item];break;
                    default:break;
                }
            }
                break;
                
            case SCDataTypeVideo:
            case SCDataTypeVideoFile:
            case SCDataTypeVideoLink:
            {
                switch (type)
                {
                    case SCDataTypeVideo:
                    case SCDataTypeVideoFile:
                    case SCDataTypeVideoLink:
                        [outArray addObject:item];break;
                    default:break;
                }
            }
                break;
                
            default:break;
        }
    }
    return outArray;
}

-(void) sendMessageBodysWithCompletionBlock:(void (^)(SCNetFileComletionType type))completionBlock
{
    _completionBlock = [completionBlock copy];
    
    for(SCMessageBodyItem* bodyItem in _messageBodys)
    {
        _progressCount++;
        [bodyItem sendItem];
    }
    
}

#pragma mark - SCMessageBodyItemDelegate <SCModelAbstarctDelegate>

-(void) netFileStatus:(SCNetFileStatus) status
{
    
}
-(void) netFileProgress:(double) percent
{
    
}
-(void) netFileError
{
    
}
-(void) netFileCompleet
{
    _progressCount--;
    if(_progressCount == 0)
        _completionBlock(SCNetFileComletionTypeCompleet);
}

@end
