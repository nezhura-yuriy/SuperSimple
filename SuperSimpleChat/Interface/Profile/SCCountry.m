//
//  SCCountry.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 16.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCCountry.h"

@implementation SCCountry

+ (NSArray *) countryList
{
    NSLocale *locale = [NSLocale currentLocale];
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    
    NSMutableArray *sortedCountryArray = [[NSMutableArray alloc] init];
    
    for (NSString *countryCode in countryArray) {
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [sortedCountryArray addObject:displayNameString];
    }
    
    [sortedCountryArray sortUsingSelector:@selector(localizedCompare:)];
    
    NSString *cleverCountry = @"Åland Islands";
    
    for (NSInteger i=0; i < sortedCountryArray.count; i++) {
        if ([sortedCountryArray[i] isEqualToString:cleverCountry]) {
            [sortedCountryArray removeObject:cleverCountry];
            [sortedCountryArray insertObject:cleverCountry atIndex:0];
        }
    }
    
    NSString *kCountryNotSelected = @" Country not selected";
    
    [sortedCountryArray insertObject:kCountryNotSelected atIndex:0];
    
    return sortedCountryArray;
}

@end
