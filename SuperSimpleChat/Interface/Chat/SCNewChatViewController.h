//
//  SCNewChatViewController.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 06.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"
#import "SCModelsDelegates.h"
#import "SCPhotoPanel.h"

@class SCSettings;
@class SCChatItem;
@class SCButtonRightImage;
@class SCImageViewWithIndicator;
@class SCPhotoPanel;
@class SCSharingPanel;

@interface SCNewChatViewController : UIViewController<SCPBManagerDelegate,SCModelChatsDelegate,UITextViewDelegate,SCBottomViewsDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,SCPhotoPanelDelegate,SCChatUsersViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,assign) SCChatType forChatType;
@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) SCChatItem* chatItem;
//@property(nonatomic,assign) BOOL isNewMode;
@property(assign, nonatomic) BOOL isNewChat;
@property(nonatomic,assign) BOOL isViewOnly;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollFormView;
@property (strong, nonatomic) IBOutlet SCPhotoPanel *imagePanel;
@property (strong, nonatomic) IBOutlet UILabel *labOwner;

@property (strong, nonatomic) IBOutlet UITextField *fldChatName;
@property (strong, nonatomic) IBOutlet UITextField *fldChatDescription;

@property (strong, nonatomic) IBOutlet SCButtonRightImage *btnSelectUsers;
@property (strong, nonatomic) IBOutlet UITableView *tableViewUsers;

@property (strong, nonatomic) IBOutlet SCSharingPanel *sharePanel;
- (IBAction)fldChatNameChange:(UITextField *)sender forEvent:(UIEvent *)event;
@end
