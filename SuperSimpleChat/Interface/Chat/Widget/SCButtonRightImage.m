//
//  SCButtonRightImage.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 24.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCButtonRightImage.h"

#define MARGIN 5
@implementation SCButtonRightImage
{
    UIImageView *_arrowImage;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self _init];
}

-(void) _init
{
    _arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowright"]];
//    _arrowImage
    [self addSubview:_arrowImage];
}

-(void) setRightImage:(UIImage*) image
{
    [_arrowImage setImage:image];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    _arrowImage.frame = CGRectMake(self.frame.size.width - _arrowImage.image.size.height - MARGIN,(self.frame.size.height - _arrowImage.image.size.height)/2, _arrowImage.image.size.width, _arrowImage.image.size.height);
    
}
@end
