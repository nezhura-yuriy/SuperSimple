//
//  SCChatItem.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
/**
 
 */

#import "SCChatItem.h"
#import "SCSettings.h"
#import "SCModelChats.h"
#import "SCMessageItem.h"
#import "SCMessageGroup.h"
#import "SCContactItem.h"
#import "SCNetFile.h"
#import "SCImageViewWithIndicator.h"

@implementation SCChatItem
{
    NSDateFormatter* _dateFormater;
}


-(void) dealloc
{
    [_delegates removeAllObjects];
}

-(void) _init
{
    _chatID = @"";
    _chatPuuid = [[SCPUUID alloc] init];
    _chatName = @"";
    _chatDescription = @"";
    _timeDestroy = 0;
    _chatType = 0;
    _securityLevel = 0;
    _netFile = [[SCNetFile alloc] initWithSettings:_settings];
    _chatDayMessages = [[NSMutableArray alloc] init];
    _dateFormater = [[NSDateFormatter alloc] init];
    [_dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
    [_dateFormater setLocale:[NSLocale systemLocale]];
    [_dateFormater setDateFormat:@"yyyy-mm-dd HH:MM:SS"];
    _delegates = [[NSMutableArray alloc] init];
    _usersInChat = [[NSMutableArray alloc] init];
    
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    if([[dictionary allKeys] containsObject:@"chatID"])
        _chatID = [dictionary objectForKey:@"chatID"];
    
    if([[dictionary allKeys] containsObject:@"chatPuuid"])
        _chatPuuid = [[SCPUUID alloc] initFromString:[dictionary objectForKey:@"chatPuuid"]];
    
    if([[dictionary allKeys] containsObject:@"chatName"])
        _chatName = [dictionary objectForKey:@"chatName"];
    
    if([[dictionary allKeys] containsObject:@"chatDescription"])
        _chatDescription = [dictionary objectForKey:@"chatDescription"];
    
    if([[dictionary allKeys] containsObject:@"timeDestroy"])
        _timeDestroy = [[dictionary objectForKey:@"timeDestroy"] integerValue];
    
    if([[dictionary allKeys] containsObject:@"chatType"])
        _chatType = (SCChatType)[[dictionary objectForKey:@"chatType"] integerValue];
    
    if(_chatType == SCChatTypePrivate)
    {
        if([[dictionary allKeys] containsObject:@"logoSource"])
        {
            NSString* logoUuid = [dictionary objectForKey:@"logoSource"];
            _logoSource = [_settings.modelUsers getUserByPUUIDString:logoUuid];
        }
    }
    else
    {
        _logoSource = self;
    }
    if([[dictionary allKeys] containsObject:@"netFile"])
    {
        _netFile = [[SCNetFile alloc] initWithSettings:_settings];
        [_netFile fromDictionary:[dictionary objectForKey:@"netFile"]];
        _netFile.logoText = _chatName;
    }

    
    if([[dictionary allKeys] containsObject:@"securityLevel"])
        _securityLevel = (SCMessageSecurityLevel)[[dictionary objectForKey:@"securityLevel"] integerValue];
//    _chatOwner
    
    if([[dictionary allKeys] containsObject:@"chatOwner"])
    {
        if(_settings)
        {
            _chatOwner = [_settings.modelUsers getUserByPUUIDString:[dictionary objectForKey:@"chatOwner"]];
        }
    }
    if([[dictionary allKeys] containsObject:@"isDeleted"])
        _isDeleted = [[dictionary objectForKey:@"isDeleted"] boolValue];
    
    if([[dictionary allKeys] containsObject:@"lastViewedMessageDate"])
        _lastViewedMessageDate = [dictionary objectForKey:@"lastViewedMessageDate"];
    
    if([[dictionary allKeys] containsObject:@"lastLoadedMessageDate"])
        _lastLoadedMessageDate = [dictionary objectForKey:@"lastLoadedMessageDate"];
    
    
//    if([[dictionary allKeys] containsObject:@""])
        _notViewedMessagesCount = 0;

}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    
    [outDict setObject:_chatID forKey:@"chatID"];
    [outDict setObject:[_chatPuuid scpuuidToString] forKey:@"chatPuuid"];
    [outDict setObject:_chatName forKey:@"chatName"];
    [outDict setObject:_chatDescription forKey:@"chatDescription"];
    [outDict setObject:[NSNumber numberWithInteger:_timeDestroy] forKey:@"timeDestroy"];
    [outDict setObject:[NSNumber numberWithInteger:_chatType] forKey:@"chatType"];
    [outDict setObject:[_netFile toDictionary] forKey:@"netFile"];
    if(_chatType == SCChatTypePrivate)
    {
        if(_logoSource && !_isDeleted)
        {
            if([_logoSource isKindOfClass:[SCContactItem class]])
            {
                NSString* userPuuid =[((SCContactItem*)_logoSource).userPUUID scpuuidToString];
                [outDict setObject:userPuuid forKey:@"logoSource"];
            }
        }
        
    }
    [outDict setObject:[NSNumber numberWithInteger:_securityLevel] forKey:@"securityLevel"];
    if(_chatOwner && _chatOwner.userPUUID)
        [outDict setObject:[_chatOwner.userPUUID scpuuidToString] forKey:@"chatOwner"];
    else
        FTLog(@"ПИПЕЦ, ЛОПУХ");
    [outDict setObject:[NSNumber numberWithBool:_isDeleted] forKey:@"isDeleted"];
    if(_lastViewedMessageDate != nil)
        [outDict setObject:_lastViewedMessageDate forKey:@"lastViewedMessageDate"];
    if(_lastLoadedMessageDate != nil)
        [outDict setObject:_lastLoadedMessageDate forKey:@"lastLoadedMessageDate"];
    
    return outDict;
}

-(NSString*) description
{
    return [NSString stringWithFormat:@"{\rchatUUID = %@,\rchatName = %@,\rchatDescription = %@,\rtimeDestroy = %f,\rlastViewedMessageDate = %@,\rnotViewedMessagesCount = %ld\r}",[_chatPuuid scpuuidToString],_chatName,_chatDescription,_timeDestroy,_lastViewedMessageDate,(long)_notViewedMessagesCount];
}

-(void) setChatImageLink:(NSString *)chatImageLink
{
    _netFile.fileType = SCMediaFileTypePhoto;
    _netFile.netPath = chatImageLink;
}

-(void) setUsersInChat:(NSMutableArray *)usersInChat
{
    _usersInChat = usersInChat;
}

-(void) addUserInChat:(SCContactItem*) user
{
    if(![_usersInChat containsObject:user])
    {
        [_usersInChat addObject:user];
    }
    else
    {
        FTLog(@"User allready in chatroom");
    }
}

-(void) userListInChat:(NSArray*) users
{
    for(SCContactItem* user in users)
    {
        if(![_usersInChat containsObject:user])
        {
            [_usersInChat addObject:user];
        }
//        else
//        {
//            FTLog(@"User already in chat");
//        }
    }
}

-(void) lifeTimeEnd:(SCMessageItem*) item
{
    SCMessageGroup* groupMessages = nil;
    groupMessages = [self searchGroup:item.messageDateTime];
    if(groupMessages)
    {
        item.isDeleted = YES;
        [groupMessages saveMessages];
        
        for(int i = _delegates.count-1.0; i >=0  ; i--)
        {
            id<SCPBManagerDelegate> delegate = [_delegates objectAtIndex:i];
            if([delegate respondsToSelector:@selector(parsedType:data:)])
                [delegate parsedType:SCPBManagerParceTypeUpdateMessage data:groupMessages];
        }

    }
}

#pragma mark messageGroup
-(SCMessageGroup*) searchGroup:(NSDate*) messageDateTime
{
    SCMessageGroup* groupMessages = nil;
    for(groupMessages in _chatDayMessages)
    {
        
        if([self isSameDay:groupMessages.messagesDate otherDay:messageDateTime])
        {
            // debug
//            [self sortGroups];
            return groupMessages;
        }
    }
    return nil;
    /*
    groupMessages = [[SCMessageGroup alloc] initWithSettings:_settings];
    groupMessages.chatItem = self;
    groupMessages.messagesDate = messageDateTime;
    [_chatDayMessages addObject:groupMessages];
    [self sortGroups];
    
    return groupMessages;
    */
}

-(SCMessageGroup*) newGroup:(NSDate*) messageDateTime
{
    SCMessageGroup* groupMessages = nil;
    groupMessages = [[SCMessageGroup alloc] initWithSettings:_settings];
    groupMessages.chatItem = self;
    groupMessages.messagesDate = messageDateTime;
    [_chatDayMessages addObject:groupMessages];
    [self sortGroups];
    
    return groupMessages;
}

-(void) sortGroups
{
    [_chatDayMessages sortUsingComparator:^NSComparisonResult (id a, id b)
     {
         
         SCMessageGroup * itemA = (SCMessageGroup*)a;
         SCMessageGroup* itemB =(SCMessageGroup*)b;
         
         
         NSComparisonResult result = [itemA.messagesDate compare:itemB.messagesDate];
         return result;
         //        if (result==NSOrderedAscending)
         //            return NSOrderedDescending;
         //        else if(result==NSOrderedDescending)
         //            return NSOrderedAscending;
         //        else
         //            return result;
     }];
}


#pragma mark message
-(SCMessageItem*) searchMessageItemByPUUID:(SCPUUID*) scPUUID
{
    SCMessageItem* _messageItem = nil;
    for(SCMessageGroup* _groupMessages in _chatDayMessages)
    {
        _messageItem = [_groupMessages searchMessageItemByPUUID:scPUUID];
        if(_messageItem)
            return _messageItem;
    }
    return nil;
}
-(SCMessageItem*) searchMessageItemByPUUID:(SCPUUID*) scPUUID messageDateTime:(NSDate*) messageDateTime
{
    SCMessageGroup* _groupMessages = nil;
    SCMessageItem* _messageItem = nil;
    
    _groupMessages = [self searchGroup:messageDateTime];
    if(!_groupMessages)
        return nil;
    
    _messageItem = [_groupMessages searchMessageItemByPUUID:scPUUID];
    if(_messageItem)
        return _messageItem;
    
    return nil;
    /*
    _messageItem = [[SCMessageItem alloc] initWithChatItem:self];
    _messageItem.messageID = [scPUUID scpuuidToString];
    _messageItem.messageDateTime = messageDateTime;

    return _messageItem;
    */
}

-(SCMessageItem*) newMessageItemByPUUID:(SCPUUID*) scPUUID messageDateTime:(NSDate*) messageDateTime
{
    SCMessageGroup* _groupMessages = nil;
    SCMessageItem* _messageItem = nil;

    _groupMessages = [self searchGroup:messageDateTime];
    if(_groupMessages)
    {
        _messageItem = [_groupMessages searchMessageItemByPUUID:scPUUID];
    }
    
    if(_messageItem)
        return _messageItem;

    _messageItem = [[SCMessageItem alloc] initWithChatItem:self];
    _messageItem.messageID = [scPUUID scpuuidToString];
    _messageItem.messageDateTime = messageDateTime;
    return _messageItem;
}
-(SCMessageItem*) getLastMessage
{
    SCMessageGroup* lastGroupMessages = nil;
    SCMessageItem* _messageItem = nil;
    // находим последнюю дату
    for(SCMessageGroup* groupMessages in _chatDayMessages)
    {
        if(!lastGroupMessages)
        {
            lastGroupMessages = groupMessages;
        }
        else
        {
            NSComparisonResult result = [lastGroupMessages.messagesDate compare:groupMessages.messagesDate];
            if(result==NSOrderedAscending)
            {
                lastGroupMessages = groupMessages;
            }
        }
    }
    
    // находим последнее сообщение в группе
    if(lastGroupMessages)
        return [lastGroupMessages getLastMessage];
    
    // нет группы
    return _messageItem;
}

-(NSUInteger) countNotViewedMessages
{
    NSUInteger countMessages = 0;
    for(SCMessageGroup* groupMessages in _chatDayMessages)
    {
        countMessages += [groupMessages countNotViewedMessages];
    }
    return countMessages;
}

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day] == [comp2 day] && [comp1 month] == [comp2 month] && [comp1 year]  == [comp2 year];
}

#pragma mark - CACHE
-(void) loadCachedMessages
{
    NSString* path = [_settings.cacheMessagesPath stringByAppendingPathComponent:[_chatPuuid scpuuidToString]];
    NSArray *locArrayDays = [NSMutableArray arrayWithContentsOfFile:path];
    for(NSDictionary* loadedDict in locArrayDays)
    {
//        SCMessageGroup* mesGroup = [self searchGroup:[loadedDict objectForKey:@"messagesDate"]]
        SCMessageGroup* mesGroup = nil;
        mesGroup = [self searchGroup:[loadedDict objectForKey:@"messagesDate"]];
        if(!mesGroup)
        {
            mesGroup = [self newGroup:[loadedDict objectForKey:@"messagesDate"]];
            [mesGroup fromDictionary:loadedDict];
            [mesGroup loadMessages];
        }
    }
    [self sortGroups];
}

-(void) saveCachedMessages
{
    NSString* path = [_settings.cacheMessagesPath stringByAppendingPathComponent:[_chatPuuid scpuuidToString]];
    NSMutableArray* arrayGroup = [[NSMutableArray alloc] init];
    for(SCMessageGroup* item in _chatDayMessages)
    {
        NSDictionary* dict = [item toDictionary];
        [arrayGroup addObject:dict];
    }
    BOOL res = [arrayGroup writeToFile:path atomically:YES];
    if(!res)
        FTLog(@"ops. Error write arrayGroup");
    else
        FTLog(@"%@ save items to cache",_chatName);
}



-(void) updateItemContent
{
//TODO: загрузка итемов чата
//    [self loadHistory:0];
    if(!_isDeleted)
    {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       self, @"delegate",
                                       [[SCPUUID alloc] initWithRandom], @"UUID",
                                       [NSNumber numberWithInteger:SCPBManagerParceTypeGetChatRoomHistory], @"type",
                                       self,@"SCChatItem",
                                       [NSNumber numberWithInteger:0],@"offset",
                                       [NSNumber numberWithInteger:200],@"limit",
                                       nil];

        [params setObject:[NSDate date] forKey:@"DateEnd"];
        if(!_lastLoadedMessageDate)
            [params setObject:[[NSDate date] dateByAddingTimeInterval:-60*60*24*30] forKey:@"DateStart"];
        else
            [params setObject:_lastLoadedMessageDate forKey:@"DateStart"];
        
    //    dtStart = [params objectForKey:@"DateStart"];
    //    dtStart = [dtEnd dateByAddingTimeInterval:-60*60*24*30];
        
        [_settings.modelProfile.protoBufManager getChatRoomHistoryWithTimeRangeRequest:params];
    }
    
    for(id<SCChatItemDelegate> delegate in _delegates)
    {
        if([delegate respondsToSelector:@selector(itemUpdate)])
           [delegate itemUpdate];
    }
}


#pragma mark SCNetEmulatorDelegates
-(void) chatNewMessages:(NSArray*) newMessages
{
    for(id<SCChatItemDelegate> delegate in _delegates)
    {
        if([delegate respondsToSelector:@selector(loadedMessageItems:)])
        {
            [delegate loadedMessageItems:newMessages];
        }
        
    }
}

#pragma mark - server requests
-(void) loadUserList
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeGetAllUsersForRoom], @"type",
                                   self, @"SCChatItem",
                                   nil];
    [_settings.modelProfile.protoBufManager getAllUsersForRoomRequest:params];

}

-(void) loadHistory:(NSInteger) offs
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeGetChatRoomHistory], @"type",
                                   self,@"SCChatItem",
                                   [NSNumber numberWithInteger:offs],@"offset",
                                   [NSNumber numberWithInteger:200],@"limit",
                                   nil];
    
    [_settings.modelProfile.protoBufManager getChatRoomHistoryWithTimeRangeRequest:params];

}

-(void) itemGetLastReadDate
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   transportPUUID, @"UUID",
                                   self,@"SCChatItem",
                                   nil];
    [_settings.modelProfile.protoBufManager getLastReadMessageDate:params];
}

-(void) itemSetLastReadDate:(NSDate*) lastReadDate
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   transportPUUID, @"UUID",
                                   self,@"SCChatItem",
                                   [NSDate date],@"lastReadDate",
                                   nil];
    [_settings.modelProfile.protoBufManager setLastReadMessageDate:params];
    
}

-(void) joinUserToRoom:(SCContactItem*) contactItem
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeJoinUserToRoom], @"type",
                                   self,@"SCChatItem",
                                   contactItem,@"SCContactItem",
                                   nil];
    
    [_settings.modelProfile.protoBufManager joinUserToRoomRequest:params];
}

-(void) leaveUserFromRoom:(SCContactItem*) contactItem
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeLeaveUserFromRoom], @"type",
                                   self,@"SCChatItem",
                                   contactItem,@"SCContactItem",
                                   nil];
    
    [_settings.modelProfile.protoBufManager leaveUserFromRoomRequest:params];
}

-(void) sendMessage:(SCMessageItem*) newMessage
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeGetAllUsersForRoom], @"type",
                                   newMessage,@"SCMessageItem",
                                   nil];
    [_settings.modelProfile.protoBufManager sendMessage:params];
}

-(void) updateMessage:(SCMessageItem*) updatedMessage
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeUpdateMessage], @"type",
                                   updatedMessage,@"SCMessageItem",
                                   nil];
    [_settings.modelProfile.protoBufManager updateMessage:params];
}

-(void) removeMessage:(SCMessageItem*) removedMessage
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeRemoveMessage], @"type",
                                   removedMessage,@"SCMessageItem",
                                   nil];
    [_settings.modelProfile.protoBufManager removeMessage:params];
}

#pragma mark SCPBManagerDelegate <NSObject>
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"SCChatViewController : receiveParsedData");
    switch (type)
    {
/* ChatRooms =================================================================== */
// MARK:SCPBManagerParceTypeCreateChat
        case SCPBManagerParceTypeCreateChat:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = (SCChatItem*)data;
                [chatItem updateItemContent];
                [_settings.modelChats parsedType:type data:chatItem];
            }
        }
            break;
// MARK:SCPBManagerParceTypeJoinUserToRoom
        case SCPBManagerParceTypeJoinUserToRoom:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = (SCChatItem*)data;
                [chatItem updateItemContent];
                [_settings.modelChats parsedType:type data:chatItem];
            }
            
        }
            break;
            
//MARK:SCPBManagerParceTypeLeaveUserFromRoom
        case SCPBManagerParceTypeLeaveUserFromRoom:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = (SCChatItem*)data;
                chatItem.isDeleted = YES;
                [_settings.modelChats parsedType:type data:chatItem];
                
                if([data isKindOfClass:[SCChatItem class]])
                {
                    for(id<SCPBManagerDelegate> delegate in _delegates)
                    {
                        if([delegate respondsToSelector:@selector(parsedType:data:)])
                            [delegate parsedType:type data:data];
                    }
                }

            }
            else
            {
                FTLog(@"Logic error SCChatItem case SCPBManagerParceTypeLeaveUserFromRoom:");
            }
            
        }
            break;
            
//MARK:SCPBManagerParceTypeGetAllUsersForRoom
        case SCPBManagerParceTypeGetAllUsersForRoom:
        {
            if([data isKindOfClass:[NSArray class]])
            {
                [self userListInChat:data];
            
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                        [delegate parsedType:type data:data];
                }
            }
        }
            break;
            
//MARK:SCPBManagerParceTypeGetLastReadMessageDate
        case SCPBManagerParceTypeGetLastReadMessageDate:
        {
            FTLog(@"%@ _lastViewedMessageDate = %@",_chatName, data);
            if([data isKindOfClass:[NSDate class]])
            {
                _lastViewedMessageDate = (NSDate*) data;
                _lastMessage = [self getLastMessage];
                _notViewedMessagesCount = [self countNotViewedMessages];
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                        [delegate parsedType:type data:data];
                }
            }
        }
            break;
            
//MARK:SCPBManagerParceTypeSetLastReadMessageDate
        case SCPBManagerParceTypeSetLastReadMessageDate:
        {
//            FTLog(@"%@",data);
            if([data isKindOfClass:[NSDictionary class]])
            {
                _lastViewedMessageDate = [data objectForKey:@"lastReadDate"];
                _lastMessage = [self getLastMessage];
                _notViewedMessagesCount = [self countNotViewedMessages];
                [_settings.modelChats parsedType:type data:data];
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                        [delegate parsedType:type data:data];
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeChangeRoomEvent:
        case SCPBManagerParceTypeChangeChat:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                        [delegate parsedType:type data:data];
                }
            }
        }
            break;
            

/* Messages ==================================================================== */
// MARK:Message 4
        case SCPBManagerParceTypeMessage:
            
        case SCPBManagerParceTypeSendMessage:
        case SCPBManagerParceTypeGetChatRoomHistory:
        case SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange:
        {
            NSMutableArray* changedGroups = [[NSMutableArray alloc] init];
            
            if([data isKindOfClass:[SCMessageItem class]])
            {
                SCMessageItem* messageItem = (SCMessageItem*) data;
                changedGroups = [self _parseMessage:messageItem toChangedGroups:changedGroups];
//                [self itemSetLastReadDate:messageItem.messageDateTime];
            }
            else if([data isKindOfClass:[NSArray class]])
            {
                NSArray* mes = data;
                for(SCMessageItem* messageItem in mes)
                {
                    changedGroups = [self _parseMessage:messageItem toChangedGroups:changedGroups];
                }
            }
            
            _lastMessage = [self getLastMessage];
            if(type == SCPBManagerParceTypeSendMessage)
            {
                _lastViewedMessageDate = _lastMessage.messageDateTime;
            }
            else if (type == SCPBManagerParceTypeMessage)
            {
                if([data isKindOfClass:[SCMessageItem class]])
                {
                    SCMessageItem* messageItem = (SCMessageItem*) data;
                    if([messageItem.contact isEqual:_settings.modelProfile.contactItem])
                        _lastViewedMessageDate = _lastMessage.messageDateTime;
                }
            }
            
            _notViewedMessagesCount = [self countNotViewedMessages];

            if(type == SCPBManagerParceTypeGetChatRoomHistory)
                _lastLoadedMessageDate = _lastMessage.messageDateTime;
            
            for (SCMessageGroup* groupMessages in changedGroups)
            {
                [groupMessages saveMessages];
            }
            
            [self sortGroups];
            if(changedGroups.count > 0)
            {
                [self saveCachedMessages];
            }
            if(type == SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange || type == SCPBManagerParceTypeGetChatRoomHistory)
            {
                [_settings.modelChats parsedType:type data:self];
            }
            else
            {
                FTLog(@"SCPBManagerParceTypeMessage");
            }
            //SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange+
            //SCPBManagerParceTypeMessage-
            
            for(int i = _delegates.count-1.0; i >=0  ; i--)
            {
                id<SCPBManagerDelegate> delegate = [_delegates objectAtIndex:i];
//                if([delegate respondsToSelector:@selector(loadedGroupsMessage:)])
//                    [delegate loadedGroupsMessage:changedGroups];
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                    [delegate parsedType:type data:changedGroups];
            }
            
            [changedGroups removeAllObjects];
            changedGroups = nil;
            
        }
            break;
            
        case SCPBManagerParceTypeUpdateMessage:
        case SCPBManagerParceTypeUpdateMessageInChatroomEvent:
            if([data isKindOfClass:[SCMessageItem class]])
            {
                
            }
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            
            break;
            
        case SCPBManagerParceTypeRemoveMessage:
        case SCPBManagerParceTypeRemoveMessageFromChatroomEvent:
        {
            NSMutableArray* changedGroups = [[NSMutableArray alloc] init];
            if([data isKindOfClass:[SCMessageItem class]])
            {
                SCMessageItem* messageItem = (SCMessageItem*) data;
                changedGroups = [self _parseMessage:messageItem toChangedGroups:changedGroups];

            }
            else if([data isKindOfClass:[NSArray class]])
            {
                NSArray* mes = data;
                for(SCMessageItem* messageItem in mes)
                {
                    changedGroups = [self _parseMessage:messageItem toChangedGroups:changedGroups];
                }
            }

            [self sortGroups];

            _lastMessage = [self getLastMessage];
            _lastViewedMessageDate = _lastMessage.messageDateTime;
            _notViewedMessagesCount = [self countNotViewedMessages];

            if(changedGroups.count > 0)
            {
                [self saveCachedMessages];
            }

            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:changedGroups];
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeRefreshedMessageEvent:
            if([data isKindOfClass:[SCMessageItem class]])
            {
                SCMessageItem* messageItem = (SCMessageItem*) data;
                
            }
            for(id<SCPBManagerDelegate> delegate in _delegates)
            {
                if([delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:type data:data];
                }
            }
            break;
            
            
// MARK:SCPBManagerParceTypeGetUserByAuth
        case SCPBManagerParceTypeGetUserByAuth:
        {
            FTLog(@"%@",data);
        }
            break;
            
        default:
        {
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
        }
            break;
    }
}

-(NSMutableArray*) _parseMessage:(SCMessageItem*) messageItem toChangedGroups:(NSMutableArray*) changedGroups
{
    SCMessageGroup* groupMessages = nil;
    
    groupMessages = [self searchGroup:messageItem.messageDateTime];
    if(!groupMessages)
        groupMessages = [self newGroup:messageItem.messageDateTime];
    
    //BOOL isAdd =
    [groupMessages addMeesageItem:messageItem];
        
    if(![changedGroups containsObject:groupMessages])// && isAdd)
        [changedGroups addObject:groupMessages];
    
    return changedGroups;
}

-(void) _parseDataMessage:(id) data
{
    NSMutableArray* changedGroups = [[NSMutableArray alloc] init];

    if([data isKindOfClass:[SCMessageItem class]])
    {
        SCMessageItem* messageItem = (SCMessageItem*) data;
        changedGroups = [self _parseMessage:messageItem toChangedGroups:changedGroups];
    }
    else if([data isKindOfClass:[NSArray class]])
    {
        NSArray* mes = data;
        for(SCMessageItem* messageItem in mes)
        {
            changedGroups = [self _parseMessage:messageItem toChangedGroups:changedGroups];
        }
    }
    
    _lastMessage = [self getLastMessage];
    _notViewedMessagesCount = [self countNotViewedMessages];
    
    for (SCMessageGroup* groupMessages in changedGroups)
    {
        [groupMessages saveMessages];
    }
    
    [self sortGroups];
    if(changedGroups.count > 0)
    {
        [self saveCachedMessages];
    }
    for(int i = _delegates.count-1.0; i >=0  ; i--)
    {
        id<SCChatItemDelegate> delegate = [_delegates objectAtIndex:i];
        if([delegate respondsToSelector:@selector(loadedGroupsMessage:)])
            [delegate loadedGroupsMessage:changedGroups];
    }

    
    [changedGroups removeAllObjects];
    changedGroups = nil;
}
@end
