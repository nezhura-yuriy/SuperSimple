 //
//  SCPBManagerTernopol(Profile).m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol(Profile).h"
#import "SCPBManagerTernopol(Converts).h"
#import "SCTypes.h"
#import "SmartChat.pb.h"
#import "SCPUUID.h"

#import "SCContactItem.h"

static NSInteger pingInterval = 9.0;
static NSString *kKeySuccess = @"success";
static NSString *kKeyData = @"data";
static NSString *kKeyField = @"field";
static NSString *kKeyValue = @"value";

@implementation SCPBManagerTernopol (Profile)

/* =======================================================================================
 MARK: PROFILE_GET
 No request object
 */

-(void) profileGet:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeProfileGet];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

/*
message GetAllFieldsResponse {
    required PUUID user_id = 1; /// Id of users, which is registered in system
    repeated Field fields = 2; /// Information fields
    optional PUUID favorite_list_id = 3; /// Favorite list id of User
}
*/
-(void) parseProfileGet:(id)data
{
    Response *response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            GetAllFieldsResponse* serverProfile = [GetAllFieldsResponse parseFromData:response.response];
            SCContactItem* contactItem = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:serverProfile.userId]];
// Используем кешированный итем
//            SCContactItem* contactItem = [[SCContactItem alloc] initWithSettings:_settings];
//            contactItem.userPUUID = [self scpuuidBuilder:serverProfile.userId];
            contactItem.contactPUUID = [self scpuuidBuilder:serverProfile.userId];
            contactItem.contactAddress = [[SCAdress alloc] initWithSettings:_settings];
            
            contactItem.favoriteListPUUID = [self scpuuidBuilder:serverProfile.favoriteListId];
            
            if(serverProfile.fields.count > 0)
            {
                [self contactFromFields:contactItem fields:serverProfile.fields];
            }
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeGetProfile data:contactItem];
            }
        }
        else
        {
            FTLog(@"Error parseProfileGet response.status.success");
        }
    }
    else
        FTLog(@"Error transportPUUIDstring");
}

/* =======================================================================================
 message SetProfileFieldRequest {
 repeated Field fields = 1;
 }
 
params keys:
 UUID
 delegate
 SCContactItem (class SCContactItem)
 fields array of SCFieldType (if no key, processed ALL FIELDS)
 */
-(void) setProfileField:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = params[@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        SCContactItem* contactItem = nil;
        if([[params allKeys] containsObject:@"SCContactItem"])
            contactItem = params[@"SCContactItem"];
        
        SetProfileFieldRequestBuilder* setProfileFieldRequestBuilder = [[SetProfileFieldRequestBuilder alloc] init];
        
        NSArray* arrayFields;
        BOOL existDataToSend = NO;
        
        if([[params allKeys] containsObject:kKeyField] && [[params allKeys] containsObject:kKeyValue])
        {
            SCFieldType scFieldType = (SCFieldType)[params[kKeyField] integerValue];
            [setProfileFieldRequestBuilder addFields:[self fieldFromContact:scFieldType fromValue:params[kKeyValue]]];
            existDataToSend = YES;
        }
        else if([[params allKeys] containsObject:@"fields"])
        {
            NSArray* scFieldArray =[params objectForKey:@"fields"];
            if(scFieldArray.count >0)
            {
                existDataToSend = YES;
                for(NSUInteger i = 0; i < scFieldArray.count;i++)
                {
                    SCFieldType scFieldType = (SCFieldType)[[scFieldArray objectAtIndex:i] integerValue];
                    [setProfileFieldRequestBuilder addFields:[self fieldFromContact:scFieldType fromItem:contactItem]];
                }
            }
            
        }
        else if(contactItem)
        {
            arrayFields = [self fieldsFromContact:contactItem];
            if(arrayFields.count > 0)
            {
                existDataToSend = YES;
                [setProfileFieldRequestBuilder setFieldsArray:arrayFields];
            }
        }
        
        if(existDataToSend)
        {
            RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
            [_requestBuilder setType:CallTypeSetProfileField];
            [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
            [_requestBuilder setRequest:[setProfileFieldRequestBuilder build].data];
            Request * query = [_requestBuilder build];
            [_socketClient sendData:[query data]];
        }
        else
            [_delegateDictionary removeObjectForKey:[transportPUUID scpuuidToString]];
        
        
    }
}
-(void) parseSetProfileField:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = _delegateDictionary[transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = params[@"delegate"];
        SCContactItem* contactItem = params[@"SCContactItem"];

        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        BOOL delegateRespondsToSel = (delegate && [delegate respondsToSelector:@selector(parsedType:data:)]);
        
        if(response.status.success)
        {
            SCFieldType scFieldType;
            
            if([[params allKeys] containsObject:kKeyField] && [[params allKeys] containsObject:kKeyValue])
            {
                scFieldType = (SCFieldType)[params[kKeyField] integerValue];
                id newValue = params[kKeyValue];

                [self contactField:contactItem fieldType:scFieldType fromValue:newValue];
            }
            
            NSDictionary *transportDict = @{kKeySuccess: [NSNumber numberWithBool:YES],
                                            kKeyData: contactItem,
                                            kKeyField: [NSNumber numberWithInteger:scFieldType]
                                            };
            
            if (delegateRespondsToSel)
            {
                [delegate parsedType:SCPBManagerParceTypeSetProfileField data:transportDict];
            }
        }
        else
        {
            if([[params allKeys] containsObject:kKeyField])
            {
                NSDictionary *transportDict = @{kKeySuccess: [NSNumber numberWithBool:NO],
                                                kKeyField: params[kKeyField]};
                
                if (delegateRespondsToSel)
                {
                    [delegate parsedType:SCPBManagerParceTypeSetProfileField data:transportDict];
                }
            }
        }
    }
    else
        FTLog(@"Error transportPUUIDstring");
}


/* ================================================================================
message RemoveProfileFieldRequest {
    repeated FieldType fields = 1; /// Information fields type
}
 в params ключ @"fields" массив полей для удаления SCFieldType
 */

-(void) removeProfileField:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = params[@"UUID"];
        _delegateDictionary[[transportPUUID scpuuidToString]] = params;
        NSArray* scFieldArray =params[@"fields"];
        
        RemoveProfileFieldRequestBuilder* removeProfileFieldRequestBuilder = [[RemoveProfileFieldRequestBuilder alloc] init];
        for(NSUInteger i = 0; i < scFieldArray.count;i++)
        {
            SCFieldType scFieldType = (SCFieldType)[[scFieldArray objectAtIndex:i] integerValue];
            [removeProfileFieldRequestBuilder addFields:[self scFieldTypeToFieldType:scFieldType]];
        }
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeRemoveProfileField];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[removeProfileFieldRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

-(void) parseRemoveProfileField:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = params[@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        NSDictionary *transportDict = @{kKeySuccess: [NSNumber numberWithBool:response.status.success],
                                        @"fields": params[@"fields"]
                                        };
        
        if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
        {
            [delegate parsedType:SCPBManagerParceTypeRemoveProfileField data:transportDict];
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}
/* ================================================================================
message CheckIsNicknameFreeRequest {
    required string nick = 1;
}*/

-(void) checkIsNicknameFree:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        NSString *newNick = params[@"contactNickName"];
        
        CheckIsNicknameFreeRequestBuilder* _checkIsNicknameFreeRequestBuilder = [[CheckIsNicknameFreeRequestBuilder alloc] init];
        [_checkIsNicknameFreeRequestBuilder setNick:newNick];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeCheckIsNicknameFree];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_checkIsNicknameFreeRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

/*
message CheckIsNicknameFreeResponse {
    required bool isFree = 1;
}*/
-(void) parseCheckIsNicknameFree:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = params[@"delegate"];
        id nickName = params[@"contactNickName"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            CheckIsNicknameFreeResponse* _checkIsNicknameFreeResponse = [CheckIsNicknameFreeResponse parseFromData:response.response];
            BOOL isFree = _checkIsNicknameFreeResponse.isFree;
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                NSDictionary *sendDictionary = @{@"contactNickName": nickName,
                                                 @"isNicknameFree":[NSNumber numberWithBool:isFree]};
                
                [delegate parsedType:SCPBManagerParceTypeCheckIsNicknameFree data:sendDictionary];
            }
        }
        else
            FTLog(@"Error parseSetProfileField response.status.success");
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}

/* ================================================================================
Change status of user
message ChangeStatusRequest {
    required StatusType status = 1; /// Status type (**ONLINE**, **AWAY**, ...)
    }
*/
-(void) changeStatus:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        SCUserStatus status = (SCUserStatus)[[params objectForKey:@"status"] integerValue];
        
        ChangeStatusRequestBuilder* changeStatusRequestBuilder = [[ChangeStatusRequestBuilder alloc] init];
        [changeStatusRequestBuilder setStatus:[self scContactStatusToStatus:status]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeUserUpdateStatus];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[changeStatusRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

-(void) parseChangeStatus:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
            SCContactItem* contactItem = [params objectForKey:@"SCContactItem"];
            SCUserStatus status = (SCUserStatus)[[params objectForKey:@"status"] integerValue];
            contactItem.contactStatus = status;
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeChangeStatus data:contactItem];
            }
        }
        else
        {
            id<SCPBManagerDelegate>delegateProfile = [params objectForKey:@"delegateProfile"];
            
            NSMutableDictionary *transportDic = [[NSMutableDictionary alloc] init];
            
            transportDic[kKeySuccess] = [NSNumber numberWithBool:response.status.success];
            
            if (delegateProfile && [delegateProfile respondsToSelector:@selector(parsedType:data:)])
            {
                [delegateProfile parsedType:SCPBManagerParceTypeChangeStatus data:transportDic];
            }
        }
        
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
    
    
}

/* ================================================================================
 PING
*/
-(void) pingServerStatus:(SCPingStatus) pingStatus
{
    switch (pingStatus)
    {
        case SCPingStatusStart:
        {
            if(_pingTimer)
            {
                [_pingTimer invalidate];
                _pingTimer = nil;
            }
            _pingTimer = [NSTimer scheduledTimerWithTimeInterval:pingInterval target:self selector:@selector(pingToServer) userInfo:nil repeats:YES];
            
        }
            break;
            
        case SCPingStatusStop:
        {
            if(_pingTimer)
            {
                [_pingTimer invalidate];
                _pingTimer = nil;
            }
        }
            break;
            
        default:
            break;
    }
}

-(void) pingToServer
{
//    FTLog(@"Send ping");
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypePing];
    [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
    Request * query = [_requestBuilder build];
    [_socketClient sendData:[query data]];
}
/* 
 no responce
*/
/* ================================================================================
message LogoutRequest {
    optional bool isAllDevices = 1 [default = false];
}
*/
-(void) logout:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeLogout];

        if([[params allKeys] containsObject:@"isAllDevices"])
        {
            LogoutRequestBuilder* _logoutRequestBuilder = [[LogoutRequestBuilder alloc] init];
            [_logoutRequestBuilder setIsAllDevices:[[params objectForKey:@"SCUserAuthItem"] boolValue]];
            [_requestBuilder setRequest:[_logoutRequestBuilder build].data];
        }
        
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
// подготовка к логауту
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
        {
            [delegate parsedType:SCPBManagerParceTypeLogout data:nil];
        }

        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
}

// Не попадает сюда 
-(void) parseLogout:(id) data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeLogout data:nil];
            }
        }
        else
        {
            FTLog(@"Error parseLogout response.status.success = NO");
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthRemove data:[self makeErrorDictionary:response]];
            }
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}


#pragma mark - AUTH
/* ================================================================================
MARK: AddAuth
 
message AddAuthRequest {
    required Auth auth = 1; /// Authentication data
}
params keys:
 UUID
 delegate
 SCUserAuthItem (class SCUserAuthItem) or Auth (class Auth)
 */

-(void) authAdd:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        Auth* auth;
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];

        if([[params allKeys] containsObject:@"SCUserAuthItem"])
        {
            SCUserAuthItem* userAuth = [params objectForKey:@"SCUserAuthItem"];
            AuthBuilder *authBuilder = [[AuthBuilder alloc] init];
            [authBuilder setType:[self theAuth:userAuth.authType]];
            [authBuilder setValue:(NSString*) userAuth.data];
            auth = [authBuilder build];
        }
        else if([[params allKeys] containsObject:@"Auth"])
        {
            auth = [params objectForKey:@"Auth"];
        }
        else
        {
            FTLog(@"Error no param");
            return;
        }

        AddAuthRequestBuilder* addAuthRequestBuilder = [[AddAuthRequestBuilder alloc] init];
        [addAuthRequestBuilder setAuth:auth];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeAuthAdd];
        [_requestBuilder setRequest:[addAuthRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

-(void) parseAuthAdd:(id) data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    SCUserAuthItem* userAuth = nil;
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        if([[params allKeys] containsObject:@"SCUserAuthItem"])
        {
            userAuth = [params objectForKey:@"SCUserAuthItem"];
        }
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthAdd data:userAuth];
            }

        }
        else
        {
            FTLog(@"Error parseAuthAdd response.status.success = NO");
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthAdd data:[self makeErrorDictionary:response]];
            }
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}

/* ================================================================================
 MARK: ConfirmAuth
 message ConfirmAuthRequest {
    required Auth auth = 1; /// Authentication data
    required string code = 2; /// Confirmation code
}
params keys:
 UUID
 delegate
 SCUserAuthItem (class SCUserAuthItem) or Auth (class Auth)
 VerifyCode
 */
-(void) authConfirm:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        Auth* auth;
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        if([[params allKeys] containsObject:@"SCUserAuthItem"])
        {
            SCUserAuthItem* userAuth = [params objectForKey:@"SCUserAuthItem"];
            AuthBuilder *authBuilder = [[AuthBuilder alloc] init];
            [authBuilder setType:[self theAuth:userAuth.authType]];
            [authBuilder setValue:(NSString*) userAuth.data];
            auth = [authBuilder build];
        }
        else if([[params allKeys] containsObject:@"Auth"])
        {
            auth = [params objectForKey:@"Auth"];
        }
        else
        {
            FTLog(@"Error no param");
            return;
        }
        
        if([[params allKeys] containsObject:@"VerifyCode"])
        {
            ConfirmAuthRequestBuilder* confirmAuthRequestBuilder = [[ConfirmAuthRequestBuilder alloc] init];
            [confirmAuthRequestBuilder setAuth:auth];
            [confirmAuthRequestBuilder setCode:[params objectForKey:@"VerifyCode"]];
            
            RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
            [_requestBuilder setType:CallTypeAuthConfirm];
            [_requestBuilder setRequest:[confirmAuthRequestBuilder build].data];
            [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
            
            Request * query = [_requestBuilder build];
            [_socketClient sendData:[query data]];
        }
        else
        {
            FTLog(@"Error no param");
            return;
        }
    }

}

-(void) parseAuthConfirm:(id) data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    SCUserAuthItem* userAuth = nil;
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        if([[params allKeys] containsObject:@"SCUserAuthItem"])
        {
            userAuth = [params objectForKey:@"SCUserAuthItem"];
        }
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            if(userAuth)
            {
                userAuth.isConfirm = YES;
            }
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthConfirm data:userAuth];
            }
        }
        else
        {
            FTLog(@"Error parseAuthConfirm response.status.success = NO");
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthConfirm data:[self makeErrorDictionary:response]];
            }
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}

/* =======================================================================================
MARK: RemoveAuth
 message RemoveAuthRequest {
    required Auth auth = 1; /// Authentication data
}
params keys:
 UUID
 delegate
 SCUserAuthItem (class SCUserAuthItem) or Auth (class Auth)
 */

-(void) authRemove:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        Auth* auth;
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        if([[params allKeys] containsObject:@"SCUserAuthItem"])
        {
            SCUserAuthItem* userAuth = [params objectForKey:@"SCUserAuthItem"];
            AuthBuilder *authBuilder = [[AuthBuilder alloc] init];
            [authBuilder setType:[self theAuth:userAuth.authType]];
            [authBuilder setValue:(NSString*) userAuth.data];
            auth = [authBuilder build];
        }
        else if([[params allKeys] containsObject:@"Auth"])
        {
            auth = [params objectForKey:@"Auth"];
        }
        else
        {
            FTLog(@"Error no param");
            return;
        }
        
        RemoveAuthRequestBuilder* removeAuthRequestBuilder = [[RemoveAuthRequestBuilder alloc] init];
        [removeAuthRequestBuilder setAuth:auth];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeAuthRemove];
        [_requestBuilder setRequest:[removeAuthRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }

}
-(void) parseAuthRemove:(id) data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    SCUserAuthItem* userAuth = nil;
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        if([[params allKeys] containsObject:@"SCUserAuthItem"])
        {
            userAuth = [params objectForKey:@"SCUserAuthItem"];
        }
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthRemove data:userAuth];
            }
        }
        else
        {
            FTLog(@"Error parseAuthAdd response.status.success = NO");
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthRemove data:[self makeErrorDictionary:response]];
            }
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}

/* =======================================================================================
 MARK: authGet
 */
-(void) authGet:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeAuthGet];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

/* ================================================================================
 message GetAuthResponse {
 repeated Auth auth = 1;
 }*/
-(void) parseAuthGet:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];

        if(response.status.success)
        {
            
            NSArray *arAuth = [GetAuthResponse parseFromData:response.response].auth;
            NSInteger typeCall = -1;
            if([[params allKeys] containsObject:@"type"])
                typeCall = [[params objectForKey:@"type"] integerValue];
            if(typeCall == SCPBManagerParceTypeGetProfile)
            {
                for(Auth* auth in arAuth)
                {
                    if(auth.confirmed)
                    {
                        [params setObject:auth forKey:@"Auth"];
                        [self getUserByAuth:params];
                        break;
                    }
                }
            }
            else
            {
                FTLog(@"");
                NSMutableArray * array = [[NSMutableArray alloc]init];
                for(Auth* auth in arAuth)
                {
//                    NSString * str = auth.value;
//                    SCUserAuthItem * item = [[SCUserAuthItem alloc] initByType:[self theUserAuth:auth.type] WithData:str];

                    SCUserAuthItem * item = [[SCUserAuthItem alloc]initWithType:[self theUserAuth:auth.type]andData:auth.value andConfirm:auth.confirmed];
                        [array addObject:item];
                    
   
                }
                
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeAuthGet data:array];
                }
            }
        }
        else
        {
            FTLog(@"Error parseAuthGet response.status.success = NO");
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeAuthGet data:[self makeErrorDictionary:response]];
            }
        }
    }
    else
        FTLog(@"Error transportPUUIDstring");
}
/* ================================================================================
message SubscribeToAmazonSNSRequest {
    required string deviceToken = 1; /// Apple device token
}*/
-(void) subscribeToAmazonSNS:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        NSString* deviceToken = [params objectForKey:@"deviceToken"];
        
        SubscribeToAmazonSNSRequestBuilder* _subscribeToAmazonSNSRequestBuilder = [[SubscribeToAmazonSNSRequestBuilder alloc] init];
        [_subscribeToAmazonSNSRequestBuilder setDeviceToken:deviceToken];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeSubscribeToAmazonSns];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_subscribeToAmazonSNSRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

-(void) parseSubscribeToAmazonSNS:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];

        NSMutableDictionary *transportDic = [[NSMutableDictionary alloc] init];
        if(response.status.success)
        {
            [transportDic setObject:[NSNumber numberWithBool:YES] forKey:@"result"];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeSubscribeToAmazonSNS data:transportDic];
            }
        }
        else
        {
            [transportDic setObject:[NSNumber numberWithBool:NO] forKey:@"result"];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeSubscribeToAmazonSNS data:transportDic];
            }
        }
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}



/* ================================================================================
message UnsubscribeFromAmazonSNSRequest {
    required string deviceToken = 1; /// Apple device token
}*/
-(void) unSubscribeFromAmazonSNS:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        NSString* deviceToken = [params objectForKey:@"deviceToken"];
        
        UnsubscribeFromAmazonSNSRequestBuilder* _unsubscribeFromAmazonSNSRequestBuilder = [[UnsubscribeFromAmazonSNSRequestBuilder alloc] init];
        [_unsubscribeFromAmazonSNSRequestBuilder setDeviceToken:deviceToken];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeUnsubscribeFromAmazonSns];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_unsubscribeFromAmazonSNSRequestBuilder build].data];
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}


-(void) parseUnsubscribeFromAmazonSns:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSDictionary* params = [_delegateDictionary objectForKey:transportPUUIDstring];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        
        NSMutableDictionary *transportDic = [[NSMutableDictionary alloc] init];
        if(response.status.success)
        {
            [transportDic setObject:[NSNumber numberWithBool:YES] forKey:@"result"];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeUnsubscribeFromAmazonSNS data:transportDic];
            }
        }
        else
        {
            [transportDic setObject:[NSNumber numberWithBool:NO] forKey:@"result"];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeUnsubscribeFromAmazonSNS data:transportDic];
            }
        }
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}

#pragma mark - Notifications функции нотификаций (events) СЮДЫ
-(void) parseAuthorizationFailedEvent:(id)data
{
    if([_settings.modelProfile respondsToSelector:@selector(parsedType:data:)])
        [_settings.modelProfile parsedType:SCPBManagerParceTypeAuthorizationFailedEvent data:nil];
}



@end
