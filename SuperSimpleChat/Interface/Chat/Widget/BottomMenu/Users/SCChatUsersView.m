//
//  SCChatUsersView.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 28.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSettings.h"
#import "SCmodelContacts.h"

#import "SCChatUsersView.h"
#import "SCChatUserCell.h"


#define CHAT_USERS_TABLEVIEW_CELL @"SCChatListCell"
#define CHAT_USERS_TABLEVIEW_CELL_HEIGHT 56

@implementation SCChatUsersView
{
    void (^afterSelect) (SCChatUsersViewItem selectItem);
    NSMutableArray* _rowsArray;
    BOOL inShowAdd;
    NSMutableArray* userInRoomCurrent;
    
    UIActivityIndicatorView* _activityIndicatorView;
}


-(id) init
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}


-(void) _init
{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self addSubview:_tableView];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicatorView.hidesWhenStopped = YES;
    [self addSubview:_activityIndicatorView];
    
    self.clipsToBounds = YES;
    self.layer.borderWidth = 1;
    _inAddMode = NO;
    
    [_tableView registerNib:[UINib nibWithNibName:@"SCChatUserCell" bundle:nil] forCellReuseIdentifier:CHAT_USERS_TABLEVIEW_CELL];
}

-(void) dealloc
{
    FTLog(@"SCChatUsersView dealloc");
}

-(void) setAfterSelect:(void(^)(SCChatUsersViewItem selectItem)) completionBlock
{
    afterSelect = [completionBlock copy];
}

-(void) setInAddMode:(BOOL)inAddMode
{
    _inAddMode = inAddMode;
    inShowAdd = _inAddMode;
}


-(void) layoutSubviews
{
    [super layoutSubviews];
    _tableView.frame = self.frame;
    _activityIndicatorView.center = CGPointMake(self.frame.size.width*0.5, self.frame.size.height*0.5);
}


+(CGSize) sizeView
{
    return CGSizeMake(320, 400);
}

-(void) setUserInRoom:(NSMutableArray *)userInRoom
{
    _userInRoom = userInRoom;
    userInRoomCurrent = [_userInRoom mutableCopy];
}

- (NSArray *) makeActionButtons
{
    NSMutableArray *actionButtons = [NSMutableArray new];
//    [actionButtons sw_addUtilityButtonWithColor:[UIColor greenColor] title:@"Add"];
    [actionButtons sw_addUtilityButtonWithColor:[UIColor redColor] title:@"Del"];
    
    return actionButtons;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _userInRoom.count;
}
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SCContactItem* curContact = [_userInRoom objectAtIndex:indexPath.row];
    
    SCChatUserCell* chatUserCell = [tableView dequeueReusableCellWithIdentifier:CHAT_USERS_TABLEVIEW_CELL];
    if(!chatUserCell)
        chatUserCell = [[SCChatUserCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CHAT_USERS_TABLEVIEW_CELL];
    
    if([self isContain:_userInRoom byItem:curContact])
        chatUserCell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        chatUserCell.accessoryType = UITableViewCellAccessoryNone;
    
    chatUserCell.textLabel.text = [NSString stringWithFormat:@"%@ %@",curContact.contactFirstName,curContact.contactLastName];
    if(curContact.contactNikName.length >0)
    {
        chatUserCell.textLabel.text = curContact.contactNikName;
    }
    else
    {
        NSString* strName = @"";
        if(curContact.contactFirstName.length)
            strName = [strName stringByAppendingFormat:@"%@",curContact.contactFirstName];
        if(curContact.contactLastName.length)
            strName = [strName stringByAppendingFormat:@" %@",curContact.contactLastName];
        chatUserCell.textLabel.text = strName;
    }

    chatUserCell.contentView.translatesAutoresizingMaskIntoConstraints = NO;//?
    if(!inShowAdd)
        [chatUserCell setRightUtilityButtons:[self makeActionButtons]];
    chatUserCell.delegate = self;
    chatUserCell.backgroundColor = CL_CLEAR_COLOR;
    return chatUserCell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    SCContactItem* curContact = [_rowsArray objectAtIndex:indexPath.row];
//    if([self isContain:userInRoomCurrent byItem:curContact])
//    {
//        [userInRoomCurrent removeObject:curContact];
//    }
//    else
//    {
//        [userInRoomCurrent addObject:curContact];
//    }
//    [_tableView reloadData];
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
            
        case 0:
        {
            FTLog(@"del button was pressed");
            [cell hideUtilityButtonsAnimated:YES];
            
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            SCContactItem* curContact = [_userInRoom objectAtIndex:cellIndexPath.row];
            NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                           self, @"delegate",
                                           [[SCPUUID alloc] initWithRandom], @"UUID",
                                           [NSNumber numberWithInteger:SCPBManagerParceTypeLeaveUserFromRoom], @"type",
                                           _chatItem,@"SCChatItem",
                                           curContact,@"SCContactItem",
                                           nil];
            
            [_settings.modelProfile.protoBufManager leaveUserFromRoomRequest:params];
            
//            [_userInRoom removeObject:curContact];
            
            [_activityIndicatorView startAnimating];
//?
//            [_tableView beginUpdates];
//            [_tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//            [_tableView endUpdates];
//?
        }
            break;
            
        default:
            break;
    }
}

-(BOOL) isContain:(NSArray*) inArray byItem:(SCContactItem*) byItem
{
    for(SCContactItem* theItem in inArray)
    {
        if([theItem.userPUUID isEqual:byItem.userPUUID])
            return YES;
    }
    return NO;
}

-(NSMutableArray*) apply
{
    for(SCContactItem* curContact in userInRoomCurrent)
    {
        if(![self isContain:_userInRoom byItem:curContact])
        {
            FTLog(@"Save %@",curContact.contactPUUID);
        }
    }
    for(SCContactItem* curContact in _userInRoom)
    {
        if(![self isContain:userInRoomCurrent byItem:curContact])
        {
            FTLog(@"delete %@",curContact.contactPUUID);
        }
    }
    return _userInRoom;
}

#pragma mark SCPBManagerDelegate <NSObject>
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"SCChatViewController : receiveParsedData");
    switch (type)
    {
        case SCPBManagerParceTypeLeaveUserFromRoom:
        {
            [_activityIndicatorView stopAnimating];
            if([data isKindOfClass:[SCContactItem class]])
            {
                NSInteger idx = [_userInRoom indexOfObject:data];
                [_userInRoom removeObject:data];
                if(idx != NSNotFound)
                {
                    NSIndexPath* indexPath =[NSIndexPath indexPathForRow:idx inSection:0];
                    [_tableView beginUpdates];
                    [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    [_tableView endUpdates];
                }
            }
        }
            break;
            
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;
    }
}
@end
