//
//  SCChatLogoView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 28.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatLogoView.h"
#import "SCSettings.h"
#import "UIImage+SCCropImageToCircleWithBorder.h"
#import "UIImageView+Letters.h"
#import "SCContactItem.h"
#import "SCChatItem.h"


#define ICON_TYPE_SIZE 16
#define ICON_TYPE_MARGIN 1

static NSString *kStatusOnlineImgName = @"status-online";
static NSString *kStatusBusyImgName = @"status-busy";
static NSString *kStatusAwayImgName = @"status-away";
static NSString *kStatusInvisibleImgName = @"status-invisible";
static NSString *kStatusOfflineImgName = @"status-offline";

@implementation SCChatLogoView
{
    UIImageView* _previewImage;
    UIImageView* _iconStatus;
    id _source;
    SCNetFile* _netFile;
}


- (instancetype)initWithSettings:(SCSettings *)settings
{
    self = [super initWithSettings:settings];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
    
    [self reFrame:self.frame];
}

-(void) setSettings:(SCSettings *)settings
{
    [super setSettings:settings];
}

-(void) _init
{
    _previewImage = [[UIImageView alloc] init];
    _previewImage.contentMode = UIViewContentModeScaleAspectFit;
    
    _iconStatus = [[UIImageView alloc] init];
    _iconStatus.contentMode = UIViewContentModeScaleAspectFit;
    
    [self addSubview:_previewImage];
    [self addSubview:_activityIndicatorView];
    [self.layer addSublayer:shadowLayer];
    [self.layer addSublayer:shapeLayer];
    [self addSubview:_iconStatus];
}

- (void)dealloc
{
    
}

-(void) clear
{
    [_netFile clearCompletion];
    if([_source isKindOfClass:[SCContactItem class]])
    {
        [(SCContactItem*)_source delDelegate:self];
    }
    else if([_source isKindOfClass:[SCChatItem class]])
    {
        [(SCChatItem*) _source delDelegate:self];
    }
    _iconStatus.hidden = YES;
    _netFile = nil;
    _source = nil;
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        [self reFrame:frame];
    }
}

-(void) reFrame:(CGRect) frame
{
    shadowLayer.frame = self.bounds;
    shapeLayer.frame = self.bounds;
    _activityIndicatorView.center = CGPointMake(frame.size.width/2, frame.size.height/2);;
    
    _previewImage.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    _iconStatus.frame = CGRectMake(frame.size.width - ICON_TYPE_SIZE - ICON_TYPE_MARGIN, frame.size.height - ICON_TYPE_SIZE - ICON_TYPE_MARGIN, ICON_TYPE_SIZE, ICON_TYPE_SIZE);
}

-(void) setStatus:(SCNetFileStatus) status
{
    [super setStatus:status];
    switch (status)
    {
        case SCNetFileStatusNew:
        case SCNetFileStatusStart:
        case SCNetFileStatusWait:
        case SCNetFileStatusTryRepeet:
        case SCNetFileStatusProgress:
        case SCNetFileStatusError:
            self.backgroundColor = [UIColor clearColor];;
            break;
        case SCNetFileStatusCompleet:
            self.backgroundColor = [UIColor clearColor];
            break;
            
        default: break;
    }
}

-(void) setLogoSource:(id) source
{
    _source = source;
    
    if([_source isKindOfClass:[SCContactItem class]])
    {
        [(SCContactItem*)_source addDelegate:self];
        _netFile = ((SCContactItem*)_source).netFile;
        [_previewImage setImageWithString:((SCContactItem*)_source).contactNikName color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
        _iconStatus.hidden = NO;
        [self changeStatus:((SCContactItem*)_source).contactStatus];
    }
    else if([_source isKindOfClass:[SCChatItem class]])
    {
        [(SCChatItem*) _source addDelegate:self];
        _netFile = ((SCChatItem*)_source).netFile;
    }
    _previewImage.image = [_settings.skinManager getImageForKeyX:@"circle-no-avatar-bg"];
    [_netFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                [self setStatus:[result doubleValue]];
                break;
                
            case SCNetFileComletionTypeProgress:
                [self setProgressPercent:[result doubleValue]];
                break;
                
            case SCNetFileComletionTypeError:
                FTLog(@"%@ SCNetFileComletionTypeError обработать",NSStringFromClass([self class]));
                if([_source isKindOfClass:[SCContactItem class]])
                {
                    [_previewImage setImageWithString:((SCContactItem*)_source).contactNikName color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
                }
                else if([_source isKindOfClass:[SCChatItem class]])
                {
                    [_previewImage setImageWithString:((SCChatItem*)_source).chatName color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
                }
//                [_imageChat endProgress];
                break;
                
            case SCNetFileComletionTypeCompleet:
                _previewImage.image = [[(SCNetFile*)result imageForSize:_previewImage.frame.size] cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
                break;
                
            default:
                break;
        }
    }];
    [self setNeedsLayout];
}

//-(void) setIconType:(UIImage*) img
//{
//    _iconType.image = img;
//}

//-(void) setPreviewImage:(UIImage*) img
//{
//    _previewImage.image = [img cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
//}

-(void)changeStatus:(SCUserStatus) status{
    
    switch (status) {
            
        case SCUserStatusAway:
            
            _iconStatus.image = [[_settings.skinManager getImageForKeyX:kStatusAwayImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            
            break;
            
        case SCUserStatusBusy:
            
            _iconStatus.image = [[_settings.skinManager getImageForKeyX:kStatusBusyImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            
            break;
            
        case SCUserStatusInvisible:
            
            _iconStatus.image = [[_settings.skinManager getImageForKeyX:kStatusInvisibleImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            
            
            break;
            
        case SCUserStatusOffline:
            
            _iconStatus.image = [[_settings.skinManager getImageForKeyX:kStatusInvisibleImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            
            
            break;
            
        case SCUserStatusOnline:
            
            _iconStatus.image = [[_settings.skinManager getImageForKeyX:kStatusOnlineImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            
            
            break;
            
        default:
            break;
            
    }
    
}

#pragma mark - SCPBManagerDelegate
-(void)parsedType:(SCPBManagerParceType)type data:(id)data
{
    
    switch (type)
    {
        case SCPBManagerParceTypeChangeStatusEvent:
        case SCPBManagerParceTypeChangeStatus:
        {
            if([data isKindOfClass:[SCContactItem class]])
                
                [self changeStatus:((SCContactItem*) data).contactStatus];
        }
            break;
            
        case SCPBManagerParceTypeGetChatRoomHistory:break;
        case SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange:break;
            
        default:break;
    }
}


@end
