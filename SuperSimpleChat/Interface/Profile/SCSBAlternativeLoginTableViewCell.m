//
//  SCSBAlternativeLoginTableViewCell.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSBAlternativeLoginTableViewCell.h"

@implementation SCSBAlternativeLoginTableViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
    [self setupUI];
}

- (void) _init
{
    
}

- (void) setupUI
{
    
}

- (instancetype)init
{
    [self _init];
    self = [super init];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    [self _init];
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    _titleLabel.frame = CGRectMake(_edgeInsets.left + self.contentView.frame.size.height, 0.0,
                                   self.contentView.frame.size.width - _edgeInsets.left - _edgeInsets.right - self.contentView.frame.size.height,
                                   self.contentView.frame.size.height);
}

- (void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    _titleLabel.textColor = SKCOLOR_TableCellFont;
}

@end
