//
//  SCButtonDelete.m
//  SmartChat
//
//  Created by Yury Radchenko on 2/25/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCButtonDelete.h"
#import "Localiser.h"

@implementation SCButtonDelete
{
    Localiser  *_localizer;
}

- (id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    _localizer = [Localiser sharedLocaliser];
    
    [self setTitle:LOCAL(@"Delete") forState:UIControlStateNormal];
    [self setBackgroundColor:CL_RED_COLOR];

    return self;
}

@end
