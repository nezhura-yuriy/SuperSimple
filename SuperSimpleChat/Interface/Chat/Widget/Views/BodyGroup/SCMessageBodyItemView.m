//
//  SCMessageBodyItemView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyItemView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCNetFile.h"

#define SEPARATOR_HEIGHT 2.0f
#define degreesToRadians(x) (((x-90.0) * M_PI / 180.0))

@implementation SCMessageBodyItemView
{
    
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super initWithSettings:settings];
    if (self)
    {
        _settings = settings;

        
//        [self _init];
    }
    return self;
}

//-(void) _init
//{
////    self.layer.borderWidth = 1;
////    self.layer.borderColor = [[UIColor greenColor] CGColor];
//    
//    shadowLayer = [CAShapeLayer layer];
//    shadowLayer.backgroundColor = [[UIColor clearColor] CGColor];
//    
//    shapeLayer = [CAShapeLayer layer];
//    shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
//
//    _activityIndicatorView = [[UIActivityIndicatorView alloc] init];
//    _activityIndicatorView.hidesWhenStopped = YES;
//    _activityIndicatorView.backgroundColor = [UIColor clearColor];
//
//}

-(void) dealloc
{
    
}

-(void) setMessageBodys:(SCMessageBodys*) messageBodys
{
    
}


-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        shadowLayer.frame = self.bounds;
        shapeLayer.frame = self.bounds;
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
}


-(CGFloat) getItemWidth
{
    return _itemWidth;
}

-(CGSize) viewSize
{
    return CGSizeMake(MARGIN, MARGIN);
}

@end



