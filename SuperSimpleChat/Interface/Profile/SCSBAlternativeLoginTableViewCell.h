//
//  SCSBAlternativeLoginTableViewCell.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"
#import "SCCellLabel.h"

@interface SCSBAlternativeLoginTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icoImageView;
@property (weak, nonatomic) IBOutlet SCCellLabel *titleLabel;

@property (nonatomic) UIEdgeInsets edgeInsets;

@property (strong, nonatomic) SCSettings *settings;

@end
