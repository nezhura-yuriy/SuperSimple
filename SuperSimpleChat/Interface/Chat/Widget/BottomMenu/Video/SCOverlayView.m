//
//  SCOverlayView.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 08.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCOverlayView.h"

typedef enum  {
    SCCurrentStatePhoto,
    SCCurrentStateVideoNoRecord,
    SCCurrentStateVideoRecording
} SCCurrentState;

static const CGFloat kAlphaViewMax = 0.6f;

@implementation SCOverlayView
{
    SCCurrentState _currentState;
    NSTimer *_timerRecordVideo;
    NSTimer *_timerUpdateTimeLabel;
    NSDate *_timeStartRecordVideo;
    NSDate *_timeEndRecordVideo;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
    [self setupUI];
}

- (void) _init
{
    //default value
    _currentMode = SCCurrentModePhoto;
    _currentFlashMode = UIImagePickerControllerCameraFlashModeAuto;
}

- (void) setupUI
{
    self.backgroundColor = [UIColor clearColor];
    
//    self.headerView.alpha = kAlphaViewMax;
//    self.headerView.backgroundColor = [UIColor whiteColor];
    
//    self.footerView.alpha = kAlphaViewMax;
//    self.footerView.backgroundColor = [UIColor whiteColor];
}

- (id)init
{
    [self _init];
    self = [super init];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    [self _init];
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    [self _init];
    
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
//    self.layer.borderColor = CL_DEBUG_BLUE_COLOR;
//    self.layer.borderWidth = 1;
    
    self.frame = CGRectMake(0, 0, SZ_SCREEN_WIDTH, SZ_SCREEN_HEIGHT);
}

- (void) dealloc {
    [self stopTimers];
}
//MARK
- (void) setCurrentMode:(SCCurrentMode)currentMode
{
    _currentMode = currentMode;
    
    if (_currentMode == SCCurrentModePhoto) {
        [self setupState:SCCurrentStatePhoto];
        
    } else if (_currentState == SCCurrentModeVideo) {
        [self setupState:SCCurrentStateVideoNoRecord];
    }
    
    //[self setNeedsDisplay];
}

//MARK: Flash
- (void) setCurrentFlashMode:(UIImagePickerControllerCameraFlashMode)currentFlashMode
{
    _currentFlashMode = currentFlashMode;
    [self setFlashButtonMode:_currentFlashMode];
}

- (void) setFlashButtonMode: (UIImagePickerControllerCameraFlashMode)flashMode
{
    [_flashButton setTitle:[self titleFlashBy:flashMode] forState:UIControlStateNormal];
}

- (NSString *) titleFlashBy: (UIImagePickerControllerCameraFlashMode)flashMode
{
    switch (flashMode) {
        case UIImagePickerControllerCameraFlashModeOff: {
            return @"Off";
            break;
        }
        case UIImagePickerControllerCameraFlashModeAuto: {
            return @"Auto";
            break;
        }
        case UIImagePickerControllerCameraFlashModeOn: {
            return @"On";
            break;
        }
        default: {
            return @"Auto";
            break;
        }
    }
}

- (UIImagePickerControllerCameraFlashMode) nextThe: (UIImagePickerControllerCameraFlashMode) flashMode
{
    switch (flashMode) {
        case UIImagePickerControllerCameraFlashModeOff: {
            return UIImagePickerControllerCameraFlashModeAuto;
            break;
        }
        case UIImagePickerControllerCameraFlashModeAuto: {
            return UIImagePickerControllerCameraFlashModeOn;
            break;
        }
        case UIImagePickerControllerCameraFlashModeOn: {
            return UIImagePickerControllerCameraFlashModeOff;
            break;
        }
        default: {
            return UIImagePickerControllerCameraFlashModeOff;
            break;
        }
    }
}

//MARK: IBActions
- (IBAction)cancelButtonPressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(cancel)]) {
        [_delegate cancel];
    }
}

- (IBAction)changeCameraButtonPressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(changeCameraDevice)]) {
        [_delegate changeCameraDevice];
    }
}

- (IBAction) flashButtonPressed: (id) sender
{
    _currentFlashMode = [self nextThe:_currentFlashMode];
    [self setFlashButtonMode:_currentFlashMode];
}

- (IBAction)videoModeButtonPressed:(id)sender
{
    [self setupState:SCCurrentStateVideoNoRecord];
}

- (IBAction)photoModeButtonPressed:(id)sender
{
    [self setupState:SCCurrentStatePhoto];
}

- (IBAction)actionButtonPressed:(id)sender
{
    if (_currentState == SCCurrentStatePhoto) {
        if ([_delegate respondsToSelector:@selector(takePicture)]) {
            [_delegate takePicture];
        }
        
    } else if (_currentState == SCCurrentStateVideoNoRecord) {
        if ([_delegate respondsToSelector:@selector(startVideoCapture)]) {
            [_delegate startVideoCapture];
            [self setupState:SCCurrentStateVideoRecording];
        };
        
    } else if (_currentState == SCCurrentStateVideoRecording) {
        if ([_delegate respondsToSelector:@selector(stopVideoCapture)]) {
            [_delegate stopVideoCapture];
            //[self setupState:SCCurrentStateVideoNoRecord];
        }
    }
}

//MARK: CurrentState
- (void) setupState:(SCCurrentState) currentState
{
    _currentState = currentState;
    
    if (_currentState == SCCurrentStatePhoto) {
        _videoModeButton.enabled = YES;
        _videoModeButton.hidden = NO;
        _videoModeButton.alpha = 1.0;
        
        _photoModeButton.enabled = NO;
        _photoModeButton.hidden = NO;
        _photoModeButton.alpha = 0.5;
        
        _flashButton.hidden = NO;
        _timeVideoLabel.hidden = YES;
        _changeCameraButton.hidden = NO;
        _cancelButton.hidden = NO;
        
        self.headerView.backgroundColor = [UIColor darkGrayColor];
        self.footerView.backgroundColor = [UIColor darkGrayColor];
        
    } else if (_currentState == SCCurrentStateVideoNoRecord) {
        
        _videoModeButton.enabled = NO;
        _videoModeButton.hidden = NO;
        _videoModeButton.alpha = 0.5;
        
        _photoModeButton.enabled = YES;
        _photoModeButton.hidden = NO;
        _photoModeButton.alpha = 1.0;
        
        _flashButton.hidden = NO;
        _timeVideoLabel.hidden = NO;
        _changeCameraButton.hidden = NO;
        _cancelButton.hidden = NO;
        
        self.headerView.backgroundColor = [UIColor darkGrayColor];
        self.footerView.backgroundColor = [UIColor darkGrayColor];
        
    } else if (_currentState == SCCurrentStateVideoRecording) {
        
        _videoModeButton.enabled = NO;
        _videoModeButton.hidden = YES;
        
        _photoModeButton.enabled = NO;
        _photoModeButton.hidden = YES;

        _flashButton.hidden = YES;
        _timeVideoLabel.hidden = NO;
        _changeCameraButton.hidden = YES;
        _cancelButton.hidden = YES;
        
        //[UIView animateWithDuration:0.5 animations:^{
            self.headerView.backgroundColor = [UIColor clearColor];
            self.footerView.backgroundColor = [UIColor clearColor];
        //}];
        
        [self startChangeTimerLabel];
        
    }
    
    [self setActionButtonState:_currentState];
}

- (void) setActionButtonState: (SCCurrentState) currentState
{
    if (_currentState == SCCurrentStatePhoto) {
        [_actionButton setTitle:@"Take Photo" forState:UIControlStateNormal];
        
    } else if (_currentState == SCCurrentStateVideoNoRecord) {
        [_actionButton setTitle:@"Record video" forState:UIControlStateNormal];
        
    } else if (_currentState == SCCurrentStateVideoRecording) {
        [_actionButton setTitle:@"Stop record" forState:UIControlStateNormal];
    }
}

//MARK: Time Label
- (void) setVideoMaximumDuration:(NSTimeInterval)videoMaximumDuration
{
    _videoMaximumDuration = videoMaximumDuration;
    _timeVideoLabel.text = [[NSString alloc] initWithFormat:@"00:00 - %@", [self formatSecond:_videoMaximumDuration]];
}

- (void) startChangeTimerLabel
{
    [_timerRecordVideo invalidate];
    _timerRecordVideo = [NSTimer scheduledTimerWithTimeInterval:_videoMaximumDuration
                                                         target:self
                                                       selector:@selector(stopVideo)
                                                       userInfo:nil
                                                        repeats:NO];
 
    _timeStartRecordVideo = [NSDate date];
    _timeEndRecordVideo = [_timerRecordVideo fireDate]; //time end of recording
    
    [_timerUpdateTimeLabel invalidate];
    _timerUpdateTimeLabel = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                             target:self
                                                           selector:@selector(updateRecorderTimerLabel:)
                                                           userInfo:nil
                                                            repeats:YES];
    
    
}

- (void) updateRecorderTimerLabel: (NSTimer *)timer
{
    NSTimeInterval timePassed = [_timeStartRecordVideo timeIntervalSinceNow];
    NSTimeInterval timeRemaining = [_timeEndRecordVideo timeIntervalSinceNow];
    _timeVideoLabel.text = [[[self formatSecond:timePassed]
                             stringByAppendingString:@" - "]
                            stringByAppendingString:[self formatSecond:timeRemaining]];
}

- (NSString *) formatSecond: (NSTimeInterval)numberSeconds
{
    if (numberSeconds < 0) {
        numberSeconds = -1 * numberSeconds;
    }
    numberSeconds = lroundf(numberSeconds);
    
    NSUInteger m = (int) (numberSeconds / 60)% 60;
    NSUInteger s = (int) numberSeconds % 60;
    return [NSString stringWithFormat:@"%02lu:%02lu", (unsigned long)m, (unsigned long)s];
}

- (void) stopVideo
{
    [self stopTimers];
    if ([_delegate respondsToSelector:@selector(stopVideoCapture)]) {
        [_delegate stopVideoCapture];
    }
}

- (void) stopTimers
{
    [_timerRecordVideo invalidate];
    [_timerUpdateTimeLabel invalidate];
}

@end
