 //
//  SCAccessEmailViewController.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 22.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAccessEmailViewController.h"
#import "SCSettings.h"
#import "SCVerificationsViewController.h"
#import "SCHTTPManager.h"
#import <SVProgressHUD.h>
#import "SCAccessEmailViewController.h"
#import "SCSkinManager.h"

@interface SCAccessEmailViewController ()<UIAlertViewDelegate,UITextFieldDelegate>

@property(strong, nonatomic) SCHTTPManager* httpManager;

@end

@implementation SCAccessEmailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Access";
    
    self.settings = [SCSettings sharedSettings];
    self.httpManager = _settings.httpManager;
    

    
    self.emailTextField.delegate = self;
    
    self.emailRegistrationOutlet.layer.cornerRadius = 4.0f;
    
    [self settingNavigationBar];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    [SVProgressHUD setBackgroundColor:[_skinManager getColorForKeyX:@"NavigationBarBackground"]];
    
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:[_skinManager getColorForKeyX:@"NavigationBarTitleFont"],
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBarTintColor:[_skinManager getColorForKeyX:@"NavigationBarBackground"]];
    self.navigationController.navigationBar.translucent = NO;
    
    
}


#pragma mark - Private Methods

-(void)setSkinManager:(SCSkinManager *)skinManager{
    
    _skinManager = skinManager;
    
    self.emailRegistrationOutlet.backgroundColor = [_skinManager getColorForKeyX:@"ButtonGreenBackground"];

}


#pragma mark - Actions

- (IBAction)emailRegistrationAction:(id)sender{
    
    if (self.emailTextField.text.length > 0){
        
        self.emailRegistrationOutlet.enabled = NO;
        
        [SVProgressHUD show];
        
        [self.emailTextField resignFirstResponder];
        
        [self.httpManager registerUserWithEmail:self.emailTextField.text
                                      onSuccess:^(NSDictionary *registerValue) {
                                          
                                          long status = [[registerValue objectForKey:@"status"] longValue];
                                          
                                          if (status == 0) {
                                              
                                              [SVProgressHUD dismiss];
                                              
                                              NSString* sessionId = [registerValue objectForKey:@"accessToken"];
                                              NSString* confirmationHash = [registerValue objectForKey:@"confirmationHash"];
                                              _settings.modelProfile.sessionID = sessionId;
                                              _settings.modelProfile.confirmationHash = confirmationHash;
                                              
                                              SCVerificationsViewController* verificationViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCVerificationsViewController" owner:self options:nil] lastObject];
                                              
                                              verificationViewController.delegate = _delegate;
                                              verificationViewController.isPhoneVerification = NO;
                                              verificationViewController.skinManager=_skinManager;
                                              
                                              self.emailRegistrationOutlet.enabled = YES;
                                              
                                              [self.navigationController pushViewController:verificationViewController animated:YES];
                                              
                                          } else {
                                              
                                              [SVProgressHUD dismiss];
                                              
                                              self.emailRegistrationOutlet.enabled = YES;
                                              
                                              NSString* errorMessage = [registerValue objectForKey:@"errorMessage"];
                                              
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                  message:errorMessage
                                                                                                 delegate:self
                                                                                        cancelButtonTitle:@"Cancel"
                                                                                        otherButtonTitles:nil];
                                              
                                              [alertView show];
                                              
                                          }
                                          
                                          
                                          
                                          
                                      }
                                      onFailure:^(NSError *error, NSInteger statusCode) {
                                          
                                          [SVProgressHUD dismiss];
                                          
                                          self.emailRegistrationOutlet.enabled = YES;
                                          
                                          FTLog(@"error = %@, code = %ld", [error localizedDescription],(long)statusCode);
#pragma mark TODO показать сообщение
                                          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                              message:[error localizedDescription]
                                                                                             delegate:self
                                                                                    cancelButtonTitle:@"Cancel"
                                                                                    otherButtonTitles:nil];
                                          [alertView show];
                                          
                                          
                                      }];
        
        
        
    } else {
        
        [self startShake:self.emailRegistrationOutlet];
        
    }

    
}

- (void)backAction {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Animation

- (void) startShake:(UIView*)view
{
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-8, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(8, 0);
    
    view.transform = leftShake;  // starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:2];
    [UIView setAnimationDuration:0.07];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    view.transform = rightShake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}


#pragma mark - UITextFieldDelegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


#pragma mark -  Supported orientation


-(BOOL)shouldAutorotate {
    
    return YES;
    
}

- (NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

#pragma mark - Initialization methods


- (void)settingNavigationBar {
    
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
}






@end
