//
//  SCSettings.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCSkinManager.h"
#import "SCWebSocketClient.h"
#import "SCHTTPManager.h"
#import "SCNotifycationManager.h"
#import "SCInterfaceDelegates.h"
#import "SCModelProfile.h"
#import "SCPBManager.h"
#import "SCAddressbookManager.h"
#import "SCTypes.h"


@class SCmodelContacts,SCContactItem,SCModelProfile,SCModelChats,SCModelUser;
//@class SCModelLists;
//@class SCSystemSettingsModel;

typedef NS_ENUM(NSInteger, SCSoundPlay) {
    SCSoundPlayIsAppSoundOn = 0,
    SCSoundPlayLogin,
    SCSoundPlayLogout,
    SCSoundPlayMessageSending,
    SCSoundPlayMessageReceive
};

@interface SCSettings : NSObject <SCWebSocketClientDelegate,SCSettingsDelegate>

@property(nonatomic,weak) id<SCMainNavControllerDelegate> delegate;

#pragma mark NetWork
@property(nonatomic,strong) SCHTTPManager* httpManager;
@property(nonatomic,strong) SCWebSocketClient* socketClient;
@property(nonatomic,strong) SCNotifycationManager* notifycationManager;

@property(nonatomic,strong) SCSkinManager* skinManager;

#pragma mark Path for resources
@property(nonatomic,strong) NSString* cacheChatPath;
@property(nonatomic,strong) NSString* cacheMessagesPath;
@property(nonatomic,strong) NSString* cacheContactsPath;
@property(nonatomic,strong) NSString* cacheListsPath;
@property(nonatomic,strong) NSString* caheImagePath;
@property(nonatomic,strong) NSString* mediaTempPath;


#pragma mark DataModels
@property(strong, nonatomic) SCModelProfile * modelProfile;
@property(nonatomic,strong) SCmodelContacts* modelContacts;
@property(nonatomic,strong) SCModelChats* modelChats;
@property(nonatomic,strong) SCAddressbookManager* modelAddressbook;
@property(nonatomic,strong) SCModelUser* modelUsers;
//@property(nonatomic,strong) SCModelLists* modelLists;


#pragma mark Runtime variables
@property(nonatomic,strong) NSString* skinShemeName;
@property (nonatomic,assign) BOOL isNotification;
@property (nonatomic,assign) BOOL isNightMode;
@property (nonatomic,assign) BOOL isAppSound;
@property (nonatomic,assign) BOOL isAppVibration;
@property (nonatomic,assign) SCChatType lastChatType;
@property (nonatomic,assign) BOOL isInBackgraund;

@property(nonatomic,readonly,retain) NSDateFormatter* dateFormater;
//@property(nonatomic, strong) SCSystemSettingsModel * appSettings;



+ (SCSettings*) sharedSettings;
-(void) settingsStart;
-(void) netInit;
-(void) prepareLogout;
-(void) logout;
-(void) loadSettings;
-(void) saveSettings;
-(void) saveStoredProfile;
-(void) clearImageCache;
-(void) clearAllCache;
-(void) play:(SCSoundPlay) sound;

-(void) appChangeState:(SCAppStateChange) appState;

@end
