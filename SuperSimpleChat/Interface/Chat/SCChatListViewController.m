//
//  SCChatListViewController.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatListViewController.h"
//#import "SCProfileViewController.h"
#import "SCTopSelector.h"
#import "SCChatListCell.h"
#import "SCChatViewController.h"
#import "SCModelChats.h"
#import "SCSettings.h"
#import "SCModelProfile.h"
#import "SCPBManager.h"
#import "SCNewChatViewController.h"
#import "SCSegmentControll.h"
#import "SCChatItem.h"
#import "SCContactItem.h"


#define CHAT_LIST_TABLEVIEW_CELL @"SCChatListCell"
#define LAST_CHAT_TABLEVIEW_CELL_HEIGHT 56
#define SEARCH_LIMIT 10
#define TOP_HEIGHT 44
#define SEARCH_BAR_HEIGHT 44

#define PULLREFRESH_TEXTINFO @"Refresh"
#define PULLREFRESH_TEXTPULL @"Pull to refresh"
#define PULLREFRESH_TEXTRELEASE @"Push to refresh"
#define PULLREFRESH_TEXTLOADING @"Refreshing ...."

@implementation SCChatListViewController
{
    SCModelChats* _modelChats;
    NSArray* _chatItems;
    SCChatType _atType;
    SCChatItem* _openPrivate;
    BOOL _isActiveController;
    
    NSUInteger _searchOffset;
    NSString* _searchTagText;
    BOOL _isChatSearchInProgress;
//    UIButton *_rightButton;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarController.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBarHidden = NO;
    
    _atType = SCChatTypeNone;
    _searchOffset = 0;
    _isChatSearchInProgress = NO;
    
    // Do any additional setup after loading the view.
    
    [_btnChangeType removeAllSegments];
    [_btnChangeType insertSegmentWithTitle:@"Private" atIndex:0 animated:YES];
    [_btnChangeType insertSegmentWithTitle:@"Friends" atIndex:1 animated:YES];
    [_btnChangeType insertSegmentWithTitle:@"Public" atIndex:2 animated:YES];
//    [_btnChangeType insertSegmentWithTitle:@"None" atIndex:3 animated:YES];
    [_btnChangeType setSelectedSegmentIndex:0];
    

    [_tableView registerNib:[UINib nibWithNibName:@"SCChatListCell" bundle:nil] forCellReuseIdentifier:CHAT_LIST_TABLEVIEW_CELL];
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationChangeSkin:) name:NOTIFICATION_CHANGE_SKIN object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationStartNewPrivateChat:) name:NOTIFICATION_START_PRIVATE_CHAT object:nil];


}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_CHANGE_SKIN_NAME object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_START_PRIVATE_CHAT object:nil];
    
    [_settings.modelChats delDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // NavigationBar
    self.tabBarController.navigationItem.titleView = nil;
    self.tabBarController.title = @"Wellcome to SuperSimple";
    self.tabBarController.navigationItem.title = @"Wellcome to SuperSimple";;
    
    
    
    self.navigationController.navigationBarHidden = NO;

    if(_atType!=SCChatTypeSearch)
        [self setType:_atType];

    _isActiveController = YES;
    
    [self updateUI];
    
    [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypePrivate] atIndex:0];
    [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypeGroup]  atIndex:1];
    [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypePublic]  atIndex:2];
    [_tableView reloadData];

}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _isActiveController = NO;
}

-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    _modelChats = _settings.modelChats;
    [_settings.modelChats addDelegate:self];
    _atType = _settings.lastChatType;
    
    [_swichChangeType setSettings:_settings];
    [_swichChangeType addItemWithTitle:@"Private"];
    [_swichChangeType addItemWithTitle:@"Friends"];
    [_swichChangeType addItemWithTitle:@"Public"];
    [_swichChangeType addTarget:self action:@selector(switchChangeTypeAction:forEvent:) forControlEvents:UIControlEventValueChanged];
    
}


-(void) updateUI
{
    self.view.backgroundColor = SKCOLOR_Background;
    _viewEmptyImageView.image = [_settings.skinManager getImageForKeyX:@"chat-creatchat-bg"];
//    [_rightButton setImage:[_settings.skinManager getImageForKey:@"btn_new_chat"] forState:UIControlStateNormal];
//    _navigationView.backgroundColor = SKCOLOR_BarColor;
//    [_btnProfile setImage:[_settings.skinManager getImageForKey:@"btn_profile"] forState:UIControlStateNormal];
//    [_btnNewChat setImage:[_settings.skinManager getImageForKey:@"btn_new_chat"] forState:UIControlStateNormal];
}

- (void) switchChangeTypeAction:(SCTopSelector *)sender forEvent:(UIEvent *)event
{
    if([sender activeSegment] == 0)
        [self setType:SCChatTypePrivate];
    else if([sender activeSegment] == 1)
        [self setType:SCChatTypeGroup];
    else if([sender activeSegment] == 2)
        [self setType:SCChatTypePublic];
    else if([sender activeSegment] == 3)
        [self setType:SCChatTypeNone];
}

- (IBAction)btnChangeTypeAction:(SCSegmentControll *)sender forEvent:(UIEvent *)event
{
    if([_btnChangeType selectedSegmentIndex] == 0)
        [self setType:SCChatTypePrivate];
    else if([_btnChangeType selectedSegmentIndex] == 1)
        [self setType:SCChatTypeGroup];
    else if([_btnChangeType selectedSegmentIndex] == 2)
        [self setType:SCChatTypePublic];
    else if([_btnChangeType selectedSegmentIndex] == 3)
        [self setType:SCChatTypeNone];
}

-(void) setType:(SCChatType) type
{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _settings.lastChatType = type;
    switch (type)
    {
        case SCChatTypePrivate:
        {
            [_btnChangeType setSelectedSegmentIndex:0];
            [rightButton setImage:[_settings.skinManager getImageForKeyX:@"chat-btn-add"] forState:UIControlStateNormal];
            _atType = SCChatTypePrivate;

            _chatItems = nil;
            _chatItems = [_modelChats items:_atType];
            [_swichChangeType setActiveSegment:0];
            
            if(_chatItems.count > 0)
                _viewEmptyBg.hidden = YES;
            else
                _viewEmptyBg.hidden = NO;
        }
            break;
            
        case SCChatTypeGroup:
        {
            [_btnChangeType setSelectedSegmentIndex:1];
            [rightButton setImage:[_settings.skinManager getImageForKeyX:@"chat-btn-add"] forState:UIControlStateNormal];
            _atType = SCChatTypeGroup;

            _chatItems = nil;
            _chatItems = [_modelChats items:_atType];
            [_swichChangeType setActiveSegment:1];

            if(_chatItems.count > 0)
                _viewEmptyBg.hidden = YES;
            else
                _viewEmptyBg.hidden = NO;
        }
            break;
            
        case SCChatTypePublic:
        {
            [_btnChangeType setSelectedSegmentIndex:2];
            [rightButton setImage:[_settings.skinManager getImageForKeyX:@"search"] forState:UIControlStateNormal];
            _atType = SCChatTypePublic;
            
            _chatItems = nil;
            _chatItems = [_modelChats items:_atType];
            [_swichChangeType setActiveSegment:2];

//            if(_chatItems.count > 0)
                _viewEmptyBg.hidden = YES;
//            else
//                _viewEmptyBg.hidden = NO;
        }
            break;
            
        case SCChatTypeSearch:
        {
            [rightButton setImage:[_settings.skinManager getImageForKeyX:@"chat-btn-add"]  forState:UIControlStateNormal];
            _atType = SCChatTypeSearch;
            [_swichChangeType setActiveSegment:2];

            _chatItems = nil;
//            [_settings.modelChats searchPublicChatRooms:@""];
            _viewEmptyBg.hidden = YES;
        }
            break;
            
        case SCChatTypeNone:
        {
            [_btnChangeType setSelectedSegmentIndex:3];
            _atType = SCChatTypeNone;
        }
            break;
            
        default:
        {
            [_btnChangeType setSelectedSegmentIndex:0];
            [rightButton setImage:[_settings.skinManager getImageForKeyX:@"chat-btn-add"]  forState:UIControlStateNormal];
            _atType = SCChatTypePrivate;
            
            _chatItems = nil;
            _chatItems = [_modelChats items:_atType];

            if(_chatItems.count > 0)
                _viewEmptyBg.hidden = YES;
            else
                _viewEmptyBg.hidden = NO;
        }
            break;
    }
    [self searchBarVisibles];
    
    rightButton.frame = CGRectMake(0.0, 0.0, rightButton.imageView.image.size.width, rightButton.imageView.image.size.height);
    [rightButton addTarget:self action:@selector(btnNewChatAction:forEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.tabBarController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];

    [_tableView reloadData];
}

-(void) searchBarVisibles
{
//    FTLog(@"\nself.view.frame = %@\n_tableView.frame = %@\nself.tabBarController.view.fram = %@",NSStringFromCGRect(self.view.frame),NSStringFromCGRect(_tableView.frame),NSStringFromCGRect(self.tabBarController.view.frame));
    if(_atType == SCChatTypeSearch)
    {
        [UIView animateWithDuration:0.25 animations:^{
            _viewSearch.frame = CGRectMake(0, TOP_HEIGHT, self.view.frame.size.width, SEARCH_BAR_HEIGHT);
            _tableView.frame = CGRectMake(0, TOP_HEIGHT + SEARCH_BAR_HEIGHT, self.view.frame.size.width, self.view.frame.size.height - TOP_HEIGHT - SEARCH_BAR_HEIGHT);
            [_searchBar setShowsCancelButton:YES animated:YES];
        } completion:^(BOOL finished) {
//
        }];
        if(!_isChatSearchInProgress)
        {
            NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                           @"",@"TagText",
                                           [NSNumber numberWithInteger:SEARCH_LIMIT],@"limit",
                                           [NSNumber numberWithInteger:_searchOffset],@"offset",
                                           nil];
            
            _isChatSearchInProgress = YES;
            
            [_settings.modelChats searchPublicChatRooms:params];
        }
    }
    else
    {
        [_searchBar resignFirstResponder];
        [UIView animateWithDuration:0.25 animations:^{
            _viewSearch.frame = CGRectMake(0, -TOP_HEIGHT, self.view.bounds.size.width, SEARCH_BAR_HEIGHT);
            _tableView.frame = CGRectMake(0, TOP_HEIGHT, self.view.frame.size.width, self.view.frame.size.height - TOP_HEIGHT);
        } completion:^(BOOL finished) {
        }];
    }
}

-(void) searchAction
{
    if(!_isChatSearchInProgress)
    {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       _searchTagText,@"TagText",
                                       [NSNumber numberWithInteger:SEARCH_LIMIT],@"limit",
                                       [NSNumber numberWithInteger:_searchOffset],@"offset",
                                       nil];

        _isChatSearchInProgress = YES;
        
        [_settings.modelChats searchPublicChatRooms:params];
    }
}

- (IBAction)btnNewChatAction:(UIButton *)sender forEvent:(UIEvent *)event
{
    if(_atType == SCChatTypePrivate)
    {
        FTLog(@"Type: PRIVATES");
        self.tabBarController.selectedIndex = 1;
    }
    else if(_atType == SCChatTypeGroup)
    {
        FTLog(@"Type: GROUPS");
        SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
        pSCNewChatViewController.isNewChat = YES;
        pSCNewChatViewController.forChatType = SCChatTypeGroup;
        pSCNewChatViewController.settings = _settings;
        pSCNewChatViewController.isNewChat = YES;
        SCChatItem* newChatItem = [[SCChatItem alloc] initWithSettings:_settings];
        newChatItem.chatType = SCChatTypeGroup;
        pSCNewChatViewController.chatItem = newChatItem;

        self.navigationController.navigationBarHidden = NO;
        [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
    }
    else if(_atType == SCChatTypePublic)
    {
        [self setType:SCChatTypeSearch];
    }
    else if(_atType == SCChatTypeSearch)
    {
        SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
        pSCNewChatViewController.isNewChat = YES;
        pSCNewChatViewController.forChatType = SCChatTypePublic;
        pSCNewChatViewController.settings = _settings;
        pSCNewChatViewController.isNewChat = YES;
        SCChatItem* newChatItem = [[SCChatItem alloc] initWithSettings:_settings];
        newChatItem.chatType = SCChatTypePublic;
        pSCNewChatViewController.chatItem = newChatItem;
        
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
    }
}


- (NSArray *) makeActionButtons:(NSIndexPath*) indexPath
{
    SCChatItem* makedItem = [_chatItems objectAtIndex:indexPath.row];

    NSMutableArray *actionButtons = [NSMutableArray new];
    switch (_atType)
    {
        case SCChatTypePrivate:
        {
            FTLog(@"Type: PRIVATES");
            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_trash"]];
        }
            break;
        case SCChatTypeGroup:
        {
            FTLog(@"Type: GROUPS");
            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_trash"]];
            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_edit"]];
        }
            break;
            
        case SCChatTypePublic:
        {
            FTLog(@"Type: PUBLIC");
            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_trash"]];
            if([makedItem.chatOwner isEqual:_settings.modelProfile.contactItem])
            {// мой могу редактировать
                [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_edit"]];
            }
            else
            {// чужой только смотреть
                [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKeyX:@"tabbar-profile"]];
            }

        }
            break;
            
        case SCChatTypeSearch:
        {
            FTLog(@"Type: SEARCH");
            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKeyX:@"btn_add"]];
            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_edit"]];
        }
            break;
            
        default:
            break;
    }
    
    return actionButtons;
}

#pragma mark UITableView
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _chatItems.count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SCChatListCell calcSize:self.view.bounds.size.width chatItem:[_chatItems objectAtIndex:indexPath.row]];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SCChatListCell* chatCell = [tableView dequeueReusableCellWithIdentifier:CHAT_LIST_TABLEVIEW_CELL];
    if(!chatCell)
        chatCell = [[SCChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CHAT_LIST_TABLEVIEW_CELL];
    chatCell.delegate = self;
    chatCell.dataDelegate = self;
    chatCell.chatData = [_chatItems objectAtIndex:indexPath.row];
    chatCell.contentView.translatesAutoresizingMaskIntoConstraints = YES;//?
    [chatCell setRightUtilityButtons:[self makeActionButtons:indexPath] WithButtonWidth:44];
    chatCell.backgroundColor = [UIColor clearColor];
    [chatCell setNeedsLayout];
    return chatCell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    switch (_atType)
//    {
//        case SCChatTypeSearch:
//        {
//            SCChatItem* chatItem = [_chatItems objectAtIndex:indexPath.row];
//            [chatItem joinUserToRoom:_settings.modelProfile.contactItem];
//        }
//            break;
//            
//        default:
//        {
            SCChatViewController* pSCChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCChatViewController" owner:self options:nil] lastObject];
            pSCChatViewController.settings = _settings;
            pSCChatViewController.chatItem = [_chatItems objectAtIndex:indexPath.row];
//            pSCChatViewController.view.frame = self.view.frame;
            [self.navigationController pushViewController:pSCChatViewController animated:YES];
//        }
//            break;
//    }
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //    FTLog(@"scrollViewDidScroll %f",scrollView.contentOffset.y);
    if(scrollView.contentOffset.y < -1)
    {
        CGRect rr = _pullRefreshView.frame;
        _pullRefreshView.frame = CGRectMake(rr.origin.x, /*_navigationView.frame.size.height*/ -rr.size.height - scrollView.contentOffset.y - 2, rr.size.width, rr.size.height);
        [UIView beginAnimations:nil context:NULL];
        if(ABS(scrollView.contentOffset.y)>REFRESH_HEADER_HEIGHT)
        {
            // User is scrolling above the header
            _refreshLabel.text = PULLREFRESH_TEXTRELEASE;
            [_refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        }
        else
        { // User is scrolling somewhere within the header
            _refreshLabel.text = PULLREFRESH_TEXTPULL;
            [_refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        }
        [UIView commitAnimations];
    }
    else if( scrollView.contentOffset.y > 10 && _atType == SCChatTypeSearch)
    {
        float y = scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom;
        float h = scrollView.contentSize.height;
        
        float reload_distance = 0;
        if(y > h + reload_distance && !_isChatSearchInProgress)
        {
            _searchOffset +=SEARCH_LIMIT;
            [self searchAction];
        }
    }
//    else
//        FTLog(@"scrollView.contentOffset.y = %f",scrollView.contentOffset.y);
    
}
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    //    FTLog(@"scrollViewWillBeginDecelerating %f",scrollView.contentOffset.y);
    if(scrollView.contentOffset.y<= -REFRESH_HEADER_HEIGHT)
    {
        [_modelChats serverLoad];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //    FTLog(@"scrollViewDidEndDecelerating %f",scrollView.contentOffset.y);
    CGRect rr = _pullRefreshView.frame;
    _pullRefreshView.frame = CGRectMake(rr.origin.x, /*_navigationView.frame.size.height*/ -rr.size.height - scrollView.contentOffset.y, rr.size.width, rr.size.height);
}

#pragma mark Notification
- (void)notificationChangeSkin:(NSNotification *)note
{
    [_tableView reloadData];
}

- (void)notificationStartNewPrivateChat:(NSNotification *)note
{
    FTLog(@"%@",note);
    SCContactItem* forContact = [note object];
    SCChatItem* _newChatItem;
    _newChatItem = [_settings.modelChats getPrivateForUser:forContact];
    if(_newChatItem == nil)
    {
        _newChatItem = [[SCChatItem alloc] initWithSettings:_settings];
        [_newChatItem addDelegate:self];
        _newChatItem.chatName = [NSString stringWithFormat:@"@%@",forContact.contactNikName];
        _newChatItem.chatDescription = [NSString stringWithFormat:@"Private room with @%@",forContact.contactNikName];
#ifdef OLD_FILE_LINK
        _newChatItem.chatImageLink = forContact.contactImageLink;
#endif
        _newChatItem.chatType = SCChatTypePrivate;
        [_newChatItem.usersInChat addObject:forContact];
        _openPrivate = _newChatItem;
        [_modelChats addNewChatRoom:_newChatItem];
    }
    else if(_newChatItem.isDeleted)
    {
        _newChatItem.isDeleted = NO;
        [_modelChats activateChatRoom:_newChatItem];
        _openPrivate = _newChatItem;
    }
    else
    {
        FTLog(@"Open");
        [self enterToRoom:_newChatItem];
    }
    
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (_atType)
    {
        case SCChatTypePrivate:
        {
            FTLog(@"Type: PRIVATES");
//            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_trash"]];
            switch (index)
            {
                case 0:
                {
                    [_modelChats removeChatRoom:((SCChatListCell*) cell).chatData];
                    [cell hideUtilityButtonsAnimated:YES];
                }
                    break;
                default:
                    break;
            }

        }
            break;
        case SCChatTypeGroup:
        {
            FTLog(@"Type: GROUPS");
//            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_trash"]];
//            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_edit"]];
            switch (index)
            {
                case 0:
                {
                    SCChatItem* chatItem = ((SCChatListCell*) cell).chatData;
                    if([chatItem.chatOwner isEqual:_settings.modelProfile.contactItem])
                    {// я создал я херю
                        [_modelChats removeChatRoom:chatItem];
                    }
                    else
                    {// нехочу херню смереть
                        [chatItem leaveUserFromRoom:_settings.modelProfile.contactItem];
                    }
                    [cell hideUtilityButtonsAnimated:YES];
                }
                    break;
                case 1:
                {
                    FTLog(@"1 button was pressed");
                    [cell hideUtilityButtonsAnimated:YES];
                    SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
                    pSCNewChatViewController.settings = _settings;
                    pSCNewChatViewController.chatItem = ((SCChatListCell*) cell).chatData;
                    [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
                }
                    break;
                default:
                    break;
            }

        }
            break;
            
        case SCChatTypePublic:
        {
            FTLog(@"Type: PUBLIC");
//            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_trash"]];
//            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_edit"]];
            switch (index)
            {
                case 0:
                {
                    SCChatItem* chatItem = ((SCChatListCell*) cell).chatData;
                    if([chatItem.chatOwner isEqual:_settings.modelProfile.contactItem])
                    {// я создал я херю
                        [_modelChats removeChatRoom:chatItem];
                    }
                    else
                    {// нехочу херню смереть
                        [chatItem leaveUserFromRoom:_settings.modelProfile.contactItem];
                    }
                    [cell hideUtilityButtonsAnimated:YES];
                }
                    break;
                case 1:
                {
                    FTLog(@"1 button was pressed");
                    [cell hideUtilityButtonsAnimated:YES];
                    SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
                    pSCNewChatViewController.settings = _settings;
                    pSCNewChatViewController.isViewOnly = YES;
                    pSCNewChatViewController.chatItem = ((SCChatListCell*) cell).chatData;
                    [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
        case SCChatTypeSearch:
        {
            FTLog(@"Type: SEARCH");
//            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_trash"]];
//            [actionButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[_settings.skinManager getImageForKey:@"cell_btn_edit"]];
            switch (index)
            {
                case 0:
                {
                    SCChatItem* chatItem = ((SCChatListCell*) cell).chatData;
                    if([chatItem.chatOwner isEqual:_settings.modelProfile.contactItem])
                    {// я создал я херю
                        [_modelChats removeChatRoom:chatItem];
                    }
                    else
                    {// хочу посмереть эту херню
                        [chatItem joinUserToRoom:_settings.modelProfile.contactItem];
                    }
                    [cell hideUtilityButtonsAnimated:YES];
                }
                    break;
                case 1:
                {
                    FTLog(@"1 button was pressed");
                    [cell hideUtilityButtonsAnimated:YES];
                    SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
                    pSCNewChatViewController.settings = _settings;
                    pSCNewChatViewController.chatItem = ((SCChatListCell*) cell).chatData;
                    [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }

/*
    switch (index)
    {
        case 0:
        {
            [_modelChats removeChatRoom:((SCChatListCell*) cell).chatData];
            [cell hideUtilityButtonsAnimated:YES];
        }
            break;
        case 1:
        {
            FTLog(@"1 button was pressed");
            [cell hideUtilityButtonsAnimated:YES];
            SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
            pSCNewChatViewController.settings = _settings;
            pSCNewChatViewController.chatItem = ((SCChatListCell*) cell).chatData;
            [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
        }
            break;
        default:
            break;
    }
*/
}

-(void) enterToRoom:(SCChatItem*) chatData
{
    /*
    SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
    pSCNewChatViewController.settings = _settings;
    pSCNewChatViewController.chatItem = chatData;
    [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
    */
    SCChatViewController* pSCChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCChatViewController" owner:self options:nil] lastObject];
    pSCChatViewController.settings = _settings;
    pSCChatViewController.chatItem = chatData;
//    pSCChatViewController.view.frame = self.view.frame;
    [self.navigationController pushViewController:pSCChatViewController animated:YES];

}
#pragma mark @protocol SCChatListViewControllerDelegate <NSObject>
-(void) cellNeedReload:(SCChatListCell*) cell
{
    if(_isActiveController)
    {
        NSIndexPath* indexPath = [_tableView indexPathForCell:cell];
        if(indexPath)
            [_tableView reloadRowsAtIndexPaths:[[NSArray alloc] initWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else
    {
//    [_tableView reloadData];
    }
}

#pragma mark SCPBManagerDelegate <NSObject>

-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"%@ : receiveParsedData",NSStringFromClass([self class]));
    switch (type)
    {
        case SCPBManagerParceTypeChangeChat:
        {
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = (SCChatItem*) data;
                [self setType:SCChatTypePrivate];
                
                if([_openPrivate isEqual:chatItem])
                {
                    _openPrivate = nil;
                    [self enterToRoom:chatItem];
                }
            }
        }
        case SCPBManagerParceTypeChangeRoomEvent:
        case SCPBManagerParceTypeCreateChat:
        case SCPBManagerParceTypeRemoveChat:
        case SCPBManagerParceTypeActivateChat:
        case SCPBManagerParceTypeGetAllRoomsForUser:
        case SCPBManagerParceTypeGetChatRoomByIdRequest:
        case SCPBManagerParceTypeJoinUserToRoomEvent:
        case SCPBManagerParceTypeCallTypeRemoveRoomEvent:
        case SCPBManagerParceTypeCallTypeLeaveUserFromRoomEvent:
        case SCPBManagerParceTypeGetAllUsersForRoom:
            if([data isKindOfClass:[SCChatItem class]])
            {
                SCChatItem* chatItem = (SCChatItem*) data;
                
                if([_openPrivate isEqual:chatItem])
                {
                    _openPrivate = nil;
                    [self setType:SCChatTypePrivate];
                    [self enterToRoom:chatItem];
                }
            }
            [self setType:_atType];
//            [_tableView reloadData];
            break;
//        case CallTypeChangeInfoChatroomEvent:
            
        case SCPBManagerParceTypeSendMessage:
        case SCPBManagerParceTypeMessage:
        case SCPBManagerParceTypeGetChatRoomHistory:
        case SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange:
        {
            [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypePrivate] atIndex:0];
            [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypeGroup]  atIndex:1];
            [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypePublic]  atIndex:2];
        }
            break;
            
//TODO:
        case SCPBManagerParceTypeSearchPublicChatRooms:
            FTLog(@"didChangeItemInModelChatsList");
            _isChatSearchInProgress = NO;
            if([data isKindOfClass:[NSArray class]])
            {
                _chatItems = data;
                [_tableView reloadData];
            }
            break;
            
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;
    }
}
#pragma mark SCModelChatsDelegate <NSObject>
-(void) didAddItemToModelChatsList:(SCChatItem*) chatItem
{
    FTLog(@"didAddItemToModelChatsList");
    [self setType:_atType];

    if([_openPrivate isEqual:chatItem])
    {
        _openPrivate = nil;
        [self enterToRoom:chatItem];
    }
    else
    {
        [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypePrivate] atIndex:0];
        [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypeGroup]  atIndex:1];
        [_swichChangeType badgeValue:[_settings.modelChats countNotViewedForType:SCChatTypePublic]  atIndex:2];
        [_tableView reloadData];
        
    }
}
-(void) didChangeItemInModelChatsList:(SCChatItem*) chatItem
{
    FTLog(@"didChangeItemInModelChatsList");
    [self setType:_atType];
    [_tableView reloadData];
}
-(void) didSearchItemInModelChatsList:(id) chatItems
{
    FTLog(@"didChangeItemInModelChatsList");
    _isChatSearchInProgress = NO;
    _chatItems = chatItems;
    [_tableView reloadData];
}

-(void) didRemoveItemFromModemChatsList:(id) respond
{
    FTLog(@"didRemoveItemFromModemChatsList");
    [self setType:_atType];
    [_tableView reloadData];
}
-(void) didChangeModelChatItem
{
    FTLog(@"didChangeModelChatItem");
    [self setType:_atType];
    [_tableView reloadData];
}

#pragma mark UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    if(!_isChatSearchInProgress)
    {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       @"",@"TagText",
                                       [NSNumber numberWithInteger:SEARCH_LIMIT],@"limit",
                                       [NSNumber numberWithInteger:_searchOffset],@"offset",
                                       nil];
        
        _isChatSearchInProgress = YES;
        
        [_settings.modelChats searchPublicChatRooms:params];
    }

}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = nil;
    
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:YES animated:YES];
    [self setType:SCChatTypePublic];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    _searchTagText = searchBar.text;
    _searchOffset = 0;
    
    [self searchAction];

    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:YES animated:YES];
}

@end
