//
//  SCPBManagerTernopol+ChatRooms.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol(ChatRooms).h"
#import "SCPBManagerTernopol(Converts).h"

//#import "SCSettings.h" in SCPBManagerTernopol.h
#import "SmartChat.pb.h"
#import "SCPUUID.h"

#import "SCChatItem.h"
#import "SCNetFile.h"
#import "SCContactItem.h"

#import "SCModelChats.h"
#import "SCModelUser.h"

@implementation SCPBManagerTernopol (ChatRooms)

/* =======================================================================================
 message CreateChatRoomRequest {
 required string name = 1;
 optional string chat_description = 2;
 optional sint64 time_destroy = 3;
 optional int32 chat_type = 4;
 optional PUUID file_image_id = 5;
 optional int32 security_level = 6;
 }*/
-(void) createChatRoomRequest:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        
        
        CreateChatRoomRequestBuilder* _createChatRoomRequestBuilder = [[CreateChatRoomRequestBuilder alloc] init];
        if(chatItem.chatName.length <1)
            return;
        [_createChatRoomRequestBuilder setName:chatItem.chatName];
        
        if(chatItem.chatDescription.length > 0)
            [_createChatRoomRequestBuilder setChatDescription:chatItem.chatDescription];
        
        if(chatItem.timeDestroy !=0)
            [_createChatRoomRequestBuilder setTimeDestroy:chatItem.timeDestroy];

        [_createChatRoomRequestBuilder setChatRoomType:[self SCChatRoomTypeToChatRoomType:chatItem.chatType]];
        
        if(chatItem.netFile.netPath.length > 0)
        {
            [_createChatRoomRequestBuilder setFileImageId:[self puuidBuilder:[[SCPUUID alloc] initFromString:chatItem.netFile.netPath]]];
        }
            
        if(chatItem.usersInChat.count>0)
        {
            for(SCContactItem* contact in chatItem.usersInChat)
            {
                [_createChatRoomRequestBuilder addMembers:[self puuidBuilder:contact.userPUUID]];
            }
        }
        
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeCreateChatroom];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_createChatRoomRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}

// response is CreateChatRoomResponse.
-(void) parseCreateChatroom:(id)data
{
    Response * response = data;
    
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        
        if(response.status.success)
        {
            
            CreateChatRoomResponse* resp = [CreateChatRoomResponse parseFromData:response.response];
            SCChatItem* newChat = [params objectForKey:@"SCChatItem"];
            newChat.chatPuuid = [self scpuuidBuilder:resp.id];
            //[[SCPUUID alloc] initWithLeastSignificantBits:resp.id.leastSignificantBits MostSignificantBits:resp.id.mostSignificantBits];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeCreateChat data:newChat];
            }
        }
        else
        {
            FTLog(@"Error parseCreateChatroom response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}

//-(void) searchCreateChatRoomRequest:(id) params
//{
//    
//}

/* =======================================================================================
 message RemoveChatRoomRequest {
 required PUUID id = 1;
 }
 */
-(void) removeChatRoomRequest:(id) params//:(SCChatItem*) chatItem
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        RemoveChatRoomRequestBuilder* removeChatRoomRequestBuilder = [[RemoveChatRoomRequestBuilder alloc] init];
        [removeChatRoomRequestBuilder setId:[self puuidBuilder:chatItem.chatPuuid]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeRemoveChatroom];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[removeChatRoomRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}//no response
-(void) parseRemoveChatroom:(id)data
{
    Response * response = data;
    
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeRemoveChat data:chatItem];
            }
        }
        else
        {
            FTLog(@"Error parseRemoveChatroom response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}

/* =======================================================================================
message DoChatRoomActiveRequest {
    required PUUID chat_room_id = 1;
}*/
-(void) chatRoomActivate:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        MakeChatRoomActiveRequestBuilder *_makeChatRoomActiveRequestBuilder = [[MakeChatRoomActiveRequestBuilder alloc] init];
        [_makeChatRoomActiveRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeMakeChatRoomActive];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_makeChatRoomActiveRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }

}

-(void) parseChatRoomActivate:(id)data
{
    Response * response = data;
    
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeActivateChat data:chatItem];
            }
        }
        else
        {
            FTLog(@"Error parseDoChatRoomActive response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring parseDoChatRoomActive");
    }
}


/* =======================================================================================
 message RenameChatRoomRequest {
 required PUUID id = 1;
 required string name = 2;
 }
 */
-(void) changeChatRoomRequest:(id)params //:(SCChatItem*) chatItem
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        ChatRoomBuilder *_chatRoomBuilder = [[ChatRoomBuilder alloc] init];
        [_chatRoomBuilder setId:[self puuidBuilder:chatItem.chatPuuid]];
//        [_chatRoomBuilder setIsDeleted:chatItem.isDeleted];
        
        if(chatItem.chatName.length >0)
            [_chatRoomBuilder setName:chatItem.chatName];
        if(chatItem.chatDescription.length > 0)
            [_chatRoomBuilder setChatDescription:chatItem.chatDescription];
        
        
        if(chatItem.netFile.netPath.length > 0)
        {
            [_chatRoomBuilder setFileImageId:[self puuidBuilder:[[SCPUUID alloc] initFromString:chatItem.netFile.netPath]]];
        }
        else
            [_chatRoomBuilder setFileImageId:[self puuidBuilder:[[SCPUUID alloc] initFromString:@"00000000-0000-0000-0000-000000000000"]]];
        
        [_chatRoomBuilder setIsDeleted:chatItem.isDeleted];
        
        ChangeChatRoomInfoRequestBuilder* _changeChatRoomInfoRequestBuilder = [[ChangeChatRoomInfoRequestBuilder alloc] init];
        [_changeChatRoomInfoRequestBuilder setChatRoom:[_chatRoomBuilder build]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeChangeInfoChatroom];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_changeChatRoomInfoRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}//no response

-(void) parseChangeChatRoom:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeChangeChat data:chatItem];
            }
            
        }
        else
        {
            FTLog(@"Error parseChangeChatRoom response.status.success");
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeChangeChat data:nil];
            }
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}


/* =======================================================================================
 no request object,
 */
-(void) getAllRoomsForUserRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        NSInteger _offset = 0;
        NSInteger _limit = 20;
        ChatRoomType _chatType = 0;
        GetAllRoomsForUserRequestBuilder* getAllRoomsForUserRequestBuilder = [[GetAllRoomsForUserRequestBuilder alloc] init];
        if([[params allKeys] containsObject:@"offset"])
        {
            _offset = [[params objectForKey:@"offset"] integerValue];
            [getAllRoomsForUserRequestBuilder setOffset:(SInt32)_offset];
        }
        if([[params allKeys] containsObject:@"limit"])
        {
            _limit = [[params objectForKey:@"limit"] integerValue];
            [getAllRoomsForUserRequestBuilder setOffset:(SInt32)_offset];
        }
        
        if([[params allKeys] containsObject:@"ChatType"])
        {
            _chatType =[self SCChatRoomTypeToChatRoomType:(SCChatType)[[params objectForKey:@"ChatType"] integerValue]];
            [getAllRoomsForUserRequestBuilder setChatRoomType:_chatType];
        }
        
//        [getAllRoomsForUserRequestBuilder setChatRoomType:ChatRoomTypeGroupRoom];
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
             [_requestBuilder setType:CallTypeGetAllRoomsForUser];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];

        if(_offset !=0 || _limit != 20 || _chatType!=0)
           [_requestBuilder setRequest:[getAllRoomsForUserRequestBuilder build].data];

        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];
        [_socketClient sendData:dataToSend];
    }
}//response is GetAllRoomsForUserResponse

-(void) parseGetAllRoomsForUser:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        NSInteger typeCall = -1;
        if([[params allKeys] containsObject:@"type"])
            typeCall = [[params objectForKey:@"type"] integerValue];
        
        NSInteger _offset = 0;
        if([[params allKeys] containsObject:@"offset"])
            _offset = [[params objectForKey:@"offset"] integerValue];
        
        NSInteger _limit = 20;
        if([[params allKeys] containsObject:@"limit"])
        {
            _limit = [[params objectForKey:@"limit"] integerValue];
        }

        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            
            NSArray *arChat = [GetAllRoomsForUserResponse parseFromData:response.response].chatRooms;
            NSMutableArray* outArray;
            if([[params allKeys] containsObject:@"outArray"])
                outArray = [params objectForKey:@"outArray"];
            else
                outArray = [[NSMutableArray alloc] init];
            
            NSMutableArray* partArray = [[NSMutableArray alloc] init];
            for(ChatRoom* cht in arChat)
            {
                SCChatItem* chatItem = [self _chatRoomToScChatRoom:cht];
                if(chatItem)
                    [partArray addObject:chatItem];
            }
            
            if(partArray.count == _limit)
            {
                [params setObject:[NSNumber numberWithInteger:_offset + _limit] forKey:@"offset"];
                [outArray addObjectsFromArray:partArray];
                [params setObject:outArray forKey:@"outArray"];
                [self getAllRoomsForUserRequest:params];
            }
            else if(typeCall == SCPBManagerParceTypeSearchCreateChat)
            {
                // 2) поиск в комнате пользователя
                //                SCContactItem* contactItem = [params objectForKey:@"SCContactItem"];
                [params setObject:partArray forKey:@"SCChatItemArray"];
                
                [self getAllUsersForRoomRequest:params];
            }
            else
            {
                [outArray addObjectsFromArray:partArray];
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeGetAllRoomsForUser data:outArray];
                }
            }
        }
        else
        {
            FTLog(@"Error parseGetAllRoomsForUser response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

/* =======================================================================================
message GetChatRoomByIdRequest {
    required PUUID chat_room_id = 1; /// ChatRoom id
}*/
-(void) getChatRoomByIdRequest:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        GetChatRoomByIdRequestBuilder* _getChatRoomByIdRequestBuilder = [[GetChatRoomByIdRequestBuilder alloc] init];
        [_getChatRoomByIdRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeChatroomGetById];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_getChatRoomByIdRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
    }
}
/*
message GetChatRoomByIdResponse {
    optional ChatRoom chat_room = 1; /// ChatRoom
}*/
-(void) parseGetChatRoomByIdResponse:(id)data
{
    Response * response = data;
    
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        NSInteger typeCall = -1;
        if([[params allKeys] containsObject:@"type"])
            typeCall = [[params objectForKey:@"type"] integerValue];
        
        if(response.status.success)
        {
            GetChatRoomByIdResponse* getChatRoomByIdResponse = [GetChatRoomByIdResponse parseFromData:response.response];
            
            SCChatItem* chatItem = [self _chatRoomToScChatRoom:getChatRoomByIdResponse.chatRoom];
            
            id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeGetChatRoomByIdRequest data:chatItem];
            }

//            [_settings.modelChats parsedType:SCPBManagerParceTypeJoinUserToRoomEvent data:chatItem];
        }
        else
        {
            FTLog(@"Error parseCreateChatroom response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUIDstring");
    }
}


/* =======================================================================================
message SeachPublicChatRoomsRequest {
    optional string test = 1 [default = ""];
    optional sint32 limit = 2 [default = 20];
    optional sint32 offset = 3 [default = 0];
}*/
-(void) searchPublicChatRooms:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        if([[params allKeys] containsObject:@"TagText"])
        {
            SeachPublicChatRoomsRequestBuilder *_seachPublicChatRoomsRequestBuilder = [[SeachPublicChatRoomsRequestBuilder alloc] init];
        
            [_seachPublicChatRoomsRequestBuilder setTest:[params objectForKey:@"TagText"]];
        
            NSInteger _offset = 0;
            if([[params allKeys] containsObject:@"offset"])
            {
                _offset = [[params objectForKey:@"offset"] integerValue];
                [_seachPublicChatRoomsRequestBuilder setOffset:(SInt32)_offset];
            }
            NSInteger _limit = 20;
            if([[params allKeys] containsObject:@"limit"])
            {
                _limit = [[params objectForKey:@"limit"] integerValue];
                [_seachPublicChatRoomsRequestBuilder setLimit:(SInt32)_limit];
            }
            
            RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
            [_requestBuilder setType:CallTypeSearchPublicChatRooms];
            [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
            [_requestBuilder setRequest:[_seachPublicChatRoomsRequestBuilder build].data];
            
            Request * query = [_requestBuilder build];
            NSData* dataToSend = [query data];
            [_socketClient sendData:dataToSend];
        }
        //
        
    }

}

/*
message SeachPublicChatRoomsResponse {
    repeated ChatRoom chat_rooms = 1;
}*/

-(void) parseSeachPublicChatRooms:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        
        NSInteger _offset = 0;
        if([[params allKeys] containsObject:@"offset"])
            _offset = [[params objectForKey:@"offset"] integerValue];
        
        NSInteger _limit = 20;
        if([[params allKeys] containsObject:@"limit"])
        {
            _limit = [[params objectForKey:@"limit"] integerValue];
        }
        
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            
            NSArray *arChat = [SeachPublicChatRoomsResponse parseFromData:response.response].chatRooms;
            NSMutableArray* outArray;
            if([[params allKeys] containsObject:@"outArray"])
                outArray = [params objectForKey:@"outArray"];
            else
                outArray = [[NSMutableArray alloc] init];
            
            NSMutableArray* partArray = [[NSMutableArray alloc] init];
            for(ChatRoom* cht in arChat)
            {
                SCChatItem* chatItem = [self _chatRoomToScChatRoom:cht];
                [partArray addObject:chatItem];
            }
            
            if(partArray.count == _limit)
            {
                [params setObject:[NSNumber numberWithInteger:_offset + _limit] forKey:@"offset"];
                [outArray addObjectsFromArray:partArray];
                [params setObject:outArray forKey:@"outArray"];
                [self getAllRoomsForUserRequest:params];
            }
            else
            {
                [outArray addObjectsFromArray:partArray];
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeSearchPublicChatRooms data:outArray];
                }
            }
        }
        else
        {
            FTLog(@"Error parseGetAllRoomsForUser response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}


/* =======================================================================================
 message GetAllUsersForRoomRequest {
 required PUUID id = 1;
 }
 */
-(void) getAllUsersForRoomRequest:(id)params//:(SCChatItem*) chatItem
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        if([[params allKeys] containsObject:@"SCChatItem"])
        {
            SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
            
            GetAllUsersForRoomRequestBuilder *getAllUsersForRoomRequestBuilder = [[GetAllUsersForRoomRequestBuilder alloc] init];
            [getAllUsersForRoomRequestBuilder setId:[self puuidBuilder:chatItem.chatPuuid]];
            
            RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
            [_requestBuilder setType:CallTypeGetAllUsersForRoom];
            [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
            [_requestBuilder setRequest:[getAllUsersForRoomRequestBuilder build].data];
            
            Request * query = [_requestBuilder build];
            NSData* dataToSend = [query data];
            [_socketClient sendData:dataToSend];
        }
        else if ([[params allKeys] containsObject:@"SCChatItemArray"])
        {
//#warning ДОДЕЛАТЬ
        }
        //
        
    }
}
// response is GetAllUsersForRoomResponse.
-(void) parseGetAllUsersForRooms:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            NSMutableArray* outUsers = [[NSMutableArray alloc] init];
            NSArray* arUsers =[GetAllUsersForRoomResponse parseFromData:response.response].users;
            for(UserShort* userShort in arUsers)
            {
//                [_settings.modelContacts getContactByPuiid:[self scpuuidBuilder:userShort.userId]];
                
//                SCContactItem* contactItem = [[SCContactItem alloc] initWithSettings:_settings];
//                contactItem.userPUUID = [self scpuuidBuilder:userShort.userId];
                
                SCContactItem* contactItem = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:userShort.userId]];
                if(userShort.hasNickName)
                    contactItem.contactNikName = userShort.nickName;
                if(userShort.hasFileImageId)
                {
                    [contactItem.netFile setImageNetPath:[[self scpuuidBuilder:userShort.fileImageId] scpuuidToString]];
                }
                [outUsers addObject:[_settings.modelUsers getUser:contactItem]];
            }
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeGetAllUsersForRoom data:outUsers];
            }
        }
        else
        {
            FTLog(@"Error parseGetAllUsersForRooms response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

/* =======================================================================================
 message LeaveUserFromRoomRequest {
 required PUUID id = 1;
 required PUUID user_id = 2;
 }
 */
-(void) leaveUserFromRoomRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        SCContactItem* contactItem = [params objectForKey:@"SCContactItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];

        LeaveUserFromRoomRequestBuilder* leaveUserFromRoomRequestBuilder = [[LeaveUserFromRoomRequestBuilder alloc] init];
        [leaveUserFromRoomRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
        [leaveUserFromRoomRequestBuilder setUserId:[self puuidBuilder:contactItem.userPUUID]];

        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeLeaveUserFromRoom];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[leaveUserFromRoomRequestBuilder build].data];
        Request * _query = [_requestBuilder build];
        NSData* dataToSend = [_query data];
        [_socketClient sendData:dataToSend];

    }
}//no response

-(void) parseLeaveUserFromRoom:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                if([NSStringFromClass([delegate class]) isEqualToString:@"SCChatUsersView"] || [NSStringFromClass([delegate class]) isEqualToString:@"SCAddUserToChatViewController"])
                    [delegate parsedType:SCPBManagerParceTypeLeaveUserFromRoom data:[params objectForKey:@"SCContactItem"]];
                else
                    [delegate parsedType:SCPBManagerParceTypeLeaveUserFromRoom data:[params objectForKey:@"SCChatItem"]];
            }
        }
        else
        {
            FTLog(@"Error parseJoinUserToRoom response.status.success");
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeLeaveUserFromRoom data:nil];
            }
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

/* =======================================================================================
 message JoinUserToRoomRequest {
 required PUUID id = 1;
 required PUUID user_id = 2;
 */
-(void) joinUserToRoomRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        SCContactItem* contactItem = [params objectForKey:@"SCContactItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        JoinUserToRoomRequestBuilder* joinUserToRoomRequestBuilder = [[JoinUserToRoomRequestBuilder alloc] init];
        [joinUserToRoomRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
        [joinUserToRoomRequestBuilder setUserId:[self puuidBuilder:contactItem.userPUUID]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeJoinUserToRoom];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[joinUserToRoomRequestBuilder build].data];
        Request * _query = [_requestBuilder build];
        NSData* dataToSend = [_query data];
        [_socketClient sendData:dataToSend];
    }
}//no response

-(void) parseJoinUserToRoom:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            
            //        [self callDelegate:SCPBManagerParceTypeSendMessage withData:nil];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                if([NSStringFromClass([delegate class]) isEqualToString:@"SCChatUsersView"] || [NSStringFromClass([delegate class]) isEqualToString:@"SCAddUserToChatViewController"])
                    [delegate parsedType:SCPBManagerParceTypeJoinUserToRoom data:[params objectForKey:@"SCContactItem"]];
                else
                    [delegate parsedType:SCPBManagerParceTypeJoinUserToRoom data:[params objectForKey:@"SCChatItem"]];
            }
        }
        else
        {
            FTLog(@"Error parseJoinUserToRoom response.status.success");
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
//                if([delegate isKindOfClass:[SCAddUserToChatViewController class]])
                    [delegate parsedType:SCPBManagerParceTypeJoinUserToRoom data:nil];
            }
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}
/* =======================================================================================
message JoinUsersToRoomRequest {
    required PUUID chat_room_id = 1; /// ChatRoom id
    repeated PUUID members = 2; /// Users, which will join to ChatRoom
}*/
-(void) joinUsersToRoomRequest:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        NSArray* contactArray = [params objectForKey:@"ContactArray"];
//        SCContactItem* contactItem;// = [params objectForKey:@"SCContactItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        JoinUsersToRoomRequestBuilder* joinUsersToRoomRequestBuilder = [[JoinUsersToRoomRequestBuilder alloc] init];
        [joinUsersToRoomRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
        for(SCContactItem* contactItem in contactArray)
        {
            [joinUsersToRoomRequestBuilder addMembers:[self puuidBuilder:contactItem.userPUUID]];
        }
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeJoinUsersToRoom];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[joinUsersToRoomRequestBuilder build].data];
        Request * _query = [_requestBuilder build];
        NSData* dataToSend = [_query data];
        [_socketClient sendData:dataToSend];
    }
}//no response

-(void) parseJoinUsersToRoom:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            
            //        [self callDelegate:SCPBManagerParceTypeSendMessage withData:nil];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeJoinUserToRoom data:[params objectForKey:@"SCContactItem"]];
            }
        }
        else
        {
            FTLog(@"Error parseJoinUserToRoom response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}
/* =======================================================================================
message GetLastReadMessageDateRequest {
    required PUUID chat_room_id = 1; /// ChatRoom id
}*/
-(void) getLastReadMessageDate:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        GetLastReadMessageDateRequestBuilder* _getLastReadMessageDateRequestBuilder = [[GetLastReadMessageDateRequestBuilder alloc] init];
        [_getLastReadMessageDateRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeLastReadMessageDateGet];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_getLastReadMessageDateRequestBuilder build].data];
        Request * _query = [_requestBuilder build];
        NSData* dataToSend = [_query data];
        [_socketClient sendData:dataToSend];
    }
}
/*
message GetLastReadMessageDateResponse {
    required sint64 last_read_date = 1; /// Date of last message read
}*/
-(void) parseGetLastReadMessageDate:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            GetLastReadMessageDateResponse* _getLastReadMessageDateResponse = [GetLastReadMessageDateResponse parseFromData:response.response];
            NSDate* lastReadDate = [NSDate dateWithTimeIntervalSince1970:((SInt64)_getLastReadMessageDateResponse.lastReadDate/1000)];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeGetLastReadMessageDate data:lastReadDate];
            }
        }
        else
        {
            FTLog(@"Error parseGetLastReadMessageDate response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}


/* =======================================================================================
message SetLastReadMessageDateRequest {
    required PUUID chat_room_id = 1; /// ChatRoom id
    required sint64 last_read_date = 2; /// Date of last read message
}*/

-(void) setLastReadMessageDate:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        NSDate* dtStart = [params objectForKey:@"lastReadDate"];
        
        SetLastReadMessageDateRequestBuilder* _setLastReadMessageDateRequestBuilder = [[SetLastReadMessageDateRequestBuilder alloc] init];
        [_setLastReadMessageDateRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
        [_setLastReadMessageDateRequestBuilder setLastReadDate:(SInt64)[dtStart timeIntervalSince1970]*1000];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeLastReadMessageDateSet];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setRequest:[_setLastReadMessageDateRequestBuilder build].data];
        Request * _query = [_requestBuilder build];
        NSData* dataToSend = [_query data];
        [_socketClient sendData:dataToSend];
    }
}

-(void) parseSetLastReadMessageDate:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeSetLastReadMessageDate data:params];
            }
        }
        else
        {
            FTLog(@"Error parseJoinUserToRoom response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}




#pragma mark - notifications
/* =======================================================================================
 
*/
-(void) parseChangeRoomEventResponse:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        ChangeRoomEventResponse* changeRoomEventResponse = [ChangeRoomEventResponse parseFromData:response.response];
        SCChatItem* inChat = nil;
        if(changeRoomEventResponse.hasChatRoom)
        {
            ChatRoom* inChatRoom = changeRoomEventResponse.chatRoom;
            inChat = [self _chatRoomToScChatRoom:inChatRoom];
        }
/* old code
        SCChatItem* inChat = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:changeRoomEventResponse.roomId]];
        if( inChat == nil )
        {
            inChat = [[SCChatItem alloc] initWithSettings:_settings];
            inChat.chatPuuid = [[SCPUUID alloc] initWithLeastSignificantBits:changeRoomEventResponse.roomId.leastSignificantBits MostSignificantBits:changeRoomEventResponse.roomId.mostSignificantBits];
        }
        
        if(changeRoomEventResponse.chatRoom.hasName)
            inChat.chatName = changeRoomEventResponse.chatRoom.name;
        if(changeRoomEventResponse.chatRoom.hasChatDescription)
            inChat.chatDescription = changeRoomEventResponse.chatRoom.chatDescription;
        if(changeRoomEventResponse.chatRoom.hasTimeDestroy)
            inChat.timeDestroy = changeRoomEventResponse.chatRoom.timeDestroy;
        if(changeRoomEventResponse.chatRoom.hasChatRoomType)
            inChat.chatType = [self ChatRoomTypeToSCChatRoomType:changeRoomEventResponse.chatRoom.chatRoomType];
#ifdef OLD_FILE_LINK
        if(changeRoomEventResponse.chatRoom.hasFileImageId)
            inChat.chatImageLink = [self scpuuidStringBuilder: changeRoomEventResponse.chatRoom.fileImageId];
#endif
        if(changeRoomEventResponse.chatRoom.hasFileImageId)
        {
            [inChat.netFile setImageNetPath:[self scpuuidStringBuilder: changeRoomEventResponse.chatRoom.fileImageId]];
        }

        if(changeRoomEventResponse.chatRoom.hasSecurityLevel)
            inChat.securityLevel = changeRoomEventResponse.chatRoom.securityLevel;
*/
//        [inChat updateItem];? нахуа
        if([_settings.modelChats respondsToSelector:@selector(parsedType:data:)])
        {
            [_settings.modelChats parsedType:SCPBManagerParceTypeChangeRoomEvent data:inChat];
        }
    }
    else
    {
        FTLog(@"Error parseChangeRoomEventResponse response.status.success");
    }
}

/* =======================================================================================
Response sends to all users in ChatRoom (exclude request sender), when new user was joined. Returns ChatRoom id, id of joined user and short info about this user
*/
-(void) parseJoinRoomEventResponse:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        JoinRoomEventResponse* joinRoomEventResponse = [JoinRoomEventResponse parseFromData:response.response];
        SCChatItem* chatItem = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:joinRoomEventResponse.roomId]];
        
        if(chatItem != nil)
        {
            FTLog(@"");
            if([_settings.modelChats respondsToSelector:@selector(parsedType:data:)])
            {
                [_settings.modelChats parsedType:SCPBManagerParceTypeJoinUserToRoomEvent data:chatItem];
            }
        }
        else
        {
            chatItem = [[SCChatItem alloc] initWithSettings:_settings];
            chatItem.chatPuuid = [self scpuuidBuilder:joinRoomEventResponse.roomId];
            SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
            NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
            [params setObject:transportPUUID forKey:@"UUID"];
            [params setObject:_settings.modelChats forKey:@"delegate"];
            [params setObject:chatItem forKey:@"SCChatItem"];
            [params setObject:[NSNumber numberWithInteger:SCPBManagerParceTypeJoinUserToRoomEvent] forKey:@"type"];
            
            [self getChatRoomByIdRequest:params];
        }
    }
}

/* =======================================================================================
 Response sends to all users in ChatRoom (exclude request sender), when user was leaved this ChatRoom. Returns ChatRoom id and if of user, who leaved ChatRoom.
*/
-(void) parseLeaveRoomEventResponse:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        LeaveRoomEventResponse* leaveRoomEventResponse = [LeaveRoomEventResponse parseFromData:response.response];
    }
}
-(void) parseRemoveRoomEventResponse:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        RemoveRoomEventResponse* removeRoomEventResponse = [RemoveRoomEventResponse parseFromData:response.response];
        SCChatItem* chatItem = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:removeRoomEventResponse.roomId]];
        
        if(chatItem != nil)
        {
            if ([_settings.modelChats respondsToSelector:@selector(parsedType:data:)])
            {
                [_settings.modelChats parsedType:SCPBManagerParceTypeRemoveChat data:chatItem];
            }
        }
    }
    else
    {
        FTLog(@"Error parseRemoveChatroom response.status.success");
    }
}

#pragma mark - PRIVATE
-(SCChatItem*) _chatRoomToScChatRoom:(ChatRoom*) chatRoom
{
    BOOL isNew = NO;
    SCChatItem* chatItem = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:chatRoom.id]];
    
    if(chatItem == nil)
    {
        chatItem = [[SCChatItem alloc] initWithSettings:_settings];
        chatItem.chatPuuid = [self scpuuidBuilder:chatRoom.id];
        isNew = YES;
    }

    chatItem.itemUpdatedTime = [NSDate date];
    if(chatRoom.hasIsDeleted)
        chatItem.isDeleted = chatRoom.isDeleted;

    if(chatRoom.hasLastReadDate)
        chatItem.lastViewedMessageDate = [NSDate dateWithTimeIntervalSince1970:((SInt64)chatRoom.lastReadDate/1000)];
    else
        chatItem.lastViewedMessageDate = [NSDate dateWithTimeIntervalSince1970:0];

    if(chatRoom.hasChatRoomType)
    {
        chatItem.chatType = [self ChatRoomTypeToSCChatRoomType:chatRoom.chatRoomType];
    }
    BOOL isLeaved = YES;

    if(chatItem.chatType == SCChatTypePrivate)
    {
        chatItem.timeDestroy = 0;
        if(chatRoom.hasSecurityLevel)
            chatItem.securityLevel = chatRoom.securityLevel;
        
        if(chatRoom.hasOwnerId)
        {
            chatItem.chatOwner = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:chatRoom.ownerId]];
        }
        if(chatRoom.usersInRoom.count == 2)
        {
            
            for(UserShort* userShort in chatRoom.usersInRoom)
            {
                SCContactItem* userItem = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:userShort.userId]];
                if([userItem isEqual:_settings.modelProfile.contactItem])
                    isLeaved = NO;
                if(userShort.hasNickName)
                    userItem.contactNikName = userShort.nickName;
                if(userShort.hasFileImageId)
                {
                    [userItem.netFile setImageNetPath:[[self scpuuidBuilder:userShort.fileImageId] scpuuidToString]];
                    userItem.netFile.logoText = userItem.contactNikName;
                }
                
                if(![userItem.userPUUID isEqual:_settings.modelProfile.contactItem.userPUUID])
                {
                    chatItem.chatName = [NSString stringWithFormat:@"@%@",userItem.contactNikName];
                    chatItem.chatDescription = [NSString stringWithFormat:@"Private room with @%@",userItem.contactNikName];
                    
                    chatItem.logoSource = userItem;
                    if(userShort.hasFileImageId)
                        [chatItem.netFile setImageNetPath:userItem.netFile.netPath];
                    chatItem.netFile.logoText = userShort.nickName;
                }
                [chatItem.usersInChat addObject:userItem];
            }
        }
        else
        {
            if(chatItem.isDeleted)
            {
                FTLog(@"chatItem.isDeleted");
            }
            chatItem.isDeleted = YES;
            chatItem.logoSource = chatItem;
            return chatItem;
        }
    }
    else
    {
        if(chatRoom.hasName)
            chatItem.chatName = chatRoom.name;
        if(chatRoom.hasChatDescription)
            chatItem.chatDescription = chatRoom.chatDescription;
        if(chatRoom.hasTimeDestroy)
            chatItem.timeDestroy = chatRoom.timeDestroy;
        if(chatRoom.hasFileImageId)
        {
            chatItem.logoSource = chatItem;
            [chatItem.netFile setImageNetPath:[[self scpuuidBuilder:chatRoom.fileImageId] scpuuidToString]];
            chatItem.netFile.logoText = chatItem.chatName;
        }
        if(chatRoom.hasSecurityLevel)
            chatItem.securityLevel = chatRoom.securityLevel;
        if(chatRoom.hasOwnerId)
            chatItem.chatOwner = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:chatRoom.ownerId]];

        for(UserShort* userShort in chatRoom.usersInRoom)
        {
            SCContactItem* userItem = [_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:userShort.userId]];
            if([userItem isEqual:_settings.modelProfile.contactItem])
                isLeaved = NO;
            if(userShort.hasNickName)
                userItem.contactNikName = userShort.nickName;
            if(userShort.hasFileImageId)
            [userItem.netFile setImageNetPath:[[self scpuuidBuilder:userShort.fileImageId] scpuuidToString]];
            if(userShort.hasStatus)
                userItem.contactStatus = [self statusToSCContactStatus:userShort.status];
            
//            [chatItem.usersInChat addObject:[_settings.modelUsers getUser:userItem]];
            [chatItem addUserInChat:[_settings.modelUsers getUser:userItem]];
        }
        chatItem.logoSource = chatItem;
    }
    if (isLeaved && chatItem.chatType != SCChatTypePublic)
    {
        chatItem.isDeleted = YES;
    }

/*
    if(isNew)
    {
        FTLog(@"is NEW item");
//        [chatItem se
//        [chatItem loadCachedMessages];
    }
    else
    {
        FTLog(@"exist item");
//        [chatItem restore];
    }
*/    
    return chatItem;
}



@end
