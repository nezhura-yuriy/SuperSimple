//
//  SCPBManagerTernopol+User.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol.h"

@interface SCPBManagerTernopol (User)


-(void) parseSearchUsersByAuth:(id)data;
-(void) parseGetUserById:(id)data;
-(void) parseInviteUsersRequest:(id)data;
-(void) parseSubscribe:(id)data;
-(void) parseUnSubscribe:(id)data;
-(void) parseGetAllUsersWhichISubscribed:(id)data;
-(void) parseCheckIfUserInBlackListRequest:(id)data;
-(void) parseAddUsersToBlackListRequest:(id)data;
-(void) parseRemoveUsersFromBlackListRequest:(id)data;
-(void) parseGetBannedUsersRequest:(id)data;
-(void) parseGetUserByAuth:(id)data;




#pragma mark - Notifications
-(void) parseChangeUserStatusEvent:(id)data;
-(void) parseUpdateProfileEvent:(id)data;


@end
