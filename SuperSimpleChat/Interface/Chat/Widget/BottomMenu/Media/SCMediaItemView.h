//
//  SCMediaItem.h
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 06.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SCMediaItem.h"
#import "SCInterfaceDelegates.h"
@class SCSettings;


@interface SCMediaItemView : UIView

@property(nonatomic,strong) SCMediaItem *mediaItem;
@property(nonatomic,strong) id<SCBottomViewsDelegate> delegate;

-(id) initWithSettings:(SCSettings*) settings;

@end
