//
//  SCChatTopMenuView.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 09.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"


@class SCSettings;


@interface SCChatTopMenuView : UIView


@property(nonatomic,strong) SCSettings* settings;

@property(nonatomic,strong) IBOutlet UIButton *btnPhoto;
@property(nonatomic,strong) IBOutlet UIButton *btnAudio;
@property(nonatomic,strong) IBOutlet UIButton *btnVideo;
@property(nonatomic,strong) IBOutlet UIButton *btnUser;
@property(nonatomic,strong) IBOutlet UIButton *btnDestroy;

-(void) setAfterSelect:(void(^)(SCChatTopMenuViewItem selectTopItem)) completionBlock;

@end
