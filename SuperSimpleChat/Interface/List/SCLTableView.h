//
//  SCLTableView.h
//  SuperSimpleChat
//
//  Created by yury on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCLTableViewDataSource.h"
#import "SCSettings.h"

extern float const kRowHeight;

@interface SCLTableView : UIView <UIScrollViewDelegate>

@property (nonatomic, weak) id <UIScrollViewDelegate> delegate;
@property (nonatomic, weak) id <SCLTableViewDataSource> datasource;
@property (nonatomic, strong, readonly) UIScrollView *scrollView;

@property (nonatomic, strong) SCSettings *settings;

- (UIView *) dequeueReusableCell;
- (NSArray *) visibleCells;
- (void) reloadData;
- (void) registerClassForCells: (Class)cellClass;

- (void) changeCellItemPUUID: (SCPUUID *) searchItemPUUID on: (SCPUUID *) rightItemPUUID;

@end
