//
//  SCMessageItem.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageItem.h"
#import "SCSettings.h"
#import "SCModelUser.h"
//#import "SCMessageBodys.h"
#import "SCChatItem.h"


@implementation SCMessageItem
{
    NSTimer* _destroyTimer;
}
-(id) init
{
    self = [super init];
    if( self )
    {
        [self _init];
    }
    return self;
}
-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if( self )
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(id) initWithChatItem:(SCChatItem*) chatItem
{
    self = [super init];
    if( self )
    {
        _settings = chatItem.settings;
        [self _init];
        _chat = chatItem;
    }
    return self;
}

-(id) initWithDictionary:(NSDictionary*) dictionary
{
    self = [super init];
    if( self )
    {
        [self _init];
        [self fromDictionary:dictionary];
    }
    return self;
}

- (void)dealloc
{
    if(_destroyTimer)
    {
        [_destroyTimer invalidate];
        _destroyTimer = nil;
    }
}

-(void) _init
{
//    _messageBodys = [[NSMutableArray alloc] init];
//    _chat = [[SCChatItem alloc] init];
    _messageBodys = [[SCMessageBodys alloc] initWithSettings:_settings];
    _contact = [[SCContactItem alloc] initWithSettings:_settings];
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    if([[dictionary allKeys] containsObject:@"messageID"])
    {
        _messageID = [dictionary objectForKey:@"messageID"];
    }
    if([[dictionary allKeys] containsObject:@"chat"])
    {
        if(![[_chat.chatPuuid scpuuidToString] isEqualToString:[dictionary objectForKey:@"chat"]])
            FTLog(@"WARNING !!!! UUID no ecvivalent %@ != %@",[_chat.chatPuuid scpuuidToString],[dictionary objectForKey:@"chat"]);
    }
    
    if([[dictionary allKeys] containsObject:@"contactPUUID"])
    {
        if(_settings)
        {
            _contact = [_settings.modelUsers getUserByPUUIDString:[dictionary objectForKey:@"contactPUUID"]];
        }
//        _contact = [[SCContactItemSmall alloc] initWithSettings:_settings];
//        [_contact fromDictionary:[dictionary objectForKey:@"contact"]];
    }
    
    if([[dictionary allKeys] containsObject:@"messageDateTime"])
    {
        _messageDateTime = [dictionary objectForKey:@"messageDateTime"];
    }
    else
    {
        _messageDateTime = [NSDate date];
    }
    
    if([[dictionary allKeys] containsObject:@"timeDestroy"])
    {
        _timeDestroy = [[dictionary objectForKey:@"timeDestroy"] integerValue];
    }
    
    if([[dictionary allKeys] containsObject:@"securityLevel"])
    {
        _securityLevel = (SCMessageSecurityLevel)[[dictionary objectForKey:@"securityLevel"] integerValue];
    }
    
    if([[dictionary allKeys] containsObject:@"messageBodys"])
    {
        for(NSDictionary* bodyDict in [dictionary objectForKey:@"messageBodys"])
        {
            SCMessageBodyItem* mb = [[SCMessageBodyItem alloc] initWithSettings:_settings];
            mb.type = (SCDataType)[[bodyDict objectForKey:@"type"] longValue];
            //old (SCMessageDataType)[[bodyDict objectForKey:@"type"] longValue];
            mb.data = [bodyDict objectForKey:@"data"];
            [_messageBodys addItem:mb];
        }
    }
    /*
    if([[dictionary allKeys] containsObject:@"isDeleted"])
        _isDeleted = [[dictionary objectForKey:@"isDeleted"] boolValue];
    
    if([[dictionary allKeys] containsObject:@"dateDestroy"])
        _dateDestroy = [dictionary objectForKey:@"dateDestroy"];
    */
    if(_timeDestroy > 0)
    {
        _dateDestroy = [_messageDateTime dateByAddingTimeInterval:_timeDestroy];
        NSComparisonResult result = [[NSDate date] compare:_dateDestroy];
        if(result == NSOrderedDescending)
        {
            _isDeleted = YES;
        }
        else
        {
            
        }
    }

}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    [outDict setObject:_messageID forKey:@"messageID"];
    [outDict setObject:[_chat.chatPuuid scpuuidToString] forKey:@"chat"];
    if(_contact.contactPUUID)
        [outDict setObject:[_contact.contactPUUID scpuuidToString] forKey:@"contactPUUID"];// ????
    [outDict setObject:[NSNumber numberWithInteger:_securityLevel] forKey:@"securityLevel"];
    [outDict setObject:[NSNumber numberWithInteger:_timeDestroy] forKey:@"timeDestroy"];
    [outDict setObject:_messageDateTime forKey:@"messageDateTime"];
    NSMutableArray* bodyArray = [[NSMutableArray alloc] init];
    for(SCMessageBodyItem* bodyItem in [_messageBodys items])
    {
        NSMutableDictionary* bodyDict = [[NSMutableDictionary alloc] init];
        [bodyDict setObject:[NSNumber numberWithInteger:bodyItem.type] forKey:@"type"];
        [bodyDict setObject:bodyItem.data forKey:@"data"];
        [bodyArray addObject:bodyDict];
    }
    [outDict setObject:bodyArray forKey:@"messageBodys"];
    
    return outDict;
}

//-(NSArray*) messageBodysAR
//{
//    return [_messageBodys items];
//}

-(void) setTimeDestroy:(NSTimeInterval)timeDestroy
{
    _timeDestroy = timeDestroy;
    if(timeDestroy > 0)
    {
        _dateDestroy = [_messageDateTime dateByAddingTimeInterval:_timeDestroy];
        NSComparisonResult result = [[NSDate date] compare:_dateDestroy];
        if(result == NSOrderedDescending)
        {
            _isDeleted = YES;
        }
        else
        {
// большая нагрузка, не стоит
//            _destroyTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(destroyTicks:) userInfo:nil repeats:YES];
        }
    }

}

-(CGFloat) lifeTime
{
    if(_timeDestroy > 0)
    {
        if(!_dateDestroy)
            _dateDestroy = [_messageDateTime dateByAddingTimeInterval:_timeDestroy];
        return [_dateDestroy timeIntervalSinceDate:[NSDate date]];
    }
    else
        return CGFLOAT_MAX;
}

/*
-(void) destroyTicks:(NSTimer*) timer
{
    NSComparisonResult result = [[NSDate date] compare:_dateDestroy];
    if(result == NSOrderedDescending)
    {
        _isDeleted = YES;
        if(_destroyTimer)
        {
            [_destroyTimer invalidate];
            _destroyTimer = nil;
        }
        for(id<SCPBManagerDelegate> delegate in _delegates)
        {
            if([delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeInternalMessageItemTickDestroy data:self];
            }
        }
    }
    else
    {
        for(id<SCPBManagerDelegate> delegate in _delegates)
        {
            if([delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeInternalMessageItemTickDestroy data:self];
            }
        }
    }
}
*/
-(NSString*) description
{
    return [NSString stringWithFormat:@"{\n_messageID = %@\n_chat = %@, \n_contact = %@, \n_securityLevel = %d, \n_timeDestroy = %f, \n_messageDateTime = %@\n}",_messageID,_chat,_contact,_securityLevel,_timeDestroy,_messageDateTime];

}

@end
