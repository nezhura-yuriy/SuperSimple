//
//  SCMessageBodySoundGroupView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 20.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodySoundGroupView.h"
#import "SCMessageBodySoundItemView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"
#import "SCNetFile.h"

@implementation SCMessageBodySoundGroupView
{
    
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    [super _init];
//    _viewsArray = [[NSMutableArray alloc] init];
    [self addSubview:_itemsScroll];
        
}

-(void) dealloc
{
    FTLog(@"%@ : dealloc",NSStringFromClass([self class]));
}

-(void) setMessageBodys:(SCMessageBodys*) messageBodys
{
    NSArray* soundsArray = [messageBodys itemsByType:SCDataTypeAudio];
    CGFloat posX = 0;
    if(soundsArray.count > 1)
    {// add prev
        _btnPrev = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnPrev addTarget:self action:@selector(btnPrevAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btnPrev setImage:[_settings.skinManager getImageForKeyX:_btnPrevImage] forState:UIControlStateNormal];
        _btnPrev.frame = CGRectMake(posX, 0, NEXT_WIDTH, ICON_SIZE);
        _btnPrev.hidden = YES;
        posX += NEXT_WIDTH;
        [self addSubview:_btnPrev];
    }
    
    for(SCMessageBodyItem* bodyItem in soundsArray)
    {
        SCMessageBodySoundItemView* _soundViewItem = [[SCMessageBodySoundItemView alloc] initWithSettings:_settings];
        [_soundViewItem setAudioNetFile:bodyItem.netFile];
        _itemWidth = MAX(_itemWidth,_soundViewItem.viewSize.width);
        _itemHeight = MAX(_itemHeight,_soundViewItem.viewSize.height);
        _soundViewItem.frame = CGRectMake(0, 0, _soundViewItem.viewSize.width,  _soundViewItem.viewSize.height);
        posX += _soundViewItem.viewSize.width + MARGIN;
        [_itemsScroll addSubview:_soundViewItem];
        [_viewsArray addObject:_soundViewItem];
    }
    
    if(soundsArray.count > 1)
    {// add next
        _btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnNext addTarget:self action:@selector(btnNextAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btnNext setImage:[_settings.skinManager getImageForKeyX:_btnNextImage] forState:UIControlStateNormal];
        posX += MARGIN;
        _btnNext.frame = CGRectMake(posX, 0, NEXT_WIDTH, ICON_SIZE);
        [self addSubview:_btnNext];
    }
}

-(CGFloat) getItemWidth
{
    return _itemWidth;
}

-(CGSize) viewSize
{
    if(_viewsArray.count >1)
        return CGSizeMake(_itemWidth + 2*(NEXT_WIDTH + NEXT_MARGIN),_itemHeight);
    else
        return CGSizeMake(_itemWidth,_itemHeight);
}

@end

