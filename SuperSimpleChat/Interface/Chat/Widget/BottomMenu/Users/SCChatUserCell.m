//
//  SCChatUserCell.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 24.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatUserCell.h"
#import "SCSettings.h"


@implementation SCChatUserCell
{
    SCSettings* _settings;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _init];
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        [self _init];
    }
    return self;
}


-(void) _init
{
    _settings = [SCSettings sharedSettings];
    
    //    self.backgroundColor = CL_CLEAR_COLOR;
    [self skinChange];
    
}

-(void) dealloc
{
    
}

-(void) prepareForReuse
{
    [self skinChange];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
}

-(void) skinChange
{
    
}

/*
-(void) setChatData:(SCChatItem *)chatData
{
    _chatData = chatData;
    _labName.text = _chatData.chatName;
    _labDescript.text = _chatData.chatDescription;
    
    _imageChat.image = [UIImage imageNamed:@"nofoto"];
    _imageChat.layer.borderWidth = SEPARATOR_HEIGHT;
    _imageChat.layer.borderColor = [[UIColor whiteColor] CGColor];
    _imageChat.layer.cornerRadius = CHAT_IMAGE_SIZE*0.5;
    
    if(_chatData.notViewedMessagesCount > 0)
    {
        _labCount.hidden = NO;
        _labCount.text = [NSString stringWithFormat:@"%ld",(long)_chatData.notViewedMessagesCount];
        _labCount.layer.cornerRadius = _labCount.frame.size.width*0.5;
    }
    else
        _labCount.hidden = YES;
    
}*/
 
@end
