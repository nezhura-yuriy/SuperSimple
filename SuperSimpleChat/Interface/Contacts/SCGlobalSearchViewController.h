//
//  SCGlobalSearchViewController.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 22.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SCSettings;

@interface SCGlobalSearchViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic,strong) SCSettings* settings;

@property (weak, nonatomic) IBOutlet UISearchBar *userSearchBar;

@end
