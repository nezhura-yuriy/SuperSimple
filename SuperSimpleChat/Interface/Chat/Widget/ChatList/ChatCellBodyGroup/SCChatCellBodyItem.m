//
//  SCChatCellBodyItem.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 24.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatCellBodyItem.h"


#define ICON_TYPE_SIZE 22
#define ICON_TYPE_MARGIN 5

@implementation SCChatCellBodyItem
{
    UIImageView* _previewImage;
    UIImageView* _iconType;
}

- (instancetype)initWithSettings:(SCSettings *)settings
{
    self = [super initWithSettings:settings];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    _previewImage = [[UIImageView alloc] init];
    _previewImage.contentMode = UIViewContentModeScaleAspectFit;
    
    _iconType = [[UIImageView alloc] init];
    _iconType.contentMode = UIViewContentModeScaleAspectFit;

    [self addSubview:_previewImage];
    [self addSubview:_activityIndicatorView];
    [self.layer addSublayer:shadowLayer];
    [self.layer addSublayer:shapeLayer];
    [self addSubview:_iconType];
}

- (void)dealloc
{
    
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        shadowLayer.frame = self.bounds;
        shapeLayer.frame = self.bounds;
        _activityIndicatorView.center = CGPointMake(frame.size.width/2, frame.size.height/2);;

        _previewImage.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        _iconType.frame = CGRectMake(ICON_TYPE_MARGIN, frame.size.height - ICON_TYPE_SIZE - ICON_TYPE_MARGIN, ICON_TYPE_SIZE, ICON_TYPE_SIZE);
    }
}

-(void) setStatus:(SCNetFileStatus) status
{
    [super setStatus:status];
    switch (status)
    {
        case SCNetFileStatusNew:
        case SCNetFileStatusStart:
        case SCNetFileStatusWait:
        case SCNetFileStatusTryRepeet:
        case SCNetFileStatusProgress:
        case SCNetFileStatusError:
            self.backgroundColor = [UIColor lightGrayColor];
            break;
        case SCNetFileStatusCompleet:
            self.backgroundColor = [UIColor clearColor];
            break;
            
        default: break;
    }
}


-(void) setIconType:(UIImage*) img
{
    _iconType.image = img;
}

-(void) setPreviewImage:(UIImage*) img
{
    _previewImage.image = img;
}
@end
