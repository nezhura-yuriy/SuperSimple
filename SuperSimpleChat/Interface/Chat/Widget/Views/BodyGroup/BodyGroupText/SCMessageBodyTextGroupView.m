//
//  SCMessageBodyTextView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyTextGroupView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"



@implementation SCMessageBodyTextGroupView
{
    NSMutableArray* _textsArray;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    [super _init];
    _textsArray = [[NSMutableArray alloc] init];
    [self addSubview:_itemsScroll];

}

-(void) dealloc
{
    FTLog(@"%@ : dealloc",NSStringFromClass([self class]));
    [_textsArray removeAllObjects];
    _textsArray = nil;
}

-(void) setMessageBodys:(SCMessageBodys*) messageBodys
{
    NSArray* textsArray = [messageBodys itemsByType:SCDataTypeText];
    CGFloat posY = 0;
    CGFloat caclWidth = 0;
    CGFloat caclHeight = 0;
    for(SCMessageBodyItem* bodyItem in textsArray)
    {
        UILabel* _label = [[UILabel alloc] initWithFrame:CGRectMake(0, posY, _maxWidht, 500)];
        _label.font = [UIFont fontWithName:FT_FontNameLight size:12.0];
        _label.text = (NSString*) bodyItem.data;
        _label.numberOfLines = 0;
        [_label sizeToFit];
        _label.frame = CGRectMake(0, posY, _label.frame.size.width, _label.frame.size.height);
        caclWidth = MAX(caclWidth, _label.frame.size.width);
        caclHeight = MAX(caclHeight,_label.frame.size.height);
        [self addSubview:_label];
        [_textsArray addObject:_label];
        posY += caclHeight;
    }
    _itemWidth = caclWidth;
    _itemHeight = caclHeight;
}

-(CGFloat) getItemWidth
{
    return _itemWidth;
}

/*
-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        CGFloat posX = MARGIN;
        
    }
}
*/

-(void) setBodyAlignment:(SCMessageAlignment)bodyAlignment
{
    _bodyAlignment = bodyAlignment;
    [self updateUI];
}


-(void) layoutSubviews
{
    [super layoutSubviews];
}

-(void) updateUI
{
    for(UILabel* _label in _textsArray)
    {
        switch (_bodyAlignment)
        {
            case SCMessageAlignmentLeft:
                _label.textColor = SKCOLOR_ChatMessageIncomingText;
                break;
            
            case SCMessageAlignmentRight:
                _label.textColor = SKCOLOR_ChatMessageOutgoingText;
                break;

            default:
                _label.textColor = SKCOLOR_ChatMessageDateText;
                break;
        }
    }
}

-(CGSize) viewSize
{
    return CGSizeMake(_itemWidth,_itemHeight);
}


@end
