//
//  SCCache.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 25.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCCache : NSObject

- (void)saveFile:(NSString*)uuid_ data:(NSData*)file_;
- (NSData*)getFile:(NSString*)uuid_;
- (NSString*)getFilePath:(NSString*)uuid_;
//- (NSString*)getName;

-(id)initWithCachePath:(NSString*) cachePath_;
-(void) setCacheImagePath:(NSString *)cachePath_;
@end
