//
//  SCMessageData.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"
#import "SCTypes.h"
#import "SCNetFile.h"


@interface SCMessageBodyItem : SCAbstractItem

@property(nonatomic,assign) SCDataType type;
@property(nonatomic,strong) NSObject* data;

@property(nonatomic,strong) SCNetFile* netFile;

-(void) toCommonDataType;
-(void) sendItem;
@end
