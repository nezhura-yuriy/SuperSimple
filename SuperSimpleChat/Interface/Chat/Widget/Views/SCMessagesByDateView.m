//
//  SCMessagesByDateView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 08.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessagesByDateView.h"
#import "SCSettings.h"
#import "SCMessageGroup.h"
//#import "SCChatItemView.h"
#import "SCMessageItemView.h"
#import "SCMessageItem.h"

@implementation SCMessagesByDateView
{
    SCSettings* _settings;
    NSDateFormatter* _df;
    NSMutableArray* _messageViewsItems;
    
    UILabel* _labDay;
    
    CGSize _viewSize;
    CGFloat _headerHeight;
}


-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if(self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    _isExpand = YES;
    _viewSize = CGSizeZero;
    _forWidth = [UIScreen mainScreen].bounds.size.width;
    _df = [[NSDateFormatter alloc] init];
    [_df setDateFormat:@"YYYY-MM-dd"];
    
    _messageViewsItems = [[NSMutableArray alloc] init];

    _labDay = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    [_labDay setFont:[UIFont fontWithName:FT_FontNameDefult size:16.0]];
    _labDay.text = @"Data";
    _labDay.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_labDay];

//    self.layer.borderColor = [[UIColor blackColor] CGColor];
//    self.layer.borderWidth = 1;
//    _labDay.layer.borderWidth = 1;
//    _labDay.layer.borderColor = [[UIColor purpleColor] CGColor];
}

-(void) dealloc
{
    
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        
    }
}
-(void) layoutSubviews
{
//    FTLog(@"SCMessagesByDateView layoutSubviews");
    [super layoutSubviews];
    _labDay.frame = CGRectMake(0, 0, _forWidth, _headerHeight);
}

-(void) setMessageGroup:(SCMessageGroup *)messageGroup
{
    _messageGroup = messageGroup;
    
    _labDay.text = [_df stringFromDate:_messageGroup.messagesDate];
    [_labDay sizeToFit];
    _headerHeight = _labDay.frame.size.height;
    
    [self _makeViews];
    
}

-(SCMessageItemView*) searchViewForItem:(SCMessageItem*) searchItem
{
    for(SCMessageItemView* itemView in _messageViewsItems)
    {
        if([itemView.messageItem isEqual:searchItem])
            return itemView;
    }
    return nil;
}

-(void) updateGroupMessages
{
    /* old
    CGFloat posY_ = _headerHeight;
    for(SCMessageItemView* itemView in _messageViewsItems)
    {
        if(itemView.messageItem.isDeleted)
        {
            [itemView removeFromSuperview];
        }
        else
        {
            itemView.frame = CGRectMake(0,posY_,_forWidth,itemView.viewSize.height);
            posY_ +=itemView.viewSize.height;

        }
    }
    _viewSize = CGSizeMake(_viewSize.width, _headerHeight + posY_);
    */
    
    CGFloat posY = _headerHeight;
    for(SCMessageItem* makedItem in _messageGroup.chatMessages)
    {
        SCMessageItemView* itemView = [self searchViewForItem:makedItem];
        if(itemView)
        {
            if(itemView.messageItem.isDeleted)
            {
                [itemView removeFromSuperview];
            }
            else
            {
                itemView.frame = CGRectMake(0,posY,_forWidth,itemView.viewSize.height);
                posY +=itemView.viewSize.height;
            }
        }
        else
        {
            if(!makedItem.isDeleted)
            {
                // create
                SCMessageItemView* itemView = [[SCMessageItemView alloc] initWithSettings:_settings forWidth:_forWidth];
                itemView.messageItem = makedItem;
                
                [_messageViewsItems addObject:itemView];
                [self addSubview:itemView];
                
                // positions
                itemView.frame = CGRectMake(0,posY,_forWidth,itemView.viewSize.height);
                posY +=itemView.viewSize.height;
            }
        }
    }
    _viewSize = CGSizeMake(_viewSize.width, _headerHeight + posY);
}

-(void) _makeViews
{
    FTLog(@"Begin make and place day");
    CGFloat posY = _headerHeight;
    for(SCMessageItem* makedItem in _messageGroup.chatMessages)
    {
        if(!makedItem.isDeleted)
        {
            // create
            SCMessageItemView* itemView = [[SCMessageItemView alloc] initWithSettings:_settings forWidth:_forWidth];
            itemView.messageItem = makedItem;
            
            [_messageViewsItems addObject:itemView];
            [self addSubview:itemView];
            
            // positions
            itemView.frame = CGRectMake(0,posY,_forWidth,itemView.viewSize.height);
            posY +=itemView.viewSize.height;
        }
    }
    _viewSize = CGSizeMake(_viewSize.width, _headerHeight + posY);
    FTLog(@"End make and place day");
}

-(CGSize) viewSize
{
    if(_isExpand)
        return _viewSize;
    else
    {
        // calculate height
        return CGSizeMake(320, 44);
    }
    return CGSizeZero;
}
@end
