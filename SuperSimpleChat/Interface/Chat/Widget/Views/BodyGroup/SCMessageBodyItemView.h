//
//  SCMessageBodyItemView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCViewWithProgress.h"
#import "SCTypes.h"

#define MARGIN 10
//#define ICON_SIZE 44
#define AUDIO_ICON_SIZE 44
#define VIDEO_ICON_SIZE 80
#define NEXT_WIDTH 15
#define NEXT_MARGIN 4
#define PHOTO_WIDTH [UIScreen mainScreen].bounds.size.width * .6
#define VIDEO_WIDTH [UIScreen mainScreen].bounds.size.width * .65
@class SCSettings,SCMessageBodys;


@interface SCMessageBodyItemView : SCViewWithProgress <UIScrollViewDelegate>
{
    CGFloat _maxWidht;
    SCMessageAlignment _bodyAlignment;
    
    CGFloat _itemWidth;
    CGFloat _itemHeight;

}

@property(nonatomic,assign) SCMessageAlignment bodyAlignment;


-(id) initWithSettings:(SCSettings*) settings;
//-(void) _init;

//-(void) setStatus:(SCNetFileStatus) status;
//-(void) setProgressPercent:(CGFloat) persents;

-(CGSize) viewSize;

@end