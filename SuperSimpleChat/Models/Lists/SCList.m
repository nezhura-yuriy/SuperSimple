//
//  SCModelList.m
//  SmartChat
//
//  Created by Yury Radchenko on 1/14/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#define defaultImageListName @"defaultListIco.png"
#define defaultImageListNameForWrite @"icoList.png"

#import "SCList.h"
#import "SCSettings.h"
#import "Localiser.h"
#import "SCContactItem.h"
#import "SCModelProfile.h"
#import "SCMessageBodyItem.h"
#import "SCPBManager.h"
#import "SCModelProfile.h"

#import "SCNotificationListKeys.h"

static const NSString* kItemsKey = @"itemsKey";
static const NSString* kItemsDirName = @"itemsDirectory";
static const NSString* kKeyPUUID = @"keyPUUID";
static const NSString *kKeyTrasportPUUID = @"keyTransportPUUID";
static const NSString *kKeySuccess = @"success";

@implementation SCList
{
    SCSettings *_settings;
    Localiser *_localizer;
    
    SCPBManager* _protoBufManager;
    NSFileManager *_fileManager;
    
    NSMutableDictionary *_tempItemDictionary;
    
    NSString *_fullNameWithPath;
    NSNotificationCenter *_notificationCenter;
}

//MARK: Life object
- (instancetype) init
{
    self = [super init];
    
    if (self) {
        [self _init];
    }
    return self;
}

- (void) _init {
    
    _settings = [SCSettings sharedSettings];
    _localizer = [Localiser sharedLocaliser];
    _protoBufManager = _settings.modelProfile.protoBufManager;
    _notificationCenter = [NSNotificationCenter defaultCenter];
    
    //Default values
    _puuid = [[SCPUUID alloc] init];
    _title = @"";
    _review = @"";
    _contentType = SCContentTypeText;
    _imageListSource = @"";
    _type = SCListTypePrivate;
    _changeTime = [NSDate date];
    _ownerPUUID = _settings.modelProfile.contactItem.contactPUUID;
        
    [_protoBufManager addDelegate:self forType:SCPBManagerParceTypeGetListItems];
    [_protoBufManager addDelegate:self forType:SCPBManagerParceTypeAddListItem];
    [_protoBufManager addDelegate:self forType:SCPBManagerParceTypeUpdateListItem];
    [_protoBufManager addDelegate:self forType:SCPBManagerParceTypeRemoveListItem];
    [_protoBufManager addDelegate:self forType:SCPBManagerParceTypeSetListItemsOrder];
    
    _items = [[NSMutableArray alloc] init];
    _tempItemDictionary = [[NSMutableDictionary alloc] init];
    
    _fileManager = [NSFileManager defaultManager];
}


- (instancetype) initAsFavorite
{
    self = [self init];
    
    if (self) {
        _puuid = _settings.modelProfile.contactItem.favoriteListPUUID;
    }
    
    return self;
}

- (instancetype) initFromDictionary:(NSDictionary*) dictionary
{
    self = [self init];
    
    _puuid = [[SCPUUID alloc] initFromString:dictionary[@"puuid"]];
    _title = dictionary[@"title"];
    _review = dictionary[@"review"];
    _imageListSource = dictionary[@"imageListSource"];
    _type = [dictionary[@"type"] intValue];
    _contentType = [dictionary[@"contentType"] intValue];
    _changeTime = dictionary[@"changeTime"];
    _ownerPUUID =  [[SCPUUID alloc] initFromString:dictionary[@"ownerPUUID"]];
    
    return self;
}

- (void) dealloc {
    
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeGetListItems];
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeAddListItem];
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeUpdateListItem];
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeRemoveListItem];
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeSetListItemsOrder];
}

//MARK: Converter
- (NSDictionary *) toDictionary {
    
    [self checkNilValues];
    
    return @{@"puuid": [_puuid scpuuidToString],
             @"title": _title,
             @"review": _review,
             @"imageListSource": _imageListSource,
             @"type": [NSNumber numberWithInt:_type],
             @"contentType": [NSNumber numberWithInt:_contentType],
             @"changeTime": _changeTime,
             @"ownerPUUID": [_ownerPUUID scpuuidToString]
             };
}

- (NSDictionary *) toDictionaryListItems
{
    NSMutableArray *itemsDictionary = [NSMutableArray new];
    for (SCListItem *item in _items) {
        [itemsDictionary addObject:[item toDictionary]];
    }
    return @{kItemsKey:itemsDictionary};
}

- (void) checkNilValues
{
    if (!_title) {
        _title = @"";
    }
    
    if (!_review) {
        _review = @"";
    }
    
    if (!_imageListSource) {
        _imageListSource = @"";
    }
}

//MARK: Work with Items
- (void) getListItemsFromServer {
    
    if (self) {
        SCPUUID *tempPUUID = [[SCPUUID alloc] initWithRandom];
        
        NSDictionary *dictionary = @{@"delegate": self,
                                     kKeyPUUID: tempPUUID,
                                     @"value": self};
        
        _tempItemDictionary[[tempPUUID scpuuidToString]] = self;
        [_protoBufManager getListItemsRequest:dictionary];
        
        [self sendItems:nil];
        
    } else {
        FTLog(@"Error: List must be not nil (SCModelList.m/getListItemsFromServer)");
    }
}


- (void) addItem:(SCListItem *) item {
    
    if (self)
    {
        NSDictionary *transportDic = @{@"list":self,
                                       @"item": item,
                                       @"delegate": self,
                                       kKeyTrasportPUUID:[[SCPUUID alloc] initWithRandom],
                                       };
        
        [_protoBufManager addItemListRequest:transportDic];
    }
    else
    {
        FTLog(@"Error: List must be not nil (SCModelList.m/addItem:)");
    }
}

- (void) updateItem: (SCListItem *) item
{
    NSDictionary *transportDic = @{@"list":self,
                                   @"item":item,
                                   @"delegate":self,
                                   kKeyTrasportPUUID:[[SCPUUID alloc] initWithRandom]
                                   };
    
    [_protoBufManager updateListItemRequest:transportDic];
}

- (void) removeItem: (SCListItem *) item
{
    NSDictionary *transportDic = @{@"list":self,
                                   @"item":item,
                                   @"delegate":self,
                                   kKeyTrasportPUUID:[[SCPUUID alloc] initWithRandom]
                                   };
    
    [_protoBufManager removeListItemRequest:transportDic];
}

- (void) setItemsOrder: (NSArray *) itemsPUUID
{
    NSDictionary *dictionary = @{@"delegate": self,
                                kKeyTrasportPUUID: [[SCPUUID alloc] initWithRandom],
                                @"list": self,
                                @"itemsPUUID": itemsPUUID};
    
    [_protoBufManager setListItemsOrderRequest:dictionary];
}

//MARK: CACHE ITEMS
- (void) sendItems: (NSMutableArray *) itemsArray
{
    if (_fullNameWithPath == nil) {
        _fullNameWithPath = [self fileNameListItemsWithPath:[_puuid scpuuidToString]];
    }
    
    BOOL fileExists = [_fileManager fileExistsAtPath:_fullNameWithPath];
    
    if (itemsArray == nil) { //The items of List ISN'T loaded from the server
        if (fileExists) {
            [self loadItemsFromFile:_fullNameWithPath];
            [_notificationCenter postNotificationName:kNotificationItemsUpdate object:nil];
        }
        
    } else { //The items of list is LOADED from server
        
        if (fileExists)
        {
            NSDictionary *dictArrayPreviouslySavedItems = [NSDictionary dictionaryWithContentsOfFile:_fullNameWithPath];
            
            NSArray *arrayDictionaryPreviouslySavedItems = dictArrayPreviouslySavedItems[kItemsKey];
            
            NSMutableArray *itemsFromFile = [[NSMutableArray alloc] init];
            
            for (NSDictionary *itemAsDict in arrayDictionaryPreviouslySavedItems)
            {
                SCListItem *item =[[SCListItem alloc] initFromDictionary:itemAsDict];
                [itemsFromFile addObject: item];
            }
            
            if ([itemsArray respondsToSelector:@selector(isEqualToListItemsArray:)])
            {
                if (![itemsArray isEqualToListItemsArray:itemsFromFile])
                {
                    [self saveToFileItemsFromArray:itemsArray];
                    [_notificationCenter postNotificationName:kNotificationItemsUpdate object:nil];
                }
            }
        }
        else
        {
            [self saveToFileItemsFromArray:itemsArray];
            [_notificationCenter postNotificationName:kNotificationItemsUpdate object:nil];
        }
    }
}

//LOAD
- (void) loadItemsFromFile: (NSString *) fileFullNameWithPath
{
    NSDictionary *dicOfArrayOfItemsDic = [NSDictionary dictionaryWithContentsOfFile:fileFullNameWithPath];
    
    [_items removeAllObjects];
    _items = [self itemsFromArrayDictionaries:dicOfArrayOfItemsDic[kItemsKey]];
}

- (NSMutableArray *) itemsFromArrayDictionaries: (NSArray *) arrayDict
{
    NSMutableArray *itemsArray = [NSMutableArray new];
    
    for (NSDictionary *itemAsDict in arrayDict) {
        SCListItem *item = [[SCListItem alloc] initFromDictionary:itemAsDict];
        [itemsArray addObject:item];
    }
    
    return itemsArray;
}

//SAVE
- (void) saveToFileItemsFromArray: (NSArray *) itemsArray
{
    [_items removeAllObjects];
    for (SCListItem *item in itemsArray) {
        [_items addObject:item];
    }
    [self writeToFileAllListItems];
}

//MARK: File Manager
- (NSString *) fileNameListItemsWithPath: (NSString *) fileNameOnly {
    
    NSString *fileName = [[NSString alloc] initWithFormat:@"%@.plist", fileNameOnly];
    NSString *itemsFilesDirectoryPath = [[NSString alloc] initWithFormat:@"%@/%@", _settings.cacheListsPath, kItemsDirName];
    
    BOOL isDir;
    if (![_fileManager fileExistsAtPath:itemsFilesDirectoryPath isDirectory:&isDir]) {
        [self createDirectoryWithPath:itemsFilesDirectoryPath];
    }
    
    return [itemsFilesDirectoryPath stringByAppendingPathComponent:fileName];
}

- (void) createDirectoryWithPath: (NSString *) pathDirName {
    
    NSError *error = nil;
    BOOL success = [_fileManager createDirectoryAtPath:pathDirName withIntermediateDirectories:YES attributes:nil error:&error];
    
    if(!success) {
        FTLog(@"Failed to create directory \'%@\'. Error = %@", pathDirName, error);
    }
}

- (void) writeToFileAllListItems
{
    NSDictionary *itemsDictionary = [self toDictionaryListItems];
    BOOL success = [itemsDictionary writeToFile:_fullNameWithPath atomically:YES];
    
    if (!success) {
        FTLog(@"List ITEMS ISN'T saved to file %@", _fullNameWithPath);
    }
}

//MARK: Method's SCPBManagerDelegate protocol
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    switch (type)
    {
        case SCPBManagerParceTypeGetList:
        {
            if ([data isKindOfClass:[SCList class]]) {
                
                SCList *newList = data;
                [self equateList:newList];
                [_notificationCenter postNotificationName:kNotificationListUpdate object:nil];
            }
        }
            break;
        
        case SCPBManagerParceTypeGetListItems:
        {
            if ([data isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dictionaryItems = data;
                _items = nil;
                _items = dictionaryItems[@"items"];
                
                [self writeToFileAllListItems];
                [_notificationCenter postNotificationName:kNotificationItemsUpdate object:nil];
            }
        }
            break;
        
        case SCPBManagerParceTypeAddListItem:
        {
            if ([data isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *transportDic = data;
                
                if ([[transportDic allKeys] containsObject:kKeySuccess])
                {
                    BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
                    
                    if (isSuccess)
                    {
                        if ([[transportDic allKeys] containsObject:@"item"] &&
                            [[transportDic allKeys] containsObject:@"list"] && //for future
                            [[transportDic allKeys] containsObject:@"newItemPUUID"])
                        {
                            SCListItem *itemAdd = transportDic[@"item"];
                            SCPUUID *itemOldPUUID = itemAdd.puuid;
                            
                            SCPUUID *itemAddPUUID = transportDic[@"newItemPUUID"];
                            itemAdd.puuid = itemAddPUUID;
                            
                            [_items insertObject:itemAdd atIndex:0];
                            [self writeToFileAllListItems];
                            
                            NSDictionary *itemAddDictionary = @{kKeySuccess: [NSNumber numberWithBool:isSuccess],
                                                                 @"item": itemAdd,
                                                                 @"itemOldPUUID": itemOldPUUID,
                                                                };
                            
                            [_notificationCenter postNotificationName:kNotificationItemAdded object:itemAddDictionary];
                            
                        }
                        else
                        {
                            FTLog(@"Error: Havn't key 'item' or 'lis' or 'newItemPUUID' ");
                        }
                    }
                    else
                    {
                        if ([[transportDic allKeys] containsObject:@"item"] &&
                            [[transportDic allKeys] containsObject:@"list"]) //for future
                        {
                            SCListItem *itemAdd = transportDic[@"item"];
                            
                            NSDictionary *itemAddDictionary = @{kKeySuccess: [NSNumber numberWithBool:isSuccess],
                                                                @"item": itemAdd,
                                                                };
                            
                            [_notificationCenter postNotificationName:kNotificationItemAdded object:itemAddDictionary];
                        }
                        else
                        {
                             FTLog(@"Error: Havn't key 'item' or 'lis' or 'newItemPUUID' ");
                        }
                    }
                }
                else
                {
                    FTLog(@"transportDic havn't key '%@'", kKeySuccess);
                }
            }
            else
            {
                FTLog(@"Error: data isn't dictionary in SCPBManagerParceTypeAddListItem parse");
            }
        }
            break;
        
        case SCPBManagerParceTypeUpdateListItem:
        {
            if ([data isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *transportDic = data;
                if ([[transportDic allKeys] containsObject:kKeySuccess])
                {
                    BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
                    
                    NSMutableDictionary *itemUpdateDic = [[NSMutableDictionary alloc] init];
                    itemUpdateDic[kKeySuccess] = [NSNumber numberWithBool:isSuccess];
                    
                    if (isSuccess)
                    {
                        SCListItem *itemUpdated = transportDic[@"item"];
                        
                        for (SCListItem *item in _items) {
                            if ([itemUpdated.puuid isEqual:item.puuid]) {
                                [item equateItem:itemUpdated];
                                [self writeToFileAllListItems];
                                break;
                            }
                        }
                    }
                    
                    [_notificationCenter postNotificationName:kNotificationItemUpdated object:itemUpdateDic];
                }
                else
                {
                    FTLog(@"Error: data isn't dictionary in SCPBManagerParceTypeUpdateListItem parse");
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeRemoveListItem:
        {
            if ([data isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *transportDic = data;
                if ([[transportDic allKeys] containsObject:kKeySuccess])
                {
                    BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
                    
                    NSMutableDictionary *itemRemoveDic = [[NSMutableDictionary alloc] init];
                    itemRemoveDic[kKeySuccess] = [NSNumber numberWithBool:isSuccess];
                    
                    if (isSuccess)
                    {
                        if (([[transportDic allKeys] containsObject:@"list"]) &&
                            ([[transportDic allKeys] containsObject:@"item"])) //for future
                        {
                            SCListItem *itemDeleted = transportDic[@"item"];
                            
                            for (SCListItem *item in _items) {
                                if ([itemDeleted.puuid isEqual:item.puuid]) {
                                    [_items removeObject:item];
                                    [self writeToFileAllListItems];
                                    break;
                                }
                            }
                        }
                    }
                    [_notificationCenter postNotificationName:kNotificationItemRemoved object:itemRemoveDic];
                }
            }
            else
            {
                FTLog(@"Error: data isn't dictionary in SCPBManagerParceTypeRemoveListItem parse");
            }
        }
            break;
            
        case SCPBManagerParceTypeSetListItemsOrder:
        {
            if ([data isKindOfClass:[NSDictionary class]]) {
                NSDictionary *transportDic = data;
                
                if ([[transportDic allKeys] containsObject:kKeySuccess]) {
                    
                    BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
                    
                    if (isSuccess)
                    {
                        NSArray *listsPUUID = transportDic[@"itemsPUUID"];
                        [self sortItemsAs:listsPUUID];
                        [self writeToFileAllListItems];
                    }
                    
                    [_notificationCenter postNotificationName:kNotificationItemRemoved object:transportDic];
                }
            }
        }
            break;
        
        default:
        {
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
        }
            break;
    }
}

- (SCListItem *) getListItemByPUUID: (SCPUUID*) itemPUUID
{
    for (NSInteger i = 0; i < _items.count; i++) {
        SCListItem *item = _items[i];
        
        if ([item.puuid isEqual:itemPUUID]) {
            return item;
        }
    }
    return nil;
}

- (void) sortItemsAs:(NSArray *) itemsPUUID
{
    NSMutableArray *newItemsOrder = [NSMutableArray new];
    
    for (NSInteger i = 0; i < itemsPUUID.count; i++)
    {
        SCPUUID *itemPUUID = itemsPUUID[i];
        SCListItem *item = [self getListItemByPUUID:itemPUUID];
        
        if (item) {
            [newItemsOrder addObject:item];
        } else {
            FTLog(@"Items with PUUID=%@ is absent", itemPUUID);
        }
    }
    _items = [newItemsOrder mutableCopy];
}

- (BOOL) isEqualList:(SCList *) list
{
    
    if (![_changeTime isEqualToDate:list.changeTime]) {
        return NO;
    }
    
    if (![_puuid isEqual:list.puuid]) {
        return NO;
    }
    
    if (![_title isEqualToString:list.title]) {
        return NO;
    }
    
    if (![_review isEqualToString:list.review]) {
        return NO;
    }
    
    if (![_imageListSource isEqualToString:list.imageListSource]) {
        return NO;
    }
    
    if (![_ownerPUUID isEqual:list.ownerPUUID]) {
        return NO;
    }
    
    if (_type != list.type) {
        return NO;
    }
    
    if (_contentType != list.contentType) {
        return NO;
    }
    
    return YES;
}

- (void) equateList: (SCList *) list
{
    //think, _puuid not change
    _title = list.title;
    _review = list.review;
    
    _imageListSource = list.imageListSource;
    _ownerPUUID = list.puuid;
    _changeTime = list.changeTime;
    
    _type = list.type;
    _contentType = list.contentType;
}

//MARK: Description
-(NSString*) description
{
    return [NSString stringWithFormat:@"puuid = %@, title = %@", _puuid, _title];
}

@end
