//
//  SCMessageBodyPhotoView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyPhotoItemView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"
#import "SCNetFile.h"


@implementation SCMessageBodyPhotoItemView
{
    UIImageView* _photoView;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super initWithSettings:settings];
    if (self)
    {
        _settings = settings;
        
        [self _init];
    }
    return self;
}

-(void) _init
{
//    [super _init];
    
    _photoView = [[UIImageView alloc] init];
    _photoView.frame = CGRectMake(MARGIN, 0, PHOTO_WIDTH, PHOTO_WIDTH);
    
    _activityIndicatorView.frame = CGRectMake(MARGIN, 0, PHOTO_WIDTH, PHOTO_WIDTH);

    [self addSubview:_photoView];
    [self addSubview:_activityIndicatorView];
    [self.layer addSublayer:shadowLayer];
    [self.layer addSublayer:shapeLayer];
}

-(void) dealloc
{
    FTLog(@"%@ : dealloc",NSStringFromClass([self class]));
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
}
-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        _activityIndicatorView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        _photoView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    }
}

-(void) setPhotoNetFile:(SCNetFile *)photoNetFile
{
//    [_activityIndicator View startAnimating];
    _photoNetFile = photoNetFile;
//    [_photoNetFile getPhoto:self];
    [_photoNetFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                [self setStatus:_photoNetFile.status];
                break;
                
            case SCNetFileComletionTypeCompleet:
            {
                [self setStatus:_photoNetFile.status];
                if([result isKindOfClass:[SCNetFile class]])
                {
                    _photoView.image = [(SCNetFile*)result imageForSize:CGSizeMake(PHOTO_WIDTH, PHOTO_WIDTH)];
                }
            }
                break;
                
            case SCNetFileComletionTypeProgress:
                [self setProgressPercent:[result doubleValue]];
                break;
                
            case SCNetFileComletionTypeError:
                FTLog(@"J,hf,jnfnm");
                break;
                
            default:
                FTLog(@"default case");
                break;
        }
    }];

}

-(void) loadPhotoFile:(NSString*) photoFile
{
//    [_activityIndicator View stopAnimating];
    [_photoView setImage:[UIImage imageWithContentsOfFile:photoFile]];
}

-(void) setBodyAlignment:(SCMessageAlignment)bodyAlignment
{
    _bodyAlignment = bodyAlignment;
    //    TODO:setBodyAlignment
    [self updateUI];
}

-(void) updateUI
{
    switch (_bodyAlignment)
    {
        case SCMessageAlignmentLeft:
            _activityIndicatorView.color = SKCOLOR_ChatMessageIncomingText;
            break;
            
        case SCMessageAlignmentRight:
            _activityIndicatorView.color = SKCOLOR_ChatMessageOutgoingText;
            break;
            
        default:
            FTLog(@"КАПЕЦ, от сервера пришел фото");
            break;
    }
}

#pragma mark -
-(CGSize) viewSize
{
    return CGSizeMake(PHOTO_WIDTH + 2*MARGIN, PHOTO_WIDTH);
}

@end


