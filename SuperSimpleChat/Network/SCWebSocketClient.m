//
//  SCWebSocketClient.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 06.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCWebSocketClient.h"
//#import "SmartChat.pb.h"
#import "SCSettings.h"
#import "SCMessageItem.h"
//#import "SCPBManagerLocal.h"

@implementation SCWebSocketClient
{
    SRWebSocket *webSocket;
    NSMutableArray* _delegates;
//    SCPBManager* _pbManager;
}

- (id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if(self)
    {
        _settings = settings;
        _delegates = [[NSMutableArray alloc] init];
        _userID = @"";
//        _pbManager = [[SCPBManagerLocal alloc] init];
    }
    return self;
    
}


- (void) dealloc {
    
    FTLog(@"Dealloc");
}


#pragma mark Delegates
-(void) addDelegate:(id<SCChatItemDelegate>) delegate
{
    if(![_delegates containsObject:delegate])
    {
        [_delegates addObject:delegate];
    }
}

-(void) delDelegate:(id<SCChatItemDelegate>) delegate
{
    if([_delegates containsObject:delegate])
    {
        [_delegates removeObject:delegate];
    }
}



#pragma mark -
- (void)connectWebSocket
{
    NSString *urlString = [NSString stringWithFormat:@"%@?%@=%@",_settings.modelProfile.ws_link,TOKEN_PARAM_NAME, _settings.modelProfile.sessionID];
    FTLog(@"urlString = %@",urlString);
    if(webSocket)
    {
        [webSocket close];
        webSocket = nil;
    }
    
    webSocket= [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:urlString]];

    webSocket.delegate =self;
    [webSocket open];
}

- (void) disconnectWebSocket
{
    [webSocket close];
}


- (void) sendData:(NSData*) data
{
    [webSocket send:data];
}


#pragma mark - SRWebSocket delegate

- (void) webSocketDidOpen:(SRWebSocket *)newWebSocket {
    
    FTLog(@"webSocketDidOpen");
    
    if(![webSocket isEqual:newWebSocket])
    {
        webSocket = newWebSocket;
        webSocket.delegate=self;
    }
    for(id<SCWebSocketClientDelegate> delegate in _delegates)
    {
        if([delegate respondsToSelector:@selector(clientSocketDidOpen)])
        {
            [delegate clientSocketDidOpen];
        }
    }
    _isConnected = YES;
}

- (void) webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    FTLog(@"didFailWithError %@",error.description);
//    [[[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    for(id<SCWebSocketClientDelegate> delegate in _delegates)
    {
        if([delegate respondsToSelector:@selector(clientSocketDidFailWithError:)])
        {
            [delegate clientSocketDidFailWithError:error];
        }
    }

    _isConnected = NO;
}

- (void) webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
     FTLog(@"didCloseWithCode");
//    NSError* error = [[NSError alloc] init];
//    error.code = code;
    
    for(id<SCWebSocketClientDelegate> delegate in _delegates)
    {
        if([delegate respondsToSelector:@selector(clientSocketDidCloseWithCode:)])
        {
            [delegate clientSocketDidCloseWithCode:nil];
        }
    }
    _isConnected = NO;
}

- (void) webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
//    FTLog(@"SCWebSocketClient : didReceiveMessage");
    
    for(id<SCWebSocketClientDelegate> delegate in _delegates)
    {
        if([delegate respondsToSelector:@selector(clientSocketDidReceiveData:)])
        {
            [delegate clientSocketDidReceiveData:message];
        }
    }
    
}


@end
