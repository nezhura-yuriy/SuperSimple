//
//  SCMediaItem.m
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 06.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMediaItemView.h"
#import "SCSettings.h"
#import "SCMessageBodyItem.h"

@implementation SCMediaItemView
{
    SCSettings* _settings;
    UIImageView* _image;
    
    AVPlayer* _videoPlayer;
    AVPlayerLayer *_videoPlayerLayer;
    AVPlayerItem *_videoPlayerItem;
    NSString* _videoFileCached;

}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if(self)
    {
        _settings = settings;
//        self.backgroundColor = [UIColor redColor];
        self.clipsToBounds = YES;
        
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [self addGestureRecognizer:tapGesture];
        
        UISwipeGestureRecognizer* swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
        [swipeGesture setDirection:UISwipeGestureRecognizerDirectionUp];
        [self addGestureRecognizer:swipeGesture];
        
        _image = [[UIImageView alloc] init];
        _image.clipsToBounds = YES;
        [self addSubview:_image];
        
    }
    return self;
}


-(void) dealloc
{
    FTLog(@"");
    _videoPlayer = nil;
    _videoPlayerLayer = nil;
    _videoPlayerItem = nil;
    _videoFileCached = nil;
    _image = nil;
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    switch (_mediaItem.type)
    {
        case SCMediaItemTypeButton:
        case SCMediaItemTypeAudio:
            _image.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
            break;
        case SCMediaItemTypeVideo:
        {
            if(_videoPlayerLayer!= nil)
                _videoPlayerLayer.frame = self.layer.bounds;
        }
            break;
            
        case SCMediaItemTypeImage:
            _image.frame = self.bounds;
            break;
            
        default:
            _image.frame = self.bounds;
            break;
    }
}

-(void) setMediaItem:(SCMediaItem *)mediaItem
{
    _mediaItem = mediaItem;
    switch (_mediaItem.type)
    {
        case SCMediaItemTypeButton:
        {
            if([_mediaItem.mediaPath isEqualToString:AddFromCameraPhoto])
            {
                _image.image = [_settings.skinManager getImageForKeyX:@"chat-add-message-photo_plus"];
                _image.frame = CGRectMake(0, 0, 34, 25);

            }
            else if([_mediaItem.mediaPath isEqualToString:AddFromCameraVideo])
            {
                _image.image = [_settings.skinManager getImageForKeyX:@"chat-add-message-video_plus"];
                _image.frame = CGRectMake(0, 0, 34, 25);
            }
            else if([_mediaItem.mediaPath isEqualToString:AddFromLibraryPhoto])
            {
                _image.image = [_settings.skinManager getImageForKeyX:@"chat-add-message-library-icon"];
                _image.frame = CGRectMake(0, 0, 24, 24);
            }
            else if([_mediaItem.mediaPath isEqualToString:AddFromLibraryVideo])
            {
                _image.image = [_settings.skinManager getImageForKeyX:@"chat-add-message-library-icon"];
                _image.frame = CGRectMake(0, 0, 24, 24);
            }
            else if([_mediaItem.mediaPath isEqualToString:AddFromMicrophoneAudio])
            {
                _image.image = [_settings.skinManager getImageForKeyX:@"chat-add-message-voice_plus"];
                _image.frame = CGRectMake(0, 0, 19, 25);
            }
        }
            break;
            
        case SCMediaItemTypeImage:
        {
            switch (_mediaItem.messageBody.type)
            {
                case SCDataTypePhoto:
                {
                    FTLog(@"");
                }
                    break;

                case SCDataTypePhotoData:
                {
                    _image.image = [[UIImage alloc] initWithData:(NSData*)_mediaItem.messageBody.data];
                    // записать локальный файл
                }
                    break;
                    
                case SCDataTypePhotoLink:
                {
                    FTLog(@"");
                }
                    break;
                    
                case SCDataTypePhotoFile:
                {
                    _image.image = [[UIImage alloc] initWithContentsOfFile:_mediaItem.messageBody.netFile.localPath];
                }
                    break;
                    
                default:break;
            }
            _image.layer.borderWidth = 1;
            _image.layer.borderColor = [[UIColor darkGrayColor] CGColor];
            _image.layer.cornerRadius = 5;

        }
            
        case SCMediaItemTypeVideo:
        {
            switch (_mediaItem.messageBody.type)
            {
                case SCDataTypeVideo:
                {
                    
                }
                    break;
                    
                case SCDataTypeVideoFile:
                {
                    /*
                    _image.image = [_settings.skinManager getImageForKey:@"chatBtnCamera_white"];
                    _image.frame = CGRectMake(0, 0, 32, 32);
                    */
//                    NSURL *_theMovieURL = [NSURL fileURLWithPath:(NSString*)_mediaItem.messageBody.data];
                    NSURL *_theMovieURL = [NSURL fileURLWithPath:_mediaItem.messageBody.netFile.localPath];
                    _videoPlayerItem = [AVPlayerItem playerItemWithURL:_theMovieURL];
                    
                    _videoPlayer = [AVPlayer playerWithPlayerItem:_videoPlayerItem];
                    _videoPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:_videoPlayer];
                    [self.layer insertSublayer:_videoPlayerLayer below:_image.layer];
                    [_videoPlayer seekToTime:kCMTimeZero];
                    
                    if(_videoPlayer.status == AVPlayerStatusReadyToPlay)
                    {
                        [_videoPlayer play];
                    }
                    else if(_videoPlayer.status == AVPlayerStatusFailed)
                    {
                        FTLog(@"_videoPlayer Error");
                    }

                }
                    break;
                case SCDataTypeVideoLink:
                {
                    
                }
                    break;
                default:break;
            }
            _image.layer.borderWidth = 1;
            _image.layer.borderColor = [[UIColor darkGrayColor] CGColor];
            _image.layer.cornerRadius = 5;

        }
            break;
            
        case SCMediaItemTypeAudio:
        {
            switch (_mediaItem.messageBody.type)
            {
                case SCDataTypeAudio:
                case SCDataTypeAudioFile:
                case SCDataTypeAudioLink:
                {
                    _image.image = [_settings.skinManager getImageForKeyX:@"chat-add-message-voice"];
                    _image.frame = CGRectMake(0, 0, 32, 32);
                }break;
                default:break;
            }
            _image.layer.borderWidth = 1;
            _image.layer.borderColor = [[UIColor darkGrayColor] CGColor];
            _image.layer.cornerRadius = 5;

        }
            break;
            
        default:break;
    }
    
    
}

-(void) tapGestureAction:(UIGestureRecognizer *) gesture
{
    FTLog(@"tapGestureAction");
    switch (_mediaItem.type)
    {
        case SCMediaItemTypeButton:
        case SCMediaItemTypeImage:
        case SCMediaItemTypeAudio:
        {
            if(_delegate && [_delegate respondsToSelector:@selector(itemTap:)])
            {
                [_delegate itemTap:_mediaItem];
            }
        }
            break;

        case SCMediaItemTypeVideo:
        {
            if(_videoPlayer)
            {
                [_videoPlayer seekToTime:kCMTimeZero];
                [_videoPlayer play];
            }
            
            /*
            if(_delegate && [_delegate respondsToSelector:@selector(itemTap:)])
            {
                [_delegate itemTap:_mediaItem];
            }
            */
        }
            break;
            
        default:
            break;
    }
}

-(void) swipeGestureAction:(UISwipeGestureRecognizer *) gesture
{
    FTLog(@"swipeGestureAction");
    switch (_mediaItem.type)
    {
        case SCMediaItemTypeImage:
        case SCMediaItemTypeAudio:
        case SCMediaItemTypeVideo:
        {
            if(_delegate && [_delegate respondsToSelector:@selector(itemSwipe:)])
            {
                [_delegate itemSwipe:_mediaItem];
            }
        }
            break;
            
        default:break;
    }
}
@end
