//
//  SCListViewController.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCLTableView.h"
#import "SCLTableViewCellDelegate.h"
#import "SCLPullToAddNewBehaviour.h"
#import "SCSettings.h"

@interface SCListViewController : UIViewController <SCLTableViewCellDelegate, SCLTableViewDataSource>

@property (weak, nonatomic) IBOutlet SCLTableView *tableView;
@property (strong, nonatomic) SCSettings *settings;

@end
