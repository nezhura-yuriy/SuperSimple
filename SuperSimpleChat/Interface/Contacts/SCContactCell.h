//
//  SCContactCell.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 13.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCContactItem.h"
#import "SWTableViewCell.h"
#import "SCImageViewWithIndicator.h"


@protocol SCContactCellDelegate <NSObject>

-(void) creatChatWithContactAtIndexPath:(NSIndexPath*) indexPath;

@end


@interface SCContactCell : UITableViewCell <SCContactItemDelegate>

@property (nonatomic, weak) id < SCContactCellDelegate > delegate;

@property (weak, nonatomic) IBOutlet SCImageViewWithIndicator *userFoto;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusViewOutlet;
@property (strong, nonatomic) SCContactItem* contact;
@property (weak, nonatomic) IBOutlet UIButton *creatChatOutlet;
@property (weak, nonatomic) IBOutlet UILabel *nikNameLabel;

@property (strong, nonatomic) NSIndexPath* indexPath;

-(void)fillWhithContact:(SCContactItem*)contact;

- (IBAction)creatChatAction:(id)sender;



@end
