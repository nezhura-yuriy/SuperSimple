//
//  SCMessageBodySoundView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 12.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodySoundItemView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"
#import "SCNetFile.h"
#import "SCMediaUtils.h"

#define WAVEFORM_WIDTH 100

@implementation SCMessageBodySoundItemView
{
    NSString* _audioFile;
    AVAudioPlayer* _audioPlayer;
    
    UIButton* _btnPlay;
    UIImageView* _waveForm;
    UILabel* _labTime;
    
    NSString* _namePlayImage;
    NSString* _nameStopImage;
    NSString* _nameWaitImage;
    NSString* _nameWaveFormImage;
    
    NSTimer* _playbackTimer;
}

static NSString *someObservationContext = @"something";

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super initWithSettings:settings];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
//    [super _init];
//    self.layer.borderWidth = 1;
//    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    self.layer.cornerRadius = 5;
    _namePlayImage = @"chat-play";
    _nameStopImage = @"chat-record-stop";
    _nameWaitImage = @"chat-add-message-voice";
    _nameWaveFormImage = @"chat-waveform";
    
    _btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnPlay.frame = CGRectMake(0, 0, AUDIO_ICON_SIZE, AUDIO_ICON_SIZE);
    [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_nameWaitImage] forState:UIControlStateNormal];
    [_btnPlay addTarget:self action:@selector(_playerPrepare) forControlEvents:UIControlEventTouchUpInside];
    
    _activityIndicatorView.frame = CGRectMake(0, 0, AUDIO_ICON_SIZE, AUDIO_ICON_SIZE);
    
    _waveForm = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WAVEFORM_WIDTH, AUDIO_ICON_SIZE - 2*MARGIN)];
    _waveForm.image = [_settings.skinManager getImageForKeyX:_nameWaveFormImage];
    
    _labTime = [[UILabel alloc] init];
    _labTime.textAlignment = NSTextAlignmentRight;
    _labTime.textColor = [UIColor blackColor];
    _labTime.text = @"0:00";

    [self addSubview:_btnPlay];
    [self addSubview:_activityIndicatorView];
    [self addSubview:_waveForm];
    [self addSubview:_labTime];
    [self.layer addSublayer:shadowLayer];
    [self.layer addSublayer:shapeLayer];
    
//    _btnPlay.layer.borderWidth = 1;
//    _btnPlay.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    _waveForm.layer.borderWidth = 1;
//    _waveForm.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    _labTime.layer.borderWidth = 1;
//    _labTime.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
}

-(void) dealloc
{
    FTLog(@"%@ : dealloc",NSStringFromClass([self class]));
    if(_audioPlayer)
    {
//        [_audioPlayer removeObserver:self forKeyPath:@"currentTime"];
        [_audioPlayer stop];
    }
    _audioPlayer = nil;
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        CGFloat posX = 0;
        _btnPlay.frame = CGRectMake(posX, (frame.size.height - AUDIO_ICON_SIZE)/2, AUDIO_ICON_SIZE, AUDIO_ICON_SIZE);
        shapeLayer.frame = _btnPlay.frame;
        shadowLayer.frame = _btnPlay.frame;
        _activityIndicatorView.frame = CGRectMake(posX, (frame.size.height - AUDIO_ICON_SIZE)/2, AUDIO_ICON_SIZE, AUDIO_ICON_SIZE);
        posX += AUDIO_ICON_SIZE;
        _waveForm.frame = CGRectMake(posX, MARGIN, WAVEFORM_WIDTH, AUDIO_ICON_SIZE - 2*MARGIN);
        posX += WAVEFORM_WIDTH;
        _labTime.frame = CGRectMake(posX, (frame.size.height - AUDIO_ICON_SIZE)/2, AUDIO_ICON_SIZE, AUDIO_ICON_SIZE);
    }
}
-(void) layoutSubviews
{
    [super layoutSubviews];    
}

-(void) setAudioNetFile:(SCNetFile *)audioNetFile
{
    _audioNetFile = audioNetFile;
//    [_audioNetFile getAudio:self];
    [_audioNetFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                [self setStatus:_audioNetFile.status];
                break;
                
            case SCNetFileComletionTypeCompleet:
            {
                if([result isKindOfClass:[SCNetFile class]])
                {
                    _audioFile = _audioNetFile.localPath;
                    [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_namePlayImage] forState:UIControlStateNormal];
                    NSError *playerError;
                    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:_audioFile] error:&playerError];
                    _audioPlayer.delegate = self;
                    if(!playerError)
                    {
                        _labTime.text = [SCMediaUtils formatSecond:_audioPlayer.duration];
                    }
                }
            }
                break;
                
            case SCNetFileComletionTypeProgress:
                [self setProgressPercent:[result doubleValue]];
                break;
                
            case SCNetFileComletionTypeError:
                break;
                
            default:
                FTLog(@"default case");
                break;
        }

    }];
    
}

-(void) loadAudioFile:(NSString*) audioFile
{
    _audioFile = audioFile;
    [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_namePlayImage] forState:UIControlStateNormal];
    NSError *playerError;
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:_audioFile] error:&playerError];
    _audioPlayer.delegate = self;
    if(!playerError)
    {
        _labTime.text = [SCMediaUtils formatSecond:_audioPlayer.duration];
    }
}

-(void) setBodyAlignment:(SCMessageAlignment)bodyAlignment
{
    _bodyAlignment = bodyAlignment;
    [self updateUI];
}

-(void) updateUI
{
    switch (_bodyAlignment)
    {
        case SCMessageAlignmentLeft:
            _labTime.textColor = SKCOLOR_ChatMessageIncomingText;
            _namePlayImage = @"chat-play";
            _nameStopImage = @"chat-record-stop";
            _nameWaitImage = @"chat-add-message-voice";
            _nameWaveFormImage = @"chat-waveform";
            _activityIndicatorView.color = SKCOLOR_ChatMessageIncomingText;
            break;
            
        case SCMessageAlignmentRight:
            _labTime.textColor = SKCOLOR_ChatMessageOutgoingText;
            _namePlayImage = @"chat-play";
            _nameStopImage = @"chat-record-stop";
            _nameWaitImage = @"chat-add-message-voice";
            _nameWaveFormImage = @"chat-waveform";
            _activityIndicatorView.color = SKCOLOR_ChatMessageOutgoingText;
            break;
            
        default:
            FTLog(@"КАПЕЦ, от сервера пришел звук");
            break;
    }
    _waveForm.image = [_settings.skinManager getImageForKeyX:_nameWaveFormImage];
    if(!_audioFile)
        [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_nameWaitImage] forState:UIControlStateNormal];
    else if(_audioPlayer && _audioPlayer.isPlaying)
        [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_nameStopImage] forState:UIControlStateNormal];
    else
        [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_namePlayImage] forState:UIControlStateNormal];
}


-(void) _playerPrepare
{
    if(_audioPlayer)
    {
        [self _playerAction];
    }
    else
    {
        if(_audioFile)
        {
            NSError *playerError;
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:_audioFile] error:&playerError];
            _audioPlayer.delegate = self;
            if(!playerError)
            {
                [self _playerAction];
            }
            else
            {
                FTLog(@"Error");
            }
        }
    }
}

-(void) _playerAction
{
    if(!_audioPlayer.isPlaying)
    {
        [_audioPlayer play];
        [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_nameStopImage] forState:UIControlStateNormal];
        _labTime.text = @"0:00";
        _playbackTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(updateTime)
                                                        userInfo:nil
                                                         repeats:YES];
    }
    else
    {
        [_audioPlayer stop];
        [self atStop];
    }
}

-(void)updateTime
{
    _labTime.text = [SCMediaUtils formatSecond:_audioPlayer.currentTime];
}

-(void) atStop
{
    [_btnPlay setImage:[_settings.skinManager getImageForKeyX:_namePlayImage] forState:UIControlStateNormal];
    if(_playbackTimer)
    {
        [_playbackTimer invalidate];
        _playbackTimer = nil;
    }
    _labTime.text = [SCMediaUtils formatSecond:_audioPlayer.duration];
}

-(CGSize) viewSize
{
    return CGSizeMake(AUDIO_ICON_SIZE + WAVEFORM_WIDTH + AUDIO_ICON_SIZE + MARGIN, AUDIO_ICON_SIZE);
}

#pragma mark AVAudioPlayerDelegate <NSObject>
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self atStop];
}

/* if an error occurs while decoding it will be reported to the delegate. */
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    
}


@end


