//
//  SCChatTopMenuView.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 09.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatTopMenuView.h"

@implementation SCChatTopMenuView

{
    void (^afterSelectTop) (SCChatTopMenuViewItem item);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
//        [self setup];
    }
    return self;
}


-(id) init
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}

-(id) initWithCompletionblock:(void (^)(SCChatTopMenuViewItem selectTopItem))completionBlock
{
    self = [super init];
    if(self)
    {
        afterSelectTop = [completionBlock copy];
        [self _init];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self _init];
}


-(void) _init
{
    self.clipsToBounds = YES;
//    self.layer.borderWidth = 1;
//    self.layer.borderColor = [[UIColor colorWithRed:164.0/255.0 green:205.0/255.0 blue:255.0/255.0 alpha:0.85] CGColor];
//    self.backgroundColor = [UIColor colorWithRed:0.7176 green:0.8235 blue:0.8784 alpha:0.85];
    
    _btnPhoto.tag = 1;
    [_btnPhoto addTarget:self action:@selector(btnsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnAudio.tag = 2;
    [_btnAudio addTarget:self action:@selector(btnsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnVideo.tag = 3;
    [_btnVideo addTarget:self action:@selector(btnsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnUser.tag = 4;
    [_btnUser addTarget:self action:@selector(btnsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnDestroy.tag = 5;
    [_btnDestroy addTarget:self action:@selector(btnsAction:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void) dealloc
{
    
}

-(void) setAfterSelect:(void(^)(SCChatTopMenuViewItem selectTopItem)) completionBlock
{
    afterSelectTop = [completionBlock copy];
}


-(void) layoutSubviews
{
    [super layoutSubviews];
}


-(void) btnsAction:(id) sender
{
    if(afterSelectTop && [afterSelectTop respondsToSelector:@selector(selectTopItem:)])
    {
        switch (((UIButton*)sender).tag)
        {
            case 1:
                [afterSelectTop selectTopItem:SCChatTopMenuViewItemPhoto];
                break;
                
            case 2:
                [afterSelectTop selectTopItem:SCChatTopMenuViewItemAudio];
                break;
                
            case 3:
                [afterSelectTop selectTopItem:SCChatTopMenuViewItemVideo];
                break;
                
            case 4:
                [afterSelectTop selectTopItem:SCChatTopMenuViewItemUser];
                break;
                
            case 5:
                [afterSelectTop selectTopItem:SCChatTopMenuViewItemDestroy];
                break;
                
                
            default:
                break;
        }
        
    }
}

+(CGSize) sizeView
{
    return CGSizeMake(320, 45);
}

@end
