//
//  AppDelegate.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 06.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "AppDelegate.h"
#import "SCMainNavController.h"
#import "SCSettings.h"
#import "SCNotifycationManager.h"
#import <FBSDKCoreKit.h>
#import <objc/runtime.h>

@interface AppDelegate ()

@property (nonatomic,strong) SCSettings* settings;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window=[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SCMainNavController* _rootViewController =[storyboard instantiateViewControllerWithIdentifier:@"SCMainNavController"];

    self.settings = [SCSettings sharedSettings];
    _rootViewController.settings = _settings;
    
    [_window setRootViewController: _rootViewController];
    [_window makeKeyAndVisible];

    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [_settings appChangeState:SCAppStateChangeWillResignActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    _settings.isInBackgraund = YES;
    [_settings appChangeState:SCAppStateChangeDidEnterBackground];
    
    BOOL backgroundAccepted = [[UIApplication sharedApplication] setKeepAliveTimeout:610 handler:^{
        [self backgroundHandler];
    }];
    
    if (backgroundAccepted)
    {
        FTLog(@"VOIP backgrounding accepted");
//        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
    UIApplication* app = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [_settings appChangeState:SCAppStateChangeWillEnterForeground];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [_settings appChangeState:SCAppStateChangeDidBecomeActive];

    [FBSDKAppEvents activateApp];
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [_settings appChangeState:SCAppStateChangeWillTerminate];

}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
//    FTLog(@"application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation");
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)backgroundHandler
{
//    FTLog(@"### -->VOIP backgrounding callback");
    UIApplication* app = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
}

#pragma mark RemoteNotifications
- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
//    FTLog(@"didRegisterForRemoteNotificationsWithDeviceToken");
    
    const unsigned *tokenBytes = deviceToken.bytes;

    FTLog(@"deviceToken %@ ",[[NSString alloc] initWithFormat:@"%08x %08x %08x %08x %08x %08x %08x %08x",
                              ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                              ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                              ntohl(tokenBytes[6]), ntohl(tokenBytes[7])]);
    
    NSString *_token;

    _token = [[NSString alloc] initWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
              ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
              ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
              ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    if(_settings && _settings.notifycationManager)
        [_settings.notifycationManager setDeviceNotifycationToken:_token];
}



- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    FTLog(@"didFailToRegisterForRemoteNotificationsWithErrore");
}



- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)notification
{
//    FTLog(@"%@",notification);
//    [[[UIAlertView alloc] initWithTitle:@"Push notifycation" message:[NSString stringWithFormat:@"%@",notification] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];

//    FTLog(@"didReceiveRemoteNotification");
    
    if(_settings && _settings.notifycationManager)
    {
        [_settings.notifycationManager receiveRemoteNotification:notification];
    }
    
}

+ (void)initialize
{
#if TARGET_IPHONE_SIMULATOR
    //DatePicker is bad work with AudioServicesPlaySystemSound in Simulator
    //solution from here http://habrahabr.ru/post/261995/
    Method m = class_getInstanceMethod(objc_lookUpClass("UIPickerTableView"), sel_getUid("_playClickIfNecessary"));
    method_setImplementation(m, imp_implementationWithBlock(^(id _self){}));
#endif
}
@end
