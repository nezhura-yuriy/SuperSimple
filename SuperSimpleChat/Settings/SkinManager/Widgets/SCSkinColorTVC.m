//
//  SCSkinColorTVC.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 29.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSkinColorTVC.h"

@implementation SCSkinColorTVC

- (void)awakeFromNib
{
    _imgColor.layer.borderColor = [[UIColor grayColor] CGColor];
    _imgColor.layer.borderWidth = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
