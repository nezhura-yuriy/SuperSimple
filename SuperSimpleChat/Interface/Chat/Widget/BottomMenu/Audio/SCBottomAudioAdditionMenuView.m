//
//  SCBottomAudioAdditionMenuView.m
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCBottomAudioAdditionMenuView.h"
#import "SCSettings.h"
#import "SCMediaCapture.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"

//#import "SCRecordButton.h"
//#import "SCPlayButton.h"
//#import "SCStopButton.h"

#import <AVFoundation/AVFoundation.h>

#define MARGIN 5
#define VIEW_HEIGHT 52

static const NSTimeInterval kTimeAudionFileMax = 60;
static const NSTimeInterval kTimeSliderInterval = 0.05;

typedef enum {
    SCPlayerModeRecord = 0,
    SCPlayerModePlay
}  SCPlayerMode;


@implementation SCBottomAudioAdditionMenuView
{
    UIButton* _recordButton;
    UIButton* _playButton;
    UIButton* _btnOk;
    UIButton* _btnClear;
    
    UIProgressView *_progressView;
    UILabel *_timeLabel;
    NSString *_audioFile;
    NSURL* _audioURL;
    
    AVAudioRecorder *_audioRecorder;
    AVAudioPlayer *_audioPlayer;
    AVAudioSession *_audioSession;
    
    NSTimer *_timerProrgess;
    float _progressViewValueMax;
    
    SCPlayerMode _playerMode;

    void (^actionSelector) (id selectItem);
}


-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    _settings = settings;
    if(self)
    {
//        self.backgroundColor = [UIColor yellowColor];

        _progressView = [[UIProgressView alloc] init];
        [self addSubview:_progressView];
        
        _recordButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_recordButton setImage:[_settings.skinManager getImageForKeyX:@"chat-record-start"] forState:UIControlStateNormal];
        [_recordButton addTarget:self action:@selector(recordButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_recordButton];
        
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton setImage:[_settings.skinManager getImageForKeyX:@"chat-play"] forState:UIControlStateNormal];
        [_playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        _playButton.enabled = NO;
        [self addSubview:_playButton];
        
        _btnOk = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnOk setImage:[_settings.skinManager getImageForKeyX:@"btn_add"] forState:UIControlStateNormal];
        [_btnOk addTarget:self action:@selector(btnOkPressed:) forControlEvents:UIControlEventTouchUpInside];
        _btnOk.enabled = NO;
        [self addSubview:_btnOk];
        
        _btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnClear setImage:[_settings.skinManager getImageForKeyX:@"chat-record-cancel"] forState:UIControlStateNormal];
        [_btnClear addTarget:self action:@selector(btnClearPressed:) forControlEvents:UIControlEventTouchUpInside];
        _btnClear.enabled = NO;
        [self addSubview:_btnClear];
        
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.textColor = SKCOLOR_FontRegular;
        _timeLabel.text = [NSString stringWithFormat:@"%@ / %@",[self formatSecond:0],[self formatSecond:(kTimeAudionFileMax)]];
        [self addSubview:_timeLabel];
        
        _audioSession = [AVAudioSession sharedInstance];
        [_audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:NULL];
        
//        self.layer.borderColor = [[UIColor greenColor] CGColor];
//        self.layer.borderWidth = 1;
//        _recordButton.layer.borderColor = [[UIColor greenColor] CGColor];
//        _recordButton.layer.borderWidth = 1;
//        _playButton.layer.borderColor = [[UIColor greenColor] CGColor];
//        _playButton.layer.borderWidth = 1;
//        _btnOk.layer.borderColor = [[UIColor greenColor] CGColor];
//        _btnOk.layer.borderWidth = 1;
//        _btnClear.layer.borderColor = [[UIColor greenColor] CGColor];
//        _btnClear.layer.borderWidth = 1;
//        _timeLabel.layer.borderColor = [[UIColor greenColor] CGColor];
//        _timeLabel.layer.borderWidth = 1;

    }
    return self;
}

-(void) dealloc
{
    
}


-(void) layoutSubviews
{
    [super layoutSubviews];
}

-(void) setActionSelector:(void(^)(id selectItem)) completionBlock
{
    actionSelector = [completionBlock copy];
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        [self reFrame:frame];
    }
}

-(void) reFrame:(CGRect) frame
{
    _progressView.frame = CGRectMake(20, 1, frame.size.width - 40, 1);
    _recordButton.frame = CGRectMake(20, 10, 32, 32);
    _playButton.frame = CGRectMake(62, 10, 32, 32);
    _btnOk.frame = CGRectMake(104, 10, 32, 32);
    _btnClear.frame = CGRectMake(146, 10, 32, 32);
    _timeLabel.frame = CGRectMake(frame.size.width - 115, 2, 100, 24);

}

-(void) setMessageBodys:(SCMessageBodys *)messageBodys
{
    _messageBodys = messageBodys;
    
    [self updateItems];
}

-(void) previewAtIndex:(NSUInteger) idx
{
    
}

-(CGSize) sizeView
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, VIEW_HEIGHT);
}

-(void) updateItems
{
    [UIView animateWithDuration:0.25 animations:^{
        [self updateItemsWithoutAnimation];
    } completion:^(BOOL finished) {
        //
    }];
}

-(void) updateItemsWithoutAnimation
{
    [self reFrame:self.frame];
    [self setNeedsLayout];
}


#pragma mark - @protocol SCMediaItemViewDelegate <NSObject>
-(void) itemTap:(id) item
{
    
}
-(void) itemSwipe:(id) item
{
    
}

//MARK: Progress View
- (void) updateProgressView: (NSTimeInterval) timeValue
{
    float progressViewValue;
    
    switch (_playerMode) {
            
        case SCPlayerModePlay:
        {
            progressViewValue = ((float) timeValue)/_progressViewValueMax;
            [_progressView setProgress:progressViewValue animated:YES];
            
            _timeLabel.text =[NSString stringWithFormat:@"%@ / %@",[self formatSecond:timeValue],[self formatSecond:(_progressViewValueMax - timeValue)]];
        }
            break;
            
            
        case SCPlayerModeRecord:
        {
            progressViewValue = ((float) timeValue)/kTimeAudionFileMax;
            [_progressView setProgress:progressViewValue animated:YES];
            
            _timeLabel.text = [NSString stringWithFormat:@"%@ / %@",[self formatSecond:timeValue],[self formatSecond:(kTimeAudionFileMax - timeValue)]];
            
        }
            break;
            
        default:
            break;
    }
}

- (NSString *) formatSecond: (NSTimeInterval)numberSeconds  {
    
    numberSeconds = lroundf(numberSeconds);
    
    NSUInteger m = (int) (numberSeconds / 60)% 60;
    NSUInteger s = (int) numberSeconds % 60;
    return [NSString stringWithFormat:@"%01lu:%02lu", (unsigned long)m, (unsigned long)s];
}

//MARK: Recorder
- (AVAudioRecorder *) createAudioRecorder
{
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey]; //kAudioFormatAppleIMA4
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey]; //numberWithFloat:16000.0
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey]; //numberWithInt: 2
    
    
    _audioFile = [_settings.mediaTempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.m4a",[self makeTempFileName]]];
    _audioURL = [[NSURL alloc] initFileURLWithPath:_audioFile];
    
    
    AVAudioRecorder *audioRecorder = [[AVAudioRecorder alloc] initWithURL:_audioURL settings:recordSetting error:NULL];
    audioRecorder.delegate = self;
    
    return audioRecorder;
}

- (void) recordButtonPressed:(id)sender
{
    if(!_audioRecorder || ![_audioRecorder isRecording])
    {
        if(!_audioRecorder)
            _audioRecorder = [self createAudioRecorder];
        
        if ([_audioRecorder prepareToRecord]) {
            [_audioRecorder recordForDuration:kTimeAudionFileMax];
            
            [self playerStopped];
            _playButton.enabled = NO;
            _playerMode = SCPlayerModeRecord;
            [_recordButton setImage:[_settings.skinManager getImageForKeyX:@"chat-record-stop"] forState:UIControlStateNormal];
            
            _timerProrgess = [NSTimer scheduledTimerWithTimeInterval:kTimeSliderInterval
                                                              target:self
                                                            selector:@selector(updateRecorderTimeSlider)
                                                            userInfo:nil
                                                             repeats:YES];
        }
    }
    else
    {
        [_audioRecorder stop];
//        [self recorderStopped];
    }
}

- (void) updateRecorderTimeSlider
{
    [self updateProgressView:_audioRecorder.currentTime];
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    [self recorderStopped];
}

- (void) recorderStopped
{
    [_recordButton setImage:[_settings.skinManager getImageForKeyX:@"chat-record-start"] forState:UIControlStateNormal];

    if([_audioRecorder isRecording])
        [_audioRecorder stop];
    [_timerProrgess invalidate];
    
    _playerMode = SCPlayerModePlay;
    
    _audioPlayer = [self createAudioPlayer];
    _progressViewValueMax = _audioPlayer.duration;
    _playButton.enabled = YES;
    _btnOk.enabled = YES;
    _btnClear.enabled = YES;
    
    [_progressView setProgress:0 animated:NO];
    _timeLabel.text = [NSString stringWithFormat:@"%@ / %@",[self formatSecond:0],[self formatSecond:(_audioPlayer.duration)]];

}

//MARK: Play
- (AVAudioPlayer *) createAudioPlayer
{
    if (_audioURL != nil) {
        AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:_audioURL error:NULL];
        audioPlayer.delegate = self;
        return audioPlayer;
    }
    return nil;
}

- (void) playButtonPressed:(id)sender
{
    
    if(_audioPlayer)
    {
        if(![_audioPlayer isPlaying])
        {
            _playerMode = SCPlayerModePlay;
            
            [_playButton setImage:[_settings.skinManager getImageForKeyX:@"chat-record-stop"] forState:UIControlStateNormal];

            [_audioPlayer setCurrentTime:0];
            [_audioPlayer play];
            
            _timerProrgess = [NSTimer scheduledTimerWithTimeInterval:kTimeSliderInterval
                                                              target:self
                                                            selector:@selector(updatePlayerTimeSlider)
                                                            userInfo:nil
                                                             repeats:YES];
        }
        else
        {
//            [_audioPlayer stop];
            [self playerStopped];
        }
    }

}

- (void) updatePlayerTimeSlider
{
    [self updateProgressView:_audioPlayer.currentTime];
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self playerStopped];
}

- (void) playerStopped
{
    [_audioPlayer stop];
    [_timerProrgess invalidate];
    
    _playerMode = SCPlayerModePlay;
    [_playButton setImage:[_settings.skinManager getImageForKeyX:@"chat-play"] forState:UIControlStateNormal];
    [_progressView setProgress:0 animated:NO];
    _timeLabel.text = [NSString stringWithFormat:@"%@ / %@",[self formatSecond:0],[self formatSecond:(_audioPlayer.duration)]];

    _progressViewValueMax = _audioPlayer.duration;
}

#pragma mark -
- (void) btnOkPressed:(id)sender
{
//TODO:Корректировать добавление звука
    FTLog(@"relativeString %@",[_audioURL relativeString]);
    FTLog(@"absoluteString %@",[_audioURL absoluteString]);
    SCMessageBodyItem* messageBody = [[SCMessageBodyItem alloc] initWithSettings:_settings];
    messageBody.type = SCDataTypeAudioFile;
    messageBody.data = _audioFile;
    [_messageBodys addItem:messageBody];

    if(actionSelector)
        actionSelector(@"Ok");
}
- (void) btnClearPressed:(id)sender
{
    _recordButton.enabled = YES;
    _playButton.enabled = NO;
    _btnOk.enabled = NO;
    _btnClear.enabled = NO;

    if(actionSelector)
        actionSelector(@"Clear");
}

-(NSString*) makeTempFileName
{
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    return [NSString stringWithFormat:@"audio_%@", guid];
}

@end
