//
//  SCSwitch.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 06.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSwitch.h"
#import "SCSettings.h"

@implementation SCSwitch
{
    SCSettings *_settings;
}
//MARK: Init's
- (void)awakeFromNib {
    [super awakeFromNib];
    [self _init];
    [self setupUI];
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        [self _init];
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if  (self) {
        [self _init];
        [self setupUI];
    }
    return self;
}

- (void) _init
{
    _settings = [SCSettings sharedSettings];
}

- (void) setupUI
{
    self.onTintColor = SKCOLOR_SwitchOn;
    self.tintColor = SKCOLOR_SwitchOff;
    //self.thumbTintColor = SKCOLOR_SwitchThumb;
}

@end
