//
//  SCLTableView.m
//  SuperSimpleChat
//
//  Created by yury on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCLTableView.h"
#import "SCLTableViewCell.h"

const float kRowHeight = 50.0f;

@implementation SCLTableView
{
    UIScrollView *_scrollView;
    id<SCLTableViewDataSource> _datasource;
    NSMutableSet *_reuseCells;
    Class _cellClass;
    
    UILabel *_helpAddNewItem;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectNull];
        [self addSubview:_scrollView];
        
        _scrollView.delegate = self;
        _scrollView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        
        _reuseCells = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _scrollView.frame = self.frame;
    [self refreshView];
    
}

//MARK: Cell lifecycle

// based on the current scroll location, recycles off-screen cells and
// creates new ones to fill the empty space.
- (void) refreshView
{
    /*Перерисовываются все ячейки на scrollView */
    
    if (CGRectIsNull(_scrollView.frame)) {
        return; //если нулевой фрейм, то ничего не делаем
    }
    
    //Задаем contentSize
    CGFloat contentSizeWidth = _scrollView.bounds.size.width;
    CGFloat contentSizeHeight = [_datasource numberOfRows] * kRowHeight;
    if (contentSizeHeight <= _scrollView.bounds.size.height) {
        //если ячеек меньше чем на экран, то размер чуть-больше экрана
        contentSizeHeight = _scrollView.bounds.size.height + 1;
    }
    
    _scrollView.contentSize = CGSizeMake(contentSizeWidth,
                                         contentSizeHeight);
    
    // remove cells that are no longer visible
    // Перебираем все видимые ячейки и если они выше или ниже экрана,
    // то скрываем их
    for (UIView *cell in [self cellSubviews]) {
        
        // is the cell off the top of the scrollview?
        if (cell.frame.origin.y + cell.frame.size.height < _scrollView.contentOffset.y)
        {
            [self recycleCell:cell];
        }
        
        // is the cell off the bottom of the scrollview?
        if (cell.frame.origin.y > _scrollView.contentOffset.y + _scrollView.frame.size.height)
        {
            [self recycleCell:cell];
        }
    }
    
    // ensure we have a cell for each row
    
    int frstVal2 = floor(_scrollView.contentOffset.y / kRowHeight);
    int firstVisibleIndex = MAX(0, frstVal2);
    
    int lstVal1 = (int) [_datasource numberOfRows];
    int lstVal2 = firstVisibleIndex + 1 + ceil(_scrollView.frame.size.height / kRowHeight);
    
    int lastVisibleIndex = MIN(lstVal1, lstVal2);
    
    //Перебираем каждый возможный индекс для ячеек
    for (int row = firstVisibleIndex; row < lastVisibleIndex; row++)
    {
        UIView *cell = [self cellForRow:row];
        if (!cell)
        {
            UIView *cell = [_datasource cellForRow:row]; //ViewController дает данные о ячейке, вкл ее содержимое
            float topEdgeForRow = row * kRowHeight; //определяем отступ от верха для каждой ячейки
            cell.frame = CGRectMake(0, topEdgeForRow, _scrollView.frame.size.width, kRowHeight);
        [_scrollView insertSubview:cell atIndex:0]; //добавление ячейки на экран
        }
    }
    
    //Show help view if need
    if (firstVisibleIndex == lastVisibleIndex) {
        
        if (!_helpAddNewItem) {
            _helpAddNewItem = [self helpAddNewItem];
            [_scrollView addSubview:_helpAddNewItem];
        }
        
    } else if (_helpAddNewItem){
        [_helpAddNewItem removeFromSuperview];
        _helpAddNewItem = nil;
    }
}

- (UILabel *) helpAddNewItem
{
    UILabel *helpLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, 44)];
    helpLabel.text = @"↓ Pull to Add Item";
    helpLabel.textColor = SKCOLOR_ViewHelpFont; //[UIColor grayColor];
    helpLabel.textAlignment = NSTextAlignmentCenter;
    
    return helpLabel;
}

// re-cycles a cell by adding it the set of re-use cells and removing it from the view
- (void) recycleCell:(UIView*)cell
{
    /* Метод для сохранения переиспользованной ячейки  */
    [_reuseCells addObject:cell]; //добавляем ячейку в множество используемых ячеек
    [cell removeFromSuperview]; //удаляем ячейку с экрана (ScrollView)
}

// returns the cell for the given row, or nil if it doesn't exist
- (UIView*) cellForRow:(NSInteger)row
{
    float topEdgeForRow = row * kRowHeight;
    for (UIView *cell in [self cellSubviews]) {
        if (cell.frame.origin.y == topEdgeForRow) {
            return cell;
        }
    }
    return nil;
}

// the scrollViewer subviews that are cells
- (NSArray*)cellSubviews
{
    NSMutableArray* cells = [[NSMutableArray alloc] init];
    for (UIView *subView in _scrollView.subviews)
    {
    //if ([subView isKindOfClass:[SCLTableViewCell class]])
        if ([subView isKindOfClass:[_cellClass class]])
        {
            [cells addObject:subView];
        }
    }
    return cells;
}

//MARK: UIScrollViewDelegate handlers
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self refreshView];
    
    // forward the delegate method
    if ([self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]){
        [self.delegate scrollViewDidScroll:scrollView];
    }
}

//MARK: UIScrollViewDelegate forwarding
- (BOOL)respondsToSelector:(SEL)aSelector
{
    if ([self.delegate respondsToSelector:aSelector]) {
        return YES;
    }
    return [super respondsToSelector:aSelector];
}

- (id)forwardingTargetForSelector:(SEL)aSelector
{
    if ([self.delegate respondsToSelector:aSelector]) {
        return self.delegate;
    }
    return [super forwardingTargetForSelector:aSelector];
}

//MARK: public API methods

- (void) registerClassForCells: (Class)cellClass
{
    _cellClass = cellClass;
}

- (NSArray *) visibleCells
{
    //Создаем массив из всех View на ScrollView с классом
    //как у ячеек
    NSMutableArray *cells = [[NSMutableArray alloc] init];
    for (UIView *subView in [self cellSubviews])
    {
        [cells addObject:subView];
    }
    
    //Сортируем массив ячеек в зависимости от того,
    //какая View массива находится выше/ниже на экране
    NSArray *sortedCells = [cells sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        UIView *view1 = (UIView*)obj1;
        UIView *view2 = (UIView*)obj2;
        
        float result = view2.frame.origin.y - view1.frame.origin.y;
        if (result > 0.0) {
            return NSOrderedAscending;
        } else if (result < 0.0){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    return sortedCells;
}

- (UIView*) dequeueReusableCell
{
    // first obtain a cell from the re-use pool
    
    //Берем любую ячейку из множества переиспользуемых ячеек
    UIView *cell = [_reuseCells anyObject];
    
    if (cell) { //если ячейка есть, удаляем ее из множества
        [_reuseCells removeObject:cell];
    }
    
    // otherwise create a new cell
    if (!cell) { //если ячейки  нет - создаем её
        cell = [[_cellClass alloc] init];
    }
    return cell;
}

- (void)reloadData
{
    // remove all subviews
    [[self cellSubviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self refreshView];
}

- (void) changeCellItemPUUID: (SCPUUID *) searchItemPUUID on: (SCPUUID *) rightItemPUUID
{
    for (UIView *subView in _scrollView.subviews){
        if ([subView isKindOfClass:[_cellClass class]])
            {
                SCLTableViewCell *cell = (SCLTableViewCell *) subView;
            
                if ([cell.item.puuid isEqual:searchItemPUUID]) {
                    cell.item.puuid = rightItemPUUID;
                    return;
                }
            }
    }
}

//MARK: Property getters / setters
- (UIScrollView *) scrollView
{
    return _scrollView;
}

- (id<SCLTableViewDataSource>)datasource
{
    return _datasource;
}

- (void)setDatasource:(id<SCLTableViewDataSource>)datasource
{
    _datasource = datasource;
    [self refreshView];
}

@end
