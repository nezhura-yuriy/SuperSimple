//
//  SCMessageBodyGroupView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 20.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCTypes.h"

#define MARGIN 10
#define ICON_SIZE 44
#define NEXT_WIDTH 15
#define NEXT_MARGIN 4

@class SCSettings,SCMessageBodys;


@interface SCMessageBodyGroupView : UIView <UIScrollViewDelegate>
{
    SCSettings* _settings;
    CGFloat _maxWidht;
    SCMessageAlignment _bodyAlignment;
    
    UIScrollView* _itemsScroll;
    UIButton* _btnPrev;
    NSMutableArray* _viewsArray;
    UIButton* _btnNext;
    CGFloat _itemWidth;
    CGFloat _itemHeight;
    NSUInteger _previewIdx;
    
    NSString* _btnPrevImage;
    NSString* _btnNextImage;
}



@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,assign) CGFloat maxWidht;
@property(nonatomic,assign) SCMessageAlignment bodyAlignment;

-(id) initWithSettings:(SCSettings*) settings;
-(void) _init;
-(void) setMessageBodys:(SCMessageBodys*) messageBodys;
-(CGSize) viewSize;

-(CGFloat) getItemWidth;
-(void) btnPrevAction:(id) sender;
-(void) btnNextAction:(id) sender;
@end



