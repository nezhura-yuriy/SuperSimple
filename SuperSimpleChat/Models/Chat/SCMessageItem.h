//
//  SCMessageItem.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"
#import "SCContactItem.h"
#import "SCMessageBodyItem.h"
#import "SCMessageBodys.h"

@class SCChatItem;
@class SCSettings;
@class SCMessageBodys;

@interface SCMessageItem : SCAbstractItem

@property(nonatomic,strong) NSString* messageID;
@property(nonatomic,strong) SCChatItem* chat;
@property(nonatomic,strong) SCContactItem* contact;
@property(nonatomic,assign) SCMessageSecurityLevel securityLevel;
@property(nonatomic,assign) NSTimeInterval timeDestroy;
@property(nonatomic,strong) NSDate* messageDateTime;
@property(nonatomic,strong) SCMessageBodys* messageBodys;

// runtime
@property(nonatomic,assign) BOOL isDeleted;
@property(nonatomic,strong) NSDate* dateDestroy;

-(id) initWithChatItem:(SCChatItem*) chatItem;
-(CGFloat) lifeTime;
//-(NSArray*) messageBodysAR;
@end
