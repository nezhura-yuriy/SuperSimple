//
//  Localiser.h
//  shoppop
//
//  Created by Yuriy Nezhura on 26.11.14.
//  Copyright (c) 2014 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Localiser : NSObject
{
    NSDictionary *_languageDict;
    NSBundle *_localizidBandle;
}

+ (id) sharedLocaliser;
- (NSArray*) getLanguageList;
- (NSArray *) getLanguagesCodesList;
- (NSString*) changeCurrentLanguage:(NSString*) lang;
- (void) setCurrentLanguage:(NSString*) langKey;
- (NSString*) languageNameForKey:(NSString*) langKey;
- (NSString*)getLocalisedStringByKey:(NSString*)strKey;

@end
