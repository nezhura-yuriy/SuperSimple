//
//  SCAbstractItem.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 20.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"

@implementation SCAbstractItem


-(id) init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }
    return self;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if( self )
    {
        _settings = settings;
        _delegates = [[NSMutableArray alloc] init];
        [self _init];
    }
    return self;
}

-(id) initWithDictionary:(NSDictionary*) dictionary
{
    self = [super init];
    if(self)
    {
        [self fromDictionary:dictionary];
        
    }
    return self;
}

-(void) _init
{

}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    
}

-(NSDictionary*) toDictionary
{
    return [[NSDictionary alloc] init];
}

#pragma mark Delegates
-(void) addDelegate:(id<SCModelAbstarctDelegate>) delegate
{
    @synchronized(_delegates)
    {
        if(![_delegates containsObject:delegate])
        {
            [_delegates addObject:delegate];
        }
    }
}

-(void) delDelegate:(id<SCModelAbstarctDelegate>) delegate
{
    @synchronized(_delegates)
    {
        if([_delegates containsObject:delegate])
        {
            [_delegates removeObject:delegate];
        }
    }
}


@end
