//
//  SCMessageItemView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SCSettings;
@class SCMessageItem;




@interface SCMessageItemView : UIView


@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) SCMessageItem* messageItem;

-(id) initWithSettings:(SCSettings*) settings forWidth:(CGFloat) width;
-(CGSize) viewSize;

@end
