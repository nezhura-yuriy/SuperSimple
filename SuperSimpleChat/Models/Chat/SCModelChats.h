//
//  SCModelChats.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModelsDelegates.h"
#import "SCTypes.h"

@class SCSettings,SCPBManager,SCChatItem,SCContactItem,SCPUUID,SCMessageItem;



@interface SCModelChats : NSObject <SCPBManagerDelegate>

@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) SCPBManager* protoBufManager;

-(id) initWithSettings:(SCSettings*) settings;
-(void) addDelegate:(id<SCModelChatsDelegate>) delegate;
-(void) delDelegate:(id<SCModelChatsDelegate>) delegate;

-(NSArray*) items:(SCChatType) scChatType;
-(SCChatItem*) getPrivateForUser:(SCContactItem*) user;
-(SCChatItem*) searchChatWithSCPUUID:(SCPUUID*) scpuuid;
-(NSUInteger) countNotViewedForType:(SCChatType) scChatType;

#pragma mark - CACHE
-(void) clearBeforeLogout;
-(void) loadCached;
-(void) saveCache;
-(void) clearCached;
-(void) checkCachedItems;

#pragma mark - DEBUG
//-(void) localLoad:(NSInteger) type;

#pragma mark - SERVER
-(void) serverLoad;
-(void) addNewChatRoom:(SCChatItem *) newChatItem;
-(void) changeChatRoom:(SCChatItem *) corrChatItem;
-(void) removeChatRoom:(SCChatItem*) chatItem;
-(void) activateChatRoom:(SCChatItem*) chatItem;
-(void) searchPublicChatRooms:(NSMutableDictionary*) params;

@end
