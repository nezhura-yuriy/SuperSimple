//
//  SCAddressTableViewController.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"
#import "SCCountrysTableViewController.h"
#import "SCLabel.h"

@interface SCAddressTableViewController : UITableViewController <UITextFieldDelegate, SCCountrysTableVCProtocol, SCModelProfileProtocol>

@property (weak, nonatomic) IBOutlet SCLabel *countryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cityImageView;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *streetTextField;
@property (weak, nonatomic) IBOutlet UITextField *houseTextField;
@property (weak, nonatomic) IBOutlet UITextField *apartmentTextField;
@property (weak, nonatomic) IBOutlet UITextField *postCodeTextField;

@property (strong, nonatomic) SCSettings *settings;

@end
