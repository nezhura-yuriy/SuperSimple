//
//  SCPhotoPanel.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 23.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPhotoPanel.h"
#import "SCImageViewWithIndicator.h"

@implementation SCPhotoPanel
{
    CGFloat startPosX;
    CGFloat posX;
    
    UIPanGestureRecognizer* _panGesture;
    UITapGestureRecognizer* _tapGesture;
}
-(void) awakeFromNib
{
    [super awakeFromNib];
    
    _btnAdd.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _btnAdd.layer.borderWidth = 0.5f;
    _btnAdd.layer.cornerRadius = 5.0f;
    [_btnAdd addTarget:self action:@selector(btnAddAction:) forControlEvents:UIControlEventTouchUpInside];
    
//    _btnView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    _btnView.layer.borderWidth = 0.5f;
//    _btnView.layer.cornerRadius = 5.0f;
//    [_btnView addTarget:self action:@selector(btnViewAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnClear.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _btnClear.layer.borderWidth = 0.5f;
    _btnClear.layer.cornerRadius = 5.0f;
    [_btnClear addTarget:self action:@selector(btnClearAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
    [_panGesture setMaximumNumberOfTouches:1];
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    _imgPhoto.userInteractionEnabled = YES;
}

-(void) dealloc
{
    
}
-(void) setProgress:(long long) totalBytesRead totalBytesExpectedToRead:(long long) totalBytesExpectedToRead
{
    [_imgPhoto setProgress:totalBytesRead totalBytesExpectedToRead:totalBytesExpectedToRead];
}
-(void) endProgress
{
    [_imgPhoto endProgress];
}

-(void) setIsEditedle:(BOOL)isEditedle
{
    _isEditedle = isEditedle;
    if(!_isEditedle)
    {
        [self removeGestureRecognizer:_panGesture];
        [self removeGestureRecognizer:_tapGesture];
    }
    else
    {
        [self addGestureRecognizer:_panGesture];
        [_imgPhoto addGestureRecognizer:_tapGesture];
    }
}

-(void) settingImage:(UIImage*) img
//-(void) setImgPhoto:(UIImageView *)imgPhoto
{
//    image = [self rotate:image andOrientation:0];
    _imgPhoto.image = img;
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:img.CGImage];
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:20.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    _imgBackgraund.image = [UIImage imageWithCGImage:cgImage];

}

-(void) panGestureAction:(UIGestureRecognizer*) panGesture
{
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            FTLog(@"UIGestureRecognizerStateBegan");
            startPosX = [panGesture locationInView:self].x;
            posX = _viewBackgraund.frame.origin.x;
        }
            break;
            
        case UIGestureRecognizerStateChanged:
        {
//            FTLog(@"UIGestureRecognizerStateChanged");
            CGFloat deltaX = [panGesture locationInView:self].x - startPosX;
            if(posX + deltaX > 0)
                deltaX = -posX;
            else if((posX + deltaX) < -_btnAdd.frame.size.width)
                deltaX = -_btnAdd.frame.size.width;
            _viewBackgraund.frame = CGRectMake(posX + deltaX, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
        }
            break;
            
        case UIGestureRecognizerStateEnded:
        {
            FTLog(@"UIGestureRecognizerStateEnded");
            CGFloat deltaX = [panGesture locationInView:self].x - startPosX;
            if((posX + deltaX) < -_btnAdd.frame.size.width/2)
            {// stay open
                [UIView animateWithDuration:0.25 animations:^{
                    _viewBackgraund.frame = CGRectMake(-_btnAdd.frame.size.width, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
                }];
            }
            else
            {// stay close
                [UIView animateWithDuration:0.25 animations:^{
                    _viewBackgraund.frame = CGRectMake(0, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
                }];
            }
        }
            break;

        case UIGestureRecognizerStateCancelled:
        {
            FTLog(@"UIGestureRecognizerStateCancelled");
            [UIView animateWithDuration:0.25 animations:^{
                _viewBackgraund.frame = CGRectMake(posX, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
            }];
        }
            break;

        default:
            break;
    }
}
-(void) tapGestureAction:(UIGestureRecognizer*) tapGesture
{
    if(tapGesture.state == UIGestureRecognizerStateEnded)
    {
        if(_viewBackgraund.frame.origin.x != 0)
        {
            [UIView animateWithDuration:0.25 animations:^{
                _viewBackgraund.frame = CGRectMake(0, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
            }];
        }
        else
        {
            [UIView animateWithDuration:0.25 animations:^{
                _viewBackgraund.frame = CGRectMake(-_btnAdd.frame.size.width, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
            }];
        }
    }

}

-(void) btnAddAction:(id) sender
{
    FTLog(@"btnAddAction");
    [UIView animateWithDuration:0.25 animations:^{
        _viewBackgraund.frame = CGRectMake(0, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
    }];
    if(_delegate && [_delegate respondsToSelector:@selector(addImageAction)])
        [_delegate addImageAction];

}

-(void) btnViewAction:(id) sender
{
    FTLog(@"btnViewAction");
    [UIView animateWithDuration:0.25 animations:^{
        _viewBackgraund.frame = CGRectMake(0, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
    }];
    if(_delegate && [_delegate respondsToSelector:@selector(showImageAction)])
        [_delegate showImageAction];
}

-(void) btnClearAction:(id) sender
{
    FTLog(@"btnClearAction");
    [UIView animateWithDuration:0.25 animations:^{
        _viewBackgraund.frame = CGRectMake(0, 0, _viewBackgraund.frame.size.width, _viewBackgraund.frame.size.height);
    }];
    if(_delegate && [_delegate respondsToSelector:@selector(clearImageAction)])
        [_delegate clearImageAction];

}

@end
