//
//  SCGlobalSearchViewController.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 22.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCGlobalSearchViewController.h"
#import "Macro.h"
#import "SCSettings.h"
#import "Localiser.h"
#import "SCGlobalContactCell.h"
#import "SCmodelContacts.h"
#import <SVProgressHUD.h>
#import "NSString+SCContains.h"


@interface SCGlobalSearchViewController () <SCmodelContactsDelegate>

@property (strong, nonatomic) Localiser* localiser;
@property (strong, nonatomic) SCContactItem* contact;
@property (strong, nonatomic) NSMutableArray* contacts;
@property (strong, nonatomic) NSString* searchAuth;
@property (strong, nonatomic) NSMutableArray* cellsArray;

@property (assign, nonatomic) BOOL isSearching;

@end

@implementation SCGlobalSearchViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.cellsArray = [NSMutableArray new];
    
    self.searchAuth = [NSString new];
    
    self.settings = [SCSettings sharedSettings];
    
    self.localiser = [Localiser sharedLocaliser];
    
    self.contacts = [NSMutableArray new];
    
    self.userSearchBar.delegate = self;

    [self settingNavigationBar];
    
    self.tableView.backgroundColor = SKCOLOR_TabBarBg;
    
    self.view.backgroundColor = SKCOLOR_TabBarBg;
    
    self.tableView.sectionIndexColor = SKCOLOR_TableCountrySectionIndexFont;
    
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    
    self.settings.modelContacts.delegate = self;
    
    [_tableView registerNib:[UINib nibWithNibName:@"SCGlobalContactCell" bundle:nil] forCellReuseIdentifier:@"contactSearch"];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeDefult]];
    
    UITextField *textField = [self.userSearchBar valueForKey: @"_searchField"];
    [textField setTextColor:SKCOLOR_FontRegular];
    [textField setFont:[UIFont fontWithName:FT_FontNameDefult size:15]];
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationItem.title = @"Global search";
    
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBarTintColor:SKCOLOR_NavBarBg];
    self.navigationController.navigationBar.translucent = NO;
   
}


#pragma mark - Notification

- (void)notificationChangeSkin:(NSNotification *)note
{
    [self.tableView reloadData];
    
    self.tableView.backgroundColor = SKCOLOR_TabBarBg;
    
    self.view.backgroundColor = SKCOLOR_TabBarBg;
    
    self.tableView.sectionIndexColor = SKCOLOR_TableCountrySectionIndexFont;
    
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
}


#pragma mark - Initialization methods

- (void)settingNavigationBar {
    
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    
    
}


#pragma mark - Actions


- (void)backAction {
    
      [self.navigationController popViewControllerAnimated:YES];
  
}


- (void)dealloc{
    
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.contacts count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"contactSearch";
    
    SCGlobalContactCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        
        [tableView registerNib:[UINib nibWithNibName:@"SCGlobalContactCell" bundle:nil] forCellReuseIdentifier:@"contactSearch"];
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
        SCContactItem* contact = [self.contacts objectAtIndex:indexPath.row];
    
        cell.searchAuth = self.searchAuth;
    
        [cell fillWhithContact:contact];
    
    
    [self.cellsArray addObject:cell];
    
    return cell;

}

#pragma mark UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - SCmodelContactsDelegate

-(void) getUserByAuthRespond:(SCContactItem*) contact{
    
    [SVProgressHUD dismiss];
    
    _isSearching = NO;
    
    if (contact) {
        
        
        [self.contacts addObject:contact];
        
        [self.tableView reloadData];
    }
}

-(void)getUsersRespondError:(NSDictionary *)errorMessage {
    
    [SVProgressHUD dismiss];
    
    _isSearching = NO;
    
    if (errorMessage) {
        
        [SVProgressHUD showErrorWithStatus:[errorMessage objectForKey:@"message"]];
    }
}

-(void) successCreateEndSetFeildsContact{
    

    [SVProgressHUD showSuccessWithStatus:@"Contact Create"];
    
    
    SCGlobalContactCell* cell = [self.cellsArray objectAtIndex:0];
    
    [cell.addUserButtonOutlet setImage:[UIImage imageNamed:@"checkButton"] forState:UIControlStateNormal];
    
    cell.addUserButtonOutlet.enabled = NO;
    
    cell.addUserButtonOutlet.tintColor = [UIColor lightGrayColor];
    
    NSString* fullName = [NSString new];
    
    if (!cell.item.contactLastName.length==0 || !cell.item.contactFirstName.length==0) {
        if (cell.item.contactLastName.length > 0) {
            fullName = [NSString stringWithFormat:@"%@ ",cell.item.contactLastName];
        }
        if (cell.item.contactFirstName.length > 0) {
            fullName = [fullName stringByAppendingString:cell.item.contactFirstName];
        }
        cell.item.fullName = fullName;
    } else if (!cell.item.contactNikName.length == 0) {
        cell.item.fullName = cell.item.contactNikName;
    }

    
    [self.settings.modelContacts addNewContactToModel:cell.item];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateContacts" object:nil];
    
}

#pragma mark UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    _isSearching = NO;

    [SVProgressHUD dismiss];

    searchBar.text = nil;
    
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
    [self.contacts removeAllObjects];
     self.searchAuth = nil;
    
    [self.cellsArray removeAllObjects];
    
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    if (!_isSearching) {
        
        [SVProgressHUD show];
        
        [self.contacts removeAllObjects];
        self.searchAuth = nil;
        [self.tableView reloadData];
        
        
        NSString* searchAuth = searchBar.text;
        
        if ([searchAuth myContainsString:@"@"]) {
            
            self.searchAuth = searchAuth;
            
            NSDictionary* authDict = @{@"mail":searchAuth};
            
            SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
            
            NSInteger typeDelegate = 10002;
            
            NSDictionary* params = @{@"AuthForSearchUser":authDict,@"delegate":_settings.modelContacts, @"UUID":transportPUUID,@"value":[NSNumber numberWithInteger:typeDelegate]};
            
            [_settings.modelContacts getUserByAuthWithParams:params];
            
        } else {
            
            self.searchAuth = searchAuth;
            
            NSDictionary* authDict = @{@"phone":searchAuth};
            
            SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
            
            NSInteger typeDelegate = 10002;
            
            NSDictionary* params = @{@"AuthForSearchUser":authDict,@"delegate":_settings.modelContacts,@"UUID":transportPUUID,@"value":[NSNumber numberWithInteger:typeDelegate]};
            
            [_settings.modelContacts getUserByAuthWithParams:params];
            
        }

    }
    
    }

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    CustomCell *Cell = [self dequeueReusableCellWithIdentifier:@"Custom_Cell_Id"];
//    if (Cell == NULL)
//    {
//        Cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Custom_Cell_Id"]];
//        [Cells_Array addObject:Cell];
//    }
//}
//
//- (void) DoSomething
//{
//    for (int i = 0;i < [Cells count];i++)
//    {
//        CustomCell *Cell = [Cells objectAtIndex:i];
//        //Access cell components
//    }
//}




@end
