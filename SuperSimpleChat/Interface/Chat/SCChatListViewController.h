//
//  SCChatListViewController.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"
#import "SCModelsDelegates.h"
#import <SWTableViewCell.h>

@class SCSettings,SCSegmentControll,SCChatItem,SCTopSelector;


@interface SCChatListViewController : UIViewController<SWTableViewCellDelegate,SCPBManagerDelegate,SCModelChatsDelegate,SCChatListViewControllerDelegate>

@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,weak) id<SCMainRevalControllerDelegate> revalDelegate;

@property (strong, nonatomic) IBOutlet UIView* pullRefreshView;
@property (strong, nonatomic) IBOutlet UIImageView *refreshArrow;
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet SCSegmentControll *btnChangeType;
@property (strong, nonatomic) IBOutlet SCTopSelector *swichChangeType;
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@property (strong, nonatomic) IBOutlet UIView *viewSearch;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;


@property (strong, nonatomic) IBOutlet UIView *viewEmptyBg;
@property (strong, nonatomic) IBOutlet UIImageView *viewEmptyImageView;
@property (strong, nonatomic) IBOutlet UILabel *viewEmptyLab;


-(void) enterToRoom:(SCChatItem*) chatData;
@end
