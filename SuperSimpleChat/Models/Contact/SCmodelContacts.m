//
//  SCmodelContacts.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 03.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCmodelContacts.h"
#import "SCSettings.h"
#import "SCContactGroup.h"
#import "SCPBManager.h"
#import <SVProgressHUD.h>



@interface SCmodelContacts ()


@property(strong,nonatomic) NSArray *paths;
@property(strong,nonatomic) NSString *cachesDirectory;
@property(strong,nonatomic) NSFileManager *fileManager;


@end

@implementation SCmodelContacts
{
    NSMutableArray* _items;
    SCPBManager* _pbManager;

}

-(id) init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }
    return self;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if ( self )
    {
        _settings = settings;
        _pbManager = _settings.modelProfile.protoBufManager;
        
        [_pbManager addDelegate:self forType:SCPBManagerParceTypeContactCreate];
        [_pbManager addDelegate:self forType:SCPBManagerParceTypeGetAllContacts];
//        [_pbManager addDelegate:self forType:SCPBManagerParceTypeContactRemove];
        [_pbManager addDelegate:self forType:SCPBManagerParceTypeSearchUsersByAuth];
        [_pbManager addDelegate:self forType:SCPBManagerParceTypeGetUserByAuth];
        [_pbManager addDelegate:self forType:SCPBManagerParceTypeContactSetFields];
        [_pbManager addDelegate:self forType:SCPBManagerParceTypeInviteUsers];
        
        [self _init];
    }
    return self;
}

-(void) _init
{
    _items = [[NSMutableArray alloc] init];
    _groups = [[NSMutableArray alloc] init];

//    [self loadCached];
    
    self.cachesDirectory = _settings.cacheContactsPath;
    
    self.path = [self.cachesDirectory stringByAppendingPathComponent:@"contacts_.plist"];
    
    
    self.fileManager = [NSFileManager defaultManager];
    
     [SVProgressHUD setBackgroundColor:SKCOLOR_NavBarBg];
     [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
}


-(void) dealloc
{
    [_pbManager delDelegate:self forType:SCPBManagerParceTypeContactCreate];
    [_pbManager delDelegate:self forType:SCPBManagerParceTypeGetAllContacts];
    [_pbManager delDelegate:self forType:SCPBManagerParceTypeContactRemove];
    [_pbManager delDelegate:self forType:SCPBManagerParceTypeSearchUsersByAuth];
    [_pbManager delDelegate:self forType:SCPBManagerParceTypeGetUserByAuth];
    [_pbManager delDelegate:self forType:SCPBManagerParceTypeContactSetFields];
    [_pbManager delDelegate:self forType:SCPBManagerParceTypeInviteUsers];


}

// 
-(NSMutableArray*) loadCached
{
    NSMutableArray* addressbookContacts = nil;
    
    NSMutableArray* contacts = [NSMutableArray new];
    
        if ([self.fileManager fileExistsAtPath: self.path])
    
        {
            addressbookContacts = [[NSMutableArray alloc] initWithContentsOfFile:self.path];
    
            for (NSDictionary* contactDict in addressbookContacts ) {
    
                SCContactItem* contact = [[SCContactItem alloc] init];
    
                [contact fromDictionary:contactDict];
    
                [contacts addObject:contact];
                
            }
        
        }
    
    return contacts;
}


-(void)saveContacts:(NSMutableArray*) contacts{
    
    NSMutableArray* contactsBook = [NSMutableArray new];
    
    for (SCContactItem* contact in contacts) {
        
        NSDictionary* contactDict = [contact toDictionary];
        
        [contactsBook addObject:contactDict];
    }
    
    if (![self.fileManager fileExistsAtPath: self.path])
    {
        self.path = [self.cachesDirectory stringByAppendingPathComponent:[NSString stringWithFormat: @"contacts_.plist"]];
    }
    
   [contactsBook writeToFile: self.path atomically:YES];
    
}


-(void)clearCached {
    
    NSError* error;
    
    [self.fileManager removeItemAtPath: self.path error: &error];
    
}


-(NSArray*) items:(NSString*) groupID
{
// future filtred
//    NSMutableArray* res = [[NSMutableArray alloc] init];
    return _items;
}

-(SCContactItem*) searchByUUID:(SCPUUID*) puuid
{
    SCContactItem* theItem;
    for(SCContactItem* item in _items)
    {
        if([item.userPUUID isEqual:puuid])
            return item;
    }
    
    return theItem;
}

-(void)addNewContactToModel:(SCContactItem*)contact{
    
    [_items addObject:contact];
}


#pragma mark DEBUG: load from server

-(void) localLoad
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"contacts" ofType:@"plist"];
    NSArray *tmpArray =[NSMutableArray arrayWithContentsOfFile:path];
    for(NSDictionary *dict in tmpArray)
    {
        SCContactItem* item = [[SCContactItem alloc] initWithSettings:_settings];
        [item fromDictionary:dict];
        [_items addObject:item];
    }
    
    path = [[NSBundle mainBundle] pathForResource:@"groupscontacts" ofType:@"plist"];
    tmpArray = [NSMutableArray arrayWithContentsOfFile:path];
    for(NSDictionary *dict in tmpArray)
    {
        SCContactGroup* itemGroup = [[SCContactGroup alloc] initWithSettings:_settings];
        [itemGroup fromDictionary:dict];
        [_groups addObject:itemGroup];
    }
}


#pragma mark - SCPBManagerDelegate


-(void)parsedType:(SCPBManagerParceType)type data:(id)data
{
    switch (type)
    {
        case SCPBManagerParceTypeContactCreate:
            FTLog(@"%@",data);
            break;
            
        case SCPBManagerParceTypeContactRemove:
            if ([data isKindOfClass:[NSString class]]) {
                
                
                if ([self.delegate respondsToSelector:@selector(errorDeleteContact:)])
                {
                    [self.delegate errorDeleteContact:data];
                }
                
            } else {

                
                if ([self.delegate respondsToSelector:@selector(successDeleteContact)])
                                    {
                                        if ([data isKindOfClass:[SCContactItem class]]) {
                                            
                                            [_items removeObject:data];

                                        }
                                        
                                        [self.delegate successDeleteContact];
                                        
                                    }
            }
            
            
        
            break;
            
        case SCPBManagerParceTypeGetAllContacts:
            
            _items = data;
            
            if ([self.delegate respondsToSelector:@selector(getContactsFromModel:)])
            {
                [self.delegate getContactsFromModel:data];
            }
            break;
            
        case SCPBManagerParceTypeSearchUsersByAuth:
            if ([self.delegate respondsToSelector:@selector(seartUsersRespond:)])
            {
                [self.delegate seartUsersRespond:data];
            }
            break;
            
        case SCPBManagerParceTypeGetUserByAuth:
            if ([self.delegate respondsToSelector:@selector(getUsersRespondError:)])
            {
                if ([data isKindOfClass:[NSDictionary class]])
                {
                    [self.delegate getUsersRespondError:data];
                }
            }
            if ([self.delegate respondsToSelector:@selector(getUserByAuthRespond:)])
            {
                if ([data isKindOfClass:[SCContactItem class]])
                {
                    [self.delegate getUserByAuthRespond:data];
                }
            }
            break;
            
        case SCPBManagerParceTypeContactSetFields:
            if ([self.delegate respondsToSelector:@selector(successCreateEndSetFeildsContact)])
            {
                [self.delegate successCreateEndSetFeildsContact];
            }
            break;
            
        case SCPBManagerParceTypeInviteUsers:
            if ([self.delegate respondsToSelector:@selector(successInvite:)])
            {
                [self.delegate successInvite:data];
            }
            break;
            

            
        default:
            FTLog(@"SCmodelContacts default");
            break;
    }
}


-(void) createNewContactWithParams:(NSMutableDictionary*)params{
    
    [_pbManager createContactRequestWithParams:params];
}


-(void) removeContact:(SCContactItem*)contact{

     [_pbManager removeContactRequest:contact];
}


-(void) getAllContacts:(NSMutableDictionary*)params{
    
    [_pbManager getAllContactsRequestWithParams:params]  ;
}

-(void) searchUsersByAuthRequest:(NSMutableDictionary*) params
{
    
    [_pbManager searchUsersByAuthRequest:params];
}

-(void) creatContactWithFields:(NSDictionary*) fields{
    
    [_pbManager creatContactWithFields:fields];
    
}

-(void) getUserByAuthWithParams:(NSDictionary*)params{
    
     [_pbManager getUserByAuth:params];
    
}

-(void)inviteUsers:(id)data{
    
    [_pbManager inviteUsersRequest:data];
    
}



@end
