//
//  SCSkinsViewController.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 28.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSkinViewController.h"
#import "SCSkinManager.h"
#import "SCSkinColorTVC.h"

@interface SCSkinViewController ()

@end

@implementation SCSkinViewController
{
    NSArray* _colorNames;
    NSDictionary* _defaultSkin;
    NSDictionary* _colorItems;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tableView registerNib:[UINib nibWithNibName:@"SCSkinColorTVC" bundle:nil] forCellReuseIdentifier:@"SCSkinColorTVC"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

-(void) setSkinManager:(SCSkinManager *)skinManager
{
    _skinManager = skinManager;
    _colorNames = [_skinManager getColorArray];
    [_switcher setSelectedSegmentIndex:[_skinManager getCurrentSkinIndex]];
    _colorItems = [[_skinManager getCurrentSkinData] objectForKey:@"Colors"];
}
- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switcherAction:(id)sender
{
    UISegmentedControl* segment = (UISegmentedControl*) sender;
    NSString *aa = [[[_skinManager getInstalledSkins] objectAtIndex:segment.selectedSegmentIndex] objectForKey:@"Name"];
    [_skinManager selectSkinName:aa];
    _colorItems = [[_skinManager getCurrentSkinData] objectForKey:@"Colors"];
    [_tableView reloadData];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _colorNames.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SCSkinColorTVC* cell = [_tableView dequeueReusableCellWithIdentifier:@"SCSkinColorTVC"];
    
    NSString* colorName = [[_colorNames objectAtIndex:indexPath.row] objectForKey:@"Name"];
    cell.labName.text = colorName;
    
    NSDictionary* colorDict = [_colorItems objectForKey:colorName];
    cell.labRGB.text = [NSString stringWithFormat:@"%1.4f |\n%1.4f | %1.4f\n%1.4f |",[[colorDict objectForKey:@"R"] floatValue],[[colorDict objectForKey:@"G"] floatValue],[[colorDict objectForKey:@"A"] floatValue],[[colorDict objectForKey:@"B"] floatValue]];
    cell.imgColor.backgroundColor = [UIColor colorWithRed:[[colorDict objectForKey:@"R"] floatValue] green:[[colorDict objectForKey:@"G"] floatValue] blue:[[colorDict objectForKey:@"B"] floatValue] alpha:[[colorDict objectForKey:@"A"] floatValue]];
    
    cell.labDescription.text = [[_colorNames objectAtIndex:indexPath.row] objectForKey:@"Description"];
    return cell;
}
@end
