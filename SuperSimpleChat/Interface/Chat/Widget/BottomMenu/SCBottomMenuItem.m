//
//  SCBottomMenuItem.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 29.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCBottomMenuItem.h"

#define MARGIN 5
#define BADGE_SIZE 20

@implementation SCBottomMenuItem
{
    UIImageView* _btnImage;
    UILabel* _labBadge;
}

-(id) init
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}

-(void) _init
{
//    self.layer.borderColor = [[UIColor redColor] CGColor];
//    self.layer.borderWidth = 1;
    
    _btnImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
    _btnImage.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_btnImage];
    
    _labBadge = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, BADGE_SIZE, BADGE_SIZE)];
    _labBadge.backgroundColor = [UIColor redColor];
    [_labBadge setFont:[UIFont systemFontOfSize:10]];
    [_labBadge setTextColor:[UIColor whiteColor]];
    _labBadge.layer.cornerRadius = BADGE_SIZE*0.5;
    _labBadge.textAlignment = NSTextAlignmentCenter;
    _labBadge.clipsToBounds = YES;
    _labBadge.hidden = YES;
    [self addSubview:_labBadge];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    [self addGestureRecognizer:tapGesture];
    
    UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureAction:)];
    [self addGestureRecognizer:longPressGesture];
}

-(void) dealloc
{
    
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    _btnImage.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    _labBadge.frame = CGRectMake(self.frame.size.width - BADGE_SIZE - MARGIN, self.frame.size.height - BADGE_SIZE - MARGIN, BADGE_SIZE, BADGE_SIZE);
}

-(void) setImage:(UIImage*) image
{
    _btnImage.image = image;
}

-(void) setBadge:(NSUInteger) badgeValue
{
    if(badgeValue > 0)
    {
        _labBadge.text = [NSString stringWithFormat:@"%lu",(unsigned long)badgeValue];
        _labBadge.frame = CGRectMake(self.frame.size.width - BADGE_SIZE - MARGIN, self.frame.size.height - BADGE_SIZE - MARGIN, BADGE_SIZE, BADGE_SIZE);
        _labBadge.hidden = NO;
    }
    else
        _labBadge.hidden = YES;
}

-(void) tapGestureAction:(UITapGestureRecognizer*) gesture
{
    if([_delegate respondsToSelector:@selector(tapAction:)])
        [_delegate tapAction:self.tag];
}

-(void) longPressGestureAction:(UILongPressGestureRecognizer*) gesture
{
//    gesture.state
    if([_delegate respondsToSelector:@selector(longPressAction:withState:)])
        [_delegate longPressAction:self.tag withState:gesture.state];
}

@end
