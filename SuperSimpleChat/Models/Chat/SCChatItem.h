//
//  SCChatItem.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"
#import "SCModelsDelegates.h"
#import "SCPUUID.h"
#import "SCNetFile.h"

@class SCContactItem,SCImageViewWithIndicator,SCMessageGroup,SCMessageItem,SCNetFile;

@interface SCChatItem : SCAbstractItem

@property(nonatomic,strong) NSString* chatID;
@property(nonatomic,strong) SCPUUID* chatPuuid;
@property(nonatomic,strong) NSString* chatName;
@property(nonatomic,strong) NSString* chatDescription;
@property(nonatomic,assign) NSTimeInterval timeDestroy;
@property(nonatomic,assign) SCChatType chatType;
@property(nonatomic,strong) SCNetFile* netFile;
@property(nonatomic,strong) SCAbstractItem* logoSource;
@property(nonatomic,assign) SCMessageSecurityLevel securityLevel;
@property(nonatomic,strong) SCContactItem* chatOwner;
@property(nonatomic,assign) BOOL isDeleted;
@property(nonatomic,strong) NSDate* lastViewedMessageDate;
@property(nonatomic,strong) NSDate* lastLoadedMessageDate;

@property(nonatomic,assign) NSInteger notViewedMessagesCount;
@property(nonatomic,strong) NSMutableArray* usersInChat;
@property(nonatomic,strong) NSMutableArray* chatDayMessages;
@property(nonatomic,strong) SCMessageItem* lastMessage;
@property(nonatomic,strong) NSDate* itemUpdatedTime;

-(void) addUserInChat:(SCContactItem*) user;

-(void) lifeTimeEnd:(SCMessageItem*) item;

#pragma mark - CACHE
-(void) loadCachedMessages;
-(void) saveCachedMessages;

#pragma mark messageGroup
-(SCMessageGroup*) searchGroup:(NSDate*) messageDateTime;
-(SCMessageGroup*) newGroup:(NSDate*) messageDateTime;
-(void) sortGroups;

#pragma mark message
//-(void) localLoad:(NSString*) chatID;
-(SCMessageItem*) searchMessageItemByPUUID:(SCPUUID*) scPUUID;
-(SCMessageItem*) searchMessageItemByPUUID:(SCPUUID*) scPUUID  messageDateTime:(NSDate*) messageDateTime;
-(SCMessageItem*) newMessageItemByPUUID:(SCPUUID*) scPUUID messageDateTime:(NSDate*) messageDateTime;
-(SCMessageItem*) getLastMessage;
//-(void) updateItem;
-(void) updateItemContent;

-(void) chatNewMessages:(NSArray*) serverMessages;
-(void) setChatImageLink:(NSString *)chatImageLink;

-(void) loadUserList;
-(void) loadHistory:(NSInteger) offs;
-(void) itemGetLastReadDate;
-(void) itemSetLastReadDate:(NSDate*) lastReadDate;
-(void) joinUserToRoom:(SCContactItem*) contactItem;
-(void) leaveUserFromRoom:(SCContactItem*) contactItem;
-(void) sendMessage:(SCMessageItem*) newMessage;
-(void) updateMessage:(SCMessageItem*) updatedMessage;
-(void) removeMessage:(SCMessageItem*) removedMessage;

-(void) parsedType:(SCPBManagerParceType) type data:(id) data;
@end

