//
//  NSMutableArray+SCNSMutableArray.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 25.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (SCNSMutableArray)

- (BOOL)isEqualToMyArray:(NSMutableArray *)array;
- (BOOL)isEqualToListsArray:(NSMutableArray *) array;
- (BOOL)isEqualToListItemsArray:(NSMutableArray *)array;

- (NSMutableArray *)mutableCopyListItem;

@end
