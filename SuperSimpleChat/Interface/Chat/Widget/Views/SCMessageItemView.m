//
//  SCMessageItemView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageItemView.h"
#import "SCSettings.h"
#import "SCModelChats.h"
#import "SCContactItem.h"
#import "SCMessageItem.h"
#import "SCMessageBodyView.h"
#import "SCBezierPathes.h"
#import "SCTypes.h"
#import "SCChatItem.h"
#import "SCChatSwipeMenu.h"



#define CHAT_IMAGE_SIZE 44.0f
#define SEPARATOR_HEIGHT 2.0f
#define MARGIN 5.0f
#define MARGIN_TIME 2.0f
#define SCBEZIER_RADIUS_SIZE [SCBezierPathes radiusSize]*0.7

@implementation SCMessageItemView
{
    CGFloat _atWidth;
    CGFloat _bodyWidth;
    SCMessageAlignment _messageAlignment;
    NSDateFormatter* _df;
    
    CAShapeLayer *shapeLayer;
//    CAShapeLayer *shapeTmpLayer;
    UIImageView* _imageUser;
    UILabel* _labNikName;
    UILabel* _labDateTime;
    SCMessageBodyView* _messageBodyView;
    
    CGSize _sizeView;
    CGSize _sizeMessage;
    CGSize _sizeNik;
    CGSize _sizeTime;
    
    NSTimer* _lifeTimer;
    
    UISwipeGestureRecognizer* _swipeLeftGesture;
    UISwipeGestureRecognizer* _swipeRightGesture;
    
    SCChatSwipeMenu* _swipeMenu;
}

-(id) initWithSettings:(SCSettings*) settings forWidth:(CGFloat) width
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        _atWidth = width;
        _bodyWidth = width*0.75;
        [self _init];
    }
    return self;
}

-(void) _init
{
    [self setUserInteractionEnabled:YES];
    _sizeView = CGSizeZero;
    _df = [[NSDateFormatter alloc] init];
    [_df setDateFormat:@"HH:mm"];

//    shapeTmpLayer = [CAShapeLayer layer];
//    [self.layer addSublayer:shapeTmpLayer];
    
    shapeLayer = [CAShapeLayer layer];
    [self.layer addSublayer:shapeLayer];

    UITapGestureRecognizer* tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserAction:)];
    tapGuesture.numberOfTapsRequired = 1;
    tapGuesture.delaysTouchesBegan = YES;
    tapGuesture.delaysTouchesEnded = NO;
    
    
     _swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
     [_swipeLeftGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    
     _swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
     [_swipeRightGesture setDirection:UISwipeGestureRecognizerDirectionRight];
     
    
//    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
//    [self addGestureRecognizer:panGesture];
    
    _imageUser = [[UIImageView alloc] init];
    _imageUser.image = [UIImage imageNamed:@"nofoto"];
    _imageUser.layer.borderColor = [[UIColor whiteColor] CGColor];
    _imageUser.layer.borderWidth = 2;
    _imageUser.layer.cornerRadius = CHAT_IMAGE_SIZE*0.5;
    _imageUser.clipsToBounds = YES;
    _imageUser.userInteractionEnabled = YES;
    [_imageUser addGestureRecognizer:tapGuesture];

    _labNikName = [[UILabel alloc] init];
    [_labNikName setFont:[UIFont fontWithName:FT_FontNameDefult size:12.0]];//[UIFont boldSystemFontOfSize:12.0];
    _labNikName.textColor = SKCOLOR_ChatMessageDateText;
    [self addSubview:_labNikName];
    
    _labDateTime = [[UILabel alloc] init];
    [_labDateTime setFont:[UIFont fontWithName:FT_FontNameDefult size:9.0]];//[UIFont boldSystemFontOfSize:9.0];
    _labDateTime.textAlignment = NSTextAlignmentRight;
    _labDateTime.textColor = SKCOLOR_ChatMessageDateText;
    [self addSubview:_labDateTime];
    
    
//    self.layer.borderWidth = 1;
//    self.layer.borderColor = [[UIColor redColor] CGColor];
//    self.backgroundColor = [UIColor yellowColor];
//    _labNikName.layer.borderWidth = 1.0f;
//    _labNikName.layer.borderColor = [[UIColor redColor] CGColor];
//    _labNikName.backgroundColor = [UIColor blueColor];
//    _labDateTime.layer.borderWidth = 1.0f;
//    _labDateTime.layer.borderColor = [[UIColor redColor] CGColor];

}

-(void) dealloc
{
    if(_lifeTimer)
    {
        [_lifeTimer invalidate];
        _lifeTimer = nil;
    }
}

-(void) setMessageItem:(SCMessageItem *)messageItem
{
    _messageItem = messageItem;
    
    CGFloat calcHeight = 0;
    _labNikName.text = _messageItem.contact.contactNikName;
    [_labNikName sizeToFit];
    _sizeNik = CGSizeMake(MIN(_labNikName.frame.size.width, _bodyWidth),_labNikName.frame.size.height);
    
    if(_messageItem.timeDestroy > 0 && !_messageItem.isDeleted)
    {
        _lifeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(destroyTicks:) userInfo:nil repeats:YES];
        NSString* secLife1 = [NSString stringWithFormat:@"-%@ ",[self formatSecond:_messageItem.lifeTime]];
        NSString* secLife = [NSString stringWithFormat:@"%@%@  ",secLife1,[_df stringFromDate:_messageItem.messageDateTime]];
        NSMutableAttributedString * labDateTimeAtribString = [[NSMutableAttributedString alloc] initWithString:secLife];
        [labDateTimeAtribString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,secLife1.length)];
        _labDateTime.attributedText = labDateTimeAtribString;
    }
    else
        _labDateTime.text = [_df stringFromDate:_messageItem.messageDateTime];
    [_labDateTime sizeToFit];
    _sizeTime = _labDateTime.frame.size;
    
    if([_messageItem.messageBodys items].count > 0 && ((SCMessageBodyItem*)[[_messageItem.messageBodys items] firstObject]).type == SCDataTypeSystemText)
    {
        _messageAlignment = SCMessageAlignmentCenter;
    }
    else if([messageItem.contact.contactPUUID isEqual:_settings.modelProfile.contactItem.userPUUID])
    {
        _messageAlignment = SCMessageAlignmentRight;
        [self addGestureRecognizer:_swipeLeftGesture];
        [self addGestureRecognizer:_swipeRightGesture];
    }
    else
    {
        _messageAlignment = SCMessageAlignmentLeft;
        calcHeight +=_sizeNik.height + MARGIN;
    }
    _messageBodyView = [[SCMessageBodyView alloc] initWithSettings:_settings];
    _messageBodyView.maxWidht = _bodyWidth;
    [_messageBodyView setMessageBodys:_messageItem.messageBodys];
    [self addSubview:_messageBodyView];
    
    // вычисление размера
    CGFloat theWidth = MIN(MAX([_messageBodyView viewSize].width, _labNikName.frame.size.width), _bodyWidth);
    CGFloat theHeight = calcHeight + [_messageBodyView viewSize].height;// + MARGIN_TIME + _sizeTime.height;
    
    _sizeMessage = CGSizeMake(theWidth, theHeight);
    _sizeView = CGSizeMake(_atWidth, theHeight + MARGIN + _sizeTime.height + 4*MARGIN);
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if( !CGRectEqualToRect(frame, CGRectZero) )
    {
        
        UIBezierPath *path1;
        CGFloat offs = SCBEZIER_RADIUS_SIZE;
        if (_messageAlignment == SCMessageAlignmentRight)
        {
            CGRect _rectMessage = CGRectMake(_atWidth - _sizeMessage.width - 2*MARGIN - offs,MARGIN,_sizeMessage.width + 2*MARGIN,_sizeMessage.height + 2*MARGIN);
            
            CGFloat posX = _rectMessage.origin.x + MARGIN;
            CGFloat posY = _rectMessage.origin.y + MARGIN;
            
            _labNikName.frame = CGRectZero;
            _messageBodyView.frame = CGRectMake(posX, posY, [_messageBodyView viewSize].width, [_messageBodyView viewSize].height);
            posY +=[_messageBodyView viewSize].height + MARGIN;
            posX = _rectMessage.origin.x + _rectMessage.size.width - _sizeTime.width;
            _labDateTime.frame = CGRectMake(posX, posY, _sizeTime.width, _sizeTime.height);
                                          
            CGPoint zeroPoint = CGPointMake(_rectMessage.origin.x + _rectMessage.size.width + offs, _rectMessage.origin.y + _rectMessage.size.height);
            path1 = [SCBezierPathes messageBezier:frame rectArea:_rectMessage zeroPoint:zeroPoint];
            _messageBodyView.bodyAlignment = SCMessageAlignmentRight;
//            _messageBodyView.textColor = SKCOLOR_ChatMessageOutgoingText;
            shapeLayer.fillColor = [SKCOLOR_ChatMessageOutgoingBg CGColor];
            _labNikName.textColor = [UIColor clearColor];
            shapeLayer.path = [path1 CGPath];

//            UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect:_rectMessage];
//            shapeTmpLayer.fillColor = [[UIColor redColor] CGColor];
//            shapeTmpLayer.path = [rectanglePath CGPath];

        }
        else if (_messageAlignment == SCMessageAlignmentLeft)
        {
            CGRect _rectMessage = CGRectMake(MARGIN + offs,MARGIN,_sizeMessage.width + 2*MARGIN,_sizeMessage.height + 2*MARGIN);

            CGFloat posX = _rectMessage.origin.x + MARGIN;
            CGFloat posY = _rectMessage.origin.y + MARGIN;

            _labNikName.frame = CGRectMake(posX, posY, _sizeNik.width, _sizeNik.height);
            posY +=_sizeNik.height + MARGIN;
            _messageBodyView.frame = CGRectMake(posX, posY, [_messageBodyView viewSize].width, [_messageBodyView viewSize].height);
            posY +=[_messageBodyView viewSize].height+ MARGIN;
            posX = _rectMessage.origin.x + _rectMessage.size.width - _sizeTime.width;
            _labDateTime.frame = CGRectMake(posX, posY, _sizeTime.width, _sizeTime.height);
            
//            UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect:_rectMessage];
//            shapeTmpLayer.fillColor = [[UIColor redColor] CGColor];
//            shapeTmpLayer.path = [rectanglePath CGPath];

            CGPoint zeroPoint = CGPointMake(_rectMessage.origin.x - offs, _rectMessage.origin.y + _rectMessage.size.height);
            path1 = [SCBezierPathes messageBezier:frame rectArea:_rectMessage zeroPoint:zeroPoint];
            _messageBodyView.bodyAlignment = SCMessageAlignmentLeft;
//            _messageBodyView.textColor = SKCOLOR_ChatMessageIncomingText;
            shapeLayer.fillColor = [SKCOLOR_ChatMessageIncomingBg CGColor];
            _labNikName.textColor = SKCOLOR_ChatListNickName;
            shapeLayer.path = [path1 CGPath];

        }
        else
        {
            CGFloat posX = (_atWidth - _bodyWidth)/2;
            CGFloat posY = MARGIN;
            _labNikName.frame = CGRectZero;
            CGRectMake(posX, posY, _sizeNik.width, _sizeNik.height);
//            posY +=_sizeNik.height+ MARGIN;
            _messageBodyView.frame = CGRectMake(posX, posY, [_messageBodyView viewSize].width, [_messageBodyView viewSize].height);
            _messageBodyView.bodyAlignment = SCMessageAlignmentCenter;
            posY +=[_messageBodyView viewSize].height + MARGIN_TIME;
            posX = _messageBodyView.frame.origin.x + _messageBodyView.frame.size.width - _sizeTime.width;
            _labDateTime.frame = CGRectMake(posX, posY, _sizeTime.width, _sizeTime.height);

//            _messageBodyView.textColor = SKCOLOR_ChatMessageDateText;
        }
        
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    //    FTLog(@"layoutSubviews %@",NSStringFromCGRect(self.frame));
}

-(CGSize) viewSize
{
    return _sizeView;
}

- (NSString *) formatSecond: (NSTimeInterval)numberSeconds  {
    
    numberSeconds = lroundf(numberSeconds);
    
    NSUInteger m = (int) (numberSeconds / 60)% 60;
    NSUInteger s = (int) numberSeconds % 60;
    return [NSString stringWithFormat:@"%01lu:%02lu", (unsigned long)m, (unsigned long)s];
}


-(void) destroyTicks:(NSTimer*) timer
{
    NSComparisonResult result = [[NSDate date] compare:_messageItem.dateDestroy];
    if(result == NSOrderedDescending)
    {
        _messageItem.isDeleted = YES;
        if(_lifeTimer)
        {
            [_lifeTimer invalidate];
            _lifeTimer = nil;
        }
        [_messageItem.chat lifeTimeEnd:_messageItem];
    }
    else
    {
        NSString* secLife1 = [NSString stringWithFormat:@"-%@ ",[self formatSecond:_messageItem.lifeTime]];
        NSString* secLife = [NSString stringWithFormat:@"%@%@",secLife1,[_df stringFromDate:_messageItem.messageDateTime]];
        NSMutableAttributedString * labDateTimeAtribString = [[NSMutableAttributedString alloc] initWithString:secLife];
        [labDateTimeAtribString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,secLife1.length)];
        _labDateTime.attributedText = labDateTimeAtribString;
    }
}

#pragma mark UITapGestureRecognizerDelegate
-(void) tapUserAction:(UITapGestureRecognizer*) tapGestureNotify
{
    FTLog(@"");
}

#pragma mark UIPanGestureRecognizerDelegate
-(void) panGestureAction:(UIPanGestureRecognizer*) panGestureNotify
{
    FTLog(@"panGestureNotify");
}

-(void)swipeGestureAction:(UISwipeGestureRecognizer *) swipeGestureNotify
{
    FTLog(@"UISwipeGestureRecognizer");
    if(swipeGestureNotify.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if(!_swipeMenu)
        {
            _swipeMenu = [[SCChatSwipeMenu alloc] initWithSettings:_settings];
            _swipeMenu.frame = CGRectMake(self.frame.size.width, self.frame.origin.y, 10, 2);
            [_swipeMenu.btnMenuDelete addTarget:self action:@selector(swipeDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.superview addSubview:_swipeMenu];
            [UIView animateWithDuration:0.5 animations:^{
                _swipeMenu.frame = CGRectMake(self.frame.size.width - 95, self.frame.origin.y+10, 90, 24);
                self.frame = CGRectMake(-100, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
            }];
        }
    }
    else if(swipeGestureNotify.direction == UISwipeGestureRecognizerDirectionRight)
    {
        [UIView animateWithDuration:0.5 animations:^{
            _swipeMenu.frame = CGRectMake(self.frame.size.width, self.frame.origin.y, 10, 2);
            self.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished) {
            [_swipeMenu removeFromSuperview];
            _swipeMenu = nil;
        }];
    }
}

-(void) swipeDeleteAction:(id) sender
{
    [_swipeMenu removeFromSuperview];
    [_messageItem.chat removeMessage:_messageItem];
    if(_swipeMenu)
    {
        [UIView animateWithDuration:0.5 animations:^{
            _swipeMenu.frame = CGRectMake(self.frame.size.width, self.frame.origin.y, 10, 2);
            self.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished) {
            [_swipeMenu removeFromSuperview];
            _swipeMenu = nil;
        }];
    }
}

@end
