//
//  SCAccessPhoneViewController.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 20.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"
#import <FBSDKLoginButton.h>
#import "SCButton.h"
#import "SCLabel.h"
#import "SCTextField.h"

@class SCSettings;


@interface SCAccessPhoneViewController : UIViewController

@property(nonatomic,weak) id<SCMainNavControllerDelegate> delegate;
@property(nonatomic,strong) SCSettings* settings;


@property (weak, nonatomic) IBOutlet SCButton *countryCodeButtonOutlet;
@property (weak, nonatomic) IBOutlet SCTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet SCButton *phoneNumberOutlet;
@property (weak, nonatomic) IBOutlet SCButton *emailRegistrationOutlet;
@property (weak, nonatomic) IBOutlet SCButton *connectWithFacebookOutlet;

- (IBAction)countryCodeAction:(id)sender;
- (IBAction)phoneRegistrationActon:(id)sender;
- (IBAction)emailRegistrationAction:(id)sender;
- (IBAction)connectWithFacebookAction:(id)sender;



@end
