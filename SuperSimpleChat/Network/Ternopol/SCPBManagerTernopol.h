//
//  SCPBManagerTernopol.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 02.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManager.h"
#import "SCSettings.h"
//#import "SCPBManagerTernopol(converts).h"
//#import "SCPBManagerTernopol(chatrooms).h"

@class PUUID;
@class Response;


@interface SCPBManagerTernopol : SCPBManager
{
    
}

//-(void) contactFromFields:(SCContactItem*)contactItem fields:(NSArray*)fields;
-(BOOL)existContact:(SCContactItem*)existContact inArray:(NSMutableArray*) array;
-(void)groupCreatesContactWithParams:(NSMutableDictionary*) params;
-(NSDictionary*) makeErrorDictionary:(Response*) response;

@end
