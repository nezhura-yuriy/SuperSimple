//
//  SCSegmentControll.m
//  SmartChat
//
//  Created by Yury Radchenko on 06.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSegmentControll.h"
#import "SCSettings.h"

#define TAG 1000
#define HEIGHT_SEGMENT_CONTROL 44

@implementation SCSegmentControll
{
    SCSettings *_settings;
    UIColor *_backgroundColor;
    CGFloat _heightSelectedLine;
    CGFloat _widthSelectedView;
}

//MARK: Init's
- (void)awakeFromNib {
    [super awakeFromNib];
    [self _init];
    [self setupUI];
}

- (instancetype)init {
    
    [self _init];
    self = [super init];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    
    [self _init];
    self = [super initWithCoder:aDecoder];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    [self _init];
    frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, HEIGHT_SEGMENT_CONTROL);
    
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithItems:(NSArray *)items
{
    [self _init];
    self = [super initWithItems:items];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void) _init
{
    _settings = [SCSettings sharedSettings];
    _backgroundColor = [UIColor colorWithRed:0.455 green:0.318 blue:0.563 alpha:1];
    
    _heightSelectedLine = 5;
    
    [self addTarget:self action:@selector(changeSegment) forControlEvents:UIControlEventValueChanged];
}

//MARK: Setters
- (void) setFrame:(CGRect)frame
{
    CGRect newFrame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, HEIGHT_SEGMENT_CONTROL);
    [super setFrame:newFrame];
    
    [self setupUI];
}

- (void) setTitle:(NSString *)title forSegmentAtIndex:(NSUInteger)segment
{
    [super setTitle:title forSegmentAtIndex:segment];
}

//MARK: Setups
- (void) setupUI
{
    UIFont *segmentFont = [UIFont fontWithName:FT_FontNameDefult size:14.0];
    
    NSDictionary *attributesNormal = @{NSForegroundColorAttributeName: SKCOLOR_SegmentControlV2FontInactive,
                                       NSFontAttributeName: segmentFont};
    
    NSDictionary *attributesHighlighted = @{NSForegroundColorAttributeName: SKCOLOR_SegmentControlV2FontActive,
                                            NSFontAttributeName: segmentFont};
    
    NSDictionary *attributesSelected = attributesHighlighted;
    
    [self setTitleTextAttributes:attributesSelected forState:UIControlStateSelected];
    [self setTitleTextAttributes:attributesNormal forState:UIControlStateNormal];
    [self setTitleTextAttributes:attributesHighlighted forState:UIControlStateHighlighted];
    
    self.backgroundColor = SKCOLOR_SegmentControlV2BgInactive;
    [self setTintColor:[UIColor clearColor]];
    
    //Down line
    CGFloat lineHieght = _heightSelectedLine/3;
    CGFloat y = self.bounds.size.height - lineHieght;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.frame.size.width, lineHieght)];
    line.backgroundColor = SKCOLOR_SegmentControlV2BgActive;
    [self addSubview:line];
    
    [self changeSegment];
}

- (void) changeSegment
{
    
    for (UIView *excessView in self.subviews) {
        if (excessView.tag == TAG) {
            [excessView removeFromSuperview];
        }
    }
    
    if (self.numberOfSegments != 0) {
        
        _widthSelectedView = self.frame.size.width/self.numberOfSegments;
        CGFloat x = _widthSelectedView * self.selectedSegmentIndex;
        CGFloat y = self.frame.size.height - _heightSelectedLine;
        
        UIView *selectedView = [[UIView alloc] initWithFrame:CGRectMake(x, y, _widthSelectedView, _heightSelectedLine)];
        
        selectedView.tag = TAG;
        selectedView.backgroundColor = SKCOLOR_SegmentControlV2BgActive;
        
        [self addSubview:selectedView];
    }
}

@end
