//
//  SCLaunchScreen.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 06.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCLaunchScreen : UIView


@property (strong, nonatomic) IBOutlet UILabel *labProgressInfo;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@end
