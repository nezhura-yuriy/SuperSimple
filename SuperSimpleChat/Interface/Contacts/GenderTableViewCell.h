//
//  GenderTableViewCell.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 18.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabelOutlet;

@end
