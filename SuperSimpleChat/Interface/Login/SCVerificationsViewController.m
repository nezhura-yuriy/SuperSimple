//
//  SCVerificationsViewController.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 21.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCVerificationsViewController.h"
#import "SCSettings.h"
#import "SCHTTPManager.h"
#import <SVProgressHUD.h>
#import "UINavigationController+SCRotation.h"
#import "SCSkinManager.h"

@interface SCVerificationsViewController () <UITextFieldDelegate>

@property(strong, nonatomic) SCHTTPManager* httpManager;
@property(strong, nonatomic) NSString* sessionId;
@property(strong, nonatomic) NSString* confirmationHash;



@end

@implementation SCVerificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.title = @"Verification";
    
    self.settings = [SCSettings sharedSettings];
    self.httpManager = _settings.httpManager;
    self.codeTextField.delegate = self;
    
    self.sessionId = self.settings.modelProfile.sessionID;
    self.confirmationHash = self.settings.modelProfile.confirmationHash;
    
    self.testWithoutRegistrationOutlet.layer.cornerRadius = 4.0f;
    self.goToSmartChatButton.layer.cornerRadius = 4.0f;
    
    [self settingNavigationBar];
    
    [self.codeTextField becomeFirstResponder];

    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    [SVProgressHUD setBackgroundColor:[_skinManager getColorForKeyX:@"NavigationBarBackground"]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
    if (!self.isPhoneVerification) {
        
        self.infoLabel.text = @"We sent you a code via e-mail. Just enter it below.";
    }
    
}

-(void)setSkinManager:(SCSkinManager *)skinManager{
    
    _skinManager = skinManager;
    
    self.goToSmartChatButton.backgroundColor = [_skinManager getColorForKeyX:@"ButtonGreenBackground"];
    
}

#pragma mark - Actions

- (IBAction)goToSmartChatButton:(id)sender{
    
    
    [self.codeTextField resignFirstResponder];
    
    
    if (self.sessionId && self.codeTextField.text.length > 0) {
        
        [SVProgressHUD show];
        
        [self.httpManager verificationUserWithSessionId:self.confirmationHash
                                                smsCode:self.codeTextField.text
                                              onSuccess:^(NSDictionary *verificationValue) {
                                                  
                                                  FTLog(@"JSON: %@",verificationValue);
                                                  
                                                  long status = [[verificationValue objectForKey:@"status"] longValue];
                                                  
                                                  if (status == 0) {
                                                      
                                                      [SVProgressHUD dismiss];
                                                      
                                                      if([[verificationValue  allKeys] containsObject:@"userId"])
                                                          _settings.modelProfile.userID = [verificationValue objectForKey:@"userId"];
                                                      
                                                      if([[verificationValue  allKeys] containsObject:@"sessionId"])
                                                          _settings.modelProfile.sessionID =[verificationValue objectForKey:@"sessionId"];
                                                      
                                                      if([[verificationValue  allKeys] containsObject:@"ws_link"])
                                                          _settings.modelProfile.ws_link =[verificationValue objectForKey:@"ws_link"];
                                                      [_settings saveSettings];
                                                      
                                                      if(_delegate && [_delegate respondsToSelector:@selector(succsessLogin)]){
                                                          
                                                          [_delegate succsessLogin];
                                                          
                                                      }
                                                      
                                                  } else {
                                                      
                                                      [SVProgressHUD dismiss];
                                                      
                                                      NSString* errorMessage = [verificationValue objectForKey:@"errorMessage"];
                                                      
                                                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                          message:errorMessage
                                                                                                         delegate:self
                                                                                                cancelButtonTitle:@"Cancel"
                                                                                                otherButtonTitles:nil];
                                                      
                                                      [alertView show];
                                                      
                                                      
                                                      
                                                  }
                                                  
                                              }
                                              onFailure:^(NSError *error, NSInteger statusCode) {
                                                  
                                                  [SVProgressHUD dismiss];
                                                  FTLog(@"error = %@, code = %ld", [error localizedDescription],(long)statusCode);
#pragma mark TODO показать сообщение
                                                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                      message:[error localizedDescription]
                                                                                                     delegate:self
                                                                                            cancelButtonTitle:@"Cancel"
                                                                                            otherButtonTitles:nil];
                                                  
                                                  [alertView show];
                                                  
                                              }];
        
    } else {
        
        [self startShake:self.goToSmartChatButton];
        
    }
    

    
}


- (void)backAction {
    
        [self.navigationController popViewControllerAnimated:YES];
        
}

- (IBAction)testWithoutRegistrationActon:(id)sender{
    
    
}



#pragma mark - Animation

- (void) startShake:(UIView*)view
{
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-8, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(8, 0);
    
    view.transform = leftShake;  // starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:2];
    [UIView setAnimationDuration:0.07];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    view.transform = rightShake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}



#pragma mark - UITextFieldDelegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}



#pragma mark - Supported orientation


-(BOOL)shouldAutorotate {
    
    return YES;
    
}

- (NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

#pragma mark - Initialization methods


- (void)settingNavigationBar {
    
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
//    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithHue:0.739 saturation:0.418 brightness:0.574 alpha:1]];
//    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
}



@end
