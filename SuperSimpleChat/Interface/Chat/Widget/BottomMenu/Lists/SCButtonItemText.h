//
//  SCButtonItemText.h
//  SmartChat
//
//  Created by Yury Radchenko on 2/16/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
/*
 Класс кнопки с данными item текстового Листа
 */

#import <UIKit/UIKit.h>
#import "SCAbstractButtonItem.h"
#import "SCSettings.h"

@interface SCButtonItemText : SCAbstractButtonItem

@end
