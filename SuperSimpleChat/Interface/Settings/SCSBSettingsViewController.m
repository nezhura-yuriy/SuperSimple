//
//  SCSBSettingsViewController.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSBSettingsViewController.h"
#import "SCTextViewController.h"
#import "SCBannedUsersTableViewController.h"
#import "SCContactItem.h"
#import <AVFoundation/AVFoundation.h>
#import "SCFeedbackViewController.h"

@interface SCSBSettingsViewController ()

@property (strong, nonatomic) NSString* cachesPath;

@end

@implementation SCSBSettingsViewController
{
    AVAudioPlayer *soundPlayer;
    UIEdgeInsets _tableViewSeparatorEdgeInsets;
}

//ViewLife
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    _notificationSwitch.on = _settings.isNotification; //_settings.isNotification; //!!! DEBUG для релиза = NO; //
    
    _nightmodeSwitch.on = _settings.isNightMode;
    _soundSwitch.on = _settings.isAppSound;
    _vibrateSwitch.on = _settings.isAppVibration;
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *myPath = [myPathList objectAtIndex:0];
    NSString *userIdentifier = [_settings.modelProfile.contactItem.userPUUID scpuuidToString];
    _cachesPath = [myPath stringByAppendingPathComponent:userIdentifier];
    self.cacheSizeLabel.text = [self folderSize:_cachesPath];
    
    _tableViewSeparatorEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

-(void)viewDidLayoutSubviews
{
    // iOS 7
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:_tableViewSeparatorEdgeInsets];
    }
    // iOS 8
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:_tableViewSeparatorEdgeInsets];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (cell.hidden) {
        return 0.0;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *blockedCellIndex = [NSIndexPath indexPathForRow:4 inSection:0];
    
    if ((indexPath.section == blockedCellIndex.section) && (indexPath.row == blockedCellIndex.row)) {
        SCBannedUsersTableViewController *bannedUsersVC = [[[NSBundle mainBundle] loadNibNamed:@"SCBannedUsersTableViewController" owner:self options:nil]lastObject];
        bannedUsersVC.settings = _settings;
        [self.navigationController pushViewController:bannedUsersVC animated:YES];
    }
}

//MARK: Setup
- (void) setupNavigation
{
    //Nav Title
    self.title = @"Settings";
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    
    //Back button
    UIImage *backButtonImage = [_settings.skinManager getImageForKeyX:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    //_notificationTitle.textColor = [UIColor grayColor]; ///!!! DEBUG для релиза
    _nightmodeTitle.textColor = SKCOLOR_SegmentControlV2FontInactive;
}

//MARK: Segue
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"PrivacyPolicySegue"]) {
        SCTextViewController *textVC = [segue destinationViewController];
        textVC.viewController = SCViewControllerPrivacyPolicy;
        textVC.settings = _settings;
        
    } else if ([[segue identifier] isEqualToString:@"TermsOfUseSegue"]) {
        SCTextViewController *textVC = [segue destinationViewController];
        textVC.viewController = SCViewControllerTermsOfUse;
        textVC.settings = _settings;
        
    } else if ([[segue identifier] isEqualToString:@"AboutSegue"]) {
        SCTextViewController *textVC = [segue destinationViewController];
        textVC.viewController = SCViewControllerAbout;
        textVC.settings = _settings;
        
    } else if ([[segue identifier] isEqualToString:@"FeedbackSegue"]) {
        SCFeedbackViewController *feedbackVC = [segue destinationViewController];
        feedbackVC.settings = _settings;
    }
}

//MARK: Actions

- (IBAction)notificationSwitchChanged:(id)sender {
    
    _settings.isNotification = _notificationSwitch.on;
    
}

- (IBAction)nightmodeSwitchChanged:(id)sender {

    _settings.isNightMode = _nightmodeSwitch.on;
}

- (IBAction)soundSwitchChanged:(id)sender {
    
    _settings.isAppSound = _soundSwitch.on;
    [_settings play:SCSoundPlayIsAppSoundOn];
}

- (IBAction)vibrateSwitchChanged:(id)sender {
    _settings.isAppVibration = _vibrateSwitch.on;
}

- (IBAction)cacheCleanButtonPressed:(id)sender {
    
    [_settings clearAllCache];
    
     self.cacheSizeLabel.text = [self folderSize:_cachesPath];
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Work with Cache



- (NSString*) fileSizeFromValue:(unsigned long long) size {
    
    static NSString* units[] = {@"B", @"KB", @"MB", @"GB", @"TB"};
    
    static int unitsCount = 5;
    
    int index = 0;
    
    double fileSize = (double)size;
    
    while (fileSize > 1024 && index < unitsCount) {
        
        fileSize /= 1024;
        
        index++;
        
    }
    return [NSString stringWithFormat:@"%.2f %@", fileSize, units[index]];
}



- (NSString*)folderSize:(NSString *)folderPath {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSArray *filesArray = [fm subpathsOfDirectoryAtPath:folderPath error:nil];
    
    unsigned long long int fileSize = 0;
    
    NSError *error;
    
    for(NSString *fileName in filesArray) {
        
        error = nil;
        NSDictionary *fileDictionary = [fm attributesOfItemAtPath:[folderPath     stringByAppendingPathComponent:fileName] error:&error];
        
        if (!error) {
            
            if([fileDictionary objectForKey:@"NSFileType"] != NSFileTypeDirectory)
                fileSize += [fileDictionary fileSize];
            
        }else{
            
            FTLog(@"ERROR: %@", error);
        }
    }
    
    
    return [self fileSizeFromValue:fileSize];
}




@end
