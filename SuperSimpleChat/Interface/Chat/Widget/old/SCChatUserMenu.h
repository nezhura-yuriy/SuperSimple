//
//  SCChatUserMenu.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 23.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"
#import <SWTableViewCell.h>


@interface SCChatUserMenu : SCChatBottomsView <UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray* userInRoom;
@property (nonatomic, assign) BOOL inAddMode;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;

- (IBAction)btnDoneAction:(UIButton *)sender forEvent:(UIEvent *)event;
- (IBAction)btnAddAction:(id)sender;
@end
