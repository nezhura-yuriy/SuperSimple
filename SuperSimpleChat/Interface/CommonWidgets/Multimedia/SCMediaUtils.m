//
//  SCMediaUtils.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 31.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMediaUtils.h"
#import <AVFoundation/AVFoundation.h>

@implementation SCMediaUtils


+(BOOL) haveCamera
{
//    BOOL isCameraPresentAndAccessed = NO;
    if([[AVCaptureDevice devices] count] < 1)
    {
        return NO;
    }
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized)
    {
        return YES;
    }
    else if(status == AVAuthorizationStatusDenied)
    {
        return NO;
    }
    else if(status == AVAuthorizationStatusRestricted)
    {
        // restricted
        return NO;
    }
    else if(status == AVAuthorizationStatusNotDetermined)
    {
        // not determined
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
        {
            if(granted)
            {
                NSLog(@"Granted access");
            }
            else
            {
                NSLog(@"Not granted access");
            }
        }];
    }
    return NO;
}

+ (NSString *) formatSecond: (NSTimeInterval)numberSeconds
{
    
    numberSeconds = lroundf(numberSeconds);
    
    NSUInteger m = (int) (numberSeconds / 60)% 60;
    NSUInteger s = (int) numberSeconds % 60;
    return [NSString stringWithFormat:@"%01lu:%02lu", (unsigned long)m, (unsigned long)s];
}

@end
