//
//  SCAddUserToChatViewController.m
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAddUserToChatViewController.h"
#import "SCSettings.h"
#import "SCmodelContacts.h"
#import "SCCoutrySection.h"
#import "SCContactCell.h"
#import "SCChatUserCell.h"

#define CHAT_USERS_TABLEVIEW_CELL @"SCChatListCell"


@interface SCAddUserToChatViewController ()

@property(strong, nonatomic) NSMutableArray* contacts;

@property (strong, nonatomic) NSOperation* currenTOperation;
@property (strong, nonatomic) NSMutableArray* sectionsArray;

@end

@implementation SCAddUserToChatViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [_tableView registerNib:[UINib nibWithNibName:@"SCContactCell" bundle:nil] forCellReuseIdentifier:@"contactItem"];
    
    [_tableView registerNib:[UINib nibWithNibName:@"SCChatUserCell" bundle:nil] forCellReuseIdentifier:CHAT_USERS_TABLEVIEW_CELL];



}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    [self settingNavigationBar];
    
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexColor = [UIColor grayColor];
    
    self.tableView.backgroundColor = SKCOLOR_TabBarBg;
    self.view.backgroundColor = SKCOLOR_TabBarBg;
    self.tableView.sectionIndexColor = SKCOLOR_TableCountrySectionIndexFont;
    
    
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.tabBarController.navigationItem.title = @"Contacts";
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBarTintColor:SKCOLOR_NavBarBg];
    self.navigationController.navigationBar.translucent = NO;
    
    self.title = @"Add Friends";

    
}

-(void)setSettings:(SCSettings *)settings{
    
    _settings = settings;
    
    _contacts = [[_settings.modelContacts items:nil] mutableCopy];
    
    NSMutableArray* tempContactArray = [NSMutableArray new];
    
    for (SCContactItem* contact in _contacts ) {
        
        if (![self isContain:_userInRoom byItem:contact]) {
            
            [tempContactArray addObject:contact];
        }
    }
    
    _contacts = tempContactArray;
    
    [self generateSectionsInBackgroundArrayContacts:self.contacts withFilter:nil];
}



#pragma mark - Initialization methods

- (void)settingNavigationBar {
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(backAction)];
    backBarButtonItem.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = backBarButtonItem;

}


#pragma mark - Actions

- (void)backAction {
    
        [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Privet Methods

-(BOOL) isContain:(NSArray*) inArray byItem:(SCContactItem*) byItem
{
    for(SCContactItem* theItem in inArray)
    {
        if([theItem.userPUUID isEqual:byItem.userPUUID])
            return YES;
    }
    return NO;
}


-(NSMutableArray*) generateSectionsFromArrayContacts:(NSMutableArray*) array withFilter:(NSString*) filterString {
    
    
    NSSortDescriptor* sortDescriptorByFirstName =[[NSSortDescriptor alloc]initWithKey:@"contactFirstName" ascending:YES];
    NSSortDescriptor* sortDescriptorByLastName =[[NSSortDescriptor alloc]initWithKey:@"contactLastName" ascending:YES];
    
    [self.contacts sortUsingDescriptors:@[sortDescriptorByLastName, sortDescriptorByFirstName]];
    
    NSMutableArray* sectionsArray = [[NSMutableArray alloc]init];
    
    NSString* currentLetter = nil;
    
    for (SCContactItem* contact in array) {
        
        if ([filterString length] > 0 && [contact.contactFirstName rangeOfString:filterString options:NSCaseInsensitiveSearch].location && [contact.contactLastName rangeOfString:filterString options:NSCaseInsensitiveSearch].location == NSNotFound) {
            continue;
        }
        
        NSString* firstLetter = nil;
        
        if (contact.contactLastName.length>0) {
            
            firstLetter = [contact.contactLastName substringToIndex:1];
            
        } else if (contact.contactFirstName.length>0) {
            
            firstLetter = [contact.contactFirstName substringToIndex:1];
            
        } else if (contact.contactNikName.length>0) {
            
            firstLetter = [contact.contactNikName substringToIndex:1];
            
        } else {
            
            firstLetter = @"N";
            
            contact.contactFirstName = @"No name";
        }
        
        
        SCCoutrySection* section = nil;
        
        if (![currentLetter isEqualToString:firstLetter]) {
            
            section = [[SCCoutrySection alloc]init];
            section.sectionName = firstLetter;
            section.itemsArray = [NSMutableArray array];
            currentLetter = firstLetter;
            [sectionsArray addObject:section];
        }else{
            section = [sectionsArray lastObject];
        }
        [section.itemsArray addObject:contact];
    }
    
    return sectionsArray;
    
}



-(void) generateSectionsInBackgroundArrayContacts : (NSMutableArray*) array withFilter:(NSString*) filterString {
    
    [self.currenTOperation cancel];
    
    __weak SCAddUserToChatViewController* weakSelf = self;
    
    self. currenTOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSMutableArray* sectionsArray = [weakSelf generateSectionsFromArrayContacts:array withFilter:filterString];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.sectionsArray = sectionsArray;
            [self.tableView reloadData];
            
            self.currenTOperation = nil;
        });
    }];
    [self.currenTOperation start];
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.sectionsArray count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return [[self.sectionsArray objectAtIndex:section] sectionName];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:section];
    
    return [sections.itemsArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
    SCContactItem* curContact = [sections.itemsArray objectAtIndex:indexPath.row];
    
    SCChatUserCell* chatUserCell = [tableView dequeueReusableCellWithIdentifier:CHAT_USERS_TABLEVIEW_CELL];
    
    if(!chatUserCell)
        
        chatUserCell = [[SCChatUserCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CHAT_USERS_TABLEVIEW_CELL];
    
    if([self isContain:self.userInRoom byItem:curContact])
        
        chatUserCell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        
        chatUserCell.accessoryType = UITableViewCellAccessoryNone;
    
    chatUserCell.textLabel.text = [NSString stringWithFormat:@"%@ %@",curContact.contactFirstName,curContact.contactLastName];
    
    if(curContact.contactNikName.length >0)
    {
        chatUserCell.textLabel.text = curContact.contactNikName;
    }
    else
    {
        NSString* strName = @"";
        if(curContact.contactFirstName.length)
            strName = [strName stringByAppendingFormat:@"%@",curContact.contactFirstName];
        if(curContact.contactLastName.length)
            strName = [strName stringByAppendingFormat:@" %@",curContact.contactLastName];
        chatUserCell.textLabel.text = strName;
    }
    
    chatUserCell.backgroundColor = CL_CLEAR_COLOR;
    
    return chatUserCell;


    
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    NSMutableArray* array = [NSMutableArray array];
    
    for(SCCoutrySection* section in self.sectionsArray){
        
        [array addObject:section.sectionName];
    }
    
    return array;
    
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [UIColor colorWithHue:0.41 saturation:0 brightness:0.899 alpha:1];
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    [header.textLabel setTextColor:[UIColor blackColor]];
    
}

#pragma mark - UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
    SCContactItem* curContact = [sections.itemsArray objectAtIndex:indexPath.row];
    
    if (_isNewChat) {
        
        if([self isContain:_userInRoom byItem:curContact])
            
        {
            [_userInRoom removeObject:curContact];
            
        }
        
        else
            
        {
            [_userInRoom addObject:curContact];
            
            
        }
        
    } else {
    
        if([self isContain:_userInRoom byItem:curContact])
            
        {
//            [_userInRoom removeObject:curContact];
            [_activityIndicatorView startAnimating];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                           self, @"delegate",
                                           [[SCPUUID alloc] initWithRandom], @"UUID",
                                           [NSNumber numberWithInteger:SCPBManagerParceTypeLeaveUserFromRoom], @"type",
                                           _chatItem,@"SCChatItem",
                                           curContact,@"SCContactItem",
                                           nil];
            
            [_settings.modelProfile.protoBufManager leaveUserFromRoomRequest:params];
            
        }
        else
        {
//            [_userInRoom addObject:curContact];
            [_activityIndicatorView startAnimating];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                           self, @"delegate",
                                           [[SCPUUID alloc] initWithRandom], @"UUID",
                                           [NSNumber numberWithInteger:SCPBManagerParceTypeJoinUserToRoom], @"type",
                                           _chatItem,@"SCChatItem",
                                           curContact,@"SCContactItem",
                                           nil];
            
            [_settings.modelProfile.protoBufManager joinUserToRoomRequest:params];
        }
        
    }
    
//        [_tableView reloadData];
}



#pragma mark SCPBManagerDelegate <NSObject>
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"SCChatViewController : receiveParsedData");
    switch (type)
    {
        case SCPBManagerParceTypeLeaveUserFromRoom:
        {
            [_activityIndicatorView stopAnimating];
            if([data isKindOfClass:[SCContactItem class]])
            {
                [_userInRoom removeObject:data];
                [_tableView reloadData];
            }
        }
            break;
            
        case SCPBManagerParceTypeJoinUserToRoom:
        {
            [_activityIndicatorView stopAnimating];
            if([data isKindOfClass:[SCContactItem class]])
            {
                [_userInRoom addObject:data];
                [_tableView reloadData];
            }
            else if([data isKindOfClass:[SCChatItem class]])
            {
                
            }
                
        }
            break;
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;
    }
}


@end
