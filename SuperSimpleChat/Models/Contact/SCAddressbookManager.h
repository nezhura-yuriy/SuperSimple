//
//  SCAddressbookManager.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 24.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSMutableArray+SCNSMutableArray.h"


@class SCSettings;

@interface SCAddressbookManager : NSObject



@property(nonatomic,strong) SCSettings* settings;



-(id) initWithSettings:(SCSettings*) settings;

-(void)getContacts;



-(void)saveAddressbook:(NSMutableArray*) contacts;

-(NSMutableArray*)returnAddressbook;


@end
