//
//  SCUserAuth.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 06.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCUserAuthItem.h"

@implementation SCUserAuthItem

-(id) initByType:(SCUserAuthItemType) type WithData:(NSString*) data
{
    self = [super init];
    
    if( self )
    {
        _authType = type;
        _data = data;
    }
    return self;
}

-(id) initWithType:(SCUserAuthItemType) type andData:(NSString*) data andConfirm:(BOOL)confirm
{
    self = [super init];
    
    if( self )
    {
        _authType = type;
        _data = data;
        _isConfirm=confirm;
    }
    return self;
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    _authType = (SCUserAuthItemType)[[dictionary objectForKey:@"authType"] integerValue];
    _data = [dictionary objectForKey:@"data"];
}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    [outDict setObject:[NSNumber numberWithInteger:_authType] forKey:@"authType"];
    [outDict setObject:_data forKey:@"data"];
    
    return outDict;
}

-(BOOL) isAuthEqual:(SCUserAuthItem*) authItem
{
    if(self.authType == authItem.authType)
        if([self.data isEqualToString:authItem.data])
//            if(self.isConfirm == authItem.isConfirm)
                return YES;
    return NO;
}

@end
