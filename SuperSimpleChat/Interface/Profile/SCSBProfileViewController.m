//
//  SCSBProfileViewController.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSBProfileViewController.h"
#import "UIImage+SCCropImageToCircleWithBorder.h"
#import "UIImageView+Letters.h"
#import "SCContactItem.h"
#import "SCSBSettingsViewController.h"
#import "SCAddressTableViewController.h"
#import "SCAlternativeLoginViewController.h"
#import "SCLogoutTableViewController.h"
#import "MAKDropDownMenu.h"

#import "SCModelProfile.h"
#import "SCContactItem.h"
#import "TransitionDelegate.h"

#import "SCSBBirthdayTableViewCell.h"
#import "SCSBAddressTableViewCell.h"
#import "SCSBAlternativeLoginTableViewCell.h"
#import "SCSBSettingsTableViewCell.h"
#import "SCSBLogoutTableViewCell.h"

#import <SVProgressHUD.h>

static const CGFloat kUIMarge = 15;

static NSString *kStatusOnlineTitle = @" Online";
static NSString *kStatusBusyTitle = @" Busy";
static NSString *kStatusAwayTitle = @" Away";
static NSString *kStatusInvisibleTitle = @" Invisible";
static NSString *kStatusOfflineTitle = @" Offline";

static NSString *kStatusOnlineImgName = @"status-online";
static NSString *kStatusBusyImgName = @"status-busy";
static NSString *kStatusAwayImgName = @"status-away";
static NSString *kStatusInvisibleImgName = @"status-invisible";
static NSString *kStatusOfflineImgName = @"status-offline";

static NSString *kItemMenuTakePhoto = @"Take photo";
static NSString *kItemMenuChooseExistingPhoto = @"Choose existing photo";
static NSString *kItemMenuDelete = @"Delete";

static NSString *kMaleString = @"Male";
static NSString *kFemaleString = @"Female";

static const int kNickNameMinLength = 3; //minimal count symbols in nickname text
static const int kNickNameMaxLength =32;

@interface SCSBProfileViewController () <MAKDropDownMenuDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SCModelAbstarctDelegate>
@end

typedef NS_ENUM(NSInteger, SCAlertType)  {
    SCAlertTypeEmptyNick = 0,
    SCAlertTypeThreeSymbols
};

@implementation SCSBProfileViewController
{
    NSArray *_titlesStatus;
    
    NSIndexPath *_selectedCellIndexPath;
    MAKDropDownMenu *dropDownMenu;
    TransitionDelegate *_transitionController;

//    UIImagePickerController *_photoPicker;
//    UIImagePickerController *_mediaLibraryPicker;
    
    NSString *_tempText;
    NSCharacterSet *_validationCharacters;
    UIView *_helpView;
    BOOL _isHelpViewShow;
    NSTimer *_hideViewTimer;
    
    CGFloat _currentOffset;
    CGRect _scrollViewRectNormal;
    NSDate *_birthdateTemp;

    BOOL _keyboardIsShow;
    SCAlertType _alertTypeActive;
    
    BOOL _needLoadPhoto;
    UIColor *_nickNameTextFieldColor;
    
    UIView *_interfaceInteractionDissableView;
    UIEdgeInsets _tableViewSeparatorEdgeInsets;
}

//MARK: View life
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupTabBarAndNavigation];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _nickNameTextField.delegate = self;
    _nickNameTextFieldColor = SKCOLOR_NavBarTitleFont;
    
    _titlesStatus = @[kStatusOnlineTitle, kStatusBusyTitle, kStatusAwayTitle, kStatusInvisibleTitle];
    
    _transitionController = [[TransitionDelegate alloc] init];
    
    [self setupbackDatePickerView];

    _birthdateTemp = _settings.modelProfile.contactItem.contactBirthdate;

    _keyboardIsShow = NO;
    
    _needLoadPhoto = YES;
    _tableViewSeparatorEdgeInsets = UIEdgeInsetsMake(0, kUIMarge, 0, kUIMarge);
    
    _validationCharacters = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-."];
    
    _isHelpViewShow = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupTabBarAndNavigation];
    
    if (_selectedCellIndexPath) {
        [_tableView deselectRowAtIndexPath:_selectedCellIndexPath animated:YES];
    }
    
    if (_needLoadPhoto) {
        [self downloadPhoto];
        _needLoadPhoto = NO;
    }
    
    [self setupRecognizer];
    [self setupUI];
    
    [self changeStatus:_settings.modelProfile.contactItem.contactStatus onServer:NO];
    
    if (_settings.modelProfile.contactItem.contactNikName.length == 0 ||
        _settings.modelProfile.contactItem.contactNikName == nil) {
        _nickNameTextField.text = @"";
        [_nickNameTextField becomeFirstResponder];
        _isNickNameRight = NO;
        
        
    } else {
        _nickNameTextField.text = _settings.modelProfile.contactItem.contactNikName;
        _isNickNameRight = YES;
    }
    
    _settings.modelProfile.delegateChangeField = self;
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_hideViewTimer invalidate];
}

- (void) dealloc
{
    [_hideViewTimer invalidate];
    [_settings.modelProfile.contactItem delDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//MARK: Setup
- (void) setupTabBarAndNavigation
{
    self.tabBarController.navigationItem.titleView = nil;
    self.tabBarController.title = @"Profile";
    
    self.tabBarController.navigationItem.title = @"My Profile";
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    //Nav Title
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
}

- (void) setupbackDatePickerView
{
    _backDatePickerView.backgroundColor = [UIColor grayColor];
    _backDatePickerView.frame = self.view.frame;
    _backDatePickerView.alpha = 0.5;
    [self.view sendSubviewToBack:_backDatePickerView];
}

- (void) setupRecognizer
{
    UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoTap:)];
    tapPhoto.delegate = self;
    _fotoImage.userInteractionEnabled = YES;
    [self.fotoImage addGestureRecognizer:tapPhoto];
}

- (void) setupUI
{
    self.tableView.backgroundColor = SKCOLOR_TabBarBg;
    self.view.backgroundColor = SKCOLOR_TabBarBg;
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height);
    _scrollView.scrollEnabled = NO;
    _scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    _scrollViewRectNormal = CGRectMake(0, 0,
                                       SZ_SCREEN_WIDTH,
                                       self.view.frame.size.height - SZ_TAB_BAR_HEIGHT);
    
    self.scrollView.frame = _scrollViewRectNormal;
    
    _fotoBluerImage.frame = CGRectMake(0, 0, SZ_SCREEN_WIDTH, 110);
    _nickNameBackgoundView.frame = CGRectMake(0,
                                              _fotoBluerImage.frame.origin.y + _fotoBluerImage.frame.size.height,
                                              SZ_SCREEN_WIDTH,
                                              44);
    _nickNameBackgoundView.backgroundColor = SKCOLOR_NavBarBg;
    
    _tableView.frame = CGRectMake(0, _nickNameBackgoundView.frame.origin.y + _nickNameBackgoundView.frame.size.height,
                                  SZ_SCREEN_WIDTH,
                                  _scrollView.frame.size.height - (_nickNameBackgoundView.frame.origin.y + _nickNameBackgoundView.frame.size.height));
    
    //Nickname area
    NSString *stringMaxLenght = @"";
    for (NSString *statusText in _titlesStatus) {
        if (stringMaxLenght.length < statusText.length) {
            stringMaxLenght = statusText;
        }
    }
    
    _statusTitleLabel.font = [UIFont fontWithName:FT_FontNameRegular size: 17];
    SCLabel *tempLabel = _statusTitleLabel;
    tempLabel.text = stringMaxLenght;
    CGSize statusTitleLabelSize = [self labelRealSizeIfLabel:tempLabel
                                                     maxSize:CGSizeMake(_nickNameBackgoundView.frame.size.width, _nickNameBackgoundView.frame.size.height)];
    
    _statusTitleLabel.frame = CGRectMake(_nickNameBackgoundView.frame.size.width - statusTitleLabelSize.width - kUIMarge,
                                        0,
                                        statusTitleLabelSize.width,
                                        _nickNameBackgoundView.frame.size.height);
    
    CGFloat kStatusImageViewSize = 12;
    _statusImageView.frame = CGRectMake(_statusTitleLabel.frame.origin.x - kStatusImageViewSize - 5,
                                       _nickNameBackgoundView.bounds.size.height/2 - kStatusImageViewSize/2 + 1,
                                        kStatusImageViewSize,
                                        kStatusImageViewSize);
    
    _statusButton.frame = CGRectMake(_statusImageView.frame.origin.x - 5,
                                     0,
                                     _nickNameBackgoundView.frame.size.width - _statusImageView.frame.origin.x - 5,
                                     _nickNameBackgoundView.frame.size.height);
    
    _atLabel.text = @"@";
    _atLabel.textAlignment = NSTextAlignmentLeft;
    CGSize atLabelSize = [self labelRealSizeIfLabel:_atLabel maxSize:CGSizeMake(_nickNameBackgoundView.frame.size.width, _nickNameBackgoundView.frame.size.height - 2)];
    _atLabel.frame = CGRectMake(_tableViewSeparatorEdgeInsets.left - 1, 0.0,
                                atLabelSize.width+1, _nickNameBackgoundView.frame.size.height - 2);
    
    _nickNameTextField.frame = CGRectMake(_atLabel.frame.origin.x + _atLabel.frame.size.width + 1,
                                          0,
                                          _statusImageView.frame.origin.x - kUIMarge - kUIMarge,
                                          _nickNameBackgoundView.frame.size.height);
    
    _nickNameTextField.font = [UIFont fontWithName:FT_FontNameRegular size:_nickNameTextField.font.pointSize];

    NSAttributedString *attrTextField = [[NSAttributedString alloc] initWithString:_nickNameTextField.text
                                                                        attributes:@{NSForegroundColorAttributeName: _nickNameTextFieldColor}];
    _nickNameTextField.attributedText = attrTextField;
    
    NSAttributedString *attrPlaceholder = [[NSAttributedString alloc] initWithString:@"nickname"
                                                                          attributes:@{NSForegroundColorAttributeName: SKCOLOR_TableTextFieldPlchldrFont}];
    _nickNameTextField.attributedPlaceholder = attrPlaceholder;
}

- (CGSize) labelRealSizeIfLabel:(SCLabel *) label maxSize:(CGSize) maxSize
{
    return [label.text boundingRectWithSize:maxSize
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:label.font.fontName size:label.font.pointSize]}
                                    context:nil].size;
}

- (void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    [_settings.modelProfile.contactItem addDelegate:self];
}

- (void) setupInitialsForPhoto
{
    NSString* initialsForPhotos = [NSString new];
    
    if (!_settings.modelProfile.contactItem.contactLastName.length==0 ||
        !_settings.modelProfile.contactItem.contactFirstName.length==0) {
        
        NSString* fullName = [NSString new];
        
        if (_settings.modelProfile.contactItem.contactLastName.length > 0) {
            fullName = [NSString stringWithFormat:@"%@ ",_settings.modelProfile.contactItem.contactLastName];
        }
        
        if (_settings.modelProfile.contactItem.contactFirstName.length > 0) {
            fullName = [fullName stringByAppendingString:_settings.modelProfile.contactItem.contactFirstName];
        }
        
        initialsForPhotos = fullName;
        
    } else if (!_settings.modelProfile.contactItem.contactNikName.length == 0) {
        initialsForPhotos  = _settings.modelProfile.contactItem.contactNikName;
        
    } else  if (!_settings.modelProfile.contactItem.contactPhone.length == 0) {
        initialsForPhotos = _settings.modelProfile.contactItem.contactPhone;
        
    } else {
        initialsForPhotos = _settings.modelProfile.contactItem.contactEmail;
    }
    
    //[_fotoImage setImageWithString:initialsForPhotos color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
    [_fotoImage setImageWithString:initialsForPhotos color:SKCOLOR_ViewNotAvatarBg circular:YES];
}

- (UIImage *)blurWithCoreImage:(UIImage *)sourceImage
{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not
    // look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@30 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    
    // Invert image coordinates
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.view.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, self.view.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
    CGContextFillRect(outputContext, self.view.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}

//MARK: Table View
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat heightRow = 44;
    if (indexPath.row == 8) { //empty cell
        heightRow = 5;
    }
    return heightRow;
}

-(void)viewDidLayoutSubviews
{
    // iOS 7
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:_tableViewSeparatorEdgeInsets];
    }
    // iOS 8
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:_tableViewSeparatorEdgeInsets];
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = indexPath.row;
    
    if (row == 0 || row == 1 || row == 2) {
        
        static NSString *cellIdentifier = @"TextFieldCell";
        SCSBTextFieldTableViewCell *cell = (SCSBTextFieldTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.settings = _settings;
        cell.edgeInsets = _tableViewSeparatorEdgeInsets;
        cell.delegate = self;
        cell.tag = row;
        
        switch (row) {
            case 0: cell.typeFieldProfile = SCTypeFieldProfileAbout; break;
            case 1: cell.typeFieldProfile = SCTypeFieldProfileFirstName; break;
            case 2: cell.typeFieldProfile = SCTypeFieldProfileLastName; break;
            default:  break;
        }
        
        return cell;
        
    } else if (row == 3) {
        
        static NSString *cellIdentifier = @"GenderCell";
        SCSBGenderTableViewCell *cell = (SCSBGenderTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.settings = _settings;
        cell.edgeInsets = _tableViewSeparatorEdgeInsets;
        cell.delegate = self;
        cell.tag = row;
        
        return cell;
        
    } else if (row == 4) {
        
        static NSString *cellIdentifier = @"BirthdayCell";
        SCSBBirthdayTableViewCell *cell = (SCSBBirthdayTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.settings = _settings;
        cell.birthdate = _birthdateTemp;
        cell.edgeInsets = _tableViewSeparatorEdgeInsets;
        cell.tag = row;

        return cell;
        
    } else if (row == 5) {
        
        static NSString *cellIdentifier = @"AddressCell";
        SCSBAddressTableViewCell *cell = (SCSBAddressTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.edgeInsets = _tableViewSeparatorEdgeInsets;
        cell.settings = _settings;
        cell.tag = row;
        
        return cell;
        
    } else if (row == 6) {
        
        static NSString *cellIdentifier = @"AlternativeLoginCell";
        SCSBAlternativeLoginTableViewCell *cell = (SCSBAlternativeLoginTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.edgeInsets = _tableViewSeparatorEdgeInsets;
        cell.settings = _settings;
        cell.tag = row;
        
        return cell;
    
    } else if (row == 7) {
        
        static NSString *cellIdentifier = @"SettingsCell";
        SCSBSettingsTableViewCell *cell = (SCSBSettingsTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.edgeInsets = _tableViewSeparatorEdgeInsets;
        cell.settings = _settings;
        cell.tag = row;
        
        return cell;
        
    } else if (row == 8) {
        
        static NSString *cellIdentifier = @"EmptyCell";
        UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.contentView.backgroundColor = SKCOLOR_TabBarBg;
        cell.tag = row;
        
        return cell;
        
    } else if (row == 9) {
        
        static NSString *cellIdentifier = @"LogoutCell";
        SCSBLogoutTableViewCell *cell = (SCSBLogoutTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.edgeInsets = _tableViewSeparatorEdgeInsets;
        cell.settings = _settings;
        cell.tag = row;
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedCellIndexPath = indexPath;
    
    switch (indexPath.row) {
        case 4://Birhday
        {
            if (_isNickNameRight) {
                [self presentModalViewControllerDatePicker];
            } else {
                [_tableView deselectRowAtIndexPath:_selectedCellIndexPath animated:YES];
                [self checkNickNameTextField];
            }
        }
            break;
            
        case 7: //Settings
        {
            _selectedCellIndexPath = indexPath;
            if (_isNickNameRight) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SCSettings" bundle:nil];
                SCSBSettingsViewController* settingsVC =[storyboard instantiateViewControllerWithIdentifier:@"Settings"];
                settingsVC.settings = _settings;
                [self.navigationController pushViewController:settingsVC animated:YES];
    
            } else {
                [_tableView deselectRowAtIndexPath:_selectedCellIndexPath animated:YES];
            }
        }
            break;
            
        default:
            break;
    }
}

//- (void) reloadBirthdateCell
//{
//    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:4 inSection:0];
//    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
//    [_tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationAutomatic];
//}

- (void) reloadCellForField: (SCFieldType)textFieldType
{
    NSInteger controlValue = 1000;
    NSInteger rowForReloadIndex = controlValue;
    
    switch (textFieldType) {
        case SCFieldTypeAbout:rowForReloadIndex = 0;break;
        case SCFieldTypeFirstName:rowForReloadIndex = 1;break;
        case SCFieldTypeLastName:rowForReloadIndex = 2;break;
        case SCFieldTypeSex:rowForReloadIndex = 3;break;
        case SCFieldTypeBirthdate:rowForReloadIndex = 4;break;
        default: break;
    }
    
    if (rowForReloadIndex != controlValue) {
        NSIndexPath *rowForReload = [NSIndexPath indexPathForRow:rowForReloadIndex inSection:0];
        NSArray* rowsForReload = [NSArray arrayWithObjects:rowForReload, nil];
        [_tableView reloadRowsAtIndexPaths:rowsForReload withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

//MARK: Segue
- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if (_isNickNameRight) {
        return YES;
    } else {
        [self checkNickNameTextField];
        return NO;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"AddressSegue"]) {
        SCAddressTableViewController *addressVC = segue.destinationViewController;
        addressVC.settings = _settings;
        
    } else if ([segue.identifier isEqualToString:@"AlternativeLoginSegue"]) {
        SCAlternativeLoginViewController *alternativeAddressVC = segue.destinationViewController;
        alternativeAddressVC.settings = _settings;
        
    } else if ([segue.identifier isEqualToString:@"LogoutSegue"]) {
        SCLogoutTableViewController *logoutTVC = segue.destinationViewController;
        logoutTVC.settings = _settings;
    }
}

//MARK: Action
- (IBAction)statusButtonPressed:(id)sender
{
    if (_isNickNameRight) {
        
        NSArray * titles = _titlesStatus;//@[kStatusOnline, kStatusBusy, kStatusAway, kStatusInvisible];
        
        NSArray *images = @[[_settings.skinManager getImageForKeyX:kStatusOnlineImgName],
                            [_settings.skinManager getImageForKeyX:kStatusBusyImgName],
                            [_settings.skinManager getImageForKeyX:kStatusAwayImgName],
                            [_settings.skinManager getImageForKeyX:kStatusInvisibleImgName]];
        
        [self creatDropDownMenuWithTitles:titles andImages:images];
        [self menuButtonDidClick];
        
    } else {
        [self checkNickNameTextField];
    }
    
}

- (void) presentModalViewControllerDatePicker
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SCProfile" bundle:nil];
    SCDatePickerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"DatePicker"];
    vc.view.backgroundColor = [UIColor clearColor];
    vc.delegate = self;
    vc.birthdate = _birthdateTemp;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    int iOSVersion = [[[UIDevice currentDevice] systemVersion] intValue];
    if (iOSVersion == 7) {
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [vc setTransitioningDelegate:_transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        
    } else if (iOSVersion >= 8 ) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
    }

    [self.view bringSubviewToFront:_backDatePickerView];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _backDatePickerView.alpha = 0.5;
                     }
                     completion:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:vc animated:YES completion:nil];
    });
}

//MARK: Status
-(void)changeStatus:(SCUserStatus) status onServer: (BOOL) onServer {
    
    switch (status) {
            
        case SCUserStatusAway:
        {
            _statusTitleLabel.text = kStatusAwayTitle;
            _statusImageView.image = [_settings.skinManager getImageForKeyX:kStatusAwayImgName];
        }
            break;
            
        case SCUserStatusBusy:
        {
            _statusTitleLabel.text = kStatusBusyTitle;
            _statusImageView.image = [_settings.skinManager getImageForKeyX:kStatusBusyImgName];
        }
            break;
            
        case SCUserStatusInvisible:
        {
            _statusTitleLabel.text = kStatusInvisibleTitle;
            _statusImageView.image = [_settings.skinManager getImageForKeyX:kStatusInvisibleImgName];
        }
            break;
            
        case SCUserStatusOffline:
        {
            _statusTitleLabel.text = kStatusOfflineTitle;
            _statusImageView.image = [_settings.skinManager getImageForKeyX:kStatusOfflineImgName];
        }
            break;
            
        case SCUserStatusOnline:
        {
            _statusTitleLabel.text = kStatusOnlineTitle;
            _statusImageView.image = [_settings.skinManager getImageForKeyX:kStatusOnlineImgName];
        }
            break;
            
        default:
            break;
    }
    
    if (onServer) {
        [_settings.modelProfile changeStatusWithEnum:status];
    }
}

//MARK: DropDownMenu
-(void)creatDropDownMenuWithTitles:(NSArray*)titles andImages:(NSArray*)images {
    
    MAKDropDownMenu *menu = [[MAKDropDownMenu alloc] initWithFrame:(CGRect){0,
        CGRectGetMaxY(self.navigationController.navigationBar.frame),
        self.view.bounds.size.width,
        self.navigationController.view.bounds.size.height - CGRectGetMaxY(self.navigationController.navigationBar.frame)}];
    menu.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    menu.backgroundColor = [UIColor colorWithWhite:.3f alpha:.3f];
    menu.tintColor = [UIColor colorWithWhite:.1f alpha:1.f];
    menu.buttonBackgroundColor = [UIColor whiteColor];
    menu.titles=titles;
    menu.separatorHeight = 1 / [UIScreen mainScreen].scale;
    menu.layer.masksToBounds = YES;
    menu.buttonsInsets = UIEdgeInsetsMake(1 / [UIScreen mainScreen].scale, 0, 0, 0);
    menu.delegate = self;
    menu.alpha=0.95;
    [self.navigationController.view addSubview:menu];
    dropDownMenu = menu;
    
    if (images){
        int counter;
        counter=0;
        for (UIButton * button in dropDownMenu.subviews)
        {
            [button setImage:[images objectAtIndex:counter] forState:UIControlStateNormal];
            counter++;
        }
    }
}

- (void)dropDownMenu:(MAKDropDownMenu *)menu itemDidSelect:(NSUInteger)itemIndex {
    
    UIButton * button = [menu.subviews objectAtIndex:itemIndex];
    NSString * menuTitle = button.titleLabel.text;
    
    if ([menuTitle isEqualToString:kStatusOnlineTitle]) {
        if (_settings.modelProfile.contactItem.contactStatus != SCUserStatusOnline) {
            [self changeStatus:SCUserStatusOnline onServer:YES];
        }
        
    } else if ([menuTitle isEqualToString:kStatusBusyTitle]) {
        if (_settings.modelProfile.contactItem.contactStatus != SCUserStatusBusy) {
            [self changeStatus:SCUserStatusBusy onServer:YES];
        }
        
    } else if ([menuTitle isEqualToString:kStatusAwayTitle]) {
        if (_settings.modelProfile.contactItem.contactStatus != SCUserStatusAway) {
            [self changeStatus:SCUserStatusAway onServer:YES];
        }
        
    } else if ([menuTitle isEqualToString:kStatusInvisibleTitle]) {
        if (_settings.modelProfile.contactItem.contactStatus != SCUserStatusInvisible) {
            [self changeStatus:SCUserStatusInvisible onServer:YES];
        }
    }
    
    if ([menuTitle isEqualToString:kItemMenuTakePhoto]) {
        [self presentPicker:UIImagePickerControllerSourceTypeCamera];
        
    } else if ([menuTitle isEqualToString:kItemMenuChooseExistingPhoto]) {
        [self presentPicker:UIImagePickerControllerSourceTypePhotoLibrary];
        
    } else if ([menuTitle isEqualToString:kItemMenuDelete]) {
        [self deletePhoto];
    }
    
    [self closeMenu];
}

- (void)dropDownMenuDidTapOutsideOfItem:(MAKDropDownMenu *)menu {
    [self closeMenu];
}

- (void)menuButtonDidClick {
    
    if (dropDownMenu.isOpen) {
        [self closeMenu];
        
    } else {
        [self openMenu];
    }
}

- (void)closeMenu {
    [dropDownMenu closeAnimated:YES];
}

- (void)openMenu {
    if (_keyboardIsShow) {
        [self.view endEditing:YES]; //hide keyboard
    }
    [dropDownMenu openAnimated:YES];
}

//MARK: Photo
- (void) photoTap: (UITapGestureRecognizer*)recognizer
{
    if (_isNickNameRight) {
        NSMutableArray * titlesMenu = [[NSMutableArray alloc]init];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [titlesMenu addObject:kItemMenuTakePhoto];
        }
        
        if([_settings.modelProfile.contactItem.netFile isNotEmptyPath])
        {
            [titlesMenu addObjectsFromArray: @[kItemMenuChooseExistingPhoto, kItemMenuDelete]];
        } else {
            [titlesMenu addObject:kItemMenuChooseExistingPhoto];
        }
        
        [self creatDropDownMenuWithTitles:titlesMenu andImages:nil];
        [self menuButtonDidClick];
        
    } else {
        [self checkNickNameTextField];
    }
}

- (void) presentPicker:(UIImagePickerControllerSourceType) pickerType
{
    switch (pickerType) {
        case UIImagePickerControllerSourceTypePhotoLibrary:
        {
            if (!_mediaLibraryPicker) {
            _mediaLibraryPicker = [[UIImagePickerController alloc] init];
            _mediaLibraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            _mediaLibraryPicker.delegate = self;
            _mediaLibraryPicker.allowsEditing = YES;
            _mediaLibraryPicker.modalPresentationStyle = UIModalPresentationFullScreen;
            _mediaLibraryPicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, (NSString *)kUTTypeMovie,nil];
            }
            
            [self presentViewController:_mediaLibraryPicker animated:YES completion:nil];
        }
            break;
            
        case UIImagePickerControllerSourceTypeCamera:
        {
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            
            if (authStatus == AVAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showCamera];
                });
                
            } else {
                if (authStatus == AVAuthorizationStatusNotDetermined) {
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        if (granted) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self showCamera];
                            });
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self cameraError];
                            });
                        }
                    }];
                } else {
                    [self cameraError];
                }
            }
        }
            break;
            
        case UIImagePickerControllerSourceTypeSavedPhotosAlbum:
        {
            //nothing
        }
            break;
        default: {
            break;
        }
    }
}

- (void) showCamera
{
    if (!_photoPicker) {
        _photoPicker = [[UIImagePickerController alloc] init];
        _photoPicker.delegate = self;
        _photoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    }
    
    self.scrollView.scrollEnabled = NO;
    
    [self presentViewController:_photoPicker animated:YES completion:nil];
}

- (void) cameraError
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Denied access to the camera"
                                                        message:@"To allow access, you need to: \n1. Open the settings of your device. \n2. Go to \"Privacy \". \n3. Click \"camera \". \n4. Allow access to the camera. \n5. Re-visit to the shooting mode."
                                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
}

- (UIImagePickerController *) loadImagePickerController
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self; 
    pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    pickerController.modalPresentationStyle = UIModalPresentationFullScreen;
    pickerController.allowsEditing = YES;
    
    pickerController.mediaTypes = @[(NSString *) kUTTypeImage];
    pickerController.navigationBarHidden = YES;
    
    //Camera controls
    /*
    pickerController.showsCameraControls = NO;
    pickerController.allowsEditing = NO;
    pickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    pickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    
    SCOverlayView *overlayView = [[[NSBundle mainBundle] loadNibNamed:@"SCOverlayView" owner:self options:nil] firstObject];
    overlayView.delegate = self;
    overlayView.videoMaximumDuration = kVideoMaximumDuration;
    overlayView.currentFlashMode = pickerController.cameraFlashMode;
    overlayView.currentMode = [self modeOverViewBy:pickerController.mediaTypes];
    overlayView.frame = CGRectMake(0, 0, SZ_SCREEN_WIDTH, SZ_SCREEN_HEIGHT);
    
    pickerController.cameraOverlayView = overlayView;
    
    pickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0.0,50.0);
    */
    return pickerController;
}


- (void) uploadPhoto: (UIImage *) imageUpload
{
    //obtaining saving path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:@"newProfileFoto.png"];
    NSLog(@"imagePath =%@", imagePath);
    
    //extracting image from the picker and saving it
    [UIImagePNGRepresentation(imageUpload) writeToFile:imagePath atomically:YES];
    
    _settings.modelProfile.contactItem.netFile.localPath = imagePath;
    
    [_settings.modelProfile.contactItem.netFile sendWithCompletionblock:^(SCNetFileComletionType type, id result)
    {
        switch (type) {
            case SCNetFileComletionTypeStatus:
            {
                FTLog(@"SCNetFileComletionTypeStatus");
            }
                break;
                
            case SCNetFileComletionTypeProgress:
            {
                [_fotoImage setProgressPercent:[result doubleValue]];
                FTLog(@"result upload =%f", [result doubleValue]);
            }
                break;
            
            case SCNetFileComletionTypeError:
            {
                FTLog(@"Can't upload file to server");
                [_fotoImage setProgressPercent:[result doubleValue]];
            }
                break;
                
            case SCNetFileComletionTypeCompleet:
            {
                [_fotoImage endProgress];
                if([result isKindOfClass:[SCNetFile class]])
                {
                    
                    FTLog(@"SCNetFileComletionTypeCompleet");
                    _fotoImage.image = [[(SCNetFile*)result imageForSize:_fotoImage.frame.size] cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
                    _fotoBluerImage.image = [(SCNetFile*)result blureImage:self.view.frame];
                }
            }
                break;
                
            default:
                break;
        }
    }];
    
    /*
     SCNetFileComletionTypeStatus,
     SCNetFileComletionTypeProgress,
     SCNetFileComletionTypeError,
     SCNetFileComletionTypeCompleet,
     */
    
    //[_settings.modelProfile.contactItem.netFile
    
    /*
    
    UIImage *oldImage = _fotoImage.image;
    
    __block UIImage* img = imageUpload;
    NSData* imgData = UIImageJPEGRepresentation(img, 0.75f);
    //FTLog(@"image size %lu",(unsigned long)imgData.length);
    
    _fotoImage.image = [img cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
    //_fotoImage.image = [[_settings.modelProfile.contactItem.netFile imageForSize:_fotoImage.frame.size] cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
    
    [_settings.httpManager fileUpload:imgData
                             fileType:@"image/jpeg"
                       progressUpload:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
                                     {
                                         [_fotoImage setProgress:totalBytesRead totalBytesExpectedToRead:totalBytesExpectedToRead];
                                     }
                            onSuccess:^(NSDictionary *imageInfo)
                                     {
                                         [_fotoImage endProgress];
                                         
                                         NSString *imagePUUID = imageInfo[@"uuid"];
                                         [_settings.modelProfile setField:SCFieldTypeImageid forValue:imagePUUID];
                                         
                                         _fotoBluerImage.image = [self blurWithCoreImage:img];
                                         //_fotoImage.image = [[(SCNetFile*)result imageForSize:_fotoImage.frame.size] cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
                                         //_fotoBluerImage.image = [(SCNetFile*)result blureImage:self.view.frame];
                                         //_fotoBluerImage.image = [_settings.modelProfile.contactItem.netFile blureImage:img.size];
                                         
                                     } onFailure:^(NSError *error, NSInteger statusCode) {
                                         //FTLog(@"Fail upload file");
                                         
                                         [_fotoImage endProgress];
                                         _fotoImage.image = [oldImage cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
                                     }
     ];
     */
}


- (void) downloadPhoto
{
    //FTLog(@"download file uuid = %@", _settings.modelProfile.contactItem.netFile.netPath);
    [self setupInitialsForPhoto];
    [_settings.modelProfile.contactItem.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result)
    {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
            {
                //FTLog(@"SCNetFileComletionTypeStatus");
            }
                break;
                
            case SCNetFileComletionTypeCompleet:
            {
                [_fotoImage endProgress];
                if([result isKindOfClass:[SCNetFile class]])
                {
                    _fotoImage.image = [[(SCNetFile*)result imageForSize:_fotoImage.frame.size] cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
                    _fotoBluerImage.image = [(SCNetFile*)result blureImage:self.view.frame];
                }
            }
                break;
                
            case SCNetFileComletionTypeProgress:
                [_fotoImage setProgressPercent:[result doubleValue]];
                break;
                
            default:
                //FTLog(@"default case");
                break;
        }
    }];
}

- (void) deletePhoto
{
         [_settings.modelProfile removeProfileField:SCFieldTypeImageid];
         [self setupInitialsForPhoto];
         _fotoBluerImage.image =nil;
}

//MARK: UIImagePickerControllerDelegate
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.image"]) {
        
        [self uploadPhoto:info[UIImagePickerControllerOriginalImage]];
        
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"Incorrect file type"
                                   delegate:nil
                          cancelButtonTitle:@"ОК"
                          otherButtonTitles:nil] show];
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        [_mediaLibraryPicker dismissViewControllerAnimated:YES completion:NULL];
        
    } else if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        [_photoPicker dismissViewControllerAnimated:YES completion:NULL];
    }
}

//MARK: UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _tempText = _nickNameTextField.text;
    
    if (textField.text.length < kNickNameMinLength) {
        [self setNickNameTextFieldRedColorWithText:textField.text];
        _isNickNameRight = NO;
        
    } else {
        [self setNickNameTextFieldNormalColorWithText:textField.text];
        _isNickNameRight = YES;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL validString = YES;
    
    for (int i = 0; i < string.length; i++)
    {
        unichar c = [string characterAtIndex:i];
        if (![_validationCharacters characterIsMember:c]) {
            validString = NO;
            break;
        }
    }
    
    if (validString)
    {
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (resultText.length < kNickNameMinLength) {
            textField.textColor = SKCOLOR_ChatListRedLabel;
            _isNickNameRight = NO;
            
        } else {
            textField.textColor = _nickNameTextFieldColor;
            _isNickNameRight = YES;
        }
        return resultText.length <= kNickNameMaxLength;
    }
    else
    {
        [self showHelpValidCharForNickname];
        return NO;
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (_isNickNameRight) {
        return YES;
    } else {
        [self checkNickNameTextField];
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (![_tempText isEqualToString:_nickNameTextField.text]) {
        
        if (_nickNameTextField.text.length == 0) {
            [self showAlert:SCAlertTypeEmptyNick];
            
        } else if (_nickNameTextField.text.length < kNickNameMinLength) {
            [self showAlert:SCAlertTypeThreeSymbols];
            
        } else {
            [self showActivity];
            [_settings.modelProfile setField:SCFieldTypeNickName forValue:_nickNameTextField.text];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) setNickNameTextFieldNormalColorWithText: (NSString*)textFieldText
{
    NSAttributedString *attrTextField = [[NSAttributedString alloc] initWithString:textFieldText
                                                                        attributes:@{NSForegroundColorAttributeName: _nickNameTextFieldColor}];
    _nickNameTextField.attributedText = attrTextField;
}

- (void) setNickNameTextFieldRedColorWithText: (NSString*)textFieldText
{
    NSAttributedString *attrTextField = [[NSAttributedString alloc] initWithString:textFieldText
                                                                        attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
    _nickNameTextField.attributedText = attrTextField;
}

//MARK: Check NickName and Alert
- (void) checkNickNameTextField
{
    if (_nickNameTextField.text.length == 0) {
        [self showAlert:SCAlertTypeEmptyNick];
        
    } else if (_nickNameTextField.text.length < kNickNameMinLength) {
        [self showAlert:SCAlertTypeThreeSymbols];
    }
}

- (void) showAlert: (SCAlertType) alertType
{
    _alertTypeActive = alertType;
    
    NSString *alertTitle;
    NSString *alertMessage;
    NSString *alertCancelTitle;
    
    if (_alertTypeActive == SCAlertTypeEmptyNick) {
        alertTitle = @"A nickname field cannot be empty and it must consist of 3 or more symbols.";
        alertMessage = nil;
        
        if (_tempText.length == 0) {
            alertCancelTitle = @"Return to editing";
        } else {
            alertCancelTitle = [[NSString alloc] initWithFormat:@"Edit '%@'", _tempText];
        }
        
    } else if (_alertTypeActive == SCAlertTypeThreeSymbols) {
        alertTitle = @"Attention!";
        alertMessage = @"Your nickname is too short. It must contain at least 3 symbols.";
        alertCancelTitle = @"Return to editing";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                    message:alertMessage
                                                   delegate:self
                                          cancelButtonTitle:alertCancelTitle
                                          otherButtonTitles:nil];
    alert.tag = 1;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (_alertTypeActive == SCAlertTypeEmptyNick) {
            [self setNickNameTextFieldNormalColorWithText:_tempText];
            [_nickNameTextField becomeFirstResponder];
            
        } else if (_alertTypeActive == SCAlertTypeThreeSymbols) {
            [_nickNameTextField becomeFirstResponder];
        }
        
    } else if (alertView.tag == 2) {
        
        [_nickNameTextField becomeFirstResponder];
    }
}

//MARK: NickName valid char help view

- (UIView *) createHelpViewWithFrame: (CGRect) frame
{
    //View
    UIView *baseView = [[UIView alloc] initWithFrame:frame];
    baseView.backgroundColor = [SKCOLOR_SegmentControlV1FontInactive colorWithAlphaComponent:0.7];
    
    UIColor *borderColor = SKCOLOR_NavBarBg;
    baseView.layer.borderColor = borderColor.CGColor;
    baseView.layer.borderWidth = 1;
    
    //Label
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    SCLabel *label = [[SCLabel alloc] initWithFrame:CGRectMake(edgeInsets.left, edgeInsets.top,
                                                               frame.size.width - edgeInsets.left - edgeInsets.right,
                                                               frame.size.height - edgeInsets.top - edgeInsets.bottom)];
    
    NSString *labelText = [NSString stringWithFormat:@"A Nickname can contain 'A-Z a-z 0-9 - _ .' only.\n\nA Nickname must be between %i and %i characters in length.", kNickNameMinLength, kNickNameMaxLength];
    label.text = labelText;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = SKCOLOR_NavBarTitleFont;
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 0;
    label.adjustsFontSizeToFitWidth = YES;
    
    [baseView addSubview:label];
    
    return baseView;
}

- (void) showHelpValidCharForNickname
{
    if (!_isHelpViewShow) {
        
        CGSize viewSize = CGSizeMake(SZ_SCREEN_WIDTH - _tableViewSeparatorEdgeInsets.left * 2, 44 * 2.5);
        CGRect startFrameView = CGRectMake(_tableViewSeparatorEdgeInsets.left, - viewSize.height,
                                           viewSize.width, viewSize.height);
        
        if (!_helpView) {
            _helpView = [self createHelpViewWithFrame:startFrameView];
            [self.view addSubview:_helpView];
        }
        
        _helpView.frame = startFrameView;
        _helpView.alpha = 1.0;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             _helpView.frame = CGRectMake(_helpView.frame.origin.x,
                                                          _tableViewSeparatorEdgeInsets.left,
                                                          _helpView.frame.size.width,
                                                          _helpView.frame.size.height);
                         }
                         completion:^(BOOL finished) {
                             _hideViewTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                                               target:self
                                                                             selector:@selector(hideHelpValidCharForNickname)
                                                                             userInfo:nil
                                                                              repeats:NO];
                         }
         ];
        _isHelpViewShow = YES;
    }
}

- (void) hideHelpValidCharForNickname
{
    if (_isHelpViewShow) {
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             _helpView.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             [_hideViewTimer invalidate];
                         }
         ];
        _isHelpViewShow = NO;
    }
}

//MARK: Keyboard
- (void) showKeyboard
{
    if ((IS_IPHONE_4_AND_OLDER || IS_IPHONE_5) && !_keyboardIsShow) {
        _scrollView.contentSize = CGSizeMake(SZ_SCREEN_WIDTH,
                                             _scrollView.frame.size.height + _fotoBluerImage.frame.size.height + _nickNameBackgoundView.frame.size.height);
        
        _currentOffset = _scrollView.contentOffset.y;
        CGFloat newOffset = _currentOffset - _fotoBluerImage.frame.size.height;
        [_scrollView setContentOffset:CGPointMake(0.0, - newOffset) animated:YES];
    }
    _keyboardIsShow = YES;
}

- (void) hideKeyboard
{
    if (IS_IPHONE_4_AND_OLDER || IS_IPHONE_5 ) {
        [UIView animateWithDuration:0.3 animations:^{
            _scrollView.contentSize = CGSizeMake(_scrollViewRectNormal.size.width,
                                                 _scrollViewRectNormal.size.height);
        }];
        [_scrollView setContentOffset:CGPointMake(0.0, _currentOffset) animated:YES];
    }
    _keyboardIsShow = NO;
}

//MARK: SCTextFieldCellProtocol
- (void) changeTextValue:(NSString *)textValue for:(SCTypeFieldProfile)typeFieldProfile
{
    switch (typeFieldProfile) {
        case SCTypeFieldProfileAbout:
        {
            if (textValue) {
                [_settings.modelProfile setField:SCFieldTypeAbout forValue:textValue];
                
            } else {
                [_settings.modelProfile removeProfileField:SCFieldTypeAbout];
            }
        }
            break;
            
        case SCTypeFieldProfileFirstName:
        {
            if (textValue) {
                [_settings.modelProfile setField:SCFieldTypeFirstName forValue:textValue];
            } else {
                [_settings.modelProfile removeProfileField:SCFieldTypeFirstName];
            }
        }
            break;
            
        case SCTypeFieldProfileLastName:
        {
            if (textValue) {
                [_settings.modelProfile setField:SCFieldTypeLastName forValue:textValue];
                
            } else {
                [_settings.modelProfile removeProfileField:SCFieldTypeLastName];
            }
        }
            break;
            
        default:
            break;
    }
}

//MARK: SCCellGenderProtocol
- (void) setupGender:(NSString *)gender
{
    if ([gender isEqualToString:kMaleString]) {
        [_settings.modelProfile setField:SCFieldTypeSex forValue:kMaleString];
        
    } else if ([gender isEqualToString:kFemaleString]) {
        [_settings.modelProfile setField:SCFieldTypeSex forValue:kFemaleString];
        
    } else {
        FTLog(@"Unknow gender");
    }
}

//MARK: SCDatePickerDelegate
- (void) setBirthdate:(NSDate *)date
{
    if (_birthdateTemp != date) {
        _birthdateTemp = date;
        
        if (date) {
            [_settings.modelProfile setField:SCFieldTypeBirthdate forValue:_birthdateTemp];
        } else {
            [_settings.modelProfile removeProfileField:SCFieldTypeBirthdate];
        }
        [self reloadCellForField:SCFieldTypeBirthdate];
    }
}

- (void) closeModalView
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         _backDatePickerView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self.view sendSubviewToBack:_backDatePickerView];
                     }];
}

//MARK: Activity
- (void) showActivity
{
    _interfaceInteractionDissableView = [[UIView alloc] initWithFrame:self.view.bounds];
    _interfaceInteractionDissableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_interfaceInteractionDissableView];
    
    [self tabbarIsActive:NO];
    
    [SVProgressHUD show];
}

- (void) hideActivity
{
    [SVProgressHUD dismiss];
    [_interfaceInteractionDissableView removeFromSuperview];
    _interfaceInteractionDissableView = nil;
    [self tabbarIsActive:YES];
}

//MARK: Tabbar
- (void) tabbarIsActive: (BOOL) isActive
{
    if (_delegate && [_delegate respondsToSelector:@selector(tabBarControllerInteractionEnable:)]) {
        [_delegate tabBarControllerInteractionEnable:isActive];
    }
}

//MARK: SCModelProfileProtocol
- (void) field:(SCFieldType)fieldType change:(SCFieldChange)fieldChange value:(id)fieldValue
{
    if (fieldType == SCFieldTypeNickName)
    {
        if (fieldChange == SCFieldChangeNickNameAlreadyExist)
        {
            [self hideActivity];
            
            if (fieldValue && [fieldValue isKindOfClass:[NSString class]])
            {
                NSString *alertTitle = [NSString stringWithFormat:@"'%@' already exists.", _nickNameTextField.text];
                NSString *alertCancelTitle = @"Return to editing";
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                                message:@"Write another nickname please."
                                                               delegate:self
                                                      cancelButtonTitle:alertCancelTitle
                                                      otherButtonTitles:nil];
                alert.tag = 2;
                [alert show];
            }
        }
        else if (fieldChange == SCFieldChangeIsSuccess)
        {
            [self hideActivity];
        }
        else if (fieldChange == SCFieldChangeNoSuccess)
        {
            [self hideActivity];
            
            NSString *nickNameCurrent = _nickNameTextField.text;
            
            if (fieldValue && [fieldValue isKindOfClass:[NSString class]]) {
                
                NSString *nickName = fieldValue;
                _nickNameTextField.text = nickName;
            }
            
            NSString *alertTitle = [NSString stringWithFormat:@"Can't save NickName"];
            NSString *alertCancelTitle = @"Return to editing";
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                            message:[NSString stringWithFormat:@"Can't save NickName '%@'", nickNameCurrent]
                                                           delegate:self
                                                  cancelButtonTitle:alertCancelTitle
                                                  otherButtonTitles:nil];
            alert.tag = 3;
            [alert show];
        }
    }
    else if (fieldType == SCFieldTypeImageid)
    {
        if (fieldChange == SCFieldChangeNoSuccess)
        {
            [self downloadPhoto];
        }
    }
    else
    {
        if (fieldType == SCFieldTypeBirthdate)
        {
            _birthdateTemp = _settings.modelProfile.contactItem.contactBirthdate;
        }
        [self reloadCellForField:fieldType];
    }
}

- (void) changeStatusCannot
{
    [self changeStatus:_settings.modelProfile.contactItem.contactStatus onServer:NO];
}

#pragma mark SCPBManagerDelegate <NSObject>

-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    switch (type)
    {
        case SCPBManagerParceTypeChangeStatus:
        case SCPBManagerParceTypeChangeStatusEvent:
            [self changeStatus:_settings.modelProfile.contactItem.contactStatus onServer:NO];
            break;
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;
    }
}

@end
