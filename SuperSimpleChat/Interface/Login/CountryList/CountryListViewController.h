//
//  CountryListViewController.h
//  Country List
//
//  Created by Pradyumna Doddala on 18/12/13.
//  Copyright (c) 2013 Pradyumna Doddala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"
@class SCSkinManager;

@protocol CountryListViewDelegate <NSObject>

- (void)didSelectCountry:(NSDictionary *)country;

@end

@interface CountryListViewController : UIViewController

@property (nonatomic, weak) id<CountryListViewDelegate>delegate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(strong, nonatomic) SCSkinManager* skinManager;


@property(nonatomic,strong) SCSettings* settings;


- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate;
@end
