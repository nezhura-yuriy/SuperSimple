//
//  SCUserItem.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCContactItem.h"
//#import "SCNetFile.h"
#import "SCContactItemSmall.h"
#import "SCUserAuthItem.h"
#import "SCPBManager.h"
#import "SCSettings.h"
//#import "SCParentTableViewCell.h"


@implementation SCContactItem

{
      SCPBManager* _pbManager;

}

-(id) init
{
    self = [super init];
    if (self )
    {
        
    }
    return self;
}

-(void) _init
{
    FTLog(@"");
    _userAuths = [[NSMutableArray alloc] init];
    _netFile = [[SCNetFile alloc] initWithSettings:_settings];
    _contactAddress = [[SCAdress alloc] initWithSettings:_settings];
}

-(void) dealloc
{
    
}

-(void)setSettings:(SCSettings *)settings
{
    
    _settings = settings;
    _pbManager = _settings.modelProfile.protoBufManager;
    
     [_pbManager addDelegate:self forType:SCPBManagerParceTypeContactSetFields];
     [_pbManager addDelegate:self forType:SCPBManagerParceTypeGetAllContactFields];
    
}


-(void)setUserPUUID:(SCPUUID *)userPUUID
{
    
//    _user = [_settings.modelUsers getUserByPUUID:userPUUID];
    
    _userPUUID = userPUUID;
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    if([[dictionary allKeys] containsObject:@"contactPUUID"])
        _contactPUUID = [[SCPUUID alloc] initFromString:[dictionary objectForKey:@"contactPUUID"]];
    
    if([[dictionary allKeys] containsObject:@"userPUUID"])
        _userPUUID = [[SCPUUID alloc] initFromString:[dictionary objectForKey:@"userPUUID"]];
    
    if([[dictionary allKeys] containsObject:@"contactLoginName"])
        _contactLoginName = [dictionary objectForKey:@"contactLoginName"];
    
    if([[dictionary allKeys] containsObject:@"contactNikName"])
        _contactNikName = [dictionary objectForKey:@"contactNikName"];
    
    if([[dictionary allKeys] containsObject:@"contactFirstName"])
        _contactFirstName = [dictionary objectForKey:@"contactFirstName"];
    
    if([[dictionary allKeys] containsObject:@"contactLastName"])
        _contactLastName = [dictionary objectForKey:@"contactLastName"];
    
    if([[dictionary allKeys] containsObject:@"contactSex"])
        _contactSex = [dictionary objectForKey:@"contactSex"];
    
    if([[dictionary allKeys] containsObject:@"contactBirthdate"])
        _contactBirthdate = [dictionary objectForKey:@"contactBirthdate"];
    
    if([[dictionary allKeys] containsObject:@"netFile"])
    {
        [_netFile fromDictionary:[dictionary objectForKey:@"netFile"]];
        if(_contactNikName)
            _netFile.logoText = _contactNikName;
        else
            _netFile.logoText = @"";
    }
    if([[dictionary allKeys] containsObject:@"userAuths"])
    {
        NSArray *arUserAuths = [dictionary objectForKey:@"userAuths"];
        for(NSDictionary* authItemDict in arUserAuths)
        {
            [_userAuths addObject:[[SCUserAuthItem alloc] initWithDictionary:authItemDict]];
        }
    }
    if([[dictionary allKeys] containsObject:@"contactEmail"])
        _contactEmail = [dictionary objectForKey:@"contactEmail"];
    
    if([[dictionary allKeys] containsObject:@"contactPhone"])
        _contactPhone = [dictionary objectForKey:@"contactPhone"];
    
    if([[dictionary allKeys] containsObject:@"contactLanguage"])
        _contactLanguage = [dictionary objectForKey:@"contactLanguage"];
    
    if([[dictionary allKeys] containsObject:@"contactTimeZone"])
        _contactTimeZone = [dictionary objectForKey:@"contactTimeZone"];
    
    if([[dictionary allKeys] containsObject:@"contactAddress"])
        [_contactAddress fromDictionary:[dictionary objectForKey:@"contactAddress"]];
    
    if([[dictionary allKeys] containsObject:@"contactAbout"])
        _contactAbout = [dictionary objectForKey:@"contactAbout"];
    
    if([[dictionary allKeys] containsObject:@"contactStatus"])
        _contactStatus = (SCUserStatus)[[dictionary objectForKey:@"contactStatus"] integerValue];
    else
        _contactStatus = SCUserStatusOffline;
    
    if([[dictionary allKeys] containsObject:@"lastConnection"])
        _lastConnection = [dictionary objectForKey:@"lastConnection"];

    if([[dictionary allKeys] containsObject:@"contactIsFriend"])
        _contactIsFriend = [dictionary objectForKey:@"contactIsFriend"];
    
    if([[dictionary allKeys] containsObject:@"contactGroupID"])
        _contactGroupID = [dictionary objectForKey:@"contactGroupID"];
}


-(NSDictionary*) toDictionary
{
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    
    if (_userPUUID.nsuuid)
        outDict[@"userPUUID"] = [_userPUUID scpuuidToString];
    
    if (_contactPUUID.nsuuid)
        outDict[@"contactPUUID"] = [_contactPUUID scpuuidToString];
    
    if (_contactLoginName)
        outDict[@"contactLoginName"] = _contactLoginName;
    
    if (_contactNikName)
        [outDict setObject:_contactNikName forKey:@"contactNikName"];
    
    if (_contactFirstName)
        [outDict setObject:_contactFirstName forKey:@"contactFirstName"];
    
    if (_contactLastName)
        [outDict setObject:_contactLastName forKey:@"contactLastName"];
    
    if (_contactSex)
        [outDict setObject:_contactSex forKey:@"contactSex"];
    
    if (_contactBirthdate)
        [outDict setObject:_contactBirthdate forKey:@"contactBirthdate"];

    
    if(_netFile)
        [outDict setObject:[_netFile toDictionary] forKey:@"netFile"];
    
    if (_contactEmail)
        [outDict setObject:_contactEmail forKey:@"contactEmail"];
    
    
    if (_contactPhone)
        [outDict setObject:_contactPhone forKey:@"contactPhone"];
    
    
    if (_contactLanguage)
        [outDict setObject:_contactLanguage forKey:@"contactLanguage"];
    
    if (_contactTimeZone)
        [outDict setObject:_contactTimeZone forKey:@"contactTimeZone"];
    
    if (_contactAddress)
        [outDict setObject:[_contactAddress toDictionary] forKey:@"contactAddress"];
    
    
    if (_contactAbout)
        [outDict setObject:_contactAbout forKey:@"contactAbout"];
    
    if (_contactStatus)
        [outDict setObject:[NSNumber numberWithInteger:_contactStatus] forKey:@"contactStatus"];
    
    if(_lastConnection)
        [outDict setObject:_lastConnection forKey:@"lastConnection"];
    
    if (_contactIsFriend)
        [outDict setObject:_contactIsFriend forKey:@"contactIsFriend"];
    
    if (_contactGroupID)
        [outDict setObject:_contactGroupID forKey:@"contactGroupID"];
    
    return outDict;
}

-(void) updateFrom:(SCContactItem*) newItem
{
    _contactPUUID = newItem.contactPUUID;
    _userPUUID = newItem.userPUUID;
    _contactLoginName = newItem.contactLoginName;
    _contactNikName = newItem.contactNikName;
    _contactFirstName = newItem.contactFirstName;
    _contactLastName = newItem.contactLastName;
    _contactSex = newItem.contactSex;
    _contactBirthdate = newItem.contactBirthdate;
    _userAuths = newItem.userAuths;
    _contactEmail = newItem.contactEmail; //deprecated
    _contactPhone = newItem.contactPhone; //deprecated
    _contactLanguage = newItem.contactLanguage;
    _contactTimeZone = newItem.contactTimeZone;
    _contactAddress = newItem.contactAddress;
    _contactAbout = newItem.contactAbout;
    _contactStatus = SCUserStatusOffline;
//    _contactIsFriend = ;
//    _contactGroupID = ;

}

-(SCContactItemSmall*) getSmall
{
    SCContactItemSmall* userSmall = [[SCContactItemSmall alloc] initWithSettings:_settings];
    userSmall.contactID = [_userPUUID scpuuidToString];
    userSmall.contactNikName = _contactNikName;
#ifdef OLD_FILE_LINK
    userSmall.imageLink = _contactImageLink;
#endif
    return userSmall;
}



#pragma mark - AUTH
-(void) contactAddAuth:(SCUserAuthItem*) userAuth
{
    for(SCUserAuthItem* auth in _userAuths)
    {
        if([auth isAuthEqual:userAuth])
        {
            auth.isConfirm = userAuth.isConfirm;
            return;
        }
    }
    if(_userAuths == nil)
        _userAuths = [[NSMutableArray alloc] init];
    [_userAuths addObject:userAuth];
}

#pragma mark - SCPBManagerDelegate
-(void)parsedType:(SCPBManagerParceType)type data:(id)data
{
    
    switch (type)
    {
            
        case SCPBManagerParceTypeContactSetFields:
            FTLog(@"%@",data);
            break;
            
        case SCPBManagerParceTypeGetAllContactFields:
            FTLog(@"%@",data);
            break;

        case SCPBManagerParceTypeAuthGet:
        {
            FTLog(@"SCPBManagerParceTypeAuthGet");
            if([data isKindOfClass:[NSDictionary class]])
            {
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(showError:)])
                        [delegate showError:data];
                }
            }
            else
            {
                if([data isKindOfClass:[NSArray class]])
                {
                    for(SCUserAuthItem* authItem in data)
                    {
                        [self contactAddAuth:authItem];
                    }
                }
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(userAuthUpdate:)])
                        [delegate userAuthUpdate:_userAuths];
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeAuthAdd:
        {
            FTLog(@"SCPBManagerParceTypeAuthAdd");
            if([data isKindOfClass:[NSDictionary class]])
            {
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(showError:)])
                        [delegate showError:data];
                }
            }
            else
            {
                SCUserAuthItem* authItem = (SCUserAuthItem*) data;
                [self contactAddAuth:authItem];
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(userAuthUpdate:)])
                        [delegate userAuthUpdate:data];
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeAuthConfirm:
        {
            FTLog(@"SCPBManagerParceTypeAuthConfirm");
            if([data isKindOfClass:[NSDictionary class]])
            {
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(showError:)])
                        [delegate showError:data];
                }
            }
            else
            {
                // Уже обновленный итем из масисва _userAuths, если нужно можно достать
                // SCUserAuthItem* userAuthItem = (SCUserAuthItem*) data;
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(userAuthUpdate:)])
                        [delegate userAuthUpdate:data];
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeAuthRemove:
        {
            FTLog(@"SCPBManagerParceTypeAuthRemove");
            if([data isKindOfClass:[NSDictionary class]])
            {
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(showError:)])
                        [delegate showError:data];
                }
            }
            else
            {
                SCUserAuthItem* authItem = (SCUserAuthItem*) data;
                [_userAuths removeObject:authItem];

                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(userAuthUpdate:)])
                        [delegate userAuthUpdate:_userAuths];
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeChangeStatus:
        case SCPBManagerParceTypeChangeStatusEvent:
        {
            if([data isKindOfClass:[SCContactItem class]])
            {
                for(id<SCContactItemDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(statusChange:)])
                        [delegate statusChange:_contactStatus];
                }
                for(id<SCPBManagerDelegate> delegate in _delegates)
                {
                    if([delegate respondsToSelector:@selector(parsedType:data:)])
                        [delegate parsedType:type data:data];
                }
            }
        }
            break;
            
        default:
            
            break;
    }
}



-(void)setContactFields{
    
//    [_pbManager setFieldsRequest:self];
    
}

-(void)getContactFields{
    
    [_pbManager getAllContactFieldsRequest:self];
    
}


/* /========================================================================================
на хуа ????
* /
-(NSArray *)toFullArray {
    
    NSMutableArray * mutableArray =[[NSMutableArray alloc]init];
   
    if(_contactNikName) {
        NSDictionary  *dict = [[NSDictionary alloc] initWithObjectsAndKeys: _contactNikName , @"Chat name",nil];
        [mutableArray addObject:dict];
    } else {
        _contactNikName=@"";
        NSDictionary  *dict = [[NSDictionary alloc] initWithObjectsAndKeys: _contactNikName , @"Chat name",nil];
        [mutableArray addObject:dict];
    }
  
    
    if(_contactAbout) {
        NSDictionary  *dict = [[NSDictionary alloc] initWithObjectsAndKeys: _contactAbout ,
                               @"About me",nil];
        [mutableArray addObject:dict];
    } else {
        _contactAbout=@"";
        NSDictionary  *dict = [[NSDictionary alloc] initWithObjectsAndKeys: _contactAbout , @"About me",nil];
        [mutableArray addObject:dict];
    }
    
    
        if(_contactFirstName) {
    NSDictionary  *dict =  [[NSDictionary alloc] initWithObjectsAndKeys:_contactFirstName , @"First name",nil];
            [mutableArray addObject:dict];
        } else {
            _contactFirstName=@"";
            NSDictionary  *dict =  [[NSDictionary alloc] initWithObjectsAndKeys:_contactFirstName , @"First name",nil];
            [mutableArray addObject:dict];
        }
    
        if(_contactLastName) {
             NSDictionary  *dict=    [[NSDictionary alloc] initWithObjectsAndKeys:_contactLastName , @"Last name" ,nil];
            [mutableArray addObject:dict];
        } else {
            _contactLastName=@"";
            NSDictionary  *dict=    [[NSDictionary alloc] initWithObjectsAndKeys:_contactLastName , @"Last name" ,nil];
            [mutableArray addObject:dict];
            
            }
    
    
    if(_contactSex) {
        NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactSex , @"Gender",nil];
        [mutableArray addObject:dict];
    } else {
        _contactSex=@"";
        NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactSex , @"Gender",nil];
        [mutableArray addObject:dict];
        
    }
    
  
    if(_contactBirthdate) {
        NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactBirthdate , @"Birthday",nil];
        [mutableArray addObject:dict];
    } else {
        _contactBirthdate=[NSDate dateWithTimeIntervalSince1970:0];
        NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactBirthdate , @"Birthday",nil];
        [mutableArray addObject:dict];
    }
    
    return mutableArray;
    
}



-(NSArray*)toAddressArray {
    
    NSMutableArray * mutableArray =[[NSMutableArray alloc]init];

    if(!_contactAddress)
    {
        _contactAddress=[[SCAdress alloc]init];
    }
        if(_contactAddress.country) {
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.country, @"Country",nil];        [mutableArray addObject:dict];
        } else {
            _contactAddress.country=@"";
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.country, @"Country",nil];        [mutableArray addObject:dict];
        }
    
    if(_contactAddress.city) {
        NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.city, @"City",nil];
        [mutableArray addObject:dict];
    } else {
        _contactAddress = [[SCAdress alloc]init];
        _contactAddress.city=@"";
        NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.city, @"City",nil];
        [mutableArray addObject:dict];
    }
    
        if(_contactAddress.street) {
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.street, @"Street",nil];
            [mutableArray addObject:dict];
        } else {
            _contactAddress.street=@"";
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.street, @"Street",nil];        [mutableArray addObject:dict];
        }
    
        if(_contactAddress.house) {
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.house, @"House",nil];        [mutableArray addObject:dict];
        } else {
            _contactAddress.house=@"";
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.house, @"House",nil];        [mutableArray addObject:dict];
        }
    
        if(_contactAddress.appartament) {
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.appartament, @"Appartament",nil];        [mutableArray addObject:dict];
        } else {
            _contactAddress.appartament=@"";
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.appartament, @"Appartament",nil];        [mutableArray addObject:dict];
        }
    
        if(_contactAddress.postCode) {
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.postCode, @"Postcode",nil];        [mutableArray addObject:dict];
        } else {
            _contactAddress.postCode=@"";
            NSDictionary  *dict =    [[NSDictionary alloc] initWithObjectsAndKeys:_contactAddress.postCode, @"Postcode",nil];        [mutableArray addObject:dict];
        }
    
    
    return mutableArray;
    
}


-(NSArray *) toFiltredArray {
    NSArray * array = [self toFullArray];
    NSMutableArray * mutableArray =[[NSMutableArray alloc]init];
    
    for (int i=0; i<[array count]; i++) {
        NSString * key =[[[array objectAtIndex:i] allKeys] objectAtIndex:0];
        NSString * value = [[array objectAtIndex:i] objectForKey:key];
        if (![value isEqualToString:@""]) {
            [mutableArray addObject:[array objectAtIndex:i]];
        }
        
    }
    return mutableArray;
}
*/
@end
