//
//  SCAgreementViewController.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 17.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"

@class SCSettings;

@interface SCAgreementViewController : UIViewController <UIWebViewDelegate>

@property(nonatomic,weak) id<SCMainNavControllerDelegate> delegate;

@property(nonatomic,strong) SCSettings* settings;

- (IBAction)agreeButton:(id)sender;


@end
