//
//  SCChatPhotoMenu.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 23.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatPhotoMenu.h"
#import "SCSettings.h"

@implementation SCChatPhotoMenu
{
    void (^afterSelect) (SCPopChatMenuViewItem selectItem);
    SCSettings *_settings;
    UIImagePickerController *_fotoPicker;
    UIImagePickerController *_videoPicker;
    UIImagePickerController *_mediaLibraryPicker;
    
    BOOL _availableMediaLibrary;
    BOOL _availableCamra;
}

+(CGSize) sizeView
{
    //return CGSizeMake(320, 45);
    return CGSizeZero;
}

//MARK: Inits

-(id) init
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}

-(id) initWithCompletionblock:(void (^)(SCPopChatMenuViewItem selectItem))completionBlock
{
    self = [super init];
    if(self)
    {
        afterSelect = [completionBlock copy];
        [self _init];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self _init];
    
    [self showActionSheet];
}


-(void) _init
{
    self.clipsToBounds = YES;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [[UIColor colorWithRed:164.0/255.0 green:205.0/255.0 blue:255.0/255.0 alpha:0.85] CGColor];
    self.backgroundColor = [UIColor colorWithRed:0.7176 green:0.8235 blue:0.8784 alpha:0.85];
    
    _availableMediaLibrary = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    if (!_mediaLibraryPicker && _availableMediaLibrary) {
        _mediaLibraryPicker = [[UIImagePickerController alloc] init];
        _mediaLibraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        _mediaLibraryPicker.delegate = self;
    }
    
    _availableCamra = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    if (!_fotoPicker && _availableCamra) {
        _fotoPicker = [[UIImagePickerController alloc] init];
    }
    
    if (!_videoPicker && _availableCamra) {
        _videoPicker = [[UIImagePickerController alloc] init];
    }
}

-(void) dealloc
{
    
}

-(void) setAfterSelect:(void(^)(SCPopChatMenuViewItem selectItem)) completionBlock
{
    afterSelect = [completionBlock copy];
}


-(void) layoutSubviews
{
    [super layoutSubviews];
}


-(void) btnsAction:(id) sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
    {
        switch (((UIButton*)sender).tag)
        {
            case 1:
                [_delegate selectItem:SCPopChatMenuViewItemPhoto];
                break;
                
            case 2:
                [_delegate selectItem:SCPopChatMenuViewItemAudio];
                break;
                
            case 3:
                [_delegate selectItem:SCPopChatMenuViewItemVideo];
                break;
                
            case 4:
                [_delegate selectItem:SCPopChatMenuViewItemUser];
                break;
                
            case 5:
                [_delegate selectItem:SCPopChatMenuViewItemSettings];
                break;
                
            case 6:
                [_delegate selectItem:SCPopChatMenuViewItemList];
                break;
                
            default:
                break;
        }
    }
}

- (void) showActionSheet
{
   UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];

    if (_availableMediaLibrary) {
        [actionSheet addButtonWithTitle:@"Media Library"];
    }
    
    if (_availableCamra) {
        [actionSheet addButtonWithTitle:@"Make a photo or video"];
    }
    
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
//        [actionSheet addButtonWithTitle:@"Media Library"];
//    }
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//        [actionSheet addButtonWithTitle:@"Make a photo or video"];
//    }
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];

    [actionSheet showInView:self];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            FTLog(@"0");
            [self takeMediaLibrary];
        }
            break;
            
        case 1:
        {
            FTLog(@"1");
        }
            break;
        
        case 2:
        {
            //Cancel
        }
            break;
            
        default:
            break;
    }
}

- (void) takeMediaLibrary
{
    //[_delegate presentViewController:_mediaLibraryPicker animated:YES completion:nil];
}

@end
