//
//  SCAddressBookContactCell.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 26.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAddressBookContactCell.h"
#import "SCAddressbookContact.h"
#import "SCSettings.h"
#import "UIImage+SCCropImageToCircleWithBorder.h"
#import "UIImageView+Letters.h"




@implementation SCAddressBookContactCell

{
    SCSettings* _settings;
}


- (void)awakeFromNib {
    
    _settings = [SCSettings sharedSettings];
    
    self.inviteButtonOutlet.layer.borderWidth = 1;
    self.inviteButtonOutlet.layer.cornerRadius = 2;
    self.inviteButtonOutlet.layer.borderColor = [SKCOLOR_NavBarBg CGColor];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)fillWhithContact:(SCAddressbookContact*)contact{
    
//    NSString* fullName = [NSString new];
//    
//    if (contact.contactLastName.length > 0) {
//        
//     fullName = [NSString stringWithFormat:@"%@ ",contact.contactLastName];
//        
//    }
//    
//    if (contact.contactFirstName.length > 0) {
//     
//     fullName = [fullName stringByAppendingString:contact.contactFirstName];
//        
//    }
    self.contactName.text = contact.fullName;

    self.contactName.font = [UIFont fontWithName:FT_FontNameDefult size:15];
    self.contactName.textColor = SKCOLOR_TableCellFont;
    self.tintColor = SKCOLOR_TableCellBg;
    
    [self.inviteButtonOutlet setTitleColor:SKCOLOR_NavBarBg forState:UIControlStateNormal];
    
    if (contact.contactFoto) {
        
        UIImage* image = contact.contactFoto;
        
        self.contactFotoView.image = [image cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
        
        
    } else {
        
        [self.contactFotoView setImageWithString:self.contactName.text color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];

    }
    
    
}

- (IBAction)inviteAction:(id)sender {
    
    [self.delegate inviteContactAtIndexPath:self.indexPath];
}




@end
