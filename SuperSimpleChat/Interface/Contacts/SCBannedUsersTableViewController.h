//
//  SCBannedUsersTableViewController.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 02.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SCSettings;

@interface SCBannedUsersTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic,strong) SCSettings* settings;


@end
