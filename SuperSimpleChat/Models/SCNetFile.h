//
//  SCNetFile.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"
#import "SCModelsDelegates.h"
#import "SCTypes.h"

typedef enum : NSUInteger {
    SCNetFileOperationDownLoad,
    SCNetFileOperationUpload,
} SCNetFileOperation;

@interface SCNetFile : SCAbstractItem <SCHTTPManagerDelegate>

@property(nonatomic,strong) NSString* logoText;
@property(nonatomic,assign) SCNetFileStatus status;
@property(nonatomic,assign) SCMediaFileType fileType;
@property(nonatomic,strong) NSString* netPath;
@property(nonatomic,strong) NSString* localPath;
@property(nonatomic,strong) NSMutableDictionary* additionInfo;

#pragma mark sets
-(void) setImageNetPath:(NSString*) netPath;
-(BOOL) isNotEmptyPath;
-(void) deletePath;

-(void) clearCompletion;
#pragma mark gets

#pragma mark - for completion block
-(UIImage*) imageForSize:(CGSize) forSize;
-(UIImage*) blureImage:(CGRect) forRect;
-(UIImage*) videoFragment;

#pragma mark - new function
-(id) getWithCompletionblock:(void (^)(SCNetFileComletionType type, id result))completionBlock;
-(id) sendWithCompletionblock:(void (^)(SCNetFileComletionType type, id result))completionBlock;

#pragma mark Services
+(NSString*) scNetFileStatusPrint:(SCNetFileStatus) aStatus;
@end
