//
//  SCNewChatViewController.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 06.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCNewChatViewController.h"
#import "SCChatUsersViewController.h"
#import "SCModelChats.h"
#import "SCSettings.h"
#import "SCPBManager.h"
#import "SCChatItem.h"
#import "SCContactItem.h"
#import "SCChatUserMenu.h"
#import "SCImageViewWithIndicator.h"
#import "SCButtonRightImage.h"
#import "SCChatUsersView.h"

//#import "SCSharingPanel.h"

//#define MARGIN 10.0f
//#define CHAT_IMAGE_SIZE 44.0f
#define SEPARATOR_HEIGHT 2.0f
//#define DEFAULT_ANIMATION_DURATION 0.25
#define TABLE_USER_VIEW_CELL @"UserCell"


@implementation SCNewChatViewController
{
    SCModelChats* _modelChats;
    SCChatUserMenu *chatUserView;
    NSMutableArray* _usersInRoom;
    UIImagePickerController* _photoPicker;
    BOOL _isImageUpdate;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    _isNewChat = NO;
    _isViewOnly = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isImageUpdate = NO;
//    _imgViewChatLogo.image = [UIImage imageNamed:@"nofoto"];
    _imagePanel.delegate = self;

    _scrollFormView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height - 44 -20);
//    [self.view addSubview:_sharePanel];
    
    _usersInRoom = [[NSMutableArray alloc] init];
    
    [_tableViewUsers setSeparatorColor:[UIColor clearColor]];
    [_tableViewUsers registerClass:[UITableViewCell class] forCellReuseIdentifier:TABLE_USER_VIEW_CELL];
}


-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    _tableViewUsers.layer.borderColor = [SKCOLOR_TableSeparator CGColor];
    _tableViewUsers.layer.borderWidth = 1;
    _tableViewUsers.layer.cornerRadius = 10;

    _modelChats = _settings.modelChats;
    if(_isNewChat)
    {
        _isViewOnly = NO;
        _imagePanel.isEditedle = YES;
    }
}


-(void) dealloc
{
    FTLog(@"SCNewChatViewController dealloc");
    if(_chatItem)
        [_chatItem.netFile clearCompletion];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_settings.modelChats addDelegate:self];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    [self updateUI];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_settings.modelChats delDelegate:self];
    
    if(_chatItem)
        [_chatItem.netFile clearCompletion];

    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

-(void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    FTLog(@"SCNewChatViewController viewDidLayoutSubviews %@",NSStringFromCGRect(self.view.frame));

}

-(void) updateUI
{
    self.view.backgroundColor = SKCOLOR_Background;
    _labOwner.backgroundColor = SKCOLOR_NavBarBg;
    _labOwner.textColor = SKCOLOR_NavBarTitleFont;
    [_labOwner setFont:[UIFont fontWithName:FT_FontNameRegular size:17.0]];
    [_fldChatDescription setFont:[UIFont fontWithName:FT_FontNameRegular size:14.0]];
    [_fldChatName setFont:[UIFont fontWithName:FT_FontNameRegular size:14.0]];
    [_btnSelectUsers.titleLabel setFont:[UIFont fontWithName:FT_FontNameLight size:14.0]];
    
    switch (_chatItem.chatType)
    {
        case SCChatTypeGroup:
        {
            if(_isNewChat)
            {
                self.navigationItem.title = @"New group chatroom";
                _labOwner.text = [NSString stringWithFormat:@"by @%@",_settings.modelProfile.contactItem.contactNikName];
            }
            else if(!_isViewOnly)
                self.navigationItem.title = @"Edit group chatroom";
            else
                self.navigationItem.title = @"About group chatroom";
        }
            break;
            
        case SCChatTypePublic:
        {
            if(_isNewChat)
            {
                self.navigationItem.title = @"New public chatroom";
                _labOwner.text = [NSString stringWithFormat:@"by @%@",_settings.modelProfile.contactItem.contactNikName];
            }
            else if(!_isViewOnly)
                self.navigationItem.title = @"Edit public chatroom";
            else
                self.navigationItem.title = @"About public chatroom";
        }
            break;
            
        default:
            break;
    }
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[_settings.skinManager getImageForKey:@"btn_back"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButton.imageView.image.size.width, backButton.imageView.image.size.height);
    [backButton addTarget:self action:@selector(btnBackAction:forEvent:) forControlEvents:UIControlEventTouchUpInside];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    if(!_isViewOnly)
    {
        [_btnSelectUsers setRightImage:[_settings.skinManager getImageForKeyX:@"btn_add"]];
        
        UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [saveButton setTitle:@"Save" forState:UIControlStateNormal];
        [saveButton.titleLabel setFont:[UIFont fontWithName:FT_FontNameBold size:18.0]];
        [saveButton sizeToFit];
        saveButton.frame = CGRectMake(0.0, 0.0, saveButton.frame.size.width, saveButton.frame.size.height);
        [saveButton addTarget:self action:@selector(btnChatCreateAction:forEvent:) forControlEvents:UIControlEventTouchUpInside];
         self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:saveButton];
    }
    else
        [_btnSelectUsers setRightImage:nil];

}

-(void) setChatItem:(SCChatItem *)chatItem
{
    if(_chatItem)
        [_chatItem.netFile clearCompletion];
    
    _chatItem = chatItem;
    _fldChatName.text = _chatItem.chatName;
    _fldChatDescription.text = _chatItem.chatDescription;
    _labOwner.text = [NSString stringWithFormat:@"by @%@",_chatItem.chatOwner.contactNikName];
    if([_chatItem.chatOwner isEqual:_settings.modelProfile.contactItem])
    {
        _isViewOnly = NO;
        _imagePanel.isEditedle = YES;
    }
    else
        _imagePanel.isEditedle = NO;
    
    _usersInRoom = _chatItem.usersInChat;
    [_btnSelectUsers setTitle:[NSString stringWithFormat:@"Users in room (%lu)",(unsigned long)_usersInRoom.count] forState:UIControlStateNormal];
    
    [_chatItem.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                break;
                
            case SCNetFileComletionTypeCompleet:
            {
                [_imagePanel endProgress];
                if([result isKindOfClass:[SCNetFile class]])
                {
                    _imagePanel.imgPhoto.image = [(SCNetFile*)result imageForSize:_imagePanel.imgPhoto.frame.size] ;
                    _imagePanel.imgBackgraund.image =  [(SCNetFile*)result blureImage:_imagePanel.imgBackgraund.frame];
                }
            }
                break;
                
            case SCNetFileComletionTypeProgress:
                [_imagePanel.imgPhoto setProgressPercent:[result doubleValue]];
                break;
                
            default:
                FTLog(@"default case");
                break;
        }

    }];
//    [_chatItem.netFile showImage:_imagePanel.imgPhoto];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}


- (IBAction)fldChatNameChange:(UITextField *)sender forEvent:(UIEvent *)event
{
    FTLog(@"fldChatNameChange");
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField
{
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:_fldChatName])
    {
        FTLog(@"_fldChatName");
    }
    else if ([textField isEqual:_fldChatDescription])
    {
        FTLog(@"_fldChatDescription");
    }
    
    return YES;
}

#pragma mark UITableView

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _chatItem.usersInChat.count;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 26;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* userCell = [tableView dequeueReusableCellWithIdentifier:TABLE_USER_VIEW_CELL forIndexPath:indexPath];
    if(!userCell)
        userCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TABLE_USER_VIEW_CELL];
    [userCell.textLabel setFont:[UIFont fontWithName:FT_FontNameLight size:14.0]];
    userCell.textLabel.text = ((SCContactItem*)[_chatItem.usersInChat objectAtIndex:indexPath.row]).contactNikName;
    
    return userCell;
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            [self photoFromSource:UIImagePickerControllerSourceTypeCamera];
        }
            break;
            
        case 1:
        {
            [self photoFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
        }
            break;
            
        default:
        {
            
        }
            break;
    }
}

-(void) photoFromSource:(UIImagePickerControllerSourceType) sorce
{
    _photoPicker = [[UIImagePickerController alloc] init];
    
    _photoPicker.delegate = self;
    _photoPicker.sourceType = sorce;
    
    NSArray *mediaTypes = [NSArray arrayWithObject:@"public.image"];
    
    _photoPicker.mediaTypes = mediaTypes;
    [self presentViewController:_photoPicker animated:YES completion:nil];
    
}

- (IBAction)btnSelectUsersAction:(UIButton *)sender forEvent:(UIEvent *)event
{
    if(!_isViewOnly)
    {
        FTLog(@"btnSelectUsersAction");

        SCChatUsersViewController* pSCChatUsersViewController = [[SCChatUsersViewController alloc] init];
        pSCChatUsersViewController.isNewChat = _isNewChat;
    //    pSCChatUsersViewController.isViewOnly = _isViewOnly;

        pSCChatUsersViewController.settings = _settings;
        pSCChatUsersViewController.userView.chatItem = _chatItem;
        pSCChatUsersViewController.userInRoom = _usersInRoom;
        pSCChatUsersViewController.delegate = self;
        
        [self.navigationController pushViewController:pSCChatUsersViewController animated:YES];
    }
/*
//    [_txtChatDescription resignFirstResponder];
    chatUserView = [[[NSBundle mainBundle] loadNibNamed:@"SCChatUserMenu" owner:self options:nil] lastObject];
    chatUserView.settings = _settings;
    chatUserView.delegate = self;
    chatUserView.alpha = 0.0f;
    chatUserView.inAddMode = YES;
    chatUserView.userInRoom = _usersInRoom;
    
    
//        CGSize chatUserSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height - _navBarView.frame.size.height);
        chatUserView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, _scrollFormView.frame.size.height);
        chatUserView.alpha = 0.0f;
        [self.view addSubview:chatUserView];
        [UIView animateWithDuration:0.25 animations:^{
            
//            chatUserView.frame = CGRectMake(0.0f ,_navBarView.frame.size.height , chatUserSize.width, chatUserSize.height);
            chatUserView.alpha = 1.0f;
            //                    _workspaceScrollView.frame = CGRectMake(0, _topView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - _topView.frame.size.height - chatUserSize.height);
            _scrollFormView.alpha = 0.0f;
        }completion:^(BOOL finished) {
            
        }];
*/
}
- (IBAction)btnSelectChatTypeAction:(UIButton *)sender forEvent:(UIEvent *)event
{
    FTLog(@"btnSelectChatTypeAction");
//    [_txtChatDescription resignFirstResponder];
}


- (IBAction)btnChatCreateAction:(UIButton *)sender forEvent:(UIEvent *)event
{
    if(!_chatItem)
    {
        _chatItem = [[SCChatItem alloc] initWithSettings:_settings];
        _chatItem.chatType = _forChatType;
        _chatItem.chatOwner = _settings.modelProfile.contactItem;
    }
    else
    {
//        _chatItem.chatType = _forChatType;
//        _chatItem.chatOwner = _settings.modelProfile.contactItem;
    }
    _chatItem.chatName = _fldChatName.text;
    _chatItem.chatDescription = _fldChatDescription.text;
    if(_chatItem.chatPuuid.nsuuid == nil)
    {
        if(_usersInRoom.count > 0)
            _chatItem.usersInChat = _usersInRoom;
        [_modelChats addNewChatRoom:_chatItem];
    }
    else
    {
        [_modelChats changeChatRoom:_chatItem];
    }
}

- (void) btnBackAction:(UIButton *)sender forEvent:(UIEvent *)event
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark SCPBManagerDelegate <NSObject>
//-(void) receiveParsedData:(id) data
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"SCModelProfile : receiveParsedData");
    switch (type)
    {
        case SCPBManagerParceTypeCreateChat:
        {
            FTLog(@"%@",data);
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        case SCPBManagerParceTypeChangeChat:
        {
            FTLog(@"%@",data);
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
        {
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            FTLog(@"%@",data);
        }
            break;
    }
}

#pragma mark SCModelChatsDelegate <NSObject>
-(void) didAddItemToModelChatsList:(id) respond
{
    FTLog(@"didAddItemToModelChatsList");
    if([respond isKindOfClass:[SCChatItem class]])
    {// success
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        FTLog(@"Error");
    }
}
-(void) didChangeItemInModelChatsList:(id) respond
{
    FTLog(@"didChangeItemInModelChatsList");
    if([respond isKindOfClass:[SCChatItem class]])
    {// success
        if(!_isImageUpdate)
            [self.navigationController popViewControllerAnimated:YES];
        else if([respond isKindOfClass:[SCChatItem class]])
        {
            _isImageUpdate = NO;
        }
    }
    else
    {
        FTLog(@"Error");
    }
}
-(void) didRemoveItemFromModemChatsList:(id) respond
{
    FTLog(@"didRemoveItemFromModemChatsList");
    if([respond isKindOfClass:[SCChatItem class]])
    {// success
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        FTLog(@"Error");
    }

}

-(void) didChangeModelChatItem
{
    FTLog(@"didChangeModelChatItem");
}
#pragma mark SCPhotoPanelDelegate <NSObject>

-(void) addImageAction
{
    FTLog(@"btnSelectLogoAction");
    //    [_txtChatDescription resignFirstResponder];
    
    NSArray *deviceModes = [UIImagePickerController availableCaptureModesForCameraDevice:UIImagePickerControllerCameraDeviceRear];
    
    BOOL isAvaible = NO;
    for (NSNumber *num in deviceModes)
    {
        if ([num intValue] == UIImagePickerControllerCameraCaptureModeVideo)
        {
            isAvaible = YES;
            
            break;
        }
    }
    
    if (isAvaible)
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cansel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Take photo",@"Choose existing photo", nil];
        
        [actionSheet showInView:self.view];
        
    }
    else
    {
        [self photoFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

-(void) showImageAction
{
    
}
-(void) clearImageAction
{
    [_chatItem.netFile deletePath];
//    [_chatItem saveCachedMessages];
    _imagePanel.imgPhoto = nil;
//    [_imagePanel settingImage:nil];
    if(_chatItem.chatPuuid.nsuuid != nil)
    {
        _isImageUpdate = YES;
        [_modelChats changeChatRoom:_chatItem];
    }
}
#pragma mark SCChatUsersViewControllerDelegate <NSObject>
-(void) updateUserList:(NSMutableArray*) userInRoomNew
{
    FTLog(@"%@",userInRoomNew);
    if(_chatItem.chatPuuid.nsuuid == nil)
    {
        _usersInRoom = userInRoomNew;
    }
    [_tableViewUsers reloadData];
}

#pragma mark SCChatViewControllerDelegate <NSObject>
-(void)selectItem:(SCPopChatMenuViewItem) selectItem
{
    switch (selectItem)
    {
        case SCPopChatMenuViewItemUserDone:
        {
            [_btnSelectUsers setTitle:[NSString stringWithFormat:@"User list: %d",_usersInRoom.count] forState:UIControlStateNormal];
        }
            break;
            
        case SCPopChatMenuViewItemUserCansel:
        {

        }
            break;
            
        default:
            break;
    }
    [UIView animateWithDuration:0.25 animations:^{
        chatUserView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
        chatUserView.alpha = 0.0f;
        _scrollFormView.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [chatUserView removeFromSuperview];
    }];

}
#pragma mark - keyboard
- (void)_keyboardWillShow:(NSNotification *)note
{
    
//    NSDictionary *info=[note userInfo];
    //    FTLog(@"%@",info);
//    CGFloat animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
//    CGSize keyboardSize=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
//    [UIView animateWithDuration:animationDuration animations:^
//     {
//         _scrollFormView.frame = CGRectMake(0, _navBarView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - _navBarView.frame.size.height - keyboardSize.height);
//     }
//                     completion:^(BOOL finished)
//     {
//         //
//     }];
    
}

- (void)_keyboardWillHide:(NSNotification *)note
{
//    NSDictionary *info=[note userInfo];
    //    FTLog(@"%@",info);
//    CGFloat animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
//    [UIView animateWithDuration:animationDuration animations:^{
//        _scrollFormView.frame = CGRectMake(0, _navBarView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - _navBarView.frame.size.height);
//        
//    } completion:^(BOOL finished)
//     {
//         //
//     }];
    
}

#pragma mark - UIImagePickerControllerDelegate<NSObject>

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.image"])
    {
        __block UIImage* img = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSData* imgData = UIImageJPEGRepresentation(img, 0.75f);
        FTLog(@"image size %d",imgData.length);
        [_settings.httpManager fileUpload:imgData
                                 fileType:@"image/jpeg"
        progressUpload:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
        {
            [_imagePanel setProgress:totalBytesRead totalBytesExpectedToRead:totalBytesExpectedToRead];
        } onSuccess:^(NSDictionary *imageInfo)
        {
            FTLog(@"Success %@",imageInfo);
            [_imagePanel settingImage:img];
            [_chatItem.netFile setImageNetPath:[imageInfo objectForKey:@"uuid"]];
            [_imagePanel endProgress];
            if(_chatItem.chatPuuid.nsuuid != nil)
            {
                _isImageUpdate = YES;
                [_modelChats changeChatRoom:_chatItem];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            FTLog(@"Fail");
            [_imagePanel endProgress];
        }];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка"
                                    message:@"Выбран неверный тип файла"
                                   delegate:nil
                          cancelButtonTitle:@"ОК"
                          otherButtonTitles:nil] show];
    }
    
    [_photoPicker dismissViewControllerAnimated:YES completion:nil];
//    [[RootViewController viewController].view setNeedsLayout];
    //    [_delegate hideMenuView];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [_photoPicker dismissViewControllerAnimated:YES completion:nil];
//    [[RootViewController viewController].view setNeedsLayout];
    //    [_delegate hideMenuView];
}


@end
