//
//  SCLTableViewDataSource.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCListItem.h"


@protocol SCLTableViewDataSource <NSObject>

- (NSInteger) numberOfRows;
- (UIView *) cellForRow:(NSInteger) row;
- (void) itemAdded;
- (void) itemAddedAtIndex:(NSInteger)index;

@end
