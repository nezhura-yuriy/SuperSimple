//
//  SCCountrysTableViewController.h
//  SuperSimpleChat
//
//  Created by yury on 12.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"

@protocol SCCountrysTableVCProtocol <NSObject>
- (void) changeCountry:(NSString *) countryTitle;
@end

@interface SCCountrysTableViewController : UITableViewController

@property (nonatomic, strong) SCSettings *settings;
@property (nonatomic, weak) id <SCCountrysTableVCProtocol> delegate;
@property (nonatomic, strong)  NSMutableArray* sectionsArray;

-(NSMutableArray*) generateSectionsFromArray:(NSArray*) array withFilter:(NSString*) filterString;

@end
