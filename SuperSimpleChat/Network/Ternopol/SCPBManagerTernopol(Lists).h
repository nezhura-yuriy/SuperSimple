//
//  SCPBManagerTernopol+Lists.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol.h"

@interface SCPBManagerTernopol (Lists)

-(void) parseGetList:(id)data;

-(void) parseAddListItem:(id)data;
-(void) parseGetListItems:(id)data;
-(void) parseUpdateListItem:(id)data;
-(void) parseRemoveListItem:(id)data;
-(void) parseSetListItemsOrder:(id) data;

@end
