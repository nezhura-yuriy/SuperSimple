//
//  SCAuthTableViewCell.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCUserAuthItem.h"

@protocol SCAuthTableViewCelllDelegate <NSObject>

-(void) confirmAuth:(SCUserAuthItem*) auth withCode:(NSString*) code;

@end

@interface SCAuthTableViewCell : UITableViewCell

@property (nonatomic, weak) id < SCAuthTableViewCelllDelegate > delegate;

- (IBAction)confirmAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *authLabel;

@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@property (strong, nonatomic) NSIndexPath* indexPath;

@property (strong, nonatomic) SCUserAuthItem* auth;

@end
