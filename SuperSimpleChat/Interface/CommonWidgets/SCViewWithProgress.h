//
//  SCViewWithProgress.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 24.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCTypes.h"

@class SCSettings;


@interface SCViewWithProgress : UIView
{
    SCSettings* _settings;
    
    CAShapeLayer* shadowLayer;
    CAShapeLayer *shapeLayer;
    
    UIActivityIndicatorView* _activityIndicatorView;

}
@property(nonatomic,strong) SCSettings* settings;

-(id) initWithSettings:(SCSettings*) settings;

-(void) setStatus:(SCNetFileStatus) status;
-(void) setProgressPercent:(CGFloat) persents;

@end
