//
//  SCChatListView.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"
#import "SCList.h"
#import "SCListItem.h"
#import "SCSettings.h"
#import "SCWarningView.h"

@interface SCChatListView : SCChatBottomsView <UIScrollViewDelegate, SCWarningViewProtocol>

@end
