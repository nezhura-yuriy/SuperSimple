//
//  SCFeedbackViewController.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 26.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCFeedbackViewController.h"

static const CGFloat kSideOffset = 15;

//TODO: указать ID приложения когда будет известно правильное ID
//Сейчас https://itunes.apple.com/ua/app/teamwork.com-projects/id726473079?mt=8
static NSString *kAppID = @"726473079";

#define K_EMAIL @"ittropica.ukr@gmail.com" //TODO: Дать точный адрес e-mail

@implementation SCFeedbackViewController
{
    NSMutableArray *_labelFooterSection;
    NSInteger _nubmerSections;
}
//MARK: View Controller Life
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;

    _nubmerSections = 2;
    _labelFooterSection = [NSMutableArray new];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

//MARK: Setups
- (void) setupUI
{
    //Nav Title
    self.title = @"Feedback";
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    
    _tableView.scrollEnabled = NO;

    //Back button
    UIImage *backButtonImage = [_settings.skinManager getImageForKeyX:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    [self setupTableViewFooterLables];
}

- (void) setupTableViewFooterLables
{
    for (NSInteger i=0; i < _nubmerSections; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(kSideOffset,
                                                                    0.0,
                                                                    self.view.bounds.size.width - kSideOffset * 2,
                                                                    44)];
        
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 10;
        
        switch (i) {
            case 0:
                label.text = @"Please leave your feedback on the Super Simple page on the App Store.";
                break;
                
            case 1:
                label.text = @"Feel free to write your suggestions or comments to the Super Simple developers.";
                break;
                
            default:
                label.text = @"";
                break;
        }
        
        NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_FontTableFooter,
                                    NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:14],
                                    };
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attribute];
        label.attributedText = attributedText;
        
        [label sizeToFit];
        [_labelFooterSection addObject:label];
    }
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.frame;
}

//MARK: TableView
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return _nubmerSections;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    UILabel *label = _labelFooterSection[section];
    return label.frame.size.height;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor clearColor];
    
    UILabel *label = _labelFooterSection[section];
    
    footerView.frame = CGRectMake(0.0, 0.0,
                                  self.view.bounds.size.width,
                                  label.frame.size.height);
    
    [footerView addSubview:label];
    return footerView;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:
        {
            cell.textLabel.text = @"Write a review";
        }
            break;
            
        case 1:
        {
            cell.textLabel.text = @"Write e-mail";
        }
        
        default:
            break;
    }
    
    cell.textLabel.font = [UIFont fontWithName:FT_FontNameDefult size:cell.textLabel.font.pointSize];
    cell.textLabel.textColor = SKCOLOR_TableCellFont;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    if (section == 0 && row == 0) {
        [self goToAppStore];
        
    } else if (section == 1 && row == 0) {
        [self sendMail];
    }
    
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//MARK: App Store Review
- (void) goToAppStore
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", kAppID]]];
}

//MARK: Mail
- (void)sendMail
{
    MFMailComposeViewController *mailClass = [[MFMailComposeViewController alloc] init];
    
    if (mailClass != nil) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            [picker setSubject:@"Super Simple: Feedback"];
            [picker setToRecipients:@[[NSString stringWithFormat:K_EMAIL]]];
            [self presentViewController:picker animated:YES completion:nil];
            
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Can't send e-mail"
                                                                message:@"This device is not configured to send e-mail"
                                                               delegate:self
                                                      cancelButtonTitle:@"Try later"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    NSString *resultTitle = nil;
    NSString *resultMsg = nil;
    NSString *resultMark = nil;
    
    switch (result) {
            
        case MFMailComposeResultCancelled:
            //resultTitle = @"Email прерван";
            //resultMsg = @"Вы прервали отправку письма";
            resultMark = @"Normal";
            break;
            
        case MFMailComposeResultSaved:
            //resultTitle = @"Email сохранен";
            //resultMsg = @"Письмо сохранено в черновиках";
            resultMark = @"Normal";
            break;
            
        case MFMailComposeResultSent:
            //resultTitle = @"Email отослан";
            //resultMsg = @"Письмо успешно отправлено";
            resultMark = @"Normal";
            break;
            
        case MFMailComposeResultFailed:
            resultTitle = @"Not send e-mail";
            resultMsg = @"Not send e-mail Text message";
            resultMark = @"Bad";
            break;
            
        default:
            resultTitle = @"Not send e-mail";
            resultMsg = @"Not send e-mail Text message";
            resultMark = @"Bad";
            break;
    }
    
    if ([resultMark isEqualToString:@"Bad"]) {
        
        UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle:resultTitle
                                                            message:resultMsg
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [mailAlert show];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

//MARK: Navigation
- (void) backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
