//
//  SCButtonItemText.m
//  SmartChat
//
//  Created by Yury Radchenko on 2/16/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCButtonItemText.h"

static const CGFloat kMarge = 0;
static const CGFloat kEdge = 0;
static const CGFloat kLineThickness = 0.5;

@implementation SCButtonItemText

- (instancetype) initWithFrame: (CGRect) frame forList:(SCList *) list itemIndex:(NSInteger) itemIndex {
    
    self = [super initWithFrame:frame forList:list itemIndex:itemIndex];
    
    SCListItem *item = list.items[itemIndex];
    
    //Title
    self.titleLabel.numberOfLines = 2;
    [self setAttributedTitle:[self attribeteText:item.text forState:UIControlStateNormal] forState:UIControlStateNormal];
    [self setAttributedTitle:[self attribeteText:item.text forState:UIControlStateHighlighted] forState:UIControlStateHighlighted];
    
    UIView *lineDown = [[UIView alloc] initWithFrame:CGRectMake(kMarge, self.frame.size.height - kEdge,
                                                                self.frame.size.width - kMarge * 2, kLineThickness)];
    lineDown.backgroundColor = SKCOLOR_ChatInputTextFieldBorder;
    [self addSubview:lineDown];
    
    UIView *lineRight = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - kEdge, kMarge,
                                                                 kLineThickness, self.frame.size.height - kMarge * 2)];
    lineRight.backgroundColor = SKCOLOR_ChatInputTextFieldBorder;
    [self addSubview:lineRight];
    
    return self;
}

- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = SKCOLOR_ChatInputTextFieldBorder;
    }
    else {
        self.backgroundColor = [UIColor clearColor];
    }
}

- (NSMutableAttributedString *) attribeteText:(NSString *) text forState: (UIControlState) controlState
{
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    UIColor *fontColor;
    UIFont *font;
    CGFloat fontSize = 12;
    
    if(controlState == UIControlStateNormal) {
        fontColor = SKCOLOR_FontRegular;
        font = [UIFont fontWithName:FT_FontNameLight size:fontSize];
        
    } else if (controlState == UIControlStateHighlighted) {
        fontColor = SKCOLOR_ChatInputTextFieldBg;
        font = [UIFont fontWithName:FT_FontNameRegular size:fontSize];
        
    } else {
        fontColor = SKCOLOR_FontRegular;
        font = [UIFont fontWithName:FT_FontNameLight size:fontSize];
    }
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:fontColor,
                                NSFontAttributeName: font,
                                NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attribute];
}

- (void) setSettings:(SCSettings *)settings
{
    [super setSettings:settings];
    _settings = settings;
}

@end
