//
//  Localiser.m
//  shoppop
//
//  Created by Yuriy Nezhura on 26.11.14.
//  Copyright (c) 2014 Yuriy Nezhura. All rights reserved.
//

#import "Localiser.h"

static Localiser *localiser = NULL;

@implementation Localiser

+ (id) sharedLocaliser
{
    if (!localiser)
    {
        localiser = [[Localiser alloc] init];
    }
    return localiser;
}


- (id) init
{
    self = [super init];
    if (self)
    {
        _languageDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                         @"en",@"English", 
                         @"fr",@"Français",
                         @"de",@"Deutsch",
                         nil];
        _localizidBandle =[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];

        localiser = self;
    }
    return self;
}

- (NSArray*) getLanguageList
{
    //Может лучше так?
    return [_languageDict allKeys];
    
//    return [NSArray arrayWithObjects: 
//            @"English", //en
//            @"Français", //fr
//            @"Deutsch", //de
//            nil];
}

- (NSArray *) getLanguagesCodesList {
    
    return [_languageDict allValues];
}

-(NSString*) languageNameForKey:(NSString*) langKey
{
    for(NSString *dictKey in _languageDict.allKeys)
    {
        if([[_languageDict objectForKey:dictKey] isEqual:langKey])
            return dictKey;
    }
    return nil;
}

- (NSString*) changeCurrentLanguage:(NSString*) lang
{
    NSString *langKey =[_languageDict objectForKey:lang];
    _localizidBandle =[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:langKey ofType:@"lproj"]];
    return langKey;
}

- (void) setCurrentLanguage:(NSString*) langKey
{
     _localizidBandle =[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:langKey ofType:@"lproj"]];

}
- (NSString*)getLocalisedStringByKey:(NSString*)strKey
{        
    return [_localizidBandle localizedStringForKey:strKey value:strKey table:nil];
}

@end