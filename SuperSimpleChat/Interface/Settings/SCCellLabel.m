//
//  SCCellLabel.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 07.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCCellLabel.h"
#import "SCSettings.h"

@implementation SCCellLabel
{
    SCSettings *_settings;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self _init];
    [self setupUI];
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        [self _init];
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if  (self) {
        [self _init];
        [self setupUI];
    }
    return self;
}

- (void) _init
{
    _settings = [SCSettings sharedSettings];
}

- (void) setupUI
{
    self.textColor = SKCOLOR_TableCellFont;
    [self setFont:[UIFont fontWithName:FT_FontNameDefult size:self.font.pointSize]];
}

@end
