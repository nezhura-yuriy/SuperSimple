//
//  SCChatBottomsView.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"
#import "SCTypes.h"

@class SCSettings,SCMessageBodys;

@interface SCChatBottomsView : UIView
{
//    id<SCChatViewControllerDelegate> _delegate;
    id<SCBottomViewsDelegate> _delegate;
    SCSettings* _settings;
    SCMessageBodys* _messageBodys;
    SCChatType _chatType;
    NSInteger _atTicks;

}

@property(nonatomic,strong) SCSettings* settings;
//@property(nonatomic,weak) id<SCChatViewControllerDelegate> delegate;
@property(nonatomic,strong) id<SCBottomViewsDelegate> delegate;
@property(nonatomic,strong) SCMessageBodys* messageBodys;
@property(nonatomic,assign) SCChatType chatType;
@property(nonatomic,assign) NSInteger atTicks;


-(id) initWithSettings:(SCSettings*) settings;
//-(id) initWithCompletionblock:(void (^)(SCPopChatMenuViewItem selectItem))completionBlock;
//-(void) setAfterSelect:(void(^)(SCPopChatMenuViewItem selectItem)) completionBlock;
-(void) _init;
-(void) updateItems;


-(CGSize) sizeView;
+(CGSize) sizeView;
@end
