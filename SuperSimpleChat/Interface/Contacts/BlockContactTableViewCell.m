//
//  BlockContactTableViewCell.m
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 21.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "BlockContactTableViewCell.h"

@implementation BlockContactTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)blockContactAction:(id)sender {
    
    if([sender isOn]){
        
        SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
        NSDictionary* params = @{@"UUID":transportPUUID,@"contact":self.contact};
        
        [self.settings.modelUsers removeUsersFromBlackListWithParams:params];
        
        self.blockContactLabel.text = @"Block Contact";

        
        
    } else {
        
            SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
            NSDictionary* params = @{@"UUID":transportPUUID,@"contact":self.contact};
        
            [self.settings.modelUsers addUsersToBlackListWithParams:params];
        
        
             self.blockContactLabel.text = @"Unblock Contact";
    }
    

    
}
@end
