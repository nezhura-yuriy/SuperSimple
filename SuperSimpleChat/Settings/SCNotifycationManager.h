//
//  SCNotifycationManager.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 23.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModelsDelegates.h"


@class SCSettings;


@interface SCNotifycationManager : NSObject <SCModelAbstarctDelegate>

@property(nonatomic, strong) SCSettings* settings;

- (instancetype)initWithSettings:(SCSettings*) settings;

-(void) setDeviceNotifycationToken:(NSString*) deviceToken;
-(void) NotifycationTokenRegisterFail:(NSError *)error;
-(void) clearDeviceNotifycationToken;
-(void) receiveRemoteNotification:(NSDictionary*) notify;

-(void) parsedType:(SCPBManagerParceType) type data:(id) data;
@end
