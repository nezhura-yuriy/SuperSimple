//
//  SCHTTPManager.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 12.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCHTTPManager.h"
#import "SCSettings.h"
#import "SCCache.h"

@implementation SCNetError
@end

@interface SCHTTPManager ()

@property (strong, nonatomic) AFHTTPRequestOperationManager* requestOperationManager;
@property (strong, nonatomic) AFHTTPSessionManager* sessionManager;
@property (strong, nonatomic) SCSettings* settings;
@property (strong, nonatomic) SCCache* imageCache;
@property (strong, nonatomic) NSString* tempPath;
@end

@implementation SCHTTPManager

- (instancetype)initWithSettings:(SCSettings*) settings
{
    self = [super init];
    
    if (self) {
        
        self.settings = settings;
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.HTTPMaximumConnectionsPerHost = 4;
        configuration.timeoutIntervalForResource = 120;
        configuration.timeoutIntervalForRequest = 120;

        self.sessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configuration];

        self.requestOperationManager = [[AFHTTPRequestOperationManager alloc]init];
        
        
        self.imageCache = [[SCCache alloc] init];
        
        [self.requestOperationManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [self.requestOperationManager setResponseSerializer:[AFHTTPResponseSerializer serializer]];


        
    }
    return self;
}

-(void) setCachePath:(NSString*) cachePath_
{
    [self.imageCache setCacheImagePath:cachePath_];
}

-(void) setTempPath:(NSString*) tempPath_
{
    _tempPath = tempPath_;
}

-(void)registerUserWithPhoneNumber: (NSString*) phoneNumber
                         onSuccess: (void(^)(NSDictionary* userValue))
                  succes onFailure: (void(^)(NSError* error, NSInteger statusCode)) failure
{

    NSDictionary* data = @{@"mobile":phoneNumber};

    [self.sessionManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    FTLog(@"HTTP_HOST = %@/%@/register",HTTP_HOST,HTTP_AUTH_PATH);
    
    [self.sessionManager POST:[NSString stringWithFormat:@"%@/%@/register",HTTP_HOST,HTTP_AUTH_PATH]
                   parameters:data
                      success:^(NSURLSessionDataTask *task, id responseObject) {
                          
                          if ([responseObject isKindOfClass:[NSDictionary class]])
                          {
                              
                              NSDictionary* userResponse = [NSDictionary dictionaryWithDictionary:responseObject];
                              
                              FTLog(@"JSON: %@",responseObject);
                              
                              if (succes) {
                                  
                                  succes(userResponse);
                              }
                          }
                          else
                          {
                              FTLog(@"responseObject IS NOT Dic");
                              if([responseObject isKindOfClass:[NSData class]]){
                                  FTLog(@"responseObject IS NSData");
                                  NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                  FTLog(@"responseObject %@",result);
                              }
                              [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Not the correct format in the server response" delegate:nil cancelButtonTitle:@"Suspend" otherButtonTitles:nil] show];
                              
                          }

                          
                      }
                      failure:^(NSURLSessionDataTask *task, NSError *error) {
                          
                          FTLog(@"Error: %@", error);

                          
                      }];
    
}




-(void)verificationUserWithSessionId: (NSString*) sessionId
                             smsCode: (NSString*) smsCode
                           onSuccess: (void(^)(NSDictionary* verificationValue))succes
                           onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    
    NSDictionary* data = @{@"confirmationHash": sessionId,
                           @"code": smsCode};
    FTLog(@"NSDictionary: %@",data);
    [self.sessionManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    [self.sessionManager POST:[NSString stringWithFormat:@"%@/%@/register_confirm",HTTP_HOST,HTTP_AUTH_PATH]
                   parameters:data
                      success:^(NSURLSessionDataTask *task, id responseObject) {
                          
                          if ([responseObject isKindOfClass:[NSDictionary class]])
                          {
                              NSDictionary* userResponse = [NSDictionary dictionaryWithDictionary:responseObject];
                              
                              if (succes)
                              {
                                  succes(userResponse);
                              }
                          }
                          else
                          {
                              FTLog(@"responseObject IS NOT Dic");
                              if([responseObject isKindOfClass:[NSData class]])
                              {
                                  FTLog(@"responseObject IS NSData");
                                  NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                  FTLog(@"responseObject %@",result);
                              }
                              [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Not the correct format in the server response" delegate:nil cancelButtonTitle:@"Suspend" otherButtonTitles:nil] show];
                          }

                          
                      }
                      failure:^(NSURLSessionDataTask *task, NSError *error) {
                          
                          FTLog(@"Error: %@", error);
                          
                      }];

}


-(void)registerUserWithEmail: (NSString*) email
                   onSuccess: (void(^)(NSDictionary* registerValue))succes
                   onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    
    NSDictionary* data = @{@"email":email};
    
    [self.sessionManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
     FTLog(@"NSDictionary: %@",data);
    
    [self.sessionManager POST:[NSString stringWithFormat:@"%@/%@/register",HTTP_HOST,HTTP_AUTH_PATH]
                   parameters:data
                      success:^(NSURLSessionDataTask *task, id responseObject) {
                          
                          if ([responseObject isKindOfClass:[NSDictionary class]])
                          {
                              FTLog(@"JSON: %@",responseObject);

                              NSDictionary* userResponse = [NSDictionary dictionaryWithDictionary:responseObject];
                              
                              if (succes)
                              {
                                  succes(userResponse);
                              }
                          }
                          else
                          {
                              FTLog(@"responseObject IS NOT Dic");
                              if([responseObject isKindOfClass:[NSData class]])
                              {
                                  FTLog(@"responseObject IS NSData");
                                  NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                  FTLog(@"responseObject %@",result);
                              }
                              [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Not the correct format in the server response" delegate:nil cancelButtonTitle:@"Suspend" otherButtonTitles:nil] show];
                          }
                          
                      }
                      failure:^(NSURLSessionDataTask *task, NSError *error) {
                          
                          FTLog(@"Error: %@", error);
                          
                      }];

}


-(void)registerUserWithFacebook:(NSString*) tokenString
                      onSuccess: (void(^)(NSDictionary* registerValue))succes
                      onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    
    NSDictionary* data = @{@"facebook":tokenString};
    
    [self.requestOperationManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    FTLog(@"NSDictionary: %@",data);
    
    [self.requestOperationManager POST:[NSString stringWithFormat:@"%@/%@/register",HTTP_HOST,HTTP_AUTH_PATH]
                            parameters:data
                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                   
                                   NSDictionary* userResponse = [NSDictionary dictionaryWithDictionary:responseObject];
                                   
                                   if ([responseObject isKindOfClass:[NSDictionary class]])
                                   {
                                       FTLog(@"NSDictionary responseObject");
                                       FTLog(@"JSON: %@",responseObject);
                                       if (succes)
                                       {
                                           succes(userResponse);
                                       }
                                   }
                                   else
                                   {
                                       FTLog(@"responseObject IS NOT Dic");
                                       if([responseObject isKindOfClass:[NSData class]])
                                       {
                                           FTLog(@"responseObject IS NSData");
                                           NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                           FTLog(@"responseObject %@",result);
                                       }
                                       [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Not the correct format in the server response" delegate:nil cancelButtonTitle:@"Suspend" otherButtonTitles:nil] show];
                                   }
                               }
                               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                   
                                   if (failure)
                                   {
                                       failure(error, operation.response.statusCode);
                                   }
                               }];

    
    
    
}

-(NSString*) fileCachedPath:(NSString*) uuid
{
    return [self.imageCache getFilePath:uuid];
}


/*
curl -i -H "Accept: application/json" \
-H "Content-Type: image/jpeg" \
--data-binary @profile_image.jpeg \
-X POST http://54.148.194.39/api/v1/files?accessToken=54947df8-0e9e-4471-a2f9-9af509fb5889
*/

-(void) fileUpload: (NSData*) fileBody
          fileType: (NSString*) fileType
    progressUpload: (void(^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)) progressUpload
         onSuccess: (void(^)(NSDictionary* verificationValue))succes
         onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{

    [self.requestOperationManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [self.requestOperationManager.requestSerializer setValue:fileType forHTTPHeaderField:@"Content-Type"];
    self.requestOperationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    FTLog(@"UPLOAD HTTP_REQUESR = %@/%@/files/?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,self.settings.modelProfile.sessionID);
    
    NSMutableURLRequest *request =
    [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST"
                                                  URLString:[NSString stringWithFormat:@"%@/%@/files?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,self.settings.modelProfile.sessionID]
                                                 parameters:nil
                                                      error:nil];
    
    [request setHTTPBody:fileBody];
    AFHTTPRequestOperation *requestOperation =
    [self.requestOperationManager HTTPRequestOperationWithRequest:request
                                                          success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        FTLog(@"operation.responseData %@",[[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding]);
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary* userResponse = [NSDictionary dictionaryWithDictionary:responseObject];
            if (succes)
            {
                succes(userResponse);
            }
        }
        else
        {
            FTLog(@"responseObject IS NOT Dic");
            if([responseObject isKindOfClass:[NSData class]])
            {
                FTLog(@"responseObject IS NSData");
                NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                FTLog(@"responseObject %@",result);
            }
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Not the correct format in the server response" delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil] show];
            
        }

    }
                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        FTLog(@"Error: %@", error);
        if (failure)
        {
            failure(error, operation.response.statusCode);
        }
    }];
    
    if(progressUpload)
    {
        [requestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
        {
             progressUpload(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
         }];
    }
    [requestOperation start];
}

/*
wget -v http://54.148.194.39/api/v1/files/b6bbeb90-b372-11e4-b849-31bf946cbfc4?accessToken=54947df8-0e9e-4471-a2f9-9af509fb5889
*/


-(void) fileDownload: (NSString*) uuid
    progressDownload: (void(^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)) progressDownload
           onSuccess: (void(^)(id imgData))succes
           onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    FTLog(@"DOWNLOAD HTTP_REQUESR = %@/%@/files/%@?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,uuid,self.settings.modelProfile.sessionID);
    
    
    NSData *imageData = [self.imageCache getFile:uuid];
    if(imageData)
    {
        if (succes)
        {
            succes(imageData);
        }
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            self.requestOperationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
            NSString* urlString = [NSString stringWithFormat:@"%@/%@/files/%@?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,uuid,self.settings.modelProfile.sessionID];
            FTLog(@"download file = %@",urlString);
            NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            AFHTTPRequestOperation *operation = [self.requestOperationManager HTTPRequestOperationWithRequest:request
                                                                                                      success:^(AFHTTPRequestOperation *operation, id responseObject)
                                                 {
                                                     [self.imageCache saveFile:uuid data:responseObject];
                                                     if (succes)
                                                     {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             succes(responseObject);
                                                         });
                                                     }
                                                 }
                                                                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                                 {
                                                     if (failure)
                                                     {
                                                         failure(error, operation.response.statusCode);
                                                     }
                                                 }];
            
            // Set the progress block of the operation.
            if(progressDownload)
            {
                [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
                 {
                     progressDownload(bytesRead, totalBytesRead, totalBytesExpectedToRead);
                 }];
            }
            [self.requestOperationManager.operationQueue addOperation:operation];
        });
    }
}


-(void) downloadToFile:(NSString*) uuid
//                target:(id) target
              delegate:(id<SCHTTPManagerDelegate>) delegate
{
    NSString* urlString = [NSString stringWithFormat:@"%@/%@/files/%@?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,uuid,self.settings.modelProfile.sessionID];
    FTLog(@"download file = %@",urlString);
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSProgress *progress;

//    if([delegate respondsToSelector:@selector(onChangeStatus:)])
//        [delegate onChangeStatus:SCNetFileStatusWait];

    NSURLSessionDownloadTask *downloadTask =
    [_sessionManager downloadTaskWithRequest:request
                                    progress:&progress
                                 destination:^NSURL *(NSURL *targetPath, NSURLResponse *response)
    {
        FTLog(@"%@",targetPath);
        return [[NSURL fileURLWithPath:self.tempPath] URLByAppendingPathComponent:[targetPath lastPathComponent]];
    }
                           completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error)
    {
        [progress removeObserver:delegate forKeyPath:@"fractionCompleted" context:NULL];
        [progress removeObserver:delegate forKeyPath:@"totalUnitCount" context:NULL];
        [progress removeObserver:delegate forKeyPath:@"completedUnitCount" context:NULL];
        if(error)
        {
            SCNetError* scNetError = [[SCNetError alloc] init];
            scNetError.error = error;
            scNetError.httpResponse = (NSHTTPURLResponse *) response;
            
            if([scNetError.httpResponse statusCode] >= 400)
                FTLog(@"%@ Error, status code %ld",NSStringFromClass([self class]),(long)[scNetError.httpResponse statusCode]);
            else
                FTLog(@"%@ Error",NSStringFromClass([self class]));
            
            if([delegate respondsToSelector:@selector(onFailure:)])
                [delegate onFailure:scNetError];
        }
        else
        {
            FTLog(@"File downloaded to: %@", filePath);
            if([delegate respondsToSelector:@selector(onSuccess:)])
                [delegate onSuccess:[filePath path]];
        }
    }];
    
    [progress addObserver:delegate forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:NULL];
    [progress addObserver:delegate forKeyPath:@"totalUnitCount" options:NSKeyValueObservingOptionNew context:NULL];
    [progress addObserver:delegate forKeyPath:@"completedUnitCount" options:NSKeyValueObservingOptionNew context:NULL];
//    if([delegate respondsToSelector:@selector(changeStatus:)])
//        [delegate onChangeStatus:SCNetFileStatusWait];
    
    [downloadTask resume];

}

-(void) uploadFromFile:(NSString*) fileFoolPath
              fileType:(NSString*) fileType
              delegate:(id<SCHTTPManagerDelegate>) delegate
{
    NSString* urlString = [NSString stringWithFormat:@"%@/%@/files?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,self.settings.modelProfile.sessionID];
    FTLog(@"upload file = %@\nto = %@",fileFoolPath,urlString);
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
    if(fileType.length >1)
        [request setValue:fileType forHTTPHeaderField:@"Content-Type"];
    
    request.HTTPMethod = @"POST";
    NSData* bodyData = [[NSData alloc] initWithContentsOfFile:fileFoolPath];
    if(bodyData && bodyData.length>0)
    {
        [request setHTTPBody:bodyData];
        
    //    FTLog(@"%@",[request HTTPBody]);

        NSProgress *progress = nil;
        NSURLSessionUploadTask* uploadTask = [_sessionManager uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error)
        {
            [progress removeObserver:delegate forKeyPath:@"fractionCompleted" context:NULL];
            [progress removeObserver:delegate forKeyPath:@"totalUnitCount" context:NULL];
            [progress removeObserver:delegate forKeyPath:@"completedUnitCount" context:NULL];
            if(error)
            {
                FTLog(@"%@ Error",NSStringFromClass([self class]));
                SCNetError* scNetError = [[SCNetError alloc] init];
                scNetError.error = error;
                scNetError.httpResponse = (NSHTTPURLResponse *) response;
                if([responseObject isKindOfClass:[NSDictionary class]])
                {
                    scNetError = [responseObject objectForKey:@"errorMessage"];
                }

                if([delegate respondsToSelector:@selector(onFailure:)])
                    [delegate onFailure:scNetError];
            }
            else
            {
                if([responseObject isKindOfClass:[NSDictionary class]])
                {
                    NSString* fileUUID = [responseObject objectForKey:@"uuid"];

                    FTLog(@"File uploaded to: %@", fileUUID);
                    if([delegate respondsToSelector:@selector(onSuccess:)])
                        [delegate onSuccess:fileUUID];
                }
                else
                {
                    FTLog(@"Error parse dictionary %@",responseObject);
                    SCNetError* scNetError = [[SCNetError alloc] init];
                    scNetError.errorString = @"No recieve UUID";
                    if([delegate respondsToSelector:@selector(onFailure:)])
                        [delegate onFailure:scNetError];
                }
            }
        }];
        
        [progress addObserver:delegate forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:NULL];
        [progress addObserver:delegate forKeyPath:@"totalUnitCount" options:NSKeyValueObservingOptionNew context:NULL];
        [progress addObserver:delegate forKeyPath:@"completedUnitCount" options:NSKeyValueObservingOptionNew context:NULL];

        [uploadTask resume];
    }
    else
    {
        SCNetError* scNetError = [[SCNetError alloc] init];
        scNetError.errorString = @"No data to send";
        if([delegate respondsToSelector:@selector(onFailure:)])
            [delegate onFailure:scNetError];

    }
}

/* Для истории. Метод не используется сервером
-(void) fileDelete:(NSString*) fileUUID
         onSuccess:(void(^)(NSDictionary* verificationValue))succes
         onFailure:(void(^)(NSError* error, NSInteger statusCode))failure
{

    [self.sessionManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    FTLog(@"delete uuid %@",[NSString stringWithFormat:@"%@/%@/files/%@?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,fileUUID,self.settings.modelProfile.sessionID]);
    [self.sessionManager DELETE:[NSString stringWithFormat:@"%@/%@/files/%@?accessToken=%@",HTTP_HOST,HTTP_AUTH_PATH,fileUUID,self.settings.modelProfile.sessionID]
                     parameters:nil
                        success:^(NSURLSessionDataTask *task, id responseObject) {
                            
                            if ([responseObject isKindOfClass:[NSDictionary class]])
                                         {
                                             NSDictionary* userResponse = [NSDictionary dictionaryWithDictionary:responseObject];
                                             FTLog(@"JSON: %@",responseObject);
                                             if (succes)
                                             {
                                                 succes(userResponse);
                                             }
                                         } else
                                                      {
                                                          FTLog(@"responseObject IS NOT Dic");
                                                          if([responseObject isKindOfClass:[NSData class]])
                                                          {
                                                              FTLog(@"responseObject IS NSData");
                                                              NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                              FTLog(@"responseObject %@",result);
                                                          }
                                                          [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Not the correct format in the server response" delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil] show];
                                                      }
                            
                        }
                        failure:^(NSURLSessionDataTask *task, NSError *error) {
                            
                            FTLog(@"Error: %@", error);

                        }];
}
*/


@end
