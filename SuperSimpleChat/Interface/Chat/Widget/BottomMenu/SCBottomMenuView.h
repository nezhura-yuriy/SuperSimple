//
//  SCPopSendMenuView.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 15.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"
#import "SCBottomMenuItem.h"
#import "SCMessageBodys.h"
#import "SCModelsDelegates.h"


@interface SCBottomMenuView : SCChatBottomsView <SCBottomMenuItemDelegate,SCMessageBodysDelegate,SCMediaCaptureDelegate>

//@property (nonatomic,strong) SCMessageBodys* messageBodys;


-(CGSize) sizeView;


@end
