//
//  SCMessageBody.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyItem.h"
#import "SCNetFile.h"
#import "SCSettings.h"

@implementation SCMessageBodyItem
{
    BOOL _isInProgressSend;
}

-(id) init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }
    return self;
}

-(void) _init
{
    _netFile = [[SCNetFile alloc] initWithSettings:_settings];
    _isInProgressSend = NO;
}

-(void) dealloc
{
    
}

-(void) setType:(SCDataType)type
{
    _type = type;

    switch (type)
    {
        case SCDataTypeAudio:
        case SCDataTypeAudioFile:
        case SCDataTypeAudioLink:
        case SCDataTypeAudioData:
            _netFile.fileType = SCMediaFileTypeAudio;
            break;
            
        case SCDataTypeVideo:
        case SCDataTypeVideoFile:
        case SCDataTypeVideoLink:
        case SCDataTypeVideoData:
            _netFile.fileType = SCMediaFileTypeVideo;
            break;

        case SCDataTypePhoto:
        case SCDataTypePhotoData:
        case SCDataTypePhotoFile:
        case SCDataTypePhotoLink:
            _netFile.fileType = SCMediaFileTypePhoto;
            break;

        default:
            break;
    }

    if(_data)
        [self setData:_data];
}

-(void) setData:(NSObject *)data
{
    _data = data;
    switch (_type)
    {
//
        case SCDataTypePhoto:
        case SCDataTypeAudio:
        case SCDataTypeVideo:
        {
            if(_netFile.netPath.length < 1 && [data isKindOfClass:[NSString class]])
            {
                
                _netFile.netPath = (NSString*) data;
            }
        }
            break;
            
//
        case SCDataTypePhotoData:
        case SCDataTypeVideoData:
        case SCDataTypeAudioData:
        {
            NSData *fileData = (NSData *)data;
            NSString* theLocalPath = [self makeTempFileName];
            BOOL res = [fileData writeToFile:theLocalPath atomically:YES];
            if(res)
            {
                _netFile.localPath = theLocalPath;
                data = theLocalPath;
                if(_type == SCDataTypePhotoData)
                    _type = SCDataTypePhotoFile;
                else if (_type == SCDataTypeVideoData)
                    _type = SCDataTypeVideoFile;
                else if (_type == SCDataTypeAudioData)
                    _type = SCDataTypeAudioFile;
            }
        }
            break;
            
        case SCDataTypePhotoFile:
        case SCDataTypeAudioFile:
        case SCDataTypeVideoFile:
        {
            if(_netFile.localPath.length < 1 && [data isKindOfClass:[NSString class]])
                _netFile.localPath = (NSString*) data;
        }
            break;
           
//            
        case SCDataTypeAudioLink:
        case SCDataTypeVideoLink:
        case SCDataTypePhotoLink:
        {
            if(_netFile.netPath.length < 1 && [data isKindOfClass:[NSString class]])
                _netFile.netPath = (NSString*) data;
        }
            break;
            
        default:
            break;
    }
    
}

-(void) toCommonDataType
{
    switch (_type)
    {
        case SCDataTypeAudioFile:
            _type = SCDataTypeAudio;
            _data = _netFile.netPath;
            break;
        case SCDataTypeAudioLink:
            _type = SCDataTypeAudio;
            break;
        case SCDataTypeAudio:
            _type = SCDataTypeAudio;
            break;
            
        case SCDataTypePhotoFile:
            _type = SCDataTypePhoto;
            _data = _netFile.netPath;
            break;
        case SCDataTypePhotoLink:
            _type = SCDataTypePhoto;
            break;
        case SCDataTypePhotoData:
            _type = SCDataTypePhoto;
            break;
        case SCDataTypePhoto:
            _type = SCDataTypePhoto;
            break;
            
        case SCDataTypeVideoFile:
            _data = _netFile.netPath;
            _type = SCDataTypeVideo;
            break;
        case SCDataTypeVideoLink:
            _type = SCDataTypeVideo;
            break;
        case SCDataTypeVideo:
            _type = SCDataTypeVideo;
            break;
            
        default: break;
    }
}

// отправка item
-(void) sendItem
{
    if(_isInProgressSend)
        return;
    if(_type == SCDataTypeText)
    {
        for(id<SCMessageBodyItemDelegate> delegate in _delegates)
        {
            if(_delegates && [delegate respondsToSelector:@selector(netFileCompleet)])
                [delegate netFileCompleet];
        }

    }
    
    FTLog(@"Need send file");
    if(_netFile.status == SCNetFileStatusNew)
    {
        if(_netFile.localPath.length > 1 )
        {
            _isInProgressSend = YES;
            [_netFile sendWithCompletionblock:^(SCNetFileComletionType type, id result)
             {
                 switch (type)
                 {
                     case SCNetFileComletionTypeStatus:
                         FTLog(@"SCNetFileComletionTypeStatus");
                         
                         for( NSInteger i = _delegates.count-1 ; i>=0 ; i-- )
                         {
                             id<SCMessageBodyItemDelegate> delegate = [_delegates objectAtIndex:i];
                             if(_delegates && [delegate respondsToSelector:@selector(netFileStatus:)])
                                 [delegate netFileStatus:(SCNetFileStatus) [result integerValue]];
                         }
                         break;
                         
                     case SCNetFileComletionTypeProgress:
                         FTLog(@"Progress %f",[result doubleValue]);
                         for( NSInteger i = _delegates.count-1 ; i>=0 ; i-- )
                         {
                             id<SCMessageBodyItemDelegate> delegate = [_delegates objectAtIndex:i];
                             if(_delegates && [delegate respondsToSelector:@selector(netFileProgress:)])
                                 [delegate netFileProgress:[result doubleValue]];
                         }
                         break;
                         
                     case SCNetFileComletionTypeError:
                         FTLog(@"SCNetFileComletionTypeError");
                         _isInProgressSend = NO;
                         for( NSInteger i = _delegates.count-1 ; i>=0 ; i-- )
                         {
                             id<SCMessageBodyItemDelegate> delegate = [_delegates objectAtIndex:i];
                             if(_delegates && [delegate respondsToSelector:@selector(netFileError)])
                                 [delegate netFileError];
                         }
                         break;
                         
                     case SCNetFileComletionTypeCompleet:
                     {
                         FTLog(@"SCNetFileComletionTypeCompleet");
                         _isInProgressSend = NO;
                         [self toCommonDataType];
                         for( NSInteger i = _delegates.count-1 ; i>=0 ; i-- )
                         {
                             id<SCMessageBodyItemDelegate> delegate = [_delegates objectAtIndex:i];
                             if(_delegates && [delegate respondsToSelector:@selector(netFileCompleet)])
                                 [delegate netFileCompleet];
                         }
                     }
                         break;
                         
                     default: break;
                 }
             }];
        }
        else
        {
            _netFile.status = SCNetFileStatusError;
            for(id<SCMessageBodyItemDelegate> delegate in _delegates)
            {
                if(_delegates && [delegate respondsToSelector:@selector(netFileError)])
                    [delegate netFileError];
            }
        }
    }
}

-(NSString*) makeTempFileName
{
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    switch (_type)
    {
        case SCDataTypeVideoData:
            return [[_settings.mediaTempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"camera_%@", guid]] stringByAppendingPathExtension:@"mov"];
            break;
        case SCDataTypeAudioData:
            return [[_settings.mediaTempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"camera_%@", guid]] stringByAppendingPathExtension:@"wav"];
            break;
        case SCDataTypePhotoData:
            return [[_settings.mediaTempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"camera_%@", guid]] stringByAppendingPathExtension:@"png"];
            break;
            
        default:
            return [_settings.mediaTempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"camera_%@", guid]];
            break;
    }

    return @"";
}


@end
