//
//  SCSBGenderTableViewCell.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSBGenderTableViewCell.h"
#import "SCModelProfile.h"
#import "SCContactItem.h"

typedef NS_ENUM(NSInteger, SCSex) {
    SCSexUnknown = 0,
    SCSexMale,
    SCSexFemale
};

static NSString *kMaleString = @"Male";
static NSString *kFemaleString = @"Female";

@implementation SCSBGenderTableViewCell
{
    SCSex _sex;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
    [self setupUI];
}

- (void) _init
{
    
}

- (void) setupUI
{
    [self setupButtons];
}

- (void) setupButtons {
    
    if (_sex == SCSexMale) {
        [_maleButton setImage:[_settings.skinManager getImageForKeyX:@"gender-man-active"]
                     forState:UIControlStateNormal];
    } else {
        [_maleButton setImage:[_settings.skinManager getImageForKeyX:@"gender-man"]
                     forState:UIControlStateNormal];
    }

    if (_sex == SCSexFemale) {
        [_femaleButton setImage:[_settings.skinManager getImageForKeyX:@"gender-woman-active"]
                       forState:UIControlStateNormal];
    } else {
        [_femaleButton setImage:[_settings.skinManager getImageForKeyX:@"gender-woman"]
                       forState:UIControlStateNormal];
    }
}


- (instancetype)init
{
    [self _init];
    self = [super init];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    [self _init];
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    _icoImageVIew.frame = CGRectMake(_edgeInsets.left, 0.0,
                                     self.contentView.frame.size.height, self.contentView.frame.size.height);
    
    CGSize buttonWidth = _femaleButton.imageView.image.size;
    
    _femaleButton.frame = CGRectMake(self.contentView.frame.size.width - _edgeInsets.right - buttonWidth.width, 0.0,
                                     buttonWidth.width, self.contentView.frame.size.height);
    
    _maleButton.frame = CGRectMake(_femaleButton.frame.origin.x - buttonWidth.width * 2, 0.0,
                                   buttonWidth.width, self.contentView.frame.size.height);
    
    CGFloat titleLabelX = _icoImageVIew.frame.origin.x + _icoImageVIew.frame.size.width;
    
    _titleLabel.frame = CGRectMake(titleLabelX, 0.0,
                                   _maleButton.frame.origin.x - titleLabelX, self.contentView.frame.size.height);
}

//MARK: Sets
- (void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    NSString *sexString = _settings.modelProfile.contactItem.contactSex;
    
    if ([sexString isEqualToString:kMaleString]) {
        _sex = SCSexMale;
        
    } else if ([sexString isEqualToString:kFemaleString]) {
        _sex = SCSexFemale;
        
    } else {
        _sex = SCSexUnknown;
    }
    
    _icoImageVIew.image = [_settings.skinManager getImageForKeyX:@"profile-gender"];
    _titleLabel.textColor = SKCOLOR_TableCellFont;
    [self setupButtons];
    
}

//MARK: Action
- (IBAction)maleButtonPressed:(id)sender
{
    if (_sex != SCSexMale) {
        _sex = SCSexMale;
        [self setupButtons];
        [_delegate setupGender:kMaleString];
    }
}

- (IBAction)femaleButtonPressed:(id)sender
{
    if (_sex != SCSexFemale) {
        _sex = SCSexFemale;
        [self setupButtons];
        [_delegate setupGender:kFemaleString];
    }
}
@end
