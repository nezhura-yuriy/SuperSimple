//
//  SCUserGroup.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCContactGroup.h"

@implementation SCContactGroup


-(id) init
{
    self = [super init];
    if (self )
    {
        
    }
    return self;
}

-(void) _init
{
    
}

-(void) dealloc
{
    
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    _grContID = [dictionary objectForKey:@"grContID"];
    _grContParentID = [dictionary objectForKey:@"grContParentID"];
    _grContName = [dictionary objectForKey:@"grContName"];
    _grContImageLink = [dictionary objectForKey:@"grContImageLink"];
}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    [outDict setObject:_grContID forKey:@"grContID"];
    [outDict setObject:_grContParentID forKey:@"grContParentID"];
    [outDict setObject:_grContName forKey:@"grContName"];
    [outDict setObject:_grContImageLink forKey:@"grContImageLink"];
    
    return outDict;
}

@end
