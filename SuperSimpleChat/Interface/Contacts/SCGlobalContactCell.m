//
//  SCGlobalContactCell.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 05.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCGlobalContactCell.h"
#import "SCSettings.h"
#import "SCContactItem.h"
#import "SCmodelContacts.h"
#import "SCPUUID.h"
#import "NSString+SCContains.h"

@implementation SCGlobalContactCell

{
    SCSettings* _settings;
}

- (void)awakeFromNib {
    // Initialization code

    _settings = [SCSettings sharedSettings];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) fillWhithContact: (SCContactItem*) contact{
    
    _item = contact;

    self.fullNameOutlet.font = [UIFont fontWithName:FT_FontNameDefult size:15];
    self.fullNameOutlet.textColor = SKCOLOR_TableCellFont;
    self.backgroundColor = SKCOLOR_TableCellBg;
    
    //=============================================================================
    
 if (!contact.contactLastName.length==0 || !contact.contactFirstName.length==0){
        
        
        NSString* fullName = [NSString new];
        
        if (contact.contactLastName.length > 0) {
            
            fullName = [NSString stringWithFormat:@"%@ ",contact.contactLastName];
            
        }
        
        if (contact.contactFirstName.length > 0) {
            
            fullName = [fullName stringByAppendingString:contact.contactFirstName];
            
        }
        
        self.fullNameOutlet.text = fullName;
        
        
 }  else if (!contact.contactNikName.length == 0){
     
     
     self.fullNameOutlet.text = contact.contactNikName;
     
     
 } else  {
        
        self.fullNameOutlet.text = self.searchAuth;

    }
    
}

- (IBAction)addUserAction:(id)sender {
    
    
    NSArray* existContacts = [_settings.modelContacts items:nil];
    
    for (SCContactItem* contact in existContacts) {
        
        if ([contact.userPUUID isEqual:_item.userPUUID] ) {

            
            [[[UIAlertView alloc] initWithTitle:@"Error"
                                       message:@"User is already in your contact list!"
                                      delegate:nil
                             cancelButtonTitle:@"Ok"
                             otherButtonTitles: nil] show];
            
            return;
            
        }
    }
    
            if (_item) {
                
                if ([self.searchAuth isEqualToString:self.fullNameOutlet.text]) {
                    
                    _item.contactFirstName = self.searchAuth;
                    
                }
                
                if ([_searchAuth myContainsString:@"@"]) {
                    
                    _item.contactEmail = self.searchAuth;
                    
                } else {
                    
                    _item.contactPhone = self.searchAuth;
                }
                
                SCPUUID* transportPUUID =  [[SCPUUID alloc] initWithRandom];
                
                NSDictionary* paramsForCreat = @{@"fieldsForCreatOneContact":_item, @"PUUID":transportPUUID, @"delegate":_settings.modelContacts};
                
                [_settings.modelContacts createNewContactWithParams:[NSMutableDictionary dictionaryWithDictionary:paramsForCreat]];
                
            }
    
}

-(void)prepareForReuse {
    
    self.addUserButtonOutlet.enabled = YES;
        
    [self.addUserButtonOutlet setImage:[UIImage imageNamed:@"Shape-1"] forState:UIControlStateNormal];
}


@end
