//
//  SCSBBirthdayTableViewCell.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSBBirthdayTableViewCell.h"
#import "SCModelProfile.h"
#import "SCContactItem.h"

@implementation SCSBBirthdayTableViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
    [self setupUI];
}

- (void) _init
{
    
}

- (void) setupUI
{
    self.accessoryType = UITableViewCellAccessoryNone;
}

- (instancetype)init
{
    [self _init];
    self = [super init];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    [self _init];
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    _icoImageView.frame = CGRectMake(_edgeInsets.left, 0.0,
                                     self.contentView.frame.size.height, self.contentView.frame.size.height);
    
    CGFloat titleLabelX = _icoImageView.frame.origin.x + _icoImageView.frame.size.width;
    _titleLabel.frame = CGRectMake(titleLabelX, 0.0,
                                   self.contentView.frame.size.width - _edgeInsets.right - titleLabelX,
                                   self.contentView.frame.size.height);
    
    _dateBirthdayLabel.frame = CGRectMake(self.contentView.frame.size.width * 0.5, 0.0,
                                          self.contentView.frame.size.width * 0.5 - _edgeInsets.right,
                                          self.contentView.frame.size.height);
}

- (void) setBirthdate:(NSDate *)birthdate
{
    _birthdate = birthdate;
    
    if (_birthdate) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NO];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        
        _dateBirthdayLabel.text = [dateFormatter stringFromDate:_birthdate];
    }
    else {
        _dateBirthdayLabel.text = @"";
    }
    
    _icoImageView.image = [_settings.skinManager getImageForKeyX:@"profile-birthday"];
    _titleLabel.textColor = SKCOLOR_TableCellFont;
}

@end
