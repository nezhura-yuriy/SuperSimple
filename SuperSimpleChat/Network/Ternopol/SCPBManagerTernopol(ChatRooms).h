//
//  SCPBManagerTernopol+ChatRooms.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol.h"

@interface SCPBManagerTernopol (ChatRooms)

-(void) parseCreateChatroom:(id)data;
-(void) parseGetChatRoomByIdResponse:(id)data;
-(void) parseRemoveChatroom:(id)data;
-(void) parseChatRoomActivate:(id)data;
-(void) parseChangeChatRoom:(id)data;
-(void) parseGetAllRoomsForUser:(id)data;
-(void) parseSeachPublicChatRooms:(id)data;
-(void) parseGetAllUsersForRooms:(id)data;
-(void) parseJoinUserToRoom:(id)data;
-(void) parseJoinUsersToRoom:(id)data;
-(void) parseLeaveUserFromRoom:(id)data;
-(void) parseGetLastReadMessageDate:(id)data;
-(void) parseSetLastReadMessageDate:(id)data;
-(void) parseChangeRoomEventResponse:(id)data;
-(void) parseJoinRoomEventResponse:(id)data;
-(void) parseLeaveRoomEventResponse:(id)data;
-(void) parseRemoveRoomEventResponse:(id)data;

@end
