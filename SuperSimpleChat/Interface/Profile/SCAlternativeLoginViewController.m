   //
//  SCAlternativeLoginViewController.m
//  SuperSimpleChat
//
//  Created by yury on 12.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAlternativeLoginViewController.h"
#import "SCAuthTableViewCell.h"
#import "SCAddAuthTableViewCell.h"
#import "SCContactItem.h"
#import "SCUserAuthItem.h"
#import <SVProgressHUD.h>
#import "NSString+SCContains.h"


typedef enum  {
    
    SCSectionPhoneAuth,
    SCSectionMailAuth
    
} SCSection;


@interface SCAlternativeLoginViewController ()<SCChatItemDelegate,UIAlertViewDelegate,SCAuthTableViewCelllDelegate>


@property (strong, nonatomic) NSMutableArray* auths;
@property (strong, nonatomic) NSMutableArray* authMail;
@property (strong, nonatomic) NSMutableArray* authPhone;


@end


@implementation SCAlternativeLoginViewController


#pragma mark - Lifecycle


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupNavigation];
    
    self.authPhone = [[NSMutableArray alloc]init];
    self.authMail = [[NSMutableArray alloc]init];
    
    _contactItem = _settings.modelProfile.contactItem;
    
    [self sortAuth:_contactItem.userAuths];
    
}


-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
    
   [_settings.modelProfile.contactItem addDelegate:self];
    
}


-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    [_settings.modelProfile.contactItem delDelegate:self];
    
}


#pragma mark - SCChatItemDelegate

-(void)userAuthUpdate:(NSArray *)userAuthes{
    
    [self sortAuth:_contactItem.userAuths];
    [self.tableView reloadData];

}

#pragma mark - Privet Methods

-(void)sortAuth:(NSArray*)userAuthes{
    
    [self.authPhone removeAllObjects];
    [self.authMail removeAllObjects];

    
    for (SCUserAuthItem* auth in userAuthes) {
        
        if (auth.authType == SCUserAuthItemTypePhone) {
            
            [self.authPhone addObject:auth];
            
        } else if (auth.authType == SCUserAuthItemTypeEmail){
            
            [self.authMail addObject:auth];
        }
    }

}


//MARK:Setups
- (void) setupNavigation
{
    self.title = @"Alternative login";
    
    //Back button
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == SCSectionPhoneAuth) {
        
        if (self.authPhone.count>0) {
          
            return self.authPhone.count+1;
            
        } else {
            
            return 1;
        }

           }
    
    if (section == SCSectionMailAuth) {
        
        if (self.authMail.count>0) {
            
            return self.authMail.count+1;
            
        } else {
            
            return 1;
        }
        
    }
    
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *autCell = @"authCell";
    
    static NSString *addAuthCell = @"addAuthCell";
    
    if (indexPath.section == SCSectionPhoneAuth) {
        
        if (indexPath.row == self.authPhone.count || self.authPhone.count == 0 ) {
            
            SCAddAuthTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:addAuthCell];
            
            return cell;
            
        } else {
            
            SCAuthTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:autCell];
            
           SCUserAuthItem* auth = [self.authPhone objectAtIndex:indexPath.row];
            
            if (auth.isConfirm) {
                
                cell.confirmButton.hidden = YES;
            }
            
            cell.authLabel.text = auth.data;
            
            cell.indexPath = indexPath;
            
            cell.auth = auth;
            
            cell.delegate = self;
            
            return cell;
        }
        
    } else {
        
        if (indexPath.row == self.authMail.count ||  self.authMail.count == 0 ) {
            
            SCAddAuthTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:addAuthCell];
            
            cell.addAuthLabel.text = @"Add email";
            
            return cell;
            
        } else {
            
            SCAuthTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:autCell];
            
            SCUserAuthItem* auth = [self.authMail objectAtIndex:indexPath.row];
            
            if (auth.isConfirm) {
                
                cell.confirmButton.hidden = YES;
            }
            
            cell.indexPath = indexPath;
            
            cell.delegate = self;
            
            cell.auth = auth;

            cell.authLabel.text = auth.data;
            
            return cell;
        }
    }
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (section == SCSectionPhoneAuth) {
        
        return @"Auths Phone";
        
    } else
        
        return @"Auths Email";
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == SCSectionPhoneAuth) {
        
        if (indexPath.row == self.authPhone.count || self.authPhone.count == 0 ) {
            
            return NO;
            
                    }
    } else {
        
        if (indexPath.row == self.authMail.count ||  self.authMail.count == 0 ) {
            
            
            return NO;
        }
        
    }
    
        return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (indexPath.section == SCSectionPhoneAuth) {
            
            if (!indexPath.row == self.authPhone.count || !self.authPhone.count == 0 ) {
                
                
                 SCUserAuthItem* auth = [self.authPhone objectAtIndex:indexPath.row];
                
                [_settings.modelProfile authRemove:auth];
                
                 [self.authPhone removeObjectAtIndex:indexPath.row];
                
                 [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                
            }
            
        } else {
            
            if (!indexPath.row == self.authMail.count ||  !self.authMail.count == 0 ) {
                
                SCUserAuthItem* auth = [self.authMail objectAtIndex:indexPath.row];
                
                [_settings.modelProfile authRemove:auth];

                [self.authMail removeObjectAtIndex:indexPath.row];

                 [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
  }
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.section == SCSectionPhoneAuth) {
        
        if (indexPath.row == self.authPhone.count || self.authPhone.count == 0 ) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter phone number"
                                                            message:nil
                                                           delegate:nil
                                                  cancelButtonTitle:@"Сancel"
                                                  otherButtonTitles: @"Ок", nil];
            alert.delegate = self;
            alert.alertViewStyle= UIAlertViewStylePlainTextInput;
            [alert show];
        }
        
    } else {
        
        if (indexPath.row == self.authMail.count ||  self.authMail.count == 0 ) {
            
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter email"
                                                                message:nil
                                                               delegate:nil
                                                      cancelButtonTitle:@"Сancel"
                                                      otherButtonTitles: @"Ок", nil];
                alert.delegate = self;
                alert.alertViewStyle= UIAlertViewStylePlainTextInput;
                [alert show];

        }
        
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UITextField *authTextField = [alertView textFieldAtIndex:0];
    
    NSString *authValue = authTextField.text;
    
    SCUserAuthItem* auth = [[SCUserAuthItem alloc]init];
    
    if (authValue.length>0) {
        
        if ([authValue myContainsString:@"@"]) {
            
            auth.authType = SCUserAuthItemTypeEmail;
            auth.data = authValue;
            
            [_settings.modelProfile authAddWithString:auth];
            
        } else {
            
            auth.authType = SCUserAuthItemTypePhone;
            auth.data = authValue;
            
            [_settings.modelProfile authAddWithString:auth];
            
        }

    }

}


#pragma mark - SCModelAbstarctDelegate

-(void) showError:(NSDictionary*) errorDictionary{
   
    [SVProgressHUD showErrorWithStatus:[errorDictionary objectForKey:@"ErrorDescription"]];
    
    [self sortAuth:_contactItem.userAuths];
    
    [self.tableView reloadData];
    
}


#pragma mark - SCAuthTableViewCelllDelegate

-(void) confirmAuth:(SCUserAuthItem*) auth withCode:(NSString*) code{
    
    [_settings.modelProfile authConfirmWithAuthItem:auth andVerifyCode:code];
    
}



@end
