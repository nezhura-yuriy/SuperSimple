//
//  SCContactsViewController.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 13.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCContactsViewController.h"
#import "SCSettings.h"
#import "SCContactCell.h"
#import "SCGlobalSearchViewController.h"
#import <AddressBook/AddressBook.h>
#import "Macro.h"
#import "Localiser.h"
#import "SCmodelContacts.h"
#import "SCContactItem.h"
#import "ContactInformationViewController.h"
#import <SVProgressHUD.h>
#import "SCSegmentControll.h"
#import "SCAddressBookContactCell.h"
#import "SCAddressbookContact.h"
#import <MessageUI/MessageUI.h>
#import "SCCoutrySection.h"
#import "SCChatItem.h"
#import "SCTextField.h"
#import "SCLabel.h"


@interface SCContactsViewController () < UITextFieldDelegate, SCmodelContactsDelegate,UIActionSheetDelegate,SCContactCellDelegate,SCAddressBookContactCellDelegate>

@property (strong, nonatomic) NSMutableArray* contacts;
@property (strong, nonatomic) NSMutableArray* localAddressbookContacts;
@property (strong, nonatomic) NSMutableArray* notRegisterUsers;
@property (strong, nonatomic) NSMutableArray* tempArray;

@property (strong, nonatomic) NSArray* titelLastNameContacts;
@property (strong, nonatomic) NSString * addressBookNum;
@property (strong, nonatomic) Localiser* localiser;
@property (strong, nonatomic) SCSegmentControll* segmentControll;
@property (strong, nonatomic) SCTextField* searchField;
@property (strong, nonatomic) SCLabel* toLabel;
@property (strong, nonatomic) UIView* searchView;

@property (strong, nonatomic) NSOperation* currenTOperation;
@property (strong, nonatomic) NSMutableArray* sectionsArray;

@end


@implementation SCContactsViewController


#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateContacts)
                                                 name:@"updateContacts"
                                               object:nil];
    

    self.settings = [SCSettings sharedSettings];
    self.localiser = [Localiser sharedLocaliser];
    self.contacts = [NSMutableArray array];
    self.contacts = [_settings.modelContacts loadCached];
    [_tableView registerNib:[UINib nibWithNibName:@"SCContactCell" bundle:nil] forCellReuseIdentifier:@"contactItem"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SCAddressBookContactCell" bundle:nil] forCellReuseIdentifier:@"addressBookContactCell"];
    
    [SVProgressHUD setBackgroundColor:SKCOLOR_NavBarBg];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
    {
        self.tableView.hidden = YES;
        self.accessView.hidden = NO;

        [self.activitiIndicator startAnimating];
    }
    
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        
        [self.activitiIndicator startAnimating];
        
    }
    else
    {
        
        self.tableView.hidden = YES;
        self.accessView.hidden = NO;
        
    }
    self.settings.modelContacts.delegate = self;
    [_settings.modelContacts getAllContacts:nil];
    
    
    [self changTableViewHeader];
    
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexColor = [UIColor grayColor];
    
    
    SCSegmentControll *statFilter = [[SCSegmentControll alloc] initWithItems:[NSArray arrayWithObjects:@"With SuperSimple", @"Without SuperSimple", nil]];
    [statFilter sizeToFit];
    statFilter.selectedSegmentIndex = 0;
    statFilter.frame = CGRectMake(0, 0, self.view.frame.size.width, 3);
    [statFilter addTarget:self action:@selector(segmentSwitch:) forControlEvents:UIControlEventValueChanged];
    self.segmentControll = statFilter;
    
    [self.view addSubview:statFilter];
    
    self.tableView.frame = CGRectMake(0,self.segmentControll.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - SZ_TAB_BAR_HEIGHT - self.segmentControll.frame.size.height);
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [self.tableView reloadData];
    [refreshControl endRefreshing];
}

-(void)viewWillAppear:(BOOL)animated

{
    [super viewWillAppear:animated];
    
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.tabBarController.navigationItem.title = @"Contacts";
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBarTintColor:SKCOLOR_NavBarBg];
    self.navigationController.navigationBar.translucent = NO;
    
    self.tableView.backgroundColor = SKCOLOR_TabBarBg;
    self.view.backgroundColor = SKCOLOR_TabBarBg;
    self.tableView.sectionIndexColor = SKCOLOR_TableCountrySectionIndexFont;
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    
    self.settings.modelContacts.delegate = self;

    [self settingNavigationBar];
    [self.tableView reloadData];
}




#pragma mark - Notification

- (void)notificationChangeSkin:(NSNotification *)note
{

//    self.tableView.backgroundColor = CL_TableBackgroundColor;
//    self.view.backgroundColor = CL_TableBackgroundColor;
//    self.tableView.sectionIndexColor = CL_FontDefaultColor;
//    self.tableView.sectionIndexBackgroundColor = CL_CLEAR_COLOR;
//    self.tableView.separatorColor = CL_TableCellSeparatorColor;
//    [self.tableView reloadData];

}


-(void)updateContacts{
    
    self.contacts = [[self.settings.modelContacts items:nil] mutableCopy];
    
    if (self.segmentControll.selectedSegmentIndex == 0) {
        
        [self generateSectionsInBackgroundArrayContacts:self.contacts withFilter:nil];
        
        
    } else {
        
        [self generateSectionsInBackgroundArrayLocalAddressbook:self.localAddressbookContacts withFilter:nil];
        
    }
    
    [self.tableView reloadData];
    
}

#pragma mark - UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionsArray count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.sectionsArray objectAtIndex:section] sectionName];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:section];
    return [sections.itemsArray count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *contactCellIdentifier = @"contactItem";
    static NSString *addressbookcellIdentifier = @"addressBookContactCell";
    
    if (self.segmentControll.selectedSegmentIndex == 0)
    {
        SCContactCell* cell = [tableView dequeueReusableCellWithIdentifier:contactCellIdentifier];
    
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"SCContactCell" bundle:nil] forCellReuseIdentifier:@"contactItem"];
            cell = [tableView dequeueReusableCellWithIdentifier:contactCellIdentifier];
        }
        
        SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
        SCContactItem* contact = [sections.itemsArray objectAtIndex:indexPath.row];
        [cell fillWhithContact:contact];
        cell.accessoryView = [self makeDetailDisclosureButton];
        cell.indexPath = indexPath;
        cell.delegate = self;
        return cell;
        
    }
    else if (self.segmentControll.selectedSegmentIndex == 1)
    {
        SCAddressBookContactCell* cell = [tableView dequeueReusableCellWithIdentifier:addressbookcellIdentifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"SCAddressBookContactCell" bundle:nil] forCellReuseIdentifier:addressbookcellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:addressbookcellIdentifier];
        }
        
        SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
        SCAddressbookContact* contact = [sections.itemsArray objectAtIndex:indexPath.row];
        [cell fillWhithContact:contact];
        cell.delegate = self;
        cell.indexPath = indexPath;
        return cell;
    }
    return nil;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.segmentControll.selectedSegmentIndex == 0) {
        
        return YES;
    }
    
    return NO;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
                    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
        
                    SCContactItem* contactForRemove = [sections.itemsArray objectAtIndex:indexPath.row];
        
                    [sections.itemsArray removeObjectAtIndex:indexPath.row];
        
                    if (sections.itemsArray.count == 0) {
        
                        [self.sectionsArray removeObject:sections];
        
                        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        
                    } else {
        
                        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
                    }
        
                    for (SCContactItem* contat in self.contacts) {
        
                        if ([contactForRemove isEqual:contat]) {
                            
                            [self.contacts removeObject:contat];
                            
                            break;
                        }
                        
                    }
        
        [self.settings.modelContacts removeContact:contactForRemove];
      
    }
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    NSMutableArray* array = [NSMutableArray array];
    
    for(SCCoutrySection* section in self.sectionsArray){
        
        [array addObject:section.sectionName];
    }
    
    return array;
    
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
     UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    [header.textLabel setFont:[UIFont fontWithName:FT_FontNameBold size:header.textLabel.font.pointSize]];
    
    header.textLabel.textColor = SKCOLOR_TableCountryHeaderFont;
    
    header.contentView.backgroundColor = SKCOLOR_TableCountryHeaderBg;
    
} 


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (UIButton *) makeDetailDisclosureButton
{
    
    UIButton *accessoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [accessoryButton setImage:[UIImage imageNamed:@"accessory"] forState:UIControlStateNormal];
    accessoryButton.frame = CGRectMake(0, 0, 11, 18);
    
    [accessoryButton addTarget: self
                        action: @selector(accessoryButtonTapped:withEvent:)
              forControlEvents: UIControlEventTouchUpInside];
    
    return (accessoryButton);
}


- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event
{
    NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: self.tableView]];
    
    if (indexPath == nil)
        
        return;
    
    [self.tableView.delegate tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    SCContactCell* cell = (SCContactCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    SCContactItem* contact = cell.contact;
    
    if ([contact.userPUUID isEqual:_settings.modelProfile.contactItem.userPUUID]) {
        
     [self.tabBarController setSelectedIndex:3];
        
    } else {
        
        ContactInformationViewController* controller = [[[NSBundle mainBundle] loadNibNamed:@"ContactInformationViewController" owner:self options:nil] lastObject];
        
        
        controller.settings = _settings;
        
        controller.contact = contact;
        
        
        [self.tabBarController.navigationController pushViewController:controller animated:YES];
        
    }
    
}


#pragma mark - UIActionSheetDelegate


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == actionSheet.cancelButtonIndex) return;
    
    buttonIndex = buttonIndex - 1;
    
    NSString* auth = self.tempArray[buttonIndex];
    
    NSMutableArray* arrayForInvite = [NSMutableArray new];
    
    [arrayForInvite addObject:auth];
    
    [self.settings.modelContacts inviteUsers:arrayForInvite];
}


#pragma mark - Invite Methode


-(void)inviteContactAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.segmentControll.selectedSegmentIndex == 1){
        
        SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
        
        SCAddressbookContact* selectedContact = [sections.itemsArray objectAtIndex:indexPath.row];
        
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"
                                              destructiveButtonTitle:nil otherButtonTitles:nil];
        
        self.tempArray = [NSMutableArray array];
        
        if (selectedContact.contactPhoneNumbers.count > 0) {
            
            for (NSString *phone in selectedContact.contactPhoneNumbers) {
                
                [action addButtonWithTitle:phone];
                
                [self.tempArray addObject:phone];
                
            }
            
        }
        
        if (selectedContact.contactEmail.count>0) {
            
            for (NSString *email in selectedContact.contactEmail) {
                
                [action addButtonWithTitle:email];
                
                [self.tempArray addObject:email];
                
            }
        }
        
        [action showInView:self.view];
        
    }
    
}


-(void)inviteAllAddressbookContacts {
    
    NSMutableArray* inviteAuthAllAddressbookContacts = [NSMutableArray array];
    
    for (SCAddressbookContact* contact in self.localAddressbookContacts) {
        
        if (contact.contactPhoneNumbers.count > 0) {
            
            for (NSString* phone in contact.contactPhoneNumbers){
                
                [inviteAuthAllAddressbookContacts addObject:phone];
            }
        }
        
        if (contact.contactEmail.count > 0 ) {
            
            for (NSString* email in contact.contactEmail) {
                
                [inviteAuthAllAddressbookContacts addObject:email];
            }
        }
    }
    
    [self.settings.modelContacts inviteUsers:inviteAuthAllAddressbookContacts];
}

#pragma mark - Actoins

-(void)globalSearchAction{
    
    SCGlobalSearchViewController* globalSearch = [[[NSBundle mainBundle] loadNibNamed:@"SCGlobalSearchViewController" owner:self options:nil] lastObject];
    
    globalSearch.settings = _settings;
    
    [self.tabBarController.navigationController pushViewController:globalSearch animated:YES];

}


#pragma mark - Initialization methods

- (void)settingNavigationBar {
    
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    UIImage *backButtonImage = [UIImage imageNamed:@"search"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self action:@selector(globalSearchAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.tabBarController.navigationItem.rightBarButtonItem = backBarButtonItem;

    
    
   
}


#pragma mark - SCmodelContactsDelegate

-(void) getContactsFromModel:(NSMutableArray*) contacts{
    
    [self.activitiIndicator stopAnimating];
    
    if (contacts.count>0) {
        
        self.accessView.hidden = YES;
        
        self.tableView.hidden = NO;
        
        self.contacts = contacts;
        
        self.localAddressbookContacts = [self separateAdddressbookContacts:[self.settings.modelAddressbook returnAddressbook]];
        
        [self creatFullNameAddressbookContacts];
        
        [self.settings.modelContacts saveContacts:contacts];
        
        
        if (self.segmentControll.selectedSegmentIndex == 0) {
            
            [self generateSectionsInBackgroundArrayContacts:self.contacts withFilter:nil];
 
            
        } else {
            
            [self generateSectionsInBackgroundArrayLocalAddressbook:self.localAddressbookContacts withFilter:nil];
            
        }
        
        [self.tableView reloadData];
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        
        self.accessView.hidden = YES;
        
        self.tableView.hidden = NO;
        
        self.localAddressbookContacts = [self separateAdddressbookContacts:[self.settings.modelAddressbook returnAddressbook]];
        
        [self creatFullNameAddressbookContacts];

        [self.tableView reloadData];

    }
}


-(void)creatFullNameAddressbookContacts{
    
    for (SCAddressbookContact* contact in self.localAddressbookContacts) {
        
        NSString* fullName = [NSString new];
        
        if (contact.contactLastName.length > 0) {
            
            fullName = [NSString stringWithFormat:@"%@ ",contact.contactLastName];
        }

        if (contact.contactFirstName.length > 0) {
            
            fullName = [fullName stringByAppendingString:contact.contactFirstName];
            
        }
        
        contact.fullName = fullName;

        
    }
    
}


-(void) successDeleteContact
{
    [SVProgressHUD showSuccessWithStatus:@"Contact Delete"];
}


-(void)successInvite:(id)data
{
    [SVProgressHUD showSuccessWithStatus:@"Invited"];
}


-(void) errorDeleteContact:(NSString*)errorMessage{
    
    [SVProgressHUD showErrorWithStatus:errorMessage];
    
    self.contacts = [[self.settings.modelContacts items:nil] mutableCopy];
    
    if (self.segmentControll.selectedSegmentIndex == 0) {
        
        [self generateSectionsInBackgroundArrayContacts:self.contacts withFilter:nil];
        
        
    } else {
        
        [self generateSectionsInBackgroundArrayLocalAddressbook:self.localAddressbookContacts withFilter:nil];
        
    }
    
    [self.tableView reloadData];
}


#pragma mark - PrivetMethods


-(void)changTableViewHeader{
    
    [self setupHeaderView];
    
    if (self.segmentControll.selectedSegmentIndex == 0) {

        [self.inviteAllContactsButton removeFromSuperview];
        [self.viewTableHeader addSubview:self.searchView];
        
    } else {
        
        [self.searchView removeFromSuperview];
        [self.viewTableHeader addSubview:self.inviteAllContactsButton];
        
    }
    
}

- (void)setupHeaderView {
    
    self.viewTableHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    self.tableView.tableHeaderView = self.viewTableHeader;

    self.inviteAllContactsButton= [[UIButton alloc]initWithFrame:CGRectMake(self.tableView.frame.size.width/2-80, 7, 160, 30)];
    self.inviteAllContactsButton.backgroundColor = [UIColor whiteColor];
    self.inviteAllContactsButton.layer.borderWidth = 1;
    self.inviteAllContactsButton.layer.cornerRadius = 2;
    self.inviteAllContactsButton.layer.borderColor = [SKCOLOR_NavBarBg CGColor];
    [self.inviteAllContactsButton setTitle:@"Invite all contacts" forState:UIControlStateNormal];
    [self.inviteAllContactsButton setTitleColor:SKCOLOR_NavBarBg forState:UIControlStateNormal];
    [self.inviteAllContactsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.inviteAllContactsButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [self.inviteAllContactsButton addTarget:self action:@selector(inviteAllAddressbookContacts) forControlEvents:UIControlEventTouchUpInside];
    
    self.searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    self.searchView.backgroundColor = [UIColor colorWithHue:0.41 saturation:0 brightness:0.943 alpha:1];
    
    self.searchField = [[SCTextField alloc] initWithFrame:CGRectMake(44, 0, self.tableView.frame.size.width-44, 44)];
    self.searchField.placeholder = @"Search contact";
    self.searchField.delegate = self;
    [self.searchField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.searchField.returnKeyType = UIReturnKeyDone;
    
    self.toLabel = [[SCLabel alloc]initWithFrame:CGRectMake(10, 0, 44, 44)];
    self.toLabel.text = @"To:";
    self.toLabel.textColor = [UIColor lightGrayColor];
    
    [self.searchView addSubview:self.toLabel];
    [self.searchView addSubview:self.searchField];
    

}


-(NSMutableArray*) generateSectionsFromArrayContacts:(NSMutableArray*) array withFilter:(NSString*) filterString {
    
    
        NSSortDescriptor* sortDescriptorFullName =[[NSSortDescriptor alloc]initWithKey:@"fullName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
        [self.contacts sortUsingDescriptors:@[sortDescriptorFullName]];
        
        NSMutableArray* sectionsArray = [[NSMutableArray alloc]init];
        
        NSString* currentLetter = nil;
        
        for (SCContactItem* contact in array) {
            
            if ([filterString length] > 0 && [contact.contactFirstName rangeOfString:filterString options:NSCaseInsensitiveSearch].location && [contact.contactLastName rangeOfString:filterString options:NSCaseInsensitiveSearch].location && [contact.contactNikName rangeOfString:filterString options:NSCaseInsensitiveSearch].location == NSNotFound) {
                continue;
            }
            
            NSString* firstLetter = nil;
            
            if (contact.fullName.length>0) {
                
                firstLetter = [contact.fullName substringToIndex:1];
            }else {
                
                firstLetter = @"N";
                
                contact.contactFirstName = @"No name";
            }

            
            
            SCCoutrySection* section = nil;
            
            if (![currentLetter isEqualToString:firstLetter]) {
                
                section = [[SCCoutrySection alloc]init];
                section.sectionName = firstLetter;
                section.itemsArray = [NSMutableArray array];
                currentLetter = firstLetter;
                [sectionsArray addObject:section];
            }else{
                section = [sectionsArray lastObject];
            }
            [section.itemsArray addObject:contact];
        }
        
        return sectionsArray;

}


-(NSMutableArray*) generateSectionsFromArrayLocalAddressbook:(NSMutableArray*) array withFilter:(NSString*) filterString {
    
        
        NSSortDescriptor* sortDescriptorFullName =[[NSSortDescriptor alloc]initWithKey:@"fullName" ascending:YES];
    
        [self.localAddressbookContacts sortUsingDescriptors:@[sortDescriptorFullName]];
    
        NSMutableArray* sectionsArray = [[NSMutableArray alloc]init];
        
        NSString* currentLetter = nil;
        
        for (SCAddressbookContact* contact in array) {
            
            if ([filterString length] > 0 && [contact.contactFirstName rangeOfString:filterString options:NSCaseInsensitiveSearch].location && [contact.contactLastName rangeOfString:filterString options:NSCaseInsensitiveSearch].location == NSNotFound) {
                continue;
            }
            
            NSString* firstLetter = nil;
            
            if (contact.fullName.length>0) {
                
                firstLetter = [contact.fullName substringToIndex:1];
            }else {
                
                firstLetter = @"N";
                
                contact.contactFirstName = @"No name";
            }
            
            SCCoutrySection* section = nil;
            
            if (![currentLetter isEqualToString:firstLetter]) {
                
                section = [[SCCoutrySection alloc]init];
                section.sectionName = firstLetter;
                section.itemsArray = [NSMutableArray array];
                currentLetter = firstLetter;
                [sectionsArray addObject:section];
            }else{
                section = [sectionsArray lastObject];
            }
            [section.itemsArray addObject:contact];
        }
        
        return sectionsArray;

}


-(void) generateSectionsInBackgroundArrayContacts : (NSMutableArray*) array withFilter:(NSString*) filterString {
    
    [self.currenTOperation cancel];
    
    __weak SCContactsViewController* weakSelf = self;
    
    self. currenTOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSMutableArray* sectionsArray = [weakSelf generateSectionsFromArrayContacts:array withFilter:filterString];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.sectionsArray = sectionsArray;
            [self.tableView reloadData];
            
            self.currenTOperation = nil;
        });
    }];
    [self.currenTOperation start];
    
}


-(void) generateSectionsInBackgroundArrayLocalAddressbook: (NSMutableArray*) array withFilter:(NSString*) filterString {
    
    [self.currenTOperation cancel];
    
    __weak SCContactsViewController* weakSelf = self;
    
    self. currenTOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSMutableArray* sectionsArray = [weakSelf generateSectionsFromArrayLocalAddressbook:array withFilter:filterString];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.sectionsArray = sectionsArray;
            [self.tableView reloadData];
            
            self.currenTOperation = nil;
        });
    }];
    [self.currenTOperation start];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    textField.text = nil;
    
    if (self.segmentControll.selectedSegmentIndex == 0) {
        
        [self generateSectionsInBackgroundArrayContacts:self.contacts withFilter:nil];
        
        
    } else {
        
        [self generateSectionsInBackgroundArrayLocalAddressbook:self.localAddressbookContacts withFilter:nil];
        
    }

    [self.searchField resignFirstResponder];
    
    return YES;
}


-(void)textFieldDidChange:(UITextField*)textField{
    
    if (self.segmentControll.selectedSegmentIndex == 0) {
        
        [self generateSectionsInBackgroundArrayContacts:self.contacts withFilter:textField.text];
        
    }
    
    else {
        
        [self generateSectionsInBackgroundArrayLocalAddressbook:self.localAddressbookContacts withFilter:textField.text];
    }
    
}


- (void)segmentSwitch:(id)sender
{
    [self changTableViewHeader];
    
    NSInteger selectedSegment = self.segmentControll.selectedSegmentIndex;
    
    if (selectedSegment == 0)
    {
        [self generateSectionsInBackgroundArrayContacts:self.contacts withFilter:nil];
    }
    else
    {
        [self generateSectionsInBackgroundArrayLocalAddressbook:self.localAddressbookContacts withFilter:nil];
    }
}


-(NSMutableArray*) separateAdddressbookContacts:(NSMutableArray*) contacts
{
    NSMutableArray* separatedArray = [NSMutableArray new];
    for (SCAddressbookContact* contact in contacts )
        
    {
        if (!contact.checked)
        {
            [separatedArray addObject:contact];
        }
    }
    
    return separatedArray;
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - SCContactCellDelegate


-(void) creatChatWithContactAtIndexPath:(NSIndexPath*) indexPath
{
    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
    SCContactItem* contact = [sections.itemsArray objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_START_PRIVATE_CHAT object:contact];
}


@end
