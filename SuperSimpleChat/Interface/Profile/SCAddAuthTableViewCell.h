//
//  SCAddAuthTableViewCell.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCAddAuthTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *addAuthLabel;

@end
