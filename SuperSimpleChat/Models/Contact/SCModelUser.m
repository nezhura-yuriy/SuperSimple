//
//  SCModelUser.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 25.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCModelUser.h"
#import "SCSettings.h"
#import "SCPBManager.h"
#import "SCContactItem.h"
#import "SCContactItemSmall.h"


@implementation SCContactItemDelegateWithID



@end

@implementation SCModelUser
{
    NSTimer* _delaySaveTimer;
}

-(id) init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }
    return self;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if ( self )
    {
        _settings = settings;
        _protoBufManager = _settings.modelProfile.protoBufManager;
        [_protoBufManager addDelegate:self forType:SCPBManagerParceTypeGetBannedUsersRequest];
        [_protoBufManager addDelegate:self forType:SCPBManagerParceTypeAddUsersToBlackListRequest];


        [self _init];
    }
    return self;
}

-(void) _init
{
    _delegates = [[NSMutableArray alloc] init];
    _items = [[NSMutableDictionary alloc] init];
    _subscribedItems = [[NSMutableArray alloc] init];

//    [self loadAllCached];
}

-(void) dealloc
{
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeGetBannedUsersRequest];
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeAddUsersToBlackListRequest];


}

-(void) checkAndUpdateItem:(SCContactItem*) processedItem
{
    NSTimeInterval intervaNeedUpdate = 5 * 60;

    BOOL isRequired = NO;
    if(!processedItem.itemUpdatedTime)
        isRequired = YES;
    else
    {
        NSComparisonResult result = [processedItem.itemUpdatedTime compare:[processedItem.itemUpdatedTime dateByAddingTimeInterval:intervaNeedUpdate]];
        if(result == NSOrderedAscending)
            isRequired = NO;
        else
            isRequired = YES;
    }
    
    if(isRequired)
    {
        processedItem.itemUpdatedTime = [NSDate date];

        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       self, @"delegate",
                                       [[SCPUUID alloc] initWithRandom], @"UUID",
                                       [NSNumber numberWithInteger:SCPBManagerParceTypeGetUserByIdExt], @"type",
                                       processedItem,@"SCContactItem",
                                       nil];
        
        [_protoBufManager getUserById:params];
    }
}

-(void) updateItem:(SCContactItem*) processedItem
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [NSNumber numberWithInteger:SCPBManagerParceTypeGetUserByIdExt], @"type",
                                   processedItem,@"SCContactItem",
                                   nil];
    
    [_protoBufManager getUserById:params];
}

-(SCContactItem*) makeContactItemPUUID:(SCPUUID *)userPUUID
{
    SCContactItem* contactItem;
    NSString* uuidString = [userPUUID scpuuidToString];
    if([[_items allKeys] containsObject:uuidString])
    {
        contactItem = (SCContactItem*)[_items objectForKey:uuidString];
        
    }
    else
    {
        contactItem = [[SCContactItem alloc] initWithSettings:_settings];
        contactItem.userPUUID = [[SCPUUID alloc] initFromString:uuidString];
        
        [_items setObject:contactItem forKey:uuidString];
    }
    return contactItem;
}

-(SCContactItem*) getUserByPUUID:(SCPUUID *)userPUUID
{
    NSString* uuidString = [userPUUID scpuuidToString];
    return [self getUserByPUUIDString:uuidString];
}

-(SCContactItem*) getUserByPUUIDString:(NSString *)uuidString
{
    SCContactItem* contactItem;
    if([[_items allKeys] containsObject:uuidString])
    {
        contactItem = (SCContactItem*)[_items objectForKey:uuidString];
        
    }
    else
    {
        contactItem = [[SCContactItem alloc] initWithSettings:_settings];
        contactItem.userPUUID = [[SCPUUID alloc] initFromString:uuidString];
        
        [_items setObject:contactItem forKey:uuidString];
    }
    
    [self checkAndUpdateItem:contactItem];
    
    return contactItem;
}

-(SCContactItem*) getUser:(SCContactItem *)inUserItem
{
    if (inUserItem.userPUUID.nsuuid)
    {
        if([[_items allKeys] containsObject:[inUserItem.userPUUID scpuuidToString]])
        {
            SCContactItem* contactItem = (SCContactItem*)[_items objectForKey:[inUserItem.userPUUID scpuuidToString]];
            if(inUserItem.contactNikName.length > 0)
                contactItem.contactNikName = inUserItem.contactNikName;
            if(inUserItem.netFile.netPath.length > 0)
                
                contactItem.netFile = inUserItem.netFile;
            
            [self checkAndUpdateItem:contactItem];

            return contactItem;
        }
        else
        {
            [_items setObject:inUserItem forKey:[inUserItem.userPUUID scpuuidToString]];
            
            [self checkAndUpdateItem:inUserItem];

            return inUserItem;

        }
    }
    else
        FTLog(@"Хммм, и чего бы это");
    return inUserItem;
}

-(SCContactItemSmall*) getUserSmall:(NSString*) uuid
{
    SCContactItem* contactItem = nil;
    if([[_items allKeys] containsObject:uuid])
    {
        contactItem = (SCContactItem*)[_items objectForKey:uuid];

        [self checkAndUpdateItem:contactItem];
        
        return [contactItem getSmall];
    }
    else
    {
        SCContactItem* userItem = [[SCContactItem alloc] initWithSettings:_settings];
        userItem.contactPUUID = [[SCPUUID alloc] initFromString:uuid];
        [_items setObject:userItem forKey:uuid];
        
        [self checkAndUpdateItem:contactItem];

        return [userItem getSmall];
    }
}

-(void) addDelegate:(id <SCContactItemDelegate>) delegate contactId:(SCPUUID*)ID
{
    FTLog(@"%@ addDelegate %@",NSStringFromClass([self class]),delegate);
    for(int i = _delegates.count-1.0;i >= 0; i--)
    {
        SCContactItemDelegateWithID* typedDelegate = [_delegates objectAtIndex:i];
        if([typedDelegate.puuid isEqual: ID])
        {
            if([typedDelegate.delegate isEqual:delegate])
            {
                return;
            }
        }
    }
    SCContactItemDelegateWithID* typedDelegate = [[SCContactItemDelegateWithID alloc] init];
    typedDelegate.puuid = ID;
    typedDelegate.delegate = delegate;
    @synchronized(_delegates)
    {
        [_delegates addObject:typedDelegate];
    }
}

-(void) delDelegate:(id <SCContactItemDelegate>) delegate contactId:(SCPUUID*)ID
{
    FTLog(@"%@ delDelegate %@",NSStringFromClass([self class]),delegate);
    for(int i = _delegates.count-1.0;i >= 0; i--)
    {
        SCContactItemDelegateWithID* typedDelegate = [_delegates objectAtIndex:i];
        if([typedDelegate.puuid isEqual: ID])
        {
            if([typedDelegate.delegate isEqual:delegate])
            {
                @synchronized(_delegates)
                {
                    [_delegates removeObject:typedDelegate];
                    typedDelegate = nil;
                }
            }
        }
    }
}

-(void) callDelegate:(SCPUUID*)ID withData:(id) idData
{
    FTLog(@"%@ callDelegate",NSStringFromClass([self class]));
    if ([idData isKindOfClass:[SCContactItem class]])
    {
        for(int i = _delegates.count-1.0;i >= 0; i--)
        {
            SCContactItemDelegateWithID* typedDelegate = [_delegates objectAtIndex:i];
            if([typedDelegate.puuid isEqual:ID])
            {
                if(typedDelegate.delegate && [typedDelegate.delegate respondsToSelector:@selector(updateContactItem:)])
                    [typedDelegate.delegate updateContactItem: idData];
            }
        }
    } else if ([idData isKindOfClass:[NSDictionary class]])
    {
        for(int i = _delegates.count-1.0;i >= 0; i--)
        {
            SCContactItemDelegateWithID* typedDelegate = [_delegates objectAtIndex:i];
            if([typedDelegate.puuid isEqual:ID])
            {
                if(typedDelegate.delegate && [typedDelegate.delegate respondsToSelector:@selector(checkUserInBlackListResponse:)])
                    
                    [typedDelegate.delegate checkUserInBlackListResponse:idData];
            }
        }
    }
}

#pragma mark - subscription
-(void) getSubscriptionList
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   nil];
    
    [_protoBufManager getAllUsersWhichISubscribed:params];
    
}

-(void) addSubscribedItems:(id) inPuuid
{
    if([inPuuid isKindOfClass:[SCPUUID class]])
        [self addSubscribedUserByPuuid:inPuuid];
    else if ([inPuuid isKindOfClass:[NSArray class]])
    {
        for(SCContactItem* item in inPuuid)
        {
            [self addSubscribedUserByPuuid:item.userPUUID];
        }
    }
}

-(SCContactItem*) addSubscribedUserByPuuid:(SCPUUID*) puuid
{
    for(SCContactItem* contactItem in _subscribedItems)
    {
        if([contactItem.userPUUID isEqual:puuid])
            return contactItem;
    }
    SCContactItem* contactItem = [self getUserByPUUID:puuid];
    [_subscribedItems addObject:contactItem];
    return contactItem;
}

-(NSArray*) getSubscribedItems
{
    return _subscribedItems;
}

#pragma mark - banned
-(void) getBannedUsersWithParams:(id)params
{
    [_protoBufManager getBannedUsersRequest:params];
}


-(void)addUsersToBlackListWithParams:(id)params
{
    [_protoBufManager addUsersToBlackListRequest:params];
}


-(void)removeUsersFromBlackListWithParams:(id)params
{
    [_protoBufManager removeUsersFromBlackListRequest:params];
}

-(void)checkUserInBlackList:(id)params
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSDictionary* param = @{@"UUID":transportPUUID,@"contact":params,@"delegate":self};
    [_protoBufManager checkIfUserInBlackListRequest:param];
}

#pragma mark - CACHE
-(void) clearBeforeLogout
{
    [self saveCache];
    [_items removeAllObjects];
}

-(void) loadAllCached
{
    // Load cached users
    NSString* path = [_settings.cacheChatPath stringByAppendingPathComponent:@"UserList"];
    
    NSArray *tmpArray = [NSArray arrayWithContentsOfFile:path];
    for(NSDictionary *dict in tmpArray)
    {
        SCContactItem* userItem = [[SCContactItem alloc] initWithSettings:_settings];
        [userItem fromDictionary:dict];
        if(userItem.userPUUID)
            [_items setObject:userItem forKey:[userItem.userPUUID scpuuidToString]];
    }
    
    /*
// TODO://load cached subscription list
    // load subscription list
    path = [_settings.cacheChatPath stringByAppendingPathComponent:@"SubscriptionList"];
    tmpArray = [NSArray arrayWithContentsOfFile:path];
    for(NSDictionary *dict in tmpArray)
    {
        SCContactItem* userItem = [[SCContactItem alloc] initWith Settings:_settings];
        [userItem fromDictionary:dict];
        [_items setObject:userItem forKey:[userItem.userPUUID scpuuidToString]];
    }
    */
}
-(void) saveCache
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    for(NSString* saveUUID in [_items allKeys])
    {
        SCContactItem* userItem = [_items objectForKey:saveUUID];
        NSDictionary* dictItem = [userItem toDictionary];
        [tmpArray addObject:dictItem];
    }
    NSString* path = [_settings.cacheChatPath stringByAppendingPathComponent:@"UserList"];
    BOOL sucsess = [tmpArray writeToFile:path atomically:YES];
    if(!sucsess)
    {
        FTLog(@"Error saveCache");
    }
// TODO://save cached subscription list
}

-(void) clearCached
{
    SCContactItem* im;
    // нашли себя
    NSString* uuidString = [_settings.modelProfile.contactItem.userPUUID scpuuidToString];
    if([[_items allKeys] containsObject:uuidString])
    {
        // спрятались
        im = (SCContactItem*)[_items objectForKey:uuidString];
        
    }
    // всех убили
    [_items removeAllObjects];
    // себя добавили
    [_items setObject:im forKey:uuidString];
    
    NSString* path = [_settings.cacheChatPath stringByAppendingPathComponent:@"UserList"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSError* err = nil;
        BOOL res;
        
        res = [[NSFileManager defaultManager] removeItemAtPath:path error:&err];
        if (!res && err)
        {
            FTLog(@"oops: %@", err);
        }
    }
}

-(void)timerSaveCache:(NSTimer *)timer
{
    if(_delaySaveTimer)
        [_delaySaveTimer invalidate];
    _delaySaveTimer = nil;
    
    [self saveCache];
}

-(void) timerSetInterval
{
    FTLog(@"timerSetInterval");
    if(_delaySaveTimer)
        [_delaySaveTimer invalidate];
    _delaySaveTimer = nil;

    _delaySaveTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerSaveCache:) userInfo:nil repeats:NO];
}

#pragma mark SCPBManagerDelegate <NSObject>
//-(void) receiveParsedData:(id) data
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"SCChatViewController : receiveParsedData");
    switch (type)
    {
        case SCPBManagerParceTypeGetUserByIdExt:
        {
            SCContactItem* userItem = (SCContactItem*) data;
            [self timerSetInterval];
            
            [self callDelegate:userItem.userPUUID withData:userItem];
            
        }
            break;
            
#pragma mark parsedType subscribtion
        case SCPBManagerParceTypeSubscribe:
        {
            FTLog(@"SCPBManagerParceTypeSubscribe");
            [self getSubscriptionList];
        }
            break;
            
        case SCPBManagerParceTypeUnSubscribe:
        {
            FTLog(@"SCPBManagerParceTypeUnSubscribe");
        }
            break;

        case SCPBManagerParceTypeUsersWhichISubscribed:
        {
            FTLog(@"SCPBManagerParceTypeUsersWhichISubscribed");
            
        }
            break;

#pragma mark banned
        case SCPBManagerParceTypeGetBannedUsersRequest:
            if ([self.delegate respondsToSelector:@selector(getBannedUsersResponse:)])
            {
                [self.delegate getBannedUsersResponse:data];
            }
            break;
            
        case SCPBManagerParceTypeAddUsersToBlackListRequest:
            if ([self.delegate respondsToSelector:@selector(addUsersToBlackResponse:)])
            {
                [self.delegate addUsersToBlackResponse:data];
            }
            break;
            
        case SCPBManagerParceTypeRemoveUsersFromBlackListRequest:
            if ([self.delegate respondsToSelector:@selector(removeUsersFromBlackResponse:)])
            {
                [self.delegate removeUsersFromBlackResponse:data];
            }
            break;
            
        case SCPBManagerParceTypeCheckIfUserInBlackListRequest:
            if ([data isKindOfClass:[NSDictionary class]])
            {
                SCContactItem* userItem = [data objectForKey:@"contact"];
                [self callDelegate:userItem.userPUUID withData:data];
            }
            break;
            
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;
    }
}
@end
