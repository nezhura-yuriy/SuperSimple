//
//  UINavigationController+SCRotation.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 23.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (SCRotation)

@end
