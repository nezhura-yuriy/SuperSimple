//
//  SCHTTPManager.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 12.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "SCModelsDelegates.h"

@class SCSettings;

@interface SCNetError : NSObject

@property(nonatomic,strong) NSError* error;
@property(nonatomic,strong) NSHTTPURLResponse *httpResponse;
@property(nonatomic,strong) NSString* errorString;
@end



@interface SCHTTPManager : NSObject

- (instancetype)initWithSettings:(SCSettings*) settings;

-(void) setCachePath:(NSString*) cachePath;
-(void) setTempPath:(NSString*) tempPath;


-(void)registerUserWithPhoneNumber: (NSString*) phoneNumber
                         onSuccess: (void(^)(NSDictionary* registerValue))succes
                         onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void)registerUserWithEmail: (NSString*) email
                   onSuccess: (void(^)(NSDictionary* registerValue))succes
                   onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void)verificationUserWithSessionId: (NSString*) sessionId
                             smsCode: (NSString*) smsCode
                           onSuccess: (void(^)(NSDictionary* verificationValue))succes
                           onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void)registerUserWithFacebook:(NSString*) tokenString
                   onSuccess: (void(^)(NSDictionary* registerValue))succes
                   onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(NSString*) fileCachedPath:(NSString*) uuid;

-(void) fileUpload: (NSData*) fileBody
          fileType:(NSString*) fileType
    progressUpload: (void(^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)) progressUpload
         onSuccess: (void(^)(NSDictionary* verificationValue))succes
         onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

/*
-(void) fileDownload:(NSString*) uuid
    progressDownload: (void(^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)) progressDownload
           onSuccess: (void(^)(id imgData))succes
           onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;
*/

-(void) downloadToFile:(NSString*) uuid
              delegate:(id<SCHTTPManagerDelegate>) delegate;


-(void) uploadFromFile:(NSString*) filePath
              fileType:(NSString*) fileType
              delegate:(id<SCHTTPManagerDelegate>) delegate;

/* Для истории. Метод не используется сервером
-(void) fileDelete:(NSString*) fileUUID
         onSuccess: (void(^)(NSDictionary* verificationValue))succes
         onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;
*/

@end
