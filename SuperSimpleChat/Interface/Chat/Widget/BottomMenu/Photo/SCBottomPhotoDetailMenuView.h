//
//  SCBottomPhotoDetailMenuView.h
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"
#import "SCMediaItemView.h"

@class SCMessageBodys;

@interface SCBottomPhotoDetailMenuView : SCChatBottomsView <SCMediaItemViewDelegate>
{

}

-(void) setActionSelector:(void(^)(id selectItem)) completionBlock;
@end
