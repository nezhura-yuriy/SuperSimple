//
//  SCNotifycationManager.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 23.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCNotifycationManager.h"
#import "SCSettings.h"


@implementation SCNotifycationManager
{
    NSString* _deviceToken;
    NSString* _waitDeviceToken;
}
- (instancetype)initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    _deviceToken = [userDefaults objectForKey:@"deviceToken"];
}

- (void)dealloc
{
    
}

-(void) setDeviceNotifycationToken:(NSString*) deviceToken
{
    if(![_deviceToken isEqualToString:deviceToken])
    {
        _waitDeviceToken = _deviceToken;
        if(_waitDeviceToken)
            [_settings.modelProfile unSubscribePushNotifycation:_waitDeviceToken];
    }

    _deviceToken = deviceToken;
    [_settings.modelProfile subscribePushNotifycation:_deviceToken];
}

-(void) NotifycationTokenRegisterFail:(NSError *)error
{
    _waitDeviceToken = _deviceToken;
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"deviceToken"];
    [userDefaults synchronize];
    
    [_settings.modelProfile unSubscribePushNotifycation:_waitDeviceToken];
}

-(void) clearDeviceNotifycationToken
{
    if(_deviceToken)
    {
        _waitDeviceToken = _deviceToken;

        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:@"deviceToken"];
        [userDefaults synchronize];
        
        [_settings.modelProfile unSubscribePushNotifycation:_waitDeviceToken];
    }
}

-(void) receiveRemoteNotification:(NSDictionary*) notify
{
    if(!_settings.isInBackgraund)
        FTLog(@"%@",notify);
    
//    [[[UIAlertView alloc] initWithTitle:@"Push notifycation" message:[NSString stringWithFormat:@"%@",notify] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
}

#pragma mark SCPBManagerDelegate <NSObject>
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"%@ : receiveParsedData",NSStringFromClass([self class]));
    switch (type)
    {
        case SCPBManagerParceTypeSubscribeToAmazonSNS:
        {
            if([data isKindOfClass:[NSDictionary class]])
                if([[data allKeys] containsObject:@"result"])
                {
                    BOOL result = [[data objectForKey:@"result"] boolValue];
                    if(result)
                    {
                        FTLog(@"SCPBManagerParceTypeSubscribeToAmazonSNS - Ok");
                        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
                        [userDefaults setObject:_deviceToken forKey:@"deviceToken"];
                        [userDefaults synchronize];
                    }
                    else
                    {
                        FTLog(@"SCPBManagerParceTypeSubscribeToAmazonSNS - Error");
                        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
                        [userDefaults removeObjectForKey:@"deviceToken"];
                        [userDefaults synchronize];
                    }
                }
        }
            break;
            
        case SCPBManagerParceTypeUnsubscribeFromAmazonSNS:
        {
            if([data isKindOfClass:[NSDictionary class]])
                if([[data allKeys] containsObject:@"result"])
                {
                    BOOL result = [[data objectForKey:@"result"] boolValue];
                    if(result)
                    {
                        FTLog(@"SCPBManagerParceTypeUnsubscribeFromAmazonSNS - Ok");
                    }
                    else
                        FTLog(@"SCPBManagerParceTypeUnsubscribeFromAmazonSNS - Error");
                }
        }
            break;
            
        default:
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            break;
    }
}
@end
