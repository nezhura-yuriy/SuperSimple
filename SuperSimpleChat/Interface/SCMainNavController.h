//
//  SCMainNavController.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 06.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"


@class SCSettings;

@interface SCMainNavController : UINavigationController <SCMainNavControllerDelegate>
@property(nonatomic,strong) SCSettings* settings;
@end
