//
//  SCSkinColorTVC.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 29.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCSkinColorTVC : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labName;
@property (strong, nonatomic) IBOutlet UILabel *labRGB;
@property (strong, nonatomic) IBOutlet UILabel *labDescription;
@property (strong, nonatomic) IBOutlet UIImageView *imgColor;
@end
