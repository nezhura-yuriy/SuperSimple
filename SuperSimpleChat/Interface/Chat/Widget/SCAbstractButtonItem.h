//
//  SCAbstractButtonItem.h
//  SmartChat
//
//  Created by Yury Radchenko on 2/25/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
/*
Абстрактный класс для кнопок клавиатуры в чате, на основе List.item
 */

#import <UIKit/UIKit.h>
#import "SCList.h"
#import "SCListItem.h"
#import "SCSettings.h"

@interface SCAbstractButtonItem : UIButton
{
    SCSettings *_settings;
}

@property (nonatomic, strong) SCSettings *settings;

- (id) initWithFrame: (CGRect) frame forList:(SCList *) list itemIndex:(NSInteger) itemIndex;

@end
