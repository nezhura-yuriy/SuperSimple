//
//  SCNotificationListKeys.h
//  SuperSimpleChat
//
//  Created by yury on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

static NSString* kNotificationListUpdate = @"listUpdate";
static NSString* kNotificationItemsUpdate = @"itemsUpdate";
static NSString* kNotificationItemAdded = @"itemAdded";
static NSString* kNotificationItemUpdated = @"itemUpdated";
static NSString* kNotificationItemRemoved = @"itemRemoved";
static NSString* kNotificationItemsOrder = @"itemsOrder";
