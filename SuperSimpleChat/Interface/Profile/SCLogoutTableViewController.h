//
//  SCLogoutTableViewController.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 16.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"

@interface SCLogoutTableViewController : UITableViewController

@property (nonatomic, strong) SCSettings *settings;

@end
