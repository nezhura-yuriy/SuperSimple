//
//  SCPBManagerAbstract.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 02.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManager.h"
#import "SCSettings.h"


@implementation SCPBManagerDelegateTypes
-(NSString*) description
{
    return [NSString stringWithFormat:@"type = %u class %@",_type,_delegate];
}
@end

@implementation SCPBManager

- (id)init
{
    self = [super init];
    if (self)
    {
        _delegates = [[NSMutableArray alloc] init];
        _delegateDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void) dealloc
{
    
}

-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    _socketClient = _settings.socketClient;
    [_socketClient addDelegate:self];

}

-(void) setSocketClient:(SCWebSocketClient *)socketClient
{
    _socketClient = socketClient;
    [_socketClient addDelegate:self];
}

-(void) addDelegate:(id<SCPBManagerDelegate>) delegate forType:(SCPBManagerParceType) type
{
//    FTLog(@"addDelegate %@",delegate);
    for(int i = _delegates.count-1.0;i >= 0; i--)
    {
        SCPBManagerDelegateTypes* typedDelegate = [_delegates objectAtIndex:i];
        if(typedDelegate.type == type)
        {
            if([typedDelegate.delegate isEqual:delegate])
            {
                FTLog(@"УПС");
                return;
            }
        }
    }
    SCPBManagerDelegateTypes* typedDelegate = [[SCPBManagerDelegateTypes alloc] init];
    typedDelegate.type = type;
    typedDelegate.delegate = delegate;
    @synchronized(_delegates) {
        [_delegates addObject:typedDelegate];
    }
}

-(void) delDelegate:(id<SCPBManagerDelegate>) delegate forType:(SCPBManagerParceType) type
{
//    FTLog(@"delDelegate %@",delegate);
    for(int i = _delegates.count-1.0;i >= 0; i--)
    {
        SCPBManagerDelegateTypes* typedDelegate = [_delegates objectAtIndex:i];
        if(typedDelegate.type == type)
        {
            if([typedDelegate.delegate isEqual:delegate])
            {
                @synchronized(_delegates) {
                    [_delegates removeObject:typedDelegate];
                    typedDelegate = nil;
                }
            }
        }
    }

    /*
    if([_delegates containsObject:delegate])
    {
        [_delegates removeObject:delegate];
    }
    */
}


-(void) callDelegate:(SCPBManagerParceType) type withData:(id) idData
{
    FTLog(@"callDelegate");
//    for(SCPBManagerDelegateTypes* typedDelegate in _delegates)
//    {
    for(int i = _delegates.count-1.0;i >= 0; i--)
    {
        SCPBManagerDelegateTypes* typedDelegate = [_delegates objectAtIndex:i];
        if(typedDelegate.type == type)
        {
            if(typedDelegate.delegate && [typedDelegate.delegate respondsToSelector:@selector(parsedType:data:)])
                [typedDelegate.delegate parsedType:type data:idData];
        }
    }
    
}


// Requests
// Profile
-(void) profileGet:(id)params{}
-(void) setProfileField:(id) params{}
-(void) removeProfileField:(id) params{}
-(void) checkIsNicknameFree:(id) params{}
-(void) changeStatus:(id) params{}
-(void) logout:(id) params{}
-(void) authAdd:(id) params{}
-(void) authConfirm:(id) params{}
-(void) authRemove:(id) params{}
-(void) authGet:(id) params{}

// users
-(void) getUserByAuth:(id) params{}
-(void) getUserById: (id) params{}
-(void) inviteUsersRequest:(id)data{}
-(void) subscribeRequest:(id)params{}
-(void) getAllUsersWhichISubscribed:(id)params{}
-(void) unSubscribeRequest:(id)params{}
-(void) checkIfUserInBlackListRequest:(id)params{}
-(void) removeUsersFromBlackListRequest:(id)params{}
-(void) addUsersToBlackListRequest:(id)params{}
-(void) getBannedUsersRequest:(id)params{}
-(void) searchUsersByAuthRequest:(NSMutableDictionary*) params{}

// Lists
-(void) getListRequest: (id) dictionary{}
-(void) addItemListRequest: (id) dictionary{}
-(void) getListItemsRequest:(id) dictionary{}
-(void) updateListItemRequest:(id) dictionary{}
-(void) removeListItemRequest:(id) dictionary{}
-(void) setListItemsOrderRequest:(id) dictionary{}

// ChatRoom
-(void) createChatRoomRequest:(id) params{}
-(void) searchCreateChatRoomRequest:(id) params{}
-(void) removeChatRoomRequest:(id) params{}
-(void) chatRoomActivate:(id) params{}
-(void) changeChatRoomRequest:(id)params{}
-(void) getAllRoomsForUserRequest:(id)params{}
-(void) getAllUsersForRoomRequest:(id)params{}
-(void) leaveUserFromRoomRequest:(id)params{}
-(void) joinUserToRoomRequest:(id)params{}
-(void) getLastReadMessageDate:(id)params{}
-(void) setLastReadMessageDate:(id)params{}


// Messages
-(void) sendMessage:(id)params{}
-(void) getChatRoomHistory:(id) params{}
-(void) getChatRoomHistoryWithTimeRangeRequest:(id) params{}

// Contacts
-(void) createContactRequestWithParams:(NSMutableDictionary*) params{}
-(void) removeContactRequest:(SCContactItem*) contact{}
-(void) getAllContactFieldsRequest:(SCContactItem*) contact{}
-(void) setFieldsRequestWithParams:(NSMutableDictionary*) params{}
-(void) removeFieldRequest:(SCContactItem*) contact{}
-(void) getAllContactsRequestWithParams:(NSMutableDictionary*) params{}
-(void) creatContactWithFields:(NSDictionary*) fields{}


@end
