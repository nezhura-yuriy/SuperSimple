//
//  SCSBProfileViewController.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SCSettings.h"
#import "SCContactItem.h"
#import "SCModelProfile.h"
#import "SCImageViewWithIndicator.h"
#import "SCDatePickerViewController.h"
#import "SCSBTextFieldTableViewCell.h"
#import "SCSBGenderTableViewCell.h"
#import "SCLabel.h"
#import "SCTextField.h"

@protocol SCSBProfileViewControllerProtocol <NSObject>
- (void) tabBarControllerInteractionEnable:(BOOL) enableValue;
@end

@interface SCSBProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SCCellGenderProtocol, UIActionSheetDelegate, SCDatePickerDelegate, SCTextFieldCellProtocol, UITextFieldDelegate, SCModelProfileProtocol>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet SCImageViewWithIndicator *fotoImage;
@property (weak, nonatomic) IBOutlet UIImageView *fotoBluerImage;

@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet SCLabel *statusTitleLabel;


@property (weak, nonatomic) IBOutlet SCLabel *atLabel;
@property (weak, nonatomic) IBOutlet SCTextField *nickNameTextField;
@property (weak, nonatomic) IBOutlet UIView *nickNameBackgoundView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *backDatePickerView;

@property (nonatomic, strong) UIImagePickerController *photoPicker;
@property (nonatomic, strong) UIImagePickerController *mediaLibraryPicker;

@property (nonatomic) BOOL isNickNameRight;

@property (nonatomic, strong) SCSettings *settings;

@property (nonatomic, weak) id <SCSBProfileViewControllerProtocol> delegate;

- (IBAction)statusButtonPressed:(id)sender;

@end
