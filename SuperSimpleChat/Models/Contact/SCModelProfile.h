//
//  SCModelProfile.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 27.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModelsDelegates.h"
#import "SCTypes.h"

@class SCSettings;
@class SCContactItem;
@class SCContactItemSmall;
@class SCPBManager;
@class SCUserAuthItem;

typedef NS_ENUM(NSInteger, SCFieldChange) {
    SCFieldChangeNoSuccess = 0,
    SCFieldChangeIsSuccess,
    SCFieldChangeNickNameAlreadyExist
};

@protocol SCModelProfileProtocol <NSObject>
- (void) field:(SCFieldType)fieldType change:(SCFieldChange) fieldChange value:(id)fieldValue;
@optional
- (void) changeStatusCannot;
@end

@interface SCModelProfile : NSObject <SCPBManagerDelegate,SCModelAbstarctDelegate>

@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,weak) id<SCSettingsDelegate> delegate;
@property(nonatomic,strong) SCContactItem * contactItem;
//@property(nonatomic,strong) SCContactItemSmall * smallInfo;

@property(nonatomic,strong) NSString* sessionID;
@property(nonatomic,strong) NSString* userID;
@property(nonatomic,strong) NSString* confirmationHash;
@property(nonatomic,strong) NSString* http_link;
@property(nonatomic,strong) NSString* ws_link;
@property(nonatomic,strong) SCPBManager* protoBufManager;

@property(nonatomic,weak) id <SCModelProfileProtocol> delegateChangeField;

-(id) initWithSettings:(SCSettings*) settings;
-(void) fromDictionary:(NSDictionary*) dictionary;
-(NSDictionary*) toDictionary;
-(void) resroteContact;

-(void) loadServerProfile;
-(void) updateProfile:(SCContactItem*) newItem;

-(void)changeStatusWithEnum:(SCUserStatus) status;
-(void)startPingServer:(SCPingStatus) statusPing;
-(void)logout:(BOOL) isAllDevices;

-(void)subscribePushNotifycation:(NSString*) deviceToking;
-(void)unSubscribePushNotifycation:(NSString*) deviceToking;

#pragma mark - Fields
-(void) setField:(SCFieldType) fieldType forValue:(id) value;
-(void) removeProfileField:(SCFieldType) fieldType;

#pragma mark - AUTH
-(void)authGet;
-(void)authAddWithString:(SCUserAuthItem *)item;
-(void)authConfirmWithAuthItem:(SCUserAuthItem*)item andVerifyCode:(NSString *)verifyCode;
-(void)authRemove:(SCUserAuthItem *)item;
@end
