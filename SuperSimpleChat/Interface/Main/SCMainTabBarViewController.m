//
//  SCMainTabBarViewController.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 06.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatListViewController.h"
#import "SCContactItem.h"
#import "SCContactsViewController.h"
#import "SCListViewController.h"
#import "SCMainTabBarViewController.h"
#import "SCModelProfile.h"
#import "SCSettings.h"

@interface SCMainTabBarViewController ()

@end

@implementation SCMainTabBarViewController
{
    BOOL _shouldSelectViewControllers;
}
- (id) init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationStartNewPrivateChat:) name:NOTIFICATION_START_PRIVATE_CHAT object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationShowListPage:) name:NOTIFICATION_SHOW_LIST_PAGE object:nil];

    //    self.na
    //[self.navigationController.navigationBar setBarTintColor:[UIColor colorWithHue:0.739 saturation:0.418 brightness:0.574 alpha:1]];
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    
    self.delegate = self;
    _shouldSelectViewControllers = YES;

}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_CHANGE_SKIN_NAME object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_START_PRIVATE_CHAT object:nil];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationChangeSkin:) name:NOTIFICATION_CHANGE_SKIN_NAME object:nil];
    
    [self updateUI];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_CHANGE_SKIN_NAME object:nil];
}

-(void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];    
}

-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    
    // Chat
    SCChatListViewController* pSCChatListViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCChatListViewController" owner:self options:nil] lastObject];
    pSCChatListViewController.settings = _settings;
    
    // Contact
    SCContactsViewController* pSCContactsViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCContactsViewController" owner:self options:nil] objectAtIndex:0];
    pSCContactsViewController.settings = _settings;
    
    // List
    SCListViewController* pSCListViewController =[[[NSBundle mainBundle] loadNibNamed:@"SCListViewController" owner:self options:nil] lastObject];
    pSCListViewController.settings = _settings;
    
    // Profile
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SCProfile" bundle:nil];
    SCSBProfileViewController* pSCSBProfileViewController =[storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    pSCSBProfileViewController.settings = _settings;
    pSCSBProfileViewController.delegate = self;
    
    [self setViewControllers:[[NSArray alloc] initWithObjects:pSCChatListViewController, pSCContactsViewController,pSCListViewController, pSCSBProfileViewController, nil]];
    
    [self updateUI];
    
    if(_settings.modelProfile.contactItem.contactNikName.length <1)
    {
//TODO ЗДЕСЯ
        self.selectedIndex = 3;
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    return _shouldSelectViewControllers;
}

-(void) updateUI
{
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:CL_WHITE_COLOR, NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:18]};
    

    [[UITabBar appearance] setTintColor: SKCOLOR_TabBarFontActive];
    if([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedDescending)
        [[UITabBar appearance] setTranslucent:NO];
    [[UITabBar appearance] setBarTintColor:SKCOLOR_TabBarBg];
    
    UIFont *tabbarFont = [UIFont fontWithName:FT_FontNameDefult size:11.0f];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: SKCOLOR_TabBarFontInactive,
                                                        NSFontAttributeName:tabbarFont
                                                        }
                                             forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:SKCOLOR_TabBarFontActive,
                                                        NSFontAttributeName:tabbarFont
                                                        }
                                             forState:UIControlStateSelected];
    
    ((UIViewController*)[self.viewControllers objectAtIndex:0]).tabBarItem =[[UITabBarItem alloc] initWithTitle:@"Chats" image:[_settings.skinManager getImageForKeyX:@"tabbar-chats"] selectedImage:[_settings.skinManager getImageForKeyX:@"tabbar_chats"]];
    
    ((UIViewController*)[self.viewControllers objectAtIndex:1]).tabBarItem =[[UITabBarItem alloc] initWithTitle:@"Contacts" image:[_settings.skinManager getImageForKeyX:@"tabbar-contacts"] selectedImage:[_settings.skinManager getImageForKeyX:@"tabbar-contacts"]];

    ((UIViewController*)[self.viewControllers objectAtIndex:2]).tabBarItem =[[UITabBarItem alloc] initWithTitle:@"List" image:[_settings.skinManager getImageForKeyX:@"tabbar-lists"] selectedImage:[_settings.skinManager getImageForKeyX:@"tabbar-lists"]];
    
    ((UIViewController*)[self.viewControllers objectAtIndex:3]).tabBarItem =[[UITabBarItem alloc] initWithTitle:@"Profile" image:[_settings.skinManager getImageForKeyX:@"tabbar-profile"] selectedImage:[_settings.skinManager getImageForKeyX:@"tabbar-profile"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

- (void)notificationChangeSkin:(NSNotification *)note
{
    [self updateUI];
}

- (void)notificationStartNewPrivateChat:(NSNotification *)note
{
    FTLog(@"%@",note);
    self.selectedIndex = 0;
}

- (void)notificationShowListPage:(NSNotification *)note
{
    FTLog(@"%@",note);
    [self.navigationController popToViewController:self animated:YES];
    self.selectedIndex = 2;
}

//MARK: SCSBProfileViewControllerProtocol
- (void) tabBarControllerInteractionEnable:(BOOL)enableValue
{
    _shouldSelectViewControllers = enableValue;
}

@end
