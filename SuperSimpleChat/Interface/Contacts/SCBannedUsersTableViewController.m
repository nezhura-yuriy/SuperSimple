//
//  SCBannedUsersTableViewController.m
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 02.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCBannedUsersTableViewController.h"
#import "SCContactCell.h"
#import "SCSettings.h"
#import "SCCoutrySection.h"
#import "SCmodelContacts.h"
#import <SVProgressHUD.h>
#import "SCModelUser.h"


@interface SCBannedUsersTableViewController () <SCContactItemDelegate>

@property (strong, nonatomic) NSMutableArray* contacts;
@property (strong, nonatomic) NSOperation* currenTOperation;


@end

@implementation SCBannedUsersTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self settingNavigationBar];
    
    [_tableView registerNib:[UINib nibWithNibName:@"SCContactCell" bundle:nil] forCellReuseIdentifier:@"contactItem"];
    
    self.contacts = [NSMutableArray new];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [SVProgressHUD show];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    
    [SVProgressHUD dismiss];
}

-(void)setSettings:(SCSettings *)settings{
    
    _settings = settings;
    
    self.settings.modelUsers.delegate = self;
    [self.settings.modelUsers getBannedUsersWithParams:nil];

}

#pragma mark - Initialization methods

- (void)settingNavigationBar {
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 80, 100)];
    navLabel.backgroundColor = CL_CLEAR_COLOR;
    navLabel.text = @"Blocked friends";
    navLabel.textColor = CL_WHITE_COLOR;
    navLabel.shadowOffset = CGSizeMake(0, -2);
    [navLabel setFont:[UIFont fontWithName:FT_FontNameDefult size:18]];
    navLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = navLabel;
    
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    
}

#pragma mark - Actions


- (void)backAction {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.contacts count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *contactCellIdentifier = @"contactItem";
    
    SCContactCell* cell = [tableView dequeueReusableCellWithIdentifier:contactCellIdentifier];
    
    if (!cell) {
        
        [tableView registerNib:[UINib nibWithNibName:@"SCContactCell" bundle:nil] forCellReuseIdentifier:@"contactItem"];
        
        cell = [tableView dequeueReusableCellWithIdentifier:contactCellIdentifier];
    }

    SCContactItem* contact = [self.contacts objectAtIndex:indexPath.row];
    
    cell.creatChatOutlet.hidden = YES;

    [cell fillWhithContact:contact];

    
    return cell;
    
}

#pragma mark - SCmodelContactsDelegate

-(void)getBannedUsersResponse:(NSMutableArray *)users {
    
    [SVProgressHUD dismiss];

    if (users.count>0) {
        
        self.contacts = users;
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        [self.tableView reloadData];
        
        
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"You have not blocked friends";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:FT_FontNameDefult size:20];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
}

@end
