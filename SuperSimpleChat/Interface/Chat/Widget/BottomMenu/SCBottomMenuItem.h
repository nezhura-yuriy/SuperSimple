//
//  SCBottomMenuItem.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 29.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SCBottomMenuItemDelegate <NSObject>
@optional
-(void) tapAction:(NSInteger) tag;
-(void) longPressAction:(NSInteger) tag withState:(UIGestureRecognizerState) state;

@end

@interface SCBottomMenuItem : UIView

@property (nonatomic,strong) id<SCBottomMenuItemDelegate> delegate;

-(void) setImage:(UIImage*) image;
-(void) setBadge:(NSUInteger) badgeValue;
@end
