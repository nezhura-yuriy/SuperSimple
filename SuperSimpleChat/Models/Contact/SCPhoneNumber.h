//
//  SCPhoneNumber.h
//  SmartChat
//
//  Created by Yuriy Troyan on 06.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCPhoneNumber : NSObject
@property (nonatomic, strong) NSString * countryCode;
@property (nonatomic, strong) NSString * phoneNumber;
@property (nonatomic, strong) NSString * opCode;



@end
