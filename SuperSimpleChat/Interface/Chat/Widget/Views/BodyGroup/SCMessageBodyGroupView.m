//
//  SCMessageBodyGroupView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 20.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyGroupView.h"
#import "SCMessageBodyItemView.h"
#import "SCSettings.h"



@implementation SCMessageBodyGroupView
{
    
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    _itemWidth = 0;
    _itemHeight = 0;
    _viewsArray = [[NSMutableArray alloc] init];
    _previewIdx = 0;
    _itemsScroll = [[UIScrollView alloc] init];
    _itemsScroll.pagingEnabled = YES;
    _itemsScroll.delegate = self;

//    self.layer.borderWidth = 1;
//    self.layer.borderColor = [[UIColor blueColor] CGColor];

    _btnPrevImage = @"chat-arrow-left";
    _btnNextImage = @"chat-arrow-right";

}

-(void) dealloc
{
    
}

-(void) setMessageBodys:(SCMessageBodys*) messageBodys
{
    
}

-(void) setBodyAlignment:(SCMessageAlignment)bodyAlignment
{
    _bodyAlignment = bodyAlignment;
    for (SCMessageBodyItemView* _theViewItem in _viewsArray)
    {
        _theViewItem.bodyAlignment = _bodyAlignment;
    }
    switch (_bodyAlignment)
    {
        case SCMessageAlignmentLeft:
            _btnPrevImage = @"chat-arrow-left";
            _btnNextImage = @"chat-arrow-right";
            break;
            
        case SCMessageAlignmentRight:
            _btnPrevImage = @"chat-arrow-left";
            _btnNextImage = @"chat-arrow-right";
            break;
            
        default:
            //            FTLog(@"КАПЕЦ, от сервера пришел звук");
            break;
    }
    [_btnPrev setImage:[_settings.skinManager getImageForKeyX:_btnPrevImage] forState:UIControlStateNormal];
    [_btnNext setImage:[_settings.skinManager getImageForKeyX:_btnNextImage] forState:UIControlStateNormal];
    //    TODO:setBodyAlignment
}

-(CGFloat) getItemWidth
{
    return _itemWidth;
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        CGFloat posX = 0;
        if(_viewsArray.count >1)
        {
            CGFloat dX = (frame.size.width - _itemWidth)*.5;
            posX = (dX-NEXT_WIDTH)*.5;
            _btnPrev.frame = CGRectMake(posX, 0, NEXT_WIDTH, ICON_SIZE);
            posX += dX;
            _itemsScroll.frame = CGRectMake(posX, 0, _itemWidth, _itemHeight);
            _itemsScroll.contentSize = CGSizeMake(_itemWidth*_viewsArray.count, _itemHeight);
            for(SCMessageBodyItemView* _theViewItem in _viewsArray)
            {
                NSUInteger offset = [_viewsArray indexOfObject:_theViewItem];
                _theViewItem.frame = CGRectMake(_theViewItem.viewSize.width*offset, 0, _theViewItem.viewSize.width,  _theViewItem.viewSize.height);
            }
            posX += _itemWidth + (dX-NEXT_WIDTH)*.5;
            _btnNext.frame = CGRectMake(posX, 0, NEXT_WIDTH, ICON_SIZE);
        }
        else
        {
            _itemsScroll.frame = CGRectMake(posX, 0, _itemWidth, _itemHeight);
            _itemsScroll.contentSize = CGSizeMake(_itemWidth, _itemHeight);
        }
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
}


-(void) btnPrevAction:(id) sender
{
    if(_previewIdx > 0)
    {
        _previewIdx--;
        CGRect frame = CGRectMake(_previewIdx*_itemWidth, 0, _itemWidth, _itemHeight);
        [_itemsScroll scrollRectToVisible:frame animated:YES];
    }
    [self checkBtnHiddens];
}

-(void) btnNextAction:(id) sender
{
    if(_previewIdx < _viewsArray.count)
    {
        _previewIdx++;
        CGRect frame = CGRectMake(_previewIdx*_itemWidth, 0, _itemWidth, _itemHeight);
        [_itemsScroll scrollRectToVisible:frame animated:YES];
    }
    [self checkBtnHiddens];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    FTLog(@"");
    _previewIdx = scrollView.contentOffset.x / _itemWidth;
    [self checkBtnHiddens];
}

-(void) checkBtnHiddens
{
    if(_previewIdx == 0)
        _btnPrev.hidden = YES;
    else
        _btnPrev.hidden = NO;
    
    if(_previewIdx == _viewsArray.count-1)
        _btnNext.hidden = YES;
    else
        _btnNext.hidden = NO;
}



-(CGSize) viewSize
{
    return CGSizeMake(MARGIN, MARGIN);
}

@end
