//
//  SCBezierPathes.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 30.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

//#import <Foundation/Foundation.h>
//#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface SCBezierPathes : NSObject

+(CGFloat) radiusSize;
+(UIBezierPath*) messageBezier:(CGRect) rectView rectArea:(CGRect) rectArea zeroPoint:(CGPoint) point;

@end
