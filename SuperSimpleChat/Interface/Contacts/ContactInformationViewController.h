//
//  ContactInformationViewController.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 15.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCContactItem.h"
#import "SCImageViewWithIndicator.h"

@class SCSettings;

@interface ContactInformationViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) SCSettings* settings;
@property (strong, nonatomic) SCContactItem* contact;

@property (weak, nonatomic) IBOutlet SCImageViewWithIndicator *contactFotoCircular;
@property (weak, nonatomic) IBOutlet UIImageView *contactFotoBlur;
@property (weak, nonatomic) IBOutlet UILabel *nikeNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *statusImageOutlet;
@property (weak, nonatomic) IBOutlet UILabel *statusLabelOutlet;

@property (weak, nonatomic) IBOutlet UILabel *lastConnectionLabelOutlet;
@property (weak, nonatomic) IBOutlet UIView *nikeNameView;
@property (weak, nonatomic) IBOutlet UIButton *creatChatIcon;

- (IBAction)creatChatAction:(id)sender;
- (IBAction)subscriptAction:(id)sender;
@end
