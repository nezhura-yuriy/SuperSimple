//
//  SCButtonDelete.h
//  SmartChat
//
//  Created by Yury Radchenko on 2/25/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
/* 
 Класс кнопки Удаления
*/

#import <UIKit/UIKit.h>

@interface SCButtonDelete : UIButton

@end
