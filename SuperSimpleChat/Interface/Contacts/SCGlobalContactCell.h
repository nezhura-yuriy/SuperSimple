//
//  SCGlobalContactCell.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 05.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
@class SCContactItem;

#import <UIKit/UIKit.h>

@interface SCGlobalContactCell : UITableViewCell

@property (strong, nonatomic) NSString* searchAuth;

@property (strong, nonatomic) SCContactItem* item;

-(void) fillWhithContact: (SCContactItem*) contact;

@property (weak, nonatomic) IBOutlet UILabel *fullNameOutlet;

@property (weak, nonatomic) IBOutlet UIButton *addUserButtonOutlet;

- (IBAction)addUserAction:(id)sender;

@end
