//
//  SCSBBirthdayTableViewCell.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"
#import "SCCellLabel.h"
#import "SCLabel.h"

@interface SCSBBirthdayTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icoImageView;
@property (weak, nonatomic) IBOutlet SCCellLabel *titleLabel;
@property (weak, nonatomic) IBOutlet SCLabel *dateBirthdayLabel;

@property (strong, nonatomic) SCSettings *settings;
@property (strong, nonatomic) NSDate *birthdate;
@property (nonatomic) UIEdgeInsets edgeInsets;

@end
