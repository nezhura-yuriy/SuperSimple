//
//  SCMessageBodyPhotoGroupView.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 20.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyPhotoGroupView.h"
#import "SCMessageBodyPhotoItemView.h"
#import "SCSettings.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"
#import "SCNetFile.h"

@implementation SCMessageBodyPhotoGroupView
{
    
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    [super _init];
//    _viewsArray = [[NSMutableArray alloc] init];
    [self addSubview:_itemsScroll];
    
}

-(void) dealloc
{
    FTLog(@"%@ : dealloc",NSStringFromClass([self class]));
}

-(void) setMessageBodys:(SCMessageBodys*) messageBodys
{
    NSArray* photoArray = [messageBodys itemsByType:SCDataTypePhoto];
    CGFloat posX = 0;
    if(photoArray.count > 1)
    {// add prev
        _btnPrev = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnPrev addTarget:self action:@selector(btnPrevAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btnPrev setImage:[_settings.skinManager getImageForKeyX:_btnPrevImage] forState:UIControlStateNormal];
        _btnPrev.frame = CGRectMake(posX, 0, NEXT_WIDTH, ICON_SIZE);
        _btnPrev.hidden = YES;
        posX += NEXT_WIDTH;
        [self addSubview:_btnPrev];
    }
    
    for(SCMessageBodyItem* bodyItem in photoArray)
    {
        SCMessageBodyPhotoItemView* _photoViewItem = [[SCMessageBodyPhotoItemView alloc] initWithSettings:_settings];
        [_photoViewItem setPhotoNetFile:bodyItem.netFile];
        _itemWidth = MAX(_itemWidth,_photoViewItem.viewSize.width);
        _itemHeight = MAX(_itemHeight,_photoViewItem.viewSize.height);
        _photoViewItem.frame = CGRectMake(0, 0, _photoViewItem.viewSize.width,  _photoViewItem.viewSize.height);
        posX += _photoViewItem.viewSize.width + MARGIN;
        [_itemsScroll addSubview:_photoViewItem];
        [_viewsArray addObject:_photoViewItem];
    }
    
    if(photoArray.count > 1)
    {// add prev
        _btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnNext addTarget:self action:@selector(btnNextAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btnNext setImage:[_settings.skinManager getImageForKeyX:_btnNextImage] forState:UIControlStateNormal];
        posX += MARGIN;
        _btnNext.frame = CGRectMake(posX, 0, NEXT_WIDTH, ICON_SIZE);
        [self addSubview:_btnNext];
    }
}

-(CGFloat) getItemWidth
{
    return _itemWidth;
}

-(CGSize) viewSize
{
    if(_viewsArray.count >1)
        return CGSizeMake(_itemWidth + 2*(NEXT_WIDTH + NEXT_MARGIN),_itemHeight);
    else
        return CGSizeMake(_itemWidth,_itemHeight);
}

@end

