//
//  SCChatViewController.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCModelsDelegates.h"
#import "SCInterfaceDelegates.h"
#import "SCOverlayView.h"

@class SCSettings,SCChatItem,SCImageViewWithIndicator,SCChatTopMenuView,SCChatLogoView;

@interface SCChatViewController : UIViewController <SCChatItemDelegate,SCBottomViewsDelegate,SCPBManagerDelegate,SCModelChatsDelegate,UIScrollViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, SCOverlayViewProtocol>

@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) SCChatItem* chatItem;

@property(nonatomic,assign) BOOL isAutoDestroy;

@property (strong, nonatomic) IBOutlet UIView* topView;
@property (strong, nonatomic) IBOutlet UIView* pullRefreshView;
@property (strong, nonatomic) IBOutlet UIImageView *refreshArrow;
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UIView* navBarView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
//@property (strong, nonatomic) IBOutlet SCImageViewWithIndicator *imageChat;
@property (strong, nonatomic) IBOutlet SCChatLogoView *chatLogoView;

@property (strong, nonatomic) IBOutlet UILabel *labChatName;
@property (strong, nonatomic) IBOutlet UILabel *labLastConn;
@property (strong, nonatomic) IBOutlet UILabel *labDestroy;

@property (strong, nonatomic) IBOutlet SCChatTopMenuView *topMenuView;
@property (strong, nonatomic) IBOutlet UIScrollView *workspaceScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *messageListsScrollView;
@property (strong, nonatomic) IBOutlet UIView* botomView;
@property (strong, nonatomic) IBOutlet UITextField *fldMessage;
@property (strong, nonatomic) IBOutlet UIButton *btnSend;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorSend;

@property (strong, nonatomic) IBOutlet UIImageView *imgBottomSeparator;

- (IBAction)btnSendAction:(id)sender;
@end
