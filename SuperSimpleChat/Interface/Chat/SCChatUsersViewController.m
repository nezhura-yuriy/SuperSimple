//
//  SCChatUsersViewController.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 28.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatUsersViewController.h"
#import "SCSettings.h"
#import "SCChatUsersView.h"
#import "SCContactItem.h"
#import "SCAddUserToChatViewController.h"


@interface SCChatUsersViewController ()

@end

@implementation SCChatUsersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    [self updateUI];
    
    [self.userView.tableView reloadData];
    
}

-(void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _userView.frame = self.view.frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    _userView = [[SCChatUsersView alloc] init];
    _userView.settings = _settings;
//    _userView.delegate = self;
    [self.view addSubview:_userView];
//    _userView.userInRoom = _chatItem.usersInChat;
}

-(void) setUserInRoom:(NSMutableArray *)userInRoom
{
    _userInRoom = userInRoom;
    _userView.userInRoom = _userInRoom;
}

-(void) updateUI

{
    self.navigationItem.title = @"People in this group";
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[_settings.skinManager getImageForKey:@"btn_back"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButton.imageView.image.size.width, backButton.imageView.image.size.height);
    [backButton addTarget:self action:@selector(btnBackAction:forEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    UIBarButtonItem* addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addContactsAction)];
    
    addButton.tintColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = addButton;
}


- (void) addContactsAction {
  
    SCAddUserToChatViewController* _addUserToChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCAddUserToChatViewController" owner:self options:nil] lastObject];
    
    _addUserToChatViewController.userInRoom = _userInRoom;
    
    _addUserToChatViewController.settings = _settings;
    
    _addUserToChatViewController.chatItem = _userView.chatItem;
    
    _addUserToChatViewController.isNewChat = _isNewChat;

    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:_addUserToChatViewController];
    
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    

}


- (void) btnBackAction:(UIButton *)sender forEvent:(UIEvent *)event
{
    if(_delegate && [_delegate respondsToSelector:@selector(updateUserList:)])
    {
        [_delegate updateUserList:[_userView apply]];
    }
    
//    NSMutableArray* _userInRoomNew = [_userView apply];
//    for(SCContactItem* curContact in _userInRoomNew)
//    {
//        if(![_userInRoom containsObject:curContact])
//        {
//            FTLog(@"Save %@",curContact.contactPUUID);
//            [_userInRoom addObject:curContact];
//        }
//    }
//    for(NSInteger i = _userInRoom.count-1;i >= 0;i--)
//    {
//        SCContactItem* curContact = [_userInRoom objectAtIndex:i];
//        if(![_userInRoomNew containsObject:curContact])
//        {
//            FTLog(@"delete %@",curContact.contactPUUID);
//            [_userInRoom removeObject:curContact];
//        }
//    }
    

    [self.navigationController popViewControllerAnimated:YES];
}


@end
