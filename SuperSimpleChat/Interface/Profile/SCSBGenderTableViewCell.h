//
//  SCSBGenderTableViewCell.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"
#import "SCCellLabel.h"

@protocol SCCellGenderProtocol <NSObject>
- (void) setupGender:(NSString*) gender;//Male, Female
@end

@interface SCSBGenderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icoImageVIew;
@property (weak, nonatomic) IBOutlet SCCellLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *maleButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;

@property (nonatomic, strong) SCSettings *settings;
@property (nonatomic) UIEdgeInsets edgeInsets;

@property (nonatomic, weak) id <SCCellGenderProtocol> delegate;

- (IBAction)maleButtonPressed:(id)sender;
- (IBAction)femaleButtonPressed:(id)sender;

@end
