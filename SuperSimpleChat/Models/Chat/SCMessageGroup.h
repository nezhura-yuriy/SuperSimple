//
//  SCMessageGroup.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 04.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"

@class SCMessageItem;
@class SCChatItem;

@interface SCMessageGroup : SCAbstractItem

@property(nonatomic,strong) SCChatItem* chatItem;
@property(nonatomic,strong) NSDate* messagesDate;
@property(nonatomic,strong) NSMutableArray* chatMessages;


-(SCMessageItem*) searchMessageItemByPUUID:(SCPUUID*) scPuuid;
-(SCMessageItem*) searchMessageItemByPUUIDString:(NSString*) puuidString;
-(SCMessageItem*) newMessageItemByPUUID:(SCPUUID*) scPuuid;
-(SCMessageItem*) newMessageItemByPUUIDString:(NSString*) puuidString;
-(SCMessageItem*) getLastMessage;
-(NSUInteger) countNotViewedMessages;
-(BOOL) addMeesageItem:(SCMessageItem*) newItem;


#pragma mark - CACHE
-(void) loadMessages;
-(void) saveMessages;

@end
