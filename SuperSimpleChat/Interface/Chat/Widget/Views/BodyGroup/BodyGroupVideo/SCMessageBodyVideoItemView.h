//
//  SCMessageBodyVideoView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyItemView.h"
#import <AVFoundation/AVFoundation.h>


@class SCSettings,SCNetFile;

@interface SCMessageBodyVideoItemView : SCMessageBodyItemView


//@property(nonatomic,strong) NSString* videoFile;
@property(nonatomic,strong) SCNetFile* videoNetFile;

-(void) loadVideoFile:(NSString*) videoFile;
-(void) loadVideoFragment:(UIImage*) fragmentImage;

@end
