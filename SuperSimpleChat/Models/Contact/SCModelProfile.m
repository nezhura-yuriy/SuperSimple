    //
//  SCModelProfile.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 27.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCModelProfile.h"
#import "SCContactItem.h"
#import "SCContactItemSmall.h"
#import "SCSettings.h"
#import "SCNotifycationManager.h"
#import "SCChatItem.h"


#ifdef USE_LOCALS_NET
    #import "SCPBManagerLocal.h"
#else
    #import "SCPBManagerTernopol.h"
#endif

static NSString *kKeySuccess = @"success";
static NSString *kKeyData = @"data";
static NSString *kKeyField = @"field";

@implementation SCModelProfile

-(id) init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }
    return self;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if ( self )
    {
        _settings = settings;
        [self _init];
    }
    return self;
}

-(void) _init
{
    _contactItem = [[SCContactItem alloc] initWithSettings:_settings];
//    _smallInfo = [[SCContactItemSmall alloc] initWithSettings:_settings];
    _sessionID = @"";
    _userID = nil;
    
#ifdef USE_LOCALS_NET
    _protoBufManager = [[SCPBManagerLocal alloc] init];
#else
    _protoBufManager = [[SCPBManagerTernopol alloc] init];
#endif
    _ws_link = [NSString stringWithFormat:@"%@/%@/chat",WS_HOST,WS_PATH];
    _http_link = [NSString stringWithFormat:@"%@/%@",HTTP_HOST,HTTP_AUTH_PATH];
    FTLog(@"_ws_link = %@",_ws_link);
    FTLog(@"_http_link = %@",_http_link);
    
}

-(void) dealloc
{
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeGetProfile];
    [_protoBufManager delDelegate:self forType:SCPBManagerParceTypeGetUserByAuth];

}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    if([[dictionary allKeys] containsObject:@"sessionID"])
    {
        _sessionID = [dictionary objectForKey:@"sessionID"];
//        FTLog(@"token = %@",_sessionID);
    }
    
    if([[dictionary allKeys] containsObject:@"userID"])
    {
        _userID = [dictionary objectForKey:@"userID"];
        FTLog(@"profile _userID = %@",_userID);
    }
/*
    else if([[dictionary allKeys] containsObject:@"contactItem"])
    {
        _contactItem = [[SCContactItem alloc] initWithSettings:_settings];
        [_contactItem fromDictionary:[dictionary objectForKey:@"contactItem"]];
        _userID = [_contactItem.userPUUID scpuuidToString];
        [_contactItem addDelegate:self];
        FTLog(@"profile _contactItem.userPUUID = %@",[_contactItem.userPUUID scpuuidToString]);
    }
*/
    
//#ifdef USE_LOCALS_NET
//    if([[dictionary allKeys] containsObject:@"ws_link"])
//        _ws_link = [dictionary objectForKey:@"ws_link"];
//    if([[dictionary allKeys] containsObject:@"http_link"])
//        _http_link = [dictionary objectForKey:@"http_link"];
//#endif
//    FTLog(@"\r_ws_link = %@\r_http_link = %@\r",_ws_link,_http_link);
}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
    if(_sessionID)
        [dictionary setObject:_sessionID forKey:@"sessionID"];
    
    if(_userID)
        [dictionary setObject:_userID forKey:@"userID"];

    /*
    if(_contactItem)
        [dictionary setObject:[_contactItem toDictionary] forKey:@"contactItem"];
    */
    
    if(_ws_link)
        [dictionary setObject:_ws_link forKey:@"ws_link"];
    if(_http_link)
        [dictionary setObject:_http_link forKey:@"http_link"];
    
    return dictionary;
}

-(void) resroteContact
{
    if(_userID)
    {
        _contactItem = [_settings.modelUsers getUserByPUUID:[[SCPUUID alloc] initFromString:_userID]];
        [_contactItem addDelegate:self];
    }
}

-(void) loadServerProfile
{
    SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
    NSDictionary *dictionary =  @{@"delegate": self,
                                  @"UUID": transportPUUID,
                                  @"type":[NSNumber numberWithInteger:SCPBManagerParceTypeGetProfile] //когда-то использовалось, пусть пока живет
                                  };

    [_protoBufManager profileGet:dictionary];
}

-(void) updateProfile:(SCContactItem*) newItem
{
    if(newItem.contactNikName){
        _contactItem.contactNikName = newItem.contactNikName;
    }
    
    if(newItem.contactFirstName) {
        _contactItem.contactFirstName = newItem.contactFirstName;
    }

    if(newItem.contactLastName) {
        _contactItem.contactLastName = newItem.contactLastName;
    }
    
#ifdef OLD_FILE_LINK
    if(newItem.contactImageLink) {
        _contactItem.contactImageLink = newItem.contactImageLink;
    }
#endif
    if(newItem.netFile.netPath)
    {
        _contactItem.netFile.netPath = newItem.netFile.netPath;
    }
    
    if(newItem.contactSex) {
        _contactItem.contactSex = newItem.contactSex;
    }
    
    if(newItem.contactBirthdate) {
        _contactItem.contactBirthdate = newItem.contactBirthdate;
    }
    
    if(newItem.contactAbout) {
        _contactItem.contactAbout = newItem.contactAbout;
    }
    
    if(newItem.contactAddress) {
        _contactItem.contactAddress =newItem.contactAddress;
    }

//TODO:Анужно ли
//    _smallInfo.contactID = [_contactItem.contactPUUID scpuuidToString];
//    _smallInfo.contactNikName = _contactItem.contactNikName;
//    _smallInfo.imageLink = _contactItem.contactImageLink;
    FTLog(@"_contactItem = %@",_contactItem);
}

#pragma mark 

-(void)changeStatusWithEnum:(SCUserStatus) status
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   _contactItem, @"delegate",
                                   self, @"delegateProfile",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   _contactItem,@"SCContactItem",
                                   [[NSNumber alloc]initWithInteger:status],@"status",
                                   nil];
    
    [_settings.modelProfile.protoBufManager changeStatus:params];
}

-(void) startPingServer:(SCPingStatus) statusPing
{
    [_settings.modelProfile.protoBufManager pingServerStatus:statusPing];
}

-(void)logout:(BOOL) isAllDevices
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   [[NSNumber alloc]initWithBool:isAllDevices],@"isAllDevices",
                                   nil];
    [_settings.modelProfile.protoBufManager logout:params];
}

-(void)subscribePushNotifycation:(NSString*) deviceTokeng
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   deviceTokeng, @"deviceToken",
                                   nil];
    [_settings.modelProfile.protoBufManager subscribeToAmazonSNS:params];
}

-(void)unSubscribePushNotifycation:(NSString*) deviceTokeng
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   self, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   deviceTokeng, @"deviceToken",
                                   nil];
    [_settings.modelProfile.protoBufManager unSubscribeFromAmazonSNS:params];
}


#pragma mark - Fields
/*
 Use:
[self setField:SCFieldTypeNickName forValue:@"New Value"];
*/
-(void) setField:(SCFieldType) fieldType forValue:(id) value
{
    if (fieldType == SCFieldTypeNickName) {
        
        [self checkIsNicknameFree:value];
        
    } else {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        params[@"UUID"] = [[SCPUUID alloc] initWithRandom];
        params[@"delegate"] = self;
        params[@"SCContactItem"] = _contactItem;
        params[@"value"] = value;
        params[kKeyField] = [NSNumber numberWithInteger:fieldType];
        
        [_settings.modelProfile.protoBufManager setProfileField:params];
    }
}

- (void) checkIsNicknameFree:(id) nickName;
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"UUID"] = [[SCPUUID alloc] initWithRandom];
    params[@"delegate"] = self;
    params[@"contactNickName"] = nickName;
    
    [_settings.modelProfile.protoBufManager checkIsNicknameFree:params];
}

- (void) setNickNameforValue:(id) nicknameValue
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    params[@"UUID"] = [[SCPUUID alloc] initWithRandom];
    params[@"delegate"] = self;
    params[@"SCContactItem"] = _contactItem;
    params[@"value"] = nicknameValue;
    params[kKeyField] = [NSNumber numberWithInteger:SCFieldTypeNickName];
    
    [_settings.modelProfile.protoBufManager setProfileField:params];
}

/*
 Use:
 [self removeProfileField:SCFieldTypeNickName];
 */
-(void) removeProfileField:(SCFieldType) fieldType
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    params[@"UUID"] = [[SCPUUID alloc] initWithRandom];
    params[@"delegate"] = self;
    params[@"fields"] = [[NSArray alloc] initWithObjects:[NSNumber numberWithInteger:fieldType], nil];

    [_settings.modelProfile.protoBufManager removeProfileField:params];
}

#pragma mark - AUTH
-(void)authGet
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   _contactItem, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   nil];
    [_settings.modelProfile.protoBufManager authGet:params];
}

-(void)authAddWithString:(SCUserAuthItem *)item
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   _contactItem, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID", item, @"SCUserAuthItem",
                                   nil];
    [_settings.modelProfile.protoBufManager authAdd:params];
    
    
}

- (void) authConfirmWithAuthItem:(SCUserAuthItem*)item andVerifyCode:(NSString *)verifyCode
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   _contactItem, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   item, @"SCUserAuthItem",
                                   verifyCode, @"VerifyCode",
                                   nil];
    [_settings.modelProfile.protoBufManager authConfirm:params];
    
}

-(void)authRemove:(SCUserAuthItem *)item
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   _contactItem, @"delegate",
                                   [[SCPUUID alloc] initWithRandom], @"UUID",
                                   item, @"SCUserAuthItem",
                                   nil];
    [_settings.modelProfile.protoBufManager authRemove:params];
    
}

#pragma mark - SCPBManagerDelegate <NSObject>
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    BOOL isDelegateChangeFieldRespondsToSel = (_delegateChangeField && [_delegateChangeField respondsToSelector:@selector(field:change:value:)]);
    
    switch (type)
    {
        case SCPBManagerParceTypeGetProfile:
        {
            if([data isKindOfClass:[SCContactItem class]])
            {
                SCUserStatus status = _contactItem.contactStatus;
                _contactItem = data;
                _contactItem.contactStatus = status;
                FTLog(@"_contactItem = %@",_contactItem);
                // сразу грузим аусы
                [self authGet];
                [_settings saveStoredProfile];
                if(_delegate && [_delegate respondsToSelector:@selector(didLoadProfile)])
                    [_delegate didLoadProfile];
            }
            else if([data isKindOfClass:[NSDictionary class]])
            {
                FTLog(@"Error. Future.");
            }
        }
            break;
            
        case SCPBManagerParceTypeSetProfileField:
        {
            if([data isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *transportDic = (NSDictionary *) data;
                
                if ([[transportDic allKeys] containsObject:kKeySuccess])
                {
                    BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
                    
                    if (isSuccess)
                    {
                        if ([[transportDic allKeys] containsObject:kKeyData]) {
                         
                            SCContactItem* updatedItem = transportDic[kKeyData];
                            
                            if([_contactItem isEqual:updatedItem])
                            {
                                _contactItem = updatedItem;
                            }   
                            else
                            {
                                [_contactItem updateFrom:updatedItem];
                            }
                            
                            [_settings saveStoredProfile];
                            
                            if ([[transportDic allKeys] containsObject:kKeyField])
                            {
                                SCFieldType fieldType = (SCFieldType)[transportDic[kKeyField] integerValue];
                                
                                if (isDelegateChangeFieldRespondsToSel) {
                                    [_delegateChangeField field:fieldType change:SCFieldChangeIsSuccess value:nil];
                                }
                            }
                            else
                            {
                                FTLog(@"Error: transportDic havn't key '%@'", kKeyField);
                                
                            }
                        }
                        else
                        {
                            FTLog(@"Error: transportDic havn't key '%@'", kKeyData);
                        }
                    }
                    else
                    {
                        if ([[transportDic allKeys] containsObject:kKeyField])
                        {
                            SCFieldType fieldType = (SCFieldType)[transportDic[kKeyField] integerValue];
                            
                            if (isDelegateChangeFieldRespondsToSel) {
                                [_delegateChangeField field:fieldType change:SCFieldChangeNoSuccess value:nil];
                            }
                        }
                        else
                        {
                            FTLog(@"Error: transportDic havn't key '%@'", kKeyField);
                        }
                    }
                }
                else
                {
                    FTLog(@"Error: transportDic havn't key '%@'", kKeySuccess);
                }
            }
            else
            {
                FTLog(@"Error. SCPBManagerParceTypeSetProfileField bad isKindOfClass");
            }
        }
            break;
        
        case SCPBManagerParceTypeCheckIsNicknameFree:
        {
            if ([data isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *incomingDictionary = data;
                
                id nickName = incomingDictionary[@"contactNickName"];
                
                NSNumber *isNicknameFreeNumber = incomingDictionary[@"isNicknameFree"];
                BOOL isNicknameFree = [isNicknameFreeNumber boolValue];
                
                if (isNicknameFree) {
                    [self setNickNameforValue:nickName];
                    
                } else {
                    if (isDelegateChangeFieldRespondsToSel) {
                        [_delegateChangeField field:SCFieldTypeNickName change:SCFieldChangeNickNameAlreadyExist value:_contactItem.contactNikName];
                    }
                }
            }
            else
            {
                FTLog(@"Error. SCPBManagerParceTypeCheckIsNicknameFree bad isKindOfClass");
            }
        }
            break;
            
        case SCPBManagerParceTypeRemoveProfileField:
        {
            if ([data isKindOfClass:[NSDictionary class]]) {
                NSDictionary *transportDic = data;
                
                if ([[transportDic allKeys] containsObject:kKeySuccess])
                {
                    BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
                    
                    if (isSuccess)
                    {
                        if ([[transportDic allKeys] containsObject:@"fields"])
                        {
                            NSArray *fieldsArray = transportDic[@"fields"];
                            for (NSInteger i = 0; i < fieldsArray.count; i++)
                            {
                                SCFieldType fieldType = (SCFieldType)[[fieldsArray objectAtIndex:i] integerValue];
                                [self makeEmptyField:fieldType];
                            }
                            
                            [_settings saveStoredProfile];
                        }
                        else
                        {
                            FTLog(@"Error: transportDic havn't key '%@'", kKeyField);
                        }
                    }
                    else
                    {
                        if ([[transportDic allKeys] containsObject:@"fields"])
                        {
                            NSArray *fieldsArray = transportDic[@"fields"];
                            for (NSInteger i = 0; i < fieldsArray.count; i++) {
                                
                                SCFieldType fieldType = (SCFieldType)[[fieldsArray objectAtIndex:i] integerValue];
                                
                                if (isDelegateChangeFieldRespondsToSel) {
                                    [_delegateChangeField field:fieldType change:SCFieldChangeNoSuccess value:nil];
                                }
                            }
                        }
                        else
                        {
                            FTLog(@"Error: transportDic havn't key '%@'", kKeyField);
                        }
                    }
                }
            }
        }
            break;
            
        case SCPBManagerParceTypeChangeStatus:
        case SCPBManagerParceTypeChangeStatusEvent:
        {
            if ([data isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *transportDic = data;
                if ([[transportDic allKeys] containsObject:kKeySuccess])
                {
                    BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
                    
                    if (!isSuccess)
                    {
                        if (isDelegateChangeFieldRespondsToSel) {
                            [_delegateChangeField changeStatusCannot];
                        }
                    }
                }
            }
        }
            break;
            
        case SCPBManagerParceTypePrepareLogout:
        {
            [_settings prepareLogout];
        }
            break;
            
        case SCPBManagerParceTypeLogout:
        {
            [_settings logout];
            FTLog(@"Logout");
        }
            break;
            
        case SCPBManagerParceTypeAuthorizationFailedEvent:
        {
            FTLog(@"SCPBManagerParceTypeAuthorizationFailedEvent");
        }
            break;
        case SCPBManagerParceTypeSubscribeToAmazonSNS:
        {
//TODO: add token
            FTLog(@"add token");
            if([data isKindOfClass:[NSDictionary class]])
            {
                if (_settings.notifycationManager && [_settings.notifycationManager respondsToSelector:@selector(parsedType:data:)])
                {
                    [_settings.notifycationManager parsedType:type data:data];
                }

            }
        }
            break;
            
        case SCPBManagerParceTypeUnsubscribeFromAmazonSNS:
        {
//TODO: clear token
            FTLog(@"clear token");
            if([data isKindOfClass:[NSDictionary class]])
            {
                if (_settings.notifycationManager && [_settings.notifycationManager respondsToSelector:@selector(parsedType:data:)])
                {
                    [_settings.notifycationManager parsedType:type data:data];
                }
                
            }
        }
            break;

        default:
        {
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
            FTLog(@"%@",data);
        }
            break;
    }
}

- (void) makeEmptyField: (SCFieldType) field
{
    switch (field) {
        case SCFieldTypeAbout: _contactItem.contactAbout = @""; break;
        case SCFieldTypeFirstName: _contactItem.contactFirstName = @""; break;
        case SCFieldTypeLastName: _contactItem.contactLastName = @""; break;
        case SCFieldTypeSex: _contactItem.contactSex = @"";break;
        case SCFieldTypeBirthdate: _contactItem.contactBirthdate = nil;break;
        case SCFieldTypeCountry: _contactItem.contactAddress.country = @"";break;
        case SCFieldTypeCyty: _contactItem.contactAddress.city = @"";break;
        case SCFieldTypeStreet: _contactItem.contactAddress.street = @"";break;
        case SCFieldTypeHose: _contactItem.contactAddress.house = @"";break;
        case SCFieldTypeAppartament: _contactItem.contactAddress.appartament = @"";break;
        case SCFieldTypePostCode: _contactItem.contactAddress.postCode = @"";break;
        case SCFieldTypeImageid: [_contactItem.netFile deletePath];break;
            
        default:
            break;
    }
}

@end
