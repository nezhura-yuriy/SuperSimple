//
//  SCSBSettingsViewController.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"

@interface SCSBSettingsViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *notificationTitle;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;

@property (weak, nonatomic) IBOutlet UILabel *nightmodeTitle;
@property (weak, nonatomic) IBOutlet UISwitch *nightmodeSwitch;

@property (weak, nonatomic) IBOutlet UILabel *soundTitle;
@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;

@property (weak, nonatomic) IBOutlet UILabel *vibrateTitle;
@property (weak, nonatomic) IBOutlet UISwitch *vibrateSwitch;

@property (weak, nonatomic) IBOutlet UILabel *blockedFriendsTitle;

@property (weak, nonatomic) IBOutlet UILabel *cacheTitle;
@property (weak, nonatomic) IBOutlet UILabel *cacheSizeLabel;
@property (weak, nonatomic) IBOutlet UIButton *cachCleanButton;

@property (weak, nonatomic) IBOutlet UILabel *PrivacyPolicyTitle;
@property (weak, nonatomic) IBOutlet UILabel *termsOfUseTitle;
@property (weak, nonatomic) IBOutlet UILabel *feedBackTitle;
@property (weak, nonatomic) IBOutlet UILabel *aboutTitle;

@property (strong, nonatomic) SCSettings *settings;

- (IBAction)notificationSwitchChanged:(id)sender;
- (IBAction)nightmodeSwitchChanged:(id)sender;
- (IBAction)soundSwitchChanged:(id)sender;
- (IBAction)vibrateSwitchChanged:(id)sender;
- (IBAction)cacheCleanButtonPressed:(id)sender;

@end
