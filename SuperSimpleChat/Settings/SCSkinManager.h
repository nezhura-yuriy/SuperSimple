//
//  SCSkinManager.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCSkinManager : NSObject


-(id) initWithSkinName:(NSString*) skinName;

-(NSArray*) getInstalledSkins;
-(NSInteger) getCurrentSkinIndex;
-(NSDictionary*) getCurrentSkin;
-(NSDictionary*) getCurrentSkinData;
-(NSDictionary*) getCurrentSkinDataAt:(NSInteger) skinIndex;
-(NSString*) getCurrentSkinName;
-(NSArray*) getColorArray;

-(void) selectSkinName:(NSString*) skinName;

-(UIColor*) getColorForKey:(NSString*) key;
-(UIColor*) getColorForKeyX:(NSString*) key;
-(BOOL) getBoolForKey:(NSString*) key;
-(UIImage*) getImageForKey:(NSString*) key;
-(UIImage*) getImageForKeyX:(NSString*) key;
@end
