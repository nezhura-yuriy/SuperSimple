//
//  SCLTableViewCell.m
//  SuperSimpleChat
//
//  Created by yury on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCLTableViewCell.h"

static const CGFloat kSeparatorLineHeight = 0.5;
static const CGFloat kLabelSideMargin = 15.0f;

@implementation SCLTableViewCell
{
    SCLItemTextField *_label;
    SCListItem *_item;
    
    UILabel* _deleteLabel;
    
    bool _deleteOnDragRelease;
    
    CGPoint _originalCenter;
    
    NSString *_sourceLabelText;
    
    UIView *_separatorView;
    UIView *_leftMarkerView;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        _cellType = SCLTableViewCellTypeExistingCell;
        [self _init];
    }
    return self;
}

- (instancetype) initWithType: (SCLTableViewCellType) cellType;
{
    self = [super init];
    if (self) {
        _cellType = cellType;
        [self _init];
    }
    return self;
}

- (void) _init
{
    //Cell view
    
    // create a label that renders the item text
    _label = [[SCLItemTextField alloc] initWithFrame:CGRectNull];

    [_label setEnabled:NO];//чтобы при попытке долгого tap не было перехода к редактированию поля
    _label.delegate = self;
    
    [self addSubview:_label];
    
    //Left Marker
    _leftMarkerView = [[UIView alloc] initWithFrame:CGRectNull];
    [self addSubview:_leftMarkerView];
    
    //Separator
    _separatorView = [[UIView alloc] initWithFrame:CGRectNull];
    [self addSubview:_separatorView];
    
    //Add Delete label
    UIFont  *deleteLableFont = [UIFont fontWithName:FT_FontNameRegular size:19];
    NSString *deleteTitle = @"Delete";
    
    _deleteLabel = [[UILabel alloc] initWithFrame:CGRectNull];
    _deleteLabel.numberOfLines = 1;
    _deleteLabel.text = deleteTitle;
    _deleteLabel.textAlignment = NSTextAlignmentCenter;
    _deleteLabel.font = deleteLableFont;
    
    CGSize maximumDeleteLabelSize = CGSizeMake(self.bounds.size.width, self.bounds.size.height);
    CGRect deleteLabelRect = [deleteTitle boundingRectWithSize:maximumDeleteLabelSize
                                             options:(NSStringDrawingUsesLineFragmentOrigin)
                                          attributes:@{NSFontAttributeName:deleteLableFont}
                                             context:nil];
    
    _deleteLabel.bounds = CGRectMake(0.0, 0.0,
                                     deleteLabelRect.size.width + deleteLabelRect.size.width/2,
                                     self.bounds.size.height);
    
    [self addSubview:_deleteLabel];
    
    //Add a pan recognizer
    UIGestureRecognizer* recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    recognizer.delegate = self;
    [self addGestureRecognizer:recognizer];
    
    //LongPressGestureRecognizer
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    longPressRecognizer.delegate = self;
    longPressRecognizer.minimumPressDuration = 0.5;
    //longPressRecognizer.delaysTouchesBegan = 1.0;
    [self addGestureRecognizer:longPressRecognizer];
    
    UITapGestureRecognizer *touchRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    touchRecognizer.delegate = self;
    [self addGestureRecognizer:touchRecognizer];
}

// sets the alpha of the contextual cues
- (void) setCueAlpha:(float)alpha
{
    _deleteLabel.alpha = alpha;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    if (_cellType == SCLTableViewCellTypeAddingCell) {

        _leftMarkerView.frame = CGRectMake(0, 0, kLabelSideMargin, self.bounds.size.height);
        
    } else if (_cellType == SCLTableViewCellTypeExistingCell) {
        
        _separatorView.frame = CGRectMake(kLabelSideMargin,
                                          self.bounds.size.height - kSeparatorLineHeight,
                                          self.bounds.size.width - kLabelSideMargin * 2,
                                          kSeparatorLineHeight);
    }
    
    // position the label and contextual cues
    _label.frame = CGRectMake(kLabelSideMargin,
                              0,
                              self.bounds.size.width - kLabelSideMargin * 2,
                              self.bounds.size.height - kSeparatorLineHeight);
    
    
    //Метка про удаление размещается сразу за пределами cell (за пределами экрана)
    _deleteLabel.frame= CGRectMake(self.bounds.size.width,
                                   0,
                                   _deleteLabel.bounds.size.width,
                                   self.bounds.size.height);
}

- (void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    [self setupUI];
}

- (void) setupUI
{
    self.backgroundColor = SKCOLOR_TableCellBg;
    
    _label.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _label.font = [UIFont fontWithName:FT_FontNameLight size:14];
    _separatorView.backgroundColor = SKCOLOR_TableSeparator;
    
    if (_cellType == SCLTableViewCellTypeAddingCell) {
        
        _label.backgroundColor = SKCOLOR_TableCellBg;
        _label.textColor = SKCOLOR_TableTextFieldPlchldrFont;
        _leftMarkerView.backgroundColor = SKCOLOR_ButtonGreenBg;
        
    } else if (_cellType == SCLTableViewCellTypeExistingCell) {
        
        _label.textColor = SKCOLOR_TableCellFont;
        _label.backgroundColor = SKCOLOR_TableCellBg;
    }
    
    _deleteLabel.backgroundColor = SKCOLOR_TableSeparator;
    _deleteLabel.textColor = SKCOLOR_TableCellBg;
}

//MARK: horizontal pan gesture methods
- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return YES;
        
    } else if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        
        // Check for horizontal gesture && left
        CGPoint translation = [gestureRecognizer translationInView:[self superview]];
        
        if ((fabs(translation.x) > fabs(translation.y)) && translation.x < 0)
        {
            return YES;
        }
        
    } else if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
        
        return YES;
    }
    
    return NO;
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    
    // ===========================================================================
    if ([recognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        
        [_label setEnabled:YES];
        [_label becomeFirstResponder];
    
    // ===========================================================================
    } else if ([recognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        
        // if the gesture has just started, record the current centre location
        if (recognizer.state == UIGestureRecognizerStateBegan)
        {
            _originalCenter = self.center;
        }
        
        if (recognizer.state == UIGestureRecognizerStateChanged)
        {
            // translate the center
            CGPoint translation = [recognizer translationInView:self];
            self.center = CGPointMake(_originalCenter.x + translation.x,
                                      _originalCenter.y);
            
            // determine whether the item has been dragged far enough to initiate a delete
            // Признак удаления, если начало фрейма ячейки ушло за экран более чем на половину
            _deleteOnDragRelease = self.frame.origin.x < -self.frame.size.width / 2;
            
            // fade the contextual cues
            float cueAlpha = fabs(self.frame.origin.x) / (self.frame.size.width / 2);
            [self setCueAlpha:cueAlpha]; //прозрачность метки зависит от изменения цвета
            
            // indicate when the item have been pulled far enough to invoke the given action
            // цвет метки зависит от того, удаляется ли ячейка или нет
            _deleteLabel.backgroundColor = _deleteOnDragRelease ? SKCOLOR_ButtonRemoveBg: SKCOLOR_TableSeparator;
        }
        
        if (recognizer.state == UIGestureRecognizerStateEnded)
        {
            // the frame this cell would have had before being dragged
            CGRect originalFrame = CGRectMake(0,
                                              self.frame.origin.y,
                                              self.bounds.size.width,
                                              self.bounds.size.height);
            
            if (!_deleteOnDragRelease)
            {
                // if the item is not being deleted, snap back to the original location
                [UIView animateWithDuration:0.2
                                 animations:^{
                                     self.frame = originalFrame;
                                 }
                 ];
            } else {
                
                // notify the delegate that this item should be deleted
                [_delegate itemDeleted:_item onServer:YES];
            }
        }
        
    // ===========================================================================
    } else if ([recognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
        
        //FTLog(@"UILongPressGestureRecognizer");
        
        static UIView *snapshot = nil;
        CGPoint location = [recognizer locationInView:[self superview]];
        
        switch (recognizer.state) {
            
            case UIGestureRecognizerStateBegan:
            {
                snapshot = [self customSnapshotFromView:self];
                __block CGPoint center = self.center;
                _originalCenter = center;
                snapshot.alpha = 0.0;
                
                [[self superview] addSubview:snapshot];
                
                snapshot.alpha = 0.0;
                [UIView animateWithDuration:0.25
                                 animations:^{
                                     center.y = location.y;
                                     snapshot.center = center;
                                     snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                                     snapshot.alpha = 0.98;
                                     
                                     // Fade out
                                     self.alpha  = 0.0;
                                     
                                 } completion:^(BOOL finished) {
                                     // FTLog(@"completion UIGestureRecognizerStateBegan");
                                     self.hidden = YES;
                                 }
                 ];
                
                [[self superview] bringSubviewToFront:self];
            }
                break;
                
            case UIGestureRecognizerStateChanged:
            {
                
                CGPoint center = snapshot.center;
                center.y = location.y;
                snapshot.center = center;
                
                CGFloat delta = _originalCenter.y - snapshot.center.y;
                
                if (delta > 0 && delta > snapshot.bounds.size.height/1.1) {
                    //FTLog(@"Тянем вверх, сменить ячейки");
                    [_delegate moveItem:self.item directionMotion:SCDirectionOfMotionUp];
                    _originalCenter = snapshot.center;
                    
                } else if (delta < 0 && fabs(delta) > (snapshot.bounds.size.height/1.1)){
                    //FTLog(@"Тянем вниз, сменить ячейки");
                    [_delegate moveItem:self.item directionMotion:SCDirectionOfMotionDown];
                    _originalCenter = snapshot.center;
                }
                
            }
                break;
                
            default:
            {
                [_delegate itemsDidEndOrdering];
                
                self.hidden = NO;
                self.alpha = 0.0;
                
                [UIView animateWithDuration:0.25
                                 animations:^{
                                     snapshot.center = self.center;
                                     snapshot.transform = CGAffineTransformIdentity;
                                     snapshot.alpha = 1.0;
                                     self.alpha = 1.0;
                                 }

                                 completion:^(BOOL finished) {
                                     [snapshot removeFromSuperview];
                                     snapshot = nil;
                                 }];
            }
                break;
        }
    }
}

- (UIView *) customSnapshotFromView:(UIView *)inputView {
    
    // Create an image view
    UIView *snapshot = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                     self.frame.origin.y,
                                                                     self.bounds.size.width,
                                                                     self.bounds.size.height)];
    
    snapshot.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kLabelSideMargin,
                                                               0,
                                                               self.bounds.size.width - kLabelSideMargin,
                                                               self.bounds.size.height)];
    
    label.font = self.label.font;
    label.textColor = CL_WHITE_COLOR;
    label.backgroundColor = snapshot.backgroundColor;
    label.text = self.label.text;
    
    [snapshot addSubview:label];
    
    return snapshot;
}

//MARK: property getters / setters

- (SCLItemTextField*) label
{
    return _label;
}

- (SCListItem*) item
{
    return _item;
}

- (void) setItem:(SCListItem *)item
{
    _item = item;
    
    // we must update all the visual state associated with the model item
    _label.text = item.text;
}

//MARK: UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([_sourceLabelText isEqualToString:textField.text]) {
        //without change
        [_delegate cellDidEndEditing:self saveItem:NO];
        
    } else {
        _item.text = textField.text;
        [_delegate cellDidEndEditing:self saveItem:YES];
    }

    [textField setEnabled:NO];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_delegate cellDidBeginEditing:self];
    _sourceLabelText = textField.text;
}

@end
