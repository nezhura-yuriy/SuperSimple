//
//  SCBottomVideoDetailMenuView.m
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCBottomAudioDetailMenuView.h"
#import "SCBottomAudioAdditionMenuView.h"
#import "SCMediaCapture.h"
#import "SCMessageBodys.h"
#import "SCMessageBodyItem.h"

#define MARGIN 5
#define VIEW_HEIGHT 44


@implementation SCBottomAudioDetailMenuView
{
    UIScrollView* _imagesScroll;
    NSMutableArray* _viewArray;
    
    SCChatBottomsView* _additionView;
    CGFloat _additionViewHeight;
    
    void (^actionSelector) (id selectItem);
}


-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    _settings = settings;
    if(self)
    {
        _additionViewHeight = 0;
//        self.backgroundColor = [UIColor yellowColor];
        _imagesScroll = [[UIScrollView alloc] init];
        [self addSubview:_imagesScroll];

        _viewArray = [[NSMutableArray alloc] init];
//        NSMutableArray* imagesArray = [[NSMutableArray alloc] init];
        SCMediaItem* _addItem = [[SCMediaItem alloc] init];
        _addItem.type = SCMediaItemTypeButton;
        _addItem.mediaPath = AddFromMicrophoneAudio;
        
        SCMediaItemView* _addItemView = [[SCMediaItemView alloc] initWithSettings:_settings];
        _addItemView.delegate = self;
        [_addItemView setMediaItem:_addItem];
        [_imagesScroll addSubview:_addItemView];
        [_viewArray addObject:_addItemView];
        
//        _addItem = [[SCMediaItem alloc] init];
//        _addItem.type = SCMediaItemTypeButton;
//        _addItem.mediaPath = AddFromZZZZAudio;
//        
//        _addItemView = [[SCMediaItemView alloc] initWithSettings:_settings];
//        _addItemView.delegate = self;
//        [_addItemView setMediaItem:_addItem];
//        [_imagesScroll addSubview:_addItemView];
//        [_viewArray addObject:_addItemView];

//        [self setImagesArray:imagesArray];
        
    }
    return self;
}

-(void) dealloc
{
    
}


-(void) layoutSubviews
{
    [super layoutSubviews];
}

-(void) setActionSelector:(void(^)(id selectItem)) completionBlock
{
    actionSelector = [completionBlock copy];
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        [self reFrame:frame];
    }
}

-(void) reFrame:(CGRect) frame
{
    
    if(_additionView)
        _additionView.frame = CGRectMake(0, 0, frame.size.width, [_additionView sizeView].height);
    
    _imagesScroll.frame = CGRectMake(MARGIN, _additionViewHeight, frame.size.width - 2*MARGIN, frame.size.height);
    CGFloat posX = 0;
    for(SCMediaItemView* _itemView in _viewArray)
    {
        if([_itemView isKindOfClass:[SCMediaItemView class]])
        {
            _itemView.frame = CGRectMake(posX, 0, VIEW_HEIGHT, VIEW_HEIGHT);
            posX += VIEW_HEIGHT +2 ;
        }
    }
    _imagesScroll.contentSize = CGSizeMake(posX, VIEW_HEIGHT);
}

-(void) setMessageBodys:(SCMessageBodys *)messageBodys
{
    _messageBodys = messageBodys;
    
    [self updateItems];
}

-(void) setImagesArray:(NSMutableArray *)imagesArray
{
    return;    
}

-(CGSize) sizeView
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, VIEW_HEIGHT + _additionViewHeight);
}

-(void) updateItems
{
    [UIView animateWithDuration:0.25 animations:^{
        [self updateItemsWithoutAnimation];
    } completion:^(BOOL finished) {
        //
    }];
}

-(void) updateItemsWithoutAnimation
{
    NSUInteger insertIdx = _viewArray.count;
    do {
        SCMediaItemView* _itemView = [_viewArray objectAtIndex:insertIdx-1];
        if(_itemView.mediaItem.type != SCMediaItemTypeButton)
        {
            if(![_messageBodys.items containsObject:_itemView.mediaItem.messageBody])
            {
                [_itemView removeFromSuperview];
                [_viewArray removeObjectAtIndex:insertIdx-1];
            }
            else
                insertIdx--;
        }
        else
            insertIdx--;
    }while (insertIdx > 0);
    
    for(SCMessageBodyItem* item in [_messageBodys items])
    {
        switch (item.type)
        {
            case SCDataTypeAudio:
            case SCDataTypeAudioFile:
            case SCDataTypeAudioLink:
            {
                BOOL isExistItem = NO;
                for(SCMediaItemView* _itemView in _viewArray)
                {
                    if(_itemView.mediaItem.type == SCMediaItemTypeButton)
                        break;
                    else if([_itemView.mediaItem.messageBody isEqual:item])
                    {
                        isExistItem = YES;
                        break;
                    }
                }
                
                if(!isExistItem)
                {
                    SCMediaItem* _mediaItem = [[SCMediaItem alloc] init];
                    _mediaItem.type = SCMediaItemTypeAudio;
                    _mediaItem.messageBody = item;
                    
                    SCMediaItemView* _itemView = [[SCMediaItemView alloc] initWithSettings:_settings];
                    _itemView.delegate = self;
                    [_itemView setMediaItem:_mediaItem];
                    [_imagesScroll addSubview:_itemView];
                    [_viewArray insertObject:_itemView atIndex:insertIdx];
//                    [_viewArray addObject:_itemView];
                }
            }
                break;
            default:break;
        }
    }
    [self reFrame:self.frame];
    [self setNeedsLayout];
}

#pragma mark - @protocol SCMediaItemViewDelegate <NSObject>
-(void) itemTap:(id) item
{
    
    SCMediaItem* _item = (SCMediaItem*)item;
    switch (_item.type)
    {
        case SCMediaItemTypeButton:
        {
            if(actionSelector)
            {
//                _additionViewHeight = 44;
                _additionView = [[SCBottomAudioAdditionMenuView alloc] initWithSettings:_settings];
                _additionViewHeight = [_additionView sizeView].height;
                _additionView.messageBodys = _messageBodys;
                
                [((SCBottomAudioAdditionMenuView*)_additionView) setActionSelector:^(id selectItem){
                    _additionViewHeight = 0;
                    [_additionView removeFromSuperview];
                    actionSelector(AddFromMicrophoneAudio);
                }];
                [self addSubview:_additionView];
            
                actionSelector(_item.mediaPath);
//                if([_item.mediaPath isEqualToString:AddFromMicrophoneAudio])
//                {
//                    actionSelector(AddFromMicrophoneAudio);
//                }
            }
        }
            break;
        case SCMediaItemTypeAudio:
        {
            FTLog(@"SCMediaItemTypeAudio");
            _additionView = [[SCBottomAudioAdditionMenuView alloc] initWithSettings:_settings];
            _additionViewHeight = [_additionView sizeView].height;
            _additionView.messageBodys = _messageBodys;
            
            [((SCBottomAudioAdditionMenuView*)_additionView) setActionSelector:^(id selectItem){
                _additionViewHeight = 0;
                [_additionView removeFromSuperview];
                actionSelector(AddFromMicrophoneAudio);
            }];
            [self addSubview:_additionView];
            
            actionSelector(_item.mediaPath);
        }
            break;
            
        case SCMediaItemTypeImage:
        {
            FTLog(@"SCMediaItemTypeImage");
        }
            break;
            
        case SCMediaItemTypeVideo:
        {
            FTLog(@"SCMediaItemTypeVideo");
        }
            break;
            
        default:
            break;
    }
}
-(void) itemSwipe:(id) item
{
    [_messageBodys removeItem:((SCMediaItem*)item).messageBody];
}
@end
