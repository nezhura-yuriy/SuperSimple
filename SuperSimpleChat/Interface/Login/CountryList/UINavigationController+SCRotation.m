//
//  UINavigationController+SCRotation.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 23.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "UINavigationController+SCRotation.h"

@implementation UINavigationController (SCRotation)

- (BOOL) shouldAutorotate
{
    return [[self topViewController] shouldAutorotate];
}

- (NSUInteger) supportedInterfaceOrientations
{
    return [[self topViewController] supportedInterfaceOrientations];
}

@end
