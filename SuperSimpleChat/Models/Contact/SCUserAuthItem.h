//
//  SCUserAuth.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 06.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"

@interface SCUserAuthItem : SCAbstractItem

@property(nonatomic,assign) SCUserAuthItemType authType;
@property(nonatomic,strong) NSString* data;
@property(nonatomic) BOOL isConfirm;


-(id) initByType:(SCUserAuthItemType) type WithData:(NSObject*) data;
-(id) initWithType:(SCUserAuthItemType) type andData:(NSString*) data andConfirm:(BOOL)confirm;

-(BOOL) isAuthEqual:(SCUserAuthItem*) authItem;
@end
