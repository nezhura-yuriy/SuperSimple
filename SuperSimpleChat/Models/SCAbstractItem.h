//
//  SCAbstractItem.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 20.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SCModelsDelegates.h"
#import "SCPUUID.h"
#import "SCTypes.h"

@class SCSettings;


@interface SCAbstractItem : NSObject
{
    SCSettings* _settings;
    NSMutableArray* _delegates;
}

@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) NSMutableArray* delegates;

-(id) initWithSettings:(SCSettings*) settings;
-(id) initWithDictionary:(NSDictionary*) dictionary;
-(void) fromDictionary:(NSDictionary*) dictionary;
-(NSDictionary*) toDictionary;

-(void) addDelegate:(id<SCModelAbstarctDelegate>) delegate;
-(void) delDelegate:(id<SCModelAbstarctDelegate>) delegate;

@end
