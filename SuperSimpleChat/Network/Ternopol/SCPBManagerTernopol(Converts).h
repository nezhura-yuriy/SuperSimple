//
//  SCPBManagerTernopol+Converts.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol.h"
#import "SCContactItem.h"
#import "SmartChat.pb.h"

@interface SCPBManagerTernopol (Converts)

//MARK: PUUID
- (PUUID *) puuidBuilder: (SCPUUID *) puuid;
- (SCPUUID *) scpuuidBuilder: (PUUID *) puuid;
- (NSString *) scpuuidStringBuilder: (PUUID *) puuid;

//MARK: statuses
-(SCUserStatus) statusToSCContactStatus:(UserStatus*) status;
-(StatusType) scContactStatusToStatus:(SCUserStatus) scStatus;

//MARK: Fields
-(SCFieldType) fieldTypeToSCFieldType:(FieldType) fieldType;
-(FieldType) scFieldTypeToFieldType:(SCFieldType) scFieldType;
-(Field*) fieldFromContact:(SCFieldType) scFieldType fromValue:(id)value;
-(Field*) fieldFromContact:(SCFieldType) scFieldType fromItem:(SCContactItem*)contactItem;
-(NSArray*) fieldsFromContact:(SCContactItem*)contactItem;
-(void) contactFromFields:(SCContactItem*)contactItem fields:(NSArray*)fields;
//-(SCContactItem*)setContactFields:(SCContactItem*)contact fields:(NSArray*)fields;
-(void) contactField:(SCContactItem*) contactItem fieldType:(SCFieldType) scFieldType fromValue:(id)value;

//MARK: Auth
-(AuthType) theAuth:(SCUserAuthItemType) type;
-(SCUserAuthItemType) theUserAuth:(AuthType) type;

// MARK: CharRooms
-(ChatRoomType) SCChatRoomTypeToChatRoomType:(SCChatType) scChatType;
-(SCChatType) ChatRoomTypeToSCChatRoomType:(ChatRoomType) chatType;
@end
