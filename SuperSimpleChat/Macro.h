//
//  Macro.h
//  shoppop
//
//  Created by Yury Radchenko on 15.12.14.
//  Copyright (c) 2014 Yuriy Nezhura. All rights reserved.
//

#import "SCSkinColors.h"
#import "SCPrints.h"

// ===========================================================================
// Настройки компиляции приложения
// Begin =====================================================================
// Выбор сервера
#define TERNOPOL_PRODUCTION_SERVER 1

//NSLOG HIDE
#ifdef DEBUG
#define FT_DEBUG 1 //если закомментить, то логов не будет
#endif

// Устаревшее но пока пусть побудет :)
#define IS_WORK 1
#define DEBUG_PARSER 2

// End Настройки компиляции приложения =======================================

#define REFRESH_HEADER_HEIGHT 52.0f

//Screen and elements of interface sizes
#define SZ_SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define SZ_SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SZ_STATUS_BAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height
#define SZ_NAV_BAR_HEIGHT 44.f
#define SZ_TAB_BAR_HEIGHT 49.f
#define SZ_TOOL_BAR_HEIGHT 44.f

#define SZ_STATUS_AND_NAV_BARS_HEIGHT (44.f + [UIApplication sharedApplication].statusBarFrame.size.height)
#define SZ_STATUS_AND_NAV_AND_TAB_BARS_HEIGHT (44.f + 49.f + [UIApplication sharedApplication].statusBarFrame.size.height)
#define SZ_STATUS_AND_NAV_AND_TOOL_BARS_HEIGHT (44.f + 44.f + [UIApplication sharedApplication].statusBarFrame.size.height)

//Devices and iOS
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) // iPhone and iPod touch style UI

#define IS_IPHONE_4_AND_OLDER_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height < 568.0f)
#define IS_IPHONE_5_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_6_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)
#define IS_IPHONE_6P_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)

#define IS_IPHONE_4_AND_OLDER_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) < 568.0f)
#define IS_IPHONE_5_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 568.0f)
#define IS_IPHONE_6_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 667.0f)
#define IS_IPHONE_6P_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 736.0f)

#define IS_IPHONE_4_AND_OLDER ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_4_AND_OLDER_IOS8 : IS_IPHONE_4_AND_OLDER_IOS7 )
#define IS_IPHONE_5 ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_5_IOS8 : IS_IPHONE_5_IOS7 )
#define IS_IPHONE_6 ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_6_IOS8 : IS_IPHONE_6_IOS7 )
#define IS_IPHONE_6P ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_6P_IOS8 : IS_IPHONE_6P_IOS7 )

//Colors  
#define CL_RED_COLOR [UIColor redColor]
#define CL_WHITE_COLOR [UIColor whiteColor]
#define CL_BLACK_COLOR [UIColor blackColor]
#define CL_BLUE_COLOR [UIColor blueColor]
#define CL_CLEAR_COLOR [UIColor clearColor]
#define CL_DARKGRAY_COLOR [UIColor darkGrayColor]
#define CL_LIGTHGRAY_COLOR [UIColor lightGrayColor]

//Degub Color
#define CL_DEBUG_RED_COLOR [[UIColor redColor] CGColor]
#define CL_DEBUG_GREEN_COLOR [[UIColor greenColor] CGColor]
#define CL_DEBUG_BLUE_COLOR [[UIColor blueColor] CGColor]


// Notifications
#define NOTIFICATION_CHANGE_SKIN_NAME   @"Notification_Application_Change_Skin_Name"
#define NTF_PRIVATELIST_UPDATE          @"updatePrivateLists"
#define NTF_FAVITEMS_UPDATE             @"updateFavoriteItems"
#define NOTIFICATION_START_PRIVATE_CHAT @"Notification_Application_Start_Private_Chat"
#define NOTIFICATION_SHOW_LIST_PAGE     @"Notification_Application_Show_List_Page"

// Стандартные сообщения
#define MESSAGE_INVITE      @"Check out SuperSimpleSmartChat. You can download here: "

//Font
#define FT_FontNameDefult   @"Montserrat-Light"
#define FT_FontNameRegular  @"Montserrat-Regular"
#define FT_FontNameBold     @"Montserrat-Bold"
#define FT_FontNameLight    @"Montserrat-Light"
#define FT_FontNameHairline @"Montserrat-Hairline"
#define FT_FontNameBlack    @"Montserrat-Black"

#define FT_FontSizeDefult 17
#define FT_FontSizeTableHeader 12
#define FT_FontSizeNavTitle 19

//Localizer
#define LOCAL(WORD) [_localizer getLocalisedStringByKey:WORD]


// В настройках компиляции TERNOPOL_PRODUCTION_SERVER
#ifdef TERNOPOL_PRODUCTION_SERVER //Producton server: 54.148.194.39
    #define HTTP_HOST @"http://zpapp1.turbochat.io"   // Ternopol
    #define WS_HOST @"ws://zpapp1.turbochat.io"
    #define SERVER_TYPE @"TERNOPOL_PRODUCTION_SERVER"
#else //Staging server: 54.93.222.229
    #define HTTP_HOST @"http://54.93.222.229"   // start
    #define WS_HOST @"ws://54.93.222.229"
    #define SERVER_TYPE @"TERNOPOL_START_SERVER"
#endif
#define HTTP_AUTH_PATH @"api/v1"
#define WS_PATH @"api/v1"
#define TOKEN_PARAM_NAME @"accessToken"


// В настройках компиляции FT_DEBUG
#if FT_DEBUG
    #define FTLog(...) (NSLog(__VA_ARGS__))
#else
    #define FTLog(...)
#endif
