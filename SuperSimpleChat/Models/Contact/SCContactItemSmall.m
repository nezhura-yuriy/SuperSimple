//
//  SCUserItem.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCContactItemSmall.h"

@implementation SCContactItemSmall

-(void) _init
{
    _contactID = @"";
    _contactNikName = @"";
    _imageLink = @"";
}

-(void) dealloc
{
    _contactID = nil;
    _contactNikName = nil;
    _imageLink = nil;
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    if([[dictionary allKeys] containsObject:@"contactID"])
        _contactID = [dictionary objectForKey:@"contactID"];
    
    if([[dictionary allKeys] containsObject:@"contactNikName"])
        _contactNikName = [dictionary objectForKey:@"contactNikName"];
    
    if([[dictionary allKeys] containsObject:@"imageLink"])
        _imageLink = [dictionary objectForKey:@"imageLink"];
    
}
-(NSString*) description
{
    return [NSString stringWithFormat:@"{\ncontactID = %@\ncontactNikName = %@\nimageLink = %@\n}",_contactID,_contactNikName,_imageLink];
}
@end
