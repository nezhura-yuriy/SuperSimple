//
//  SCAccessEmailViewController.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 22.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"
#import "SCButton.h"
#import "SCLabel.h"
#import "SCTextField.h"

@class SCSettings;
@class SCSkinManager;

@interface SCAccessEmailViewController : UIViewController


@property(nonatomic,weak) id<SCMainNavControllerDelegate> delegate;
@property(nonatomic,strong) SCSettings* settings;


@property (weak, nonatomic) IBOutlet SCTextField *emailTextField;
@property (weak, nonatomic) IBOutlet SCButton *emailRegistrationOutlet;

@property(strong, nonatomic) SCSkinManager* skinManager;


- (IBAction)emailRegistrationAction:(id)sender;

@end
