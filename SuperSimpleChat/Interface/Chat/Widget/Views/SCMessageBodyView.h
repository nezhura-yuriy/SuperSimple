//
//  SCMessageBodyView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCTypes.h"

@class SCSettings,SCMessageBodys;

@interface SCMessageBodyView : UIView



@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,assign) CGFloat maxWidht;
//@property(nonatomic,strong) UIColor* textColor;
@property(nonatomic,assign) SCMessageAlignment bodyAlignment;
//@property(nonatomic,strong) NSArray* messageBodys;

-(id) initWithSettings:(SCSettings*) settings;
-(void) setMessageBodys:(SCMessageBodys*) messageBodys;
-(CGSize) viewSize;
@end
