//
//  SCPBManagerTernopol+Messages.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol(Messages).h"
#import "SCPBManagerTernopol(Converts).h"

//#import "SCSettings.h" in SCPBManagerTernopol.h
#import "SmartChat.pb.h"
#import "SCPUUID.h"

#import "SCChatItem.h"
//#import "SCContactItem.h"
#import "SCMessageItem.h"
#import "SCMessageBodyItem.h"

#import "SCModelChats.h"
#import "SCModelUser.h"

@implementation SCPBManagerTernopol (Messages)

#define FOR_FILE_ID_AUDIO @"1"
#define FOR_FILE_ID_VIDEO @"2"
#define FOR_FILE_ID_PHOTO @"3"
/* =======================================================================================
 message SendMessageRequest {
 required PUUID message_id = 1;
 optional PUUID user_id = 2;
 optional PUUID chat_room_id = 3;
 optional sint64 message_date = 4;
 optional sint64 time_destroy = 5;
 optional sint64 security_level = 6;
 repeated MessageBody bodies = 7;
 }*/
-(void) sendMessage:(id)params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCMessageItem* messageItem = [params objectForKey:@"SCMessageItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        
        SendMessageRequestBuilder* _sendMessageRequestBuilder = [[SendMessageRequestBuilder alloc] init];
        [_sendMessageRequestBuilder setMessageId:[self puuidBuilder:[[SCPUUID alloc] initWithRandom]]];
        [_sendMessageRequestBuilder setChatRoomId:[self puuidBuilder:messageItem.chat.chatPuuid]];
        [_sendMessageRequestBuilder setTimeDestroy:messageItem.timeDestroy];
        
        for(SCMessageBodyItem* messageBody in [messageItem.messageBodys items])
        {
            MessageBodyBuilder *_messageBodyBuilder = [[MessageBodyBuilder alloc] init];
            
            /*
            if(messageBody.type == SCDataTypeText) //old SCMessageDataTypeText
            {
                [_messageBodyBuilder setType:MessageDataTypeText];
                [_messageBodyBuilder setText:(NSString*) messageBody.data];
                [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
            }
            */
            
            switch (messageBody.type)
            {
                case SCDataTypeText:
                {
                    [_messageBodyBuilder setType:MessageDataTypeText];
                    [_messageBodyBuilder setText:(NSString*) messageBody.data];
                    [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
                }
                    break;
                    
                case SCDataTypeAudio:
                {
                    [_messageBodyBuilder setType:MessageDataTypeFileIdAudio];
                    [_messageBodyBuilder setText:FOR_FILE_ID_AUDIO];
                    [_messageBodyBuilder setFileId:[self puuidBuilder:[[SCPUUID alloc] initFromString:(NSString*)messageBody.data]]];
                    [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
                }
                    break;
                    
                case SCDataTypeAudioFile:
                {
                    [_messageBodyBuilder setType:MessageDataTypeFileIdAudio];
                    [_messageBodyBuilder setText:FOR_FILE_ID_AUDIO];
                    [_messageBodyBuilder setFileId:[self puuidBuilder:[[SCPUUID alloc] initFromString:messageBody.netFile.netPath]]];
                    [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
                }
                    break;
                    
                case SCDataTypeVideo:
                {
                    [_messageBodyBuilder setType:MessageDataTypeFileIdVideo];
                    [_messageBodyBuilder setText:FOR_FILE_ID_VIDEO];
                    [_messageBodyBuilder setFileId:[self puuidBuilder:[[SCPUUID alloc] initFromString:(NSString*)messageBody.data]]];
                    [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
                }
                    break;

                case SCDataTypeVideoFile:
                {
                    [_messageBodyBuilder setType:MessageDataTypeFileIdVideo];
                    [_messageBodyBuilder setText:FOR_FILE_ID_VIDEO];
                    [_messageBodyBuilder setFileId:[self puuidBuilder:[[SCPUUID alloc] initFromString:messageBody.netFile.netPath]]];
                    [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
                }
                    break;
                    
                case SCDataTypePhoto:
                {
                    [_messageBodyBuilder setType:MessageDataTypeFileIdPhoto];
                    [_messageBodyBuilder setText:FOR_FILE_ID_PHOTO];
                    [_messageBodyBuilder setFileId:[self puuidBuilder:[[SCPUUID alloc] initFromString:(NSString*)messageBody.data]]];
                    [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
                }
                    break;
                case SCDataTypePhotoFile:
                {
                    [_messageBodyBuilder setType:MessageDataTypeFileIdPhoto];
                    [_messageBodyBuilder setText:FOR_FILE_ID_PHOTO];
                    [_messageBodyBuilder setFileId:[self puuidBuilder:[[SCPUUID alloc] initFromString:messageBody.netFile.netPath]]];
                    [_sendMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
                }
                    break;

                default:
                    break;
            }
        }
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeSendMessage];
        [_requestBuilder setRequest:[_sendMessageRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];
        [_socketClient sendData:dataToSend];
    }
}

-(void) parseSendMessage:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        SCMessageItem* messageItem = [params objectForKey:@"SCMessageItem"];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            SendMessageResponse* sendMessageResponse = [SendMessageResponse parseFromData:response.response];
            messageItem.messageID = [[self scpuuidBuilder:sendMessageResponse.messageId] scpuuidToString];
            messageItem.messageDateTime = [NSDate dateWithTimeIntervalSince1970:((SInt64)sendMessageResponse.messageDate/1000)];
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeSendMessage data:messageItem];
            }
        }
        else
        {
            FTLog(@"Error parseSendMessage response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}
/* =======================================================================================
message UpdateMessageRequest {
    repeated MessageBody bodies = 1;
    required PUUID chat_room_id = 2;
    required PUUID message_id = 3;
}*/
-(void) updateMessage:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCMessageItem* messageItem = [params objectForKey:@"SCMessageItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        UpdateMessageRequestBuilder* _updateMessageRequestBuilder = [[UpdateMessageRequestBuilder alloc] init];
        [_updateMessageRequestBuilder setMessageId:[self puuidBuilder:[[SCPUUID alloc] initFromString:messageItem.messageID]]];
        [_updateMessageRequestBuilder setChatRoomId:[self puuidBuilder:messageItem.chat.chatPuuid]];
        MessageBodyBuilder *_messageBodyBuilder = [[MessageBodyBuilder alloc] init];
        for(SCMessageBodyItem* messageBody in [messageItem.messageBodys items])
        {
            if(messageBody.type == SCDataTypeText) //old SCMessageDataTypeText
            {
                [_messageBodyBuilder setType:MessageDataTypeText];
                [_messageBodyBuilder setText:(NSString*) messageBody.data];
                [_updateMessageRequestBuilder addBodies:[_messageBodyBuilder build]];
            }
        }

        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeUpdateMessage];
        [_requestBuilder setRequest:[_updateMessageRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];
        [_socketClient sendData:dataToSend];
    }
}
-(void) parseUpdateMessage:(id) data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        SCMessageItem* messageItem = [params objectForKey:@"SCMessageItem"];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeUpdateMessage data:messageItem];
            }
        }
        else
        {
            FTLog(@"Error parseSendMessage response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

/* =======================================================================================
message RemoveMessageRequest {
    required PUUID message_id = 1;
    optional PUUID chat_room_id = 3;
}*/
-(void) removeMessage:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        SCMessageItem* messageItem = [params objectForKey:@"SCMessageItem"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        RemoveMessageRequestBuilder* _removeMessageRequestBuilder = [[RemoveMessageRequestBuilder alloc] init];
        [_removeMessageRequestBuilder setMessageId:[self puuidBuilder:[[SCPUUID alloc] initFromString:messageItem.messageID]]];
        [_removeMessageRequestBuilder setChatRoomId:[self puuidBuilder:messageItem.chat.chatPuuid]];
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeRemoveMessage];
        [_requestBuilder setRequest:[_removeMessageRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * query = [_requestBuilder build];
        NSData* dataToSend = [query data];
        [_socketClient sendData:dataToSend];
    }

}
-(void) parseRemoveMessage:(id) data // parseRemoveMessageFromChatRoom:response
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        SCMessageItem* messageItem = [params objectForKey:@"SCMessageItem"];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            messageItem.isDeleted = YES;
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeRemoveMessage data:messageItem];
            }
        }
        else
        {
            FTLog(@"Error parseSendMessage response.status.success");
        }
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}
// DEPRECATED
/* =======================================================================================
 message GetHistoryRequest {
 optional int32 offset = 2 [default = 0];
 optional int32 limit = 3 [default = 20];
 }* /
-(void) getHistory:(id) params
{
    if([params isKindOfClass:[NSDictionary class]])
    {
        SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
        
        GetListRequestBuilder* getHistoryRequestBuilder = [[GetHistoryRequestBuilder alloc] init];
        
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setType:CallTypeHistoryGet];
        [_requestBuilder setRequest:[getHistoryRequestBuilder build].data];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        Request * _query = [_requestBuilder build];
        NSData* _dataToSend = [_query data];
        [_socketClient sendData:_dataToSend];
    }
}
-(void) parseHistory:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeGetProfile data:data];
            }
            else
            {
                FTLog(@"Error");
            }
        }
    }
    else
    {
        FTLog(@"Error");
    }
    
}*/

/* =======================================================================================
 message GetChatRoomHistoryRequest {
 required PUUID chat_room_id = 1;
 optional int32 offset = 2 [default = 0];
 optional int32 limit = 3 [default = 20];
 }*/
-(void) getChatRoomHistory:(id) params
{
    SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
    SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
    
    [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
    
    GetChatRoomHistoryRequestBuilder* _getChatRoomHistoryRequestBuilder = [[GetChatRoomHistoryRequestBuilder alloc] init];
    [_getChatRoomHistoryRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
    if([[params allKeys] containsObject:@"offset"])
        [_getChatRoomHistoryRequestBuilder setOffset:(SInt32)[[params objectForKey:@"offset"] integerValue]];
    if([[params allKeys] containsObject:@"limit"])
        [_getChatRoomHistoryRequestBuilder setLimit:(SInt32)[[params objectForKey:@"limit"] integerValue]];
    
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeChatRoomHistoryGet];
    [_requestBuilder setRequest:[_getChatRoomHistoryRequestBuilder build].data];
    [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
    Request * _query = [_requestBuilder build];
    NSData* _dataToSend = [_query data];
    [_socketClient sendData:_dataToSend];
    
}
/*
 message GetChatRoomHistoryResponse {
 repeated Message messages = 1;
 }*/

-(void) parseGetChatRoomHistory:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
//        SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        if(response.status.success)
        {
            GetChatRoomHistoryResponse *_getChatRoomHistoryResponse = [GetChatRoomHistoryResponse parseFromData:response.response];
            NSArray* mesArray = _getChatRoomHistoryResponse.messages;
            NSMutableArray *outArray = [[NSMutableArray alloc] initWithCapacity:mesArray.count];
            for(Message* mes in mesArray)
            {
                SCMessageItem *messageItem = [self _messageToSCMessageItem:mes];
                [outArray addObject:messageItem];
            }
            
            // GETUSER
            if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
            {
                [delegate parsedType:SCPBManagerParceTypeGetChatRoomHistory data:outArray];
            }

        }
        else
        {
            FTLog(@"Error parseGetChatRoomHistory response.status.success");
        }
        
    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}
/* =======================================================================================
message GetChatRoomHistoryWithTimeRangeRequest {
    required PUUID chat_room_id = 1; /// ChatRoom id
    required sint64 start_time = 2; /// Get messages from start time of time range
    required sint64 end_time = 3; /// Get messages untill end time of time range
    optional int32 offset = 4 [default = 0]; /// Start get messages from **offset** position. For example, if **offset** is 10 - get all messages skipping first 10 messages.
    optional int32 limit = 5 [default = 20]; /// Messages limit
}*/

-(void) getChatRoomHistoryWithTimeRangeRequest:(id) params
{
    SCPUUID *transportPUUID = [params objectForKey:@"UUID"];
    SCChatItem* chatItem = [params objectForKey:@"SCChatItem"];
    NSDate* dtStart;
    NSDate* dtEnd;
    SInt32 _offset = 0;
    SInt32 _limit = 20;
    

    if([[params allKeys] containsObject:@"DateEnd"])
        dtEnd = [params objectForKey:@"DateEnd"];
    else
    {
        dtEnd = [NSDate date];
    }

    if([[params allKeys] containsObject:@"DateStart"])
        dtStart = [params objectForKey:@"DateStart"];
    else
    {
        dtStart = [dtEnd dateByAddingTimeInterval:-60*60*24*30];
    }
    
    [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
    
    GetChatRoomHistoryWithTimeRangeRequestBuilder* _getChatRoomHistoryWithTimeRangeRequestBuilder = [[GetChatRoomHistoryWithTimeRangeRequestBuilder alloc] init];
    [_getChatRoomHistoryWithTimeRangeRequestBuilder setChatRoomId:[self puuidBuilder:chatItem.chatPuuid]];
    [_getChatRoomHistoryWithTimeRangeRequestBuilder setStartTime:(SInt64)[dtStart timeIntervalSince1970]*1000];
    [_getChatRoomHistoryWithTimeRangeRequestBuilder setEndTime:(SInt64)[dtEnd timeIntervalSince1970]*1000];
/*
    if([[params allKeys] containsObject:@"offset"])
        [_getChatRoomHistoryWithTimeRangeRequestBuilder setOffset:(SInt32)[[params objectForKey:@"offset"] integerValue]];
    if([[params allKeys] containsObject:@"limit"])
        [_getChatRoomHistoryWithTimeRangeRequestBuilder setLimit:(SInt32)[[params objectForKey:@"limit"] integerValue]];
*/
    if([[params allKeys] containsObject:@"offset"])
    {
        _offset = [[params objectForKey:@"offset"] intValue];
        [_getChatRoomHistoryWithTimeRangeRequestBuilder setOffset:_offset];
    }
    if([[params allKeys] containsObject:@"limit"])
    {
        _limit = [[params objectForKey:@"limit"] intValue];
        [_getChatRoomHistoryWithTimeRangeRequestBuilder setLimit:_limit];
    }
    
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeChatRoomHistoryGetWithTimeRange];
    [_requestBuilder setRequest:[_getChatRoomHistoryWithTimeRangeRequestBuilder build].data];
    [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
    Request * _query = [_requestBuilder build];
    NSData* _dataToSend = [_query data];
    [_socketClient sendData:_dataToSend];
}

/*
 message GetChatRoomHistoryWithTimeRangeResponse {
 repeated Message messages = 1;
 }*/
-(void) parseGetChatRoomHistoryWithTimeRangeResponse:(id)data
{
    Response * response = data;
    SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
    NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
    if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
    {
        NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
        id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
        [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        
        NSInteger _offset = 0;
        if([[params allKeys] containsObject:@"offset"])
            _offset = [[params objectForKey:@"offset"] integerValue];
        
        NSInteger _limit = 20;
        if([[params allKeys] containsObject:@"limit"])
        {
            _limit = [[params objectForKey:@"limit"] integerValue];
        }

        if(response.status.success)
        {
            GetChatRoomHistoryResponse *_getChatRoomHistoryResponse = [GetChatRoomHistoryResponse parseFromData:response.response];
            NSArray* mesArray = _getChatRoomHistoryResponse.messages;
            
            NSMutableArray* outArray;
            if([[params allKeys] containsObject:@"outArray"])
                outArray = [params objectForKey:@"outArray"];
            else
                outArray = [[NSMutableArray alloc] init];
            
            NSMutableArray* outAr = [[NSMutableArray alloc] init];

            for(Message* mes in mesArray)
            {
//                FTLog(@"%@",mes);
                SCMessageItem* messageItem = [self _messageToSCMessageItem:mes];
                [outAr addObject:messageItem];
            }
            if(outAr.count == _limit)
            {
                [params setObject:[NSNumber numberWithInteger:_offset + _limit] forKey:@"offset"];
                [outArray addObjectsFromArray:outAr];
                [params setObject:outArray forKey:@"outArray"];
                [self getChatRoomHistoryWithTimeRangeRequest:params];
            }
            else
            {
                [outArray addObjectsFromArray:outAr];
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                {
                    [delegate parsedType:SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange data:outArray];
                }
            }
            
        }
        else
        {
            FTLog(@"Error parseGetAllRoomsForUser response.status.success");
        }

    }
    else
    {
        FTLog(@"Error transportPUUID");
    }
}

#pragma mark - notifications
/* =======================================================================================
 
 */
-(void) parseReceiveMessage:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        Message* mes = [Message parseFromData:response.response];
        SCMessageItem *messageItem = [self _messageToSCMessageItem:mes];

        [_settings play:SCSoundPlayMessageReceive];
//        if([messageItem respondsToSelector:@selector(parsedType:data:)])
//        {
//            [messageItem parsedType:SCPBManagerParceTypeMessage data:messageItem];
//        }
        
        if([_settings.modelChats respondsToSelector:@selector(parsedType:data:)])
        {
            [_settings.modelChats parsedType:SCPBManagerParceTypeMessage data:messageItem];
        }
    }
    else
    {
        FTLog(@"Error parseReceiveMessage response.status.success");
    }
}

/* =======================================================================================
message RefreshedMessageEvent {
    required PUUID chat_room_id = 1;
    required PUUID message_id = 2;
}*/
-(void) parseRefreshedMessageEvent:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        RefreshedMessageEvent* refreshedMessageEvent = [RefreshedMessageEvent parseFromData:response.response];
        
        SCChatItem* inChat = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:refreshedMessageEvent.chatRoomId]];
        NSMutableArray* refreshedMessages = [[NSMutableArray alloc] init];
        for(PUUID* mesPuuid in refreshedMessageEvent.messageId)
        {
            SCMessageItem *_messageItem = [inChat searchMessageItemByPUUID:[self scpuuidBuilder:mesPuuid]];
            if(_messageItem)
            {
                [refreshedMessages addObject:_messageItem];
            }
        }
        
        [inChat parsedType:SCPBManagerParceTypeRefreshedMessageEvent data:refreshedMessages];
        
    }
    else
    {
        FTLog(@"Error parseReceiveMessage response.status.success");
    }

}
-(void) parseUpdateMessageInChatroomEvent:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        RefreshedMessageEvent* refreshedMessageEvent = [RefreshedMessageEvent parseFromData:response.response];
        
        SCChatItem* inChat = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:refreshedMessageEvent.chatRoomId]];
        NSMutableArray* refreshedMessages = [[NSMutableArray alloc] init];
        for(PUUID* mesPuuid in refreshedMessageEvent.messageId)
        {
            SCMessageItem *_messageItem = [inChat searchMessageItemByPUUID:[self scpuuidBuilder:mesPuuid]];
            if(_messageItem)
            {
                [refreshedMessages addObject:_messageItem];
            }
        }
        
        [inChat parsedType:SCPBManagerParceTypeUpdateMessageInChatroomEvent data:refreshedMessages];
        
    }
    else
    {
        FTLog(@"Error parseReceiveMessage response.status.success");
    }
}

/* =======================================================================================
 message RefreshedMessageEvent {
 required PUUID chat_room_id = 1;
 repeated PUUID message_id = 2;
 }*/


-(void) parseRemoveMessageFromChatRoomEvent:(id) data // parseRemoveMessageFromChatRoom:response
{
    Response * response = data;
    
    RefreshedMessageEvent* removedMessageEvent = [RefreshedMessageEvent parseFromData:response.response];
    if(response.status.success)
    {
        SCChatItem* inChat = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:removedMessageEvent.chatRoomId]];
        NSMutableArray* removedMessages = [[NSMutableArray alloc] init];
        for(PUUID* mesPuuid in removedMessageEvent.messageId)
        {
            SCMessageItem *_messageItem = [inChat searchMessageItemByPUUID:[self scpuuidBuilder:mesPuuid]];
            if(_messageItem)
            {
                _messageItem.isDeleted = YES;
                [removedMessages addObject:_messageItem];
            }
        }
        
        [inChat parsedType:SCPBManagerParceTypeRemoveMessageFromChatroomEvent data:removedMessages];
    }
    else
    {
        FTLog(@"Error parseSendMessage response.status.success");
    }
}


#pragma mark - PRIVATE
-(SCMessageItem*) _messageToSCMessageItem:(Message*) inMessage
{
    SCChatItem* inChat = [_settings.modelChats searchChatWithSCPUUID:[self scpuuidBuilder:inMessage.chatRoomId]];
    SCMessageItem *_messageItem = nil;

    if( inChat == nil )
    {
        FTLog(@"Капець");
        inChat = [[SCChatItem alloc] initWithSettings:_settings];
        inChat.chatType = SCChatTypeSearch;
    }
//    else
    
    {
        //
        NSDate* messageDate = [NSDate dateWithTimeIntervalSince1970:((SInt64)inMessage.messageDate/1000)];
//        FTLog(@"\nmessageDate = %@\n",messageDate);
        _messageItem = [inChat searchMessageItemByPUUID:[self scpuuidBuilder:inMessage.id] messageDateTime:messageDate];
        if(!_messageItem)
            _messageItem = [inChat newMessageItemByPUUID:[self scpuuidBuilder:inMessage.id] messageDateTime:messageDate];

        _messageItem.contact = [_settings.modelUsers getUserByPUUIDString:[[self scpuuidBuilder:inMessage.userFrom] scpuuidToString]];
        if(inMessage.hasSecurityLevel)
            _messageItem.securityLevel = (SInt32) inMessage.securityLevel;
        if(inMessage.hasTimeDestroy)
            _messageItem.timeDestroy = inMessage.timeDestroy;
        
        if(_messageItem.timeDestroy > 0)
        {
            _messageItem.dateDestroy = [_messageItem.messageDateTime dateByAddingTimeInterval:_messageItem.timeDestroy];
//            NSDate* dt = [NSDate date];
            NSComparisonResult result = [[NSDate date] compare:_messageItem.dateDestroy];
            if(result == NSOrderedDescending)
            {
                _messageItem.isDeleted = YES;
            }
        }
        
        for(MessageBody *mesBody in inMessage.bodies)
        {
            NSUInteger bodyIdx = [inMessage.bodies indexOfObject:mesBody];
            SCMessageBodyItem* messageBody = [_messageItem.messageBodys editAtIndex:bodyIdx];
//            SCMessageBody* messageBody = [[SCMessageBody alloc] init];
            switch (mesBody.type)
            {
                case MessageDataTypeText:
                {
                    messageBody.type = SCDataTypeText; //old SCMessageDataTypeText
                    messageBody.data = mesBody.text;
                }
                    break;
                    
                case MessageDataTypeFileId:
                {
                    NSString* mark = mesBody.text;
                    if([mark isEqualToString:FOR_FILE_ID_AUDIO])
                    {
                        messageBody.type = SCDataTypeAudio;
                    }
                    else if ([mark isEqualToString:FOR_FILE_ID_VIDEO])
                    {
                        messageBody.type = SCDataTypeVideo;
                    }
                    else if ([mark isEqualToString:FOR_FILE_ID_PHOTO])
                    {
                        messageBody.type = SCDataTypePhoto;
                    }
                    else
                        messageBody.type = SCDataTypeVideo;
                    messageBody.data = [self scpuuidStringBuilder:mesBody.fileId];
                }
                    break;
                    
                case MessageDataTypeFileIdAudio:
                    messageBody.type = SCDataTypeAudio;
                    messageBody.data = [self scpuuidStringBuilder:mesBody.fileId];
                    break;
                    
                case MessageDataTypeFileIdVideo:
                    messageBody.type = SCDataTypeVideo;
                    messageBody.data = [self scpuuidStringBuilder:mesBody.fileId];
                    break;
                    
                case MessageDataTypeFileIdPhoto:
                    messageBody.type = SCDataTypePhoto;
                    messageBody.data = [self scpuuidStringBuilder:mesBody.fileId];
                    break;
                    
                case MessageDataTypeSystemMessages:
                {
                    NSString* userSys = @"";
                    for(PUUID* puid in mesBody.systemMessages.usersId)
                    {
                        SCContactItem* contact = [_settings.modelUsers getUserByPUUIDString:[self scpuuidStringBuilder:puid]];
                        userSys = [userSys stringByAppendingFormat:@"%@,",contact.contactNikName];
                    }
                    switch (mesBody.systemMessages.type)
                    {
                        case SystemMessagesTypeLeaveUsers:
                        {
                            messageBody.type = SCDataTypeSystemText;
                            messageBody.data = [NSString stringWithFormat:@"User %@, leave from chatroom",userSys];
                        }
                            break;
                        case SystemMessagesTypeJoinUsers:
                        {
                            messageBody.type = SCDataTypeSystemText;
                            messageBody.data = [NSString stringWithFormat:@"User %@, join to chatroom",userSys];
                        }
                            break;
                        case SystemMessagesTypeChangeChatroomInfo:
                        {
                            SCContactItem* contact = [_settings.modelUsers getUserByPUUIDString:[self scpuuidStringBuilder:mesBody.systemMessages.ownerId]];
                            messageBody.type = SCDataTypeSystemText;
                            messageBody.data = [NSString stringWithFormat:@"User %@, change chatroom information",contact.contactNikName];
                        }
                            break;
                        case SystemMessagesTypeMessageRemove:
                        {
                            messageBody.type = SCDataTypeSystemText;
                            messageBody.data = [NSString stringWithFormat:@"User %@, remove message in chatroom",userSys];
                        }
                            break;
                            
                        default:
                            break;
                    }
                    
                }
                    break;
                    
                default:
                {
                    FTLog(@"R");
                }
                    break;
            }
            
//            [_messageItem.messageBodysCL addItem:messageBody];
        }
    }
    
    return _messageItem;
}

@end
