//
//  SCVerificationsViewController.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 21.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"
#import "SCButton.h"
#import "SCLabel.h"
#import "SCTextField.h"

@class SCSettings;
@class SCSkinManager;

@interface SCVerificationsViewController : UIViewController

@property(nonatomic,weak) id<SCMainNavControllerDelegate> delegate;
@property(assign, nonatomic) BOOL isPhoneVerification;
@property(nonatomic,strong) SCSettings* settings;


@property (weak, nonatomic) IBOutlet SCLabel *infoLabel;
@property (weak, nonatomic) IBOutlet SCTextField *codeTextField;
@property (weak, nonatomic) IBOutlet SCButton* goToSmartChatButton;
@property (weak, nonatomic) IBOutlet SCButton *testWithoutRegistrationOutlet;
@property(strong, nonatomic) SCSkinManager* skinManager;



- (IBAction)goToSmartChatButton:(id)sender;

- (IBAction)testWithoutRegistrationActon:(id)sender;



@end
