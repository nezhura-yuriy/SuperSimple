//
//  SCTopSelector.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 26.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCTopSelector.h"
#import "SCSettings.h"

@implementation SCTopSelectorItem
{
    SCSettings* _settings;
    UILabel* _itemName;
    UILabel* _itemBadge;
}

- (instancetype)initWithSettings:(SCSettings*) settings itemName:(NSString*) itemName
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        
        _itemName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 200)];
        _itemName.text = itemName;
        _itemName.textAlignment = NSTextAlignmentCenter;
        _itemName.font = [UIFont fontWithName:FT_FontNameLight size:12.0];
        _itemName.backgroundColor = [UIColor clearColor];
        [_itemName sizeToFit];
        [self addSubview:_itemName];
        
        _itemBadge = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _itemBadge.clipsToBounds = YES;
        _itemBadge.textAlignment = NSTextAlignmentCenter;
        _itemBadge.textColor = [UIColor whiteColor];
        _itemBadge.font = [UIFont fontWithName:FT_FontNameLight size:12];
        _itemBadge.backgroundColor = SKCOLOR_ChatListBadge;//[UIColor greenColor];//
//        _itemBadge.text = @"0";
        _itemBadge.hidden = YES;
        [self addSubview:_itemBadge];
        
//        self.layer.borderColor = [[UIColor redColor] CGColor];
//        self.layer.borderWidth = 1;
    }
    return self;
}

- (void) dealloc
{
    
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        _itemName.center = CGPointMake(self.frame.size.width*.45, self.frame.size.height*.5);// self.center;
        _itemBadge.frame = CGRectMake(_itemName.frame.origin.x + _itemName.frame.size.width + 5,
                                      _itemName.frame.origin.y*0.9,  20, 20);
        _itemBadge.layer.cornerRadius = MAX(_itemBadge.frame.size.width, _itemBadge.frame.size.height)*0.5;
        //CGPointMake(_itemName.frame.origin.x + _itemName.frame.size.width + 2, _itemName.frame.origin.y);
    }
}

-(void) setBadgeValue:(NSInteger) value
{
    if(value > 0)
    {
        _itemBadge.text = [NSString stringWithFormat:@"%ld",(long)value];
    //    [_itemBadge sizeToFit];
        _itemBadge.layer.cornerRadius = MAX(_itemBadge.frame.size.width, _itemBadge.frame.size.height)*0.5;
        _itemBadge.hidden = NO;
    }
    else
        _itemBadge.hidden = YES;
}

@end



@implementation SCTopSelector
{
    SCSettings* _settings;
    NSMutableArray* items;
    UIImageView* _separator;
    UIImageView* _marker;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self _init];
    }
    return self;
}

- (instancetype)initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
    }
    return self;
}

-(void) _init
{
    _activeSegment = 0;
    items = [[NSMutableArray alloc] init];
    _separator = [[UIImageView alloc] init];
    _separator.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:_separator];
    
    _marker =  [[UIImageView alloc] init];
    _marker.backgroundColor = [UIColor lightGrayColor];
    _marker.layer.cornerRadius = 5;
    [self addSubview:_marker];
    
}

- (void)dealloc
{
    FTLog(@"%@ dealoc",NSStringFromClass([self class]));
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        [self reFrame:frame];
    }
}

-(void) reFrame:(CGRect) frame
{
    CGFloat itemWidth = frame.size.width/items.count;
    int i =0;
    for(SCTopSelectorItem* item in items)
    {
        item.frame = CGRectMake(i*itemWidth, 0, itemWidth, 44);
        i++;
    }
    _separator.frame = CGRectMake(10, self.frame.size.height - 1, self.frame.size.width - 20, 1);
    _marker.frame = CGRectMake(0, 0, itemWidth*0.85, 28);
    if(items.count >0)
        _marker.center = ((SCTopSelectorItem*)[items objectAtIndex:_activeSegment]).center;
}

-(void) clearItems
{
    [items removeAllObjects];
}

-(void) addItemsWithTitles:(NSArray*) titles
{
    for(NSString* title in titles)
        [self addItemWithTitle:title];
}

-(void) addItemWithTitle:(NSString*) title
{
    SCTopSelectorItem* newItem = [[SCTopSelectorItem alloc] initWithSettings:_settings itemName:title];
    [newItem addTarget:self action:@selector(onChange:) forControlEvents:UIControlEventTouchUpInside];
    newItem.tag = items.count;
    [items addObject:newItem];
    [self addSubview:newItem];
    [self reFrame:self.frame];
}

-(void) setActiveSegment:(NSUInteger)activeSegment
{
    _activeSegment = activeSegment;
    [UIView animateWithDuration:0.5 animations:^{
        _marker.center = ((SCTopSelectorItem*)[items objectAtIndex:_activeSegment]).center;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) badgeValue:(NSInteger) value atIndex:(NSUInteger) atIndex
{
    SCTopSelectorItem* item = [items objectAtIndex:atIndex];
    [item setBadgeValue:value];
}

-(void) onChange:(SCTopSelectorItem*) sender
{
    [self setActiveSegment:sender.tag];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    
}

@end
