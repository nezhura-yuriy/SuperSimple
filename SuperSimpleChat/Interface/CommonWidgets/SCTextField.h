//
//  SCTextField.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 17.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCTextField : UITextField

@end
