//
//  SCPBManagerTernopol+Contacts.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol(Contacts).h"
#import "SCPBManagerTernopol(Converts).h"
#import "SCPBManagerTernopol(User).h"

//#import "SCSettings.h" in SCPBManagerTernopol.h
#import "SmartChat.pb.h"
#import "SCPUUID.h"

#import "SCContactItem.h"
#import "SCmodelContacts.h"
#import "SCAddressbookContact.h"

@implementation SCPBManagerTernopol (Contacts)

/* =======================================================================================
 CONTACT_CREATE
 */
-(void) createContactRequestWithParams:(NSMutableDictionary*) params
{
    
    if([params isKindOfClass:[NSMutableDictionary class]]){
        
        SCPUUID *transportPUUID = nil;
        
        if([[params allKeys] containsObject:@"fieldsForCreatOneContact"])
        {
            
            transportPUUID = [params objectForKey:@"PUUID"];
            
            [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
            
        } else {
            
            transportPUUID = [params objectForKey:@"idFilds"];
            
            [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
            
        }
        
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_requestBuilder setType:CallTypeContactCreate];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
}

- (void) parseCreateContact:(id)data
{
    Response * response = data;
    if(response.status.success)
        
    {
        
        SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
        
        NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
        
        if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring]) {
            
            
            NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
            
            
            [_delegateDictionary removeObjectForKey:transportPUUIDstring];
            
            PUUID* resp = [CreateContactResponse parseFromData:response.response].id;
            
            SCPUUID* puuid = [[SCPUUID alloc]initWithLeastSignificantBits:resp.leastSignificantBits MostSignificantBits:resp.mostSignificantBits];
            
            if([[params allKeys] containsObject:@"fieldsForCreatOneContact"])
            {
                
                SCContactItem* contact = [params objectForKey:@"fieldsForCreatOneContact"];
                
                contact.userPUUID = contact.userPUUID;
                contact.contactPUUID = puuid;
                
                contact.user = [_settings.modelUsers getUserByPUUID:contact.userPUUID];
                
                [params setObject:contact forKey:@"fieldsForCreatOneContact"];
                
                NSDictionary *paramsSubscribe = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            self, @"delegate",
                                            [[SCPUUID alloc] initWithRandom], @"UUID",
                                            contact.userPUUID,@"userUUID",
                                            nil];
                
//                [self getAllContactsRequestWithParams:nil];

                [self subscribeRequest:paramsSubscribe];
                
                
            } else {
                
                SCContactItem* contact = [params objectForKey:@"fields"];
                
                contact.contactPUUID = puuid;
                
                [params setObject:contact forKey:@"fields"];
                
                NSDictionary *paramsUnSubscribe = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                 self, @"delegate",
                                                 [[SCPUUID alloc] initWithRandom], @"UUID",
                                                 contact.userPUUID,@"userUUID",
                                                 nil];
                

                [self subscribeRequest:paramsUnSubscribe];
            }
            
            [self setFieldsRequestWithParams:params];
            
        }
        
        
        
    }
    else
    {
        FTLog(@"Error");
    }
    
    
}


/* =======================================================================================
 CONTACT_REMOVE
 */

-(void) removeContactRequest:(SCContactItem*) contact

{
    
    NSMutableDictionary* params = [NSMutableDictionary dictionary];
    SCPUUID * transportPUUID = [[SCPUUID alloc] initWithRandom];
    [params setObject:contact forKey:@"contact"];
    [params setObject:transportPUUID forKey:@"UUID"];

    
    [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
    
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeContactRemove];
    
    RemoveContactRequestBuilder* _removeContactRequestBuilder = [[RemoveContactRequestBuilder alloc] init];
    
    [_removeContactRequestBuilder setId:[self puuidBuilder:contact.contactPUUID]];
    
    [_requestBuilder setRequest:[_removeContactRequestBuilder build].data];
    [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
    
    Request * query = [_requestBuilder build];
    [_socketClient sendData:[query data]];
    
    NSDictionary *paramsUnSubscribe = [[NSDictionary alloc] initWithObjectsAndKeys:
                                       self, @"delegate",
                                       [[SCPUUID alloc] initWithRandom], @"UUID",
                                       contact.userPUUID,@"userUUID",
                                       nil];
    [self unSubscribeRequest:paramsUnSubscribe];
    
    
}

- (void) parseRemoveContact:(id)data
{
    Response * response = data;
    
    if(response.status.success)
    {
        
        SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
        
        NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
        
        SCContactItem* contact = nil;
        
        if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring]) {
            
            NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
            
            contact = [params objectForKey:@"contact"];
            
            
            [_delegateDictionary removeObjectForKey:transportPUUIDstring];
        }
        
        if([_settings.modelContacts respondsToSelector:@selector(parsedType:data:)])
            
            [_settings.modelContacts parsedType:SCPBManagerParceTypeContactRemove data:contact];
        
    }
    else
    {
        
        if([_settings.modelContacts respondsToSelector:@selector(parsedType:data:)])
            
            [_settings.modelContacts parsedType:SCPBManagerParceTypeContactRemove data:[NSString stringWithFormat:@"Error delete"]];

        FTLog(@"Error");
    }
}


/* =======================================================================================
 CONTACT_FIELDS_GET
 */
-(void)getAllContactFieldsRequest:(SCContactItem*) contact
{
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeContactFieldsGet];
    
    GetAllContactFieldsRequestBuilder* _getAllContactFieldsRequestBuilder = [[GetAllContactFieldsRequestBuilder alloc] init];
    
    [_getAllContactFieldsRequestBuilder setId:[self puuidBuilder:contact.contactPUUID]];
    
    [_requestBuilder setRequest:[_getAllContactFieldsRequestBuilder build].data];
    
    Request * query = [_requestBuilder build];
    [_socketClient sendData:[query data]];
}

- (void) parseGetAllContactFields:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        
        
        NSArray* fields = [GetAllContactFieldsResponse parseFromData:response.response].fields;
        
        SCContactItem* contact = [[SCContactItem alloc] init];
        
//        contact = [self setContactFields:contact fields:fields];
        
        [self contactFromFields:contact fields:fields];
       
        
        [self callDelegate:SCPBManagerParceTypeGetAllContactFields withData:contact];
        /*
        if([_settings.modelContacts respondsToSelector:@selector(parsedType:data:)])
            [_settings.modelContacts parsedType:SCPBManagerParceTypeGetAllContactFields data:contact];
        */
    }
    else
    {
        FTLog(@"Error");
    }
}


/* =======================================================================================
 CONTACT_SET_FIELDS
 */
-(void)setFieldsRequestWithParams:(NSMutableDictionary*) params

{
    
    SCPUUID *transportPUUIDForGetParams = nil;
    
    SCContactItem* contact = nil;
    
    if([[params allKeys] containsObject:@"fieldsForCreatOneContact"])
    {
        
        contact = [params objectForKey:@"fieldsForCreatOneContact"];
        
        transportPUUIDForGetParams = [params objectForKey:@"PUUID"];
        
        [_delegateDictionary setObject:params forKey:[transportPUUIDForGetParams scpuuidToString]];
        
    } else {
        
        contact = [params objectForKey:@"fields"];
        
        transportPUUIDForGetParams = [params objectForKey:@"id"];
        
    }
    
    
    SetFieldsRequestBuilder* _setFieldsRequestBuilder = [[SetFieldsRequestBuilder alloc] init];
    
    [_setFieldsRequestBuilder setContactId:[self puuidBuilder:contact.contactPUUID]];
    
    [_setFieldsRequestBuilder setUserId:[self puuidBuilder:contact.userPUUID]];
    
    
    NSArray* arrayFields = [self fieldsFromContact:contact];
    
    
    if(arrayFields.count > 0)
        
    {
        [_setFieldsRequestBuilder setFieldsArray:arrayFields];
        RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
        
        [_requestBuilder setId:[self puuidBuilder:transportPUUIDForGetParams]];
        
        [_requestBuilder setType:CallTypeContactSetFields];
        [_requestBuilder setRequest:[_setFieldsRequestBuilder build].data];
        
        Request * query = [_requestBuilder build];
        [_socketClient sendData:[query data]];
        
    }
}

- (void) parseSetFields:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        
        SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
        
        NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
        
        if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring]) {
            
            NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
            
            if([[params allKeys] containsObject:@"fieldsForCreatOneContact"])
            {
                [_delegateDictionary removeObjectForKey:transportPUUIDstring];
                
                id<SCPBManagerDelegate>delegate = [params objectForKey:@"delegate"];
                
                if (delegate && [delegate respondsToSelector:@selector(parsedType:data:)])
                    
                {
                    [delegate parsedType:SCPBManagerParceTypeContactSetFields data:nil];
                }
                
                
            } else {
                
                SCAddressbookManager* addressbookModel = _settings.modelAddressbook;
                
                NSMutableArray* addressbook = [params objectForKey:@"addressbook"];
                
                NSMutableArray* contactForCreat = [params objectForKey:@"contactForCreat"];
                
                int indexCreationObject = [[params objectForKey:@"indexCreationObject"]integerValue]-1;
                
                SCContactItem* contact = contactForCreat[indexCreationObject];
                
                for(SCAddressbookContact* addressbookContact in addressbook){
                    
                    for (NSString* phone in addressbookContact.contactPhoneNumbers) {
                        
                        if ([contact.contactPhone isEqualToString:phone]) {
                            
                            addressbookContact.checked = YES;
                        }
                    }
                    
                    for ( NSString* email in addressbookContact.contactEmail ) {
                        
                        if ([contact.contactEmail isEqualToString:email]) {
                            
                            addressbookContact.checked = YES;
                            
                        }
                        
                    }
                    
                }
                
                [addressbookModel saveAddressbook:addressbook];
                
                
                [self groupCreatesContactWithParams:params];
                
                
            }
            
            
        }
        
        //     [self callDelegate:SCPBManagerParceTypeContactSetFields withData:nil];
        
    }
    else
    {
        FTLog(@"Error");
    }
}


/* =======================================================================================
 CONTACT_REMOVE_FIELD
 */
-(void)removeFieldRequest:(SCContactItem*) contact
{
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeContactRemoveField];
    
    RemoveFieldRequestBuilder* _removeFieldRequestBuilder = [[RemoveFieldRequestBuilder alloc] init];
    
    [_removeFieldRequestBuilder setContactId:[self puuidBuilder:contact.contactPUUID]];
    
    
    [_requestBuilder setRequest:[_removeFieldRequestBuilder build].data];
    
    Request * query = [_requestBuilder build];
    [_socketClient sendData:[query data]];
}

- (void) parseRemoveField:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
    }
    else
    {
        FTLog(@"Error");
    }
}


/* =======================================================================================
 CONTACTS_GET
 */
-(void) getAllContactsRequestWithParams:(NSMutableDictionary*) params
{
    RequestBuilder * _requestBuilder = [[RequestBuilder alloc]init];
    [_requestBuilder setType:CallTypeContactsGet];
    
    if (params)
    {
        SCPUUID *transportPUUID = [[SCPUUID alloc] initWithRandom];
        [_requestBuilder setId:[self puuidBuilder:transportPUUID]];
        [_delegateDictionary setObject:params forKey:[transportPUUID scpuuidToString]];
    }
    
    Request * query = [_requestBuilder build];
    [_socketClient sendData:[query data]];
}

- (void) parseGetAllContacts:(id)data
{
    Response * response = data;
    if(response.status.success)
    {
        NSArray* contacts = [GetAllContactsResponse parseFromData:response.response].contacts;
        NSMutableArray* contactsItem = [NSMutableArray array];
        
        for (Contact* contact in contacts )
        {
            /*
            SCContactItem* contactItem =[_settings.modelUsers getUserByPUUID:[self scpuuidBuilder:contact.userId]];
            contactItem.userPUUID = [self scpuuidBuilder:contact.userId];
            */
            SCContactItem* contactItem = [[SCContactItem alloc]initWithSettings:_settings];
            contactItem.contactPUUID = [self scpuuidBuilder:contact.id];
            contactItem.userPUUID = [self scpuuidBuilder:contact.userId];
            contactItem.user = [_settings.modelUsers getUserByPUUID:contactItem.userPUUID];
            [self contactFromFields:contactItem fields:contact.fields];
            
            NSString* fullName = [NSString new];
            
            if (!contactItem.contactLastName.length==0 || !contactItem.contactFirstName.length==0) {
                if (contactItem.contactLastName.length > 0) {
                    fullName = [NSString stringWithFormat:@"%@ ",contactItem.contactLastName];
                }
                if (contactItem.contactFirstName.length > 0) {
                    fullName = [fullName stringByAppendingString:contactItem.contactFirstName];
                }
                contactItem.fullName = fullName;
            } else if (!contactItem.contactNikName.length == 0) {
                contactItem.fullName = contactItem.contactNikName;
            }
            
            [contactsItem addObject:contactItem];
            
        }
        
        NSDictionary *paramsSubscribe = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         self, @"delegate",
                                         [[SCPUUID alloc] initWithRandom], @"UUID",
                                         contactsItem,@"userArray",
                                         nil];

        [self subscribeRequest:paramsSubscribe];
        
        
        SCPUUID *transportPUUID = [self scpuuidBuilder:response.id];
        NSString *transportPUUIDstring = [transportPUUID scpuuidToString];
        if ([[_delegateDictionary allKeys] containsObject:transportPUUIDstring])
        {
            NSMutableDictionary* params = [[_delegateDictionary objectForKey:transportPUUIDstring] mutableCopy];
            [params setObject:contactsItem forKey:@"contacts"];
            [self searchUsersByAuthRequest:params];
        }
        
        if([_settings.modelContacts respondsToSelector:@selector(parsedType:data:)])
        {
            [_settings.modelContacts parsedType:SCPBManagerParceTypeGetAllContacts data:contactsItem];
        }
    }
    else
    {
        FTLog(@"Error");
    }
}



@end
