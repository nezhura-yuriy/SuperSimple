//
//  SCLPullToAddNewBehaviour.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCLTableView.h"
#import "SCSettings.h"

@class SCLTableView;

@interface SCLPullToAddNewBehaviour : NSObject <UIScrollViewDelegate>

@property (nonatomic, strong) SCSettings *settings;

- (instancetype) initWithTableView:(SCLTableView *) tableView NS_DESIGNATED_INITIALIZER;

@end
