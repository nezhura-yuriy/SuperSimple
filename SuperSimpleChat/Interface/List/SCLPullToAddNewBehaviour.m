//
//  SCLPullToAddNewBehaviour.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCLPullToAddNewBehaviour.h"
#import "SCLTableViewCell.h"
#import "SCLTableView.h"

@implementation SCLPullToAddNewBehaviour
{
    SCLTableView *_tableView;
    BOOL _pullDownInProgress;
    SCLTableViewCell *_placeholderCell;
}

- (instancetype)initWithTableView:(SCLTableView *)tableView
{
    self = [super init];
    if (self) {
        _placeholderCell = [[SCLTableViewCell alloc] initWithType:SCLTableViewCellTypeAddingCell];
        _tableView = tableView;
        tableView.delegate = self;
    }
    return self;
}

- (void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    _placeholderCell.settings = _settings;
}

///MARK: UIScrollViewDelegate methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    // this behaviour starts when a user pulls down while at the top of the table
    _pullDownInProgress = scrollView.contentOffset.y <= 0.0f;
    
    if (_pullDownInProgress)
        {
        // add our placeholder
        [_tableView insertSubview:_placeholderCell atIndex:0];
        }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_pullDownInProgress && _tableView.scrollView.contentOffset.y <= 0.0f)
        {
        // maintain the location of the placeholder
            _placeholderCell.frame = CGRectMake(0,
                                                - _tableView.scrollView.contentOffset.y - kRowHeight,
                                                _tableView.frame.size.width,
                                                kRowHeight);
        
            _placeholderCell.label.text = -_tableView.scrollView.contentOffset.y > kRowHeight ?
        @"Release to Add Item" : @"Pull to Add Item";
        
            _placeholderCell.alpha = MIN(1.0f, - _tableView.scrollView.contentOffset.y / kRowHeight);
        }
    else
        {
            _pullDownInProgress = false;
        }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    // check whether the user pulled down far enough
    if (_pullDownInProgress && - _tableView.scrollView.contentOffset.y > kRowHeight)
        {
            [_tableView.datasource itemAdded]; //Add Cell via ViewController
        }
    
    _pullDownInProgress = false;
    [_placeholderCell removeFromSuperview]; //remove _placeholderCell
}


@end
