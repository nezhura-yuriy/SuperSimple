//
//  SCCountrysTableViewController.m
//  SuperSimpleChat
//
//  Created by yury on 12.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCCountrysTableViewController.h"
#import "SCCountry.h"
#import "SCContactItem.h"
#import "SCCoutrySection.h"

static NSString *kCountryNotSelected = @" Country not selected";

@implementation SCCountrysTableViewController
{
    NSArray *_countryArray;
    NSString *_countryCurrent;
    NSIndexPath *_indexPathCountryCurrent;
    
    NSOperation* currentOperation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    _countryArray = [SCCountry countryList];
    
    if (_settings.modelProfile.contactItem.contactAddress.country) {
        _countryCurrent = _settings.modelProfile.contactItem.contactAddress.country;
    } else {
        _countryCurrent = kCountryNotSelected;
    }
    
    [self generateSectionsInBackgroundArrayContacts:_countryArray withFilter:nil];
    
    self.tableView.sectionIndexColor = SKCOLOR_TableCountrySectionIndexFont;
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

//MARK:Setups
- (void) setupNavigation
{
    //Nav Title
    self.title = @"Country";
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBar.barTintColor = SKCOLOR_NavBarBg;
    self.navigationController.navigationBar.translucent = NO;
    
    //Back button
    UIImage *backButtonImage = [_settings.skinManager getImageForKeyX:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

-(void) generateSectionsInBackgroundArrayContacts : (NSArray*) array withFilter:(NSString*) filterString {
    
    [currentOperation cancel];
    
    __weak SCCountrysTableViewController* weakSelf = self;
    
    currentOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSMutableArray* sectionsArray = [weakSelf generateSectionsFromArray:array withFilter:filterString];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.sectionsArray = sectionsArray;
            [self.tableView reloadData];
            
            currentOperation = nil;
        });
    }];
    [currentOperation start];
}

-(NSMutableArray*) generateSectionsFromArray:(NSArray*) array withFilter:(NSString*) filterString
{
    NSMutableArray* sectionsArray = [[NSMutableArray alloc]init];
    NSString* currentLetter = nil;
    
    for (NSString* countryTitle in array) {
        
        if ([filterString length] > 0 && [countryTitle rangeOfString:filterString options:NSCaseInsensitiveSearch].location== NSNotFound) {
            continue;
        }
        
        NSString* firstLetter = [countryTitle substringToIndex:1];
        
        SCCoutrySection* sectionX = nil;
        
        if (![currentLetter isEqualToString:firstLetter]) {
            
            sectionX = [[SCCoutrySection alloc]init];
            sectionX.sectionName = firstLetter;
            sectionX.itemsArray = [NSMutableArray array];
            currentLetter = firstLetter;
            [sectionsArray addObject:sectionX];
            
        }else{
            sectionX = [sectionsArray lastObject];
        }
        [sectionX.itemsArray addObject:countryTitle];
    }
    return sectionsArray;
}

#pragma mark - Table view data source
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    SCCoutrySection* sections = self.sectionsArray[section];
    return sections.itemsArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sectionsArray[section] sectionName];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:FT_FontNameBold size:header.textLabel.font.pointSize]];
    header.textLabel.textColor = SKCOLOR_TableCountryHeaderFont;
    header.tintColor = SKCOLOR_TableCountryHeaderBg;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 35.0;
    }
    return 22;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    SCCoutrySection* sections = self.sectionsArray[indexPath.section];
    NSString *countryTitle = sections.itemsArray[indexPath.row];
    cell.textLabel.text = countryTitle;
    cell.textLabel.font = [UIFont fontWithName:FT_FontNameLight size:cell.textLabel.font.pointSize];
    cell.textLabel.textColor = SKCOLOR_TableCountryCellText;
        
    if ([countryTitle isEqualToString:_countryCurrent]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray* array = [NSMutableArray array];
    
    for(SCCoutrySection* section in self.sectionsArray){
        [array addObject:section.sectionName];
    }
    return array;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    NSString *selectedCountry = cell.textLabel.text;
    
    if (![selectedCountry isEqualToString:_countryCurrent]) {
        
        if ([selectedCountry isEqualToString:kCountryNotSelected])
        {
            [_settings.modelProfile removeProfileField:SCFieldTypeCountry];
            [_delegate changeCountry:nil];
            
        } else {
            [_settings.modelProfile setField:SCFieldTypeCountry forValue:selectedCountry];
            [_delegate changeCountry:selectedCountry];
        }
    }
    [self backAction];
}

//MARK: Actions
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
