//
//  SCChatListView.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatListView.h"
#import "SCSettings.h"
#import "SCButtonItemText.h"
#import "SCNotificationListKeys.h"

#define VIEW_HEIGHT 210
static const CGFloat kFullHeightView = 210;
static const CGFloat kButtonHeight = 41;
static const CGFloat kMargin = 0;
static const NSInteger kColumnCountOnOneScreen = 2;
static const CGFloat kMarginWarningView = 25;



@implementation SCChatListView
{
    void (^afterSelect) (SCPopChatMenuViewItem selectItem);

    SCSettings *_settings;
    SCList *_list;
    NSArray *_listItems;
    
    CGFloat _heightViewKeyboard; //keyboar + pageControll
    
    CGFloat _buttonWidth;
    
    UIPageControl *_pageControll;
    UIScrollView *_scrollViewKeyboard;
    
    UIButton *_backspaceButton;
    UIScrollView *_listsSrcollView;
    
    SCWarningView *_warningView;
    
    SEL _touchItemButtonSelector;
}

+(CGSize) sizeView
{
    return CGSizeMake(SZ_SCREEN_WIDTH, kFullHeightView);
}

//MARK: Inits
-(instancetype) init
{
    self = [super init];
    if(self)
    {
        [self _init];
        [self setupUI];
    }
    return self;
}
-(instancetype) initWithCompletionblock:(void (^)(SCPopChatMenuViewItem selectItem))completionBlock
{
    self = [super init];
    if(self)
    {
        afterSelect = [completionBlock copy];
        [self _init];
        [self setupUI];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
}

-(void) _init
{
//    _settings = [SCSettings sharedSettings];
    
    _heightViewKeyboard = kFullHeightView;
    
    _pageControll = [UIPageControl new];
    
    _scrollViewKeyboard = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _scrollViewKeyboard.showsHorizontalScrollIndicator = NO;
    _scrollViewKeyboard.showsVerticalScrollIndicator = NO;
    _scrollViewKeyboard.pagingEnabled = YES;
    _scrollViewKeyboard.delegate = self;
    
    _listItems = [NSArray new];
    [self subscribeNotifications];
    
    if (_list == nil) {
        _list = [[SCList alloc] initAsFavorite];
        [_list getListItemsFromServer];
    }
    
    if ([self respondsToSelector:@selector(touchItemButton:)]) {
        _touchItemButtonSelector = @selector(touchItemButton:);
    }
}

-(void) dealloc
{
    [self unSubscribeNotifications];
}

-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    
    [self _init];
    [self setupUI];

}

-(void) setAfterSelect:(void(^)(SCPopChatMenuViewItem selectItem)) completionBlock
{
    afterSelect = [completionBlock copy];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
}

//MARK: Notifications
- (void) subscribeNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateListItems) name:kNotificationItemsUpdate object:nil];
}

- (void) unSubscribeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//MARK: Setup
- (void) setupUI
{
    self.backgroundColor = SKCOLOR_ChatBarBg;
    
    //PageControl
    _pageControll.pageIndicatorTintColor = SKCOLOR_ChatInputTextFieldBorder;
    _pageControll.currentPageIndicatorTintColor = SKCOLOR_NavBarBg;
    _pageControll.hidesForSinglePage = YES;
    
    //WarningView
    _warningView = [[SCWarningView alloc] initWithFrame:CGRectMake(kMarginWarningView, kMarginWarningView,
                                                                   SZ_SCREEN_WIDTH - 2 * kMarginWarningView,
                                                                   self.frame.size.height - 2 * kMarginWarningView)
                                           withSettings:_settings];

    _warningView.delegate = self;
}

//Setup Views
- (NSInteger) rowCount {
    return (_heightViewKeyboard - kMargin) / (kButtonHeight + kMargin);
}

- (NSInteger) objectsCountMaxInOneScreen {
    return [self rowCount] * kColumnCountOnOneScreen;
}

- (NSInteger) objectsCountInLastPage {
    return [self moduloDividend:_listItems.count divider:[self objectsCountMaxInOneScreen]];
}

- (NSInteger) pagesCount {
    return (_listItems.count - [self objectsCountInLastPage]) / [self objectsCountMaxInOneScreen] + 1;
}

- (NSInteger) maxObjectInLastRow {
    return [self moduloDividend:[self objectsCountInLastPage] divider:kColumnCountOnOneScreen];
}

- (NSInteger) moduloDividend:(NSInteger) dividend divider:(NSInteger) divider {
    
    NSInteger modulo;
    
    if ((divider == 1) || (dividend == divider)) {
        modulo = dividend;
    } else {
        modulo = dividend % divider;
    }
    return modulo;
}

- (void) setupListView {
    
    if (_listItems.count != 0) {
        
        //DEBUG
        if (_warningView !=nil) {
            [_warningView removeFromSuperview];
        }
        
        //Count button in one screen (page)
        NSInteger rowCount = [self rowCount];
        NSInteger objectsCountMaxInOneScreen = [self objectsCountMaxInOneScreen];
        
        if (objectsCountMaxInOneScreen > _listItems.count) {
            objectsCountMaxInOneScreen = _listItems.count;
        }
        
        NSInteger maxRowsInScreen = rowCount;
        NSInteger maxObjectsInRow = kColumnCountOnOneScreen;
        
        //Count of page for scrollview and count of object in last screen (page)
        NSInteger objectsCountInLastPage = [self objectsCountInLastPage];
        
        NSInteger pagesCount = 1;
        NSInteger maxRowsInLastScreen = 0;
        NSInteger maxObjectInLastRow = 0;
        
        if (objectsCountInLastPage != 0) {
            
            pagesCount = [self pagesCount];
            maxObjectInLastRow = [self maxObjectInLastRow];
            
            if (maxObjectInLastRow != 0) {
                maxRowsInLastScreen = (objectsCountInLastPage - maxObjectInLastRow) / kColumnCountOnOneScreen + 1;
                
            } else {
                maxRowsInLastScreen = objectsCountInLastPage / kColumnCountOnOneScreen;
            }
            
        } else {
            pagesCount = _listItems.count / objectsCountMaxInOneScreen;
        }
        
        //PageControll
        if (_pageControll != nil) {
            [_pageControll removeFromSuperview];
        }
        
        CGSize pageControllSize = [_pageControll sizeForNumberOfPages:pagesCount];
        CGFloat heightPageControll = pageControllSize.height / 3;
        
        _pageControll.frame = CGRectMake(self.frame.size.width/2 - pageControllSize.width/2,
                                         - 2,
                                         pageControllSize.width,
                                         heightPageControll);
        
        _pageControll.numberOfPages = pagesCount;
        _pageControll.currentPage = 0;
        
        [self addSubview:_pageControll];
        
        //Create ScrollView
        CGFloat scrollViewKeyboardY = _pageControll.frame.origin.y + _pageControll.frame.size.height - 2;
        
        _scrollViewKeyboard.frame = CGRectMake(0,
                                               scrollViewKeyboardY,
                                               SZ_SCREEN_WIDTH,
                                               _heightViewKeyboard - scrollViewKeyboardY);
        
        _scrollViewKeyboard.contentSize = CGSizeMake(_scrollViewKeyboard.frame.size.width * pagesCount,
                                                     _scrollViewKeyboard.frame.size.height);
        
        //Create Buttons
        _buttonWidth = (SZ_SCREEN_WIDTH - kMargin)/kColumnCountOnOneScreen - kMargin;
        
        CGPoint buttonPoint;
        CGRect buttonFrame;
        
        NSInteger maxRowsInScreenForCurrentPage = maxRowsInScreen;
        NSInteger maxObjectsInRowForCurrentPage = maxObjectsInRow;
        NSInteger index = 0;
        
        for (int currentPageNumber = 0; currentPageNumber < pagesCount; currentPageNumber++) {
            
            if (currentPageNumber == pagesCount - 1) {
                maxRowsInScreenForCurrentPage = maxRowsInLastScreen;
            }

            for (int currentRow = 0; currentRow < maxRowsInScreenForCurrentPage; currentRow++) {
                
                if ((currentPageNumber == pagesCount - 1) && (currentRow == maxRowsInScreenForCurrentPage - 1)) {
                    
                    if (maxObjectInLastRow != 0) {
                        maxObjectsInRowForCurrentPage = maxObjectInLastRow;
                    }
                }
                
                for (int currentColumn = 0; currentColumn < maxObjectsInRowForCurrentPage; currentColumn++) {
                    
                    buttonPoint.x = kMargin +
                        (_buttonWidth + kMargin) * currentColumn +
                        currentPageNumber * _scrollViewKeyboard.frame.size.width;
                    
                    buttonPoint.y = kMargin +
                        (kButtonHeight + kMargin) * currentRow;
                    
                    buttonFrame = CGRectMake(buttonPoint.x, buttonPoint.y,
                                             _buttonWidth, kButtonHeight);
                    
                    SCButtonItemText *button = [[SCButtonItemText alloc] initWithFrame:buttonFrame forList:_list itemIndex:index];
                    [button addTarget:self action:_touchItemButtonSelector forControlEvents:UIControlEventTouchUpInside];
                    button.settings = _settings;
                    [_scrollViewKeyboard addSubview:button];
                    
                    index++;
                }
            }
        }
        [self addSubview:_scrollViewKeyboard];
        
    } else {
        [self addSubview:_warningView];
    }
}

//MARK: Scrolling

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.frame.size.width;
    _pageControll.currentPage = scrollView.contentOffset.x / pageWidth;
}

//MARK: IBActions
-(void) touchItemButton:(UIButton*)sender
{
    FTLog(@"%@", NSStringFromSelector(_cmd));
    
    //Наследство из SC, пересмотреть в работе
    SCListItem *item = _list.items[sender.tag];
//    item.contentType = SCDataTypeText;//DEBUG
    
    if ([_delegate respondsToSelector:@selector(sendFromListItemData:)]) {
        [_delegate sendFromListItemData:[item toMessageArray]];
    }
}

- (void) updateListItems
{
    FTLog(@"updateListItems neeeeeed");
    _listItems = [_list.items copy];
    [self setupListView];
}


-(CGSize) sizeView
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, VIEW_HEIGHT);
}

//MARK: SCWarningViewProtocol
- (void) gotoListController
{
    //TODO: НЕОБХОДИМО ПЕРЕХОДИТЬ В LIST VIEW для создания новых ITEM
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SHOW_LIST_PAGE object:nil];

}

@end
