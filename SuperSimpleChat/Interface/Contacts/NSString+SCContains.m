//
//  NSString+SCContains.m
//  SuperSimpleChat
//
//  Created by Линник Александр on 07.08.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "NSString+SCContains.h"

@implementation NSString (SCContains)

- (BOOL)myContainsString:(NSString*)other {
    
    NSRange range = [self rangeOfString:other];
    return range.length != 0;
}

@end
