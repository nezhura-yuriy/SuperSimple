//
//  SCSkinColors.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 08.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#ifndef SmartChat_SCSkinColors_h
#define SmartChat_SCSkinColors_h


//Right
#define SKCOLOR_ColorTestRed [_settings.skinManager getColorForKeyX:@"ColorTestRed"]

#define SKCOLOR_ViewBg [_settings.skinManager getColorForKeyX:@"ViewBackgorund"]
#define SKCOLOR_ViewNotAvatarBg [_settings.skinManager getColorForKeyX:@"ViewNotAvatarBackground"]
#define SKCOLOR_ViewHelpFont [_settings.skinManager getColorForKeyX:@"ViewHelpFont"]

//From Alexander V
#define SKCOLOR_SegmentControlV1FontActive [_settings.skinManager getColorForKeyX:@"SegmentControlV1FontActive"]
#define SKCOLOR_SegmentControlV1FontInactive [_settings.skinManager getColorForKeyX:@"SegmentControlV1FontInactive"]
#define SKCOLOR_SegmentControlV1BgActive [_settings.skinManager getColorForKeyX:@"SegmentControlV1BackgroundActive"]
#define SKCOLOR_SegmentControlV1BgInactive [_settings.skinManager getColorForKeyX:@"SegmentControlV1BackgroundInactive"]
#define SKCOLOR_SegmentControlV1BgViewUnder [_settings.skinManager getColorForKeyX:@"SegmentControlV1BackgroundViewUnder"]

//From Yury R
#define SKCOLOR_SegmentControlV2FontActive [_settings.skinManager getColorForKeyX:@"SegmentControlV2FontActive"]
#define SKCOLOR_SegmentControlV2FontInactive [_settings.skinManager getColorForKeyX:@"SegmentControlV2FontInactive"]
#define SKCOLOR_SegmentControlV2BgActive [_settings.skinManager getColorForKeyX:@"SegmentControlV2BackgroundActive"]
#define SKCOLOR_SegmentControlV2BgInactive [_settings.skinManager getColorForKeyX:@"SegmentControlV2BackgroundInactive"]

#define SKCOLOR_TableTextFieldPlchldrFont [_settings.skinManager getColorForKeyX:@"TableTextFieldPlaceholderFont"]

#define SKCOLOR_NavBarBg [_settings.skinManager getColorForKeyX:@"NavigationBarBackground"]
#define SKCOLOR_NavBarTitleFont [_settings.skinManager getColorForKeyX:@"NavigationBarTitleFont"]
#define SKCOLOR_TabBarBg [_settings.skinManager getColorForKeyX:@"TabBarBackground"]
#define SKCOLOR_TabBarFontActive [_settings.skinManager getColorForKeyX:@"TabBarFontActive"]
#define SKCOLOR_TabBarFontInactive [_settings.skinManager getColorForKeyX:@"TabBarFontInactive"]

#define SKCOLOR_TableCountrySectionIndexFont [_settings.skinManager getColorForKeyX:@"TableCountrySectionIndexFont"]
#define SKCOLOR_TableCountryHeaderFont [_settings.skinManager getColorForKeyX:@"TableContryHeaderFont"]
#define SKCOLOR_TableCountryHeaderBg [_settings.skinManager getColorForKeyX:@"TableCountryHeaderBackground"]
#define SKCOLOR_TableCountryCellText [_settings.skinManager getColorForKeyX:@"TableCountryCellText"]
#define SKCOLOR_TableCountrySearchPlchldrBg [_settings.skinManager getColorForKeyX:@"TableCountrySearchPlaceholderBackground"]
#define SKCOLOR_TableCountrySearchPlchldrFont [_settings.skinManager getColorForKeyX:@"TableCountrySearchPlaceholderFont"]
#define SKCOLOR_TableCountrySearchViewBg [_settings.skinManager getColorForKeyX:@"TableCountrySearchViewBackground"]

#define SKCOLOR_TableCellFont [_settings.skinManager getColorForKeyX:@"TableCellFont"]
#define SKCOLOR_TableCellBg [_settings.skinManager getColorForKeyX:@"TableCellBackground"]
#define SKCOLOR_TableSeparator [_settings.skinManager getColorForKeyX:@"TableSeparator"]

#define SKCOLOR_SwitchOn [_settings.skinManager getColorForKeyX:@"SwitchOn"]
#define SKCOLOR_SwitchOff [_settings.skinManager getColorForKeyX:@"SwitchOff"]
#define SKCOLOR_SwitchThumb [_settings.skinManager getColorForKeyX:@"SwitchThumb"]

#define SKCOLOR_ButtonRemoveBg [_settings.skinManager getColorForKeyX:@"ButtonRemoveBackground"]
#define SKCOLOR_ButtonFacebookBg [_settings.skinManager getColorForKeyX:@"ButtonFacebookBackground"]
#define SKCOLOR_ButtonGreenBg [_settings.skinManager getColorForKeyX:@"ButtonGreenBackground"]

#define SKCOLOR_FontRegular [_settings.skinManager getColorForKeyX:@"FontRegular"]
#define SKCOLOR_FontTableFooter [_settings.skinManager getColorForKeyX:@"FontTableFooter"]

#define SKCOLOR_ChatListBadge [_settings.skinManager getColorForKeyX:@"ChatListBadge"]
#define SKCOLOR_ChatListRedLabel [_settings.skinManager getColorForKeyX:@"ChatListRedLabel"]
#define SKCOLOR_ChatListNickName [_settings.skinManager getColorForKeyX:@"ChatListNickName"]
#define SKCOLOR_ChatListTagText [_settings.skinManager getColorForKeyX:@"ChatListTagText"]
#define SKCOLOR_ChatListMessageText [_settings.skinManager getColorForKeyX:@"ChatListMessageText"]

#define SKCOLOR_ChatMessageOutgoingBg [_settings.skinManager getColorForKeyX:@"ChatMessageOutgoingBackground"]
#define SKCOLOR_ChatMessageOutgoingText [_settings.skinManager getColorForKeyX:@"ChatMessageOutgoingText"]
#define SKCOLOR_ChatMessageIncomingBg [_settings.skinManager getColorForKeyX:@"ChatMessageIncomingBackground"]
#define SKCOLOR_ChatMessageIncomingText [_settings.skinManager getColorForKeyX:@"ChatMessageIncomingText"]
#define SKCOLOR_ChatMessageDateText [_settings.skinManager getColorForKeyX:@"ChatMessageDateText"]
#define SKCOLOR_ChatGoupTitle [_settings.skinManager getColorForKeyX:@"ChatGoupTitle"]

#define SKCOLOR_ChatBarBg [_settings.skinManager getColorForKeyX:@"ChatBarBackground"]
#define SKCOLOR_ChatBarBorder [_settings.skinManager getColorForKeyX:@"ChatBarBorder"]
#define SKCOLOR_ChatInputTextFieldBg [_settings.skinManager getColorForKeyX:@"ChatInputTextFieldBackground"]
#define SKCOLOR_ChatInputTextFieldBorder [_settings.skinManager getColorForKeyX:@"ChatInputTextFieldBorder"]
#define SKCOLOR_ChatButtonSendText [_settings.skinManager getColorForKeyX:@"ChatButtonSendText"]

//=========================== END ====================

//ВСЁ ВНИЗУ НАДО УДАЛИТЬ

//Skin Color (name key see in skinData.plist)

//#define SC_BackgroundColor @"BackgroundColor"
//#define SC_BarColor @"BarColor"
//#define SC_BarDarkColor @"BarDarkColor"
//#define SC_BarTintColor @"BarTintColor"
//#define SC_MessageBgMy @"MessageBgMy"
//#define SC_MessageBgCompanion @"MessageBgCompanion"
//#define SC_MessageTextMy @"MessageTextMy"
//#define SC_MessageTextCompanion @"MessageTextCompanion"
//#define SC_ButtonColor @"ButtonColor"

#define SKCOLOR_Background [_settings.skinManager getColorForKey:@"BackgroundColor"]
#define SKCOLOR_BarColor [_settings.skinManager getColorForKey:@"BarColor"]
#define SKCOLOR_BarDarkColor [_settings.skinManager getColorForKey:@"BarDarkColor"]
#define SKCOLOR_BarTintColor [_settings.skinManager getColorForKey:@"BarTintColor"]
#define SKCOLOR_MessageBgMy [_settings.skinManager getColorForKey:@"MessageBgMy"]
#define SKCOLOR_MessageBgCompanion [_settings.skinManager getColorForKey:@"MessageBgCompanion"]
#define SKCOLOR_MessageTextMy [_settings.skinManager getColorForKey:@"MessageTextMy"]
#define SKCOLOR_MessageTextCompanion [_settings.skinManager getColorForKey:@"MessageTextCompanion"]
#define SKCOLOR_ButtonColor [_settings.skinManager getColorForKey:@"ButtonColor"]



//Skin Color (name key see in skinData.plist)
#define CL_BaseDarkColor [UIColor colorWithRed:0.5529 green:0.7098 blue:0.8118 alpha:1]//[_settings.skinManager getColorForKey:@"BaseDark"]
#define CL_WhiteCGColor [UIColor colorWithRed:1 green:1 blue:1 alpha:1]//[_settings.skinManager getColorForKey:@"White"]
#define CL_NavigationBarColor [UIColor colorWithRed:0.5529 green:0.7098 blue:0.8118 alpha:1]//[_settings.skinManager getColorForKey:@"NavigationBar"]
#define CL_NavigationBarTintColor [UIColor colorWithRed:0.4745 green:0.6588 blue:0.7647 alpha:1]//[_settings.skinManager getColorForKey:@"NavigationBarTintColor"]
#define CL_TableBackgroundColor [UIColor colorWithRed:0.8824 green:0.9255 blue:0.9451 alpha:1]//[_settings.skinManager getColorForKey:@"TableBackground"]
#define CL_TableCellDarkColor [UIColor colorWithRed:0.8902 green:0.9451 blue:0.9608 alpha:1]//[_settings.skinManager getColorForKey:@"TableCellDark"]
#define CL_TableCellLightColor [UIColor colorWithRed:0.9765 green:0.9922 blue:1 alpha:1]//[_settings.skinManager getColorForKey:@"TableCellLight"]
#define CL_TableButtonUnderCellColor [UIColor colorWithRed:0.5294 green:0.7176 blue:0.8078 alpha:1]//[_settings.skinManager getColorForKey:@"TableButtonUnderCell"]
#define CL_TableCellMonoColor [UIColor colorWithRed:0.8902 green:0.9451 blue:0.9569 alpha:1]//[_settings.skinManager getColorForKey:@"TableCellMono"]
#define CL_TableCellSeparatorColor [UIColor colorWithRed:0.6588 green:0.7961 blue:0.8588 alpha:0.8]//[_settings.skinManager getColorForKey:@"TableCellSeparator"]
#define CL_TableHeader [UIColor colorWithRed:0.8902 green:0.9451 blue:0.9569 alpha:1]//[_settings.skinManager getColorForKey:@"TableHeader"]
#define CL_ViewBackgroundColor [UIColor colorWithRed:0.5529 green:0.7098 blue:0.8118 alpha:1]//[_settings.skinManager getColorForKey:@"ViewBackground"]
#define CL_ViewBackgroundLightColor [UIColor colorWithRed:0.8902 green:0.9451 blue:0.9569 alpha:1]//[_settings.skinManager getColorForKey:@"ViewBackgroundLight"]
#define CL_BackgroundBookmarkChatColor [UIColor colorWithRed:0.7176 green:0.8235 blue:0.8784 alpha:0.8]//[_settings.skinManager getColorForKey:@"BackgroundBookmarkChat"]
#define CL_BackgroundBookmarkContactsColor [UIColor colorWithRed:0.6588 green:0.7961 blue:0.8588 alpha:0.8]//[_settings.skinManager getColorForKey:@"BackgroundBookmarkContacts"]
#define CL_BackgroundBookmarkListColor [UIColor colorWithRed:0.5176 green:0.6745 blue:0.7529 alpha:0.8]//[_settings.skinManager getColorForKey:@"BackgroundBookmarkList"]
#define CL_ButtonAddGreenColor [UIColor colorWithRed:0.4901 green:0.7372 blue:0 alpha:1]//[_settings.skinManager getColorForKey:@"ButtonAddGreen"]
#define CL_ButtonDigitKeyboardColor [UIColor colorWithRed:0.7333 green:0.8667 blue:0.8863 alpha:1]//[_settings.skinManager getColorForKey:@"ButtonDigitKeyboard"]
#define CL_CircleGreenDigitColor [UIColor colorWithRed:0.7529 green:0.9059 blue:0.7882 alpha:1]//[_settings.skinManager getColorForKey:@"CircleGreenDigit"]
#define CL_CircleGreenCheckColor [UIColor colorWithRed:0.7529 green:0.9059 blue:0.7882 alpha:1]//[_settings.skinManager getColorForKey:@"CircleGreenCheck"]
#define CL_CircleRedColor [UIColor colorWithRed:0.8863 green:0.1882 blue:0.2549 alpha:1]//[_settings.skinManager getColorForKey:@"CircleRed"]
#define CL_FontRedColor [UIColor colorWithRed:0.8863 green:0.4471 blue:0.498 alpha:1]//[_settings.skinManager getColorForKey:@"FontRed"]
#define CL_FontWhiteColor [UIColor colorWithRed:1 green:1 blue:1 alpha:1]//[_settings.skinManager getColorForKey:@"FontWhite"]
#define CL_FontLightColor [UIColor colorWithRed:0.5255 green:0.7137 blue:0.8039 alpha:1]//[_settings.skinManager getColorForKey:@"FontLight"]
#define CL_FontDefaultColor [UIColor colorWithRed:0.5255 green:0.7137 blue:0.8039 alpha:1]//[_settings.skinManager getColorForKey:@"FontDefault"]
#define CL_PageControllCurrenPage [UIColor colorWithRed:0.7215 green:0.8235 blue:0.8901 alpha:1]//[_settings.skinManager getColorForKey:@"PageControllCurrenPage"]
#define CL_TabNotActiveColor [UIColor colorWithRed:0.9059 green:0.9412 blue:0.9412 alpha:1]//[_settings.skinManager getColorForKey:@"TabNotActive"]
#define CL_MessageBackgroundDarkColor [UIColor colorWithRed:0.5294 green:0.7176 blue:0.8078 alpha:1]//[_settings.skinManager getColorForKey:@"MessageBackgroundDark"]
#define CL_MessageBackgroundLightColor [UIColor colorWithRed:0.9059 green:0.9412 blue:0.9608 alpha:1]//[_settings.skinManager getColorForKey:@"MessageBackgroundLight"]


#endif
