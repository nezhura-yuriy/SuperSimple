//
//  SCMediaItem.h
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 06.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SCMessageBodyItem;


typedef enum : NSUInteger {
    SCMediaItemTypeImage,
    SCMediaItemTypeAudio,
    SCMediaItemTypeVideo,
    SCMediaItemTypeButton,
} SCMediaItemType;


@interface SCMediaItem : NSObject

@property(nonatomic,assign) SCMediaItemType type;
@property(nonatomic,strong) NSString* mediaPath;
@property(nonatomic,strong) SCMessageBodyItem* messageBody;

@end
