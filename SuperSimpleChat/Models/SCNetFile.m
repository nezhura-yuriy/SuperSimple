//
//  SCNetFile.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCNetFile.h"
#import "SCHTTPManager.h"
#import "SCSettings.h"
#import "SCImageViewWithIndicator.h"
#import "UIImageView+Letters.h"

#import "SCMessageBodyItemView.h"
#import "SCMessageBodyPhotoItemView.h"
#import "SCMessageBodySoundItemView.h"
#import "SCMessageBodyVideoItemView.h"
#import <UIKit/UIKit.h>

@implementation SCNetFile
{
    CGFloat _progressPercent;
    CGFloat _totalBytes;
    CGFloat _bytesProcessed;
    NSFileManager* _fileManager;
    SCNetFileOperation _curentOperation;
    
//    UIImageView* loadedImageView;
//    id loadedObject;
    void (^_completionBlock) (SCNetFileComletionType type, id data);
    NSInteger _tryLoads;
    NSTimer* _waitBeforeTry;
}

#pragma init
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self _init];
    }
    return self;
}

- (void)dealloc
{
    _completionBlock = nil;
}

-(void) _init
{
    _netPath = @"";
    _localPath = @"";
    _fileType = SCMediaFileTypeUnknown;
    _fileManager = [NSFileManager defaultManager];
    _tryLoads = 5;
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    if([[dictionary allKeys] containsObject:@"netPath"])
        _netPath = [dictionary objectForKey:@"netPath"];
    
    if([[dictionary allKeys] containsObject:@"localPath"])
        _localPath = [dictionary objectForKey:@"localPath"];
    
    if([[dictionary allKeys] containsObject:@"fileType"])
        _fileType = [[dictionary objectForKey:@"fileType"] integerValue];

    if([[dictionary allKeys] containsObject:@"additionInfo"])
        _additionInfo = [dictionary objectForKey:@"additionInfo"];
    
}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    if(_netPath)
        [outDict setObject:_netPath forKey:@"netPath"];
    if(_localPath)
        [outDict setObject:_localPath forKey:@"localPath"];
    [outDict setObject:[NSNumber numberWithInteger:_fileType] forKey:@"fileType"];
    if(_additionInfo)
        [outDict setObject:_additionInfo forKey:@"additionInfo"];
    return outDict;
}


#pragma mark sets
-(void) setImageNetPath:(NSString*) netPath
{
    _fileType = SCMediaFileTypePhoto;
    if(netPath)
    {
        [self setNetPath:netPath];
        _status = SCNetFileStatusNew;
    }
}

-(void) setNetPath:(NSString *)netPath
{
    if([self isNotEmptyPath])
    {
        if(![_netPath isEqualToString:netPath])
        {
            [self deletePath];
        }
    }
    
    if([netPath isEqualToString:@"00000000-0000-0000-0000-000000000000"])
    {
        _netPath = @"";
    }
    else
    {
        _netPath = netPath;
    }
}

-(BOOL) isNotEmptyPath
{
    if(_netPath && _netPath.length >0)
    {
        return YES;
    }
    return NO;
}

-(void) deletePath
{
    if( _fileType == SCMediaFileTypePhoto )
    {
        NSArray *dirContents = [_fileManager contentsOfDirectoryAtPath:_settings.caheImagePath error:nil];
        NSPredicate *fltr = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"self ENDSWITH '%@.png'",_netPath]];
        NSArray *filteredFiles = [dirContents filteredArrayUsingPredicate:fltr];
        for(NSString* delFileName in filteredFiles)
        {
            NSError* error = nil;
            NSString* delFilePath = [_settings.caheImagePath stringByAppendingPathComponent:delFileName];
            FTLog(@"del cached file %@",delFilePath);
            [_fileManager removeItemAtPath:delFilePath error:&error];
        }
        
        dirContents = [_fileManager contentsOfDirectoryAtPath:_settings.caheImagePath error:nil];
        fltr = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"self ENDSWITH '%@.png'",_netPath]];
        filteredFiles = [dirContents filteredArrayUsingPredicate:fltr];
        FTLog(@"verify %@",filteredFiles);
        
    }
    else if (_fileType == SCMediaFileTypeVideo )
    {
        NSArray *dirContents = [_fileManager contentsOfDirectoryAtPath:_settings.caheImagePath error:nil];
        NSArray* filtArray = [[NSArray alloc] initWithObjects:@"self ENDSWITH '%%@@.png'",@"self ENDSWITH '%%@@.png'", nil];
        for(NSString* predirate in filtArray)
        {
            NSPredicate *fltr = [NSPredicate predicateWithFormat:[NSString stringWithFormat:predirate,_netPath]];
            NSArray *filteredFiles = [dirContents filteredArrayUsingPredicate:fltr];
            for(NSString* delFileName in filteredFiles)
            {
                NSError* error = nil;
                NSString* delFilePath = [_settings.caheImagePath stringByAppendingPathComponent:delFileName];
                FTLog(@"del cached file %@",delFilePath);
                [_fileManager removeItemAtPath:delFilePath error:&error];
            }
        }
        
        dirContents = [_fileManager contentsOfDirectoryAtPath:_settings.caheImagePath error:nil];
        NSPredicate *fltr = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"self ENDSWITH '%@.png'",_netPath]];
        NSArray* filteredFiles = [dirContents filteredArrayUsingPredicate:fltr];
        FTLog(@"verify %@",filteredFiles);
    }
    else if (_fileType == SCMediaFileTypeAudio )
    {
        NSArray *dirContents = [_fileManager contentsOfDirectoryAtPath:[_settings.caheImagePath stringByAppendingPathComponent:_netPath] error:nil];
        NSPredicate *fltr = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"self ENDSWITH '%@.png'",_netPath]];
        NSArray *filteredFiles = [dirContents filteredArrayUsingPredicate:fltr];
        for(NSString* delFileName in filteredFiles)
        {
            NSString* delFilePath = [_settings.caheImagePath stringByAppendingPathComponent:delFileName];
            FTLog(@"del cached file %@",delFilePath);
        }
    }
    
    // удаляем оригинал
    if(_localPath.length > 0)
    {
        NSString* delFileFullPath = [self _makeLocalPath];
        if([_fileManager fileExistsAtPath:delFileFullPath])
        {
            [_fileManager removeItemAtPath:delFileFullPath error:nil];
        }
    }
    _localPath = nil;
    _netPath = nil;
    _fileType = SCMediaFileTypeUnknown;
}

-(void) clearCompletion
{
    @synchronized(self)
    {
        _completionBlock = nil;
    }
}

-(NSString*) _makeLocalPath
{
    return [self _makeLocalPathByType:_fileType];
}

-(NSString*) _makeLocalPathByType:(SCMediaFileType) atType
{
    switch (atType)
    {
        case SCMediaFileTypeVideo:
            return [[_settings.caheImagePath stringByAppendingPathComponent:_netPath] stringByAppendingPathExtension:@"mov"];
            break;
        case SCMediaFileTypeAudio:
            return [[_settings.caheImagePath stringByAppendingPathComponent:_netPath] stringByAppendingPathExtension:@"wav"];
            break;
        case SCMediaFileTypePhoto:
            return [[_settings.caheImagePath stringByAppendingPathComponent:_netPath] stringByAppendingPathExtension:@"png"];
            break;
            
        default:
            return [_settings.caheImagePath stringByAppendingPathComponent:_netPath];
            break;
    }
}

#pragma mark gets

#pragma mark - for completion block
-(UIImage*) imageForSize:(CGSize) forSize_
{
    UIImage* mainImage = nil;
    NSString* sizebleImageFileName = nil;
    if(CGSizeEqualToSize(forSize_, CGSizeZero))
        return nil;
    
    CGFloat curentScale = 1.0;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {
        curentScale = [[UIScreen mainScreen] scale];
    }
    
    CGSize forSize = CGSizeMake(forSize_.width*curentScale, forSize_.height*curentScale);
    if( _fileType == SCMediaFileTypePhoto )
    {
        // ищем фрагмент
        sizebleImageFileName = [_settings.caheImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"sizeble_%3.3fx%3.3F_%@.png",forSize.width,forSize.height,_netPath]];
        if([_fileManager fileExistsAtPath:sizebleImageFileName])
        {// есть - возвращаем
            return [UIImage imageWithContentsOfFile:sizebleImageFileName];
        }
        else
        {// нету - генерим
            mainImage = [UIImage imageWithContentsOfFile:_localPath];
        }
    }
    else if (_fileType == SCMediaFileTypeVideo)
    {
        // ищем уменьшенный фрагмент
        sizebleImageFileName = [_settings.caheImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"sizeble_%3.3fx%3.3F_frag_%@.png",forSize.width,forSize.height,_netPath]];
        if([_fileManager fileExistsAtPath:sizebleImageFileName])
        {
            // есть - возвращаем
            return [UIImage imageWithContentsOfFile:sizebleImageFileName];
        }
        else
        {
            // ищем фрагмент
            NSString* fragmentImageFileName = [_settings.caheImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"frag_%@.png",_netPath]];
            if([_fileManager fileExistsAtPath:fragmentImageFileName])
            {
                mainImage = [UIImage imageWithContentsOfFile:fragmentImageFileName];
                return mainImage;
                // есть - на генерацию уменьшенного
            }
            else
            {
                // нету - генерим
                mainImage = [self videoFragment];
                return mainImage;
            }
        }
    }
    
    // если что то нашли генерим
    if(mainImage)
    {
//        FTLog(@"%@",NSStringFromCGSize(mainImage.size));
//        CGFloat oldWidth = mainImage.size.width;
//        CGFloat oldHeight = mainImage.size.height;
//
//        CGFloat scaleFactor = (oldWidth > oldHeight) ? forSize.width / oldWidth : forSize.height / oldHeight;
//        
//        CGFloat newHeight = oldHeight * scaleFactor;
//        CGFloat newWidth = oldWidth * scaleFactor;
//        CGSize newImageSise = CGSizeMake(newWidth, newHeight);
//
//        UIGraphicsBeginImageContext(forSize);
////        UIGraphicsBeginImageContextWithOptions(forSize, NO, 1.0);
//        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
//            if (curentScale > 1.0) {
//                UIGraphicsBeginImageContextWithOptions(forSize, YES, curentScale);
//            } else {
//                UIGraphicsBeginImageContext(forSize);
//            }
//        } else {
//            UIGraphicsBeginImageContext(forSize);
//        }
//
//        UIGraphicsBeginImageContext(forSize_);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        CGContextSaveGState(context);
//        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        
        UIGraphicsBeginImageContext(forSize);
        UIGraphicsBeginImageContextWithOptions(forSize, NO, 1.0);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);


        [mainImage drawInRect:CGRectMake(0, 0, forSize.width, forSize.height)];
        UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        NSData *data = UIImagePNGRepresentation(scaledImage);
        [data writeToFile:sizebleImageFileName atomically:YES];
        return scaledImage;
    }
    return nil;
}
- (UIImage *) blureImage:(CGRect) forRect
{
    if( _fileType != SCMediaFileTypePhoto )
    {
        return nil;
    }

    // ищем фрагменнт
    NSString* blureImageFileName = [_settings.caheImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"blure_%3.3fx%3.3F_%@.png",forRect.size.width,forRect.size.height,_netPath]];
    if([_fileManager fileExistsAtPath:blureImageFileName])
    {// есть - возвращаем
        return [UIImage imageWithContentsOfFile:blureImageFileName];
    }
    else
    {// нету - генерим
        
        UIImage *fixedImage = [self scaleAndRotateImage:[UIImage imageWithContentsOfFile:_localPath]];
        CIImage *inputImage = [CIImage imageWithCGImage:fixedImage.CGImage];
        
        // Apply Affine-Clamp filter to stretch the image so that it does not
        // look shrunken when gaussian blur is applied
        CGAffineTransform transform = CGAffineTransformIdentity;
        CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
        [clampFilter setValue:inputImage forKey:@"inputImage"];
        [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
        
        // Apply gaussian blur filter with radius of 30
        CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
        [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
        [gaussianBlurFilter setValue:@30 forKey:@"inputRadius"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
        
        // Set up output context.
        UIGraphicsBeginImageContext(forRect.size);
        CGContextRef outputContext = UIGraphicsGetCurrentContext();
        
        // Invert image coordinates
        CGContextScaleCTM(outputContext, 1.0, -1.0);
        CGContextTranslateCTM(outputContext, 0, -forRect.size.height);
        
        // Draw base image.
        CGContextDrawImage(outputContext, forRect, cgImage);
        CGImageRelease(cgImage);
        
        // Apply white tint
        CGContextSaveGState(outputContext);
        CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
        CGContextFillRect(outputContext, forRect);
        CGContextRestoreGState(outputContext);
        
        // Output image is ready.
        UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
        NSData * binaryImageData = UIImagePNGRepresentation(outputImage);
        UIGraphicsEndImageContext();
        
        [binaryImageData writeToFile:blureImageFileName atomically:YES];
        
        return outputImage;
    }
}


- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


-(UIImage*) videoFragment
{
    // ищем фрагменнт
    NSString* fragmentImageFileName = [_settings.caheImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"frag_%@.png",_netPath]];
    if([_fileManager fileExistsAtPath:fragmentImageFileName])
    {
        // есть - возвращаем
        return [UIImage imageWithContentsOfFile:fragmentImageFileName];
    }
    // нету - генерим
    else
    {
        //
        NSURL* _theMovieURL = [NSURL fileURLWithPath:[self _makeLocalPathByType:SCMediaFileTypeVideo]];
        AVPlayerItem* _videoPlayerItem = [AVPlayerItem playerItemWithURL:_theMovieURL];
        //TODO: сохранить соотношение сторон
        
        AVAssetImageGenerator *imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:[_videoPlayerItem asset]];

        NSArray* tracksArray = [[_videoPlayerItem asset] tracksWithMediaType:AVMediaTypeVideo];
        if(tracksArray.count > 0)
        {
            AVAssetTrack* track = [tracksArray objectAtIndex:0];
            CGSize sizeVideo = [track naturalSize];
            imageGenerator.maximumSize = sizeVideo;
        }


        imageGenerator.apertureMode = AVAssetImageGeneratorApertureModeProductionAperture;
        imageGenerator.appliesPreferredTrackTransform = YES;
        
        CGImageRef ref = [imageGenerator copyCGImageAtTime:_videoPlayerItem.currentTime actualTime:nil error:nil];
        UIImage *viewImage = [UIImage imageWithCGImage:ref];
        CGImageRelease(ref);

// ??????
        if(viewImage)
        {
            CGFloat curentScale = 1.0;
            if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
            {
                curentScale = [[UIScreen mainScreen] scale];
            }
            
            CGSize forSize_ = CGSizeMake(MAX(viewImage.size.width,viewImage.size.height), MAX(viewImage.size.width,viewImage.size.height));
            CGFloat oldWidth = viewImage.size.width;
            CGFloat oldHeight = viewImage.size.height;

            CGFloat scaleFactor = (oldWidth > oldHeight) ? forSize_.width / oldWidth : forSize_.height / oldHeight;

            CGFloat newHeight = oldHeight * scaleFactor;
            CGFloat newWidth = oldWidth * scaleFactor;
    //        CGSize newImageSise = CGSizeMake(newWidth, newHeight);
            
            CGRect rectForDrawRect = CGRectMake((forSize_.width-newWidth)*.5, (forSize_.height-newHeight)*.5, newWidth, newHeight);
            
            UIGraphicsBeginImageContext(forSize_);

            if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
                if (curentScale > 1.0) {
                    UIGraphicsBeginImageContextWithOptions(forSize_, YES, curentScale);
                } else {
                    UIGraphicsBeginImageContext(forSize_);
                }
            } else {
                UIGraphicsBeginImageContext(forSize_);
            }
            UIGraphicsBeginImageContext(forSize_);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSaveGState(context);
            CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
            [viewImage drawInRect:rectForDrawRect];
            UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            NSData *data = UIImagePNGRepresentation(scaledImage);

            BOOL res = [data writeToFile:fragmentImageFileName atomically:YES];
            if(!res)
                FTLog(@"Error write to cache %@",fragmentImageFileName);
            
            return scaledImage;
        }
        else
        {
            FTLog(@"Error make fragment %@",[self _makeLocalPathByType:SCMediaFileTypeVideo]);
            return nil;
        }
    }
}


#pragma mark - new function

#pragma mark private function
- (UIImage *) _blurWithCore:(CGRect) forRect
{
    // ищем фрагменнт
    NSString* blureImageFileName = [_settings.caheImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"blure_%3.3fx%3.3F_%@",forRect.size.width,forRect.size.height,_netPath]];
    if([_fileManager fileExistsAtPath:blureImageFileName])
    {// есть - возвращаем
        return [UIImage imageWithContentsOfFile:blureImageFileName];
    }
    else
    {// нету - генерим
    
        CIImage *inputImage = [CIImage imageWithCGImage:[UIImage imageWithContentsOfFile:_localPath].CGImage];
        
        // Apply Affine-Clamp filter to stretch the image so that it does not
        // look shrunken when gaussian blur is applied
        CGAffineTransform transform = CGAffineTransformIdentity;
        CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
        [clampFilter setValue:inputImage forKey:@"inputImage"];
        [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
        
        // Apply gaussian blur filter with radius of 30
        CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
        [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
        [gaussianBlurFilter setValue:@30 forKey:@"inputRadius"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
        
        // Set up output context.
        UIGraphicsBeginImageContext(forRect.size);
        CGContextRef outputContext = UIGraphicsGetCurrentContext();
        
        // Invert image coordinates
        CGContextScaleCTM(outputContext, 1.0, -1.0);
        CGContextTranslateCTM(outputContext, 0, -forRect.size.height);
        
        // Draw base image.
        CGContextDrawImage(outputContext, forRect, cgImage);
        
        // Apply white tint
        CGContextSaveGState(outputContext);
        CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
        CGContextFillRect(outputContext, forRect);
        CGContextRestoreGState(outputContext);
        
        // Output image is ready.
        UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
        NSData * binaryImageData = UIImagePNGRepresentation(outputImage);
        UIGraphicsEndImageContext();
        
        [binaryImageData writeToFile:blureImageFileName atomically:YES];
        
        return outputImage;
    }
}


#pragma mark controll loader
-(id) getWithCompletionblock:(void (^)(SCNetFileComletionType type, id result))completionBlock
{
    _completionBlock = [completionBlock copy];
    // файл уже загружен
    if( _status == SCNetFileStatusCompleet && _localPath.length > 0)
    {
        [self onChangeStatus:SCNetFileStatusCompleet];
        completionBlock(SCNetFileComletionTypeCompleet,self);
        return _localPath;
    }
    // ошибки нет и есть окуда грузить
    else if( _status != SCNetFileStatusError && _netPath.length > 0 )
    {
        // файл лежит на диске
        if([_fileManager fileExistsAtPath:[self _makeLocalPath]])
        {
            _localPath = [self _makeLocalPath];
            [self onChangeStatus:SCNetFileStatusCompleet];
            completionBlock(SCNetFileComletionTypeCompleet,self);
            return _localPath;
        }
        // идет загрузка - ХЗ шо делать
        else if(_status == SCNetFileStatusProgress)
        {
            [self onChangeStatus:_status];
            FTLog(@"Отдохни, парень, я работаю над загрузкой ......");
        }
        // ждем - ХЗ шо делать
        else if(_status == SCNetFileStatusWait)
        {
            [self onChangeStatus:_status];
            FTLog(@"Не мешай, парень, я жду свойей очереди на загрузку ......");
        }
        // если не прогресс - запускаем загрузку
        else
        {
            [self onChangeStatus:SCNetFileStatusWait];
            _curentOperation = SCNetFileOperationDownLoad;
            [_settings.httpManager downloadToFile:_netPath delegate:self];
        }
    }
    else if(_netPath.length > 0 )
    {
        FTLog(@"Че то не сложилось для загрузки ......");
        // подождет и попробуем еще раз
        if(_waitBeforeTry)
        {
            FTLog(@"Немного ждем, и повторяем ......");
        }
        else if ( _tryLoads > 0)
        {
            FTLog(@"Еще есть попытка.... Через время .....");
            _tryLoads--;
            _waitBeforeTry = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(_waitBeforeTryAction:) userInfo:nil repeats:NO];
            
        }
        else
        {
            _completionBlock = [completionBlock copy];
            FTLog(@"Попытки закончились");
            FTLog(@"onChangeStatus статус - Кырдык");
            [self onChangeStatus:SCNetFileStatusError];
            _completionBlock(SCNetFileComletionTypeError,self);
        }
    }
    else
    {
        _completionBlock = [completionBlock copy];
        FTLog(@"А че грузить то ...... А нету ...... Абзац..");
        FTLog(@"onChangeStatus статус - Кырдык");
        [self onChangeStatus:SCNetFileStatusError];
        _completionBlock(SCNetFileComletionTypeError,self);
    }
    
    return nil;
}

-(id) sendWithCompletionblock:(void (^)(SCNetFileComletionType type, id result))completionBlock
{
//    if( _status == SCNetFileStatusNew && _localPath.length > 0)
//    {
//        
//    }
    _completionBlock = [completionBlock copy];

    [self onChangeStatus:SCNetFileStatusWait];
    NSString* fileType = @"";
    switch (_fileType) {
        case SCMediaFileTypeAudio:fileType = @"audio/mpeg";break;
        case SCMediaFileTypeVideo:fileType = @"video/mpeg";break;
        case SCMediaFileTypePhoto:fileType = @"image/jpeg";break;
            
        default: break;
    }
    
    _curentOperation = SCNetFileOperationUpload;
    [_settings.httpManager uploadFromFile:_localPath fileType:fileType delegate:self];
    return nil;
}
#pragma mark SCNetFileDelegate <NSObject>
-(void) onChangeStatus:(SCNetFileStatus) status
{
    _status = status;

//    FTLog(@"%@ onChangeStatus %@",NSStringFromClass([self class]),[SCNetFile scNetFileStatusPrint:status]);

    
    if (_completionBlock)
        _completionBlock(SCNetFileComletionTypeStatus,[NSNumber numberWithDouble:_status]);

}

-(void) onSuccess:(NSString*) tmpfile
{
    if(_curentOperation == SCNetFileOperationDownLoad)
    {
        NSError* error;
        if([_fileManager fileExistsAtPath:[self _makeLocalPath]])
            [_fileManager removeItemAtPath:[self _makeLocalPath] error:nil];
        BOOL res = [_fileManager moveItemAtPath:tmpfile toPath:[self _makeLocalPath] error:&error];
        if(res)
        {
            _localPath = [self _makeLocalPath];
            [self onChangeStatus:SCNetFileStatusCompleet];
        }
        else
        {
            _localPath = @"";
            [self onChangeStatus:SCNetFileStatusError];
        }
        
        if (_completionBlock)
        {
    //        FTLog(@"_completionBlock(SCNetFileComletionTypeStatus,_localPath)");
            _completionBlock(SCNetFileComletionTypeCompleet,self);
        }
    }
    else
    {
        NSError* error;
        _netPath = tmpfile;

        if([_fileManager fileExistsAtPath:[self _makeLocalPath]])
            [_fileManager removeItemAtPath:[self _makeLocalPath] error:nil];
        NSString* newLocalPath = [self _makeLocalPath];
        BOOL res = [_fileManager moveItemAtPath:_localPath toPath:newLocalPath error:&error];
        if(res)
        {
            _localPath = newLocalPath;
            [self onChangeStatus:SCNetFileStatusCompleet];
        }
        else
        {
            _netPath = @"";
            [self onChangeStatus:SCNetFileStatusError];
        }

        if (_completionBlock)
        {
//        FTLog(@"_completionBlock(SCNetFileComletionTypeStatus,_localPath)");
            _completionBlock(SCNetFileComletionTypeCompleet,self);
        }

    }
}

-(void) onFailure:(SCNetError*) scNetError
{
    [self onChangeStatus:SCNetFileStatusError];

    if(_curentOperation == SCNetFileOperationDownLoad)
    {
        if(scNetError.httpResponse)
        {
            NSInteger statusCode = [scNetError.httpResponse statusCode];
            if(statusCode >= 400 && statusCode < 500)
            {
                FTLog(@"Файлу на сервере кырдык. Больше не пробуем");
                _netPath = @"";

                if (_completionBlock)
                    _completionBlock(SCNetFileComletionTypeStatus,@"Error");
            }
        }
        else
        {
            if(_tryLoads < 1)
            {
                if (_completionBlock)
                    _completionBlock(SCNetFileComletionTypeStatus,@"Error");
                
            }
            else
            {
                if(_waitBeforeTry && _tryLoads > 0 )
                {
                    [_waitBeforeTry invalidate];
                    _waitBeforeTry = nil;
                }
                if(_tryLoads > 0 )
                {
                    [self onChangeStatus:SCNetFileStatusTryRepeet];
                    _tryLoads--;
                    _waitBeforeTry = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(_waitBeforeTryAction:) userInfo:nil repeats:NO];
                }
            }
        }
    }
    else
    {
        
    }
}
-(void) _waitBeforeTryAction:(NSTimer*) timer
{
    FTLog(@"Пришло время попробовать еще раз .....");
    [_waitBeforeTry invalidate];
    _waitBeforeTry = nil;
    
    [self onChangeStatus:SCNetFileStatusTryRepeet];
//    id tryObject = [timer userInfo];
    [self getWithCompletionblock:[_completionBlock copy]];
    
}
#pragma mark *** Observing Progress ***
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"fractionCompleted"])
    {
        NSProgress *progress = (NSProgress *)object;
        _progressPercent = progress.fractionCompleted;
//            FTLog(@"_completionBlock(SCNetFileComletionTypeProgress,[NSNumber numberWithDouble:_progressPercent])");
        if ([[NSThread currentThread] isMainThread])
        {
            if (_completionBlock)
            {
                _completionBlock(SCNetFileComletionTypeProgress,[NSNumber numberWithDouble:_progressPercent]);
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (_completionBlock)
                {
                    _completionBlock(SCNetFileComletionTypeProgress,[NSNumber numberWithDouble:_progressPercent]);
                }
            });
        }
        
    }
    else if ([keyPath isEqualToString:@"totalUnitCount"])
    {
        NSProgress *progress = (NSProgress *)object;
        _totalBytes = progress.totalUnitCount;

//        FTLog(@"Progress… totalUnitCount = %lld", progress.totalUnitCount);
        if ([[NSThread currentThread] isMainThread])
        {
            [self onChangeStatus:SCNetFileStatusProgress];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self onChangeStatus:SCNetFileStatusProgress];
            });
        }
        
    }
    else if ([keyPath isEqualToString:@"completedUnitCount"])
    {
        NSProgress *progress = (NSProgress *)object;
        
//        FTLog(@"Progress… completedUnitCount = %lld", progress.completedUnitCount);
        _bytesProcessed = progress.completedUnitCount;
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

+(NSString*) scNetFileStatusPrint:(SCNetFileStatus) aStatus
{
    switch (aStatus)
    {
        case SCNetFileStatusNew:return @"SCNetFileStatusNew";break;
        case SCNetFileStatusWait:return @"SCNetFileStatusWait";break;
        case SCNetFileStatusStart:return @"SCNetFileStatusStart";break;
        case SCNetFileStatusProgress:return @"SCNetFileStatusProgress";break;
        case SCNetFileStatusError:return @"SCNetFileStatusError";break;
        case SCNetFileStatusCompleet:return @"SCNetFileStatusCompleet";break;
        case SCNetFileStatusTryRepeet:return @"SCNetFileStatusTryRepeet";break;
        default:return @"SCNetFileStatus";break;
    }
}

@end
