//
//  AppDelegate.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 06.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

