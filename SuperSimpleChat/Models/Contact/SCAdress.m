//
//  SCAdress.m
//  SmartChat
//
//  Created by Yuriy Troyan on 06.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAdress.h"

@implementation SCAdress

-(void) _init
{
    
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
    if([[dictionary allKeys] containsObject:@"country"])
        _country = [dictionary objectForKey:@"country"];
    if([[dictionary allKeys] containsObject:@"postCode"])
        _postCode = [dictionary objectForKey:@"postCode"];
    if([[dictionary allKeys] containsObject:@"city"])
        _city = [dictionary objectForKey:@"city"];
    if([[dictionary allKeys] containsObject:@"street"])
        _street = [dictionary objectForKey:@"street"];
    if([[dictionary allKeys] containsObject:@"house"])
        _house = [dictionary objectForKey:@"house"];
    if([[dictionary allKeys] containsObject:@"appartament"])
        _appartament = [dictionary objectForKey:@"appartament"];
}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* outDict = [[NSMutableDictionary alloc] init];
    if (_country)
        [outDict setObject:_country forKey:@"country"];
    if(_postCode)
        [outDict setObject:_postCode forKey:@"postCode"];
    if(_city)
        [outDict setObject:_city forKey:@"city"];
    if(_street)
        [outDict setObject:_street forKey:@"street"];
    if(_house)
        [outDict setObject:_house forKey:@"house"];
    if(_appartament)
        [outDict setObject:_appartament forKey:@"appartament"];
    
    return outDict;
}

-(NSString*) fullAddress
{
    return [NSString stringWithFormat:@"%@, %@ %@, %@ %@-%@",_country,_postCode,_city,_street,_house,_appartament];
}



@end
