//
//  SCMediaCapture.m
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMediaCapture.h"
#import "SCSettings.h"
#import "SCMediaUtils.h"


#define CAPTURE_FRAMES_PER_SECOND		20

static void * RecordingContext = &RecordingContext;
static void * CapturingStillImageContext = &CapturingStillImageContext;


@implementation SCMediaCapture
{
    SCSettings* _settings;
    AVCaptureSession* _captureSession;
    AVCaptureDeviceInput* _videoInput;
    AVCaptureVideoPreviewLayer *_captureVideoPreviewLayer;
    AVCaptureMovieFileOutput* _movieFileOutput;
    AVCaptureStillImageOutput *_stillImageOutput;
    UIDeviceOrientation _deviceOrientation;
    NSString *_outputPath;
    
    BOOL weAreRecording;
    NSTimer* _timerUIblinck;
    NSTimer* _recordingTimer;
    BOOL _isBtnRecordHileght;
    CGFloat _maxScale;
    UIButton* _btnClose;
    UIButton* _btnCameraSelect;
    UIButton* _btnRecord;
    UILabel* _labTime;
}

-(id) init
{
    self = [super init];
    if(self)
    {
//        [self _init];
    }
    return self;
}

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if(self)
    {
        _settings = settings;
//        [self _init];
    }
    return self;
}

-(void) _init
{
    _deviceOrientation = UIDeviceOrientationPortrait;
    _isBtnRecordHileght = NO;
    _maxScale = 1.0;
    
    _captureSession = [[AVCaptureSession alloc] init];
    
    [_captureSession beginConfiguration];
    _captureSession.sessionPreset = AVCaptureSessionPresetHigh;

    _captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    _captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _captureVideoPreviewLayer.frame = self.bounds;

    [self.layer addSublayer:_captureVideoPreviewLayer];
    
    AVCaptureDevice *VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    _videoInput = [AVCaptureDeviceInput deviceInputWithDevice:VideoDevice error:&error];
    if (!_videoInput)
    {
        FTLog(@"ERROR: trying to open camera: %@", error);
    }
    else
        [_captureSession addInput:_videoInput];
    
    AVCaptureDevice *audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];

    AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioCaptureDevice error:&error];
    if (audioInput)
    {
        [_captureSession addInput:audioInput];
    }
    
    _btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnClose.frame = CGRectMake(10,20,32,32);
    [_btnClose setImage:[_settings.skinManager getImageForKeyX:@"btn_back"] forState:UIControlStateNormal];
    [_btnClose addTarget:self action:@selector(btnCloseAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_btnClose];
    
    if ([self hasMultipleCameras])
    {
        _btnCameraSelect = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnCameraSelect.frame = CGRectMake((self.frame.size.width-32)*0.5,20,32,32);
        [_btnCameraSelect setImage:[_settings.skinManager getImageForKeyX:@"profile-settings"] forState:UIControlStateNormal];
        [_btnCameraSelect addTarget:self action:@selector(btnCameraSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btnCameraSelect];
    }
    
#pragma mark Video init
    if(_captureMode == SCCaptureModeVideo)
    {
// UI
        _labTime = [[UILabel alloc] init];
        _labTime.frame = CGRectMake(self.frame.size.width-110, 60, 100, 20);
        [_labTime setTextAlignment:NSTextAlignmentRight];
        [_labTime setFont:[UIFont fontWithName:FT_FontNameLight size:14]];
        [_labTime setTextColor:[UIColor redColor]];
        [_labTime setHidden:YES];
        [self addSubview:_labTime];
        
        _btnRecord = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnRecord.frame = CGRectMake(self.frame.size.width-32-10,20,32,32);
        [_btnRecord setImage:[_settings.skinManager getImageForKeyX:@"chat-record-start"] forState:UIControlStateNormal];
        [_btnRecord addTarget:self action:@selector(btnRecordAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btnRecord];
        
// Video
        NSLog(@"Adding movie file output");
        _movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
        
        Float64 TotalSeconds = 60;			//Total seconds
        int32_t preferredTimeScale = 30;	//Frames per second
        CMTime maxDuration = CMTimeMakeWithSeconds(TotalSeconds, preferredTimeScale);
        _movieFileOutput.maxRecordedDuration = maxDuration;
        _movieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024;
        
        if ([_captureSession canAddOutput:_movieFileOutput])
            [_captureSession addOutput:_movieFileOutput];
        
        
        [_captureSession setSessionPreset:AVCaptureSessionPresetMedium];
        if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset640x480])
            [_captureSession setSessionPreset:AVCaptureSessionPreset640x480];
        
        [self addObserver:self forKeyPath:@"movieFileOutput.recording"  options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];

        [self cameraSetOutputProperties];

    }
#pragma mark Photo init
    else if (_captureMode == SCCaptureModePhoto)
    {
// UI
        _btnRecord = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnRecord.frame = CGRectMake(self.frame.size.width-32-10,20,32,32);
        [_btnRecord setImage:[_settings.skinManager getImageForKeyX:@"chat-add-message-photo"] forState:UIControlStateNormal];
        [_btnRecord addTarget:self action:@selector(btnTakePhotoAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btnRecord];
        
// image
        _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
//        NSDictionary *outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA], (id)kCVPixelBufferPixelFormatTypeKey,AVVideoCodecJPEG, nil];
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
        [_stillImageOutput setOutputSettings:outputSettings];
//        [_stillImageOutput setOutputSettings:@{ AVVideoCodecKey : AVVideoCodecJPEG }];
        
        if ( [_captureSession canAddOutput:_stillImageOutput] )
            [_captureSession addOutput:_stillImageOutput];
        [_captureSession setSessionPreset:AVCaptureSessionPresetPhoto];
        [self setFlashMode:AVCaptureFlashModeOff];

        
//        [self cameraSetOutputProperties];

    }
    
    [_captureSession commitConfiguration];
    [_captureSession startRunning];
}

-(void) dealloc
{
    FTLog(@"%@ dealloc",NSStringFromClass([self class]));
    if(_captureMode == SCCaptureModeVideo)
        [self removeObserver:self forKeyPath:@"movieFileOutput.recording"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    FTLog(@"");
    if (context == RecordingContext)
    {
        BOOL isRecording = [change[NSKeyValueChangeNewKey] boolValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isRecording)
            {
                [_btnRecord setImage:[_settings.skinManager getImageForKeyX:@"chat-record-stop"] forState:UIControlStateNormal];
                _timerUIblinck = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(_timerUIblinckAction:) userInfo:nil repeats:YES];
                _recordingTimer = [NSTimer scheduledTimerWithTimeInterval:0.33f target:self selector:@selector(_recordingTimerAction:) userInfo:nil repeats:YES];
                [_recordingTimer fire];
                [_labTime setHidden:NO];
            }
            else
            {
                [_labTime setHidden:YES];
                if(_recordingTimer)
                {
                    [_recordingTimer invalidate];
                    _recordingTimer = nil;
                }
                if(_timerUIblinck)
                {
                    [_timerUIblinck invalidate];
                    _timerUIblinck = nil;
                }
                [_btnRecord setImage:[_settings.skinManager getImageForKeyX:@"chat-record-start"] forState:UIControlStateNormal];

            }
        });
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

-(void) setSettings:(SCSettings*) settings
{
    _settings = settings;
    [self _init];
}
-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectIsEmpty(frame))
    {
        _btnClose.frame = CGRectMake(10,20,32,32);
        if(_btnCameraSelect)
            _btnCameraSelect.frame = CGRectMake((self.frame.size.width-32)*0.5,20,32,32);
        _btnRecord.frame = CGRectMake(self.frame.size.width-32-10,20,32,32);
        if(_labTime)
            _labTime.frame = CGRectMake(self.frame.size.width-110, 60, 100, 20);
    }
    
}
-(void) layoutSubviews
{
    [super layoutSubviews];
    _captureVideoPreviewLayer.frame = self.bounds;
}

#pragma mark - Actions
-(void) btnCloseAction:(id) sender
{
    [UIView animateWithDuration:0.25 animations:^{
        self.frame = CGRectMake(0, 0, -self.frame.size.width, self.frame.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}
-(void) btnCameraSelectAction:(id) sender
{
    [self cameraToggle];
}

-(void) btnRecordAction:(id) sender
{
    if (!weAreRecording)
    {
        NSLog(@"START RECORDING");
        weAreRecording = YES;
        
        _outputPath = [_settings.mediaTempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mov",[self makeTempFileName]]];
        NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:_outputPath];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:_outputPath])
        {
            NSError *error;
            if ([fileManager removeItemAtPath:_outputPath error:&error] == NO)
            {
                FTLog(@"Error deleting %@ file",_outputPath);
            }
        }
        [_movieFileOutput startRecordingToOutputFileURL:outputURL recordingDelegate:self];
    }
    else
    {
        NSLog(@"STOP RECORDING");
        weAreRecording = NO;
        
        [_movieFileOutput stopRecording];
    }
}

-(void) btnTakePhotoAction:(id) sender
{
    
    NSString* mediaType = AVMediaTypeVideo;
    NSArray* connections = _stillImageOutput.connections;
    AVCaptureConnection *videoConnection = nil;
    for ( AVCaptureConnection *connection in connections )
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [port.mediaType isEqual:mediaType] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection)
        {
            break;
        }
    }
    
    if (!videoConnection)
    {
        NSError *error = [NSError errorWithDomain:@"DBCamera" code:-1 userInfo: @{NSLocalizedFailureReasonErrorKey: @"cameraimage.noconnection" }];
        return;
    }
    
    if ( [videoConnection isVideoOrientationSupported] )
    {
        switch (_deviceOrientation)
        {
            case UIDeviceOrientationPortraitUpsideDown:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
                break;
                
            case UIDeviceOrientationLandscapeLeft:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
                break;
                
            case UIDeviceOrientationLandscapeRight:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                break;
                
            default:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                break;
        }
    }

//    [_captureSession setSessionPreset:AVCaptureSessionPresetPhoto];
//
//    AVCaptureConnection *videoConnection = [_stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
//    [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
    
//    NSLog(@"%@",[_stillImageOutput outputSettings]);
    
    [videoConnection setVideoScaleAndCropFactor:_maxScale];
    
    [_stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error)
     {
         [_captureSession stopRunning];
         
         if ( imageDataSampleBuffer != NULL )
         {
             _outputPath = [_settings.mediaTempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[self makeTempFileName]]];
             

             
//             CFDictionaryRef metadata = CMCopyDictionaryOfAttachments(NULL, imageDataSampleBuffer, kCMAttachmentMode_ShouldPropagate);
//             NSDictionary *meta = [[NSDictionary alloc] initWithDictionary:(__bridge NSDictionary *)(metadata)];
//             CFRelease(metadata);
             NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
             
             UIImage* im = [[UIImage alloc] initWithData:imageData];
             UIImage *rotatedImage = [self rotateUIImage:im];
             NSData* rotatedImageData = UIImageJPEGRepresentation(rotatedImage, 0.75);
             BOOL res = [rotatedImageData writeToFile:_outputPath atomically:YES];
             if(res)
             {
                 if(_delegate && [_delegate respondsToSelector:@selector(fileCaptured:forType:)])
                 {
                     [_delegate fileCaptured:_outputPath forType:SCDataTypePhotoFile];
                     [self btnCloseAction:self];
                 }
             }
             //             UIImage *image = [[UIImage alloc] initWithData:imageData];
             
             
             
         }
         NSLog(@"End");
     }];
    
}

#pragma mark -
-(void) _timerUIblinckAction:(NSTimer*) tm
{
    if(_isBtnRecordHileght)
    {
        [_btnRecord setImage:[_settings.skinManager getImageForKeyX:@"chat-record-stop"] forState:UIControlStateNormal];
        _isBtnRecordHileght = NO;
    }
    else
    {
        [_btnRecord setImage:[_settings.skinManager getImageForKeyX:@"chat-record-stop-white"] forState:UIControlStateNormal];
        _isBtnRecordHileght = YES;
    }
}

-(void) _recordingTimerAction:(NSTimer*) tm
{
    double duration = CMTimeGetSeconds([_movieFileOutput recordedDuration]);
    double time = CMTimeGetSeconds([_movieFileOutput maxRecordedDuration]);
//    CGFloat progress = (CGFloat) (duration / time);
//    NSLog(@"%f",progress);
    [_labTime setText:[NSString stringWithFormat:@"%@ / %@",[SCMediaUtils formatSecond:duration],[SCMediaUtils formatSecond:time]]];
}

#pragma mark AVCaptureFileOutputRecordingDelegate <NSObject>
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections
{
    
}

- (void) captureOutput:(AVCaptureFileOutput *)captureOutput
didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
       fromConnections:(NSArray *)connections
                 error:(NSError *)error
{
    BOOL recordedSuccessfully = YES;
    if ([error code] != noErr)
    {
        // A problem occurred: Find out if the recording was successful.
        id value = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];
        if (value)
        {
            recordedSuccessfully = [value boolValue];
        }
    }
    if (recordedSuccessfully)
    {
        //----- RECORDED SUCESSFULLY -----
        if(_delegate && [_delegate respondsToSelector:@selector(fileCaptured:forType:)])
           {
               [_delegate fileCaptured:_outputPath forType:SCDataTypeVideoFile];
               [self btnCloseAction:self];
           }
    }
    weAreRecording = NO;
    NSLog(@"STOP RECORDING");
}

- (void) cameraSetOutputProperties
{
    AVCaptureConnection *videoConnection = nil;
    if(_movieFileOutput)
        videoConnection = [_movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
    else if (_stillImageOutput)
    {
        videoConnection = [_stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
    }
    

    if ( [videoConnection isVideoOrientationSupported] )
    {
        switch (_deviceOrientation)
        {
            case UIDeviceOrientationPortraitUpsideDown:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
                break;
                
            case UIDeviceOrientationLandscapeLeft:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
                break;
                
            case UIDeviceOrientationLandscapeRight:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                break;
                
            case UIDeviceOrientationPortrait:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                break;
                
            default:
                [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                break;
        }
    }
}

- (void) setCameraMaxScale:(CGFloat)maxScale
{
    _maxScale = maxScale;
}



- (BOOL) cameraToggle
{
    BOOL success = NO;
    
    if ( [self hasMultipleCameras] )
    {
        NSError *error;
        AVCaptureDeviceInput *newVideoInput;
        AVCaptureDevicePosition position = _videoInput.device.position;
        
        if ( position == AVCaptureDevicePositionBack )
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self frontCamera] error:&error];
        else if (position == AVCaptureDevicePositionFront)
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self backCamera] error:&error];
        else
            goto bail;
        
        if ( newVideoInput != nil )
        {
            [_captureSession beginConfiguration];
            [_captureSession removeInput:_videoInput];
            
            if ( [_captureSession canAddInput:newVideoInput] )
            {
                [_captureSession addInput:newVideoInput];
                
                _videoInput = newVideoInput;
            }
            else
                [_captureSession addInput:_videoInput];
            
            [self cameraSetOutputProperties];
            [_captureSession commitConfiguration];
            
            success = YES;
        }
        else if ( error )
        {
            FTLog(@"someOtherError:");
        }
    }
    
bail:
    return success;
}



- (BOOL) hasMultipleCameras
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count] > 1 ? YES : NO;
}

- (BOOL) hasFlash
{
    return _videoInput.device.hasFlash;
}

- (AVCaptureFlashMode) flashMode
{
    return _videoInput.device.flashMode;
}

- (void) setFlashMode:(AVCaptureFlashMode)flashMode
{
    AVCaptureDevice *device = _videoInput.device;
    if ( [device isFlashModeSupported:flashMode] && device.flashMode != flashMode )
    {
        NSError *error;
        if ( [device lockForConfiguration:&error] )
        {
            device.flashMode = flashMode;
            [device unlockForConfiguration];
        }
        else
        {
            FTLog(@"acquiringDeviceLockFailedWithError:");
        }
    }
}


- (BOOL) hasTorch
{
    return _videoInput.device.hasTorch;
}

- (BOOL) hasFocus
{
    AVCaptureDevice *device = _videoInput.device;
    
    return  [device isFocusModeSupported:AVCaptureFocusModeLocked] ||
    [device isFocusModeSupported:AVCaptureFocusModeAutoFocus] ||
    [device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus];
}

- (BOOL) hasExposure
{
    AVCaptureDevice *device = _videoInput.device;
    
    return  [device isExposureModeSupported:AVCaptureExposureModeLocked] ||
    [device isExposureModeSupported:AVCaptureExposureModeAutoExpose] ||
    [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure];
}

- (BOOL) hasWhiteBalance
{
    AVCaptureDevice *device = _videoInput.device;
    
    return  [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeLocked] ||
    [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance];
}

#pragma mark - Focus & Exposure

- (void) focusAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = _videoInput.device;
    if ( device.isFocusPointOfInterestSupported && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus] )
    {
        NSError *error;
        if ( [device lockForConfiguration:&error] )
        {
            device.focusPointOfInterest = point;
            device.focusMode = AVCaptureFocusModeAutoFocus;
            [device unlockForConfiguration];
        }
        else
        {
            FTLog(@"acquiringDeviceLockFailedWithError:");
        }
    }
}

- (void) exposureAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = _videoInput.device;
    if ( device.isExposurePointOfInterestSupported && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure] )
    {
        NSError *error;
        if ( [device lockForConfiguration:&error] )
        {
            device.exposurePointOfInterest = point;
            device.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
            [device unlockForConfiguration];
        }
        else
        {
            FTLog(@"acquiringDeviceLockFailedWithError:");
        }
    }
}

- (CGPoint) convertToPointOfInterestFrom:(CGRect)frame coordinates:(CGPoint)viewCoordinates layer:(AVCaptureVideoPreviewLayer *)layer
{
    CGPoint pointOfInterest = (CGPoint){ 0.5f, 0.5f };
    CGSize frameSize = frame.size;
    
    AVCaptureVideoPreviewLayer *videoPreviewLayer = layer;
    
    if ( [[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResize] )
        pointOfInterest = (CGPoint){ viewCoordinates.y / frameSize.height, 1.0f - (viewCoordinates.x / frameSize.width) };
    else {
        CGRect cleanAperture;
        for (AVCaptureInputPort *port in _videoInput.ports)
        {
            if ([port mediaType] == AVMediaTypeVideo)
            {
                cleanAperture = CMVideoFormatDescriptionGetCleanAperture([port formatDescription], YES);
                CGSize apertureSize = cleanAperture.size;
                CGPoint point = viewCoordinates;
                
                CGFloat apertureRatio = apertureSize.height / apertureSize.width;
                CGFloat viewRatio = frameSize.width / frameSize.height;
                CGFloat xc = 0.5f;
                CGFloat yc = 0.5f;
                
                if ( [[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspect] )
                {
                    if (viewRatio > apertureRatio)
                    {
                        CGFloat y2 = frameSize.height;
                        CGFloat x2 = frameSize.height * apertureRatio;
                        CGFloat x1 = frameSize.width;
                        CGFloat blackBar = (x1 - x2) / 2;
                        if (point.x >= blackBar && point.x <= blackBar + x2)
                        {
                            xc = point.y / y2;
                            yc = 1.0f - ((point.x - blackBar) / x2);
                        }
                    }
                    else
                    {
                        CGFloat y2 = frameSize.width / apertureRatio;
                        CGFloat y1 = frameSize.height;
                        CGFloat x2 = frameSize.width;
                        CGFloat blackBar = (y1 - y2) / 2;
                        if (point.y >= blackBar && point.y <= blackBar + y2)
                        {
                            xc = ((point.y - blackBar) / y2);
                            yc = 1.0f - (point.x / x2);
                        }
                    }
                }
                else if ([[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspectFill])
                {
                    if (viewRatio > apertureRatio)
                    {
                        CGFloat y2 = apertureSize.width * (frameSize.width / apertureSize.height);
                        xc = (point.y + ((y2 - frameSize.height) / 2.0f)) / y2;
                        yc = (frameSize.width - point.x) / frameSize.width;
                    }
                    else
                    {
                        CGFloat x2 = apertureSize.height * (frameSize.height / apertureSize.width);
                        yc = 1.0f - ((point.x + ((x2 - frameSize.width) / 2)) / x2);
                        xc = point.y / frameSize.height;
                    }
                }
                
                pointOfInterest = (CGPoint){ xc, yc };
                break;
            }
        }
    }
    return pointOfInterest;
}
- (NSUInteger) cameraCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}

- (AVCaptureTorchMode) torchMode
{
    return _videoInput.device.torchMode;
}

- (void) setTorchMode:(AVCaptureTorchMode)torchMode
{
    AVCaptureDevice *device = _videoInput.device;
    if ( [device isTorchModeSupported:torchMode] && device.torchMode != torchMode )
    {
        NSError *error;
        if ( [device lockForConfiguration:&error] )
        {
            device.torchMode = torchMode;
            [device unlockForConfiguration];
        }
        else
        {
            FTLog(@"acquiringDeviceLockFailedWithError:");
        }
    }
}

- (AVCaptureFocusMode) focusMode
{
    return _videoInput.device.focusMode;
}

- (void) setFocusMode:(AVCaptureFocusMode)focusMode
{
    AVCaptureDevice *device = _videoInput.device;
    if ([device isFocusModeSupported:focusMode] && device.focusMode != focusMode)
    {
        NSError *error;
        if ([device lockForConfiguration:&error])
        {
            device.focusMode = focusMode;
            [device unlockForConfiguration];
        }
        else
        {
            FTLog(@"acquiringDeviceLockFailedWithError:");
        }
    }
}

- (AVCaptureExposureMode) exposureMode
{
    return _videoInput.device.exposureMode;
}

- (void) setExposureMode:(AVCaptureExposureMode)exposureMode
{
    if ( exposureMode == AVCaptureExposureModeAutoExpose )
        exposureMode = AVCaptureExposureModeContinuousAutoExposure;
    
    AVCaptureDevice *device = _videoInput.device;
    if ( [device isExposureModeSupported:exposureMode] && device.exposureMode != exposureMode )
    {
        NSError *error;
        if ( [device lockForConfiguration:&error] )
        {
            device.exposureMode = exposureMode;
            [device unlockForConfiguration];
        }
        else
        {
            FTLog(@"acquiringDeviceLockFailedWithError:");
        }
    }
}

- (AVCaptureWhiteBalanceMode) whiteBalanceMode
{
    return _videoInput.device.whiteBalanceMode;
}

- (void) setWhiteBalanceMode:(AVCaptureWhiteBalanceMode)whiteBalanceMode
{
    if ( whiteBalanceMode == AVCaptureWhiteBalanceModeAutoWhiteBalance )
        whiteBalanceMode = AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;
    
    AVCaptureDevice *device = _videoInput.device;
    if ( [device isWhiteBalanceModeSupported:whiteBalanceMode] && device.whiteBalanceMode != whiteBalanceMode )
    {
        NSError *error;
        if ( [device lockForConfiguration:&error] )
        {
            device.whiteBalanceMode = whiteBalanceMode;
            [device unlockForConfiguration];
        }
        else
        {
            FTLog(@"acquiringDeviceLockFailedWithError:");
        }
    }
}

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition)position
{
    __block AVCaptureDevice *deviceBlock = nil;
    
    [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] enumerateObjectsUsingBlock:^( AVCaptureDevice *device, NSUInteger idx, BOOL *stop )
    {
        if ( [device position] == position )
        {
            deviceBlock = device;
            *stop = YES;
        }
    }];
    
    return deviceBlock;
}

- (AVCaptureDevice *) frontCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionFront];
}

- (AVCaptureDevice *)backCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionBack];
}

-(NSString*) makeTempFileName
{
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    return [NSString stringWithFormat:@"camera_%@", guid];
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (UIImage *) rotateUIImage:(UIImage*) inImage
{
    // No-op if the orientation is already correct
    if (inImage.imageOrientation == UIImageOrientationUp) return inImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (inImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, inImage.size.width, inImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, inImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, inImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (inImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, inImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, inImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, inImage.size.width, inImage.size.height,
                                             CGImageGetBitsPerComponent(inImage.CGImage), 0,
                                             CGImageGetColorSpace(inImage.CGImage),
                                             CGImageGetBitmapInfo(inImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (inImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,inImage.size.height,inImage.size.width), inImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,inImage.size.width,inImage.size.height), inImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}
@end
