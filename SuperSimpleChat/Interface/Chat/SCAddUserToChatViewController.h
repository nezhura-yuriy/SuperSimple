//
//  SCAddUserToChatViewController.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCChatItem.h"

@class SCSettings;


@interface SCAddUserToChatViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) SCChatItem* chatItem;
@property(assign, nonatomic) BOOL isNewChat;


@property (nonatomic, strong) NSMutableArray* userInRoom;



@end
