//
//  SCChatPhotoMenu.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 23.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface SCChatPhotoMenu : SCChatBottomsView <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, strong) NSURL *fotoURL;

@end
