//
//  SCPBManagerTernopol+Converts.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol(Converts).h"
#import "SmartChat.pb.h"

#import "SCPUUID.h"
#import "SCContactItem.h"
#import "SCNetFile.h"


@implementation SCPBManagerTernopol (Converts)

#pragma mark - PUUID
- (PUUID *) puuidBuilder: (SCPUUID *) puuid
{
    PUUIDBuilder* puidBuilder = [[PUUIDBuilder alloc] init];
    
    [puidBuilder setLeastSignificantBits:puuid.leastSignificantBits];
    [puidBuilder setMostSignificantBits:puuid.mostSignificantBits];
    
    return [puidBuilder build];
}

- (SCPUUID *) scpuuidBuilder: (PUUID *) puuid
{
    return [[SCPUUID alloc] initWithLeastSignificantBits:puuid.leastSignificantBits MostSignificantBits:puuid.mostSignificantBits];
}

- (NSString *) scpuuidStringBuilder: (PUUID *) puuid
{
    SCPUUID *scpuuid = [[SCPUUID alloc] initWithLeastSignificantBits:puuid.leastSignificantBits MostSignificantBits:puuid.mostSignificantBits];
    
    return [scpuuid scpuuidToString];
}

#pragma mark - statuses
-(SCUserStatus) statusToSCContactStatus:(UserStatus*) status
{
    switch (status.statusType)
    {
        case StatusTypeOnline:return SCUserStatusOnline;break;
        case StatusTypeOffline:return SCUserStatusOffline;break;
        case StatusTypeAway:return SCUserStatusAway;break;
        case StatusTypeBusy:return SCUserStatusBusy;break;
        case StatusTypeInvisible:return SCUserStatusInvisible;break;
            
        default:return SCUserStatusOffline;break;
    }
}
-(StatusType) scContactStatusToStatus:(SCUserStatus) scStatus
{
    switch (scStatus)
    {
        case SCUserStatusOnline:
            return StatusTypeOnline;break;
        case SCUserStatusOffline:
            return StatusTypeOffline;break;
        case SCUserStatusAway:
            return StatusTypeAway;break;
        case SCUserStatusBusy:
            return StatusTypeBusy;break;
        case SCUserStatusInvisible:
            return StatusTypeInvisible;break;
            
        default:
            return StatusTypeOffline;break;
    }
}

#pragma mark - Fields
-(SCFieldType) fieldTypeToSCFieldType:(FieldType) fieldType
{
    switch (fieldType) {
        case FieldTypeLoginName:return SCFieldTypeLoginName;break;
        case FieldTypeNickname:return SCFieldTypeNickName;break;
        case FieldTypeFirstname:return SCFieldTypeFirstName;break;
        case FieldTypeLastname:return SCFieldTypeLastName;break;
        case FieldTypeSex:return SCFieldTypeSex;break;
        case FieldTypeBirthdate:return SCFieldTypeBirthdate;break;
        case FieldTypeImageLink:return SCFieldTypeImageLink;break;
        case FieldTypeLanguage:return SCFieldTypeLanguage;break;
        case FieldTypeTimezone:return SCFieldTypeTimeZone;break;
        case FieldTypeCountry:return SCFieldTypeCountry;break;
        case FieldTypeAbout:return SCFieldTypeAbout;break;
        case FieldTypeWebsite:return SCFieldTypeWebsite;break;
        case FieldTypeStreet:return SCFieldTypeStreet;break;
        case FieldTypeCity:return SCFieldTypeCyty;break;
        case FieldTypePostCode:return SCFieldTypePostCode;break;
        case FieldTypeAppartament:return SCFieldTypeAppartament;break;
        case FieldTypeHouse:return SCFieldTypeHose;break;
        case FieldTypeImageId:return SCFieldTypeImageid;break;
//        case FieldTypeAuths:return SCFieldTypeAuths;break;
        default:return SCFieldTypeNone;break;

    }
}

-(FieldType) scFieldTypeToFieldType:(SCFieldType) scFieldType
{
    switch (scFieldType) {
        case SCFieldTypeLoginName:return FieldTypeLoginName;break;
        case SCFieldTypeNickName:return FieldTypeNickname;break;
        case SCFieldTypeFirstName:return FieldTypeFirstname;break;
        case SCFieldTypeLastName:return FieldTypeLastname;break;
        case SCFieldTypeSex:return FieldTypeSex;break;
        case SCFieldTypeBirthdate:return FieldTypeBirthdate;break;
        case SCFieldTypeImageLink:return FieldTypeImageLink;break;
        case SCFieldTypeLanguage:return FieldTypeLanguage;break;
        case SCFieldTypeTimeZone:return FieldTypeTimezone;break;
        case SCFieldTypeCountry:return FieldTypeCountry;break;
        case SCFieldTypeAbout:return FieldTypeAbout;break;
        case SCFieldTypeWebsite:return FieldTypeWebsite;break;
        case SCFieldTypeStreet:return FieldTypeStreet;break;
        case SCFieldTypeCyty:return FieldTypeCity;break;
        case SCFieldTypePostCode:return FieldTypePostCode;break;
        case SCFieldTypeAppartament:return FieldTypeAppartament;break;
        case SCFieldTypeHose:return FieldTypeHouse;break;
            
            
        case SCFieldTypeImageid:return FieldTypeImageId;break;
//        case SCFieldTypeAuths:return FieldTypeAuths;break;
            
//        case SCFieldTypePhone:return FieldTypePhone;break;
//        case SCFieldTypeEmail:return FieldTypeEmail;break;
        default:return 0;break;
    }
}

-(Field*) fieldFromContact:(SCFieldType) scFieldType fromValue:(id)value
{
    FieldBuilder *fieldBuilder= [[FieldBuilder alloc]init];
    [fieldBuilder setType:[self scFieldTypeToFieldType:scFieldType]];
    switch (scFieldType) {
        case SCFieldTypeBirthdate:
        {
            if([value isKindOfClass:[NSDate class]])
            {
                SInt64 longDate = [((NSDate*)value) timeIntervalSince1970]*1000;
                NSString* stringDate = [NSString stringWithFormat:@"%lld",longDate];
                FTLog(@"%@",stringDate);
                [fieldBuilder setValue:stringDate];
            }
        }
            break;
        default:
        {
            NSString* stringValue = value;
            FTLog(@"in SCPBManagerTernopol(Converts).m value XX = %@",stringValue);
            [fieldBuilder setValue:stringValue];
        }
            break;
    }
    
    return [fieldBuilder build];
}

-(Field*) fieldFromContact:(SCFieldType) scFieldType fromItem:(SCContactItem*)contactItem
{
    FieldBuilder *fieldBuilder= [[FieldBuilder alloc]init];
    [fieldBuilder setType:[self scFieldTypeToFieldType:scFieldType]];
    switch (scFieldType) {
        case SCFieldTypeLoginName:[fieldBuilder setValue:contactItem.contactLoginName];break;
        case SCFieldTypeNickName:[fieldBuilder setValue:contactItem.contactNikName];break;
        case SCFieldTypeFirstName:[fieldBuilder setValue:contactItem.contactFirstName];break;
        case SCFieldTypeLastName:[fieldBuilder setValue:contactItem.contactLastName];break;
        case SCFieldTypeSex:[fieldBuilder setValue:contactItem.contactSex];break;
        case SCFieldTypeBirthdate:{
            SInt64 longDate = (SInt64)[NSDate dateWithTimeIntervalSince1970:((SInt64)contactItem.contactBirthdate*1000)];
            [fieldBuilder setValue:[NSString stringWithFormat:@"%lld",longDate]];
        }break;
        case SCFieldTypeImageLink:
#ifdef OLD_FILE_LINK
            [fieldBuilder setValue:contactItem.contactImageLink];
#endif
            [fieldBuilder setValue:contactItem.netFile.netPath];
            break;
        case SCFieldTypeLanguage:[fieldBuilder setValue:contactItem.contactLanguage];break;
        case SCFieldTypeTimeZone:[fieldBuilder setValue:contactItem.contactTimeZone];break;
        case SCFieldTypeCountry:[fieldBuilder setValue:contactItem.contactAddress.country];break;
        case SCFieldTypeAbout:[fieldBuilder setValue:contactItem.contactAbout];break;
        case SCFieldTypeWebsite:[fieldBuilder setValue:contactItem.contactWebSite];break;
        case SCFieldTypeStreet:[fieldBuilder setValue:contactItem.contactAddress.street];break;
        case SCFieldTypeCyty:[fieldBuilder setValue:contactItem.contactAddress.city];break;
        case SCFieldTypePostCode:[fieldBuilder setValue:contactItem.contactAddress.postCode];break;
        case SCFieldTypeAppartament:[fieldBuilder setValue:contactItem.contactAddress.appartament];break;
        case SCFieldTypeHose:[fieldBuilder setValue:contactItem.contactAddress.house];break;
        case SCFieldTypeImageid:
#ifdef OLD_FILE_LINK
            [fieldBuilder setValue:contactItem.contactImageLink];
#endif
            [fieldBuilder setValue:contactItem.netFile.netPath];
            break;
        case SCFieldTypePhone:[fieldBuilder setValue:contactItem.contactPhone];break;
        case SCFieldTypeEmail:[fieldBuilder setValue:contactItem.contactEmail];break;
//        case SCFieldTypeAuths:[fieldBuilder setValue:contactItem.userAuths];break;
            
        default:
            break;
    }

    return [fieldBuilder build];
}

-(NSArray*) fieldsFromContact:(SCContactItem*)contactItem
{
    NSMutableArray* arrayFields = [[NSMutableArray alloc] init];
    
    
    if (contactItem.contactNikName)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeNickName fromItem:contactItem]];

    if (contactItem.contactLoginName)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeLoginName fromItem:contactItem]];

    if (contactItem.contactFirstName)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeFirstName fromItem:contactItem]];

    if (contactItem.contactLastName)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeLastName fromItem:contactItem]];

    if (contactItem.contactSex)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeSex fromItem:contactItem]];

    if (contactItem.contactBirthdate)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeBirthdate fromItem:contactItem]];

#ifdef OLD_FILE_LINK
    if (contactItem.contactImageLink)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeImageLink fromItem:contactItem]];
#endif
    
    if (contactItem.netFile.netPath)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeImageLink fromItem:contactItem]];
    
    if (contactItem.contactLanguage)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeLanguage fromItem:contactItem]];
    
    if (contactItem.contactTimeZone)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeTimeZone fromItem:contactItem]];
    
    if (contactItem.contactAbout)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeAbout fromItem:contactItem]];
    
    if (contactItem.contactWebSite)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeWebsite fromItem:contactItem]];
    
    if (contactItem.contactAddress.postCode)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypePostCode fromItem:contactItem]];
    
    if (contactItem.contactAddress.street)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeStreet fromItem:contactItem]];
    
    if (contactItem.contactAddress.house)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeHose fromItem:contactItem]];
    
    if (contactItem.contactAddress.country)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeCountry fromItem:contactItem]];
    
    if (contactItem.contactAddress.city)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeCyty fromItem:contactItem]];
    
    if (contactItem.contactAddress.appartament)
        [arrayFields addObject:[self fieldFromContact:SCFieldTypeAppartament fromItem:contactItem]];

//    if (contactItem.contactEmail)
//        [arrayFields addObject:[self fieldFromContact:SCFieldTypeEmail fromItem:contactItem]];
//
//    if (contactItem.contactPhone)
//        [arrayFields addObject:[self fieldFromContact:SCFieldTypePhone fromItem:contactItem]];

    return (NSArray*) arrayFields;
}
-(Field*) afieldFromArray:(NSArray*) fields byType:(SCFieldType) fieldType
{
    for (Field *field in fields)
    {
        if (field.type == [self scFieldTypeToFieldType:fieldType])
        {
            return field;
        }
    }
    return nil;
}

-(NSString*) fieldValueFromArray:(NSArray*) fields byType:(SCFieldType) fieldType
{
    FieldType checkedField = [self scFieldTypeToFieldType:fieldType];
    for (Field *field in fields)
    {
        
        if (field.type == checkedField)
        {
            if (field.hasValue)
                return field.value;
        }
    }
    return nil;
}

-(void) contactFromFields:(SCContactItem*)contactItem fields:(NSArray*)fields
{
    [self contactField:contactItem fieldType:SCFieldTypeLoginName fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeLoginName]];
    [self contactField:contactItem fieldType:SCFieldTypeNickName fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeNickName]];
    [self contactField:contactItem fieldType:SCFieldTypeFirstName fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeFirstName]];
    [self contactField:contactItem fieldType:SCFieldTypeLastName fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeLastName]];
    [self contactField:contactItem fieldType:SCFieldTypeSex fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeSex]];
    [self contactField:contactItem fieldType:SCFieldTypeBirthdate fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeBirthdate]];
    [self contactField:contactItem fieldType:SCFieldTypeImageLink fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeImageLink]];
    [self contactField:contactItem fieldType:SCFieldTypeLanguage fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeLanguage]];
    [self contactField:contactItem fieldType:SCFieldTypeTimeZone fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeTimeZone]];
    [self contactField:contactItem fieldType:SCFieldTypeCountry fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeCountry]];
    [self contactField:contactItem fieldType:SCFieldTypeAbout fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeAbout]];
    [self contactField:contactItem fieldType:SCFieldTypeWebsite fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeWebsite]];
    [self contactField:contactItem fieldType:SCFieldTypeStreet fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeStreet]];
    [self contactField:contactItem fieldType:SCFieldTypeCyty fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeCyty]];
    [self contactField:contactItem fieldType:SCFieldTypePostCode fromValue:[self fieldValueFromArray:fields byType:SCFieldTypePostCode]];
    [self contactField:contactItem fieldType:SCFieldTypeAppartament fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeAppartament]];
    [self contactField:contactItem fieldType:SCFieldTypeHose fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeHose]];
    [self contactField:contactItem fieldType:SCFieldTypeImageid fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeImageid]];
    [self contactField:contactItem fieldType:SCFieldTypePhone fromValue:[self fieldValueFromArray:fields byType:SCFieldTypePhone]];
    [self contactField:contactItem fieldType:SCFieldTypeEmail fromValue:[self fieldValueFromArray:fields byType:SCFieldTypeEmail]];
    
}

-(void) contactField:(SCContactItem*) contactItem fieldType:(SCFieldType) scFieldType fromValue:(id)value
{
//    if(value)
    {
        switch (scFieldType)
        {
            case SCFieldTypeLoginName:contactItem.contactLoginName = value;break;
            case SCFieldTypeNickName:contactItem.contactNikName = value;break;
            case SCFieldTypeFirstName:contactItem.contactFirstName = value;break;
            case SCFieldTypeLastName:contactItem.contactLastName = value;break;
            case SCFieldTypeSex:contactItem.contactSex = value;break;
            case SCFieldTypeBirthdate:
            {
                if([value isKindOfClass:[NSDate class]])
                {
                    contactItem.contactBirthdate = value;
                }
                else if([value isKindOfClass:[NSString class]])
                {
                    if(!value || [value length] < 1)
                        contactItem.contactBirthdate = nil;
                    else
                    {
                        SInt64 longDate = [value longLongValue];
                        contactItem.contactBirthdate = [NSDate dateWithTimeIntervalSince1970:(longDate/1000)];
                    }
                }
            }break;
            case SCFieldTypeImageLink:
                [contactItem.netFile setImageNetPath:value];
                break;
            case SCFieldTypeLanguage:contactItem.contactLanguage = value;break;
            case SCFieldTypeTimeZone:contactItem.contactTimeZone = value;break;
            case SCFieldTypeCountry:contactItem.contactAddress.country = value;break;
            case SCFieldTypeAbout:contactItem.contactAbout = value;break;
            case SCFieldTypeWebsite:contactItem.contactWebSite = value;break;
            case SCFieldTypeStreet:contactItem.contactAddress.street = value;break;
            case SCFieldTypeCyty:contactItem.contactAddress.city = value;break;
            case SCFieldTypePostCode:contactItem.contactAddress.postCode = value;break;
            case SCFieldTypeAppartament:contactItem.contactAddress.appartament = value;break;
            case SCFieldTypeHose:contactItem.contactAddress.house = value;break;
            case SCFieldTypeImageid:
                [contactItem.netFile setImageNetPath:value];
                break;
            case SCFieldTypePhone:contactItem.contactPhone = value;break;
            case SCFieldTypeEmail:contactItem.contactEmail = value;break;
                //        case SCFieldTypeAuths:[fieldBuilder setValue:contactItem.userAuths];break;
                
            default:
                break;
        }
    }
// Отладка
//    else
    if(!value)
        FTLog(@"%@ empty",[self scFieldTypePrint:scFieldType]);
}


#pragma mark - Auth
-(AuthType) theAuth:(SCUserAuthItemType) type
{
    switch (type) {
        case SCUserAuthItemTypePhone: return AuthTypePhoneAuth;break;
        case SCUserAuthItemTypeEmail: return AuthTypeEmailAuth;break;
        case SCUserAuthItemTypeFacebook: return AuthTypeFacebookAuth;break;
        default:return AuthTypePhoneAuth;break;
    }
}

-(SCUserAuthItemType) theUserAuth:(AuthType) type
{
    switch (type) {
        case  AuthTypePhoneAuth: return SCUserAuthItemTypePhone;break;
        case AuthTypeEmailAuth: return SCUserAuthItemTypeEmail;break;
        case AuthTypeFacebookAuth: return SCUserAuthItemTypeFacebook;break;
        default:return SCUserAuthItemTypePhone;break;
    }
}

#pragma mark - CharRooms
-(ChatRoomType) SCChatRoomTypeToChatRoomType:(SCChatType) scChatType
{
    switch (scChatType) {
        case SCChatTypePrivate:return ChatRoomTypePrivateRoom;break;
        case SCChatTypeGroup:return ChatRoomTypeGroupRoom;break;
        case SCChatTypePublic:return ChatRoomTypePublicRoom;break;
            
        default:return ChatRoomTypePublicRoom;break;
    }
}

-(SCChatType) ChatRoomTypeToSCChatRoomType:(ChatRoomType) chatType
{
    switch (chatType) {
        case ChatRoomTypePrivateRoom:return SCChatTypePrivate;break;
        case ChatRoomTypeGroupRoom:return SCChatTypeGroup;break;
        case ChatRoomTypePublicRoom:return SCChatTypePublic;break;
            
        default:return SCChatTypePublic;break;
    }
}

-(NSString*) scFieldTypePrint:(SCFieldType) scFieldType
{
    switch (scFieldType) {
        case SCFieldTypeLoginName:return @"SCFieldTypeLoginName";break;
        case SCFieldTypeNickName:return @"SCFieldTypeNickName";break;
        case SCFieldTypeFirstName:return @"SCFieldTypeFirstName";break;
        case SCFieldTypeLastName:return @"SCFieldTypeLastName";break;
        case SCFieldTypeSex:return @"SCFieldTypeSex";break;
        case SCFieldTypeBirthdate:return @"SCFieldTypeBirthdate";break;
        case SCFieldTypeImageLink:return @"SCFieldTypeImageLink";break;
        case SCFieldTypeLanguage:return @"SCFieldTypeLanguage";break;
        case SCFieldTypeTimeZone:return @"SCFieldTypeTimeZone";break;
        case SCFieldTypeCountry:return @"SCFieldTypeCountry";break;
        case SCFieldTypeAbout:return @"SCFieldTypeAbout";break;
        case SCFieldTypeWebsite:return @"SCFieldTypeWebsite";break;
        case SCFieldTypeStreet:return @"SCFieldTypeStreet";break;
        case SCFieldTypeCyty:return @"SCFieldTypeCyty";break;
        case SCFieldTypePostCode:return @"SCFieldTypePostCode";break;
        case SCFieldTypeAppartament:return @"SCFieldTypeAppartament";break;
        case SCFieldTypeHose:return @"SCFieldTypeHose";break;
        case SCFieldTypeImageid:return @"SCFieldTypeImageid";break;
        case SCFieldTypeAuths:return @"SCFieldTypeAuths";break;
            
//        case SCFieldTypePhone:return @"SCFieldTypePhone";break;
//        case SCFieldTypeEmail:return @"SCFieldTypeEmail";break;
        default:return @"";break;
    }
}


@end
