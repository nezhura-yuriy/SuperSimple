//
//  SCSkinManager.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSkinManager.h"

@implementation SCSkinManager
{
    NSString* _skinsPath;
    NSString* _curSkinPath;
    NSDictionary* _currentSkinDict;
    NSDictionary* _skinColorDict;
    NSInteger _currentSkinIndex;
    NSBundle* currentBundle;
}

-(id) init
{
    self = [super init];
    if( self )
    {
        [self _init];
        
        NSString* instaledSkinsFile = [_skinsPath stringByAppendingPathComponent:@"skins.plist"];
        NSDictionary* instaledSkins = [NSMutableDictionary dictionaryWithContentsOfFile:instaledSkinsFile];
        NSString* curSkinPath = [[_skinsPath stringByAppendingPathComponent:[instaledSkins objectForKey:[[instaledSkins allKeys] objectAtIndex:0]]]stringByAppendingPathComponent:@"skinData.plist"];
        _currentSkinDict = [NSDictionary dictionaryWithContentsOfFile:curSkinPath];
        

    }
    return self;
}

-(id) initWithSkinName:(NSString*) skinName
{
    self = [super init];
    if( self )
    {
        [self _init];
        [self selectSkinName:skinName];
    }
    return self;
}

-(void) dealloc
{
    
}

-(void) _init
{
    NSMutableArray* instaledSkins;
    _skinColorDict = [[NSDictionary alloc] init];

    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *myPath    = [myPathList  objectAtIndex:0];
    
    NSError *error = NULL;
    _skinsPath = [myPath stringByAppendingPathComponent:@"/Skins"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:_skinsPath]) {
        BOOL didCreate = [[NSFileManager defaultManager] createDirectoryAtPath:_skinsPath
                                                   withIntermediateDirectories:YES
                                                                    attributes:NULL
                                                                         error:&error];
        if (didCreate)
            FTLog(@"%@", _skinsPath);
        NSAssert(didCreate, @"can't create cache Chat directory");
    }
    NSString* instaledSkinsFile = [_skinsPath stringByAppendingPathComponent:@"skins.plist"];

    if(![[NSFileManager defaultManager] fileExistsAtPath:instaledSkinsFile])
    {
        instaledSkins =[[NSMutableArray alloc] init];
    }
    else
    {
        instaledSkins = [NSMutableArray arrayWithContentsOfFile:instaledSkinsFile];
        [instaledSkins removeAllObjects];
    }
    
    NSArray* skinInBundle = [[NSArray alloc] initWithObjects:@"defaultSkinLight",@"defaultSkinNight",nil];
    for(NSString* skinBundleName in skinInBundle)
    {
        NSString* path = [[NSBundle mainBundle] pathForResource:skinBundleName ofType:@"bundle"] ;
        
        NSDictionary* _storedDict = [NSDictionary dictionaryWithContentsOfFile:[path stringByAppendingPathComponent:@"skinData.plist"]];
        
        NSString* newPath = [_skinsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.bundle",skinBundleName]];
        
//#ifdef DEBUG
        if([[NSFileManager defaultManager] fileExistsAtPath:newPath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:newPath error:&error];
        }
//#endif
        if(![[NSFileManager defaultManager] fileExistsAtPath:newPath])
        {
            BOOL didCopy = [[NSFileManager defaultManager] copyItemAtPath:path
                                                                   toPath:newPath
                                                                    error:&error];
            if (didCopy)
            {
                FTLog(@"copy %@ to %@",path, newPath);
                BOOL needAdd = YES;
                for(NSDictionary* item in instaledSkins)
                {
                    if([[item allKeys] containsObject:@"SkinBundle"] && [[item objectForKey:@"SkinBundle"] isEqualToString:[NSString stringWithFormat:@"%@",skinBundleName]])
                    {
                        needAdd = NO;
                    }
                }
                
                if(needAdd)
                {
                    [instaledSkins addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                        [_storedDict objectForKey:@"Name"], @"Name",
                 [_storedDict objectForKey:@"Description"], @"Description",
                                                   newPath, @"SkinPath" ,
   [NSString stringWithFormat:@"%@.bundle",skinBundleName], @"SkinBundle", nil]];
                }
            }
        }
    }

    [instaledSkins writeToFile:instaledSkinsFile atomically:YES];
    
}

-(NSArray*) getInstalledSkins
{
    NSString* instaledSkinsFile = [_skinsPath stringByAppendingPathComponent:@"skins.plist"];
    NSArray* instaledSkins = [NSArray arrayWithContentsOfFile:instaledSkinsFile];
    
    return instaledSkins;
}

-(NSDictionary*) getCurrentSkin
{
    NSString* instaledSkinsFile = [_skinsPath stringByAppendingPathComponent:@"skins.plist"];
    NSArray* instaledSkins = [NSArray arrayWithContentsOfFile:instaledSkinsFile];
    return [instaledSkins objectAtIndex:_currentSkinIndex];
}

-(NSDictionary*) getCurrentSkinData
{
    NSString* instaledSkinsFile = [_skinsPath stringByAppendingPathComponent:@"skins.plist"];
    NSArray* instaledSkins = [NSArray arrayWithContentsOfFile:instaledSkinsFile];
    NSString* skinPath = [[_skinsPath stringByAppendingPathComponent:[[instaledSkins objectAtIndex:_currentSkinIndex] objectForKey:@"SkinBundle"]] stringByAppendingPathComponent:@"skinData.plist"];
    //[[_skinsPath stringByAppendingPathComponent:[[instaledSkins objectAtIndex:_currentSkinIndex] objectForKey:@"Name"]] stringByAppendingPathComponent:@"skinData.plist"];
    return [NSDictionary dictionaryWithContentsOfFile:skinPath];
}

-(NSDictionary*) getCurrentSkinDataAt:(NSInteger) skinIndex
{
    NSString* instaledSkinsFile = [_skinsPath stringByAppendingPathComponent:@"skins.plist"];
    NSArray* instaledSkins = [NSArray arrayWithContentsOfFile:instaledSkinsFile];
    NSString* skinPath = [[_skinsPath stringByAppendingPathComponent:[[instaledSkins objectAtIndex:skinIndex] objectForKey:@"SkinBundle"]] stringByAppendingPathComponent:@"skinData.plist"];
    return [NSDictionary dictionaryWithContentsOfFile:skinPath];
}


-(NSInteger) getCurrentSkinIndex
{
    return _currentSkinIndex;
}

-(NSString*) getCurrentSkinName
{
    return [[self getCurrentSkin] objectForKey:@"Name"];
}
-(NSArray*) getColorArray
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"skinConfig" ofType:@"plist"];
    NSDictionary* _configDict = [NSDictionary dictionaryWithContentsOfFile:path];

    return [_configDict objectForKey:@"Colors"];
}

-(void) selectSkinName:(NSString*) skinName
{
    NSString* instaledSkinsFile = [_skinsPath stringByAppendingPathComponent:@"skins.plist"];
    NSArray* instaledSkins = [NSArray arrayWithContentsOfFile:instaledSkinsFile];
    
    _currentSkinIndex = 0;
    for(NSDictionary* skin in instaledSkins)
    {
        if([[skin objectForKey:@"Name"] isEqualToString:skinName])
        {
            _currentSkinIndex = [instaledSkins indexOfObject:skin];
            break;
        }
    }

    
    
    _curSkinPath = [_skinsPath stringByAppendingPathComponent:[[instaledSkins objectAtIndex:_currentSkinIndex] objectForKey:@"SkinBundle"]];
    
    NSString* currentSkinDictPath = [_curSkinPath stringByAppendingPathComponent:@"skinData.plist"];

    if([[NSFileManager defaultManager] fileExistsAtPath:currentSkinDictPath])
        FTLog(@"EXIST \n%@",currentSkinDictPath);

    _currentSkinDict = [NSDictionary dictionaryWithContentsOfFile:currentSkinDictPath];
    _skinColorDict = [[NSDictionary alloc] initWithDictionary:[self toColorDictionary:[_currentSkinDict objectForKey:@"Colors"]]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CHANGE_SKIN_NAME object:nil];
}
#pragma mark - converts
-(UIColor*) colorFromHex:(NSString*) hexString
{
    unsigned hexint = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&hexint];
    return [UIColor colorWithRed:((float)((hexint & 0xFF0000) >> 16))/255.0
                    green:((float)((hexint & 0x00FF00) >>  8))/255.0
                     blue:((float)((hexint & 0x0000FF) >>  0))/255.0
                    alpha:1.0];
}

-(NSDictionary*) toColorDictionary:(NSDictionary*) dictionary
{
    NSMutableDictionary* outDictionary = [[NSMutableDictionary alloc] init];
    for(NSString* colorKey in [dictionary allKeys])
    {
        NSDictionary* dictColor = [dictionary objectForKey:colorKey];
        UIColor* theColor;
        NSArray* colorDataAr = [dictColor allKeys];
        if([colorDataAr containsObject:@"R"] &&
           [colorDataAr containsObject:@"G"] &&
           [colorDataAr containsObject:@"B"] &&
           [colorDataAr containsObject:@"A"] )
 // color in RGBA
        {
            if([[dictColor objectForKey:@"R"] floatValue] <=1 &&
               [[dictColor objectForKey:@"G"] floatValue] <=1 &&
               [[dictColor objectForKey:@"B"] floatValue] <=1 &&
               [[dictColor objectForKey:@"A"] floatValue] <=1 )
// is normalized RGB
            {
                theColor = [UIColor colorWithRed:[[dictColor objectForKey:@"R"] floatValue]
                                           green:[[dictColor objectForKey:@"G"] floatValue]
                                            blue:[[dictColor objectForKey:@"B"] floatValue]
                                           alpha:[[dictColor objectForKey:@"A"] floatValue]];
            }
            else
            {
                theColor = [UIColor colorWithRed:[[dictColor objectForKey:@"R"] floatValue]/255.0
                                           green:[[dictColor objectForKey:@"G"] floatValue]/255.0
                                            blue:[[dictColor objectForKey:@"B"] floatValue]/255.0
                                           alpha:[[dictColor objectForKey:@"A"] floatValue]];
            }
        }
        else if ([colorDataAr containsObject:@"colorHEX"])
// color in HEX
        {
            theColor = [self colorFromHex:[dictColor objectForKey:@"colorHEX"]];
        }
        
        [outDictionary setObject:theColor forKey:colorKey];
    }
    return outDictionary;
}

#pragma mark OUT DATA
-(UIColor*) getColorForKey:(NSString*) key
{
    if([[_skinColorDict allKeys] containsObject:key])
        return (UIColor*)[_skinColorDict objectForKey:key];
    else
        return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    
    /* old code
    __weak NSDictionary* colors = [_currentSkinDict objectForKey:@"Colors"];
    if([[colors allKeys] containsObject:key])
    {
        __weak NSDictionary* keyColor = [colors objectForKey:key];
        return [UIColor colorWithRed:[[keyColor objectForKey:@"R"] floatValue]
                               green:[[keyColor objectForKey:@"G"] floatValue]
                                blue:[[keyColor objectForKey:@"B"] floatValue]
                               alpha:[[keyColor objectForKey:@"A"] floatValue]];
    }
    return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    */
}

-(UIColor*) getColorForKeyX:(NSString*) key
{
    if([[_skinColorDict allKeys] containsObject:key])
    {
        return (UIColor*)[_skinColorDict objectForKey:key];
    }
    else
    {
        return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    }

    /* old code
    __weak NSDictionary* colors = [_currentSkinDict objectForKey:@"Colors"];
    if([[colors allKeys] containsObject:key])
    {
        __weak NSDictionary* keyColor = colors[key];
        return [UIColor colorWithRed: (float) [keyColor[@"R"] floatValue]/256
                               green: (float) [keyColor[@"G"] floatValue]/256
                                blue: (float) [keyColor[@"B"] floatValue]/256
                               alpha: (float) [keyColor[@"A"] floatValue]];
    }
    return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    */
}

-(BOOL) getBoolForKey:(NSString*) key
{
    BOOL ret = NO;
    NSDictionary* skinValues = [_currentSkinDict objectForKey:@"Values"];
    if([[skinValues allKeys] containsObject:key])
        ret = [[skinValues objectForKey:key] boolValue];
    return ret;
}

-(UIImage*) getImageForKey:(NSString*) key
{
    if([UIScreen mainScreen].scale == 3)
    {
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/Images/%@@3x.png",_curSkinPath,key]];
    }
    else if([UIScreen mainScreen].scale == 2)
    {
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/Images/%@@2x.png",_curSkinPath,key]];
    }
    else
    {
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/Images/%@.png",_curSkinPath,key]];
    }
    

}

-(UIImage*) getImageForKeyX:(NSString*) key
{
    if([UIScreen mainScreen].scale == 3)
    {
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/Images/1-right/%@@3x.png",_curSkinPath,key]];
    }
    else if([UIScreen mainScreen].scale == 2)
    {
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/Images/1-right/%@@2x.png",_curSkinPath,key]];
    }
    else
    {
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/Images/1-right/%@.png",_curSkinPath,key]];
    }
}

@end
