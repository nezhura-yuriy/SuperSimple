//
//  SCChatListDetailView.m
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 03.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatListDetailView.h"
#include "SCSettings.h"
#import "SCMessageBodyItem.h"
#import "SCMessageBodys.h"
#import "SCNetFile.h"
#import "SCChatCellBodyItem.h"

#import "SCImageViewWithIndicator.h"


#define MARGIN 5
#define SIZE_TEXT 23
#define SIZE_AUDIO 32
#define SIZE_PHOTO 80

#define TEXT_FONT_SIZE 11.0

@implementation SCChatListDetailView
{
//    NSArray* _bodyArray;
    SCMessageBodys* _messageBodys;
    SCSettings* _settings;
    
    UILabel* _labMessageLine;
    SCChatCellBodyItem* _imageAudio;
    SCChatCellBodyItem* _imageVideo;
    SCChatCellBodyItem* _imagePhoto;
    SCMessageBodyItem* _bodyItemVideo;
    SCMessageBodyItem* _bodyItemPhoto;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
//    self.layer.borderWidth = 1;
//    self.layer.borderColor = [[UIColor redColor] CGColor];
}

-(void) dealloc
{
    FTLog(@"%@ dealloc",NSStringFromClass([self class]));
    [self clear];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    [self reFrame:self.frame];
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        [self reFrame:frame];
    }
}

-(void) reFrame:(CGRect) frame
{
    CGFloat posY = 0;
    CGFloat posX = 0;
    if(_labMessageLine)
    {
        _labMessageLine.frame = CGRectMake(posX, posY, frame.size.width, _labMessageLine.frame.size.height);
        posY += _labMessageLine.frame.size.height + MARGIN;
    }
    if(_imageAudio)
    {
//        _imgAudio.frame = CGRectMake(posX, posY, SIZE_MEDIA, SIZE_MEDIA);
        _imageAudio.frame = CGRectMake(posX, posY, SIZE_AUDIO, SIZE_AUDIO);
        posX += SIZE_AUDIO + MARGIN;
    }
    if(_imageVideo)
    {
        _imageVideo.frame = CGRectMake(posX, posY, SIZE_PHOTO, SIZE_PHOTO);
        posX += SIZE_PHOTO + MARGIN;
    }
//    if(_imgAudio || _imgVideo)
//        posY += SIZE_MEDIA + MARGIN;
    
    if(_imagePhoto)
    {
        _imagePhoto.frame = CGRectMake(posX, posY, SIZE_PHOTO, SIZE_PHOTO);
    }
}

-(void) setDescription:(NSString*) chatDescription withSettings:(SCSettings*) settings
{
    _labMessageLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 100)];
    [_labMessageLine setFont:[UIFont fontWithName:FT_FontNameLight size:TEXT_FONT_SIZE]];
    _labMessageLine.textColor = SKCOLOR_ChatListMessageText;
    _labMessageLine.numberOfLines = 2;
    _labMessageLine.text = chatDescription;
    [_labMessageLine sizeToFit];
    //        _labMessageLine.layer.borderColor = [[UIColor greenColor] CGColor];
    //        _labMessageLine.layer.borderWidth = 1;
    [self addSubview:_labMessageLine];

}

-(void) setMessageBodys:(SCMessageBodys*) messageBodys withSettings:(SCSettings*) settings
{
    _settings = settings;
    _messageBodys = messageBodys;

    NSArray* items = [_messageBodys itemsByType:SCDataTypeText];
    if(items.count > 0)
    {
        SCMessageBodyItem* bodyItem = [items objectAtIndex:0];
        _labMessageLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 100)];
        [_labMessageLine setFont:[UIFont fontWithName:FT_FontNameLight size:TEXT_FONT_SIZE]];
        _labMessageLine.textColor = SKCOLOR_ChatListMessageText;
        _labMessageLine.numberOfLines = 2;
        _labMessageLine.text = (NSString*)bodyItem.data;
        [_labMessageLine sizeToFit];
//        _labMessageLine.layer.borderColor = [[UIColor greenColor] CGColor];
//        _labMessageLine.layer.borderWidth = 1;
        [self addSubview:_labMessageLine];
    }
    
    items = [_messageBodys itemsByType:SCDataTypeAudio];
    if(items.count > 0)
    {
        _imageAudio = [[SCChatCellBodyItem alloc] initWithSettings:_settings];
        _imageAudio.contentMode = UIViewContentModeScaleAspectFit;
        [_imageAudio setPreviewImage:[_settings.skinManager getImageForKeyX:@"chat-add-message-voice"]];
//        _imageAudio.layer.borderColor = [[UIColor greenColor] CGColor];
//        _imageAudio.layer.borderWidth = 1;
        [self addSubview:_imageAudio];
    }
    
    items = [_messageBodys itemsByType:SCDataTypeVideo];
    if(items.count > 0)
    {
        _bodyItemVideo = [items objectAtIndex:0];
        _imageVideo = [[SCChatCellBodyItem alloc] initWithSettings:_settings];
        _imageVideo.frame = CGRectMake(0, 0, SIZE_PHOTO, SIZE_PHOTO);
        [_imageVideo setIconType:[_settings.skinManager getImageForKeyX:@"chat-add-message-video"]];
        [_bodyItemVideo.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
             switch (type)
             {
                 case SCNetFileComletionTypeStatus:
                     [_imageVideo setStatus:[result doubleValue]];
                     break;
                     
                 case SCNetFileComletionTypeCompleet:
                 {
                     if([result isKindOfClass:[SCNetFile class]])
                     {
                         [_imageVideo setPreviewImage:[(SCNetFile*)result imageForSize:CGSizeMake(SIZE_PHOTO, SIZE_PHOTO)]];
                     }
                 }
                     break;
                     
                 case SCNetFileComletionTypeProgress:
                     [_imageVideo setProgressPercent:[result doubleValue]];
                     break;
                     
                 default:
                     FTLog(@"default case");
                     break;
             }
         }];
//        _imageVideo.layer.borderColor = [[UIColor greenColor] CGColor];
//        _imageVideo.layer.borderWidth = 1;
        [self addSubview:_imageVideo];
    }
    items = [_messageBodys itemsByType:SCDataTypePhoto];
    if(items.count > 0)
    {
        _bodyItemPhoto = [items objectAtIndex:0];
        _imagePhoto = [[SCChatCellBodyItem alloc] initWithSettings:_settings];
        [_imagePhoto setIconType:[_settings.skinManager getImageForKeyX:@"chat-add-message-photo"]];

        [_bodyItemPhoto.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result)
         {
             switch (type)
             {
                 case SCNetFileComletionTypeStatus:
                     [_imagePhoto setStatus:[result integerValue]];
                     break;
                     
                 case SCNetFileComletionTypeCompleet:
                 {
                     if([result isKindOfClass:[SCNetFile class]])
                     {
                         [_imagePhoto setPreviewImage:[(SCNetFile*)result imageForSize:CGSizeMake(SIZE_PHOTO, SIZE_PHOTO)]];
                     }
                 }
                     break;
                     
                 case SCNetFileComletionTypeProgress:
                     [_imagePhoto setProgressPercent:[result doubleValue]];
                     break;
                     
                 default:
                     break;
             }
         }];
//        _imagePhoto.layer.borderColor = [[UIColor greenColor] CGColor];
//        _imagePhoto.layer.borderWidth = 1;

        [self addSubview:_imagePhoto];

    }
}

-(void) clear
{
    if(_labMessageLine)
    {
        [_labMessageLine removeFromSuperview];
        _labMessageLine = nil;
    }
    if(_imageAudio)
    {
        [_imageAudio removeFromSuperview];
        _imageAudio = nil;
    }
    if(_imageVideo)
    {
        [_imageVideo removeFromSuperview];
        _imageVideo = nil;
        [_bodyItemVideo.netFile clearCompletion];
        _bodyItemVideo = nil;
    }
    if(_imagePhoto)
    {
        [_imagePhoto removeFromSuperview];
        _imagePhoto = nil;
        [_bodyItemPhoto.netFile clearCompletion];
        _bodyItemPhoto = nil;
    }
    _messageBodys = nil;
}
+(CGFloat) calcHeightDescription:(NSString*) chatDescription forWidth:(CGFloat) width
{
    CGFloat heightText = 0;
    
    if([chatDescription sizeWithAttributes:@{ NSFontAttributeName :[UIFont systemFontOfSize:TEXT_FONT_SIZE]}].width > width)
        heightText = 2*TEXT_FONT_SIZE;
    else
        heightText = TEXT_FONT_SIZE;
    
    return heightText + MARGIN;
}

+(CGFloat) calcHeight:(SCMessageBodys*) messageBodys forWidth:(CGFloat) width
{
    CGFloat heightText = 0;
    CGFloat heightAudio = 0;
    CGFloat heightPhotoVideo = 0;
    CGFloat heightMedia = 0;
    
    //texts
    NSArray* bodyArray = [messageBodys itemsByType:SCDataTypeText];
    if(bodyArray.count > 0)
    {
        NSString* str = @"";
        for(SCMessageBodyItem* body in bodyArray)
        {
            str = [str stringByAppendingString:(NSString*)body.data];
        }
//        CGSize a = [str sizeWithAttributes:@{ NSFontAttributeName :[UIFont systemFontOfSize:TEXT_FONT_SIZE]}];
//        FTLog(@"%@",NSStringFromCGSize(a));
        if([str sizeWithAttributes:@{ NSFontAttributeName :[UIFont systemFontOfSize:TEXT_FONT_SIZE]}].width > width)
            heightText = 2*TEXT_FONT_SIZE;
        else
            heightText = TEXT_FONT_SIZE;
    }
    if([messageBodys itemsByType:SCDataTypeAudio].count > 0 )
        heightAudio = SIZE_AUDIO;
    
    if([messageBodys itemsByType:SCDataTypePhoto].count > 0 || [messageBodys itemsByType:SCDataTypeVideo].count > 0 )
        heightPhotoVideo = SIZE_PHOTO;
    
    if(heightPhotoVideo > 0)
    {
        heightMedia = MAX(heightAudio,heightPhotoVideo) + 2*MARGIN;
    }
//    if(heightText > 0 && heightMedia > 0)
        return heightText + heightMedia + MARGIN;
//    else
//        return heightText + heightMedia;
}
@end
