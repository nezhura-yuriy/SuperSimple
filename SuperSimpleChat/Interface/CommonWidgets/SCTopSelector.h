//
//  SCTopSelector.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 26.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SCSettings;

@interface SCTopSelectorItem : UIControl


- (instancetype)initWithSettings:(SCSettings*) settings itemName:(NSString*) itemName;
-(void) setBadgeValue:(NSInteger) value;
@end


@interface SCTopSelector : UIControl

@property (nonatomic,strong) SCSettings* settings;
@property (nonatomic,assign) NSUInteger activeSegment;
- (instancetype)initWithSettings:(SCSettings*) settings;
-(void) clearItems;
-(void) addItemsWithTitles:(NSArray*) titles;
-(void) addItemWithTitle:(NSString*) title;
-(void) badgeValue:(NSInteger) value atIndex:(NSUInteger) atIndex;
@end
