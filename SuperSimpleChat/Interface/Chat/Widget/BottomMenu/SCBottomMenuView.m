//
//  SCPopSendMenuView.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 15.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCBottomMenuView.h"
#import "SCSettings.h"
#import "SCMessageBodyItem.h"
#import "SCMediaCapture.h"
#import "SCMediaUtils.h"

//#import "SCChatPhotoMenu.h"
#import "SCBottomPhotoDetailMenuView.h"

//#import "SCChatAudioMenu.h"
#import "SCBottomAudioDetailMenuView.h"
#import "SCBottomAudioAdditionMenuView.h"

#import "SCBottomVideoDetailMenuView.h"

#import "SCBottomDestroyMenuView.h"

#import "SCChatListView.h"

#define MARGIN 5
#define BUTTON_WIDHT 88
#define BUTTON_HEIGHT 44
#define BOTTON_PANEL_HEIGHT 44
#define SKCOLOR_TabBarSelBg [UIColor lightGrayColor]

typedef enum : NSUInteger {
    SCBottomMenuItemTagNone = 0,
    SCBottomMenuItemTagPhoto = 1,
    SCBottomMenuItemTagAudio = 2,
    SCBottomMenuItemTagVideo = 3,
    SCBottomMenuItemTagUser = 4,
    SCBottomMenuItemTagDestroy = 5,
    SCBottomMenuItemTagList = 6
} SCBottomMenuItemTag;


@implementation SCBottomMenuView
{
    SCSettings* _settings;
    CGFloat _detailViewSize;
    
    UIView* _viewBtns;
    SCBottomMenuItem *_btnPhoto;
    SCBottomMenuItem *_btnAudio;
    SCBottomMenuItem *_btnVideo;
    SCBottomMenuItem *_btnUser;
    SCBottomMenuItem *_btnDestroy;
    SCBottomMenuItem *_btnList;
    
    SCChatBottomsView* _detailView;
//    __block void (^afterSelect) (SCPopChatMenuViewItem selectItem);
}


-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    _settings = settings;
    if(self)
    {
        self.backgroundColor = SKCOLOR_TabBarBg;
        _detailViewSize = 0;
        
        _viewBtns = [[UIView alloc] init];
        [self addSubview:_viewBtns];
        
        
    }
    return self;
}

-(void) setChatType:(SCChatType)chatType
{
    _chatType = chatType;
    
    _btnPhoto = [[SCBottomMenuItem alloc] init];
    [_btnPhoto setImage:[_settings.skinManager getImageForKeyX:@"chat-add-message-photo"]];
    _btnPhoto.tag = SCBottomMenuItemTagPhoto;
    _btnPhoto.delegate = self;
    [_viewBtns addSubview:_btnPhoto];
    
    _btnAudio = [[SCBottomMenuItem alloc] init];
    _btnAudio.tag = SCBottomMenuItemTagAudio;
    [_btnAudio setImage:[_settings.skinManager getImageForKeyX:@"chat-add-message-voice"]];
    _btnAudio.delegate = self;
    [_viewBtns addSubview:_btnAudio];
    
    _btnVideo = [[SCBottomMenuItem alloc] init];
    _btnVideo.tag = SCBottomMenuItemTagVideo;
    [_btnVideo setImage:[_settings.skinManager getImageForKeyX:@"chat-add-message-video"]];
    _btnVideo.delegate = self;
    [_viewBtns addSubview:_btnVideo];
    
    _btnUser = [[SCBottomMenuItem alloc] init];
    _btnUser.tag = SCBottomMenuItemTagUser;
    [_btnUser setImage:[_settings.skinManager getImageForKeyX:@"tabbar-contacts"]];
    _btnUser.delegate = self;
    [_viewBtns addSubview:_btnUser];
    if(_chatType == SCChatTypePrivate)
    {
        _btnUser.hidden = YES;
    }
    
    _btnDestroy = [[SCBottomMenuItem alloc] init];
    _btnDestroy.tag = SCBottomMenuItemTagDestroy;
    [_btnDestroy setImage:[_settings.skinManager getImageForKeyX:@"chat-add-message-bomb"]];
    _btnDestroy.delegate = self;
    [_viewBtns addSubview:_btnDestroy];
    
    _btnList = [[SCBottomMenuItem alloc] init];
    _btnList.tag = SCBottomMenuItemTagList;
    [_btnList setImage:[_settings.skinManager getImageForKeyX:@"tabbar-lists"]];
    _btnList.delegate = self;
    [_viewBtns addSubview:_btnList];
}

-(void) dealloc
{
    FTLog(@"SCChatBottomsView : dealoc");
    _messageBodys.delegate = nil;
}

/*
-(void) setAfterSelect:(void(^)(SCPopChatMenuViewItem selectItem)) completionBlock
{
    afterSelect = [completionBlock copy];
}
*/

-(void) layoutSubviews
{
    [super layoutSubviews];
}


-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if( !CGRectEqualToRect(frame, CGRectZero) )
    {
        NSInteger elemCount = 5;
        if(_chatType != SCChatTypePrivate)
            elemCount++;
        CGFloat wX = self.frame.size.width/elemCount;
        
        _viewBtns.frame = CGRectMake(0, self.frame.size.height - BOTTON_PANEL_HEIGHT, self.frame.size.width,BOTTON_PANEL_HEIGHT);
        NSInteger i = 0;
        _btnPhoto.frame = CGRectMake(i*wX, 0, wX, BOTTON_PANEL_HEIGHT);
        i++;
        _btnAudio.frame = CGRectMake(i*wX, 0, wX, BOTTON_PANEL_HEIGHT);
        i++;
        _btnVideo.frame = CGRectMake(i*wX, 0, wX, BOTTON_PANEL_HEIGHT);
        i++;
        if(_chatType != SCChatTypePrivate)
        {
            _btnUser.frame = CGRectMake(i*wX, 0, wX, BOTTON_PANEL_HEIGHT);
            i++;
        }
        _btnDestroy.frame = CGRectMake(i*wX, 0, wX, BOTTON_PANEL_HEIGHT);
        i++;
        _btnList.frame = CGRectMake(i*wX, 0, wX, BOTTON_PANEL_HEIGHT);
        
        if(_detailView)
        {
            _detailView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - BOTTON_PANEL_HEIGHT);
        }
    }
}

-(void) setMessageBodys:(SCMessageBodys *)messageBodys
{
    _messageBodys = messageBodys;
    _messageBodys.delegate = self;
    [self updateCounters];
}

-(void) updateCounters
{
    [_btnPhoto setBadge:[_messageBodys countOfType:SCDataTypePhoto]];
    [_btnAudio setBadge:[_messageBodys countOfType:SCDataTypeAudio]];
    [_btnVideo setBadge:[_messageBodys countOfType:SCDataTypeVideo]];
}


-(CGSize) sizeView
{
    if(_detailView && _detailViewSize > 0)
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width, BOTTON_PANEL_HEIGHT +[_detailView sizeView].height);
    else
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width, BOTTON_PANEL_HEIGHT + _detailViewSize);
}

-(void) tapAction:(NSInteger) tag
{
    switch (tag)
    {
        case SCBottomMenuItemTagPhoto:
        {
//            _detailViewSize = 44;
            if(!_detailView)
            {
                // открываем новое вью
                if([_messageBodys countOfType:SCDataTypePhoto]>0)
                {
                    [self showSubMenuPhoto];
//                    afterSelect(SCPopChatMenuViewItemSettings);
                    if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                        [_delegate selectItem:SCPopChatMenuViewItemSettings];
                }
                else
                {
                    if([SCMediaUtils haveCamera])
                    {
//                        afterSelect(SCPopChatMenuViewItemShowCamera);
                        [self showCameraView:SCCaptureModePhoto];
                    }
                    else
                    {
//                        afterSelect(SCPopChatMenuViewItemShowLibrary);
                        if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                            [_delegate selectItem:SCPopChatMenuViewItemShowLibrary];
                    }
                }
            }
            else
            {
                // закрывем открытую
                [self hideSubMenu];
//                afterSelect(SCPopChatMenuViewItemSettings);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemSettings];
            }
        }
            break;
            
        case SCBottomMenuItemTagAudio:
        {
            if(!_detailView)
            {
                // открываем новое вью
                if([_messageBodys countOfType:SCDataTypeAudio]>0)
                {
                    [self showSubMenuAudio];
//                    afterSelect(SCPopChatMenuViewItemSettings);
                    if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                        [_delegate selectItem:SCPopChatMenuViewItemSettings];
                }
                else
                {
                    [self showSubMenuAudioRecorder];
//                    afterSelect(SCPopChatMenuViewItemSettings);
                    if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                        [_delegate selectItem:SCPopChatMenuViewItemSettings];
//                    afterSelect(SCPopChatMenuViewItemAudio);
                }
            }
            else
            {
                // закрывем открытую
                [self hideSubMenu];
//                afterSelect(SCPopChatMenuViewItemSettings);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemSettings];
            }
        }
            break;
            
        case SCBottomMenuItemTagVideo:
        {
            if(!_detailView)
            {
                // открываем новое вью
                if([_messageBodys countOfType:SCDataTypeVideo]>0)
                {
                    [self showSubMenuVideo];
//                    afterSelect(SCPopChatMenuViewItemSettings);
                    if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                        [_delegate selectItem:SCPopChatMenuViewItemSettings];
                }
                else
                    if([SCMediaUtils haveCamera])
                    {
//                        afterSelect(SCPopChatMenuViewItemShowCamera);
                        [self showCameraView:SCCaptureModeVideo];
                    }
                    else
                    {
//                        afterSelect(SCPopChatMenuViewItemShowLibrary);
                        if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                            [_delegate selectItem:SCPopChatMenuViewItemShowLibrary];
                    }
            }
            else
            {
                // закрывем открытую
                [self hideSubMenu];
//                afterSelect(SCPopChatMenuViewItemSettings);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemSettings];
            }
        }
            break;
            
        case SCBottomMenuItemTagUser:
        {
            _detailViewSize = 44;
//            afterSelect(SCPopChatMenuViewItemUser);
            if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                [_delegate selectItem:SCPopChatMenuViewItemUser];
        }
            break;
            
        case SCBottomMenuItemTagDestroy:
        {
            if(!_detailView)
            {
                // открываем новое вью
                [self showMenuDestroy];
//                _detailView = [[SCBottomDestroyMenuView alloc] initWithSettings:_settings];
//                _detailViewSize = [(SCBottomDestroyMenuView*)_detailView sizeView].height;
//                _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
//                [self addSubview:_detailView];
            }
            else if ([_detailView isKindOfClass:[SCBottomDestroyMenuView class]])
            {
                // закрывем открытую
                [self hideSubMenu];
            }
            //
//            afterSelect(SCPopChatMenuViewItemSettings);
            if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                [_delegate selectItem:SCPopChatMenuViewItemSettings];
        }
            break;
            
        case SCBottomMenuItemTagList:
        {
            if(!_detailView)
            {
                [self showSubMenuList];
            }
            else if ([_detailView isKindOfClass:[SCChatListView class]])
            {
                [self hideSubMenu];
            }
//            afterSelect(SCPopChatMenuViewItemSettings);
            if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                [_delegate selectItem:SCPopChatMenuViewItemSettings];
//            afterSelect(SCPopChatMenuViewItemList);
        }
            break;
            
        default:
            break;
    }
//    self.backgroundColor = [UIColor blackColor];
}

-(void) longPressAction:(NSInteger) tag withState:(UIGestureRecognizerState) state
{
    if(state ==  UIGestureRecognizerStateBegan)
    {
        [_settings play:SCSoundPlayMessageSending];
        switch (tag)
        {
            case SCBottomMenuItemTagPhoto:
            {
                if(!_detailView)
                {
                    [self showSubMenuPhoto];
                }
                else //if ([_detailView isKindOfClass:[SCBottomPhotoDetailMenuView class]])
                {
                    // закрывем открытую
                    [self hideSubMenu];
//                    [UIView animateWithDuration:0.25 animations:^{
//                        _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
//                    } completion:^(BOOL finished) {
//                        [_detailView removeFromSuperview];
//                        _detailView = nil;
//                    }];
                    _detailViewSize = 0;
                }
//                afterSelect(SCPopChatMenuViewItemSettings);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemSettings];

            }
                break;
                
            case SCBottomMenuItemTagAudio:
            {
                if(!_detailView)
                {
                    [self showSubMenuAudio];
                }
                else
                {
                    [self hideSubMenu];
                    _detailViewSize = 0;
                }
//                afterSelect(SCPopChatMenuViewItemSettings);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemSettings];
            }
                break;
                
            case SCBottomMenuItemTagVideo:
            {
                if(!_detailView)
                {
                    [self showSubMenuVideo];
                }
                else
                {
                    [self hideSubMenu];
                    _detailViewSize = 0;
                }
//                afterSelect(SCPopChatMenuViewItemSettings);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemSettings];
            }
                break;
                
            case SCBottomMenuItemTagUser:
            {
                
            }
                break;
                
            case SCBottomMenuItemTagDestroy:
            {
                
            }
                break;
                
            case SCBottomMenuItemTagList:
            {
                
            }
                break;
                
            default:
                break;
        }
    }
    else if (state ==  UIGestureRecognizerStateEnded)
    {
        
    }
}

-(void) markSelected:(NSUInteger) idx
{
    if(idx == SCBottomMenuItemTagNone)
    {
        _btnPhoto.backgroundColor = SKCOLOR_TabBarBg;
        _btnAudio.backgroundColor = SKCOLOR_TabBarBg;
        _btnVideo.backgroundColor = SKCOLOR_TabBarBg;
        if(_btnUser)
            _btnUser.backgroundColor = SKCOLOR_TabBarBg;
        _btnDestroy.backgroundColor = SKCOLOR_TabBarBg;
        _btnList.backgroundColor = SKCOLOR_TabBarBg;
        return;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        if(idx == SCBottomMenuItemTagPhoto)
            _btnPhoto.backgroundColor = SKCOLOR_TabBarBg;
        else
            _btnPhoto.backgroundColor = SKCOLOR_TabBarSelBg;
        
        if(idx == SCBottomMenuItemTagAudio)
            _btnAudio.backgroundColor = SKCOLOR_TabBarBg;
        else
            _btnAudio.backgroundColor = SKCOLOR_TabBarSelBg;
        
        if(idx == SCBottomMenuItemTagVideo)
            _btnVideo.backgroundColor = SKCOLOR_TabBarBg;
        else
            _btnVideo.backgroundColor = SKCOLOR_TabBarSelBg;
        
        if(_btnUser)
        {
            if(idx == SCBottomMenuItemTagUser)
                _btnUser.backgroundColor = SKCOLOR_TabBarBg;
            else
                _btnUser.backgroundColor = SKCOLOR_TabBarSelBg;
        }
        
        if(idx == SCBottomMenuItemTagDestroy)
            _btnDestroy.backgroundColor = SKCOLOR_TabBarBg;
        else
            _btnDestroy.backgroundColor = SKCOLOR_TabBarSelBg;
        
        if(idx == SCBottomMenuItemTagList)
            _btnList.backgroundColor = SKCOLOR_TabBarBg;
        else
            _btnList.backgroundColor = SKCOLOR_TabBarSelBg;
    } completion:^(BOOL finished) {
        
    }];
    
}

#pragma clang diagnostic ignored "-Warc-retain-cycles"
-(void) showSubMenuPhoto
{
    [self markSelected:SCBottomMenuItemTagPhoto];
    _detailView = [[SCBottomPhotoDetailMenuView alloc] initWithSettings:_settings];
    _detailViewSize = [_detailView sizeView].height;
    _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
    _detailView.messageBodys = _messageBodys;
    [((SCBottomPhotoDetailMenuView*)_detailView) setActionSelector:^(id selectItem) {
        if([selectItem isKindOfClass:[NSString class]])
        {
            if([selectItem isEqualToString:AddFromCameraPhoto])
            {
//                afterSelect(SCPopChatMenuViewItemShowCamera);
                [self showCameraView:SCCaptureModePhoto];
            }
            else if([selectItem isEqualToString:AddFromLibraryPhoto])
            {
//                afterSelect(SCPopChatMenuViewItemShowLibrary);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemShowLibrary];
            }

        }
        
    }];
    [self addSubview:_detailView];
}
-(void) showSubMenuAudio
{
    [self markSelected:SCBottomMenuItemTagAudio];
    _detailView = [[SCBottomAudioDetailMenuView alloc] initWithSettings:_settings];
    
    _detailViewSize = [_detailView sizeView].height;
    _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
    _detailView.messageBodys = _messageBodys;
    [((SCBottomAudioDetailMenuView*)_detailView) setActionSelector:^(id selectItem) {
        if([selectItem isKindOfClass:[NSString class]])
        {
//            afterSelect(SCPopChatMenuViewItemSettings);
            if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                [_delegate selectItem:SCPopChatMenuViewItemSettings];
//            if([selectItem isEqualToString:AddFromMicrophoneAudio])
//            {
//                afterSelect(SCPopChatMenuViewItemAudio);
//            }
//            else if([selectItem isEqualToString:AddFromZZZZAudio])
//            {
//                afterSelect(SCPopChatMenuViewItemShowLibrary);
//            }
            
        }
        
    }];
    [self addSubview:_detailView];
    
}
-(void) showSubMenuAudioRecorder
{
    [self markSelected:SCBottomMenuItemTagAudio];
    _detailView = [[SCBottomAudioAdditionMenuView alloc] initWithSettings:_settings];
    _detailViewSize = [_detailView sizeView].height;
    _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
    _detailView.messageBodys = _messageBodys;
    [((SCBottomAudioAdditionMenuView*)_detailView) setActionSelector:^(id selectItem) {
        FTLog(@"setAfterSelect");
        [self hideSubMenu];
//        afterSelect(SCPopChatMenuViewItemSettings);
        if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
            [_delegate selectItem:SCPopChatMenuViewItemSettings];
    }];
    [self addSubview:_detailView];

}

-(void) showSubMenuVideo
{
    [self markSelected:SCBottomMenuItemTagVideo];
    _detailView = [[SCBottomVideoDetailMenuView alloc] initWithSettings:_settings];
    _detailViewSize = [_detailView sizeView].height;
    _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
    _detailView.messageBodys = _messageBodys;
    [((SCBottomVideoDetailMenuView*)_detailView) setActionSelector:^(id selectItem) {
        if([selectItem isKindOfClass:[NSString class]])
        {
            if([selectItem isEqualToString:AddFromCameraVideo])
            {
//                afterSelect(SCPopChatMenuViewItemShowCamera);
                [self showCameraView:SCCaptureModeVideo];
            }
            else if([selectItem isEqualToString:AddFromLibraryVideo])
            {
//                afterSelect(SCPopChatMenuViewItemShowLibrary);
                if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
                    [_delegate selectItem:SCPopChatMenuViewItemShowLibrary];
            }
            
        }
        
    }];
    [self addSubview:_detailView];
}


-(void) showMenuDestroy
{
    [self markSelected:SCBottomMenuItemTagDestroy];

    _detailView = [[SCBottomDestroyMenuView alloc] initWithSettings:_settings];
    _detailViewSize = [(SCBottomDestroyMenuView*)_detailView sizeView].height;
    _detailView.delegate = _delegate;
    _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
    _detailView.messageBodys = _messageBodys;
    [((SCBottomVideoDetailMenuView*)_detailView) setActionSelector:^(id selectItem) {
        FTLog(@"setAfterSelect");
        [self hideSubMenu];
//        afterSelect(SCPopChatMenuViewItemSettings);
        if(_delegate && [_delegate respondsToSelector:@selector(selectItem:)])
            [_delegate selectItem:SCPopChatMenuViewItemSettings];
    }];
    [self addSubview:_detailView];
}

-(void) showSubMenuList
{
    [self markSelected:SCBottomMenuItemTagList];
//    _detailView = [[SCBottomDestroyMenuView alloc] initWithSettings:_settings];
    _detailView = [[[NSBundle mainBundle] loadNibNamed:@"SCChatListView" owner:self options:nil] lastObject];
    _detailView.settings = _settings;
//TODO:Куйня
    _detailView.delegate = _delegate;
    _detailViewSize = [(SCBottomDestroyMenuView*)_detailView sizeView].height;
    _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
    [self addSubview:_detailView];
}
-(void) hideSubMenu
{
    [UIView animateWithDuration:0.25 animations:^{
        [self markSelected:SCBottomMenuItemTagNone];
        _detailView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
    } completion:^(BOOL finished) {
        [_detailView removeFromSuperview];
        _detailView = nil;
    }];
    _detailViewSize = 0;
}

-(void) showCameraView:(SCCaptureMode) captureMode
{
    SCMediaCapture* mediaCapture =[[SCMediaCapture alloc] init];//WithSettings:_settings];
    mediaCapture.captureMode = captureMode;
    mediaCapture.delegate = self;
    [mediaCapture setSettings:_settings];
    
    CGRect scrRect = [[UIScreen mainScreen] bounds];
    mediaCapture.frame = CGRectMake(0, scrRect.size.height, scrRect.size.width, scrRect.size.height);
    [self.window addSubview:mediaCapture];
    [UIView animateWithDuration:0.25 animations:^{
        mediaCapture.frame = scrRect;
    } completion:^(BOOL finished) {
        
    }];
}
#pragma mark - @protocol SCMessageBodysDelegate <NSObject>
-(void) arrayChanged
{
    [self updateCounters];
    [_detailView updateItems];
}

#pragma mark SCMediaCaptureDelegate <NSObject>
-(void) fileCaptured:(NSString*) fileName forType:(SCDataType) type
{
    SCMessageBodyItem* bodyItem = [[SCMessageBodyItem alloc] initWithSettings:_settings];
    bodyItem.type = type;
    bodyItem.data = fileName;
//    bodyItem.netFile.localPath = fileName;
    [_messageBodys addItem:bodyItem];
}
@end
