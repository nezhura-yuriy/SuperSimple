//
//  SCAddressBookContactCell.h
//  SmartChat
//
//  Created by Alexsandr Linnik on 26.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@class SCAddressbookContact;


@protocol SCAddressBookContactCellDelegate <NSObject>

-(void) inviteContactAtIndexPath:(NSIndexPath*) indexPath;

@end

@interface SCAddressBookContactCell : UITableViewCell

@property (nonatomic, weak) id < SCAddressBookContactCellDelegate > delegate;

@property (weak, nonatomic) IBOutlet UILabel *contactName;

@property (weak, nonatomic) IBOutlet UIImageView *contactFotoView;

@property (weak, nonatomic) IBOutlet UIButton *inviteButtonOutlet;

@property (strong, nonatomic) NSIndexPath* indexPath;

-(void)fillWhithContact:(SCAddressbookContact*)contact;

- (IBAction)inviteAction:(id)sender;


@end
