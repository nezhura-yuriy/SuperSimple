//
//  SCMessageGroup.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 04.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageGroup.h"
#import "SCMessageItem.h"
#import "SCSettings.h"
#import "SCChatItem.h"

@implementation SCMessageGroup
{
    NSDateFormatter* _df;
}

-(void) _init
{
    _chatMessages = [[NSMutableArray alloc] init];
    _df = [[NSDateFormatter alloc] init];
    [_df setDateFormat:@"YYYY-mm-DD"];
}

-(void) dealloc
{
    
}

-(void) fromDictionary:(NSDictionary*) dictionary
{
//    FTLog(@"fromDictionary %@",dictionary);
//    _messagesDate = [dictionary objectForKey:@"messagesDate"];
}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[_chatItem.chatPuuid scpuuidToString] forKey:@"chatItem"];
    [dict setObject:_messagesDate forKey:@"messagesDate"];

    return dict;
}


-(SCMessageItem*) searchMessageItemByPUUID:(SCPUUID*) scPuuid
{
    NSString* puuidString = [scPuuid scpuuidToString];
    return [self searchMessageItemByPUUIDString:puuidString];
}

-(SCMessageItem*) searchMessageItemByPUUIDString:(NSString*) puuidString
{
    for(SCMessageItem* item in _chatMessages)
    {
        if([item.messageID isEqualToString:puuidString])
        {
            return item;
        }
    }
    //??    [_chatMessages addObject:newItem];
    return nil;
}

-(SCMessageItem*) newMessageItemByPUUID:(SCPUUID*) scPuuid
{
    NSString* puuidString = [scPuuid scpuuidToString];
    return [self newMessageItemByPUUIDString: puuidString];
}

-(SCMessageItem*) newMessageItemByPUUIDString:(NSString*) puuidString
{
    for(SCMessageItem* item in _chatMessages)
    {
        if([item.messageID isEqualToString:puuidString])
        {
            return item;
        }
    }
    SCMessageItem* messageItem = nil;
    messageItem = [[SCMessageItem alloc] initWithSettings:_settings];
    messageItem.chat = _chatItem;
    
    return messageItem;
}

-(SCMessageItem*) getLastMessage
{
    SCMessageItem* lastMessageItem = nil;
//    @synchronized(_chatMessages)
    {
        for(SCMessageItem* item in _chatMessages)
        {
            if(!lastMessageItem)
            {
                lastMessageItem = item;
            }
            else
            {
                NSComparisonResult result = [lastMessageItem.messageDateTime compare:item.messageDateTime];
                if(result==NSOrderedAscending)
                {
                    lastMessageItem = item;
                }
            }
        }
    }
    return lastMessageItem;
}

-(NSUInteger) countNotViewedMessages
{
    NSUInteger countMessages = 0;
    for(SCMessageItem* item in _chatMessages)
    {
//        FTLog(@"\nlastViewedMessageDate = %@\n      messageDateTime = %@",_chatItem.lastViewedMessageDate,item.messageDateTime);
        NSComparisonResult result = [_chatItem.lastViewedMessageDate compare:item.messageDateTime];
        if(result==NSOrderedAscending)
        {
            countMessages++;
        }
    }
    return countMessages;
}

-(BOOL) addMeesageItem:(SCMessageItem*) newItem
{
    for(SCMessageItem* item in _chatMessages)
    {
        if([item.messageID isEqualToString:newItem.messageID])
        {
//            FTLog(@"May be dublicate");
            return NO;
        }
    }
    [_chatMessages addObject:newItem];
    [self sortDayMessages];
    return YES;
}



#pragma mark - CACHE
-(void) loadMessages
{
    NSString* groupFile = [[_chatItem.chatPuuid scpuuidToString] stringByAppendingFormat:@"=%@",[_df stringFromDate:_messagesDate]];
    NSString* path = [_settings.cacheMessagesPath stringByAppendingPathComponent:groupFile];
//    FTLog(@"%@",path);
    NSArray *tmpArray = [NSMutableArray arrayWithContentsOfFile:path];
    for(NSDictionary *dict in tmpArray)
    {
        SCMessageItem* messageItem = nil;
        messageItem = [self searchMessageItemByPUUIDString:[dict objectForKey:@"messageID"]];
        if(!messageItem)
            messageItem = [self newMessageItemByPUUIDString:[dict objectForKey:@"messageID"]];

        [messageItem fromDictionary:dict];
        [_chatMessages addObject:messageItem];
    }
    [self sortDayMessages];
}

-(void) saveMessages
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    for(SCMessageItem* messageItem in _chatMessages)
    {
        NSDictionary* dictItem = [messageItem toDictionary];
        [tmpArray addObject:dictItem];
    }
    NSString* groupFile = [[_chatItem.chatPuuid scpuuidToString] stringByAppendingFormat:@"=%@",[_df stringFromDate:_messagesDate]];
    NSString* path = [_settings.cacheMessagesPath stringByAppendingPathComponent:groupFile];
    BOOL sucsess = [tmpArray writeToFile:path atomically:YES];
    
    if(!sucsess)
    {
        FTLog(@"Error saveCache");
    }

}

-(void) sortDayMessages
{
//    FTLog(@"%@",_chatMessages);
    [_chatMessages sortUsingComparator:^NSComparisonResult (id a, id b)
    {
         
        SCMessageItem * itemA = (SCMessageItem*)a;
        SCMessageItem* itemB =(SCMessageItem*)b;


        NSComparisonResult result = [itemA.messageDateTime compare:itemB.messageDateTime];
        return result;
//        if (result==NSOrderedAscending)
//            return NSOrderedDescending;
//        else if(result==NSOrderedDescending)
//            return NSOrderedAscending;
//        else
//            return result;
    }];
//    FTLog(@"%@",_chatMessages);
}

@end


