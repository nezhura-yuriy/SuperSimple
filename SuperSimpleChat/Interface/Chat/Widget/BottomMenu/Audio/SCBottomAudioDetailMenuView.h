//
//  SCBottomAudioDetailMenuView.h
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"
#import "SCMediaItemView.h"

@class SCMessageBodys;

@interface SCBottomAudioDetailMenuView : SCChatBottomsView <SCMediaItemViewDelegate>
{
//    SCMessageBodys* _messageBodys;
}
//@property (nonatomic,strong) SCMessageBodys* messageBodys;
@property(nonatomic,strong) NSMutableArray* imagesArray;

-(void) setActionSelector:(void(^)(id selectItem)) completionBlock;
@end
