//
//  CytiTableViewCell.h
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 18.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;

@end
