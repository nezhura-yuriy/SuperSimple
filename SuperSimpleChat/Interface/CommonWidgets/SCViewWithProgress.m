//
//  SCViewWithProgress.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 24.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCViewWithProgress.h"
#import "SCNetFile.h"


@implementation SCViewWithProgress

#define degreesToRadians(x) (((x-90.0) * M_PI / 180.0))

-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;

        [self _localInit];
    }
    return self;
}

-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    
    [self _localInit];

}

-(void) _localInit
{
    shadowLayer = [CAShapeLayer layer];
    shadowLayer.backgroundColor = [[UIColor clearColor] CGColor];
    
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] init];
    _activityIndicatorView.hidesWhenStopped = YES;
    _activityIndicatorView.backgroundColor = [UIColor clearColor];

}
-(void) dealloc
{
    
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        shadowLayer.frame = self.bounds;
        shapeLayer.frame = self.bounds;
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
}

-(void) setStatus:(SCNetFileStatus) status
{
//    FTLog(@"%@ setStatus %@",NSStringFromClass([self class]),[SCNetFile scNetFileStatusPrint:status]);
    switch (status)
    {
        case SCNetFileStatusNew:
            //            break;
        case SCNetFileStatusStart:
        case SCNetFileStatusWait:
        case SCNetFileStatusTryRepeet:
            shapeLayer.hidden = YES;
            shadowLayer.hidden = YES;
            [_activityIndicatorView startAnimating];
            break;
            
        case SCNetFileStatusProgress:
            shapeLayer.hidden = NO;
            shadowLayer.hidden = NO;
            [_activityIndicatorView stopAnimating];
            break;
            
        case SCNetFileStatusCompleet:
        case SCNetFileStatusError:
            shapeLayer.hidden = YES;
            shadowLayer.hidden = YES;
            [_activityIndicatorView stopAnimating];
            break;
            
        default: break;
    }
}

-(void) setProgressPercent:(CGFloat) persents
{
    
    UIBezierPath *pathShadow = [[UIBezierPath alloc] init];
    [pathShadow moveToPoint:CGPointMake(shadowLayer.frame.size.width*0.5,shadowLayer.frame.size.height*0.5)];
    [pathShadow addArcWithCenter:CGPointMake(shadowLayer.frame.size.width*0.5,shadowLayer.frame.size.height*0.5) radius:shadowLayer.frame.size.width*0.45 startAngle:degreesToRadians(0) endAngle:degreesToRadians(360) clockwise:YES];
    [pathShadow moveToPoint:CGPointMake(shadowLayer.frame.size.width*0.5,shadowLayer.frame.size.height*0.5)];
    shadowLayer.fillColor = [[UIColor colorWithWhite:0.1 alpha:0.4] CGColor];
    shadowLayer.path = [pathShadow CGPath];
    //    [shadowLayer setNeedsLayout];
    shadowLayer.hidden = NO;
    
    CGFloat u = persents*360;
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(shapeLayer.frame.size.width*0.5,shapeLayer.frame.size.height*0.5)];
    [path addArcWithCenter:CGPointMake(shapeLayer.frame.size.width*0.5,shapeLayer.frame.size.height*0.5) radius:shapeLayer.frame.size.width*0.40 startAngle:degreesToRadians(0) endAngle:degreesToRadians(u) clockwise:YES];
    [path moveToPoint:CGPointMake(shapeLayer.frame.size.width*0.5,shapeLayer.frame.size.height*0.5)];
    
    shapeLayer.path = [path CGPath];
    shapeLayer.lineWidth = 2.1;
    shapeLayer.fillColor = [[UIColor colorWithWhite:0.9 alpha:0.4] CGColor];
    //    [shapeLayer setNeedsLayout];
    shapeLayer.hidden = NO;
    //    [self.layer addSublayer:shapeLayer];
}


@end
