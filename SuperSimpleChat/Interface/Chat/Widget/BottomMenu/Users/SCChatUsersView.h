//
//  SCChatUsersView.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 28.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell.h>

typedef enum : NSUInteger {
    SCChatUsersViewItemAdd,
    SCChatUsersViewItemCancel,
} SCChatUsersViewItem;

@class SCChatItem;


@interface SCChatUsersView : UIView <UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>

@property (nonatomic,strong) SCSettings* settings;
@property (nonatomic,strong) SCChatItem* chatItem;
@property (nonatomic, strong) NSMutableArray* userInRoom;
@property (nonatomic, assign) BOOL inAddMode;
@property (strong, nonatomic) UITableView *tableView;


-(NSMutableArray*) apply;
@end
