//
//  CountryListViewController.m
//  Country List
//
//  Created by Pradyumna Doddala on 18/12/13.
//  Copyright (c) 2013 Pradyumna Doddala. All rights reserved.
//

#import "CountryListViewController.h"
#import "CountryListDataSource.h"
#import "CountryCell.h"
#import "SCCoutrySection.h"
#import "SCSkinManager.h"


@interface CountryListViewController ()<UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataRows;
@property (strong, nonatomic) NSOperation* currenTOperation;
@property (strong, nonatomic) NSMutableArray* sectionsArray;


@end

@implementation CountryListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        _delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    _dataRows = [[dataSource countries] mutableCopy];
    
    self.sectionsArray = [self generateSectionsFromArray:self.dataRows withFilter:self.searchBar.text];
    
//    [_tableView reloadData];
    self.title = @"Select your coutry code";
    [self settingNavigationBar];
    
//    self.searchBar.tintColor = [UIColor blackColor]; // цвет штриха
//    self.searchBar.backgroundColor = [UIColor lightGrayColor];
    self.searchBar.delegate = self;
    
    self.searchBar.barTintColor = [UIColor colorWithHue:0 saturation:0 brightness:0.949 alpha:1];// цвет ободочка
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor colorWithHue:0 saturation:0 brightness:0.949 alpha:1]];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                  [UIColor grayColor],
                                                                                                  NSForegroundColorAttributeName,
                                                                                                  nil]
                                                                                        forState:UIControlStateNormal];
 
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexColor = [UIColor grayColor];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

#pragma mark - UITableView Datasource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sectionsArray count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return [[self.sectionsArray objectAtIndex:section] sectionName];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:section];
    
    
    return [sections.itemsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    CountryCell *cell = (CountryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[CountryCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    SCCoutrySection* sections = [self.sectionsArray objectAtIndex:indexPath.section];
    
    NSDictionary* country = [sections.itemsArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [country valueForKey:kCountryName];
    cell.textLabel.font = [UIFont fontWithName:FT_FontNameLight size:cell.textLabel.font.pointSize];
    
    cell.detailTextLabel.text = [country valueForKey:kCountryCallingCode];
    cell.detailTextLabel.font = [UIFont fontWithName:FT_FontNameLight size:cell.detailTextLabel.font.pointSize];
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    NSMutableArray* array = [NSMutableArray array];
    for(SCCoutrySection* section in self.sectionsArray){
        
        [array addObject:section.sectionName];
    }
    return array;
}



- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [UIColor colorWithHue:0.41 saturation:0 brightness:0.899 alpha:1];
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
    [header.textLabel setFont:[UIFont fontWithName:FT_FontNameLight size:header.textLabel.font.pointSize]];

}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - Actions



#pragma mark - Initialization methods

- (void)settingNavigationBar {
    
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0.0, 0.0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBarTintColor:[_skinManager getColorForKeyX:@"NavigationBarBackground"]];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:[_skinManager getColorForKeyX:@"NavigationBarTitleFont"],
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
 }


- (void)backAction {
    
    [self.searchBar resignFirstResponder];

    if ([_delegate respondsToSelector:@selector(didSelectCountry:)]) {
        
        
        SCCoutrySection* sections = [self.sectionsArray objectAtIndex:[_tableView indexPathForSelectedRow].section];
        
        NSDictionary* country = [sections.itemsArray objectAtIndex:[_tableView indexPathForSelectedRow].row];
        
        [self.delegate didSelectCountry:country];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        FTLog(@"CountryListView Delegate : didSelectCountry not implemented");
    }
    
}

#pragma mark - Privet Methods

-(NSMutableArray*) generateSectionsFromArray:(NSMutableArray*) array withFilter:(NSString*) filterString {
    
    NSSortDescriptor* sortDescriptorName =[[NSSortDescriptor alloc]initWithKey:@"name" ascending:YES];
    
    [self.dataRows sortUsingDescriptors:@[sortDescriptorName]];
    
    NSMutableArray* sectionsArray = [[NSMutableArray alloc]init];
    
    NSString* currentLetter = nil;
    
    for (NSDictionary* country in array) {
        
//        if ([string rangeOfString: @" " options:NSCaseInsensitiveSearch].location == NSNotFound)
//        {
//            FTLog(@"string does not contain bla");
//        }
        
        if ([filterString length] > 0 && [[country objectForKey:@"name"] rangeOfString:filterString options:NSCaseInsensitiveSearch].location== NSNotFound) {
            continue;
        }
        
        NSString* firstLetter = [[country objectForKey:@"name"] substringToIndex:1];
        
        SCCoutrySection* section = nil;
        
        if (![currentLetter isEqualToString:firstLetter]) {
            
            section = [[SCCoutrySection alloc]init];
            section.sectionName = firstLetter;
            section.itemsArray = [NSMutableArray array];
            currentLetter = firstLetter;
            [sectionsArray addObject:section];
        }else{
            section = [sectionsArray lastObject];
        }
        [section.itemsArray addObject:country];
    }
  
    return sectionsArray;
    
}

#pragma mark UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text = nil;
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self generateSectionsInBackgroundArraByFirstName:self.dataRows withFilter:searchBar.text];
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
//    [self generateSectionsFromArray:self.dataRows withFilter:self.searchBar.text];
    
    
    [self generateSectionsInBackgroundArraByFirstName:self.dataRows withFilter:self.searchBar.text];
    
}

-(void) generateSectionsInBackgroundArraByFirstName : (NSMutableArray*) array withFilter:(NSString*) filterString {
    
    [self.currenTOperation cancel];
    
    __weak CountryListViewController* weakSelf = self;
    
    self. currenTOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSMutableArray* sectionsArray = [weakSelf generateSectionsFromArray:array withFilter:filterString];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.sectionsArray = sectionsArray;
            [self.tableView reloadData];
            
            self.currenTOperation = nil;
        });
    }];
    [self.currenTOperation start];
    
}


@end
