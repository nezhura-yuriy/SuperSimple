//
//  SCSkinsViewController.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 28.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SCSkinManager;

@interface SCSkinViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) SCSkinManager* skinManager;
@property (strong, nonatomic) IBOutlet UISegmentedControl *switcher;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end
