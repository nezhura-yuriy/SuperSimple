//
//  SCChatCellBodyItem.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 24.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCViewWithProgress.h"

@interface SCChatCellBodyItem : SCViewWithProgress

//@property(nonatomic,strong) UIImageView* previewImage;
//@property(nonatomic,strong) UIImageView* iconType;


-(void) setIconType:(UIImage*) img;
-(void) setPreviewImage:(UIImage*) img;
@end
