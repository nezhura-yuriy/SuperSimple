//
//  SCChatSwipeMenu.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 31.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SCSettings;


@interface SCChatSwipeMenu : UIView


@property(nonatomic,strong) UIButton* btnMenuDelete;



- (instancetype)initWithSettings:(SCSettings*) settings;



@end
