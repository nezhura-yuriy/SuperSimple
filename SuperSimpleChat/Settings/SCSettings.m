//
//  SCSettings.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSettings.h"
#import "SCmodelContacts.h"
#import "SCModelChats.h"
#import "SCContactItem.h"
#import "SCPBManager.h"
#import "SCModelUser.h"

#import "SCPUUID.h"
#import <AVFoundation/AVFoundation.h>

#define DEFAULT_SKIN_NAME   @"DefaultLighte"
#define NIGHT_SKIN_NAME   @"DefaultLighte"

@implementation SCSettings
{
    BOOL isFirst;
    NSArray *_soundsArray;
    AVAudioPlayer *_soundPlayer;
    BOOL _isTryRestoreConnect;
    BOOL _isLogout;
}

+ (SCSettings*) sharedSettings {
    
    static SCSettings* settings = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        settings = [[SCSettings alloc] initFromShare];
    });
    
    return settings;
}

-(id) init
{
    self = [super init];
    if(self)
    {
        NSException* myException = [NSException
                                    exceptionWithName:@"Неправильная инициализация"
                                    reason:@"Используй [SCSettings sharedSettings]"
                                    userInfo:nil];
        @throw myException;
    }
    return self;
}

-(id) initFromShare
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}

-(void) _init
{
    isFirst = YES;
    _isInBackgraund = NO;
    _isTryRestoreConnect = NO;
    _lastChatType = SCChatTypePublic;
    
    _httpManager = [[SCHTTPManager alloc]initWithSettings:self];
    
    _socketClient = [[SCWebSocketClient alloc] initWithSettings:self];
    [_socketClient addDelegate:self];
    
    _notifycationManager = [[SCNotifycationManager alloc] initWithSettings:self];
    
}



-(void) dealloc
{
    
}

-(void) settingsStart
{
    
    _modelProfile = [[SCModelProfile alloc] initWithSettings:self];
    _modelProfile.delegate = self;
    _modelProfile.protoBufManager.settings = self;
    
    //TODO: исправить
    _dateFormater = [[NSDateFormatter alloc] init];
    [_dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
    [_dateFormater setLocale:[NSLocale systemLocale]];
    [_dateFormater setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    [self loadSettings];
    
    [self loadStoredProfile];
    _skinManager = [[SCSkinManager alloc] initWithSkinName:DEFAULT_SKIN_NAME];
}

#pragma mark - private func
-(void) _initVariables
{
    
    // DefaultLighte DefaultNight
//    _skinManager = [[SCSkinManager alloc] initWithSkinName:_skinShemeName];
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *myPath    = [myPathList  objectAtIndex:0];
    
    NSString *userIdentifier = _modelProfile.userID;//[_modelProfile.contactItem.userPUUID scpuuidToString];
    NSError *error = NULL;
    
    NSString* _cachesPath = [myPath stringByAppendingPathComponent:userIdentifier];
    if (![[NSFileManager defaultManager] fileExistsAtPath:_cachesPath])
    {
        BOOL didCreate = [[NSFileManager defaultManager] createDirectoryAtPath:_cachesPath
                                                   withIntermediateDirectories:YES
                                                                    attributes:NULL
                                                                         error:&error];
        if (didCreate)
            FTLog(@"%@", _cachesPath);
        NSAssert(didCreate, @"can't create caches directory");
    }
    
    _cacheChatPath = _cachesPath;
    
    _cacheMessagesPath  = [_cachesPath stringByAppendingPathComponent:@"/cacheMessages"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:_cacheMessagesPath])
    {
        BOOL didCreate = [[NSFileManager defaultManager] createDirectoryAtPath:_cacheMessagesPath
                                                   withIntermediateDirectories:YES
                                                                    attributes:NULL
                                                                         error:&error];
        if (didCreate)
            FTLog(@"%@", _cacheMessagesPath);
        NSAssert(didCreate, @"can't create cache Messages directory");
    }
    
    _caheImagePath  = [_cachesPath stringByAppendingPathComponent:@"/cacheImages"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:_caheImagePath])
    {
        BOOL didCreate = [[NSFileManager defaultManager] createDirectoryAtPath:_caheImagePath
                                                   withIntermediateDirectories:YES
                                                                    attributes:NULL
                                                                         error:&error];
        if (didCreate)
            FTLog(@"%@", _caheImagePath);
        NSAssert(didCreate, @"can't create cache Messages directory");
    }
    
    
    _cacheContactsPath  = _cachesPath;
    
    
    
    myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    myPath    = [myPathList  objectAtIndex:0];
    
    _cacheListsPath = [[myPath stringByAppendingPathComponent:userIdentifier] stringByAppendingPathComponent:@"/userLists"];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:_cacheListsPath])
    {
        BOOL didCreate = [[NSFileManager defaultManager] createDirectoryAtPath:_cacheListsPath
                                                   withIntermediateDirectories:YES
                                                                    attributes:NULL
                                                                         error:&error];
        if (didCreate)
            FTLog(@"%@", _cacheListsPath);
        NSAssert(didCreate, @"can't create user Lists directory");
    }

    _mediaTempPath = [_cachesPath stringByAppendingPathComponent:@"/mediaTempPath"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:_mediaTempPath])
    {
        BOOL didCreate = [[NSFileManager defaultManager] createDirectoryAtPath:_mediaTempPath
                                                   withIntermediateDirectories:YES
                                                                    attributes:NULL
                                                                         error:&error];
        if (didCreate)
            FTLog(@"%@", _mediaTempPath);
        NSAssert(didCreate, @"can't create user mediaTempPath directory");
    }
    

    [_httpManager setCachePath:_caheImagePath];
    [_httpManager setTempPath:_mediaTempPath];
    
    _modelUsers = [[SCModelUser alloc] initWithSettings:self];
    _modelContacts = [[SCmodelContacts alloc] initWithSettings:self];
    _modelAddressbook = [[SCAddressbookManager alloc]initWithSettings:self];
    _modelChats = [[SCModelChats alloc] initWithSettings:self];
    
//    [self clearAllCache];
    
    [_modelUsers loadAllCached];
    [_modelChats loadCached];
    
    //_modelLists = [[SCModelLists alloc] initWithSettings:self];
    
}

-(void) _initAppSounds
{
    if (_isAppSound) {
        
        //Attantion! Order in the array must match the order in the enum SCSoundPlay
        _soundsArray = @[@"sounds_on.wav",
                         @"login.wav",
                         @"logout.wav",
                         @"message_sending.wav",
                         @"message_receive.wav"];
        
    } else {
        _soundPlayer = nil;
        _soundsArray = nil;
    }
}

-(void) _initPushNotifycation
{
    if(_isNotification)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }

    }
    else
    {
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
        [_notifycationManager clearDeviceNotifycationToken];
    }
}

#pragma mark - сохранение/востановление
-(void) loadStoredProfile
{
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* storedProfileDict;
    if([[userDefaults objectForKey:@"SERVER_TYPE"] isEqualToString:SERVER_TYPE])
    {
        storedProfileDict = [userDefaults objectForKey:@"CURRENT_PROFILE"];
        if(storedProfileDict)
        {
            FTLog(@"%@",storedProfileDict);
            [_modelProfile fromDictionary:storedProfileDict];
            isFirst = NO;
            
            [self _initVariables];
            
            [_modelProfile resroteContact];
        }
    }
}
-(void) saveStoredProfile
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* storedProfileDict = [_modelProfile toDictionary];
    
    [userDefaults setObject:storedProfileDict forKey:@"CURRENT_PROFILE"];
    [userDefaults setObject:SERVER_TYPE forKey:@"SERVER_TYPE"];
}

-(void) loadSettings
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
// skinShemeName
    NSString* skin = [userDefaults objectForKey:@"skinShemeName"];
    if([skin length] > 0)
        _skinShemeName = skin;
    else
        _skinShemeName = DEFAULT_SKIN_NAME;
    
// lastChatType
    if ([userDefaults objectForKey:@"lastChatType"])
    {
        _lastChatType = (SCChatType)[userDefaults integerForKey:@"lastChatType"];
    }
    else
    {
        _lastChatType = SCChatTypePublic; //Default
    }
    
// notification
    if ([userDefaults objectForKey:@"notification"])
    {
        _isNotification = [userDefaults boolForKey:@"notification"];
    }
    else
    {
        _isNotification = YES; //Default
    }
    
// nightmode
    if ([userDefaults objectForKey:@"nightmode"])
    {
        _isNightMode = [userDefaults boolForKey:@"nightmode"];
    }
    else
    {
        _isNightMode = NO; //Default
    }
    
// appsound
    if ([userDefaults objectForKey:@"appsound"])
    {
        _isAppSound = [userDefaults boolForKey:@"appsound"];
    }
    else
    {
        [self setIsAppSound:YES]; //Default
    }
    
    [self _initAppSounds];/*
    if (_isAppSound) {
        
        //Attantion! Order in the array must match the order in the enum SCSoundPlay
        _soundsArray = @[@"sounds_on.wav",
                         @"login.wav",
                         @"logout.wav",
                         @"message_sending.wav",
                         @"message_receive.wav"];
    }*/
    
// appvibration
    if ([userDefaults objectForKey:@"appvibration"])
    {
        _isAppVibration = [userDefaults boolForKey:@"appvibration"];
    }
    else
    {
        _isAppVibration = YES; //Default
    }
}

-(void) saveSettings
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:SERVER_TYPE forKey:@"SERVER_TYPE"];
    [userDefaults setObject:_skinShemeName forKey:@"skinShemeName"];
    [userDefaults setInteger:_lastChatType forKey:@"lastChatType"];
    
    [userDefaults setBool:_isNotification forKey:@"notification"];
    [userDefaults setBool:_isNightMode forKey:@"nightmode"];
    [userDefaults setBool:_isAppSound forKey:@"appsound"];
    [userDefaults setBool:_isAppVibration forKey:@"appvibration"];
}


-(void) netInit
{

    if([_modelProfile.sessionID length] > 0)
    {// is logined

//TODO: инициализировать если не было загрузки профайла
        if(isFirst)
            [self _initVariables];

        _socketClient.userID = _modelProfile.sessionID;
// востанавливаем сохраненные параметры. Если ошибка соединения ОЧИСТИТЬ
        
        [_socketClient connectWebSocket];

    }
    else
    {// is NOT logined
        
        
        [_delegate tryLogin];
    }
}

-(void) prepareLogout
{
    
}

-(void) logout
{
    [_modelProfile startPingServer:SCPingStatusStop];
    isFirst = YES;
    _isTryRestoreConnect = NO;

    [_modelChats clearBeforeLogout];
    [_modelUsers clearBeforeLogout];

    [_httpManager setCachePath:nil];
    [_httpManager setTempPath:nil];

    _cacheChatPath = nil;
    _cacheMessagesPath = nil;
    _caheImagePath = nil;
    _cacheContactsPath = nil;
    _cacheListsPath = nil;
    _mediaTempPath = nil;
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    if([[userDefaults objectForKey:@"SERVER_TYPE"] isEqualToString:SERVER_TYPE])
    {
        [userDefaults removeObjectForKey:@"CURRENT_PROFILE"];
        [userDefaults synchronize];
    }
}

-(void) appChangeState:(SCAppStateChange) appState
{
    FTLog(@"appChangeState to %@",[SCSettings printAppState:appState]);
    switch (appState)
    {
        case SCAppStateChangeWillResignActive:
        {
            [_modelProfile startPingServer:SCPingStatusStop];

            [self saveSettings];
            [self saveStoredProfile];
        }
            break;
            
        case SCAppStateChangeDidEnterBackground:
            _isInBackgraund = YES;
            break;
            
        case SCAppStateChangeWillEnterForeground:
        {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
            [[UIApplication sharedApplication] cancelAllLocalNotifications];

            if(!_socketClient.isConnected)
            {
                _isTryRestoreConnect = YES;
                [self netInit];
            }
        }
            break;
            
        case SCAppStateChangeDidBecomeActive:
            if(_socketClient.isConnected)
                [_modelProfile startPingServer:SCPingStatusStart];
            break;
            
        case SCAppStateChangeWillTerminate:
        {
            [self saveSettings];
            [self saveStoredProfile];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - изменение установок
- (void) setIsNotification:(BOOL)isNotification
{
    _isNotification = isNotification;
    //TODO: Отправить уведомление на сервер
    [self _initPushNotifycation];
    [self saveSettings];
}

- (void) setIsNightMode:(BOOL)isNightMode
{
    _isNightMode = isNightMode;
    //TODO: Дать команду на изменение скина
    if(_isNightMode)
        _skinShemeName = NIGHT_SKIN_NAME;
    else
        _skinShemeName = DEFAULT_SKIN_NAME;
    
    [self saveSettings];
    [_skinManager selectSkinName:_skinShemeName];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CHANGE_SKIN object:nil];
}

- (void) setIsAppSound:(BOOL)isAppSound
{
    _isAppSound = isAppSound;
    [self saveSettings];
    
    [self _initAppSounds];
    /*if (_isAppSound) {
        
        //Attantion! Order in the array must match the order in the enum SCSoundPlay
        _soundsArray = @[@"sounds_on.wav",
                         @"login.wav",
                         @"logout.wav",
                         @"message_sending.wav",
                         @"message_receive.wav"];
        
    } else {
        _soundPlayer = nil;
        _soundsArray = nil;
    }*/
}

- (void) setIsAppVibration:(BOOL)isAppVibration
{
    _isAppVibration = isAppVibration;
    [self saveSettings];
}

#pragma mark - SCSettingsDelegate <NSObject>
-(void) didLoadProfile
{
    [self play:SCSoundPlayLogin];
    // данные по профайлу с сервера получены. продолжаем работу.

    // обновляем сабскрипшин лист
//    [_modelUsers getSubscriptionList];
    
    // обновляем контакты с сервера
    [_modelAddressbook getContacts];
    
    // обновляем чатрумы с сервера
    [_modelChats serverLoad];

    
//    [_modelLists getListsFromServerWithFavoriteList:NO];
    
    // записываем профайл
    [self saveStoredProfile];

    // запускаемся
    [_delegate succsessNetInit];
}

-(void) didUpdateProfile
{
//    _modelProfile
}


#pragma mark - SCWebSocketClientDelegate <NSObject>
-(void) clientSocketDidOpen
{
    FTLog(@"clientSocketDidOpen");
    
    if(!_isTryRestoreConnect)
    {
        // АСИНХРОННАЯ Загрузка профиля с сервера
        [_modelProfile loadServerProfile];
    }
    else
    {
        FTLog(@"_isTryRestoreConnect");
        _isTryRestoreConnect = NO;
    }
    [_modelProfile startPingServer:SCPingStatusStart];
}

-(void) clientSocketDidFailWithError:(NSError *)error
{
    
    // NSPOSIXErrorDomain Code=60 "The operation couldn’t be completed. Operation timed out"
    
    // Domain=org.lolrus.SocketRocket Code=2132 "received bad response code from server 403" UserInfo=0x14e97920 {NSLocalizedDescription=received bad response code from server 403}
    switch (error.code)
    {
        case 60:
        case 51:
        case 57:
        {
            if(_isInBackgraund)
            {
                _isTryRestoreConnect = YES;
                [self netInit];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                [_delegate tryLogin];
            }
        }
            break;
            
        case 2145:
            _isTryRestoreConnect = YES;
            break;
            
        case 61:
        case 2132:
        {
            [_delegate tryLogin];
        }
            break;

        default:
            FTLog(@"clientSocketDidFailWithError code:%ld",(long)error.code);
            break;
    }
    _modelProfile.ws_link = [NSString stringWithFormat:@"%@/%@/chat",WS_HOST,WS_PATH];
    _modelProfile.http_link = [NSString stringWithFormat:@"%@/%@",HTTP_HOST,HTTP_AUTH_PATH];

    [_modelProfile startPingServer:SCPingStatusStop];
}

-(void) clientSocketDidCloseWithCode:(NSError *)error
{
    FTLog(@"clientSocketDidCloseWithCode");

    if(_isInBackgraund && !_isTryRestoreConnect)
    {
        _isTryRestoreConnect = YES;
        [self netInit];
    }
    else
    {
        _isTryRestoreConnect = NO;
        [_delegate tryLogin];
    }
}

#pragma mark - Cache controll
-(void) clearImageCache
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:_caheImagePath])
    {
        NSDirectoryEnumerator* en = [[NSFileManager defaultManager] enumeratorAtPath:_caheImagePath];
        NSError* err = nil;
        BOOL res;
        
        NSString* file;
        while (file = [en nextObject]) {
            res = [[NSFileManager defaultManager] removeItemAtPath:[_caheImagePath stringByAppendingPathComponent:file] error:&err];
            if (!res && err) {
                FTLog(@"oops: %@", err);
            }
        }
    }
}



-(void) clearCacheAtPath:(NSString*) path


{
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
      {

    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    
//    NSArray* array =[fm contentsOfDirectoryAtPath:path error:&error];
    
    for (NSString *file in [fm contentsOfDirectoryAtPath:path error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", path, file] error:&error];
        if (!success || error) {
            // it failed.
        }
    }
}
    
    
    
//    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
//    {
//        NSDirectoryEnumerator* en = [[NSFileManager defaultManager] enumeratorAtPath:path];
//        NSError* err = nil;
//        BOOL res;
//        
//        NSString* file;
//        while (file = [en nextObject]) {
//            res = [[NSFileManager defaultManager] removeItemAtPath:[path stringByAppendingPathComponent:file] error:&err];
//            if (!res && err) {
//                FTLog(@"oops: %@", err);
//            }
//        }
//    }
    
}



-(void) clearAllCache
{
    //[self clearCacheAtPath:_cacheChatPath];
    if(_modelChats)
        [_modelChats clearCached];
    [self clearCacheAtPath:_cacheMessagesPath];
    
//    [self clearCacheAtPath:_cacheContactsPath];
    if(_modelUsers)
        [_modelUsers clearCached];
    [self clearCacheAtPath:_cacheListsPath];
    [self clearCacheAtPath:_caheImagePath];
    [self clearCacheAtPath:_mediaTempPath];
    
    if (_modelContacts) {
        
        [_modelContacts clearCached];
        
    }
    
}

//MARK: Sound
-(void) play:(SCSoundPlay) sound
{
    if (_isAppSound) {
        NSString *path = [[NSBundle mainBundle] resourcePath];
        NSString *soundName = _soundsArray[sound];
        NSString *soundNameWithPath = [[NSString alloc] initWithFormat:@"%@/%@", path, soundName];
        NSURL *soundURL = [NSURL fileURLWithPath:soundNameWithPath];
        
        NSError *error;
        _soundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:&error];
        
        [_soundPlayer prepareToPlay];
        
        if (_soundPlayer) {
            [_soundPlayer play];
        } else {
            FTLog(@"Sound '%@' not play. Error: %@", soundNameWithPath, [error description]);
        }
    }
}
+(NSString*) printAppState:(SCAppStateChange) state
{
    switch (state)
    {
        case SCAppStateChangeWillResignActive: return @"SCAppStateChangeWillResignActive";break;
        case SCAppStateChangeDidEnterBackground: return @"SCAppStateChangeDidEnterBackground";break;
        case SCAppStateChangeWillEnterForeground: return @"SCAppStateChangeWillEnterForeground";break;
        case SCAppStateChangeDidBecomeActive: return @"SCAppStateChangeDidBecomeActive";break;
        case SCAppStateChangeWillTerminate:return @"SCAppStateChangeWillTerminate";break;
        default:return @"";break;
    }
}

@end
