//
//  SCTextViewController.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 10.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"

typedef NS_ENUM(NSInteger, SCViewController) {
    SCViewControllerPrivacyPolicy = 0,
    SCViewControllerTermsOfUse,
    SCViewControllerAbout
};

@interface SCTextViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic) SCViewController viewController;
@property (nonatomic, strong) SCSettings *settings;
@end
