//
//  SCChatListCell.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell.h>
#import "SCChatListDetailView.h"
#import "SCModelsDelegates.h"
#import "SCInterfaceDelegates.h"


@class SCChatItem,SCImageViewWithIndicator,SCChatLogoView;

@interface SCChatListCell : SWTableViewCell <SCChatItemDelegate>
@property (nonatomic, weak) id<SCChatListViewControllerDelegate> dataDelegate;
@property (nonatomic, strong) SCChatItem* chatData;
//@property (strong, nonatomic) IBOutlet SCImageViewWithIndicator *imageChat;
@property (strong, nonatomic) IBOutlet SCChatLogoView *chatLogoView;

@property (strong, nonatomic) IBOutlet UILabel *labCount;

@property (strong, nonatomic) IBOutlet UILabel *labName;
@property (nonatomic, strong) IBOutlet SCChatListDetailView* detail;

//@property (weak, nonatomic) IBOutlet UILabel *labTimer;
@property (strong, nonatomic) IBOutlet UIImageView *imageShow;
@property (strong, nonatomic) IBOutlet UIImageView *imageSeparator;


+(CGFloat) calcSize:(CGFloat) width chatItem:(SCChatItem*) chatItem;

@end
