//
//  SCGroupChatItem.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 03.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"

@interface SCGroupChatItem : SCAbstractItem


@property(nonatomic,strong) NSString* grChatID;
@property(nonatomic,strong) NSString* grChatParentID;
@property(nonatomic,strong) NSString* grChatName;
@property(nonatomic,strong) NSString* grChatLink;
@end
