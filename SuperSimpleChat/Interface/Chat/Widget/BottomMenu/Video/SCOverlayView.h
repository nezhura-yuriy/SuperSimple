//
//  SCOverlayView.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 08.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SCOverlayViewProtocol <NSObject>
- (void) cancel;
- (void) changeCameraDevice;
- (void) takePicture;
- (void) startVideoCapture;
- (void) stopVideoCapture;
@end

typedef enum  {
    SCCurrentModePhoto,
    SCCurrentModeVideo
} SCCurrentMode;

@interface SCOverlayView : UIView

//Header
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UILabel *timeVideoLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeCameraButton;

//Footer
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *photoModeButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *videoModeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

//Other
@property (nonatomic) NSTimeInterval videoMaximumDuration;
@property (nonatomic) SCCurrentMode currentMode;
@property (nonatomic) UIImagePickerControllerCameraFlashMode currentFlashMode;

@property (nonatomic) id <SCOverlayViewProtocol> delegate;

- (IBAction)flashButtonPressed:(id)sender;

- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)changeCameraButtonPressed:(id)sender;
- (IBAction)videoModeButtonPressed:(id)sender;
- (IBAction)photoModeButtonPressed:(id)sender;
- (IBAction)actionButtonPressed:(id)sender;

@end
