//
//  SCModelUser.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 25.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModelsDelegates.h"

@class SCSettings;
@class SCPBManager;
@class SCChatItem;
@class SCPUUID;
@class SCContactItem;
@class SCContactItemSmall;


@interface SCContactItemDelegateWithID : NSObject

@property(nonatomic,assign) SCPUUID* puuid;
@property(nonatomic,weak) id <SCContactItemDelegate> delegate;

@end



@interface SCModelUser : NSObject <SCPBManagerDelegate>

@property (nonatomic, weak) id < SCContactItemDelegate > delegate;


@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) NSMutableDictionary* items;
@property(nonatomic,strong) SCPBManager* protoBufManager;
@property(nonatomic, strong) NSMutableArray* delegates;
@property(nonatomic,strong) NSMutableArray* subscribedItems;
@property(nonatomic,strong) NSMutableArray* blackListItems;


-(id) initWithSettings:(SCSettings*) settings;

-(void) checkAndUpdateItem:(SCContactItem*) processedItem;
-(void) updateItem:(SCContactItem*) processedItem;
-(SCContactItem*) makeContactItemPUUID:(SCPUUID *)userPUUID;
-(SCContactItem*) getUserByPUUID:(SCPUUID *)userPUUID;
-(SCContactItem*) getUserByPUUIDString:(NSString *)uuidString;
-(SCContactItem*) getUser:(SCContactItem *)inUserItem;
-(SCContactItemSmall*) getUserSmall:(NSString*) uuid;

#pragma mark - subscription
-(void) getSubscriptionList;
-(SCContactItem*) addSubscribedUserByPuuid:(SCPUUID*) puuid;
-(void) addSubscribedItems:(id) inPuuid;

#pragma mark - banned
-(void)getBannedUsersWithParams:(id)params;
-(void)addUsersToBlackListWithParams:(id)params;
-(void)removeUsersFromBlackListWithParams:(id)params;
-(void)checkUserInBlackList:(id)params;

#pragma mark - CACHE
-(void) clearBeforeLogout;
-(void) loadAllCached;
-(void) saveCache;
-(void) clearCached;

-(void) addDelegate:(id <SCContactItemDelegate>) delegate contactId:(SCPUUID*)ID;
-(void) delDelegate:(id <SCContactItemDelegate>) delegate contactId:(SCPUUID*)ID;
-(void) callDelegate:(SCPUUID*)ID withData:(id) idData;

@end
