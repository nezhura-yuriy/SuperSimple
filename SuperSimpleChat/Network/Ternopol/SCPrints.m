//
//  SCPrints.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 01.08.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPrints.h"
#import "SmartChat.pb.h"
#import "SCModelsDelegates.h"

//@implementation SCPrints

NSString* NSStringCallType(NSInteger type)
{
    switch ((CallType)type)
    {
            //Profile
        case CallTypeProfileGet:return @"CallTypeProfileGet";break;
        case CallTypeSetProfileField:return @"CallTypeSetProfileField";break;
        case CallTypeRemoveProfileField:return @"CallTypeRemoveProfileField";break;
        case CallTypeAuthGet:return @"CallTypeAuthGet";break;
        case CallTypeAuthAdd:return @"CallTypeAuthAdd"; break;
        case CallTypeAuthRemove:return @"CallTypeAuthRemove"; break ;
        case CallTypeAuthConfirm:return @"CallTypeAuthConfirm"; break;
        case CallTypeUserUpdateStatus:return @"CallTypeUserUpdateStatus";break;
        case CallTypeAuthorizationFailedEvent:return @"CallTypeAuthorizationFailedEvent";break;
        case CallTypeSubscribeToAmazonSns:return @"CallTypeSubscribeToAmazonSns";break;
        case CallTypeUnsubscribeFromAmazonSns:return @"CallTypeUnsubscribeFromAmazonSns";break;
            // Users
        case CallTypeGetUserByAuth:return @"CallTypeGetUserByAuth";break;
        case CallTypeGetUserById:return @"CallTypeGetUserById";break;
        case CallTypeSearchUsersByAuth:return @"CallTypeSearchUsersByAuth";break;
        case CallTypeSubscribe:return @"CallTypeSubscribe";break;
        case CallTypeUnSubscribe:return @"CallTypeUnSubscribe";break;
        case CallTypeGetAllUsersWhichISubscribed:return @"CallTypeGetAllUsersWhichISubscribed";break;
        case CallTypeInviteUsers:return @"CallTypeInviteUsers";break;
        case CallTypeCheckIfUserInBlackList:return @"CallTypeCheckIfUserInBlackList";break;
        case CallTypeAddUsersToBlackList:return @"CallTypeAddUsersToBlackList";break;
        case CallTypeRemoveUsersFromBlackLists:return @"CallTypeRemoveUsersFromBlackLists"; break;
        case CallTypeGetAllBannedUsers:return @"CallTypeGetAllBannedUsers";break;
        case CallTypeUserUpdateStatusEvent:return @"CallTypeUserUpdateStatusEvent";break;
        case CallTypeUpdateProfileEvent:return @"CallTypeUpdateProfileEvent";break;
            
            // Lists
        case CallTypeListGet:return @"CallTypeListGet";break;
        case CallTypeListsGet:return @"CallTypeListsGet";break;
        case CallTypeListAdd:return @"CallTypeListAdd";break;
        case CallTypeListUpdate:return @"CallTypeListUpdate";break;
        case CallTypeListRemove:return @"CallTypeListRemove";break;
        case CallTypePublicListAdd:return @"CallTypePublicListAdd";break;
        case CallTypePublicListGet:return @"CallTypePublicListGet";break;
        case CallTypeListItemAdd:return @"CallTypeListItemAdd";break;
        case CallTypeListItemGet:return @"CallTypeListItemGet";break;
        case CallTypeListItemsGet:return @"CallTypeListItemsGet";break;
        case CallTypeListItemUpdate:return @"CallTypeListItemUpdate";break;
        case CallTypeListItemRemove:return @"CallTypeListItemRemove";break;
        case CallTypeListItemsSetOrder:return @"CallTypeListItemsSetOrder";break;
            
            // ChatRoom
        case CallTypeCreateChatroom:return @"CallTypeCreateChatroom";break;
        case CallTypeRemoveChatroom:return @"CallTypeRemoveChatroom";break;
        case CallTypeMakeChatRoomActive:return @"CallTypeMakeChatRoomActive";break;
        case CallTypeChangeInfoChatroom:return @"CallTypeChangeInfoChatroom";break;
        case CallTypeGetAllUsersForRoom:return @"CallTypeGetAllUsersForRoom";break;
        case CallTypeGetAllRoomsForUser:return @"CallTypeGetAllRoomsForUser";break;
        case CallTypeSearchPublicChatRooms:return @"CallTypeSearchPublicChatRooms";break;
        case CallTypeJoinUserToRoom:return @"CallTypeJoinUserToRoom";break;
        case CallTypeJoinUsersToRoom:return @"CallTypeJoinUsersToRoom";break;
        case CallTypeLeaveUserFromRoom:return @"CallTypeLeaveUserFromRoom";break;
        case CallTypeChatroomGetById:return @"CallTypeChatroomGetById";break;
        case CallTypeLastReadMessageDateGet:return @"CallTypeLastReadMessageDateGet";break;
        case CallTypeLastReadMessageDateSet:return @"CallTypeLastReadMessageDateSet";break;
        case CallTypeChangeInfoChatroomEvent:return @"CallTypeChangeInfoChatroomEvent";break;
        case CallTypeJoinUserToRoomEvent:return @"CallTypeJoinUserToRoomEvent";break;
        case CallTypeLeaveUserFromRoomEvent:return @"CallTypeLeaveUserFromRoomEvent";break;
        case CallTypeRemoveChatroomEvent:return @"CallTypeRemoveChatroomEvent";break;
            
            // Messages
        case CallTypeSendMessage:return @"CallTypeSendMessage";break;
        case CallTypeMessage:return @"CallTypeMessage";break;
        case CallTypeChatRoomHistoryGet:return @"CallTypeChatRoomHistoryGet";break;
        case CallTypeChatRoomHistoryGetWithTimeRange:return @"CallTypeChatRoomHistoryGetWithTimeRange";break;
            //Contacts
        case CallTypeContactCreate:return @"CallTypeContactCreate";break;
        case CallTypeContactRemove:return @"CallTypeContactRemove";break;
        case CallTypeContactFieldsGet:return @"CallTypeContactFieldsGet";break;
        case CallTypeContactSetFields:return @"CallTypeContactSetFields";break;
        case CallTypeContactRemoveField:return @"CallTypeContactRemoveField";break;
        case CallTypeContactsGet:return @"CallTypeContactsGet";break;
            
        default:return [NSString stringWithFormat:@"response type = %d",(int)type];break;
    }
}

NSString *NSStringSCCallType(NSInteger scType)
{
    switch ((SCPBManagerParceType)scType)
    {
            //Profile
        case SCPBManagerParceTypeNone:return @"SCPBManagerParceTypeNone";break;
        case SCPBManagerParceTypeGetProfile:return @"SCPBManagerParceTypeGetProfile";break;
        case SCPBManagerParceTypeSetProfileField:return @"SCPBManagerParceTypeSetProfileField";break;
        case SCPBManagerParceTypeRemoveProfileField:return @"SCPBManagerParceTypeRemoveProfileField";break;
        case SCPBManagerParceTypeCheckIsNicknameFree:return @"SCPBManagerParceTypeCheckIsNicknameFree";break;
        case SCPBManagerParceTypeChangeStatus:return @"SCPBManagerParceTypeChangeStatus";break;
        case SCPBManagerParceTypePrepareLogout:return @"SCPBManagerParceTypePrepareLogout";break;
        case SCPBManagerParceTypeLogout:return @"SCPBManagerParceTypeLogout";break;
        case SCPBManagerParceTypeAuthorizationFailedEvent:return @"SCPBManagerParceTypeAuthorizationFailedEvent";break;
        case SCPBManagerParceTypeChangeStatusEvent:return @"SCPBManagerParceTypeChangeStatusEvent";break;
        case SCPBManagerParceTypeUpdateProfileEvent:return @"SCPBManagerParceTypeUpdateProfileEvent";break;
        case SCPBManagerParceTypeSubscribeToAmazonSNS:return @"SCPBManagerParceTypeSubscribeToAmazonSNS";break;
        case SCPBManagerParceTypeUnsubscribeFromAmazonSNS:return @"SCPBManagerParceTypeUnsubscribeFromAmazonSNS";break;
        case SCPBManagerParceTypeAuthAdd:return @"SCPBManagerParceTypeAuthAdd";break;
        case SCPBManagerParceTypeAuthConfirm:return @"SCPBManagerParceTypeAuthConfirm";break;
        case SCPBManagerParceTypeAuthRemove:return @"SCPBManagerParceTypeAuthRemove";break;
        case SCPBManagerParceTypeAuthGet:return @"SCPBManagerParceTypeAuthGet";break;
        case SCPBManagerParceTypeGetUserByAuth:return @"SCPBManagerParceTypeGetUserByAuth";break;
        case SCPBManagerParceTypeGetUserById:return @"SCPBManagerParceTypeGetUserById";break;
        case SCPBManagerParceTypeGetUserByIdExt:return @"SCPBManagerParceTypeGetUserByIdExt";break;
        case SCPBManagerParceTypeGetUserByIdContact:return @"SCPBManagerParceTypeGetUserByIdContact";break;
        case SCPBManagerParceTypeSubscribe:return @"SCPBManagerParceTypeSubscribe";break;
        case SCPBManagerParceTypeUnSubscribe:return @"SCPBManagerParceTypeUnSubscribe";break;
        case SCPBManagerParceTypeUsersWhichISubscribed:return @"SCPBManagerParceTypeUsersWhichISubscribed";break;
        case SCPBManagerParceTypeCheckIfUserInBlackListRequest:return @"SCPBManagerParceTypeCheckIfUserInBlackListRequest";break;
        case SCPBManagerParceTypeAddUsersToBlackListRequest:return @"SCPBManagerParceTypeAddUsersToBlackListRequest";break;
        case SCPBManagerParceTypeRemoveUsersFromBlackListRequest:return @"SCPBManagerParceTypeRemoveUsersFromBlackListRequest";break;
        case SCPBManagerParceTypeGetBannedUsersRequest:return @"SCPBManagerParceTypeGetBannedUsersRequest";break;
        case SCPBManagerParceTypeGetLists:return @"SCPBManagerParceTypeGetLists";break;
        case SCPBManagerParceTypeAddList:return @"SCPBManagerParceTypeAddList";break;
        case SCPBManagerParceTypeGetList:return @"SCPBManagerParceTypeGetList";break;
        case SCPBManagerParceTypeUpdateList:return @"SCPBManagerParceTypeUpdateList";break;
        case SCPBManagerParceTypeRemoveList:return @"SCPBManagerParceTypeRemoveList";break;
        case SCPBManagerParceTypeAddPublicList:return @"SCPBManagerParceTypeAddPublicList";break;
        case SCPBManagerParceTypeGetPublicLists:return @"SCPBManagerParceTypeGetPublicLists";break;
        case SCPBManagerParceTypeAddListItem:return @"SCPBManagerParceTypeAddListItem";break;
        case SCPBManagerParceTypeGetListItem:return @"SCPBManagerParceTypeGetListItem";break;
        case SCPBManagerParceTypeGetListItems:return @"SCPBManagerParceTypeGetListItems";break;
        case SCPBManagerParceTypeUpdateListItem:return @"SCPBManagerParceTypeUpdateListItem";break;
        case SCPBManagerParceTypeRemoveListItem:return @"SCPBManagerParceTypeRemoveListItem";break;
        case SCPBManagerParceTypeSetListItemsOrder:return @"SCPBManagerParceTypeSetListItemsOrder";break;
        case SCPBManagerParceTypeCreateChat:return @"SCPBManagerParceTypeCreateChat";break;
        case SCPBManagerParceTypeGetChatRoomByIdRequest:return @"SCPBManagerParceTypeGetChatRoomByIdRequest";break;
        case SCPBManagerParceTypeSearchCreateChat:return @"SCPBManagerParceTypeSearchCreateChat";break;
        case SCPBManagerParceTypeChatRoomGetById:return @"SCPBManagerParceTypeChatRoomGetById";break;
        case SCPBManagerParceTypeRemoveChat:return @"SCPBManagerParceTypeRemoveChat";break;
        case SCPBManagerParceTypeActivateChat:return @"SCPBManagerParceTypeActivateChat";break;
        case SCPBManagerParceTypeChangeChat:return @"SCPBManagerParceTypeChangeChat";break;
        case SCPBManagerParceTypeGetAllRooms:return @"SCPBManagerParceTypeGetAllRooms";break;
        case SCPBManagerParceTypeGetAllRoomsForUser:return @"SCPBManagerParceTypeGetAllRoomsForUser";break;
        case SCPBManagerParceTypeSearchPublicChatRooms:return @"SCPBManagerParceTypeSearchPublicChatRooms";break;
        case SCPBManagerParceTypeGetAllUsersForRoom:return @"SCPBManagerParceTypeGetAllUsersForRoom";break;
        case SCPBManagerParceTypeLeaveUserFromRoom:return @"SCPBManagerParceTypeLeaveUserFromRoom";break;
        case SCPBManagerParceTypeJoinUserToRoom:return @"SCPBManagerParceTypeJoinUserToRoom";break;
        case SCPBManagerParceTypeGetHistory:return @"SCPBManagerParceTypeGetHistory";break;
        case SCPBManagerParceTypeGetChatRoomHistory:return @"SCPBManagerParceTypeGetChatRoomHistory";break;
        case SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange:return @"SCPBManagerParceTypeGetChatRoomHistoryWithTimeRange";break;
        case SCPBManagerParceTypeGetLastReadMessageDate:return @"SCPBManagerParceTypeGetLastReadMessageDate";break;
        case SCPBManagerParceTypeSetLastReadMessageDate:return @"SCPBManagerParceTypeSetLastReadMessageDate";break;
        case SCPBManagerParceTypeChangeRoomEvent:return @"SCPBManagerParceTypeChangeRoomEvent";break;
        case SCPBManagerParceTypeJoinUserToRoomEvent:return @"SCPBManagerParceTypeJoinUserToRoomEvent";break;
        case SCPBManagerParceTypeCallTypeLeaveUserFromRoomEvent:return @"SCPBManagerParceTypeCallTypeLeaveUserFromRoomEvent";break;
        case SCPBManagerParceTypeCallTypeRemoveRoomEvent:return @"SCPBManagerParceTypeCallTypeRemoveRoomEvent";break;
        case SCPBManagerParceTypeSendMessage:return @"SCPBManagerParceTypeSendMessage";break;
        case SCPBManagerParceTypeUpdateMessage:return @"SCPBManagerParceTypeUpdateMessage";break;
        case SCPBManagerParceTypeRemoveMessage:return @"SCPBManagerParceTypeRemoveMessage";break;
        case SCPBManagerParceTypeMessage:return @"SCPBManagerParceTypeMessage";break;
        case SCPBManagerParceTypeRefreshedMessageEvent:return @"SCPBManagerParceTypeRefreshedMessageEvent";break;
        case SCPBManagerParceTypeContactCreate:return @"SCPBManagerParceTypeContactCreate";break;
        case SCPBManagerParceTypeContactRemove:return @"SCPBManagerParceTypeContactRemove";break;
        case SCPBManagerParceTypeGetAllContacts:return @"SCPBManagerParceTypeGetAllContacts";break;
        case SCPBManagerParceTypeGetAllContactFields:return @"SCPBManagerParceTypeGetAllContactFields";break;
        case SCPBManagerParceTypeContactSetFields:return @"SCPBManagerParceTypeContactSetFields";break;
        case SCPBManagerParceTypeSearchUsersByAuth:return @"SCPBManagerParceTypeSearchUsersByAuth";break;
        case SCPBManagerParceTypeUpdateAddressBook:return @"SCPBManagerParceTypeUpdateAddressBook";break;
        case SCPBManagerParceTypeInviteUsers:return @"SCPBManagerParceTypeInviteUsers";break;
        case SCPBManagerParceTypeInternalMessageItemTickDestroy:return @"SCPBManagerParceTypeInternalMessageItemTickDestroy";break;
        case SCPBManagerParceTypeInternalChatItemTickDestroy:return @"SCPBManagerParceTypeInternalChatItemTickDestroy";break;
        default:return [NSString stringWithFormat:@"response type = %d",(int)scType];break;
    }
}


