//
//  SCSegmentControll.h
//  SmartChat
//
//  Created by Yury Radchenko on 06.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCSegmentControll : UISegmentedControl

@end
