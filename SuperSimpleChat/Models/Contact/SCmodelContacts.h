//
//  SCmodelContacts.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 03.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModelsDelegates.h"
#import "SCContactItem.h"



@class SCSettings;

@protocol SCmodelContactsDelegate <NSObject>


@optional

-(void) seartUsersRespond:(NSMutableArray*) contacts;
-(void) getContactsFromModel:(NSMutableArray*) contacts;
-(void) getUserByAuthRespond:(SCContactItem*) contact;
-(void) getUsersRespondError:(NSDictionary*) errorMessage;

-(void) successInvite:(id)data;
-(void) successCreateEndSetFeildsContact;
-(void) successDeleteContact;

-(void) errorDeleteContact:(NSString*)errorMessage;


@end

@interface SCmodelContacts : NSObject <SCPBManagerDelegate>

@property (nonatomic, weak) id < SCmodelContactsDelegate > delegate;

@property(nonatomic,strong) SCSettings* settings;

@property(nonatomic,strong) NSMutableArray* groups;

@property(strong,nonatomic) NSString *path;


-(id) initWithSettings:(SCSettings*) settings;
-(NSArray*)items:(NSString*) groupID;

-(void)removeContact:(SCContactItem*)contact;
-(void)getAllContacts:(NSMutableDictionary*)params;
-(void)searchUsersByAuthRequest:(NSMutableDictionary*) params;
-(void)createNewContactWithParams:(NSDictionary*)params;
-(void)getUserByAuthWithParams:(NSDictionary*)params;
-(void)inviteUsers:(id)data;
-(void)addNewContactToModel:(SCContactItem*)contact;

-(void)saveContacts:(NSMutableArray*) contacts;
-(SCContactItem*) searchByUUID:(SCPUUID*) puuid;
-(NSMutableArray*) loadCached;
-(void)clearCached;





#pragma mark DEBUG: load from server
-(void) localLoad;
@end
