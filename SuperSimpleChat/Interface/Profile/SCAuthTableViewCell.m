//
//  SCAuthTableViewCell.m
//  SuperSimpleChat
//
//  Created by Alexsandr Linnik on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAuthTableViewCell.h"

typedef enum  {
    
    SCSectionPhoneAuth,
    SCSectionMailAuth
    
} SCSection;

@implementation SCAuthTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)confirmAction:(id)sender {
    
    
    if (_indexPath.section == SCSectionPhoneAuth) {
    
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm phone number"
                                                                message:@"Enter sms code"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Сancel"
                                                      otherButtonTitles: @"Ок", nil];
                alert.delegate = self;
                alert.alertViewStyle= UIAlertViewStylePlainTextInput;
                [alert show];
        
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm email"
                                                                message:@"Enter sms code"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Сancel"
                                                      otherButtonTitles: @"Ок", nil];
                alert.delegate = self;
                alert.alertViewStyle= UIAlertViewStylePlainTextInput;
                [alert show];
        
            }
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UITextField *authTextField = [alertView textFieldAtIndex:0];
    
    NSString *authCode = authTextField.text;
    
    if (authCode.length>0) {
        
        [self.delegate confirmAuth:self.auth withCode:authCode];
    }
    
}

-(void)prepareForReuse {
    
    self.confirmButton.hidden = NO;
}



@end
