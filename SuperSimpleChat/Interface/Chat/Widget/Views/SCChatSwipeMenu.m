//
//  SCChatSwipeMenu.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 31.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatSwipeMenu.h"
#import "SCSettings.h"


@implementation SCChatSwipeMenu
{
    SCSettings* _settings;
    UIButton* _btnMenuDelete;
}

- (instancetype)initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if (self)
    {
        _settings = settings;
        self.layer.borderColor = [SKCOLOR_ChatMessageOutgoingBg CGColor];
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 5;
        
        _btnMenuDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnMenuDelete.frame = CGRectMake(0, 0, 98, 30);
        [_btnMenuDelete.titleLabel setFont:[UIFont fontWithName:FT_FontNameRegular size:12.0]];
        _btnMenuDelete.backgroundColor = [UIColor clearColor];//[UIColor blackColor];//
        [_btnMenuDelete setTitleColor:SKCOLOR_ChatMessageOutgoingBg forState:UIControlStateNormal];//[UIColor blackColor]//
        [_btnMenuDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [self addSubview:_btnMenuDelete];
        
    }
    return self;
}

- (void)dealloc
{
    FTLog(@"%@",NSStringFromClass([self class]));
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(!CGRectEqualToRect(frame, CGRectZero))
    {
        _btnMenuDelete.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    }
}
@end
