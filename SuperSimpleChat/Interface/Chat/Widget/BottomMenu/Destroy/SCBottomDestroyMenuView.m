//
//  SCBottomDestroyMenuView.m
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//


typedef enum  {
    
    ThreeDaysTimeDestroy,
    TwentyFourHoursTimeDestroy,
    SixHoursTimeDestroy,
    OneHoursTimeDestroy,
//    OneMinuteTimeDestroy,
    NowTimeDestroy
    
} TimeDestroy;

#import "SCBottomDestroyMenuView.h"
#import "SCSettings.h"

@interface SCBottomDestroyMenuView ()<UIPickerViewDataSource,UIPickerViewDelegate>

@property(strong,nonatomic) UIPickerView* timeDestroyPicker;
@property(strong,nonatomic) NSArray* pickerData;
@property(strong,nonatomic) UIButton* btnOkey;
@property(strong,nonatomic) UIButton* btnCancel;
@end

@implementation SCBottomDestroyMenuView
{
    void (^actionSelector) (id selectItem);
}


-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    _settings = settings;
    if(self)
    {
        
        _pickerData = @[@"3 Days", @"24 Hours", @"6 Hours", @"1 Hours", @"Never"];// @"1 Minute",

        self.clipsToBounds = YES;
        _timeDestroyPicker = [[UIPickerView alloc] init];//WithFrame:CGRectMake(0, -50, 320, 200)];
        _timeDestroyPicker.delegate = self;
        [self addSubview:_timeDestroyPicker];
        [_timeDestroyPicker selectRow:4 inComponent:0 animated:NO];

        _btnOkey = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnOkey setImage:[_settings.skinManager getImageForKeyX:@"btn_add"] forState:UIControlStateNormal];
        [_btnOkey addTarget:self action:@selector(btnOkeyAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btnOkey];
        
        _btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnCancel setImage:[_settings.skinManager getImageForKeyX:@"chat-record-cancel"] forState:UIControlStateNormal];
        [_btnCancel addTarget:self action:@selector(btnCanselAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btnCancel];

    }
    return self;
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _timeDestroyPicker.frame = CGRectMake(0, -40, frame.size.width, 180);
    _btnOkey.frame = CGRectMake(frame.size.width - 44, 0, 44, 44);
    _btnCancel.frame = CGRectMake(0, 0, 44, 44);
}

-(void) setActionSelector:(void(^)(id selectItem)) completionBlock
{
    actionSelector = [completionBlock copy];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return _pickerData.count;
}

-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSString *title = _pickerData[row];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    return attString;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    switch (row) {
            
        case ThreeDaysTimeDestroy: _atTicks = 3*24*60*60; break;
        case TwentyFourHoursTimeDestroy: _atTicks = 24*60*60; break;
        case SixHoursTimeDestroy: _atTicks = 6*60*60; break;
        case OneHoursTimeDestroy: _atTicks = 1*60*60; break;
//        case OneMinuteTimeDestroy: _atTicks = 10; break;
        case NowTimeDestroy: _atTicks = 0; break;
        default: break;
    }
    if ([_delegate respondsToSelector:@selector(setDestroyTime:)]) {
        [_delegate setDestroyTime:_atTicks];
        
    }

}

-(void) btnOkeyAction:(id) sender
{
    //    timeDestroyPicker(0);
    actionSelector(0);
}

-(void) btnCanselAction:(id) sender
{
    //    timeDestroyPicker(0);
    actionSelector(0);
}

-(void) dealloc
{
    
}

-(CGSize) sizeView
{
    return CGSizeMake(0, 100);
}
@end
