//
//  SCMessagesByDateView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 08.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SCMessageGroup;
@class SCSettings;

@interface SCMessagesByDateView : UIView


@property(nonatomic,assign) BOOL isExpand;
@property(nonatomic,assign) CGFloat forWidth;
@property(nonatomic,strong) SCMessageGroup* messageGroup;


-(id) initWithSettings:(SCSettings*) settings;
-(void) updateGroupMessages;

-(CGSize) viewSize;
@end
