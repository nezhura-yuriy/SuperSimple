//
//  SCAccessPhoneViewController.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 20.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAccessPhoneViewController.h"
#import "CountryListViewController.h"
#import "SCSettings.h"
#import "SCVerificationsViewController.h"
#import "SCHTTPManager.h"
#import <SVProgressHUD.h>
#import "CountryListDataSource.h"
#import "SCAccessEmailViewController.h"
#import <FBSDKCoreKit.h>
#import <FBSDKLoginButton.h>
#import <FBSDKLoginManager.h>
#import "SCSkinManager.h"


@interface SCAccessPhoneViewController () <UIAlertViewDelegate,UITextFieldDelegate,CountryListViewDelegate,FBSDKLoginButtonDelegate>

@property(strong, nonatomic) NSString* contryCode;
@property(strong, nonatomic) NSString* code;

@property(strong, nonatomic) SCHTTPManager* httpManager;
@property(strong, nonatomic) SCSkinManager* skinManager;

@end

@implementation SCAccessPhoneViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void) setSettings:(SCSettings *)settings
{
    
    _settings = settings;
    self.skinManager = _settings.skinManager;
    
//    self.skinManager = [[SCSkinManager alloc]initWithSkinName:[[NSUserDefaults standardUserDefaults] objectForKey:@"skinShemeName"]];
    
    self.title = @"Access";
    
    self.httpManager = _settings.httpManager;
    
    [SVProgressHUD setBackgroundColor:[_skinManager getColorForKeyX:@"NavigationBarBackground"]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
    
    self.phoneNumberTextField.delegate = self;
    
    
    self.phoneNumberOutlet.backgroundColor = [_skinManager getColorForKeyX:@"ButtonGreenBackground"];
    self.emailRegistrationOutlet.backgroundColor = [_skinManager getColorForKeyX:@"ButtonGreenBackground"];
    self.connectWithFacebookOutlet.backgroundColor =[_skinManager getColorForKeyX:@"ButtonFacebookBackground"];
    
    
    self.phoneNumberOutlet.layer.cornerRadius = 4.0f;
    self.emailRegistrationOutlet.layer.cornerRadius = 4.0f;
    self.connectWithFacebookOutlet.layer.cornerRadius = 4.0f;
    
    
    
//    self.connectWithFacebookOutlet.delegate = self;
    
    [self searchCurrentCountryCode];
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    

    NSDictionary *attribute = @{NSForegroundColorAttributeName:[_skinManager getColorForKeyX:@"NavigationBarTitleFont"],
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    
    self.navigationController.navigationBar.titleTextAttributes = attribute;
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBarTintColor:[_skinManager getColorForKeyX:@"NavigationBarBackground"]];
    self.navigationController.navigationBar.translucent = NO;


}



#pragma mark - Dissmis Kayboard by touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self.phoneNumberTextField resignFirstResponder];
    
    
}


#pragma mark - Private Methods


-(void)searchCurrentCountryCode{
    
    NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    FTLog(@"%@",countryCode);
    
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    NSArray *data = [dataSource countries];
    
    
    BOOL found = NO;
    
    for (NSDictionary *dict in data) {
        
        found = [[dict objectForKey:@"code"] isEqualToString:countryCode];
        
        if (found){
            
            self.contryCode =[NSString stringWithString:[dict objectForKey:@"dial_code"]];
            self.code = [NSString stringWithString:[dict objectForKey:@"code"]];
            [self.countryCodeButtonOutlet setTitle: [NSString stringWithFormat:@"%@ %@",self.contryCode, self.code] forState:UIControlStateNormal];
            
            break;
        };
    }
    
}




#pragma mark - CountryListViewDelegate

- (void)didSelectCountry:(NSDictionary *)country {
    
    self.contryCode =[NSString stringWithString:[country objectForKey:@"dial_code"]];
    self.code = [NSString stringWithString:[country objectForKey:@"code"]];
    
    [self.countryCodeButtonOutlet setTitle: [NSString stringWithFormat:@"%@ %@",self.contryCode, self.code] forState:UIControlStateNormal];
    
}



#pragma mark - Actions

- (IBAction)countryCodeAction:(id)sender {
    
    CountryListViewController *cv = [[CountryListViewController alloc] initWithNibName:@"CountryListViewController" delegate:self];
    cv.settings = _settings;
    cv.skinManager = _skinManager;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:cv];
    
//    [self.navigationController pushViewController:cv animated:YES];
    [self presentViewController:nav animated:YES completion:nil];
    
}

- (IBAction)phoneRegistrationActon:(id)sender {
    
        if (self.contryCode && self.phoneNumberTextField.text.length > 0){
            
            self.phoneNumberOutlet.enabled = NO;
            
            [SVProgressHUD show];
            
            [self.phoneNumberTextField resignFirstResponder];
            
            [self.httpManager registerUserWithPhoneNumber:[NSString stringWithFormat:@"%@%@",self.contryCode,self.phoneNumberTextField.text]
                                                onSuccess:^(NSDictionary *registerValue) {
                                                    
                                                    long status = [[registerValue objectForKey:@"status"] longValue];
                                                    
                                                    if (status == 0) {
                                                        
                                                        [SVProgressHUD dismiss];
                                                        
                                                        NSString* sessionId = [registerValue objectForKey:@"accessToken"];
                                                        NSString* confirmationHash = [registerValue objectForKey:@"confirmationHash"];
                                                        _settings.modelProfile.sessionID = sessionId;
                                                        _settings.modelProfile.confirmationHash = confirmationHash;
                                                        
                                                        SCVerificationsViewController* verificationViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCVerificationsViewController" owner:self options:nil] lastObject];
                                                        
                                                        verificationViewController.delegate = _delegate;
                                                        verificationViewController.isPhoneVerification = YES;
                                                        verificationViewController.skinManager=_skinManager;

                                                        
                                                        self.phoneNumberOutlet.enabled = YES;
                                                        
                                                        [self.navigationController pushViewController:verificationViewController animated:YES];
                                                        
                                                    } else{
                                                        
                                                        [SVProgressHUD dismiss];
                                                        
                                                        self.phoneNumberOutlet.enabled = YES;
                                                        
                                                        NSString* errorMessage = [registerValue objectForKey:@"errorMessage"];
                                                        
                                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                            message:errorMessage
                                                                                                           delegate:self
                                                                                                  cancelButtonTitle:@"Cancel"
                                                                                                  otherButtonTitles:nil];
                                                        
                                                        [alertView show];
                                                        
                                                    }
                                                    
                                                }
                                                onFailure:^(NSError *error, NSInteger statusCode) {
                                                    
                                                    [SVProgressHUD dismiss];
                                                    
                                                    self.phoneNumberOutlet.enabled = YES;
                                                    
                                                    FTLog(@"error = %@, code = %ld", [error localizedDescription],(long)statusCode);
#pragma mark TODO показать сообщение
                                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                        message:[error localizedDescription]
                                                                                                       delegate:self
                                                                                              cancelButtonTitle:@"Cancel"
                                                                                              otherButtonTitles:nil];
                                                    
                                                    [alertView show];
                                                    
                                                }];
            
        } else {
            
            [self startShake:self.phoneNumberOutlet];
            
        }
        
        
    
}

- (IBAction)emailRegistrationAction:(id)sender {
    
    SCAccessEmailViewController* verificationViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCAccessEmailViewController" owner:self options:nil] lastObject];
    
    verificationViewController.delegate = _delegate;
    verificationViewController.skinManager = _skinManager;
    
    [self.navigationController pushViewController:verificationViewController animated:YES];
    
}

- (IBAction)connectWithFacebookAction:(id)sender {
    
    
}



#pragma mark - Animation

- (void) startShake:(UIView*)view
{
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-8, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(8, 0);
    
    view.transform = leftShake;  // starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:2];
    [UIView setAnimationDuration:0.07];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    view.transform = rightShake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}


#pragma mark - UITextFieldDelegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


#pragma mark -  Supported orientation


-(BOOL)shouldAutorotate {
    
    return YES;
    
}

- (NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

#pragma mark - FBSDKLoginButtonDelegate

- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{
    
    if (result) {
        
        NSString* token = [[FBSDKAccessToken currentAccessToken]tokenString];
        
        [SVProgressHUD show];
        
        //         [[FBSDKLoginManager new] logOut];

         [self.httpManager registerUserWithFacebook:token
                                          onSuccess:^(NSDictionary *registerValue) {
                                              
                                              long status = [[registerValue objectForKey:@"status"] longValue];
                                              
                                              if (status == 0) {
                                                  
                                                  [SVProgressHUD dismiss];
                                                  
                                                  NSString* sessionId = [registerValue objectForKey:@"accessToken"];
                                                  _settings.modelProfile.sessionID = sessionId;
                                                  
                                                  
                                                  if(_delegate && [_delegate respondsToSelector:@selector(succsessLogin)]){
                                                      
                                                      [_delegate succsessLogin];
                                                      
                                                  }

                                                  
                                              } else{
                                                  
                                                  
                                                  [SVProgressHUD dismiss];
                                                  
                                                  NSString* errorMessage = [registerValue objectForKey:@"errorMessage"];
                                                  
                                                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                      message:errorMessage
                                                                                                     delegate:self
                                                                                            cancelButtonTitle:@"Cancel"
                                                                                            otherButtonTitles:nil];
                                                  
                                                  [alertView show];
                                                  
                                              }

             
             
    
                                        } onFailure:^(NSError *error, NSInteger statusCode) {
    
    
    
             }];
        
      
    }
    
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
    
}

@end
