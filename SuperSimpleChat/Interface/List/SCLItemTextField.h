//
//  SCLItemTextField.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCLItemTextField : UITextField

@end
