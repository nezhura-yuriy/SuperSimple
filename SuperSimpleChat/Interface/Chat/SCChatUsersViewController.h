//
//  SCChatUsersViewController.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 28.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"


@class SCSettings;
@class SCChatUsersView;

@interface SCChatUsersViewController : UIViewController



@property(nonatomic,strong) SCSettings* settings;
@property(nonatomic,strong) SCChatUsersView* userView;
@property (nonatomic, strong) NSMutableArray* userInRoom;

@property(assign, nonatomic) BOOL isNewChat;


@property (nonatomic,weak) id<SCChatUsersViewControllerDelegate> delegate;
@end
