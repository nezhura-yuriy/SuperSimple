//
//  SCPhotoPanel.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 23.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SCPhotoPanelDelegate <NSObject>

-(void) addImageAction;
-(void) showImageAction;
-(void) clearImageAction;
@end


@class SCImageViewWithIndicator;

@interface SCPhotoPanel : UIView

@property(nonatomic,weak) id<SCPhotoPanelDelegate> delegate;
@property(nonatomic,strong) IBOutlet UIButton* btnAdd;
@property(nonatomic,strong) IBOutlet UIButton* btnView;
@property(nonatomic,strong) IBOutlet UIButton* btnClear;
@property(nonatomic,strong) IBOutlet UIView* viewBackgraund;
@property(nonatomic,strong) IBOutlet UIImageView* imgBackgraund;
@property(nonatomic,strong) IBOutlet SCImageViewWithIndicator* imgPhoto;
@property(nonatomic,assign) BOOL isEditedle;

-(void) setProgress:(long long) totalBytesRead totalBytesExpectedToRead:(long long) totalBytesExpectedToRead;
-(void) settingImage:(UIImage*) img;
-(void) endProgress;

@end
