//
//  SCListViewController.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 18.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCListViewController.h"
#import "SCList.h"
#import "SCListItem.h"
#import "SCLTableViewCell.h"
#import "SCLPullToAddNewBehaviour.h"

#import "SCNotificationListKeys.h"

static const NSString *kKeySuccess = @"success";

typedef NS_ENUM(NSInteger, SCEditingBehavior){
    SCEditingBehaviorAddItem,
    SCEditingBehaviorEditItem
};

@implementation SCListViewController
{
    SCList *_list;
    NSMutableArray *_listItems;
    
    float _editingOffsetY;
    SCEditingBehavior _editingBehavior;
    
    SCLPullToAddNewBehaviour *_pullAddNewBehaviour;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self subscribeNotifications];
    
    _listItems = [[NSMutableArray alloc] init];
    _list = [[SCList alloc] initAsFavorite];
    
    self.view.backgroundColor = SKCOLOR_ViewBg;
    
    //Configure the table
    //указание, какой именно класс будет использоваться для ячеек таблицы
    [_tableView registerClassForCells:[SCLTableViewCell class]];
    _tableView.datasource = self;
    
    _pullAddNewBehaviour = [[SCLPullToAddNewBehaviour alloc] initWithTableView:self.tableView];
    
    _editingBehavior = SCEditingBehaviorEditItem;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupTabBarAndNavigation];
    
    [_list getListItemsFromServer];
    
    _tableView.settings = _settings;
    _pullAddNewBehaviour.settings = _settings;
    
    _tableView.frame = CGRectMake(self.view.frame.origin.x,
                                  self.view.frame.origin.y,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height - SZ_TAB_BAR_HEIGHT);
    
    self.view.backgroundColor = SKCOLOR_TabBarBg;
}

- (void) dealloc
{
    [self unSubscribeNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

//MARK: Notifications
- (void) subscribeNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateList) name:kNotificationListUpdate object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateListItems) name:kNotificationItemsUpdate object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addedListItem:) name:kNotificationItemAdded object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateListItem:) name:kNotificationItemUpdated object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removedListItem:) name:kNotificationItemRemoved object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateItemsOrder:) name:kNotificationItemsOrder object:nil];
}

- (void) unSubscribeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) updateListItems
{
    _listItems = nil;
    
    //TODO: Это для создания скрина
    //потом закоментить
    /*
    if (_list.items.count == 0) {
        NSArray *screenListPhrasesArray = @[@"People say nothing is impossible, but I do nothing every day.",
                                            @"My darling, my lover, my beautiful wife:Marrying you screwed up my life.",
                                            @"There are two theories to arguing with women. Neither one works.",
                                            @"I'm on a seafood diet. Every time I see food, I eat it.",
                                            @"Why does a round pizza come in a square box?",
                                            @"Don't follow me, I am lost too!",
                                            @"Gone crazy... Be back soon..",
                                            @"I think... therefore I'm single.",
                                            @"Entrance Only - DO NOT ENTER",
                                            @"Can you cry under water?",
                                            @"It's not lost. I just can't find it."];
        
        NSMutableArray *screenListItems = [[NSMutableArray alloc] init];
        
        for (NSString *phrase in screenListPhrasesArray) {
            
            SCListItem *item = [[SCListItem alloc] initWithText:phrase];
            
            [screenListItems addObject:item];
        }
        
        _listItems = screenListItems;
    }
    */
    
    _listItems = [_list.items mutableCopyListItem]; //TODO: это надо раскоментить
    [_tableView reloadData];
}

- (void) updateList
{
    FTLog(@"%@", NSStringFromSelector(_cmd));
}

- (void) addedListItem:(NSNotification*)notification
{
    if ([[notification object] isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *transportDic = [notification object];
        BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
        
        SCListItem *transportItem = transportDic[@"item"];
        SCPUUID *searchPUUID = transportDic[@"itemOldPUUID"];
        
        if (isSuccess)
        {
            SCPUUID *rightPUUID =  transportItem.puuid;
            
            for (SCListItem *item in _listItems) {
                if ([item.puuid isEqual:searchPUUID]) {
                    item.puuid = rightPUUID;
                    [_tableView changeCellItemPUUID:searchPUUID on:rightPUUID];
                    return;
                }
            }
            [_tableView reloadData];
        }
        else
        {
            [_listItems removeObject:transportItem]; //xорошо бы возвращать item с анимацией
            [_tableView reloadData];
        }
    }
    else
    {
        FTLog(@"Error: addedListItem:(NSNotification*)notification get Not Dictionary");
    }
}

- (void) updateListItem:(NSNotification*)notification
{
    if ([[notification object] isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *transportDic = [notification object];
        BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
        
        if (!isSuccess) {
            _listItems = nil;
            _listItems = [_list.items mutableCopyListItem];//хорошо бы обновлять item с анимацией
            [_tableView reloadData];
        }
        
    } else {
        FTLog(@"Error: updateListItem:(NSNotification*)notification get Not Dictionary");
    }
}

- (void) removedListItem:(NSNotification*)notification
{
    if ([[notification object] isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *transportDic = [notification object];
        BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
        
        if (!isSuccess)
        {
            _listItems = nil;
            _listItems = [_list.items mutableCopyListItem];//хорошо бы возвращать item с анимацией
            [_tableView reloadData];
        }
        
    } else {
        FTLog(@"Error: removedListItem:(NSNotification*)notification get Not Dictionary");
    }
}

- (void) updateItemsOrder:(NSNotification*)notification
{
    if ([[notification object] isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *transportDic = [notification object];
        BOOL isSuccess = [transportDic[kKeySuccess] boolValue];
        
        if (!isSuccess)
        {
            _listItems = nil;
            _listItems = [_list.items mutableCopyListItem]; //хорошо бы возвращать порядок item с анимацией
            [_tableView reloadData];
        }
        
    } else {
        FTLog(@"Error: updateItemsOrder:(NSNotification*)notification get Not Dictionary");
    }
    
}

//MARK: Setups
- (void) setupTabBarAndNavigation
{
    self.tabBarController.navigationItem.titleView = nil;
    self.tabBarController.title = @"List";
    self.tabBarController.navigationItem.title = @"List";
    
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationController.title = @"List";
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_NavBarTitleFont,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameDefult size:FT_FontSizeNavTitle]};
    self.navigationController.navigationBar.titleTextAttributes = attribute;
}

//MARK: SHCTableViewCellDelegate methods
- (void) cellDidBeginEditing:(SCLTableViewCell *)editingCell
{
    _editingOffsetY = - editingCell.frame.origin.y;
    for (SCLTableViewCell *cell in [_tableView visibleCells]) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             //Перенос cell вверх
                             cell.transform = CGAffineTransformMakeTranslation(0, _editingOffsetY);
                             if (cell != editingCell) {
                                 cell.alpha = 0.3;
                             }
                         }
         ];
    }
}

- (void) cellDidEndEditing:(SCLTableViewCell *)editingCell saveItem:(BOOL)isSaveItem
{
    switch (_editingBehavior) {
            
        case SCEditingBehaviorAddItem: //В режиме создания нового Item
        {
            if (editingCell.item.text.length > 0) { //Если ячейка не пустая
                
                if (isSaveItem) {
                    [_list addItem:editingCell.item]; //Добавляем новый item на сервер
                }
                
                for (SCLTableViewCell *cell in [_tableView visibleCells]) {
                    [UIView animateWithDuration:0.3
                                     animations:^{
                                         //Возвращает на место cell
                                         cell.transform = CGAffineTransformIdentity;
                                         if (cell != editingCell) {
                                             cell.alpha = 1.0;
                                         }
                                     }
                     ];
                }
                
            } else { //удаляем ячейку если она пустая, без обращения к серверу
                    [self itemDeleted:editingCell.item onServer:isSaveItem];
            }
        
            _editingBehavior = SCEditingBehaviorEditItem;
        }
            break;
            
        case SCEditingBehaviorEditItem: //В режиме редактирования существующего Item
        {
            if (editingCell.item.text.length > 0 ) { //Если текст в ячейке есть
                
                if (isSaveItem) {
                    //Обновляем Item
                    [_list updateItem:editingCell.item];
                }
                
                for (SCLTableViewCell *cell in [_tableView visibleCells]) {
                    [UIView animateWithDuration:0.3
                                     animations:^{
                                         cell.transform = CGAffineTransformIdentity;
                                         if (cell != editingCell) {
                                             cell.alpha = 1.0;
                                         }
                                     }
                     ];
                }
                
            } else { //Редактирование приравнивается к удалению
                    [self itemDeleted:editingCell.item onServer:isSaveItem]; //YES удаляем ячейку и item на сервере
            }
        }
            break;
            
        default:
            break;
    }
}

- (void) itemDeleted:(SCListItem *)itemDeleted onServer:(BOOL)isDeleteItemOnServer
{
    float delay = 0.0;
    
    if (_editingBehavior == SCEditingBehaviorAddItem) {
        
        //Удаление item из локального массива Item
        [_listItems removeObject:itemDeleted];
        
        NSArray *visibleCells = [_tableView visibleCells];
        
        UIView *lastView = [visibleCells lastObject];
        bool startAnimating = false;
        
        for (SCLTableViewCell *cell in visibleCells)
        {
            
            if (startAnimating)
            {
                [UIView animateWithDuration:0.3
                                      delay:delay
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     //смещение ячейки вверх на величину высоты этой ячейки
                                     cell.frame = CGRectOffset(cell.frame, 0.0f, -cell.frame.size.height);
                                 }
                                 completion:^(BOOL finished) {
                                     if (cell == lastView)
                                     {
                                         [_tableView reloadData];
                                     }
                                 }];
                delay+=0.03;
            }
            
            //Если добрались до удаляемой ячейки
            if (cell.item == itemDeleted)
            {
                startAnimating = true;
                cell.label.text = @"";
                cell.hidden = YES;
                
                if (_listItems.count == 0) {
                    [_tableView reloadData];
                }
            }
        }
        
    } else {
        if (isDeleteItemOnServer) {
            [_list removeItem:itemDeleted];
        }
        
        //Удаление item из локального массива Item
        [_listItems removeObject:itemDeleted];
        
        NSArray *visibleCells = [_tableView visibleCells];
        
        UIView *lastView = [visibleCells lastObject];
        bool startAnimating = false;
        
        for (SCLTableViewCell *cell in visibleCells)
        {
            if (startAnimating)
            {
                [UIView animateWithDuration:0.3
                                      delay:delay
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     //смещение ячейки вверх на величину высоты этой ячейки
                                     cell.frame = CGRectOffset(cell.frame, 0.0f, -cell.frame.size.height);
                                 }
                                 completion:^(BOOL finished) {
                                     if (cell == lastView)
                                     {
                                         [_tableView reloadData];
                                     }
                                 }];
                delay+=0.03;
            }
            
            //Если добрались до удаляемой ячейки
            if (cell.item == itemDeleted)
            {
                startAnimating = true;
                cell.hidden = YES;
                
                if (_listItems.count == 0) {
                    [_tableView reloadData];
                }
            }
        }
    }
    
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES]; //hide keyboard
}

- (void) moveItem:(SCListItem *)item directionMotion:(SCDirectionOfMotion)directionOfMotion
{
    NSInteger itemIndex = [_listItems indexOfObject:item];
    NSArray *visibleCells = [_tableView visibleCells];
    
    float animateWithDuration = 0.3;
    float delay = 0.3;
    
    switch (directionOfMotion) {
        case SCDirectionOfMotionUp:
        {
            if (itemIndex != 0) {
                [_listItems exchangeObjectAtIndex:itemIndex withObjectAtIndex:(itemIndex - 1)];
                
                for (int i = 0; i < visibleCells.count; i++) {
                    
                    SCLTableViewCell *cell = visibleCells[i];
                    
                    if (cell.item == item) {
                        
                        SCLTableViewCell *cellPrev = visibleCells[i-1];
                        
                        [UIView animateWithDuration:animateWithDuration
                                              delay:delay
                                            options:UIViewAnimationOptionTransitionCurlUp
                                         animations:^{
                                             cell.frame = CGRectOffset(cell.frame, 0.0f, -cell.frame.size.height);
                                             cellPrev.frame = CGRectOffset(cellPrev.frame, 0.0f, cellPrev.frame.size.height);
                                         } completion:^(BOOL finished) {
                                             //FTLog(@"animate end");
                                         }];
                    }
                }
            }
        }
            break;

        case SCDirectionOfMotionDown:
        {
            if (itemIndex != (_listItems.count-1)) {
                [ _listItems exchangeObjectAtIndex:itemIndex withObjectAtIndex:(itemIndex + 1)];
                
                for (int i = 0; i < visibleCells.count; i++) {
                    
                    SCLTableViewCell *cell = visibleCells[i];
                    
                    if (cell.item == item) {
                        
                        SCLTableViewCell *cellNext = visibleCells[i+1];
                        
                        [UIView animateWithDuration:animateWithDuration
                                              delay:delay
                                            options:UIViewAnimationOptionTransitionCurlDown
                                         animations:^{
                                             cell.frame = CGRectOffset(cell.frame, 0.0f, cell.frame.size.height);
                                             cellNext.frame = CGRectOffset(cellNext.frame, 0.0f, -cellNext.frame.size.height);
                                         
                                         } completion:^(BOOL finished) {
                                             //FTLog(@"animate end");
                                         }
                         ];
                    }
                }
            }
        }
            break;
            
        default:
            break;
    }
}

- (void) itemsDidEndOrdering
{
    if ([self itemsOrderChanged]) {
        FTLog(@"Порядок Items меняем на сервере");
        [_list setItemsOrder:[self itemsPUUID]];
    }
}

- (NSArray *) itemsPUUID {
    
    NSMutableArray *itemsPUUID = [NSMutableArray new];
    
    for (SCListItem *item in _listItems) {
        [itemsPUUID addObject:item.puuid];
    }
    
    return itemsPUUID;
}

- (BOOL) itemsOrderChanged
{
    if (_listItems.count == _list.items.count) {
        for (NSInteger i = 0; i < _listItems.count; i++) {
            
            SCListItem *localItem = _listItems[i];
            SCPUUID *localItemPUUID = localItem.puuid;
            
            SCListItem *sourceItem = _list.items[i];
            SCPUUID *sourceItemPUUID = sourceItem.puuid;
            
            if (![localItemPUUID isEqual:sourceItemPUUID]) {
                return YES;
            }
        }
    }
    return NO;
}

//MARK: SHCCustomTableViewDataSource methods
- (NSInteger) numberOfRows
{
    return _listItems.count;
}

- (UIView *) cellForRow:(NSInteger)row
{
    //Создание переиспользуемой ячейки таблицы
    SCLTableViewCell *cell = (SCLTableViewCell*) [_tableView dequeueReusableCell];
    
    //И указание данных item для этой ячейки
    NSInteger index = row;
    SCListItem *item = _listItems[index];
    
    cell.item = item;
    cell.settings = _settings;
    cell.delegate = self;
    
    return cell;
}

- (void) itemAdded
{
    if (_editingBehavior == SCEditingBehaviorEditItem) {
        [self itemAddedAtIndex:0];
        
    } else {
        [self.view endEditing:YES];
        [self itemAddedAtIndex:0];
    }
}

- (void) itemAddedAtIndex:(NSInteger)index
{
    //Create the new item
    //Создание нового пустого Item и добавлени его в локальный массив
    SCListItem *itemAdd = [SCListItem itemRandomPUUIDWithText:@""];
    [_listItems insertObject:itemAdd atIndex:index];
    
    _editingBehavior = SCEditingBehaviorAddItem;
    
    //Refresh the table
    [_tableView reloadData];
    
    //Enter edit mode
    //Перебираем все видимые ячейки таблицы
    //и если эта ячейка с нашим item
    //то устанавливаем в неё редактирование
    SCLTableViewCell *editCell;
    for (SCLTableViewCell *cell in _tableView.visibleCells)
    {
        if ([cell.item isEqualItem:itemAdd])
        {
            editCell = cell;
            break;
        }
    }
    [editCell.label setEnabled:YES];
    [editCell.label becomeFirstResponder];
}

@end
