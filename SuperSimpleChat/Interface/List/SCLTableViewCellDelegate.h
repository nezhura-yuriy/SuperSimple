//
//  SCLTableViewCellDelegate.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 19.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCListItem.h"

typedef NS_ENUM(NSInteger, SCDirectionOfMotion) {
    SCDirectionOfMotionUp = 0,
    SCDirectionOfMotionDown
};

@class SCLTableViewCell;

#import <Foundation/Foundation.h>

@protocol SCLTableViewCellDelegate <NSObject>

- (void) itemDeleted:(SCListItem*) itemDeleted onServer:(BOOL) deleteItemOnServer;
- (void) moveItem:(SCListItem *) item directionMotion:(SCDirectionOfMotion) directionOfMotion;
- (void) itemsDidEndOrdering;

- (void) cellDidBeginEditing:(SCLTableViewCell*) cell;
- (void) cellDidEndEditing:(SCLTableViewCell*) cell saveItem:(BOOL) saveItem;

@end