//
//  SCDatePickerViewController.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 18.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCDatePickerViewController.h"

@implementation SCDatePickerViewController
{
    UIButton *_bigButtonDismiss;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupDataDatePicker];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_birthdate) {
        [_datePicker setDate:_birthdate animated:NO];
    }
    
    _bigButtonDismiss = [UIButton buttonWithType:UIButtonTypeCustom];
    [_bigButtonDismiss addTarget:self action:@selector(dismissModalView) forControlEvents:UIControlEventTouchUpInside];
    _bigButtonDismiss.backgroundColor = [UIColor clearColor];
    [_bigButtonDismiss setTitle:@"" forState:UIControlStateNormal];
    
    [self.view addSubview:_bigButtonDismiss];
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

//MARK: Setup
- (void) setupDataDatePicker
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *today = [NSDate date];
    NSDateComponents *minusHundredYears = [NSDateComponents new];
    minusHundredYears.year = -100;
    NSDate *hundredYearsAgo = [calendar dateByAddingComponents:minusHundredYears
                                                        toDate:today
                                                       options:0];
    
    NSDateComponents *minusTenYears = [NSDateComponents new];
    minusTenYears.year = -10;
    NSDate *tenYearsAgo = [calendar dateByAddingComponents:minusTenYears
                                                    toDate:today
                                                   options:0];
    
    _datePicker.calendar = calendar;
    _datePicker.maximumDate = today;
    _datePicker.minimumDate = hundredYearsAgo;
    [_datePicker setDate:tenYearsAgo animated:NO];
    _datePicker.locale = [NSLocale currentLocale];
    _datePicker.datePickerMode = UIDatePickerModeDate;
}

- (void) setBirthdate:(NSDate *)birthdate
{
    _birthdate = birthdate;
}

- (void) setupUI
{
    _backgroundView.frame = CGRectMake(0,
                                       self.view.frame.size.height - (162 + 44),
                                       self.view.frame.size.width,
                                       162 + 44);
    
    _datePicker.frame = CGRectMake(0,
                                   44,
                                   _backgroundView.frame.size.width,
                                   162);
    
    _toolbar.frame = CGRectMake(0, 0,
                                _backgroundView.frame.size.width,
                                44);
    
    _bigButtonDismiss.frame = CGRectMake(0, 0,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - (_backgroundView.frame.size.height));
}

- (IBAction)deleteButtonPressed:(id)sender {
    
    if ([_delegate respondsToSelector:@selector(setBirthdate:)]) {
        [_delegate setBirthdate:nil];
    }
    [self dismissModalView];
}

- (IBAction)doneButtonPressed:(id)sender {
    
    if ([_delegate respondsToSelector:@selector(setBirthdate:)]) {
        [_delegate setBirthdate:_datePicker.date];
    }
    [self dismissModalView];
}

- (void) dismissModalView
{
    if ([_delegate respondsToSelector:@selector(closeModalView)]) {
        [_delegate closeModalView];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
