//
//  SCAbstractButtonItem.m
//  SmartChat
//
//  Created by Yury Radchenko on 2/25/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractButtonItem.h"
//#import "SCSettings.h"


@implementation SCAbstractButtonItem

@synthesize settings = _settings;

- (id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        _settings = [SCSettings sharedSettings];
    }
    return self;
}

- (id) initWithFrame: (CGRect) frame forList:(SCList *) list itemIndex:(NSInteger) itemIndex {
    
    self = [self initWithFrame:frame];
    
    self.tag = itemIndex;
    
    return self;
}

@end
