//
//  SCContactCell.m
//  SmartChat
//
//  Created by Alexsandr Linnik on 13.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCContactCell.h"
#import "UIImage+SCCropImageToCircleWithBorder.h"
#import "SCSettings.h"
#import "UIImageView+Letters.h"
#import "SCPUUID.h"

static NSString *kStatusOnlineImgName = @"status-online";
static NSString *kStatusBusyImgName = @"status-busy";
static NSString *kStatusAwayImgName = @"status-away";
static NSString *kStatusInvisibleImgName = @"status-invisible";
static NSString *kStatusOfflineImgName = @"status-offline";

@implementation SCContactCell
{
    SCSettings* _settings;
}

- (void)awakeFromNib {
    // Initialization code
    
//    UIImage* image = self.userFoto.image;
//    self.userFoto.image = [image cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:20];
    _settings = [SCSettings sharedSettings];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) fillWhithContact: (SCContactItem*) contact{
    
    _contact = contact;

    
    [_contact.user addDelegate:self];
    
    [self changeStatus:_contact.user.contactStatus];
    
    
    if (contact.fullName.length>0) {
        
        self.userNameLabel.text = contact.fullName;
        
        if (![contact.fullName isEqualToString:contact.user.contactNikName]){
            
            self.nikNameLabel.text = [NSString stringWithFormat:@"@%@", contact.user.contactNikName];
            self.nikNameLabel.hidden = NO;
        }
 
        
    } else {

    if (!contact.contactLastName.length==0 || !contact.contactFirstName.length==0) {
        
        NSString* fullName = [NSString new];

        if (contact.contactLastName.length > 0) {

            fullName = [NSString stringWithFormat:@"%@ ",contact.contactLastName];

        }

        if (contact.contactFirstName.length > 0) {

            fullName = [fullName stringByAppendingString:contact.contactFirstName];

        }
        
        self.userNameLabel.text = fullName;

        
        } else if (!contact.contactNikName.length == 0) {
            
            self.userNameLabel.text = contact.contactNikName;
            
        } else  if (!contact.contactPhone.length == 0) {
            
            self.userNameLabel.text = contact.contactPhone;

        } else {
            
            self.userNameLabel.text = contact.contactEmail;

        }
        
    
    }

    [self.userFoto setImageWithString:self.userNameLabel.text color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
    
    [_contact.user.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        
         switch (type)
        
         {
             case SCNetFileComletionTypeStatus:
                 
                 FTLog(@"SCNetFileComletionTypeStatus");
                 
                 break;
                 
             case SCNetFileComletionTypeCompleet:
             {
                 [self.userFoto endProgress];
                 
                 if([result isKindOfClass:[SCNetFile class]])
                 {
                     self.userFoto.image = [[(SCNetFile*)result imageForSize:self.userFoto.frame.size] cropToCircleWithBorderColor:[UIColor whiteColor] lineWidth:2];
                 }
             }
                 break;
                 
             case SCNetFileComletionTypeProgress:
                 [self.userFoto setProgressPercent:[result doubleValue]];
                 break;
                 
             case SCNetFileComletionTypeError:
                 [self.userFoto endProgress];
                 break;
                 
             default:
                 FTLog(@"default case");
                 break;
         }
     }];
    
    [_settings.modelUsers addDelegate:self contactId:contact.userPUUID];
    
    self.userNameLabel.font = [UIFont fontWithName:FT_FontNameDefult size:15];
    
    self.userNameLabel.textColor = SKCOLOR_TableCellFont;
    
    self.nikNameLabel.textColor = [UIColor lightGrayColor];
    
    self.nikNameLabel.font = [UIFont fontWithName:FT_FontNameDefult size:10];

    
    self.backgroundColor = SKCOLOR_TableCellBg;
    
    self.statusViewOutlet.image = [self.statusViewOutlet.image cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
    
    [_settings.modelUsers checkUserInBlackList:contact];
    
    self.creatChatOutlet.hidden = YES;
    
    [self.creatChatOutlet setImage:[_settings.skinManager getImageForKeyX:@"tabbar-chats"] forState:UIControlStateNormal];
    
}

- (IBAction)creatChatAction:(id)sender {
    
    [self.delegate creatChatWithContactAtIndexPath:self.indexPath];
}


-(void)changeStatus:(SCUserStatus) status{
    
    switch (status) {
            
        case SCUserStatusAway:
         
            self.statusViewOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusAwayImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];
            
            break;
            
        case SCUserStatusBusy:
            
            self.statusViewOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusBusyImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];

            break;
            
        case SCUserStatusInvisible:
            
            self.statusViewOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusInvisibleImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];

            
            break;
            
        case SCUserStatusOffline:
            
            self.statusViewOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusInvisibleImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];

            
            break;
            
        case SCUserStatusOnline:
            
            self.statusViewOutlet.image = [[_settings.skinManager getImageForKeyX:kStatusOnlineImgName]cropToCircleWithBorderColor:[UIColor whiteColor]  lineWidth:3];

            
            break;
            
        default:
            break;
            
    }
    
}


#pragma mark - SCPBManagerDelegate


-(void)parsedType:(SCPBManagerParceType)type data:(id)data
{
    
    switch (type)
    {
        case SCPBManagerParceTypeChangeStatusEvent:
        case SCPBManagerParceTypeChangeStatus:
        {
            if([data isKindOfClass:[SCContactItem class]])
                
                [self changeStatus:((SCContactItem*) data).contactStatus];
        }
            break;
            
        default:break;
    }
}

#pragma mark - SCContactItemDelegate


-(void)checkUserInBlackListResponse:(id)data{
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        
        self.contact.isBlocked = [[data objectForKey:@"blackListStatus"] boolValue];
        
        if (!self.contact.isBlocked) {
            
            self.creatChatOutlet.hidden = NO;
        }

    }
    
}


- (void)dealloc
{
  [_settings.modelUsers delDelegate:self contactId:_contact.userPUUID];
    
    [_contact.user delDelegate:self];

}

-(void)prepareForReuse

{
    
    [_settings.modelUsers delDelegate:self contactId:_contact.userPUUID];


    [_contact.user delDelegate:self];
    
    self.creatChatOutlet.hidden = YES;
}

@end
