//
//  SCWarningView.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 15.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSettings.h"

@protocol SCWarningViewProtocol <NSObject>
- (void) gotoListController;
@end

@interface SCWarningView : UIView

@property (nonatomic, weak) id <SCWarningViewProtocol> delegate;

- (instancetype)initWithFrame:(CGRect)frame withSettings:(SCSettings *) settings NS_DESIGNATED_INITIALIZER;

@end
