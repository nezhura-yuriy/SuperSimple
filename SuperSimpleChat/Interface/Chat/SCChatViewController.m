//
//  SCChatViewController.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatViewController.h"
#import "SCChatUsersViewController.h"
#import "SCNewChatViewController.h"
#import "UIImageView+Letters.h"
#import "ContactInformationViewController.h"
#import "SCChatUsersView.h"
#import "SCSettings.h"
#import "SCBottomMenuView.h"
#import "SCChatItem.h"
#import "SCNetFile.h"
#import "SCMessageItem.h"
#import "SCMessageBodys.h"
#import "SCMessageGroup.h"

#import "SCMessagesByDateView.h"
//#import "SCChatItemView.h"
#import "SCChatBottomsView.h"
#import "SCChatListView.h"
#import "SCChatPhotoMenu.h"
//#import "SCChatAudioMenu.h"
//#import "SCChatVideoMenu.h"
#import "SCChatUserMenu.h"
#import "SCMessageBodyItem.h" //add YR
#import "SCContactItem.h"
#import "SCModelChats.h"
#import "SCList.h"
#import "SCImageViewWithIndicator.h"
#import "SCChatTopMenuView.h"
#import "SCChatLogoView.h"

//#import "SCBottomMenuViewSec.h"

#define MARGIN 10.0f
#define CHAT_IMAGE_SIZE 44.0f
#define SEPARATOR_HEIGHT 2.0f
#define DEFAULT_ANIMATION_DURATION 0.25

#define PULLREFRESH_TEXTINFO @"Refresh"
#define PULLREFRESH_TEXTPULL @"Pull to refresh"
#define PULLREFRESH_TEXTRELEASE @"Push to refresh"
#define PULLREFRESH_TEXTLOADING @"Refreshing ...."

//#define SEND_FIELD_IMAGE 

//Radchenko
static const NSTimeInterval kVideoMaximumDuration = 60; //sec
static NSString *kMediaLibraryMenu = @"Media Library";
static NSString *kFotoVideoMenu = @"Make a photo or video";

@implementation SCChatViewController
{
    UIButton* _btnFieldSend;
    NSMutableArray* _daysViewsItems;
    SCBottomMenuView* _bottomMenuView;
    SCMessageBodys* _messageBodys;
    CGFloat scrollPosY0;
    //Radchenko
    UIImagePickerController *_fotoVideoPicker;
    UIImagePickerController *_mediaLibraryPicker;
    NSInteger messageDestroyTime;
}


-(void) awakeFromNib
{
    [super awakeFromNib];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _isAutoDestroy = NO;
    messageDestroyTime = 0;

    _btnFieldSend = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnFieldSend setImage:[UIImage imageNamed:@"btn_plus"] forState:UIControlStateNormal];
    [_btnFieldSend addTarget:self action:@selector(fieldBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_btnFieldSend setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, _fldMessage.bounds.size.height*0.12)];
    _btnFieldSend.frame = CGRectMake(0, 0, _fldMessage.bounds.size.height*0.82, _fldMessage.bounds.size.height*0.7);
    UIView* borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, _fldMessage.bounds.size.height)];
    _fldMessage.rightViewMode = UITextFieldViewModeAlways;
    _fldMessage.rightView = _btnFieldSend;
    _fldMessage.leftViewMode = UITextFieldViewModeAlways;
    _fldMessage.leftView = borderView;
    _fldMessage.placeholder = @"Write a message";
    _fldMessage.layer.cornerRadius = 5;
    
//    _messageViewsItems = [[NSMutableArray alloc] init];
    _daysViewsItems = [[NSMutableArray alloc] init];
    _messageBodys = [[SCMessageBodys alloc] initWithSettings:_settings];
    _messageListsScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height*2);
    _messageListsScrollView.contentOffset = CGPointMake(0,0);

    scrollPosY0 = _workspaceScrollView.frame.origin.y;
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    [_chatLogoView addGestureRecognizer:tapGesture];
    
}

-(void) dealloc
{
    FTLog(@"%@ dealloc",NSStringFromClass([self class]));
    [_chatLogoView clear];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    
    [_chatItem loadUserList];
    
    [_btnBack setImage:[_settings.skinManager getImageForKey:@"btn_back"] forState:UIControlStateNormal];

    [_settings.modelChats addDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [self updateUI];

}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

-(void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [_settings.modelChats delDelegate:self];
    [_settings.modelProfile.protoBufManager delDelegate:self forType:SCPBManagerParceTypeSendMessage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

-(void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    [_chatLogoView setSettings:_settings];
//    _topMenuView.settings = settings;
}

-(void) setChatItem:(SCChatItem *)chatItem
{
    if(_chatItem)
        [_chatItem delDelegate:self];

    _chatItem = chatItem;
    [_chatItem addDelegate:self];
    _labChatName.text = _chatItem.chatName;
    
//    [_chatItem itemGetLastReadDate];
    [_chatItem itemSetLastReadDate:[NSDate date]];
    [_chatLogoView setLogoSource:_chatItem.logoSource];
    /*
//    [_chatItem.netFile showLogo:_imageChat];
    [_imageChat setImageWithString:_chatItem.chatName color:[UIColor colorWithHue:0.41 saturation:0 brightness:0.737 alpha:1] circular:YES];
    [_chatItem.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result) {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                break;
                
            case SCNetFileComletionTypeProgress:
                [_imageChat setProgressPercent:[result doubleValue]];
                break;
                
            case SCNetFileComletionTypeError:
                FTLog(@"%@ SCNetFileComletionTypeError обработать",NSStringFromClass([self class]));
                [_imageChat endProgress];
                break;
                
            case SCNetFileComletionTypeCompleet:
                _imageChat.image = [_chatItem.netFile imageForSize:_imageChat.frame.size];
                [_imageChat endProgress];
                break;
                
            default:
                break;
        }
    }];
    */
    /*
    [_chatItem.netFile getWithCompletionblock:^(SCNetFileComletionType type, id result)
    {
        switch (type)
        {
            case SCNetFileComletionTypeStatus:
                break;
            case SCNetFileComletionTypeProgress:
                
                break;
            case SCNetFileComletionTypeError:
                break;
            case SCNetFileComletionTypeCompleet:
                break;
                
            default:
                break;
        }
    }];
    */
    if(_chatItem.chatType == SCChatTypePublic && ![_chatItem.usersInChat containsObject:_settings.modelProfile.contactItem])
    {
        FTLog(@"is new");
        // новая комната
        // спрятать сенд

        // загрузить итемы
    }
    else
    {
        [self _makeDaysView];
    }
    CGRect rr = CGRectMake(0, _messageListsScrollView.contentSize.height - _messageListsScrollView.bounds.size.height, _messageListsScrollView.bounds.size.width, _messageListsScrollView.bounds.size.height);
    [_messageListsScrollView scrollRectToVisible:rr animated:YES];
/*
    // определить активный день
    SCMessageGroup* mainGroup = [_chatItem searchGroup:_chatItem.lastMessage.messageDateTime];
    [self _makeDayView:mainGroup];

    // переместиться на сообщение
*/
}

-(void) updateUI
{
//    _imageChat.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    _topView.backgroundColor = [UIColor clearColor];
    [_btnFieldSend setImage:[_settings.skinManager getImageForKeyX:@"btn_plus"] forState:UIControlStateNormal];
    [_labChatName  setFont:[UIFont fontWithName:FT_FontNameRegular size:14.0]];
    _navBarView.backgroundColor = SKCOLOR_NavBarBg;
    self.view.backgroundColor = SKCOLOR_Background;
    _botomView.backgroundColor = SKCOLOR_TabBarBg;
    [_btnSend setTitleColor:SKCOLOR_FontRegular forState:UIControlStateNormal];
    [_btnSend.titleLabel setFont:[UIFont fontWithName:FT_FontNameBold size:14.0]];
    _fldMessage.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _fldMessage.layer.borderWidth = 1;
    [_fldMessage setFont:[UIFont fontWithName:FT_FontNameRegular size:16.0]];
    _imgBottomSeparator.backgroundColor = [UIColor lightGrayColor];
}


#pragma mark - show content
-(void) _makeDaysView
{
    FTLog(@"Begin make dayly view");
    
    for(SCMessageGroup* group in _chatItem.chatDayMessages)
    {
//        FTLog(@"%@",group);
        [self _makeDayView:group];
    }
    
    FTLog(@"Make dayly view compleet");
    
    [self _placeDaysView];
}

-(void) _makeDaysViewWidthArray:(NSArray*) newGroups
{
    for(SCMessageGroup* group in newGroups)
    {
        [self _makeDayView:group];
    }
    [self _placeDaysView];
}

-(void) _makeDayView:(SCMessageGroup*) group
{
    if(group == nil)
        return;
    
    for(SCMessagesByDateView* groupItem in _daysViewsItems)
    {
        if([groupItem.messageGroup.messagesDate isEqualToDate:group.messagesDate])
        {
            [groupItem updateGroupMessages];
            return;
        }
    }
    
    SCMessagesByDateView* daylyView = [[SCMessagesByDateView alloc] initWithSettings:_settings];
    daylyView.forWidth = self.view.frame.size.width;
    [daylyView setMessageGroup:group];
    [_daysViewsItems insertObject:daylyView atIndex:0];
    [_messageListsScrollView addSubview:daylyView];
}


-(void) _placeDaysView
{
    FTLog(@"Begin place dayly view");
    FTLog(@"Sort Days View");
    [self _sortDaysView];
    FTLog(@"Sort Days View");
    
    [UIView animateWithDuration:0.25 animations:^{
        CGFloat posY = 0;
        for(SCMessagesByDateView* daylyView in _daysViewsItems)
        {
            daylyView.frame = CGRectMake(0,posY,self.view.bounds.size.width,daylyView.viewSize.height);
            daylyView.alpha = 1.0;
            posY +=daylyView.viewSize.height;
        }
        
        _messageListsScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, MAX(posY + MARGIN,_messageListsScrollView.frame.size.height + 1));
    } completion:^(BOOL finished)
    {
        FTLog(@"Place dayly view compleet");
    }];

}
-(void) _sortDaysView
{
    [_daysViewsItems sortUsingComparator:^NSComparisonResult (id a, id b)
     {
         
         SCMessagesByDateView* groupItemA = (SCMessagesByDateView*)a;
         SCMessagesByDateView* groupItemB =(SCMessagesByDateView*)b;
         
         
         NSComparisonResult result = [groupItemA.messageGroup.messagesDate compare:groupItemB.messageGroup.messagesDate];
         return result;
     }];
}

#pragma mark - nav content
-(void) positionMessageBottom:(SCMessageItem*) theItem
{
    
}

#pragma mark - Actions
-(void) fieldBtnAction:(id) sender
{
    [_fldMessage resignFirstResponder];
    
    if(!_bottomMenuView)
    {
        SCBottomMenuView* menu = [[SCBottomMenuView alloc] initWithSettings:_settings];
        menu.chatType = _chatItem.chatType;
        menu.delegate = self;
        menu.messageBodys = _messageBodys;
        menu.delegate = self;
        
        CGSize menuSize = CGSizeMake(self.view.bounds.size.width, [menu sizeView].height);
        menu.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
        menu.alpha = 0.0f;
        [self.view addSubview:menu];
        _bottomMenuView = menu;
        [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
            
            menu.frame = CGRectMake(0, self.view.bounds.size.height - menuSize.height, menuSize.width, menuSize.height);
            menu.alpha = 1.0f;
            _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.bounds.size.width, self.view.bounds.size.height - scrollPosY0 - menuSize.height);
        }];
    }
    else
    {
        [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
            _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.bounds.size.width, self.view.bounds.size.height - scrollPosY0);
            _bottomMenuView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
            _bottomMenuView.alpha = 0.0;
            _workspaceScrollView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [_bottomMenuView removeFromSuperview];
            _bottomMenuView = nil;
        }];
    }
}

- (IBAction)btnBackAction:(id)sender
{
    if(_chatItem)
        [_chatItem delDelegate:self];

    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void) loadHistory
{
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
//                                   self, @"delegate",
//                                   [[SCPUUID alloc] initWithRandom], @"UUID",
//                                   [NSNumber numberWithInteger:SCPBManagerParceTypeGetChatRoomHistory], @"type",
//                                   _chatItem,@"SCChatItem",
//                                   [NSNumber numberWithInteger:_messageViewsItems.count],@"offset",
//                                   [NSNumber numberWithInteger:20],@"limit",
//                                   nil];
//    [_settings.modelProfile.protoBufManager getChatRoomHistory:params];

    [_chatItem loadHistory:0];

}

#pragma mark - top menu actions
-(void)selectTopItem:(SCChatTopMenuViewItem) item
{
    switch (item)
    {
        case SCChatTopMenuViewItemPhoto:
        {
        }
        default:
            break;
    }
}

#pragma mark - bottom actions
-(void)selectItem:(SCPopChatMenuViewItem) selectItem
{
    switch (selectItem) {
        case SCPopChatMenuViewItemPhoto:
        {
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:nil];
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                [actionSheet addButtonWithTitle:kMediaLibraryMenu];
                
            }
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                [actionSheet addButtonWithTitle:kFotoVideoMenu];
            }
            
            actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
            
            [actionSheet showInView:self.view];
            
            [self hideBottomMenu];

        }
            break;
            
        case SCPopChatMenuViewItemAudio:
        {
            FTLog(@"select Audio source");
            [self hideBottomMenu];

        }
            break;
        case SCPopChatMenuViewItemVideo:
        {
            FTLog(@"select Video source");
            [self hideBottomMenu];

        }
            break;
            
        case SCPopChatMenuViewItemUser:
        {
            FTLog(@"select User source");
            
            SCChatUsersViewController* pSCChatUsersViewController = [[SCChatUsersViewController alloc] init];
            pSCChatUsersViewController.settings = _settings;
            pSCChatUsersViewController.userView.chatItem = _chatItem;
            pSCChatUsersViewController.userInRoom = _chatItem.usersInChat;
            [self.navigationController pushViewController:pSCChatUsersViewController animated:YES];
            
            [self hideBottomMenu];


        }
            break;
            
        case SCPopChatMenuViewItemSettings:
        {
            CGSize menuSize = CGSizeMake(self.view.bounds.size.width, [_bottomMenuView sizeView].height);
            [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
                
                _bottomMenuView.frame = CGRectMake(0, self.view.bounds.size.height - menuSize.height, menuSize.width, menuSize.height);
                _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.bounds.size.width, self.view.bounds.size.height - scrollPosY0 - menuSize.height);
            }];

        }
            break;
        
        case SCPopChatMenuViewItemList:
        {
            FTLog(@"select list source");
            SCChatListView *listView = [[[NSBundle mainBundle] loadNibNamed:@"SCChatListView" owner:self options:nil] lastObject];
            listView.delegate = self;
            listView.settings = _settings;
            //SCModelList *listForView = [[SCModelList alloc] initFromFile:@"emotionBase.plist"]; //Favorit_File_Name]; //
            //listView.list = listForView;
            
            
            [UIView animateWithDuration:0.25 animations:^{
                _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.bounds.size.width, self.view.bounds.size.height - scrollPosY0);
                _bottomMenuView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
                _bottomMenuView.alpha = 0.0;
            } completion:^(BOOL finished) {
                [_bottomMenuView removeFromSuperview];
                _bottomMenuView = nil;
                
                
                CGSize listSize = CGSizeMake(self.view.frame.size.width,[SCChatListView sizeView].height);
                listView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
                listView.alpha = 0.0f;
                [self.view addSubview:listView];
                _bottomMenuView = listView;
                [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
                    
                    listView.frame = CGRectMake(0, self.view.bounds.size.height - listSize.height, listSize.width, listSize.height);
                    listView.alpha = 1.0f;
                    _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.bounds.size.width, self.view.bounds.size.height - scrollPosY0 - listSize.height);
                }];

            }];
            [self hideBottomMenu];
        }
            break;
            
        case SCPopChatMenuViewItemUserDone:
        {
            [self fieldBtnAction:nil];
            [self hideBottomMenu];
        }
            break;
            
        case SCPopChatMenuViewItemUserCansel:
        {
            [self fieldBtnAction:nil];
            [self hideBottomMenu];
        }
            break;
            
        case SCPopChatMenuViewItemShowCamera:
        {
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            
            if (authStatus == AVAuthorizationStatusAuthorized) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showCamera];
                });
                
            } else {
                if (authStatus == AVAuthorizationStatusNotDetermined) {
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        if (granted) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self showCamera];
                            });
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self camraError];
                            });
                        }
                    }];
                } else {
                    [self camraError];
                }
            }

        }
            break;
            
        case SCPopChatMenuViewItemShowLibrary:
        {
            if (!_mediaLibraryPicker) {
                _mediaLibraryPicker = [[UIImagePickerController alloc] init];
                _mediaLibraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                _mediaLibraryPicker.delegate = self;
                _mediaLibraryPicker.allowsEditing = YES;
                _mediaLibraryPicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, (NSString *)kUTTypeMovie,nil];
            }
            [self takeMediaLibrary];

        }
            break;
            
        default:
            break;
    }
}

-(void) hideBottomMenu
{
    [UIView animateWithDuration:0.25 animations:^{
        _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.bounds.size.width, self.view.bounds.size.height - scrollPosY0);
        _bottomMenuView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
        _bottomMenuView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [_bottomMenuView removeFromSuperview];
        _bottomMenuView = nil;
    }];
}

- (IBAction)btnSendAction:(id)sender
{

    [_activityIndicatorSend startAnimating];
    _btnSend.enabled = NO;
    
    // Проверка текста
    if(_fldMessage.text.length > 0)
    {
        SCMessageBodyItem* messageBody = [[SCMessageBodyItem alloc] initWithSettings:_settings];
        messageBody.type = SCDataTypeText;//SCMessageDataTypeText
        messageBody.data = _fldMessage.text;
        [_messageBodys addItem:messageBody];
    }

    _fldMessage.text = @"";
    [_fldMessage resignFirstResponder];
    
    [_messageBodys sendMessageBodysWithCompletionBlock:^(SCNetFileComletionType type)
    {
        if(type == SCNetFileComletionTypeCompleet)
        {
            [self _sendProcess];
        }
        else if (type == SCNetFileComletionTypeError)
        {
            [_activityIndicatorSend stopAnimating];
            _btnSend.enabled = YES;
        }
    }];
}

-(void) _sendProcess
{
    if(_messageBodys && [_messageBodys items].count>0)
    {
        SCMessageItem* newMessage = [[SCMessageItem alloc] initWithChatItem:_chatItem];
        newMessage.contact = _settings.modelProfile.contactItem;
        newMessage.messageDateTime = [NSDate date];
        newMessage.timeDestroy = messageDestroyTime;
        [newMessage.messageBodys addItemsFromArray:[_messageBodys items]];
        [_messageBodys clear];

        [_activityIndicatorSend stopAnimating];
        _btnSend.enabled = YES;

        [_settings play:SCSoundPlayMessageSending];
        
        [_chatItem sendMessage:newMessage];
/*
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       _chatItem, @"delegate",
                                       [[SCPUUID alloc] initWithRandom], @"UUID",
                                       [NSNumber numberWithInteger:SCPBManagerParceTypeGetAllUsersForRoom], @"type",
                                       newMessage,@"SCMessageItem",
                                       nil];
        [_settings.modelProfile.protoBufManager sendMessage:params];
*/
        messageDestroyTime = 0;
    }
}

-(void) tapGestureAction:(UITapGestureRecognizer*) gesture
{
    if (_chatItem.chatType == SCChatTypePrivate)
    {
        FTLog(@"%@",_chatItem.chatOwner);
        
        ContactInformationViewController* pContactInformationViewController = [[[NSBundle mainBundle] loadNibNamed:@"ContactInformationViewController" owner:self options:nil] lastObject];
        pContactInformationViewController.settings = _settings;
        pContactInformationViewController.contact = _chatItem.chatOwner;
        self.navigationController.navigationBarHidden = NO;
        
        [self.navigationController pushViewController:pContactInformationViewController animated:YES];
    }
    else if(_chatItem.chatType == SCChatTypeGroup)
    {
        SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
        pSCNewChatViewController.settings = _settings;
        pSCNewChatViewController.chatItem = _chatItem;
        self.navigationController.navigationBarHidden = NO;
        
        [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
    }
    else if (_chatItem.chatType == SCChatTypePublic)
    {
        SCNewChatViewController* pSCNewChatViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCNewChatViewController" owner:self options:nil] lastObject];
        pSCNewChatViewController.settings = _settings;
        pSCNewChatViewController.chatItem = _chatItem;
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController pushViewController:pSCNewChatViewController animated:YES];
    }
}


#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    FTLog(@"scrollViewDidScroll %f",scrollView.contentOffset.y);
    if(scrollView.contentOffset.y < -1)
    {
        CGRect rr = _pullRefreshView.frame;
        _pullRefreshView.frame = CGRectMake(rr.origin.x, _topView.frame.size.height - rr.size.height - scrollView.contentOffset.y - 2, rr.size.width, rr.size.height);
        [UIView beginAnimations:nil context:NULL];
        if(ABS(scrollView.contentOffset.y)>REFRESH_HEADER_HEIGHT)
        {
            // User is scrolling above the header
            _refreshLabel.text = PULLREFRESH_TEXTRELEASE;
            [_refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        }
        else
        { // User is scrolling somewhere within the header
            _refreshLabel.text = PULLREFRESH_TEXTPULL;
            [_refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        }
        [UIView commitAnimations];
    }
}
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
//    FTLog(@"scrollViewWillBeginDecelerating %f",scrollView.contentOffset.y);
    if(scrollView.contentOffset.y<= -REFRESH_HEADER_HEIGHT)
    {
        [self loadHistory];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    FTLog(@"scrollViewDidEndDecelerating %f",scrollView.contentOffset.y);
    CGRect rr = _pullRefreshView.frame;
    _pullRefreshView.frame = CGRectMake(rr.origin.x, _topView.frame.size.height - rr.size.height - scrollView.contentOffset.y, rr.size.width, rr.size.height);
}

#pragma mark SCChatItemDelegate
-(void)loadedMessageItems:(NSArray*) newItems
{
    dispatch_async(dispatch_get_main_queue(), ^{
        FTLog(@"");
//        [self _makeViews:newItems];
    });
    
}
-(void)loadedGroupsMessage:(NSArray*) changedGroups
{
    __block NSArray* _changedGroups = [changedGroups copy];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self _makeDaysViewWidthArray:_changedGroups];
        _changedGroups = nil;
    });
}

#pragma mark - keyboard
- (void)_keyboardWillShow:(NSNotification *)note
{
    
    NSDictionary *info=[note userInfo];
//    FTLog(@"keyboard show %@",info);
    CGFloat animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGSize keyboardSize=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self hideBottomMenu];
    [UIView animateWithDuration:animationDuration animations:^
     {
         _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.frame.size.width, self.view.frame.size.height - scrollPosY0 - keyboardSize.height);
     }
                     completion:^(BOOL finished)
     {
     }];
    
}

- (void)_keyboardWillHide:(NSNotification *)note
{
    NSDictionary *info=[note userInfo];
//   FTLog(@"keyboard hide %@",info);
    CGFloat animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:animationDuration animations:^{
        _workspaceScrollView.frame = CGRectMake(0, scrollPosY0, self.view.frame.size.width, self.view.frame.size.height - scrollPosY0);
        
    } completion:^(BOOL finished)
     {
         //
     }];
    
}

#pragma mark - Protocol's Methods
- (void)sendFromListItemData:(NSArray *)array {
    //TODO: обработать вставку данных в сообщение
    
    for (SCMessageBodyItem *messageBody in array) {
        
        switch (messageBody.type) {
            case SCDataTypeText:
            {
                NSString *itemText = (NSString*) messageBody.data;
                _fldMessage.text = [_fldMessage.text stringByAppendingString:itemText];
//                FTLog(@"Item as Text =%@", itemText);
            }
                break;
                
            case SCDataTypeEmoticonLink:
            {
                NSString *sourceEmoticonFile = (NSString*) messageBody.data;
                FTLog(@"Item as Emoticon. File =%@", sourceEmoticonFile);
            }
                break;
                
                
            default:
                break;
        }
    }
}
-(void)setDestroyTime:(NSInteger) destroyTime
{
    messageDestroyTime = destroyTime;
}


#pragma mark SCPBManagerDelegate <NSObject>
-(void) parsedType:(SCPBManagerParceType) type data:(id) data
{
    FTLog(@"SCChatViewController : receiveParsedData ");
    switch (type)
    {
        case SCPBManagerParceTypeSendMessage:
        case SCPBManagerParceTypeMessage:
//        case SCPBManagerParceTypeGetChatRoomHistory:
        {
            if([data isKindOfClass:[NSArray class]])
            {
                [self _makeDaysViewWidthArray:data];
                CGRect rr = CGRectMake(0, _messageListsScrollView.contentSize.height - _messageListsScrollView.bounds.size.height, _messageListsScrollView.bounds.size.width, _messageListsScrollView.bounds.size.height);
                [_messageListsScrollView scrollRectToVisible:rr animated:YES];
            }
            else
                FTLog(@"Ups");
        }
            break;
            
        case SCPBManagerParceTypeUpdateMessage:
        case SCPBManagerParceTypeRemoveMessage:
        case SCPBManagerParceTypeRemoveMessageFromChatroomEvent:
        case SCPBManagerParceTypeUpdateMessageInChatroomEvent:
        {
            [UIView animateWithDuration:0.25 animations:^{
                
                NSMutableArray *processed;
                if([data isKindOfClass:[SCMessageGroup class]])
                {
                    processed = [[NSMutableArray alloc] initWithObjects:data, nil];
                }
                else if([data isKindOfClass:[NSArray class]])
                {
                    processed = data;
                }
                
                for(SCMessageGroup* groupMessages in processed)
                {
                    for(SCMessagesByDateView* groupItem in _daysViewsItems)
                    {
                        if([groupItem.messageGroup.messagesDate isEqualToDate:groupMessages.messagesDate])
                        {
                            [groupItem updateGroupMessages];
                            break;
                        }
                    }
                    [self _placeDaysView];
                }
            }];
        }
            break;
        
        case SCPBManagerParceTypeJoinUserToRoom:
        {
            [_chatItem.usersInChat addObject:data];
        }
            break;
            
        case SCPBManagerParceTypeGetAllUsersForRoom:
        {
            [_chatItem setUsersInChat:data];
        }
            break;
            
        case SCPBManagerParceTypeGetUserByAuth:
        {
            FTLog(@"");
            /*
            if([data isKindOfClass:[SCContactItem class]])
            {
                SCContactItem* addContactItem = data;
                NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                               self, @"delegate",
                                               [[SCPUUID alloc] initWithRandom], @"UUID",
                                               [NSNumber numberWithInteger:SCPBManagerParceTypeJoinUserToRoom], @"type",
                                               _chatItem,@"SCChatItem",
                                               addContactItem,@"SCContactItem",
                                               nil];
                [_settings.modelProfile.protoBufManager join UserToRoomRequest:params];
            }
            */
        }
            break;
            
        case SCPBManagerParceTypeGetChatRoomHistory:
        {
            NSArray* mesArray = (NSArray*) data;
            if(mesArray.count > 0)
            {
                SCChatItem* chat = ((SCMessageItem*) [mesArray objectAtIndex:0]).chat;
                [chat chatNewMessages:mesArray];
            }
        }
            break;
            
        case SCPBManagerParceTypeGetLastReadMessageDate:
        case SCPBManagerParceTypeSetLastReadMessageDate:
        {
            
        }
            break;
            
        default:
        {
            FTLog(@"%@ default case %@ ",NSStringFromClass([self class]),NSStringSCCallType(type)) ;
        }
            break;
    }
}


#pragma mark SCModelChatsDelegate <NSObject>
-(void) didAddModelChatsList:(SCChatItem*) chatItem
{
    FTLog(@"didAddModelChatsList");
}
-(void) didChangeModelChatsList:(SCChatItem*) chatItem
{
    FTLog(@"didChangeModelChatsList");
}
-(void) didChangeModelChatItem
{
    FTLog(@"didChangeModelChatItem");
}

#pragma mark Radchenko Method's
- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *titleMenu = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([titleMenu isEqualToString:kMediaLibraryMenu]) {
        
        if (!_mediaLibraryPicker) {
            _mediaLibraryPicker = [[UIImagePickerController alloc] init];
            _mediaLibraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            _mediaLibraryPicker.delegate = self;
            _mediaLibraryPicker.allowsEditing = YES;
            _mediaLibraryPicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, (NSString *)kUTTypeMovie,nil];
        }
        [self takeMediaLibrary];
        
        
    } else if ([titleMenu isEqualToString:kFotoVideoMenu]) {
        
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        
        if (authStatus == AVAuthorizationStatusAuthorized) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showCamera];
            });
            
        } else {
            if (authStatus == AVAuthorizationStatusNotDetermined) {
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if (granted) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self showCamera];
                        });
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self camraError];
                        });
                    }
                }];
            } else {
                [self camraError];
            }
        }
    };
}

- (void) camraError
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Denied access to the camera"
                                                        message:@"To allow access, you need to: \n1. Open the settings of your device. \n2. Go to \"Privacy \". \n3. Click \"camera \". \n4. Allow access to the camera. \n5. Re-visit to the shooting mode."
                                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
}

- (void) takeMediaLibrary
{
    [self presentViewController:_mediaLibraryPicker animated:YES completion:nil];
}

- (void) showCamera
{
    if (_fotoVideoPicker)
    {
        _fotoVideoPicker = nil;
    }
    
    _fotoVideoPicker = [self loadImagePickerController];
    [self presentViewController:_fotoVideoPicker animated:YES completion:nil];
}

- (UIImagePickerController *) loadImagePickerController
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    pickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    pickerController.mediaTypes = @[(NSString *) kUTTypeImage];
    
    //Camera controls
    pickerController.showsCameraControls = NO;
    pickerController.navigationBarHidden = YES;
    pickerController.allowsEditing = NO;
    pickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    pickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    
    SCOverlayView *overlayView = [[[NSBundle mainBundle] loadNibNamed:@"SCOverlayView" owner:self options:nil] firstObject];
    overlayView.delegate = self;
    overlayView.videoMaximumDuration = kVideoMaximumDuration;
    overlayView.currentFlashMode = pickerController.cameraFlashMode;
    overlayView.currentMode = [self modeOverViewBy:pickerController.mediaTypes];
    overlayView.frame = CGRectMake(0, 0, SZ_SCREEN_WIDTH, SZ_SCREEN_HEIGHT);
    
    pickerController.cameraOverlayView = overlayView;
    
    pickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0.0,50.0);
    
    return pickerController;
}

- (SCCurrentMode) modeOverViewBy: (NSArray *) mediaTypesArray
{
    if ([mediaTypesArray isEqualToArray:@[(NSString *) kUTTypeImage]]) {
        return SCCurrentModePhoto;
        
    } else if ([mediaTypesArray isEqualToArray:@[(NSString *) kUTTypeMovie]]) {
        return SCCurrentModeVideo;
    }
    
    return SCCurrentModePhoto;
}

#pragma mark UIImagePickerControllerDelegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString* mediaFileType = (NSString*)[info objectForKey:@"UIImagePickerControllerMediaType"];

//    if ([picker.mediaTypes isEqualToArray:@[(NSString *) kUTTypeImage]])
//    if([info[@"UIImagePickerControllerMediaType"] isEqualToString: @"public.movie"])
    if ([mediaFileType isEqualToString:@"public.image"])
    {
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];//UIImagePickerControllerEditedImage
        FTLog(@"chosenImage = %@",chosenImage);
        
        SCMessageBodyItem* messageBody = [[SCMessageBodyItem alloc] initWithSettings:_settings];
        messageBody.type = SCDataTypePhotoData;
        messageBody.data = UIImagePNGRepresentation(chosenImage);
        
        [_messageBodys addItem:messageBody];
        
    }
//    else if ([picker.mediaTypes containsObject:@[(NSString *) kUTTypeMovie]])
//    else if ([info[@"UIImagePickerControllerMediaType"] isEqualToString:(NSString *) kUTTypeMovie])
    else if ([mediaFileType isEqualToString:@"public.movie"])
    {
        NSURL *videoURL = info[UIImagePickerControllerMediaURL];
        FTLog(@"path =%@", [videoURL path]);
        FTLog(@"baseURL =%@", [videoURL baseURL]);
        FTLog(@"relativeString =%@", [videoURL relativeString]);
        FTLog(@"absoluteURL =%@", [videoURL absoluteURL]);
        FTLog(@"videoURL =%@", videoURL);

        SCMessageBodyItem* messageBody = [[SCMessageBodyItem alloc] initWithSettings:_settings];
        messageBody.type = SCDataTypeVideoFile;
        messageBody.data = [videoURL path];
        
        [_messageBodys addItem:messageBody];

    }
    

    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        [_mediaLibraryPicker dismissViewControllerAnimated:YES completion:NULL];
        
    } else if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        [_fotoVideoPicker dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark SCOverlayViewProtocol 
- (void) cancel
{
    [_fotoVideoPicker dismissViewControllerAnimated:YES completion:NULL];
}

- (void) changeCameraDevice
{
    if (_fotoVideoPicker.cameraDevice == UIImagePickerControllerCameraDeviceRear) {
        
        [UIView transitionWithView:_fotoVideoPicker.view duration:1.0 options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionTransitionFlipFromLeft animations:^{
            _fotoVideoPicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        } completion:NULL];
        
    } else {
        
        [UIView transitionWithView:_fotoVideoPicker.view duration:1.0 options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionTransitionFlipFromLeft animations:^{
            _fotoVideoPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        } completion:NULL];
    }
}

- (void) takePicture
{
    if ([_fotoVideoPicker.mediaTypes isEqualToArray:@[(NSString *) kUTTypeMovie]]) {
        _fotoVideoPicker.mediaTypes = @[(NSString *) kUTTypeImage];
        _fotoVideoPicker.cameraViewTransform = CGAffineTransformMakeTranslation(0.0,50.0);
    }
    
    [_fotoVideoPicker takePicture];
    
}
- (void) startVideoCapture
{
    //Move picker
    if ([_fotoVideoPicker.mediaTypes isEqualToArray:@[(NSString *) kUTTypeImage]]) {
        _fotoVideoPicker.mediaTypes = @[(NSString *) kUTTypeMovie];
        _fotoVideoPicker.videoMaximumDuration = kVideoMaximumDuration;
        _fotoVideoPicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
        _fotoVideoPicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
        
        [UIView animateWithDuration:1.0 animations:^{
            _fotoVideoPicker.cameraViewTransform = CGAffineTransformMakeTranslation(0.0,0.0);
        }];
    }
    
    BOOL resultStartVideoFlag = [_fotoVideoPicker startVideoCapture];
    if (resultStartVideoFlag) {
        FTLog(@"OK [_fotoVideoPicker startVideoCapture]");
    } else {
        FTLog(@"not start video");
    }
    
}
- (void) stopVideoCapture
{
    [_fotoVideoPicker stopVideoCapture];
}

@end
