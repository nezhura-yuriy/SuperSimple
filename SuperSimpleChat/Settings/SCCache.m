//
//  SCCache.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 25.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCCache.h"
#import <CommonCrypto/CommonDigest.h>

@interface SCCache()

@property(nonatomic, strong) NSString *cachePath;
@property(nonatomic, strong) NSString *name;

@end


@implementation SCCache


-(id)initWithCachePath:(NSString*) cachePath_
{
    if ( (self = [super init]))
    {
        self.cachePath = cachePath_;
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:self.cachePath])
        {
            NSAssert(YES, @"can't create cache directory");
        }
        NSAssert(YES, @"TEST");
        
    }
    return self;
}

-(void) setCacheImagePath:(NSString *)cachePath_
{
    self.cachePath = cachePath_;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.cachePath])
    {
        NSAssert(YES, @"can't create cache directory");
    }
    NSAssert(YES, @"TEST");
}

- (void)saveFile:(NSString*)uuid_ data:(NSData*)file_
{
    NSString *name = [self _sha1String:uuid_];
    [file_ writeToFile:[NSString stringWithFormat:@"%@/%@", self.cachePath, name]
            atomically:NO];
}

- (NSData*)getFile:(NSString*)uuid_
{
    NSString *name = [self _sha1String:uuid_];
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@", self.cachePath, name];
    if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
        return [NSData dataWithContentsOfFile:fullPath];
    }
    return nil;
}

- (NSString*)getFilePath:(NSString*)uuid_
{
    NSString *name = [self _sha1String:uuid_];
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@", self.cachePath, name];
    if (fullPath) {
        return fullPath;
    }
    return nil;
}

#pragma mark private

- (NSString*)_sha1String:(NSString*)source
{
    NSData *data = [source dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}


@end
