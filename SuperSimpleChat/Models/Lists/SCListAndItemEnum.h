//
//  SCListAndItemEnum.h
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 13.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

//ListType
//https://gitlab.com/Bischak/smartchat-backend-api/blob/staging/api_proto_interface.md#listtype
typedef NS_ENUM(NSInteger, SCListType)  {
    SCListTypePublic,
    SCListTypePrivate,
    SCListTypePurchased,
    SCListTypeNotInitialised,
    SCListTypeDefault,
    SCListTypeShared
};

//ContentType
//https://gitlab.com/Bischak/smartchat-backend-api/blob/staging/api_proto_interface.md#contenttype
typedef NS_ENUM(NSInteger, SCContentType) {
    SCContentTypeText = 0,
    SCContentTypeFile,
    SCContentTypeEmoticon
};
