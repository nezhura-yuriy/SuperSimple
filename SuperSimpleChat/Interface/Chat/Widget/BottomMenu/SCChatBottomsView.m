//
//  SCChatBottomsView.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"

@implementation SCChatBottomsView
{
//    void (^afterSelect) (SCPopChatMenuViewItem selectItem);
    
}

-(id) init
{
    self = [super init];
    if(self)
    {
        [self _init];
    }
    return self;
}
-(id) initWithSettings:(SCSettings*) settings
{
    self = [super init];
    if(self)
    {
        _settings = settings;
    }
    return self;
}

/*
-(id) initWithCompletionblock:(void (^)(SCPopChatMenuViewItem selectItem))completionBlock
{
    self = [super init];
    if(self)
    {
        afterSelect = [completionBlock copy];
        [self _init];
    }
    return self;
    
}
*/

-(void) awakeFromNib
{
    [super awakeFromNib];
//    [self _init];
}

-(void) _init
{
    self.clipsToBounds = YES;
}

-(void) dealloc
{
    FTLog(@"SCChatBottomsView : dealoc");
}

/*
-(void) setAfterSelect:(void(^)(SCPopChatMenuViewItem selectItem)) completionBlock
{
    afterSelect = [completionBlock copy];
}
*/

-(void) layoutSubviews
{
    [super layoutSubviews];
}


-(void) updateItems
{
    
}

-(CGSize) sizeView
{
    // переопределить в наследнике
    return CGSizeMake(1, 1);
}

+(CGSize) sizeView
{
    // переопределить в наследнике
    return CGSizeMake(0, 0);
}

@end
