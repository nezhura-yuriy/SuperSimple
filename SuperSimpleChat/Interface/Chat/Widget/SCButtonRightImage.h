//
//  SCButtonRightImage.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 24.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCButtonRightImage : UIButton


-(void) setRightImage:(UIImage*) image;
@end
