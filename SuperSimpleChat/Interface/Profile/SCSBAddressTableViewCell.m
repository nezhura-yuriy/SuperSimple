//
//  SCSBAddressTableViewCell.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 11.06.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCSBAddressTableViewCell.h"

@implementation SCSBAddressTableViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
    [self setupUI];
}

- (void) _init
{
    
}

- (void) setupUI
{
    
}

- (instancetype)init
{
    [self _init];
    self = [super init];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    [self _init];
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    _icoImageView.frame = CGRectMake(_edgeInsets.left, 0.0,
                                     self.contentView.frame.size.height, self.contentView.frame.size.height);
    
    CGFloat titleLabelX = _icoImageView.frame.origin.x + _icoImageView.frame.size.width;
    
    _titleLabel.frame = CGRectMake(titleLabelX, 0.0,
                                   self.contentView.frame.size.width - _edgeInsets.left - _edgeInsets.right,
                                   self.contentView.frame.size.height);
}

- (void) setSettings:(SCSettings *)settings
{
    _settings = settings;
    _icoImageView.image = [_settings.skinManager getImageForKeyX:@"profile-address"];
    _titleLabel.textColor = SKCOLOR_TableCellFont;
}

@end
