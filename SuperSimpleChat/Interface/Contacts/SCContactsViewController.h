//
//  SCContactsViewController.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 13.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCInterfaceDelegates.h"


@class SCSettings;


@interface SCContactsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic,strong) SCSettings* settings;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic,strong) id<SCMainRevalControllerDelegate> revalDelegate;

@property (weak, nonatomic) IBOutlet UIView *accessView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activitiIndicator;

@property (strong, nonatomic) UISearchBar *searchBar;

@property (strong, nonatomic) UIButton *inviteAllContactsButton;

@property (strong ,nonatomic) UIView* viewTableHeader;



@end
