//
//  SCMediaCapture.h
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SCModelsDelegates.h"
#import "SCTypes.h"

typedef enum : NSUInteger {
    SCCaptureModeVideo,
    SCCaptureModePhoto,
} SCCaptureMode;

@class SCSettings;

@interface SCMediaCapture : UIView <AVCaptureFileOutputRecordingDelegate>

@property(nonatomic,assign) SCCaptureMode captureMode;
@property(nonatomic,weak) id<SCMediaCaptureDelegate> delegate;
//-(id) initWithSettings:(SCSettings*) settings;
-(void) setSettings:(SCSettings*) settings;
@end
