//
//  NSString+SCContains.h
//  SuperSimpleChat
//
//  Created by Линник Александр on 07.08.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SCContains)

- (BOOL)myContainsString:(NSString*)other;

@end
