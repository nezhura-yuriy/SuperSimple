//
//  SCListItem.h
//  SmartChat
//
//  Created by Yury Radchenko on 1/14/15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//
/**
 Класс описывает модель фразы, выражения, картинки в Листе
 https://gitlab.com/Bischak/smartchat-backend-api/blob/staging/com.smartchat.models.List#listitem
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SCPUUID.h"
#import "SCListAndItemEnum.h"

@class SCListItem;

@protocol SCListItemProtocol <NSObject>
- (void) modifiedItem:(SCListItem*) item;
@end

@interface SCListItem : NSObject

@property (strong, nonatomic) SCPUUID *puuid;
@property (strong, nonatomic) NSString *text;
@property (nonatomic) SCContentType type;
@property (nonatomic) NSInteger sortBy; // **DEPRECATED**
@property (strong, nonatomic) NSString *contentSource;

@property (nonatomic, weak) id <SCListItemProtocol> delegate;

- (instancetype) initWithText: (NSString *) text;
+ (instancetype) itemWithText: (NSString *) text;
+ (instancetype) itemRandomPUUIDWithText: (NSString *) text ;
- (instancetype) initWithItem:(SCListItem *) realItem;

- (instancetype) initFromDictionary: (NSDictionary *) dictionary;
- (NSDictionary *) toDictionary;

- (NSArray *) toMessageArray;

- (BOOL) isEqualItem:(SCListItem *) item;
- (void) equateItem: (SCListItem *) item;

@end
