//
//  SCContactItemSmall.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 19.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"

@interface SCContactItemSmall : SCAbstractItem

@property(nonatomic,strong) NSString* contactID;
@property(nonatomic,strong) NSString* contactNikName;
@property(nonatomic,strong) NSString* imageLink;


@end
