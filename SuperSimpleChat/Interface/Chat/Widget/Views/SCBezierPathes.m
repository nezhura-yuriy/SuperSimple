//
//  SCBezierPathes.m
//  SmartChat
//
//  Created by Yuriy Nezhura on 30.03.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCBezierPathes.h"

#define RADIUS_SIZE 10.0f


@implementation SCBezierPathes

+(CGFloat) radiusSize
{
    return RADIUS_SIZE;
}

+(UIBezierPath*) messageBezier:(CGRect) rectView rectArea:(CGRect) rectArea zeroPoint:(CGPoint) point
{
    UIBezierPath* path = [UIBezierPath bezierPath];
    CGFloat rad = RADIUS_SIZE;

    [path moveToPoint:CGPointMake(rectArea.origin.x,rectArea.origin.y + rad)];
    [path moveToPoint:CGPointMake(rectArea.origin.x,rectArea.origin.y + rad)];
    // лево
    [path addLineToPoint:CGPointMake(rectArea.origin.x, rectArea.origin.y + rectArea.size.height - rad)];
    if(point.x < rectArea.origin.x)
    {
/////
        [path addQuadCurveToPoint:CGPointMake(point.x, point.y)
                     controlPoint:CGPointMake(rectArea.origin.x, rectArea.origin.y + rectArea.size.height)];
        [path addQuadCurveToPoint:CGPointMake(rectArea.origin.x, rectArea.origin.y + rectArea.size.height - 0.25f*rad)
                     controlPoint:CGPointMake(rectArea.origin.x, rectArea.origin.y + rectArea.size.height)];
/////
    }
    // лево - низ
    [path addQuadCurveToPoint:CGPointMake(rectArea.origin.x + rad, rectArea.origin.y + rectArea.size.height)
                 controlPoint:CGPointMake(rectArea.origin.x, rectArea.origin.y + rectArea.size.height)];
    // низ
    [path addLineToPoint:CGPointMake(rectArea.origin.x + rectArea.size.width -  rad, rectArea.origin.y + rectArea.size.height)];
    if(point.x > rectArea.origin.x + rectArea.size.width)
    {
/////
        [path addQuadCurveToPoint:CGPointMake(rectArea.origin.x + rectArea.size.width, rectArea.origin.y + rectArea.size.height- 0.25*rad)
                     controlPoint:CGPointMake(rectArea.origin.x + rectArea.size.width, rectArea.origin.y + rectArea.size.height)];
        [path addQuadCurveToPoint:CGPointMake(point.x, point.y)
                     controlPoint:CGPointMake(rectArea.origin.x + rectArea.size.width, rectArea.origin.y + rectArea.size.height)];
/////
    }
    // низ - право
        [path addQuadCurveToPoint:CGPointMake(rectArea.origin.x + rectArea.size.width, rectArea.origin.y + rectArea.size.height - rad)
                 controlPoint:CGPointMake(rectArea.origin.x + rectArea.size.width, rectArea.origin.y + rectArea.size.height)];
    // право
    [path addLineToPoint:CGPointMake(rectArea.origin.x + rectArea.size.width, rectArea.origin.y + rad)];
    // право - верх
    [path addQuadCurveToPoint:CGPointMake(rectArea.origin.x + rectArea.size.width - rad, rectArea.origin.y)
                 controlPoint:CGPointMake(rectArea.origin.x + rectArea.size.width, rectArea.origin.y)];
    // верх
    [path addLineToPoint:CGPointMake(rectArea.origin.x + rad, rectArea.origin.y)];
    //  верх - лево
    [path addQuadCurveToPoint:CGPointMake(rectArea.origin.x, rectArea.origin.y + rad)
                 controlPoint:CGPointMake(rectArea.origin.x, rectArea.origin.y)];

    
    return path;
}
@end
