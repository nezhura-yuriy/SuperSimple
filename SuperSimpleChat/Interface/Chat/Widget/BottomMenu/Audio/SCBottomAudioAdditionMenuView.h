//
//  SCBottomAudioAdditionMenuView.h
//  SuperSimpleChat
//
//  Created by Nezhura Yuriy on 05.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCChatBottomsView.h"
#import "SCMediaItemView.h"

@class SCMessageBodys;

@interface SCBottomAudioAdditionMenuView : SCChatBottomsView <SCMediaItemViewDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
//    SCMessageBodys* _messageBodys;
}
//@property (nonatomic,strong) SCMessageBodys* messageBodys;
//@property(nonatomic,strong) NSMutableArray* imagesArray;

-(void) setActionSelector:(void(^)(id selectItem)) completionBlock;
-(void) previewAtIndex:(NSUInteger) idx;
@end
