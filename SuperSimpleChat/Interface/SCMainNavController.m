//
//  SCMainNavController.m
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 06.05.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMainNavController.h"
#import "SCMainTabBarViewController.h"
#import "SCLaunchScreen.h"
#import "SCSettings.h"
#import "SCVerificationsViewController.h"
#import "SCAccessPhoneViewController.h"
#import "SCAgreementViewController.h"

@interface SCMainNavController ()

@end

@implementation SCMainNavController
{
    SCMainTabBarViewController* mainTabBarViewController;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [_settings settingsStart];
    _settings.delegate = self;
    
    if (![userDefaults objectForKey:@"isFirstLoadApp"]){
    
    SCAgreementViewController* agreementViewControlle= [[[NSBundle mainBundle] loadNibNamed:@"SCAgreementViewController" owner:self options:nil]lastObject];
        
        //_settings = [SCSettings sharedSettings];  /* Для пушей */
        
        agreementViewControlle.settings = _settings;
        
    self.viewControllers = [[NSArray alloc] initWithObjects:agreementViewControlle, nil];

        
    } else {
        
        
        [self launchAppAfterAcceptAgreement];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void) launchAppAfterAcceptAgreement
{
    
    SCLaunchScreen* launchScreenView = [[[NSBundle mainBundle] loadNibNamed:@"SCLaunchScreen" owner:self options:nil]lastObject];

    launchScreenView.frame = [UIScreen mainScreen].bounds;
    launchScreenView.labProgressInfo.text = @"Try restore previous session";
    
    UIViewController* launchScreen = [[UIViewController alloc] init];
    [launchScreen.view addSubview:launchScreenView];
    self.viewControllers = [[NSArray alloc] initWithObjects:launchScreen, nil];
    
//    _settings = [SCSettings sharedSettings]; /* Для пушей */
    
    [_settings netInit];
    
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    FTLog(@"%@ : didReceiveMemoryWarning",NSStringFromClass([self class]));
}

#pragma mark SCMainNavControllerDelegate <NSObject>
-(void) succsessNetInit
{
    UIViewController* oldViewC = [self.viewControllers lastObject];
    mainTabBarViewController = [[SCMainTabBarViewController alloc] init];
    mainTabBarViewController.settings = _settings;
    
    self.navigationBar.translucent = YES;
    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    
    self.viewControllers = [[NSArray alloc] initWithObjects:mainTabBarViewController,oldViewC, nil];
    [self popViewControllerAnimated:YES];
}

-(void) tryLogin
{
    UIViewController* oldViewC = [self.viewControllers objectAtIndex:0];
    
    SCAccessPhoneViewController* pSCLoginViewController = nil;
    
    pSCLoginViewController = [[[NSBundle mainBundle] loadNibNamed:@"SCAccessPhoneViewController" owner:self options:nil]lastObject];
    pSCLoginViewController.settings = _settings;
    pSCLoginViewController.delegate = self;
    self.viewControllers = [[NSArray alloc] initWithObjects:pSCLoginViewController,oldViewC, nil];
    [self popViewControllerAnimated:YES];
    
}

-(void) succsessLogin
{
    [_settings netInit];
}

@end
