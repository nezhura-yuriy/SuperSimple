//
//  SCMessageBodyTextView.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 10.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCMessageBodyItemView.h"
#import "SCMessageBodyGroupView.h"

@class SCSettings;

@interface SCMessageBodyTextGroupView : SCMessageBodyGroupView


-(id) initWithSettings:(SCSettings*) settings;
-(CGSize) viewSize;

@end
