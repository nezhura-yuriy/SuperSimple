//
//  SCInterfaceDelegates.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 12.01.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>


// SCMainNavControllerDelegate ==========================================================================
@protocol SCMainNavControllerDelegate <NSObject>

-(void) succsessNetInit;
-(void) tryLogin;
-(void) succsessLogin;

@end




// SCMenuViewDelegate ==========================================================================
typedef enum
{
    SCMenuViewKindFull,
    SCMenuViewKindSmall,
    SCMenuViewKindHidd
} SCMenuViewKind;

typedef enum
{
    SCMenuViewSelectChats,
    SCMenuViewSelectContacts,
    SCMenuViewSelectLists
} SCMenuViewSelect;


@protocol SCMenuViewDelegate <NSObject>

-(void) selectItem:(SCMenuViewSelect) selItem;
-(void) changeKind;

@end




// SCMainRevalControllerDelegate ========================================================================
typedef enum
{
    SCMainRevalItemMenu,
    SCMainRevalItemChat,
    SCMainRevalItemContacts,
    SCMainRevalItemList,
    SCMainRevalItemProfile
} SCMainRevalItem;

@protocol SCMainRevalControllerDelegate <NSObject>

-(void) selectItem:(SCMainRevalItem) item;

@end

// SCChatViewControllerDelegate =========================================================================
typedef enum
{
    // level top
    SCPopChatMenuViewItemPhoto,
    SCPopChatMenuViewItemAudio,
    SCPopChatMenuViewItemVideo,
    SCPopChatMenuViewItemUser,
    SCPopChatMenuViewItemSettings,
    SCPopChatMenuViewItemList,
    // level Photo
    SCPopChatMenuViewItemPhotoSource,
    // level Audio
    SCPopChatMenuViewItemAudioSource,
    // level Video
    SCPopChatMenuViewItemVideoSource,
    // level User
    SCPopChatMenuViewItemUserAction,
    SCPopChatMenuViewItemUserDone,
    SCPopChatMenuViewItemUserCansel,
    // level chatSettings
    SCPopChatMenuViewItemChatSettings,
    // level Lists
    SCPopChatMenuViewItemListAction,
    // controlls
    SCPopChatMenuViewItemShowCamera,
    SCPopChatMenuViewItemShowLibrary
    
} SCPopChatMenuViewItem;

typedef enum
{
    // level top
    SCChatTopMenuViewItemPhoto,
    SCChatTopMenuViewItemAudio,
    SCChatTopMenuViewItemVideo,
    SCChatTopMenuViewItemUser,
    SCChatTopMenuViewItemDestroy,
} SCChatTopMenuViewItem;

// SCChatListViewController =========================================================================
@class SCChatListCell;
@protocol SCChatListViewControllerDelegate <NSObject>

@optional
-(void) cellNeedReload:(SCChatListCell*) cell;
@end

// SCChatViewControllerDelegate =========================================================================
//@protocol SCChatViewControllerDelegate <NSObject>
//
////-(void)selectItem:(SCPopChatMenuViewItem) selectItem;
//
//@optional
//-(void)sendFromListItemData:(NSArray *) array;
//-(void)selectTopItem:(SCChatTopMenuViewItem) item;
//-(void)setDestroyTime:(NSInteger) destroyTime;
//
//@end



// SCMediaItemViewDelegate =========================================================================
#define AddFromCameraPhoto @"ADD_FROM_CAMERA_PHOTO"
#define AddFromLibraryPhoto @"ADD_FROM_LIBRARY_PHOTO"
#define AddFromCameraVideo @"ADD_FROM_CAMERA_VIDEO"
#define AddFromLibraryVideo @"ADD_FROM_LIBRARY_VIDEO"
#define AddFromMicrophoneAudio @"ADD_FROM_MICROPHONE_AUDIO"

@protocol SCMediaItemViewDelegate <NSObject>

@optional
-(void) item_Tap:(id) item;
-(void) item_Swipe:(id) item;
@end

// SCBottomViewsDelegate ========================================================================
@protocol SCBottomViewsDelegate <NSObject>

@optional
-(void) itemTap:(id) item;
-(void) itemSwipe:(id) item;
//-(void) selectItem:(id) item;
-(void)sendFromListItemData:(NSArray *) array;
-(void)selectTopItem:(SCChatTopMenuViewItem) item;
-(void)setDestroyTime:(NSInteger) destroyTime;
-(void)selectItem:(SCPopChatMenuViewItem) selectItem;

@end



// SCChatUsersViewController =========================================================================
@protocol SCChatUsersViewControllerDelegate <NSObject>

@optional
-(void) updateUserList:(NSMutableArray*) userInRoomNew;
@end
// =========================================================================

