//
//  SCPBManagerTernopol+Profile.h
//  SmartChat
//
//  Created by Yuriy Nezhura on 14.04.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCPBManagerTernopol.h"

@interface SCPBManagerTernopol (Profile)

-(void) parseProfileGet:(id)data;
-(void) parseSetProfileField:(id)data;
-(void) parseRemoveProfileField:(id)data;
-(void) parseCheckIsNicknameFree:(id)data;
-(void) parseChangeStatus:(id)data;
-(void) parseLogout:(id) data;

-(void) parseAuthAdd:(id) data;
-(void) parseAuthConfirm:(id) data;
-(void) parseAuthRemove:(id) data;
-(void) parseAuthGet:(id)data;

//-(void) authAllGet:(id) params;
-(void) parseSubscribeToAmazonSNS:(id)data;
-(void) parseUnsubscribeFromAmazonSns:(id)data;

-(void) parseAuthorizationFailedEvent:(id)data;
@end
