//
//  SCAdress.h
//  SmartChat
//
//  Created by Yuriy Troyan on 06.02.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCAbstractItem.h"

@interface SCAdress : SCAbstractItem

@property (nonatomic, strong) NSString * country;
@property (nonatomic, strong) NSString * postCode;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * street;
@property (nonatomic, strong) NSString * house;
@property (nonatomic, strong) NSString * appartament;


-(void) fromDictionary:(NSDictionary*) dictionary;
-(NSDictionary*) toDictionary;
-(NSString*) fullAddress;
@end
