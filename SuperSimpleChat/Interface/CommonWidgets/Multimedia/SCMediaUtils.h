//
//  SCMediaUtils.h
//  SuperSimpleChat
//
//  Created by Yuriy Nezhura on 31.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCMediaUtils : NSObject

+(BOOL) haveCamera;
+ (NSString *) formatSecond: (NSTimeInterval)numberSeconds;
@end
