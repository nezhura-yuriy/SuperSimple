//
//  SCWarningView.m
//  SuperSimpleChat
//
//  Created by Yury Radchenko on 15.07.15.
//  Copyright (c) 2015 Yuriy Nezhura. All rights reserved.
//

#import "SCWarningView.h"

@implementation SCWarningView
{
    UILabel *_warningLabel;
    UIButton *_gotoListViewControllerButton;
    SCSettings *_settings;
}
- (void) awakeFromNib
{
    [super awakeFromNib];
    [self _init];
}

- (instancetype)initWithFrame:(CGRect)frame withSettings:(SCSettings *) settings
{
    self = [super initWithFrame:frame];
    
    if (self) {
        _settings = settings;
        [self _init];
        [self setupUI];
    }
    return self;
}

- (void) _init
{
    //LABEL
    _warningLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    _warningLabel.numberOfLines = 4;
    _warningLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_warningLabel];
    
    //BUTTON
    _gotoListViewControllerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_gotoListViewControllerButton addTarget:self action:@selector(gotoViewControllerButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_gotoListViewControllerButton];
}

- (void) setupUI
{
    //LABEL
    NSDictionary *attribute = @{NSForegroundColorAttributeName:SKCOLOR_FontRegular,
                                NSFontAttributeName: [UIFont fontWithName:FT_FontNameLight size:12]
                                };
    
    NSString *warningLabelTitle = @"You can create a list of your standart phrases and quickly insert them into your messages";
    _warningLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:warningLabelTitle
                                                                          attributes:attribute];
    
    //BUTTON
    NSString *buttonTitle = @"Go to a list to create a phrase";
    _gotoListViewControllerButton.titleLabel.numberOfLines = 2;
    [_gotoListViewControllerButton setAttributedTitle:[self attribeteText:buttonTitle forState:UIControlStateNormal] forState:UIControlStateNormal];
    [_gotoListViewControllerButton setAttributedTitle:[self attribeteText:buttonTitle forState:UIControlStateHighlighted] forState:UIControlStateHighlighted];
    
    [_gotoListViewControllerButton setBackgroundColor:[UIColor clearColor]];
    
    UIColor *buttonBorderColor = SKCOLOR_FontRegular;
    _gotoListViewControllerButton.layer.borderColor = buttonBorderColor.CGColor;
    _gotoListViewControllerButton.layer.borderWidth = 0.5;
    
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    _warningLabel.frame = CGRectMake(0, 0,
                                     self.bounds.size.width, self.bounds.size.height/2);
    
    _gotoListViewControllerButton.frame = CGRectMake(0, _warningLabel.frame.origin.y + _warningLabel.frame.size.height,
                                                 self.bounds.size.width, 44);
}

//MARK: Button
- (NSMutableAttributedString *) attribeteText:(NSString *) text forState: (UIControlState) controlState
{
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    UIColor *fontColor;
    UIFont *font;
    CGFloat fontSize = 13;
    
    if(controlState == UIControlStateNormal) {
        fontColor = SKCOLOR_FontRegular;
        font = [UIFont fontWithName:FT_FontNameLight size:fontSize];
        
    } else if (controlState == UIControlStateHighlighted) {
        fontColor = SKCOLOR_FontRegular;
        font = [UIFont fontWithName:FT_FontNameRegular size:fontSize];
        
    } else {
        fontColor = SKCOLOR_FontRegular;
        font = [UIFont fontWithName:FT_FontNameLight size:fontSize];
    }
    
    NSDictionary *attribute = @{NSForegroundColorAttributeName:fontColor,
                                NSFontAttributeName: font,
                                NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attribute];
}


//MARK: Actions
- (void) gotoViewControllerButtonPressed
{
    [_delegate gotoListController];
}

@end
